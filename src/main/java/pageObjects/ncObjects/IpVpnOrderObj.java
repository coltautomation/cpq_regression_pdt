package pageObjects.ncObjects;

import java.io.File;
import java.io.IOException;

import baseClasses.DataMiner;
import baseClasses.ReadExcelFile;
public class IpVpnOrderObj {
	
	public static class IpVpnOrder
	 { 
		public static final String Topnavegation="@xpath=//*[@id='gen_menu_3']";
		public static final String BandwidthEtherlink="@xpath=//td[text()='Bandwidth']/following-sibling::*//input";
		public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String FilterSelectName="@xpath=//*[@class='inputs']";
		public static final String FilterInputValue="@xpath=//*[@name='inp']";
		public static final String ApplyButton="@xpath=//*[@class='FilterButtons'] //*[text()='Apply']";
		public static final String NewCompositeOrder="@xpath=//a[text()='New Composite Order']";
		public static final String NcOrderNumber="@xpath=//td[@id='vv_-1']/span";
		public static final String OrderDescription="@xpath=//*[@name='common_descr']";
		public static final String Update="@xpath=//a[text()='Update']";
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String LinkforOrder="@xpath=//a[contains(text(),'Order #')]";
		public static final String OrderTab="@xpath=//a[text()='Orders']";
		public static final String AddonOrderTab="@xpath=//a[text()='Add']";
		public static final String Productifram="@xpath=//iframe[@id='linkage']";
		public static final String Addbutton="@xpath=//button[text()='Add']";
		public static final String IpvpnserviceCheckBox="@xpath=//a[text()='IP VPN Service Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String IpvpnSiteCheckBox="@xpath=//a[text()='IP VPN Site Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String IpVpnEndPointProdCheckBox="@xpath=//a[text()='VPN Endpoint Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String UnderlyningipvpnServiceOrder="@xpath=//span[contains(text(),'IP VPN Service Product Order')]/parent::a";
		public static final String UnderlyningIpvpnSiteOrder="@xpath=//span[contains(text(),'IP VPN Site Product Order')]/parent::a";
		public static final String UnderlyningEthernetOrder="@xpath=//span[contains(text(),'Ethernet Connection')]/parent::a";
		public static final String UnderlyningVpnEndpointOrder="@xpath=//span[contains(text(),'VPN Endpoint Product Order')]/parent::a";
		public static final String UnderlyingLanVlanOrder="@xpath=//span[contains(text(),'LAN VLAN Product Order')]/parent::a";
		public static final String Serviceproductorder="@xpath=//a/span[contains(text(),'IP VPN Service Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Subvpnproductorder="@xpath=(//a/span[contains(text(),'New Sub-VPN Product Order')])/parent::*";
		public static final String SiteProductOrder="@xpath=//a/span[contains(text(),'IP VPN Site Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String SubEthernetOrder="@xpath=//a/span[contains(text(),'New Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String EndSiteOrder="@xpath=//span[contains(text(),'End Site')]/parent::a";
		public static final String VpnEndPointOrder="@xpath=//a/span[contains(text(),'New VPN Endpoint Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Edit="@xpath=//a[text()='Edit']";
		public static final String Bandwidth="@xpath=//td/a[text()='Service Bandwidth (Primary)']/parent::*/following-sibling::*//select";
		public static final String Layer3Resilience="@xpath=//td/a[text()='Layer 3 Resilience']/parent::*/following-sibling::*//select";
		public static final String SiteOrderNumber="@xpath=//td/a[text()='Order Number']/parent::*/following-sibling::*//input";
		public static final String RouterType="@xpath=//td/a[text()='Router Type']/parent::*/following-sibling::*//select";
		public static final String Ipvpnnetworkreference="@xpath=//td/a[text()='IPVPN Network Reference']/parent::*/following-sibling::*//input";
		public static final String OrderSystemOrderNo="@xpath=//td/a[text()='Order System Order Number']/parent::*/following-sibling::*//input";
		public static final String SiteType="@xpath=//td/a[text()='Site Type']/parent::*/following-sibling::*//select";
		public static final String Topology="@xpath=//td/a[text()='Topology']/parent::*/following-sibling::*//select";
		public static final String SnmpEnabledFlag="@xpath=//td/a[text()='SNMP Enabled']/parent::*/following-sibling::*//select";
		public static final String HubMeshPrimaryFlag="@xpath=//td/a[text()='Hub/Mesh Primary']/parent::*/following-sibling::*//select";
		public static final String OrderSystemName="@xpath=//td/a[text()='Order System']/parent::*/following-sibling::*//select";
		public static final String RouterTechnology="@xpath=//td/a[text()='Router Technology']/parent::*/following-sibling::*//select";
		public static final String OrderSysSerId="@xpath=//td/a[text()='Order System Service ID']/parent::*/following-sibling::*//input";
		public static final String OrderManageGrp="@xpath=//td/a[text()='Order Management Group']/parent::*/following-sibling::*//select";
		public static final String BackupBandwidth="@xpath=//td/a[text()='Service Bandwidth (Backup)']/parent::*/following-sibling::*//select";
		public static final String ServiceOrderRef="@xpath=//td/a[text()='Service Order Reference']/parent::*/following-sibling::*//input";
		public static final String Servicetype="@xpath=//td/a[text()='Service Type']/parent::*/following-sibling::*//select";
		public static final String NetFlowFlag="@xpath=//td/a[text()='Netflow enabled']/parent::*/following-sibling::*//select";
		public static final String AdvFirewallFlag="@xpath=//td/a[text()='Advanced Firewall']/parent::*/following-sibling::*//select";
		public static final String RelatedIpVpnService="@xpath=//td/a[text()='Related IP VPN Service']/parent::*/following-sibling::*//input";
		public static final String RelatedIpVpnSite="@xpath=//td/a[text()='Related IP VPN Site']/parent::*/following-sibling::*//input";
		public static final String NCServiceid="@xpath=//td/a[text()='NC Service ID']/parent::*/following-sibling::*//a";
		public static final String CommercialProductName="@xpath=//td/a[text()='Commercial Product Name']/parent::*/following-sibling::*//input";
		public static final String ServiceInfoTab="@xpath=//td/a[text()='Commercial Product Name']/parent::*/following-sibling::*//input";
		public static final String OrderCheckbox="@xpath=//input[@class='checkbox checkbox_input']";
		
		//GeneralInformation
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";
		public static final String SubVPNProductComponentlink="@xpath=//span[text()='Sub-VPN Product Component']/parent::*";
		public static final String IpAddressingFormat="@xpath=//td/a[text()='IP Addressing Format']/parent::*/following-sibling::*//select";
		public static final String Subvpnid="@xpath=//td/a[text()='Sub-VPN ID']/parent::*/following-sibling::*//input";
		public static final String Customeralias="@xpath=//td/a[text()='Customer Alias']/parent::*/following-sibling::*//input";
		public static final String Voiptrafficonly="@xpath=//td/a[text()='VoIP Traffic Only']/parent::*/following-sibling::*//select";
		public static final String EthernetProdVlComponentLink="@xpath=//span[text()='Ethernet Product VL Component']/parent::*";
		public static final String EthernetconnectionproductLink="@xpath=//span[text()='Ethernet Connection Product']/parent::*";
		public static final String SubVPNPlink="@xpath=//span[text()='Sub VPN']/parent::*";
		public static final String CircuitCategory="@xpath=//td/a[text()='Circuit Category']/parent::*/following-sibling::*//select";
		public static final String VLCompBandwidth="@xpath=(//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//input)[3]";
		public static final String CosClassification="@xpath=//td/a[text()='COS Classification']/parent::*/following-sibling::*//select";
		public static final String ClassOfService="@xpath=//td/a[text()='Class of Service']/parent::*/following-sibling::*//select";
		public static final String AddFeaturelink="@xpath=//a[text()='Add Feature']";
		public static final String TypeofFeature="@xpath=//a[text()='Add Feature']/following-sibling::*//textarea";
		public static final String SelectFeature="@xpath=//input[@value='Select']";
		public static final String Browse="@xpath=//div/a[text()='Browse']";
	//	public static final String SelectFeature="@xpath=//input[@value='Select']";
		public static final String SelectButton="@xpath=//div/a[contains(text(),'Select')]";
		
		// NetFlow
		public static final String Netflowlink="@xpath=//span[text()='Netflow']/parent::*";
		public static final String IPv4address="@xpath=//td/a[text()='IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String Netflowversion="@xpath=//td/a[text()='Netflow version']/parent::*/following-sibling::*//select";
		public static final String Portnumber="@xpath=//td/a[text()='Port Number']/parent::*/following-sibling::*//input";
		
		//Snmp
		public static final String Snmpv3link="@xpath=//span[text()='SNMP v3']/parent::*";
		public static final String groupname="@xpath=//td/a[text()='Group Name']/parent::*/following-sibling::*//input";
		public static final String Username="@xpath=//td/a[text()='Username']/parent::*/following-sibling::*//input";
		public static final String Authentpassword="@xpath=//td/a[text()='Authentication Password']/parent::*/following-sibling::*//input";
		public static final String Privacypassword="@xpath=//td/a[text()='Privacy Password']/parent::*/following-sibling::*//input";
		public static final String SnmpIPv4deviceslink="@xpath=//span[text()='SNMP IPv4 Devices']/parent::*";
		public static final String Customersubvpnalias="@xpath=//td/a[text()='Customer Sub-VPN Alias']/parent::*/following-sibling::*//input";
		public static final String Devicetype="@xpath=//td/a[text()='Device Type']/parent::*/following-sibling::*//select";
		public static final String Snmpreadonlylink="@xpath=//span[text()='SNMP v3 Read Only Access']/parent::*";
		public static final String Snmpformat="@xpath=//td/a[text()='SNMP Format']/parent::*/following-sibling::*//select";

		//ComponentDetails

		public static final String Diversitylink="@xpath=//span[contains(text(),'Diversity')]/parent::*";
		public static final String RelatedServiced="@xpath=//td/a[text()='Related Service Technical Service ID']/parent::*/following-sibling::*//input";
		public static final String Diversityselect="@xpath=//td/a[text()='Diversity']/parent::*/following-sibling::*//select";

		public static final String DhcpRelayLink="@xpath=//span[contains(text(),'DHCP Relay')]/parent::*";
		public static final String DhcpFormat="@xpath=//td/a[text()='DHCP Format']/parent::*/following-sibling::*//select";
		public static final String SiebelCompId="@xpath=//td/a[text()='Siebel Component ID']/parent::*/following-sibling::*//input";
		public static final String Ipv4DhcpRelayLink="@xpath=//span[contains(text(),'IPv4 DHCP Relay')]/parent::*";
		public static final String DhcpRelayIpv4Add="@xpath=//td/a[text()='DHCP Relay IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String ClassofServiceLink="@xpath=//span[contains(text(),'Class of Service')]/parent::*";
		public static final String ManagementPerc="@xpath=//td/a[text()='Management (%)']/parent::*/following-sibling::*//input";
		public static final String PremiumPerc="@xpath=//td/a[text()='Premium (%)']/parent::*/following-sibling::*//input";
		public static final String StandardPerc="@xpath=//td/a[text()='Standard (%)']/parent::*/following-sibling::*//input";
		public static final String Business1Perc="@xpath=//td/a[text()='Business 1 (%)']/parent::*/following-sibling::*//input";
		public static final String Business2Perc="@xpath=//td/a[text()='Business 2 (%)']/parent::*/following-sibling::*//input";
		public static final String Business3Perc="@xpath=//td/a[text()='Business 3 (%)']/parent::*/following-sibling::*//input";
		public static final String CoSMarking="@xpath=//td/a[text()='CoS Marking']/parent::*/following-sibling::*//select";

		//WanAddressing
		public static final String WanAddressingLink="@xpath=//span[contains(text(),'WAN Addressing')]/parent::*";
		public static final String PEName="@xpath=//td/a[text()='PE Name']/parent::*/following-sibling::*//input";
		public static final String PEIpv4Prefix="@xpath=//td/a[text()='IPv4 Prefix']/parent::*/following-sibling::*//input";
		public static final String PEIpv4Address="@xpath=//td/a[text()='PE IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String CPEIpv4Address="@xpath=//td/a[text()='CPE IPv4 Address']/parent::*/following-sibling::*//input";
	

		// Ipv4AddBasedMarking
		public static final String Ipv4AddBasedMarkingLink="@xpath=//span[contains(text(),'IPv4 Address Based Marking')]/parent::*";
		public static final String TrafficClass="@xpath=//td/a[text()='Traffic Class']/parent::*/following-sibling::*//select";
		public static final String DestIpv4Netmask="@xpath=//td/a[text()='Destination IPv4 Netmask']/parent::*/following-sibling::*//input";
		public static final String SourIpv4Netmask="@xpath=//td/a[text()='Source IPv4 Netmask']/parent::*/following-sibling::*//input";
		public static final String DestIpv4Addr="@xpath=//td/a[text()='Destination IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String SourIpv4Add="@xpath=//td/a[text()='Source IPv4 Address']/parent::*/following-sibling::*//input";
		// DscpMarking
		public static final String DscpMarkingLink="@xpath=//span[contains(text(),'DSCP Marking')]/parent::*";
		public static final String StandardDscp="@xpath=//td/a[text()='Standard DSCP']/parent::*/following-sibling::*//select";
		public static final String PreniumDscp="@xpath=//td/a[text()='Premium DSCP']/parent::*/following-sibling::*//select";
		public static final String Business1Dscp="@xpath=//td/a[text()='Business 1 DSCP']/parent::*/following-sibling::*//select";
		public static final String Business2Dscp="@xpath=//td/a[text()='Business 2 DSCP']/parent::*/following-sibling::*//select";
		public static final String Business3Dscp="@xpath=//td/a[text()='Business 3 DSCP']/parent::*/following-sibling::*//select";
		
		// LanVlan
		public static final String LanVlanCompLink="@xpath=//span[contains(text(),'LAN VLAN Component')]/parent::*";
		public static final String LanVlanTagId="@xpath=//td/a[text()='VLAN Tag ID']/parent::*/following-sibling::*//input";
		public static final String LanPhyPortIdPrimary="@xpath=//td/a[text()='Physical Port ID (Primary)']/parent::*/following-sibling::*//input";
		public static final String LanPhyPortIdSecondary="@xpath=//td/a[text()='Physical Port ID (Secondary)']/parent::*/following-sibling::*//input";
		public static final String LanInterIpv4AddLink="@xpath=//span[contains(text(),'LAN Interface IPV4 Addressing')]/parent::*";
		public static final String PrimaryRange="@xpath=//td/a[text()='Primary Range']/parent::*/following-sibling::*//select";
		public static final String LanInterfIpv4Add="@xpath=//td/a[text()='LAN Interface IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String LanInterfIpv4Prefix="@xpath=//td/a[text()='LAN Interface IPv4 Prefix']/parent::*/following-sibling::*//input";
		public static final String LanInterfVrrpIpv4Add1="@xpath=//td/a[text()='LAN VRRP IPv4 Address 1']/parent::*/following-sibling::*//input";
		public static final String LanInterfVrrpIpv4Add2="@xpath=//td/a[text()='LAN VRRP IPv4 Address 2']/parent::*/following-sibling::*//input";
		public static final String SecondCpeLanInterfIpv4Add="@xpath=//td/a[text()='Secondary CPE LAN Interface IPv4 Address']/parent::*/following-sibling::*//input";
	
		public static final String EndpointCircuitDetailVL="@xpath=//span[contains(text(),'Endpoint Circuit Details VL')]/parent::*";
		public static final String VLServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//select";
		public static final String VLConnectionType="@xpath=//td/a[text()='Connection Type']/parent::*/following-sibling::*//select";
		public static final String Wanvlanid="@xpath=//td/a[text()='WAN VLAN ID']/parent::*/following-sibling::*//input";

		// Nat
		public static final String StaticNatLink="@xpath=//span[text()='Static NAT']/parent::*";
		public static final String StaticIPRuleLink="@xpath=//span[text()='Static NAT IPv4 Rule']/parent::*";
		public static final String SubnetMask="@xpath=//td/a[text()='Sub Net Mask']/parent::*/following-sibling::*//input";
		public static final String InsideIpv4Addr="@xpath=//td/a[text()='Inside IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String OutsideIpv4Addr="@xpath=//td/a[text()='Outside IPv4 Address']/parent::*/following-sibling::*//input";
		// Bgp
		public static final String WanBgpLink="@xpath=//span[text()='WAN BGP']/parent::*";
		public static final String Md5Pwd="@xpath=//td/a[text()='MD 5 Password']/parent::*/following-sibling::*//input";
		public static final String HoldTimer="@xpath=//td/a[text()='Hold-Timer (s)']/parent::*/following-sibling::*//input";
		public static final String KeepAlive="@xpath=//td/a[text()='Keep-Alive (s)']/parent::*/following-sibling::*//input";
		public static final String BgpComponentLink="@xpath=//span[text()='BGP Component']/parent::*";
		public static final String Ipv4PeerAdd="@xpath=//td/a[text()='IPv4 Peer Address']/parent::*/following-sibling::*//input";
		public static final String PeerASNum="@xpath=//td/a[text()='Peer AS Number']/parent::*/following-sibling::*//input";
		
		public static final String StaticRouteIpv4Link="@xpath=//span[contains(text(),'Static IPv4 Route')]/parent::*";
		public static final String TargetNetwork="@xpath=//td/a[text()='Target Network']/parent::*/following-sibling::*//input";
		public static final String NextHopIp="@xpath=//td/a[text()='Next Hop IP']/parent::*/following-sibling::*//input";
	//	public static final String SubnetMask="@xpath=//td/a[text()='Sub Net Mask']/parent::*/following-sibling::*//input";
		public static final String VpnCpeCfsVlLink="@xpath=//span[contains(text(),'VPN CPE CFS VL')]/parent::*";
		public static final String WanVlanId="@xpath=//td/a[text()='WAN VLAN ID']/parent::*/following-sibling::*//input";
		// SiteProductDetail
		public static final String ResilienceOption="@xpath=//td/a[text()='Resilience Option']/parent::*/following-sibling::*//select";
		public static final String AccessTechnolgy="@xpath=//td/a[text()='Access Technology']/parent::*/following-sibling::*//select";
		public static final String AccessType="@xpath=//td/a[text()='Access Type']/parent::*/following-sibling::*//select";
		public static final String SiteEnd="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*//select";
		public static final String SiteID="@xpath=//td/a[text()='Site ID']/parent::*/following-sibling::*//input";
		// Accessport
		
		public static final String PresentConnectType="@xpath=//td/a[text()='Presentation Connector Type']/parent::*/following-sibling::*//select";
		public static final String PresentInterface="@xpath=//td/a[text()='Presentation Interface']/parent::*/following-sibling::*//select";
		public static final String AccessportRole="@xpath=//td/a[text()='Port Role']/parent::*/following-sibling::*//select";
		public static final String VlanTaggingMode="@xpath=//td/a[text()='VLAN Tagging Mode']/parent::*/following-sibling::td[1]//select";
		public static final String AutoNegotiation="@xpath=//td/a[text()='AutoNegotiation']/parent::*/following-sibling::*//select";
		// CPE
		public static final String CPElink="@xpath=//span[text()='CPE Information']/parent::*";
	
		public static final String CabinetID="@xpath=//td/a[text()='Cabinet ID']/parent::*/following-sibling::*//input";
		// VLAN
		public static final String VLANLink="@xpath=//span[text()='VLAN']/parent::*";
		
		public static final String Ethertype="@xpath=//*[text()[contains(.,'Ethertype')]]/parent::*/following-sibling::*";
		
		public static final String Decompose="@xpath=//a[text()='Decompose']";
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";
		// Tasks
		public static final String SerialNumbersetvalidate="@xpath=(//td/a[text()='GX/LTS Serial Number']/parent::*/following-sibling::*//input[@type='text'])[1]";
		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String TaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[New]')]";
		public static final String Workitems="@xpath=//a[text()='Work Items']";
		public static final String Scheduler="@xpath=//a[text()='Scheduler']";
		public static final String Errors="@xpath=//a[text()='Errors']";
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";
		public static final String  TaskActivetoComplete="@xpath=//td[text()='Active']/parent::*/td//a";
		public static final String AccessNetworkElement="@xpath=//td/a[text()='Access Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AccessPort="@xpath=//td/a[text()='Access Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort1="@xpath=//*[text()='CPE NNI Port 1']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort2="@xpath=//*[text()='CPE NNI Port 2']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PENetworkElement="@xpath=//*[text()='PE Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PePort="@xpath=//*[text()='PE Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String VCXController="@xpath=//*[text()='VCX Controller']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Beacon="@xpath=//*[text()='Beacon']/parent::*/following-sibling::*//input[@type='text']";
		public static final String EthernetLinkTransportRFS_1="@xpath=(//span[text()='Ethernet Link Transport RFS']/parent::*)[1]";
		public static final String EthernetLinkTransportRFS_2="@xpath=(//span[text()='Ethernet Link Transport RFS']/parent::*)[2]";
		public static final String L3cpeName="@xpath=//td/a[text()='L3 CPE']/parent::*/following-sibling::*//input[@type='text']";
		public static final String L3cpeUNIPort="@xpath=//td/a[text()='L3 CPE UNI Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String EgressPolicyMap="@xpath=//td/a[text()='Egress Policy Map']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Asr="@xpath=//td/a[text()='ASR']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AsrGil="@xpath=//td/a[text()='ASR GIL']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OAMProfile="@xpath=//*[text()='OAM Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String SerialTaskName="@xpath=//td/a[text()='Name']/parent::*/following-sibling::*[1]";
		public static final String UpdateAntSerialNumber="@xpath=//a[text()='Update ANT Serial Number']";
		public static final String UpdateSerialNumber="@xpath=//a[text()='Update Serial Number']";
		public static final String LanIpAddress="@xpath=//td/a[text()='LAN IP Address']/parent::*/following-sibling::*//input[@type='text']";
		public static final String RequestNewIp="@xpath=//a[text()='Request New IP Range in EIP']";
		public static final String ReleaseLanRange="@xpath=//*[text()='Release LAN Range']/parent::*/following-sibling::*//select";
		public static final String BespokeHQoSEntries="@xpath=//a[text()='Bespoke HQoS Entries']";
		public static final String WanIpSubnetSize="@xpath=//td/a[text()='WAN IPv4 Subnet Size']/parent::*/following-sibling::*//select";
		public static final String WanIpSubnetSizeFlag="@xpath=(//td/a[text()='WAN IPv4 Subnet Size']/parent::*/following-sibling::*//select)[2]";
		public static final String OtherWanIpSubnetSize="@xpath=//td/a[text()='Other WAN IPv4 Subnet Size']/parent::*/following-sibling::*//input";
		public static final String Duplex="@xpath=//td/a[text()='duplex']/parent::*/following-sibling::*//select";
		public static final String DuplexYesorNo="@xpath=(//td/a[text()='duplex']/parent::*/following-sibling::*//select)[2]";
		public static final String Speed="@xpath=//td/a[text()='speed']/parent::*/following-sibling::*//select";
		public static final String SpeedYesorNo="@xpath=(//td/a[text()='speed']/parent::*/following-sibling::*//select)[2]";
		public static final String BfdInterval="@xpath=//td/a[text()='bfd interval/min_rx']/parent::*/following-sibling::*//input";
		public static final String BfdIntervalYesorNo="@xpath=//td/a[text()='bfd interval/min_rx']/parent::*/following-sibling::*//select";
		public static final String Multiplier="@xpath=//td/a[text()='multiplier']/parent::*/following-sibling::*//input";
		public static final String MultiplierYesorNo="@xpath=//td/a[text()='multiplier']/parent::*/following-sibling::*//select";
		public static final String PremiumCir="@xpath=//td/a[text()='Premium CIR %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String InternetCir="@xpath=//td/a[text()='Internet CIR %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OLONNIProfile="@xpath=//td/a[text()='OLO NNI Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OVCCircuit="@xpath=//td/a[text()='OVC Circuit']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeProfile="@xpath=//*[text()='Legacy CPE Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeOamLevel="@xpath=//*[text()='Legacy CPE OAM Level']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Complete="@xpath=//a[@tooltip='Complete']";
		public static final String TransComplete="@xpath=//a[text()='Complete']";
		public static final String WorkItemSelect="@xpath=//a/span[contains(text(),'Reserve Access Resources')]/parent::*/parent::*/parent::*/td//input";
		public static final String View="@xpath=//a[text()='View']";
		public static final String OrderCompleted="@xpath=//a/span[contains(text(),'+arrOfStr1[1]+')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]";


		
	//	public static final String SiebelCompId="@xpath=//td/a[text()='Siebel Component ID']/parent::*/following-sibling::*//input";
		public static final String LLF="@xpath=//td/a[text()='LLF']/parent::*/following-sibling::*//select";
		public static final String ThirdPartyAccessProvider="@xpath=//a[contains(@name,'Third Party Access Provider')]//following-sibling::*//select";
		public static final String ThirdPartySlaTier="@xpath=//a[contains(@name,'Third Party SLA Tier')]//following-sibling::*//select";
		public static final String ThirdPartyConnecRef="@xpath=//a[contains(@name,'3rd Party Connection Reference')]//following-sibling::*//input";
		public static final String AddHub="@xpath=//a[text()='Add Hub']";
		public static final String TypeofHub="@xpath=//a[text()='Add Hub']/following-sibling::*//textarea";
		public static final String AddHubProdOrder="@xpath=//img[@alt='New/Modify Hub Product Order']/following-sibling::div[1]";
		public static final String NewHubSiteProductend="@xpath=(//a/span[contains(text(),'New Hub Site Product')])/parent::*";
		public static final String NewEndSiteProductAend="@xpath=//td[text()='Entering']/parent::*/td/a";
		public static final String NewEndSiteProductBend="@xpath=(//a/span[contains(text(),'New End Site Product')])[2]/parent::*";
		public static final String TransportCfs="@xpath=//a[starts-with(@tooltip,'New Transport CFS Order')]";


		public static final String ServiceOrderTab="@xpath=//a[text()='Service Order']";
		public static final String InputOrderNo="@xpath=//input[@aria-labelledby='QuerySrchSpec_Label']";
		public static final String SearchOrderNo="@xpath=//input[@aria-labelledby='QuerySrchSpec_Label']/following-sibling::*[1]";
		
		public static final String ClickOrderNo="@xpath=//a[@name='COLT Service Order Num']";
		public static final String Accounts="@xpath=//a[text()='Accounts']";
		public static final String AccountAdministration="@xpath=//a[text()='Accounts Administration']";
		public static final String AccountsDropdown="@xpath=//div[@class='siebui-btn-grp-search']//span[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']";
		public static final String SelectAccountNo="@xpath=//li[text()='Account Number (#)']";
		public static final String InputAccountNo="@xpath=//input[@aria-labelledby='QuerySrchSpec_Label']";
		public static final String AccountGoButton="@xpath=//input[@aria-labelledby='QuerySrchSpec_Label']/following-sibling::*";
		public static final String OCNfield="@xpath=//input[@aria-label='Account Number(OCN)']";
		public static final String GoButton="@xpath=//span[text()='Go']";
		public static final String AccountName="@xpath=//a[text()='ZZZZ ES TEST']";
		public static final String InstalledAssetNew="@xpath=//*[@id='s_1_1_34_0_Ctrl']";
		public static final String OpportunityNo="@xpath=//*[(@aria-labelledby='Opportunity_Number_Label')]";
		public static final String RequestReceivedDate="@xpath=//*[(@aria-label='Request Received Date')]";
		public static final String DeliveryChannel="@xpath=//*[(@aria-labelledby='COLT_Delivery_Channel_Label')]";
		public static final String OrderingPartySearch="@xpath=//div[@class='mceGridField siebui-value mceField']//span[@id='s_2_1_61_0_icon']";
		public static final String PartySearchPopupDropdown="@xpath=//*[@class='siebui-popup-button']//*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']";
		public static final String PartyName="@xpath=//*[@class='ui-menu-item'][text()='Party Name']";
		public static final String InputPartyname="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String PickAccntOk="@xpath=//button[@title='Pick Account:OK']";
		public static final String OrderingParty="@xpath=//*[(@aria-labelledby='Ordering_Party_Label')]";
		public static final String OrderingPartyAddrSearch="@xpath=//div[@class='mceGridField siebui-value mceField']//span[@id='s_2_1_137_0_icon']";
		public static final String OrderingPartyAddrSearchDropdown="@xpath=//*[@class='siebui-popup-button']//*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']";
		public static final String PartyAddr="@xpath=//*[@class='ui-menu-item'][text()='Street Name']";
		public static final String InputPartyAddr="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String PickAddrOk="@xpath=//button[@title='Pick Address:OK']";
		public static final String PickAddrSubmit="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Address:Go']";
		public static final String PickAccnt="@xpath=//*[contains(@title,'13869')]";
		public static final String OrderingPartyContactSearch="@xpath=//div[@class='mceGridField siebui-value mceField']//span[@id='s_2_1_31_0_icon']";
		public static final String PartyContactPopupDropdown="@xpath=//*[@class='siebui-popup-button']//*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']";
		public static final String FirstName="@xpath=//*[@class='ui-menu-item'][text()='First Name']";
		public static final String InputFirstname="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String FirstnameSubmit="@xpath=//button[@title='Pick Contact:OK']";
		public static final String SalesChannel="@xpath=//*[(@aria-labelledby='COLT_Sales_Channel_Label')]";
		public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]";
		public static final String AddEthernetLine1="@xpath=//button[@id='addButton4']";
		public static final String AddEthernetLine2="@xpath=//div[text()='Ethernet Line']/following-sibling::*";
		public static final String AddEthernetLine="@xpath=//button[@id='addButton1']";
		public static final String AddEthernetHub="@xpath=//div[text()='Ethernet Hub']/following-sibling::*";
		public static final String AddEthernetSpoke="@xpath=//div[text()='Ethernet Spoke']/following-sibling::*";
		public static final String AddIpAccess="@xpath=//button[@id='addButton5']";
		public static final String ServiceOrderReferenceNo="@xpath=//a[@name='Order Number']";
		public static final String ExistingOrderNo="@xpath=//a[@name='COLT Service Order Num']";
		public static final String SearchAddressSiteA="@xpath=//img[@id='imgSelectAddressSiteA']";
		public static final String SearchAddressSiteB="@xpath=//img[@id='imgSelectAddressSiteB']";
		public static final String Searchbutton="@xpath=//span[@class='search_img']/img";
		public static final String SelectSite="@xpath=//span[@title='Select a site']";
		public static final String StreetName="@xpath=//input[@id='txtStreetName']";
		public static final String IpAStreetname="@xpath=//input[@name='streetName']";
		public static final String Country="@xpath=//select[@class='ctrlClasstype'][@id='ddlCountry']";
		public static final String IpACountry="@xpath=//input[@name='country']";
		public static final String City="@xpath=//input[@class='ctrlClasstype'][@id='txtCityTown']";
		public static final String IpACity="@xpath=//input[@name='cityTown']";
		public static final String PostalCode="@xpath=//input[@class='ctrlClasstype'][@id='txtPostalCode']";
		public static final String IpAPostalCode="@xpath=//input[@name='postalZipCode']";
		public static final String Premises="@xpath=//input[@class='ctrlClasstype'][@id='txtPremises']";
		public static final String IpAPremises="@xpath=//input[@name='premisesNumber']";
		public static final String Search="@xpath=//input[@value='Search']";
		public static final String IpASearch="@xpath=//div/button[@class='colt-siteselection-button colt-primary-btn']";
		public static final String pickAddress="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[1]";
		public static final String IpASelectAddress="@xpath=//div[@id='colt-siteselection-result-container']//tr[2]";
		public static final String PickAddress="@xpath=//table[@id='address_table']/tbody/tr[2]";
		public static final String Pick="@xpath=//div[@id='PopupDiv']//input[@value='Pick']";
		public static final String IpAPickAddressBtn="@xpath=//div[@id='colt-site-selection-popup-content']//button[text()='Pick Address']";
		public static final String pickBuilding="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[3]";
		public static final String PickBulding="@xpath=//table[@id='address_table']/tbody/tr";
		public static final String IpAPickBuildingBtn="@xpath=//div[@id='colt-site-selection-popup-content']//button[text()='Pick Building']";
		public static final String IpASelectBuilding="@xpath=//div[@id='colt-site-selection-popup-content']";
		public static final String PickSite="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[1]";
		public static final String IpAPickSiteBtn="@xpath=//div[@id='colt-site-selection-popup-content']//button[text()='Pick Site']";
		public static final String ServicePartySearchSiteA="@xpath=//div[@id='SiteAddress_A']//img[@name='=Service Party']";
		public static final String ServicePartySearchSiteB="@xpath=//div[@id='SiteAddress_B']//img[@name='Service Party']";
		public static final String ServicePartySearch="@xpath=//img[@name='Service Party']";
		public static final String SelectServiceParty="@xpath=//select[@id='ddlSearchData']";
		public static final String InputServicePartyName="@xpath=//input[@id='txtSearchInput']";
		public static final String SearchServicePartyName="@xpath=//img[@title='Search'][@id='imgSearchContactData']";
		public static final String pickServiceParty="@xpath=//div[@id='divAddressList']//table[@id='address_table']/tbody/tr[1]";
		public static final String PickButton="@xpath=//input[@value='Pick'][@id='PickAccount']";
		public static final String IpAServicePartySearch="@xpath=//span[text()='Service Party']/following-sibling::*/following-sibling::*";
		public static final String IpASelServiceParty="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']";
		public static final String IpAInpServicePartyName="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String IpASearchServPartyName="@xpath=//button[@id='s_6_1_378_0_Ctrl']";
		public static final String IpAPickServiceParty="@xpath=//div[@id='ui-id-581']//tr[2]";
		public static final String IpAOkButton="@xpath=//button[@title='Pick Account:OK']";
		public static final String SiteContactSearchSiteA="@xpath=//div[@id='SiteAddress_A']//img[@name='Site Contact']";
		public static final String SiteContactSearchSiteB="@xpath=//div[@id='SiteAddress_B']//img[@name='Site Contact']";
		public static final String SiteContactSearch="@xpath=//img[@name='Site Contact']";
		public static final String InputContactName="@xpath=//input[@id='txtSearchInput']";
		public static final String SearchContactName="@xpath=//img[@id='imgSearchContactData']";
		public static final String pickContactName="@xpath=//div[@id='divAddressList']//table[@id='address_table']/tbody/tr[1]";
		public static final String PickContact="@xpath=//input[@id='PickContact']";
		public static final String IpASiteContactSearch="@xpath=//span[text()='Site Contact']/following-sibling::*/following-sibling::*";
		public static final String IpASearchSiteContactName="@xpath=//button[@id='s_12_1_378_0_Ctrl']";
		public static final String IpAPickSiteContact="@xpath=//div[@id='ui-id-581']//tr[3]";
		public static final String IpASiteContactOk="@xpath=//button[@title='Pick Contact:OK']";
		public static final String SiteDetailCustomize="@xpath=//button[@id='btnSiteCustom'][@data-lineid='1-1HQOZH7']";
		public static final String SiteDetailCustomizeSiteA="@xpath=//div[@id='SiteinfoA']//button[@id='btnSiteCustom']";
		public static final String CustomizeButton="@xpath=//button[@id='btnMiddleCustom']";
		public static final String IpAccClickFullInfo="@xpath=//div/a[@id='colt-s-order-enabledfeatures-showfullinfo']";
		public static final String EthernetlineLink="@xpath=//a[text()='Ethernet Line']";
		public static final String Coverage="@xpath=//a[contains(@name,'Coverage')]//following-sibling::*//select";
		public static final String CoverageSpoke="@xpath=//table[@id='customtable']//select[@name='Coverage']";
		public static final String ServiceBandwidthSpoke="@xpath=//table[@id='customtable']//select[@name='Service Bandwidth']";
		public static final String AEndResilienceOption="@xpath=//a[contains(@name,'A End Resilience Option')]//following-sibling::*//select";
		public static final String BEndResilienceOption="@xpath=//a[contains(@name,'B End Resilience Option')]//following-sibling::*//select";
		public static final String ResilienceSpoke="@xpath=//table[@id='customtable']//select[@name='Resilience Option']";
		public static final String Resilience="@xpath=//a[contains(@name,'Resilience Option')]//following-sibling::*//select";
		//public static final String RouterType="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//div[@id="@xpath=attr_1-34XV95Z"]/input";
		public static final String ManualRouterSelection="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//div[@id='attr_1-34XV961']/input";
		//public static final String RouterTechnology="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//div[@id="@xpath=attr_1-34XV960"]/input";
		public static final String SerBandwidth="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//div[@id='attr_1-34XV962']/input";
		public static final String LayerResilience="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//div[@id='attr_1-34XV96B']/input";
		public static final String SavePage="@xpath=//div/span/a[@href='#']";
		public static final String AEndSite="@xpath=//a[text()='A End Site'][@href='javascript:void(0);']";
		public static final String BEndSite="@xpath=//a[text()='B End Site'][@href='javascript:void(0);']";
		public static final String InstallationTimeLink="@xpath=//a[text()='Installation Time'][@href='javascript:void(0);']";
		public static final String InstallTime="@xpath=//a[contains(@name,'Install Time')]//following-sibling::*//select";
		public static final String InstallTimeBEnd="@xpath=//a[contains(@name,'Install Time')]//following-sibling::*//select";
		public static final String ThirdPartyAccessProviderSearch="@xpath=//div[@class='cxEdit siebui-ecfg-editfield']//span[@class='siebui-icon-icon_pick ']/img";
		public static final String ThirdPartyAccessProviderSelect="@xpath=//table[@summary='Select Third Party Access Provider']/tbody/tr[2]";
		public static final String ThirdPartyAccessProviderSubmit="@xpath=//button[@title='Select Third Party Access Provider:OK']";
		public static final String AccessTechnologySearch="@xpath=//div[@class='cxEdit']//span[@class='siebui-icon-icon_pick ']/img";
		public static final String AccessTechnologyInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String AccessTechnologySelect="@xpath=//tr[@id='3']//td[text()='Ethernet over NGN']";
		public static final String AccessTechnologySubmit="@xpath=//button[@title='Select Access Technology:OK']";

		public static final String CustomerSitePopStatus="@xpath=//a[contains(@name,'Customer Site Pop Status')]//following-sibling::*//select";
		public static final String CustomerSitePopStatusBEnd="@xpath=//a[contains(@name,'Customer Site Pop Status')]//following-sibling::*//select";
		public static final String BuildingType="@xpath=//a[contains(@name,'Building Type')]//following-sibling::*//select";
		public static final String BuildingtypeBEnd="@xpath=//a[contains(@name,'Building Type')]//following-sibling::*//select";
		public static final String CPEInformationLink="@xpath=//a[text()='CPE Information'][@href='javascript:void(0);']";
		public static final String CabinetType="@xpath=//td/a[text()='Cabinet Type']/parent::*/following-sibling::*//select";
		public static final String CabinetTypeBEnd="@xpath=//a[contains(@name,'Cabinet Type')]/following-sibling::*//select";
		public static final String CabinetId="@xpath=//a[contains(@name,'Cabinet ID')]/following-sibling::*//input";
		public static final String CabinetIdBEnd="@xpath=//a[contains(@name,'Cabinet ID')]/following-sibling::*//input";
		public static final String ShelfID="@xpath=//a[contains(@name,'Shelf ID')]/following-sibling::*//input";
		public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
		public static final String PortRole="@xpath=//a[contains(@name,'Port Role')]/following-sibling::*//select";
		public static final String SlotID="@xpath=//a[contains(@name,'Slot ID')]/following-sibling::*//input";
		public static final String PhysicalPortID="@xpath=//a[contains(@name,'Physical Port ID')]/following-sibling::*//input";
		public static final String VLAN="@xpath=//a[text()='VLAN'][@href='javascript:void(0);']";
		public static final String Vlannew="@xpath=//div/a[text()='VLAN'][@href='javascript:void(0);']";

		public static final String VlanTagId="@xpath=//a[contains(@name,'VLAN Tag ID')]/following-sibling::*//input";
		public static final String PortRoleBEnd="@xpath=//a[contains(@name,'Port Role')]/following-sibling::*//select";
		public static final String PresentationInterfaceSearch="@xpath=//img[@class='siebui-ctrl-img ']";
		public static final String SelectPresentationInterface="@xpath=//tr[@id='5']//td[text()='LC/PC']";
		public static final String SelectPresentationInterfaceSpoke="@xpath=//tr[@id='3']//td[text()='LC/PC']";
		public static final String SelectPresentationInterfaceHub="@xpath=//tr[@id='2']//td[text()='LC/PC']";
		public static final String PickPresentationInterface="@xpath=//table[@summary='Select Presentation Interface-Connector Type']/tbody/tr[2]";
		public static final String SubmitPresentationInterface="@xpath=//button[@data-display='OK']";
		public static final String EthernetHubLink="@xpath=//a[text()='Ethernet Hub']";
		public static final String SelectServiceBandwidth="@xpath=//a[contains(@name,'Service Bandwidth')]//following-sibling::*//select";
		public static final String SelectResilienceOption="@xpath=//a[contains(@name,'Resilience Option')]//following-sibling::*//select";
		public static final String EthernetConnectionLink="@xpath=//a[text()='Ethernet Connection'][@href='javascript:void(0);']";
		public static final String HubSiteLink="@xpath=//a[text()='Hub Site']";
		public static final String SpokeSiteLink="@xpath=//a[text()='Spoke Site']";
		public static final String SelectAccessTechnology="@xpath=//table[@summary='Select Access Technology']/tbody/tr[2]";
		public static final String SubmitAccessTechnology="@xpath=//button[@title='Select Access Technology:OK']";
		public static final String DoneEthernetConnection="@xpath=//button[text()='Done']";
		public static final String Proceed="@xpath=//button[text()='Proceed']";
		public static final String OrderSubTypeSearch="@xpath=//div[@class='mceGridField siebui-value mceField']/span[@aria-label='Multiple Selection Field']";
		public static final String AddOrderSubType="@xpath=//button[@title='Service Order Sub Type:New']";
		public static final String OrderSubTypeDropDown="@xpath=//td[@id='1_s_5_l_COLT_Order_Item_Sub_Type']/span";
		public static final String SelectNew="@xpath=//li[text()='New']";
		public static final String InputOrderSubType="@xpath=//input[@name='COLT_Order_Item_Sub_Type']";
		public static final String SubmitSubOrderType="@xpath=//button[@title='Service Order Sub Type:OK']";
		public static final String ContractSearch="@xpath=//table[@class='GridBack']//tr[11]/td[2]/div/span[@aria-label='Selection Field']";
		public static final String SelectContract="@xpath=//table[@summary='Pick Contract']/tbody/tr[2]";
		public static final String SubmitContract="@xpath=//button[@title='Pick Contract:OK']";
		public static final String NetworkReferenceSearch="@xpath=//table[@class='GridBack']//tr[11]/td[4]/div/span";
		public static final String AddNetworkReferenceSearch="@xpath=//button[@title='Pick Network Reference:New']";
		public static final String SelectNetworkReference="@xpath=//table[@summary='Pick Network Reference']/tbody/tr[2]";
		public static final String SubmitNetworkReference="@xpath=//button[@title='Pick Network Reference:OK']";
		public static final String SearchNetworkReference="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Network Reference:Go']";
		public static final String InputNetworkReference="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";

		public static final String TextNetworkReference="@xpath=//table[@summary='Pick Network Reference']//tr[2]/td[2]";
		public static final String OrderDates="@xpath=//a[text()='Order Dates']";
		public static final String OrderSignedDate="@xpath=//input[@aria-label='Order Signed Date']";
		public static final String ColtActualDate="@xpath=//input[@aria-labelledby='COLT_Actual_RFS_Date_Label']";
		public static final String OrderReceivedDate="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']";
		public static final String Billing="@xpath=//a[text()='Billing']";
		public static final String ExpandAllButton="@xpath=//button[@title='Service Charges:Expand All']";
		public static final String InstallationChargeNRC="@xpath=//table[@summary='Service Charges']/tbody/tr[3]/td[5]";
		public static final String InputinstallationChargeNRC="@xpath=//table[@summary='Service Charges']/tbody/tr[3]/td[5]/input";
		public static final String RecurringChargeMRC="@xpath=//table[@summary='Service Charges']/tbody/tr[5]/td[5]";
		public static final String InputRecurringChargeMRC="@xpath=//table[@summary='Service Charges']/tbody/tr[5]/td[5]/input";
		public static final String BCNInstallationChargeNRC="@xpath=//table[@summary='Service Charges']/tbody/tr[3]/td[6]";
		public static final String BCNInstallationChargeNRCSearch="@xpath=//table[@summary='Service Charges']/tbody/tr[3]/td[6]/span";
		public static final String BCNInstallationChargeNRCInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String BCNNRCSubmit="@xpath=//td[@class='siebui-popup-filter']//button[@title='Billing Profile:Go']";
		
		public static final String BCNRecurringChargeMRC="@xpath=//table[@summary='Service Charges']/tbody/tr[5]/td[6]";
		public static final String BCNRecurringChargeMRCSearch="@xpath=//table[@summary='Service Charges']/tbody/tr[5]/td[6]/span";
		public static final String BCNRecurringChargeMRCInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String ContractTerm="@xpath=//input[@aria-label='Contract Term']";
		public static final String SelectContractTerm="@xpath=//li[text()='12']";
		public static final String ContractRenewalFlag="@xpath=//input[@aria-label='Contract Renewal Flag']";
		public static final String SelectContractRenewalFlag="@xpath=//ul[@role='combobox']/li[text()='No']";
		public static final String BillCustomer="@xpath=//input[@aria-labelledby='COLT_Customer_Reference/_PO_#_Label']";
		public static final String Offnet="@xpath=//a[text()='Offnet']";
		public static final String CeosReferenceNum="@xpath=//td[contains(@title,'CEOS')]";
		public static final String OrderStatus="@xpath=//input[@aria-label='Order Status']";
		public static final String OrderStatusDropdown="@xpath=//table[@class='GridBack']/tbody/tr[4]/td[3]/div/span";
		public static final String SelectTechnicalValidation="@xpath=//li[text()='Technical Validation']";
		public static final String SelectDeliveryValidation="@xpath=//li[text()='Delivery']";
		public static final String SelectCommercialValidation="@xpath=//ul[@role='combobox']/li[text()='Commercial Validation']";
		public static final String SelectCompleted="@xpath=//li[text()='Completed']";
		public static final String BDWREVWCheckbox="@xpath=";
		public static final String TriggerTRButton="@xpath=//button[text()='Trigger TR']";
		public static final String ExistingCapacityLeadTimePrimary="@xpath=//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']";
		public static final String OSSPlatformFlag="@xpath=//select[@name='COLT OSS Platform Flag']";
		public static final String PrimaryTestCompleted="@xpath=";
		public static final String ExistingCapacity="@xpath=";
		public static final String SubmitTechnicalOrder="@xpath=//button[text()='SubmitTechnicalOrder']";
		public static final String CircuitReference="@xpath=//input[@name='Circuit Reference']";
		public static final String TechnicalOrderTab="@xpath=//a[text()='Technical Orders']";
		public static final String TechnicalOrderStatus="@xpath=//td[@id='1_s_5_l_COLT_Technical_Order_Status']";
		public static final String TechnicalOrderStatus1="@xpath=//td[@title='Completed' and @id='1_s_5_l_COLT_Technical_Order_Status']";
		public static final String DropDown="@xpath=//*[@aria-label='Third Level View Bar']";
		public static final String InstallationAndTestTab="@xpath=//a[text()='Installation and Test']";
		public static final String PrimaryTestingMethod="@xpath=//input[@aria-label='Primary Testing Method']";
		public static final String OrderComplete="@xpath=//button[text()='Yes']";
		public static final String VLANTaggingMode="@xpath=//a[contains(@name,'VLAN Tagging Mode')]/following-sibling::*//select";
		public static final String WarningOk="@xpath=//span[text()='Ok']";
		public static final String CopyOrder="@xpath=//span[text()='Copy Order']";
		public static final String ManualValidation="@xpath=//span[text()='Manual Validation']";
		//Siebel
		public static final String NetworkReference="@xpath=//input[@aria-labelledby='COLT_Network_Reference_Label']";
		public static final String ServiceBandwidth="@xpath=//a[contains(@name,'Service Bandwidth')]//following-sibling::*//select";
		//public static final String ThirdPartyAccessProvider="@xpath=//a[contains(@name,'Third Party Access Provider')]//following-sibling::*//select";
		//public static final String ThirdPartyConnecRef="@xpath=//a[contains(@name,'3rd Party Connection Reference')]//following-sibling::*//input";
		//public static final String ThirdPartySlaTier="@xpath=//a[contains(@name,'Third Party SLA Tier')]//following-sibling::*//select";
		// Ipvpns
		public static final String ColtGoToNextSet="@xpath=//div[@id='coltGoToNextSet']";
		public static final String Addipvpnservice="@xpath=//div[text()='IP VPN Service']/following-sibling::*";
		public static final String Networktopologys="@xpath=//span[text()='Network Topology']/following-sibling::input";
		
		public static final String ButtonbrowserArrow="@xpath=//i[@role='button']";
	//	public static final String SubVPNPlink="@xpath=//span[text()='Sub VPN']/parent::*";
		//added Shivananda Rai
		public static final String Submenuinventory="@xpath=//span[text()='Inventory']/parent::*";
		public static final String COLTInventoryProject="@xpath=//*[text()='COLT Inventory Project']/parent::*";
		public static final String Circuits="@xpath=//*[text()='Circuits']/parent::*";
		public static final String EthernetCircuitFolder="@xpath=//span[text()='Ethernet Circuit Folder']/parent::a";
		public static final String NewCircuit="@xpath=//a[text()='New Circuit']";

		public static final String Circuitname="@xpath=(//input[@name='xname'][@type='text'])";
		public static final String comboarrowtype="@xpath=//table[@id='type_label0']";
		public static final String comboarrowtypeplus="@xpath=//img[@id='type_nodeIcon6']";
		public static final String EthernetLink="@xpath=(//a[text()='Ethernet Link'][@title='Ethernet Link'])";
	//	public static final String BandwidthEtherlink="@xpath=//td[text()='Bandwidth']/following-sibling::*//input";
		public static final String Createbutton="@xpath=//a[text()='Create']";
		public static final String Parameterstab="@xpath=//a[text()='Parameters']";
	//	public static final String Edit="@xpath=//a[text()='Edit']";

		public static final String LogicalStatus1="@xpath=//td[text()='Logical Status']/following-sibling::*//select";
		public static final String LogicalStatus="@xpath=//td/a[text()='Logical Status']/parent::*/following-sibling::*//select";

		public static final String Parentovcname="@xpath=//td/a[text()='Parent OVC Name']/parent::*/following-sibling::*//input";
		public static final String Overbookingfactor="@xpath=//td/a[text()='Overbooking Factor, %']/parent::*/following-sibling::*//input";

	//	public static final String Update="@xpath=//a[text()='Update']";
		public static final String EthernetLinkPathElements="@xpath=//a[text()='Ethernet Link Path Elements']";
		public static final String AddEthernetEndPoint="@xpath=//a[text()='Add Ethernet End Point']";
		public static final String Carrier="@xpath=(//td[text()='Carrier']/following-sibling::*//input)[3]";

		public static final String SelectResource="@xpath=//a[text()='Select Resource(s)']";
		public static final String SelectResourcetext="@xpath=(//a[text()='Select Resource(s)']/following::input)[3]";
		public static final String SelectResourcebutton="@xpath=(//input[@value='Select'])[2]";
		
		
		//Duplicated Xpath
		//public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
		//public static final String CabinetType="@xpath=//td/a[text()='Cabinet Type']/parent::*/following-sibling::*//select";
		//public static final String VlanTagId="@xpath=//td/a[text()='VLAN Tag ID']/parent::*/following-sibling::*//input";
		//public static final String NetworkReference="@xpath=//td/a[text()='Network Reference']/parent::*/following-sibling::*//input";
		//public static final String SearchOrderNo="@xpath=//div[@class='AppletButtons siebui-applet-buttons']//button[@title='=Service Order List:Go']";
		//public static final String BCNNRCSubmit="@xpath=//button[@title='Billing Profile:OK']";
		

	 }
	
	
	
	
}