package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.APT_HSS_Obj;
import pageObjects.aptObjects.APT_IPTransitObj;

public class APT_HSS extends SeleniumUtils {
	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();
	Lanlink_NewTab NewTab = new Lanlink_NewTab();
	SoftAssert sa = new SoftAssert();

	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		// ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer
		// Creation Functionality");

		verifyExists(APT_HSS_Obj.APT_HSS.ManageCustomerServiceLink, " Manage Customer Service Link");
		click(APT_HSS_Obj.APT_HSS.ManageCustomerServiceLink,"ManageCustomerServiceLink");

		verifyExists(APT_HSS_Obj.APT_HSS.createcustomerlink, " create customer link");
		click(APT_HSS_Obj.APT_HSS.createcustomerlink, "Create Customer Link");

		verifyExists(APT_HSS_Obj.APT_HSS.createcustomer_header, "create customer page header");

		// scrolltoend();
		verifyExists(APT_HSS_Obj.APT_HSS.okbutton, "ok");
		click(APT_HSS_Obj.APT_HSS.okbutton, "ok");

		// Warning msg check
		verifyExists(APT_HSS_Obj.APT_HSS.customernamewarngmsg, "Legal Customer Name");
		verifyExists(APT_HSS_Obj.APT_HSS.countrywarngmsg, "Country");
		verifyExists(APT_HSS_Obj.APT_HSS.ocnwarngmsg, "OCN");
		verifyExists(APT_HSS_Obj.APT_HSS.typewarngmsg, "Type");
		verifyExists(APT_HSS_Obj.APT_HSS.emailwarngmsg, "Email");

		// Create customer by providing all info

		verifyExists(APT_HSS_Obj.APT_HSS.nametextfield, "name");
		sendKeys(APT_HSS_Obj.APT_HSS.nametextfield, name, "name");

		verifyExists(APT_HSS_Obj.APT_HSS.maindomaintextfield, "Main Domain");
		sendKeys(APT_HSS_Obj.APT_HSS.maindomaintextfield, maindomain, "Main Domain");

		verifyExists(APT_HSS_Obj.APT_HSS.country, "country");
		// sendKeys(APT_HSS_Obj.APT_HSS.country,country,"country");
		addDropdownValues_commonMethod("Country", APT_HSS_Obj.APT_HSS.country, country);

		verifyExists(APT_HSS_Obj.APT_HSS.ocntextfield, "ocn");
		sendKeys(APT_HSS_Obj.APT_HSS.ocntextfield, ocn, "ocn");

		verifyExists(APT_HSS_Obj.APT_HSS.referencetextfield, "reference");
		sendKeys(APT_HSS_Obj.APT_HSS.referencetextfield, reference, "reference");

		verifyExists(APT_HSS_Obj.APT_HSS.technicalcontactnametextfield, "technical contact name");
		sendKeys(APT_HSS_Obj.APT_HSS.technicalcontactnametextfield, technicalcontactname, "technical contact name");

		verifyExists(APT_HSS_Obj.APT_HSS.typedropdown, "technical contact name");
		// sendKeys(APT_HSS_Obj.APT_HSS.typedropdown,type,"technical contact
		// name");
		addDropdownValues_commonMethod("Type", APT_HSS_Obj.APT_HSS.type, type);

		verifyExists(APT_HSS_Obj.APT_HSS.emailtextfield, "email");
		sendKeys(APT_HSS_Obj.APT_HSS.emailtextfield, email, "email");

		verifyExists(APT_HSS_Obj.APT_HSS.phonetextfield, "phone text field");
		sendKeys(APT_HSS_Obj.APT_HSS.phonetextfield, phone, "phone text field");

		verifyExists(APT_HSS_Obj.APT_HSS.faxtextfield, "fax text field");
		sendKeys(APT_HSS_Obj.APT_HSS.faxtextfield, fax, "fax text field");

		verifyExists(APT_HSS_Obj.APT_HSS.okbutton, "ok");
		click(APT_HSS_Obj.APT_HSS.okbutton, "ok");

		verifyExists(APT_HSS_Obj.APT_HSS.customercreationsuccessmsg, "Customer successfully created.");

	}

	public void selectCustomertocreateOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

		mouseMoveOn(APT_HSS_Obj.APT_HSS.ManageCustomerServiceLink);

		verifyExists(APT_HSS_Obj.APT_HSS.CreateOrderServiceLink, "Create Order Service Link");
		click(APT_HSS_Obj.APT_HSS.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(APT_HSS_Obj.APT_HSS.nextbutton,"Next button");
		// click(APT_HSS_Obj.APT_HSS.nextbutton,"Next button");

		verifyExists(APT_HSS_Obj.APT_HSS.entercustomernamefield, "name");
		sendKeys(APT_HSS_Obj.APT_HSS.entercustomernamefield, name, "name");

		// verifyExists(APT_HSS_Obj.APT_HSS.entercustomernamefield,"name");
		// sendKeys(APT_HSS_Obj.APT_HSS.entercustomernamefield,"*","name");

		waitForAjax();

		addDropdownValues_commonMethod("Choose a customer", APT_HSS_Obj.APT_HSS.chooseCustomerdropdown, name);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(APT_HSS_Obj.APT_HSS.nextbutton, "Next button");
		click(APT_HSS_Obj.APT_HSS.nextbutton, "Next button");

	}

	public void createorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderService");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		ScrollIntoViewByString(APT_HSS_Obj.APT_HSS.nextbutton);

		verifyExists(APT_HSS_Obj.APT_HSS.nextbutton, "Next button");
		click(APT_HSS_Obj.APT_HSS.nextbutton, "Next button");

		verifyExists(APT_HSS_Obj.APT_HSS.order_contractnumber_warngmsg, "order contract number");
		verifyExists(APT_HSS_Obj.APT_HSS.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("YES")) {
			verifyExists(APT_HSS_Obj.APT_HSS.newordertextfield, "new order textfield");
			sendKeys(APT_HSS_Obj.APT_HSS.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(APT_HSS_Obj.APT_HSS.newrfireqtextfield, "new rfireq textfield");
			sendKeys(APT_HSS_Obj.APT_HSS.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(APT_HSS_Obj.APT_HSS.createorderbutton, "create order button");
			click(APT_HSS_Obj.APT_HSS.createorderbutton, "create order button");
			
			
			verifyExists(APT_HSS_Obj.APT_HSS.OrderCreatedSuccessMsg, "Order Created Success Msg");
			waitForAjax();

		} else if (existingorderservice.equalsIgnoreCase("YES")) {
			verifyExists(APT_HSS_Obj.APT_HSS.selectorderswitch, "select order switch");
			click(APT_HSS_Obj.APT_HSS.selectorderswitch, "select order switch");

			verifyExists(APT_HSS_Obj.APT_HSS.existingorderdropdown, "existing order drop down");
			select(APT_HSS_Obj.APT_HSS.existingorderdropdown, existingordernumber);
		} else {
			Reporter.log("Order not selected");
		}

	}

	public void verifyselectservicetype(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException

	{
		String CreateOrderServiceHeader = "Create Order / Service";
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		// String
		// actualValue=getTextFrom(APT_HSS_Obj.APT_HSS.service);

		// select service type
		ScrollIntoViewByString(APT_HSS_Obj.APT_HSS.servicetypetextfield);

		// verifyExists(APT_HSS_Obj.APT_HSS.nextbutton,"Next button");
		// click(APT_HSS_Obj.APT_HSS.nextbutton,"Next button");

		// verifyExists(APT_HSS_Obj.APT_HSS.ServiceTypeWarningMessage,"Service
		// Type Warning Message");

		verifyExists(APT_HSS_Obj.APT_HSS.servicetypetextfield, "service type textfield");
		addDropdownValues_commonMethod("Service Type", APT_HSS_Obj.APT_HSS.servicetypetextfield, servicetype);

		verifyExists(APT_HSS_Obj.APT_HSS.nextbutton, "Next button");
		click(APT_HSS_Obj.APT_HSS.nextbutton, "Next button");

	}

	public void verifyServiceCreation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceIdvalue");
		String BillingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BillingTypevalue");
		String TerminationDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TerminationDate");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EmailValue");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PhoneContactValue");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RemarkValue");
		/*
		 * String ManageService = DataMiner.fngetcolvalue(testDataFile,
		 * sheetName, scriptNo, dataSetNo, "ManageService"); String
		 * RouterConfigurationViewIPv4 = DataMiner.fngetcolvalue(testDataFile,
		 * sheetName, scriptNo, dataSetNo, "RouterConfigurationViewIPv4");
		 * String RouterConfigurationViewIPv6 =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "RouterConfigurationViewIPv6"); String PerformanceReporting =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "PerformanceReporting"); String IPGuardian =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "IPGuardian"); String SNMPNotification =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "SNMPNotification"); String DeliveryChannel =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "DeliveryChannel"); String RouterBasedFirewall =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "RouterBasedFirewall"); String TrapTargetAddress =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "TrapTargetAddress"); String Qos =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "Qos"); String ExtendPErangetocustomerLAN =
		 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
		 * "ExtendPErangetocustomerLAN");
		 */
		String PerformanceReporting_Checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerformanceReporting_Checkbox");
		String WaveService_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"WaveService_checkbox");
		String InterfaceSpeedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceSpeedValue");
		String TypeOfServiceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TypeOfServiceValue");

		verifyExists(APT_HSS_Obj.APT_HSS.serviceidentificationtextfield, "Service Identification");
		sendKeys(APT_HSS_Obj.APT_HSS.serviceidentificationtextfield, ServiceIdentification, "Service Identification");

		verifyExists(APT_HSS_Obj.APT_HSS.emailtextfield_Service, "Email Service");
		sendKeys(APT_HSS_Obj.APT_HSS.emailtextfield_Service, EmailService, "Email Service");
		click(APT_HSS_Obj.APT_HSS.emailarrow, "Email arrow");

		verifyExists(APT_HSS_Obj.APT_HSS.phonecontacttextfield, "Phone Service");
		sendKeys(APT_HSS_Obj.APT_HSS.phonecontacttextfield, PhoneService, "Phone Service");
		click(APT_HSS_Obj.APT_HSS.phonearrow, "Phone arrow");

		verifyExists(APT_HSS_Obj.APT_HSS.remarktextarea, "Remarks");
		sendKeys(APT_HSS_Obj.APT_HSS.remarktextarea, Remarks, "Remarks");

		click(APT_HSS_Obj.APT_HSS.forwardarrow_1, ">>");

		scrollDown(APT_HSS_Obj.APT_HSS.managementoptions_header);
		waitForAjax();
		addCheckbox_commonMethod(APT_HSS_Obj.APT_HSS.performanceReportingcheckbox, "Performance Reporting",
				PerformanceReporting_Checkbox);

		addCheckbox_commonMethod(APT_HSS_Obj.APT_HSS.waveServiceCheckbox, "Wave Service", WaveService_checkbox);

		if (WaveService_checkbox.equalsIgnoreCase("Yes")) {
			verifyExists(APT_HSS_Obj.APT_HSS.InterfaceSpeedDropdown, "Interface Speed");
			addDropdownValues_commonMethod("Interface Speed", APT_HSS_Obj.APT_HSS.InterfaceSpeedDropdown,
					InterfaceSpeedValue);
			verifyExists(APT_HSS_Obj.APT_HSS.TypeofServiceDropdown, "Type Of Service");
			addDropdownValues_commonMethod("Type Of Service", APT_HSS_Obj.APT_HSS.TypeofServiceDropdown,
					TypeOfServiceValue);
		}

		scrollDown(APT_HSS_Obj.APT_HSS.OKbutton);
		click(APT_HSS_Obj.APT_HSS.OKbutton, "OK");
		waitforPagetobeenable();
		NewTab.verifysuccessmessage("Service successfully created.");

	}

	public void verifyCustomerDetailsInformation(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"newCustomerCreation");
		String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomerSelection");
		String newCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
		String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");

		if (newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
			verifyExists(APT_HSS_Obj.APT_HSS.Name_Value, "Name Value");
			String nameValue = getTextFrom(APT_HSS_Obj.APT_HSS.Name_Value, "Name Value");
			nameValue.equalsIgnoreCase(newCustomer);

			verifyExists(APT_HSS_Obj.APT_HSS.Country_Value, "Country");
			String countryValue = getTextFrom(APT_HSS_Obj.APT_HSS.Country_Value, "Country");
			countryValue.equalsIgnoreCase(country);

			verifyExists(APT_HSS_Obj.APT_HSS.OCN_Value, "OCN");
			String OCN = getTextFrom(APT_HSS_Obj.APT_HSS.OCN_Value, "OCN");
			OCN.equalsIgnoreCase(ocn);

			verifyExists(APT_HSS_Obj.APT_HSS.Reference_Value, "Reference");
			String Reference = getTextFrom(APT_HSS_Obj.APT_HSS.Reference_Value, "Reference");
			Reference.equalsIgnoreCase(reference);

			verifyExists(APT_HSS_Obj.APT_HSS.TechnicalContactName_Value, "Technical Contact Name");
			String technicalContactName = getTextFrom(APT_HSS_Obj.APT_HSS.TechnicalContactName_Value, "Technical Contact Name");
			technicalContactName.equalsIgnoreCase(technicalcontactname);

			verifyExists(APT_HSS_Obj.APT_HSS.Type_Value, "Type");
			String Type = getTextFrom(APT_HSS_Obj.APT_HSS.Type_Value, "Type");
			Type.equalsIgnoreCase(type);

			verifyExists(APT_HSS_Obj.APT_HSS.Email_Value, "Email");
			String Email = getTextFrom(APT_HSS_Obj.APT_HSS.Email_Value, "Email");
			Email.equalsIgnoreCase(email);

			verifyExists(APT_HSS_Obj.APT_HSS.Phone_Value, "Phone");
			String Phone = getTextFrom(APT_HSS_Obj.APT_HSS.Phone_Value, "Phone");
			Phone.equalsIgnoreCase(phone);

			verifyExists(APT_HSS_Obj.APT_HSS.Fax_Value, "Fax");
			String Fax = getTextFrom(APT_HSS_Obj.APT_HSS.Fax_Value, "Fax");
			Fax.equalsIgnoreCase(fax);
		} else if (newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {
			verifyExists(APT_HSS_Obj.APT_HSS.Name_Value, "Fax");
			String ExistingCustomer = getTextFrom(APT_HSS_Obj.APT_HSS.Name_Value, "Fax");
			ExistingCustomer.equalsIgnoreCase(existingCustomer);
		}
		// Main Domain
		if (maindomain.equalsIgnoreCase("Null")) {
			Reporter.log("A default displays for main domain field, if no provided while creating customer");

		} else {
			verifyExists(APT_HSS_Obj.APT_HSS.MainDomain_Value, "Main Domain");
			String Maindomain = getTextFrom(APT_HSS_Obj.APT_HSS.MainDomain_Value, "Main Domain");
			Maindomain.equalsIgnoreCase(maindomain);
		}
		Reporter.log("Customer Details panel fields Verified");
	}

	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException

	{
		// String TerminationDate =
		// getTextFrom(APT_HSS_Obj.APT_HSS.managesubnet_header, "Termination
		// Date");
		// String EmailValue =
		// getTextFrom(APT_HSS_Obj.APT_HSS.managesubnet_successmsg, "Email");
		String ServicepanelHeader = getTextFrom(APT_HSS_Obj.APT_HSS.servicepanel_header, "Service panel Header");
		String ServiceIdentification = getTextFrom(APT_HSS_Obj.APT_HSS.servicepanel_serviceidentificationvalue,
				"Service Identification");
		String ServiceType = getTextFrom(APT_HSS_Obj.APT_HSS.servicepanel_servicetypevalue, "Service Type");
		String PhoneContact = getTextFrom(APT_HSS_Obj.APT_HSS.servicepanel_phone, "Phone Contact");
		String Remarks = getTextFrom(APT_HSS_Obj.APT_HSS.servicepanel_remarksvalue, "Remarks");

		String ServicepanelHeaderValue = getTextFrom(APT_HSS_Obj.APT_HSS.servicepanel_header, "Service panel Header");
		String ServiceIdentificationValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceIdvalue");
		String ServiceTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String PhoneContactValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PhoneContactValue");
		String BillingTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Billing Type");
		String RemarksValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RemarkValue");

		scrollDown(APT_HSS_Obj.APT_HSS.orderpanelheader);

		ServicepanelHeader.equals(ServicepanelHeaderValue);
		ServiceIdentification.equals(ServiceIdentificationValue);
		ServiceType.equals(ServiceTypeValue);
		PhoneContact.equals(PhoneContactValue);
		// BillingType.equals(BillingTypeValue);
		Remarks.equals(RemarksValue);

	}

	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_VoicelineNumber");

		if (editOrderSelection.equalsIgnoreCase("no")) {
			Reporter.log("Edit Order is not performed");
		} else if (editOrderSelection.equalsIgnoreCase("Yes")) {
			Reporter.log("Performing Edit Order Functionality");

			verifyExists(APT_HSS_Obj.APT_HSS.orderactionbutton, "Action dropdown");
			click(APT_HSS_Obj.APT_HSS.orderactionbutton, "Action dropdown");

			verifyExists(APT_HSS_Obj.APT_HSS.editorderlink, "Edit Order");
			click(APT_HSS_Obj.APT_HSS.editorderlink, "Edit Order");

			verifyExists(APT_HSS_Obj.APT_HSS.editorderheader, "Edit Order");

			String EditOrderNo = getTextFrom(APT_HSS_Obj.APT_HSS.editorderno, "Edit Order No");
			click(APT_HSS_Obj.APT_HSS.editorderno, "edit order no");
			clearTextBox(APT_HSS_Obj.APT_HSS.editorderno);

			verifyExists(APT_HSS_Obj.APT_HSS.editorderno, "Order Number");
			sendKeys(APT_HSS_Obj.APT_HSS.editorderno, editorderno, "Order Number");

			verifyExists(APT_HSS_Obj.APT_HSS.editvoicelineno, "edit voice line no");
			clearTextBox(APT_HSS_Obj.APT_HSS.editvoicelineno);
			sendKeys(APT_HSS_Obj.APT_HSS.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APT_HSS_Obj.APT_HSS.cancelbutton, "cancel button");
			click(APT_HSS_Obj.APT_HSS.cancelbutton, "cancel button");

			verifyExists(APT_HSS_Obj.APT_HSS.orderactionbutton, "Action dropdown");
			click(APT_HSS_Obj.APT_HSS.orderactionbutton, "Action dropdown");

			verifyExists(APT_HSS_Obj.APT_HSS.editorderlink, "Edit Order");
			click(APT_HSS_Obj.APT_HSS.editorderlink, "Edit Order");

			verifyExists(APT_HSS_Obj.APT_HSS.editorderheader, "Edit Order Header");
			String editOrderHeader = getTextFrom(APT_HSS_Obj.APT_HSS.editorderheader, "Edit Order Header");
			String EditOrder = "Edit Order";
			editOrderHeader.equalsIgnoreCase(EditOrder);

			verifyExists(APT_HSS_Obj.APT_HSS.editorderno, "edit order no");
			click(APT_HSS_Obj.APT_HSS.editorderno, "edit order no");
			clearTextBox(APT_HSS_Obj.APT_HSS.editorderno);
			sendKeys(APT_HSS_Obj.APT_HSS.editorderno, editorderno, "Order Number");

			verifyExists(APT_HSS_Obj.APT_HSS.editvoicelineno, "edit voice line no");
			clearTextBox(APT_HSS_Obj.APT_HSS.editvoicelineno);
			sendKeys(APT_HSS_Obj.APT_HSS.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APT_HSS_Obj.APT_HSS.editorder_okbutton, "OK Button");
			click(APT_HSS_Obj.APT_HSS.editorder_okbutton, "OK Button");

			if (editorderno.equalsIgnoreCase("Null")) {
				Reporter.log("Order/Contract Number (Parent SID) field is not edited");
			} else {
				verifyExists(APT_HSS_Obj.APT_HSS.ordernumbervalue, "order number value");
				String editOrderno = getTextFrom(APT_HSS_Obj.APT_HSS.ordernumbervalue,"order number value");
				editOrderno.equalsIgnoreCase(editorderno);
			}

			if (editvoicelineno.equalsIgnoreCase("Null")) {
				Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
			} else {
				verifyExists(APT_HSS_Obj.APT_HSS.ordervoicelinenumbervalue, "order voice line number value");
				String editVoicelineno = getTextFrom(APT_HSS_Obj.APT_HSS.ordervoicelinenumbervalue, "order voice line number value");
				editVoicelineno.equalsIgnoreCase(editvoicelineno);
			}
			Reporter.log("Edit Order is successful");

		}

	}

	public void verifyEditservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		// Cancel edit service
		waitForAjax();
		scrollDown(APT_HSS_Obj.APT_HSS.orderpanelheader);
		click(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "Action dropdown");
		click(APT_HSS_Obj.APT_HSS.edit, "Edit");
		waitForAjax();
		waitToPageLoad();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(APT_HSS_Obj.APT_HSS.cancelbutton1, "Cancel");
		waitForAjax();
		waitToPageLoad();
		scrollDown(APT_HSS_Obj.APT_HSS.orderpanelheader);
		if (isElementPresent(APT_HSS_Obj.APT_HSS.servicepanel_header)) {
			Reporter.log("Navigated to view service page");
		} else
			// Didn't navigate to view service page
			Reporter.log("Didn't navigate to view service page");

		// Edit service
		click(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "Action dropdown");
		click(APT_HSS_Obj.APT_HSS.edit, "Edit");
		waitForAjax();
		waitToPageLoad();
		scrollIntoTop();
		// sendKeys(application, "Service Identification",
		// "serviceidentificationtextfield", sid, xml);
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// sendKeys(application, "Termination Date",
		// "terminationdate_field", editterminationdate, xml);
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// selectValueInsideDropdown(application, "billingtype_dropdown",
		// "Billing Type", editbillingtypevalue, xml);
		// selectValueInsideDropdown(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// sendKeys(application, "Remarks", "remarktextarea",
		// EditRemarks, xml);
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);

		scrollDown(APT_HSS_Obj.APT_HSS.remarktextarea);
		// Edit email
		click(APT_HSS_Obj.APT_HSS.selectedemail, "Selected Email");
		click(APT_HSS_Obj.APT_HSS.emailremovearrow, "Email remove arrow");
		// sendKeys(application, , "emailtextfieldvalue",
		// editemail,"Email");
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);

		click(APT_HSS_Obj.APT_HSS.emailaddarrow, "Email adding arrow");
		// Edit phone contact
		scrollDown(APT_HSS_Obj.APT_HSS.remarktextarea);
		click(APT_HSS_Obj.APT_HSS.selectedphone, "Selected phone contact");
		click(APT_HSS_Obj.APT_HSS.phoneremovearrow, "Phonecontact remove arrow");
		// sendKeys(application, ,
		// "phonecontacttextfieldvalue", editphonecontact,"Phone Contact");
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);

		click(APT_HSS_Obj.APT_HSS.phoneaddarrow, "phonecontact adding Arrow");

		// management options panel
		scrollDown(APT_HSS_Obj.APT_HSS.managementoptions_header);
		// editcheckbox_commonMethod(application,
		// edit_performancereporting_checkbox, "performancereporting_checkbox",
		// "Performance Reporting", xml);
		// editcheckbox_commonMethod(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// editcheckbox_commonMethod(application, edit_ipguardian_checkbox,
		// "ipguardian_checkbox", "IP Guardian", xml);
		// editcheckbox_commonMethod(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		waitForAjax();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(APT_HSS_Obj.APT_HSS.editservice_okbutton, "OK");
		waitForAjax();
		waitToPageLoad();

		if (isElementPresent(APT_HSS_Obj.APT_HSS.customerdetailsheader)) {
			Reporter.log("Navigated to view service page");
			// Outband.verifysuccessmessage("Service updated successfully");
		} else {
			Reporter.log("Service not updated");
		}

	}

	public void verifyDump() throws IOException {

		// String Serviceheader =
		// getTextFrom(APT_HSS_Obj.APT_HSS.service_header, "Service header");

		// String ServiceheaderValue = "Service";

		scrollDown(APT_HSS_Obj.APT_HSS.orderpanelheader);
		click(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "Action dropdown");
		click(APT_HSS_Obj.APT_HSS.dump_link, "Dump");
		waitForAjax();
		waitToPageLoad();
		// String DumpHeader = getTextFrom(APT_HSS_Obj.APT_HSS.dumppage_header,
		// "Dump header");
		// String ServiceRetrievedTime =
		// getTextFrom(APT_HSS_Obj.APT_HSS.serviceretrieved_text,
		// "Service retrieved time");

		// Serviceheader.equals(ServiceheaderValue);
		getTextFrom(APT_HSS_Obj.APT_HSS.dumppage_text, "Dump page service details");
		waitForAjax();
		click(APT_HSS_Obj.APT_HSS.closesymbol, "Close");
		waitForAjax();
	}

	public void navigateToAddNewDevicepage() throws InterruptedException, IOException {

		scrollDown(APT_HSS_Obj.APT_HSS.deviceheader);
		waitForAjax();
		compareText("Device", APT_HSS_Obj.APT_HSS.deviceheader, "Device");
		click(APT_HSS_Obj.APT_HSS.AddDevice, "Add Device");
		// compareText("Add Device Header",
		// APT_HSS_Obj.APT_HSS.adddevice_header, "Add Device");
		// click(APT_HSS_Obj.APT_HSS.addnewdevice_togglebutton, "Add New Device
		// toggle");
	}

	public void addNewPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceName");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementAddress");
		String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SSH");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmp2C");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmp3");
		String snmpro2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
		String Snmpv3Usernamevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3UsernameNewValue");
		String Snmpv3Authpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3AuthpasswordNewValue");
		String Snmpv3Privpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3PrivpasswordNewValue");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
		String Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityCode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseName");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseCode");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingCityValue");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing PremiseValue");

		try {
			verifyExists(APT_HSS_Obj.APT_HSS.okbutton, "Ok Button");
			click(APT_HSS_Obj.APT_HSS.okbutton, "Ok Button");

			verifyExists(APT_HSS_Obj.APT_HSS.devicename, "Name");
			sendKeys(APT_HSS_Obj.APT_HSS.devicename, name, "Name");

			verifyExists(APT_HSS_Obj.APT_HSS.vendormodelinput, "Vendor/Model");
			addDropdownValues_commonMethod("Vendor/Model", APT_HSS_Obj.APT_HSS.vendormodelinput, vendormodel);

			verifyExists(APT_HSS_Obj.APT_HSS.managementaddresstextbox, "Management Address");
			sendKeys(APT_HSS_Obj.APT_HSS.managementaddresstextbox, managementaddress, "Management Address");

			String SnmproValue = getAttributeFrom(APT_HSS_Obj.APT_HSS.snmprotextfield, "value");
			Reporter.log("Snmpro value is displayed as: " + SnmproValue);

			if (vendormodel.contains("Ciena")) {

				// Connectivity Protocol
				if ((telnet.equalsIgnoreCase("No")) && (ssh.equalsIgnoreCase("No"))) {

					// verifyDefaultSelection_connectivityprotocol_ssh();

					// verifyDefaultSelection_connectivityprotocol_telnet();
				}
			} else {

				// verifyDefaultSelection_connectivityprotocol_ssh();

				// verifyDefaultSelection_connectivityprotocol_telnet();

				if ((telnet.equalsIgnoreCase("Yes")) && (ssh.equalsIgnoreCase("No"))) {
					verifyExists(APT_HSS_Obj.APT_HSS.telnetradiobutton, "Telnet");
					click(APT_HSS_Obj.APT_HSS.telnetradiobutton, telnet);
				} else if ((telnet.equalsIgnoreCase("No")) && (ssh.equalsIgnoreCase("Yes"))) {
					// verifyExists(APT_HSS_Obj.APT_HSS.sshradiobutton, "SSH");
					// click(APT_HSS_Obj.APT_HSS.sshradiobutton, ssh);
				}

				if ((snmp2c.equalsIgnoreCase("Yes")) && (snmp3.equalsIgnoreCase("NO"))) {
					// verifyExists(APT_HSS_Obj.APT_HSS.c2cradiobutton, "SNMP
					// Version-2c");
					// click(APT_HSS_Obj.APT_HSS.c2cradiobutton, snmp2c);

					verifyExists(APT_HSS_Obj.APT_HSS.snmprotextfield, "Snmpro");
					sendKeys(APT_HSS_Obj.APT_HSS.snmprotextfield, snmpro2cvalue);

					// verifyExists(APT_HSS_Obj.APT_HSS.snmprwtextfield,
					// "Snmprw");
					// sendKeys(APT_HSS_Obj.APT_HSS.snmprwtextfield,
					// snmpro2cvalue);

				} else if ((snmp2c.equalsIgnoreCase("no")) && (snmp3.equalsIgnoreCase("yes"))) {
					verifyExists(APT_HSS_Obj.APT_HSS.c3radiobutton, "SNMP Version-3");
					click(APT_HSS_Obj.APT_HSS.c3radiobutton, snmp3);

					verifyExists(APT_HSS_Obj.APT_HSS.snmpv3username, "Snmpro");
					sendKeys(APT_HSS_Obj.APT_HSS.snmpv3username, Snmpv3Usernamevalue);

					verifyExists(APT_HSS_Obj.APT_HSS.snmpv3authpassword, "Snmp v3 Auth password");
					sendKeys(APT_HSS_Obj.APT_HSS.snmpv3authpassword, Snmpv3Authpasswordvalue);

					verifyExists(APT_HSS_Obj.APT_HSS.snmpv3privpassword, "Snmp v3 Auth password");
					sendKeys(APT_HSS_Obj.APT_HSS.snmpv3privpassword, Snmpv3Privpasswordvalue);
				}

				verifyExists(APT_HSS_Obj.APT_HSS.countryinput, "Country dropdown");
				addDropdownValues_commonMethod("Country", APT_HSS_Obj.APT_HSS.countryinput, Country);
				// selectByVisibleText(APT_HSS_Obj.APT_HSS.countryinput,
				// Country, "Country");
				// select(APT_HSS_Obj.APT_HSS.countryinput,Country);

				if (existingcity.equalsIgnoreCase("no") & newcity.equalsIgnoreCase("yes")) {
					verifyExists(APT_HSS_Obj.APT_HSS.addcityswitch, "city switch");
					click(APT_HSS_Obj.APT_HSS.addcityswitch, "city switch");

					verifyExists(APT_HSS_Obj.APT_HSS.citynameinputfield, "City Name");
					sendKeys(APT_HSS_Obj.APT_HSS.citynameinputfield, cityname, "City Name");

					verifyExists(APT_HSS_Obj.APT_HSS.citycodeinputfield, "City Code");
					sendKeys(APT_HSS_Obj.APT_HSS.citycodeinputfield, Citycode, "City Code");

					verifyExists(APT_HSS_Obj.APT_HSS.sitenameinputfield_addCityToggleSelected, "Site Name");
					sendKeys(APT_HSS_Obj.APT_HSS.sitenameinputfield_addCityToggleSelected, sitename, "Site Name");

					verifyExists(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addCityToggleSelected, "Site Code");
					sendKeys(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addCityToggleSelected, sitecode, "Site Code");

					verifyExists(APT_HSS_Obj.APT_HSS.premisenameinputfield_addCityToggleSelected, "Premise Name");
					sendKeys(APT_HSS_Obj.APT_HSS.premisenameinputfield_addCityToggleSelected, premisename,
							"Premise Name");

					verifyExists(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addCityToggleSelected, "Premise Code");
					sendKeys(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addCityToggleSelected, premisecode,
							"Premise Code");
				} else if (existingcity.equalsIgnoreCase("yes") & newcity.equalsIgnoreCase("no")) {
					verifyExists(APT_HSS_Obj.APT_HSS.citydropdowninput, "City dropdown");
					// addDropdownValues_commonMethod("Country",APT_HSS_Obj.APT_HSS.countryinput,Country);
					addDropdownValues_commonMethod("City", APT_HSS_Obj.APT_HSS.citydropdowninput, existingcityvalue);
					// select(APT_HSS_Obj.APT_HSS.citydropdowninput,existingcityvalue);

					if (existingsite.equalsIgnoreCase("yes") & newsite.equalsIgnoreCase("no")) {
						verifyExists(APT_HSS_Obj.APT_HSS.sitedropdowninput, "Site");
						addDropdownValues_commonMethod("Site", APT_HSS_Obj.APT_HSS.sitedropdowninput,
								existingsitevalue);

						// addDropdownValues_commonMethod("Country",APT_HSS_Obj.APT_HSS.sitedropdowninput,existingsitevalue);
						// select(APT_HSS_Obj.APT_HSS.sitedropdowninput,existingsitevalue);

						if (existingpremise.equalsIgnoreCase("yes") & NewPremise.equalsIgnoreCase("no")) {
							verifyExists(APT_HSS_Obj.APT_HSS.premisedropdowninput, "Premise");
							addDropdownValues_commonMethod("Premise", APT_HSS_Obj.APT_HSS.premisedropdowninput,
									existingpremisevalue);
						}

						else if (existingpremise.equalsIgnoreCase("no") & NewPremise.equalsIgnoreCase("yes")) {
							verifyExists(APT_HSS_Obj.APT_HSS.addpremiseswitch, "premise switch");
							click(APT_HSS_Obj.APT_HSS.addpremiseswitch, "premise switch");

							verifyExists(APT_HSS_Obj.APT_HSS.premisenameinputfield_addPremiseToggleSelected,
									"Premise name");
							sendKeys(APT_HSS_Obj.APT_HSS.premisenameinputfield_addPremiseToggleSelected, premisename,
									"Premise name");

							verifyExists(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addPremiseToggleSelected,
									"Premise Code");
							sendKeys(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addPremiseToggleSelected, premisecode,
									"Premise Code");
						}
					}

					else if (existingsite.equalsIgnoreCase("no") & newsite.equalsIgnoreCase("yes")) {
						verifyExists(APT_HSS_Obj.APT_HSS.addsiteswitch, "site switch");
						click(APT_HSS_Obj.APT_HSS.addsiteswitch, "site switch");

						verifyExists(APT_HSS_Obj.APT_HSS.sitenameinputfield_addSiteToggleSelected, "Premise name");
						sendKeys(APT_HSS_Obj.APT_HSS.sitenameinputfield_addSiteToggleSelected, sitename, "Site Name");

						verifyExists(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addSiteToggleSelected, "Site Code");
						sendKeys(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addSiteToggleSelected, sitecode, "Site Code");

						verifyExists(APT_HSS_Obj.APT_HSS.premisenameinputfield_addSiteToggleSelected, "Premise name");
						sendKeys(APT_HSS_Obj.APT_HSS.premisenameinputfield_addSiteToggleSelected, premisename,
								"Premise name");

						verifyExists(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addSiteToggleSelected, "Premise Code");
						sendKeys(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addSiteToggleSelected, premisecode,
								"Premise Code");
					}
				}
			}
		}

		catch (StaleElementReferenceException e) {

			e.printStackTrace();
		}
		verifyExists(APT_HSS_Obj.APT_HSS.okbutton, "Ok Button");
		click(APT_HSS_Obj.APT_HSS.okbutton, "Ok Button");

	}

	public void verifyViewpage_Devicedetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String nameValue = getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_devicename, "Name");
		// Device Name
		name.equals(nameValue);

		String vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String vendormodelValue = getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_vendormodel, "Vendor/Model");
		// Vendor/model
		vendormodel.equals(vendormodelValue);

		String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Ssh");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmp2c");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmp3");
		String snmpro2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmpro2cvalue");
		String snmprw2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmprw2cvalue");
		String Snmpv3Usernamevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3Usernamevalue");
		String Snmpv3Authpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3Authpasswordvalue");

		String Snmpv3Privpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3Privpasswordvalue");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
		String Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityCode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseName");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseCode");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingCityValue");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing PremiseValue");

		String telnetValue = "telnet";
		String SshValue = "ssh";

		// Connectivity protocol
		if ((telnet.equalsIgnoreCase("Yes")) && (ssh.equalsIgnoreCase("No"))) {
			telnet.equals(telnetValue);
		} else if ((telnet.equalsIgnoreCase("no")) && (ssh.equalsIgnoreCase("Yes"))) {
			ssh.equals(SshValue);
		}

		// SNMP Version
		if ((snmp2c.equalsIgnoreCase("Yes")) && (snmp3.equalsIgnoreCase("No"))) {

			String snmp2cValue = "2c";
			// snmp version
			snmp2c.equals(snmp2cValue);
			// Snmpro
			if (!snmpro2cvalue.equalsIgnoreCase("Null")) {
				(APT_HSS_Obj.APT_HSS.viewpage_snmpro).equals(snmpro2cvalue);
			} else {
				String snmpro2cvalue1 = "incc";
				snmpro2cvalue.equals(snmpro2cvalue1);
			}

			// Snmprw
			if (!snmprw2cvalue.equalsIgnoreCase("Null")) {
				(APT_HSS_Obj.APT_HSS.viewpage_snmprw).equals(snmprw2cvalue);
			} else {
				String Snmprw1 = "ip4corp3";
				snmprw2cvalue.equals(Snmprw1);
			}

		} else if ((snmp2c.equalsIgnoreCase("No")) && (snmp3.equalsIgnoreCase("Yes"))) {

			// Snmp v3 Username
			if (!Snmpv3Usernamevalue.equalsIgnoreCase("Null")) {
				(APT_HSS_Obj.APT_HSS.viewpage_snmpv3username).equals(Snmpv3Usernamevalue);
			} else {
				String Snmpv3Usernamevalue1 = "colt-nms";
				Snmpv3Usernamevalue.equals(Snmpv3Usernamevalue1);
			}

			// Snmp v3 Auth Password
			if (!Snmpv3Authpasswordvalue.equalsIgnoreCase("Null")) {
				(APT_HSS_Obj.APT_HSS.viewpage_snmpv3authpassword).equals(Snmpv3Authpasswordvalue);
			} else {
				String Snmpv3Authpasswordvalue1 = "OrHzjWmRvr4piJZb";
				Snmpv3Authpasswordvalue.equals(Snmpv3Authpasswordvalue1);
			}

			String Snmpv3Privpasswordvalue1 = "3k0hw8thNxHucQkE";
			// Snmp v3 Priv Password
			if (!Snmpv3Privpasswordvalue.equalsIgnoreCase("Null")) {
				(APT_HSS_Obj.APT_HSS.viewpage_snmpv3privpassword).equals(Snmpv3Privpasswordvalue);
			} else {
				Snmpv3Privpasswordvalue.equals(Snmpv3Privpasswordvalue1);
			}
		}

		// Management Address
		getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_managementaddress, "Management Address");

		// Country
		getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_country, "Country");

		// City
		if ((existingcity.equalsIgnoreCase("yes")) && (newcity.equalsIgnoreCase("NO"))) {

			// Existing City
			getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_city, "City");

		} else if ((existingcity.equalsIgnoreCase("NO")) && (newcity.equalsIgnoreCase("Yes"))) {

			// new City
			getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_city, "City");

		}

		// Site
		if ((existingsite.equalsIgnoreCase("yes")) && (newsite.equalsIgnoreCase("NO"))) {

			// Existing Site
			getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_site, "Site");
		}

		else if ((existingsite.equalsIgnoreCase("No")) && (newsite.equalsIgnoreCase("Yes"))) {

			// New Site
			getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_site, "Site");
		}

		// Premise
		if ((existingpremise.equalsIgnoreCase("yes")) && (NewPremise.equalsIgnoreCase("NO"))) {

			// Existing premise
			getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_premise, "Premise");
		}

		else if ((existingpremise.equalsIgnoreCase("No")) && (NewPremise.equalsIgnoreCase("Yes"))) {

			// new premise
			getTextFrom(APT_HSS_Obj.APT_HSS.viewpage_premise, "Premise");
		}
	}

	public void verifyEditDevice_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");

		String editDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editVendorModel");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editManagementAddress");

		String editCountry = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCountry");
		String editExistingCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingCity");
		String editNewCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewCity");
		String editNewCityName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewCityName");
		String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewCityCode");
		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewSiteName");
		String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewSiteCode");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewPremiseName");
		String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewPremiseCode");
		String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingCityValue");
		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingSite");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewSite");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingSiteValue");
		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingPremise");
		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewPremise");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingPremiseValue");

		Reporter.log("Verifying Edit PE Device Functionality");

		scrollDown(APT_HSS_Obj.APT_HSS.existingdevicegrid);

		if (isClickable(APT_HSS_Obj.APT_HSS.existingdevicegrid)) {
			waitForAjax();
			// verifyExists(APT_HSS_Obj.APT_HSS.viewservicepage_editdevicelink,
			// "Edit");
			// click(APT_HSS_Obj.APT_HSS.viewservicepage_editdevicelink,"Edit");
			webDriver.findElement(By.xpath("//td[contains(.,'" + name + "')]//span//a[text()='Edit']")).click();
			// td[contains(.,'"+name+"')]//span//a[text()='Edit']
			waitForAjax();

			verifyExists(APT_HSS_Obj.APT_HSS.editdeviceheader, "City");
			String editPEDevice = getTextFrom(APT_HSS_Obj.APT_HSS.editdeviceheader, "City");
			String EditPEDevice = "Edit PE Device";
			editPEDevice.equalsIgnoreCase(EditPEDevice);

			verifyExists(APT_HSS_Obj.APT_HSS.nametextfield, "Name");
			sendKeys(APT_HSS_Obj.APT_HSS.nametextfield, editDevicename, "Name");

			verifyExists(APT_HSS_Obj.APT_HSS.vendormodelinput, "Vendor/Model");
			select(APT_HSS_Obj.APT_HSS.vendormodelinput, editVendorModel);

			// verifyExists(APT_HSS_Obj.APT_HSS.managementaddresstextbox,"Name");
			// sendKeys(APT_HSS_Obj.APT_HSS.managementaddresstextbox,editManagementAddress,"Name");

			verifyExists(APT_HSS_Obj.APT_HSS.countryinput, "Vendor/Model");
			select(APT_HSS_Obj.APT_HSS.countryinput, editCountry);

			if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
				verifyExists(APT_HSS_Obj.APT_HSS.addcityswitch, "add city switch");
				click(APT_HSS_Obj.APT_HSS.addcityswitch, "add city switch");

				verifyExists(APT_HSS_Obj.APT_HSS.citynameinputfield, "Name");
				sendKeys(APT_HSS_Obj.APT_HSS.citynameinputfield, editNewCityName, "Name");

				verifyExists(APT_HSS_Obj.APT_HSS.citycodeinputfield, "CityCode");
				sendKeys(APT_HSS_Obj.APT_HSS.citycodeinputfield, editNewCityCode, "CityCode");

				verifyExists(APT_HSS_Obj.APT_HSS.sitenameinputfield_addCityToggleSelected, "Site Name");
				sendKeys(APT_HSS_Obj.APT_HSS.sitenameinputfield_addCityToggleSelected, editNewSiteName, "Site Name");

				verifyExists(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addCityToggleSelected, "SiteCode");
				sendKeys(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addCityToggleSelected, editNewSiteCode, "SiteCode");

				verifyExists(APT_HSS_Obj.APT_HSS.premisenameinputfield_addCityToggleSelected, "Premise Name");
				sendKeys(APT_HSS_Obj.APT_HSS.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
						"Premise Name");

				verifyExists(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addCityToggleSelected, "Premise Name");
				sendKeys(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
						"Premise Code");
			}

			else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
				verifyExists(APT_HSS_Obj.APT_HSS.citydropdowninput, "City");
				select(APT_HSS_Obj.APT_HSS.citydropdowninput, editExistingCityValue);

				if (editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
					verifyExists(APT_HSS_Obj.APT_HSS.sitedropdowninput, "Site");
					select(APT_HSS_Obj.APT_HSS.sitedropdowninput, editExistingSiteValue);

					if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
						verifyExists(APT_HSS_Obj.APT_HSS.premisedropdowninput, "City");
						select(APT_HSS_Obj.APT_HSS.premisedropdowninput, editExistingPremiseValue);
					}

					else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {
						verifyExists(APT_HSS_Obj.APT_HSS.addpremiseswitch, "add premise switch");
						click(APT_HSS_Obj.APT_HSS.addpremiseswitch, "add premise switch");

						verifyExists(APT_HSS_Obj.APT_HSS.premisenameinputfield_addPremiseToggleSelected,
								"Premise Name");
						sendKeys(APT_HSS_Obj.APT_HSS.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName,
								"Premise Name");

						verifyExists(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addPremiseToggleSelected,
								"Premise Code");
						sendKeys(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode,
								"Premise Code");
					}
				}

				else if (editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {
					verifyExists(APT_HSS_Obj.APT_HSS.addsiteswitch, "site switch");
					click(APT_HSS_Obj.APT_HSS.addsiteswitch, "site switch");

					verifyExists(APT_HSS_Obj.APT_HSS.sitenameinputfield_addCityToggleSelected, "Site Name");
					sendKeys(APT_HSS_Obj.APT_HSS.sitenameinputfield_addCityToggleSelected, editNewSiteName,
							"Site Name");

					verifyExists(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addCityToggleSelected, "SiteCode");
					sendKeys(APT_HSS_Obj.APT_HSS.sitecodeinputfield_addCityToggleSelected, editNewSiteCode, "SiteCode");

					verifyExists(APT_HSS_Obj.APT_HSS.premisenameinputfield_addCityToggleSelected, "Premise Name");
					sendKeys(APT_HSS_Obj.APT_HSS.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
							"Premise Name");

					verifyExists(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addCityToggleSelected, "Premise Name");
					sendKeys(APT_HSS_Obj.APT_HSS.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
							"Premise Code");
				}

			}
			verifyExists(APT_HSS_Obj.APT_HSS.Routes_OKButton, "Ok Button");
			click(APT_HSS_Obj.APT_HSS.Routes_OKButton, "Ok Button");

		}

		else if (editCountry.equalsIgnoreCase("Null")) {
			Reporter.log("No changes made for 'Country' dropdown");

			CreateFirewall.editCity(editExistingCity, editNewCity, "citydropdowninput", "selectcityswitch",
					"addcityswitch", editExistingCityValue, editNewCityName, editNewCityCode, "City");

			// Site
			CreateFirewall.editSite(editExistingSite, editNewSite, "sitedropdowninput", "selectsiteswitch",
					"addsiteswitch", editExistingSiteValue, editNewSiteName, editNewSiteCode, "Site");

			// Premise
			CreateFirewall.editPremise(editExistingPremise, editNewPremise, "premisedropdowninput",
					"selectpremiseswitch", "addpremiseswitch", editExistingPremiseValue, editNewPremiseName,
					editNewPremiseCode, "Premise");
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void addExistingPEDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingDeviceName");

		verifyExists(APT_HSS_Obj.APT_HSS.providerequipment_header, "provide requipment header");
		String providerequipment = getTextFrom(APT_HSS_Obj.APT_HSS.providerequipment_header, "provide requipment header");
		String provideRequipment = "Provider Equipment (PE)";
		providerequipment.equalsIgnoreCase(provideRequipment);

		verifyExists(APT_HSS_Obj.APT_HSS.addpedevice_link, "addpe device");
		click(APT_HSS_Obj.APT_HSS.addpedevice_link, "addpe device");

		if (isClickable(APT_HSS_Obj.APT_HSS.addpedevice_header)) {
			Reporter.log("Add PE Device' page navigated as expected");

			verifyExists(APT_HSS_Obj.APT_HSS.typepename_dropdown, "typepe name dropdown");
			click(APT_HSS_Obj.APT_HSS.typepename_dropdown, "typepe name dropdown");

			addDropdownValues_commonMethod("Type PE name to filter", APT_HSS_Obj.APT_HSS.typepename_dropdown,
					existingdevicename);
			waitForAjax();
			click(APT_HSS_Obj.APT_HSS.typepename_dropdown, "typepe name dropdown");
			webDriver.findElement(By.xpath("(//div[label[text()='Type PE name to filter']]//span[contains(text(),'"
					+ existingdevicename + "')])[1]")).click();

			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_vendormodelvalue, "existing device vendor model value");
			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_managementaddressvalue,
					"existing device management addressvalue");
			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_connectivityprotocol,
					"existing device connectivi typrotocol");

			String SNMPVersionValue = APT_HSS_Obj.APT_HSS.existingdevice_snmpversion;

			if (SNMPVersionValue.equalsIgnoreCase("2c")) {
				verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_snmpro, "existing device snmpro");
				verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_snmprw, "existing device snmprw");
			}

			else if (SNMPVersionValue.equalsIgnoreCase("3")) {
				verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_snmpv3username, "existing device snmp v3 username");
				verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_snmpv3authpassword,
						"existing device snmp v3 auth password");
				verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_snmpv3privpassword,
						"existingdevice snmp v3 priv password");

			} else {
				verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_snmpversion, "existing device snmp version");

			}
			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_country, "Country");
			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_city, "City");
			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_site, "Site");
			verifyExists(APT_HSS_Obj.APT_HSS.existingdevice_premise, "Premise");

			verifyExists(APT_HSS_Obj.APT_HSS.Next_Button, "Next");
			click(APT_HSS_Obj.APT_HSS.Next_Button, "Next");

			verifyExists(APT_HSS_Obj.APT_HSS.successmsg, "Add Device success msg");
			String successMsg = getTextFrom(APT_HSS_Obj.APT_HSS.successmsg, "Add Device success msg");
			String SuccessMsg = "Device is added to Service.";

			successMsg.equalsIgnoreCase(SuccessMsg);

		} else {
			Reporter.log("Add PE Device' page not navigated");

		}

	}

	public void deleteExistingDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException, AWTException {
		// waitForAjax();

		verifyExists(APT_HSS_Obj.APT_HSS.deviceAction, "Delete Action Dropdown");
		scrollDown(APT_HSS_Obj.APT_HSS.deviceAction);
		click(APT_HSS_Obj.APT_HSS.deviceAction, "Delete Action Dropdown");
		// waitForAjax();
		verifyExists(APT_HSS_Obj.APT_HSS.deviceDelete, "Delete Action Dropdown");
		click(APT_HSS_Obj.APT_HSS.deviceDelete, "Delete Option");
		// waitForAjax();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		waitForAjax();
		verifyExists(APT_HSS_Obj.APT_HSS.deleteMsg, "Delete message");

	}

	public void verifyManageService(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String Sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String serviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String servicestatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceStatus");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceStatusChangeRequired");

		verifyExists(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "service action dropdown");
		scrollDown(APT_HSS_Obj.APT_HSS.serviceactiondropdown);
		click(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "service action dropdown");

		verifyExists(APT_HSS_Obj.APT_HSS.manageLink, "manage");
		click(APT_HSS_Obj.APT_HSS.manageLink, "manage");

		verifyExists(APT_HSS_Obj.APT_HSS.manageservice_header, "manage service");

		verifyExists(APT_HSS_Obj.APT_HSS.status_servicename, "space name");
		String sid = getTextFrom(APT_HSS_Obj.APT_HSS.status_servicename, "space name");
		sid.equalsIgnoreCase(Sid);

		verifyExists(APT_HSS_Obj.APT_HSS.status_servicetype, "space name");
		String servicetype = getTextFrom(APT_HSS_Obj.APT_HSS.status_servicetype, "space name");
		servicetype.equalsIgnoreCase(serviceType);

		String ServiceDetails_value = getTextFrom(APT_HSS_Obj.APT_HSS.status_servicedetails,"status service details");

		if (isValueEmpty(ServiceDetails_value)) {
			Reporter.log("Service Details column value is empty as expected");

		} else {
			Reporter.log("Service Details column value should be empty");
		}

		String serviceStatus = getTextFrom(APT_HSS_Obj.APT_HSS.status_currentstatus,"status_current status");
		serviceStatus.equalsIgnoreCase(servicestatus);

		String LastModificationTime_value = getTextFrom(APT_HSS_Obj.APT_HSS.status_modificationtime,"status_modification time");

		if (LastModificationTime_value.contains("BST")) {
			Reporter.log("Service status is displayed as");

		}

		else {
			Reporter.log("Incorrect modification time format");

		}
		verifyExists(APT_HSS_Obj.APT_HSS.statuslink, "Status");
		click(APT_HSS_Obj.APT_HSS.statuslink, "Status");

		if (servicestatuschangerequired == "Yes") {

			if (isClickable(APT_HSS_Obj.APT_HSS.Servicestatus_popup)) {
				verifyExists(APT_HSS_Obj.APT_HSS.changestatus_dropdown, "change status");
				click(APT_HSS_Obj.APT_HSS.changestatus_dropdown, "change status");

				verifyExists(APT_HSS_Obj.APT_HSS.changestatus_dropdownvalue, "change status");
				click(APT_HSS_Obj.APT_HSS.changestatus_dropdownvalue, "change status");

				verifyExists(APT_HSS_Obj.APT_HSS.okbutton, "ok");
				click(APT_HSS_Obj.APT_HSS.okbutton, "ok");

				if (isClickable(APT_HSS_Obj.APT_HSS.servicestatushistory)) {
					Reporter.log("Service status change request logged");
				} else {
					Reporter.log(" Service status change request is not logged");
				}
			} else {
				Reporter.log(" Status link is not working");

			}
		} else {
			Reporter.log("Service status change not reqired");
			verifyExists(APT_HSS_Obj.APT_HSS.servicestatus_popupclose, "service status");
			click(APT_HSS_Obj.APT_HSS.servicestatus_popupclose, "service status");
		}

		String syncServicename = getTextFrom(APT_HSS_Obj.APT_HSS.sync_servicename,"sync_service name");
		syncServicename.equalsIgnoreCase(sid);

		String syncServiceType = getTextFrom(APT_HSS_Obj.APT_HSS.sync_servicetype,"sync_service type");
		syncServiceType.equalsIgnoreCase(serviceType);

		if (isEmptyOrNull(APT_HSS_Obj.APT_HSS.sync_servicedetails)) {
			Reporter.log("Service Details column value is empty as expected");
		} else {
			Reporter.log("Service Details column value should be empty");
		}
		verifyExists(APT_IPTransitObj.APT_IPTransit.back, "manage page back button");
		click(APT_IPTransitObj.APT_IPTransit.back, "manage page back button");
	}

	public void deleteDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException, AWTException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingDeviceName");
		if (isElementPresent(APT_HSS_Obj.APT_HSS.existingdevicegrid1)) {
			webDriver
					.findElement(By.xpath(
							"//tr//*[contains(.,'" + name + "')]//following-sibling::span//a[contains(.,'Delete')]"))
					.click();

			/*
			 * Robot r = new Robot(); r.keyPress(KeyEvent. VK_ENTER);
			 * r.keyRelease(KeyEvent. VK_ENTER);
			 */

			click(APT_HSS_Obj.APT_HSS.deletealertbutton, "Delete Device");
			waitForAjax();
			NewTab.verifysuccessmessage("Device successfully deleted");
			//scrollDown(APT_HSS_Obj.APT_HSS.Devicedeletionsuccessmessage);
			//verifyExists(APT_HSS_Obj.APT_HSS.Devicedeletionsuccessmessage, "Delete success message");
		} else {
			Reporter.log("No Device added in grid");
		}

	}

	public void deleteService() throws InterruptedException, IOException, AWTException {
		
		verifyExists(APT_HSS_Obj.APT_HSS.servicepanel_header, "service panel header");
		scrollDown(APT_HSS_Obj.APT_HSS.servicepanel_header);
		waitForAjax();
		click(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "service creation drop down");

		verifyExists(APT_HSS_Obj.APT_HSS.delete, "delete option");
		click(APT_HSS_Obj.APT_HSS.delete, "Delete option");

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		// waitForAjax();
		//NewTab.verifysuccessmessage("Service deleted successfully");

	}

	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_newOrderNumber");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_existingOrderNumber");

		if ((changeOrderSelection_newOrder.equalsIgnoreCase("No"))
				&& (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
			Reporter.log("Change Order is not performed");

		} else if (changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {
			verifyExists(APT_HSS_Obj.APT_HSS.orderactionbutton, "Action dropdown");
			click(APT_HSS_Obj.APT_HSS.orderactionbutton, "Action dropdown");

			verifyExists(APT_HSS_Obj.APT_HSS.changeorderlink, "change order link");
			click(APT_HSS_Obj.APT_HSS.changeorderlink, "change order link");

			verifyExists(APT_HSS_Obj.APT_HSS.changeorderheader, "change order header");
			String changeOrderHeader = getTextFrom(APT_HSS_Obj.APT_HSS.changeorderheader,"change order header");
			String changeOrder = "Change Order";
			changeOrderHeader.equalsIgnoreCase(changeOrder);

			verifyExists(APT_HSS_Obj.APT_HSS.changeorder_selectorderswitch, "select order switch");
			click(APT_HSS_Obj.APT_HSS.changeorder_selectorderswitch, "select order switch");

			verifyExists(APT_HSS_Obj.APT_HSS.changeordernumber, "change order number");
			click(APT_HSS_Obj.APT_HSS.changeordernumber, "change order number");
			sendKeys(APT_HSS_Obj.APT_HSS.editorderno, ChangeOrder_newOrderNumber);

			verifyExists(APT_HSS_Obj.APT_HSS.changeordervoicelinenumber, "change order voiceline number");
			click(APT_HSS_Obj.APT_HSS.changeordervoicelinenumber, "change order voiceline number");
			sendKeys(APT_HSS_Obj.APT_HSS.changeordervoicelinenumber, changevoicelineno);

			verifyExists(APT_HSS_Obj.APT_HSS.createorder_button, "create order button");
			click(APT_HSS_Obj.APT_HSS.createorder_button, "create order button");

			verifyExists(APT_HSS_Obj.APT_HSS.changeorder_okbutton, "change order ok button");
			click(APT_HSS_Obj.APT_HSS.changeorder_okbutton, "change order ok button");

			Reporter.log("Change Order is successful");
		} else if (changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
			Reporter.log("Performing Change Order functionality");

			verifyExists(APT_HSS_Obj.APT_HSS.orderactionbutton, "order action button");
			click(APT_HSS_Obj.APT_HSS.orderactionbutton, "order action button");

			verifyExists(APT_HSS_Obj.APT_HSS.changeorderlink, "change order link");
			click(APT_HSS_Obj.APT_HSS.changeorderlink, "change order link");

			verifyExists(APT_HSS_Obj.APT_HSS.changeorderheader, "change order");

			verifyExists(APT_HSS_Obj.APT_HSS.changeorder_chooseorderdropdown, "Order/Contract Number (Parent SID)");
			select(APT_HSS_Obj.APT_HSS.changeorder_chooseorderdropdown, ChangeOrder_existingOrderNumber);

			verifyExists(APT_HSS_Obj.APT_HSS.changeorder_okbutton, "change order ok button");
			click(APT_HSS_Obj.APT_HSS.changeorder_okbutton, "change order ok button");

			Reporter.log("Change Order is successful");

		}

	}

	public void VerifyManagementPanel(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String PerformanceReporting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PerformanceReporting_Checkbox");
		String WaveService_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"WaveService_checkbox");

		String ManagementOptions = "Management Options";
		String waveservice_value1 = "Yes";
		String waveservice_value2 = "No";
		scrollDown(APT_HSS_Obj.APT_HSS.managementoptions_header);
		waitForAjax();

		getTextFrom(APT_HSS_Obj.APT_HSS.managementoptions_header,"management options_header").equals(ManagementOptions);
		getTextFrom(APT_HSS_Obj.APT_HSS.managementoptions_header, "Management Options");
		// Verify Performance Reporting
		if (PerformanceReporting_Checkbox.equalsIgnoreCase("Null")) {
			getTextFrom(APT_HSS_Obj.APT_HSS.performancereporting_value, "Performance Reporting");
		} else if (PerformanceReporting_Checkbox.equalsIgnoreCase("Yes")) {
			getTextFrom(APT_HSS_Obj.APT_HSS.performancereporting_value, "Performance Reporting")
					.equals(PerformanceReporting_Checkbox);
		} else {
			getTextFrom(APT_HSS_Obj.APT_HSS.performancereporting_value, "Performance Reporting")
					.equals(PerformanceReporting_Checkbox);
		}
		// Verify Wave Service
		if (WaveService_checkbox.equalsIgnoreCase("Null")) {
			getTextFrom(APT_HSS_Obj.APT_HSS.waveservice_value, "Wave Service").equals(WaveService_checkbox);
		} else if (WaveService_checkbox.equalsIgnoreCase("Yes")) {
			getTextFrom(APT_HSS_Obj.APT_HSS.waveservice_value, "Wave Service").equals(WaveService_checkbox);
		} else {
			getTextFrom(APT_HSS_Obj.APT_HSS.waveservice_value, "Wave Service").equals(WaveService_checkbox);
		}

		if (WaveService_checkbox.equalsIgnoreCase("Yes")) {
			// verify interface speed
			getTextFrom(APT_HSS_Obj.APT_HSS.interfacespeed_value, "Interface Speed");
			// verify Type of service
			getTextFrom(APT_HSS_Obj.APT_HSS.typeofservice_value, "Type Of Service");
		}
	}

	public void verifyorderpanelinformation_Neworder(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String expectedneworderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "");
		String expectednewvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "");

		scrollDown(APT_HSS_Obj.APT_HSS.userspanel_header);

		if (neworder.equalsIgnoreCase("YES")) {

			String actualorderno = getTextFrom(APT_HSS_Obj.APT_HSS.ordernumbervalue,"order number value");
			Reporter.log("actual order number displayed in order panel is : " + actualorderno);

			String actualvoicelineno = getTextFrom(APT_HSS_Obj.APT_HSS.ordervoicelinenumbervalue,"order voice line number value");
			Reporter.log("actual voice line number displayed in order panel is : " + actualvoicelineno);

			if (expectedneworderno.equalsIgnoreCase(actualorderno)
					&& expectednewvoicelineno.equalsIgnoreCase(actualvoicelineno)) {
				Reporter.log("order information is matched");
			} else {
				sa.fail("order information is not matched");
				Reporter.log("order information is not matched");
			}

		} else {

			Reporter.log("new order is not selected");
		}

		sa.assertAll();
	}

	public void SelectInterface() throws InterruptedException, IOException {

		String InterfacesHeader = "Interfaces";
		scrollDown(APT_HSS_Obj.APT_HSS.viewdeviceheader);
		getTextFrom(APT_HSS_Obj.APT_HSS.interfaceheader, "Interfaces").equals(InterfacesHeader);
		waitForAjax();
		String InterfacesGridCheck = APT_HSS_Obj.APT_HSS.InterfacesGridCheck;
		String InterfacesGrid = getAttributeFrom(InterfacesGridCheck, "style");
		if (!InterfacesGrid.contains("height: 1px")) {
			if (isElementPresent(APT_HSS_Obj.APT_HSS.AddedInterfaces)) {
				click(APT_HSS_Obj.APT_HSS.AddedInterfaces, "Add interfaces");
				waitForAjax();
				click(APT_HSS_Obj.APT_HSS.interfacespanel_actiondropdown, "Action dropdown");
				click(APT_HSS_Obj.APT_HSS.interfacespanel_OK_link, "OK");

				waitToPageLoad();
			}

			else {
				// No Interfaces available to select
				click(APT_HSS_Obj.APT_HSS.Backbutton, "Back");
			}
		} else {
			// No Interfaces available to select
			click(APT_HSS_Obj.APT_HSS.Backbutton, "Back");
		}

		// scrollDown(APT_HSS_Obj.APT_HSS.showinterfaces_link);
		// click(APT_HSS_Obj.APT_HSS.showinterfaces_link,"Show Interfaces");

	}

	public void verifyShowInfovistaReport() throws Exception {

		showInfovista();
		waitForAjax();
	}

	/*
	 * public void selectInterfacelinkforDevice(String testDataFile, String
	 * dataSheet, String scriptNo, String dataSetNo) throws
	 * InterruptedException, IOException {
	 * 
	 * String existingDeviceName = DataMiner.fngetcolvalue(testDataFile,
	 * dataSheet, scriptNo, dataSetNo, "ExistingDeviceName"); waitForAjax();
	 * scrollDown(APT_HSS_Obj.APT_HSS.deviceheader); waitForAjax();
	 * 
	 * if(isElementPresent(APT_HSS_Obj.APT_HSS.existingdevicegrid1)) {
	 * List<String> addeddevicesList=
	 * Arrays.asList(getTextFrom(APT_HSS_Obj.APT_HSS.addeddevices_list));
	 * System.out.println(addeddevicesList); int AddedDevicesCount=
	 * addeddevicesList.size(); for(int i=0;i<AddedDevicesCount;i++) { String
	 * AddedDeviceNameText= addeddevicesList.get(i); String AddedDevice_SNo=
	 * AddedDeviceNameText.substring(0, 1);
	 * if(AddedDeviceNameText.contains(existingDeviceName)) { String
	 * AddedDevice_SelectInterfacesLink=
	 * getTextFrom(APT_HSS_Obj.APT_HSS.addeddevice_selectinterfaceslink).replace
	 * ("value", AddedDevice_SNo); click(AddedDevice_SelectInterfacesLink,
	 * "Select interface"); waitForAjax(); waitforPagetobeenable(); } } } else {
	 * Reporter.log("No Device added in grid"); }
	 * 
	 * }
	 */

	public void showInfovista() throws Exception {

		scrollDown(APT_HSS_Obj.APT_HSS.orderpanelheader);
		waitForAjax();
		click(APT_HSS_Obj.APT_HSS.serviceactiondropdown, "Action dropdown");
		waitForAjax();
		click(APT_HSS_Obj.APT_HSS.showinfovistareport_link, "Show Infovista Report");
		waitForAjax();
		waitToPageLoad();
		String expectedPageName = "SSO Login Page";

		// Switch to new tab
		List<String> browserTabs = new ArrayList<String>(webDriver.getWindowHandles());
		webDriver.switchTo().window(browserTabs.get(1));
		waitForAjax();

		try {
			if ((webDriver.switchTo().window(browserTabs.get(1)).getTitle()).contains(expectedPageName)) {
				// Get Tab name
				String pageTitle = webDriver.switchTo().window(browserTabs.get(1)).getTitle();
				Reporter.log("page title displays as: " + pageTitle);

				// assertEquals(pageTitle, expectedPageName, " on clicking 'Show
				// Infovista link', it got naviagted to "+pageTitle);
				sa.assertEquals(pageTitle, expectedPageName);

				waitForAjax();
				webDriver.close();
				webDriver.switchTo().window(browserTabs.get(0));

				Reporter.log("on clicking 'Show Infovista link', it got naviagted to " + pageTitle + " as expected");
				waitForAjax();

				Reporter.log("show info vista page actual title: " + pageTitle);
				Reporter.log("show info vista page expected title: " + expectedPageName);
			} else {
				webDriver.close();
				webDriver.switchTo().window(browserTabs.get(0));
			}
		} catch (Exception e) {

			e.printStackTrace();

			waitForAjax();
			webDriver.close();
			webDriver.switchTo().window(browserTabs.get(0));

			Reporter.log("Page is not displaying");

		}
		sa.assertAll();
	}

	public void AddExistingDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingDeviceName");
		/*
		 * scrollDown(APT_HSS_Obj.APT_HSS.deviceheader); waitForAjax();
		 * compareText("Device", APT_HSS_Obj.APT_HSS.deviceheader, "Device");
		 * click(APT_HSS_Obj.APT_HSS.AddDevice, "Add Device");
		 */
		compareText("Add Device Header", APT_HSS_Obj.APT_HSS.adddevice_header, "Add Device");
		scrollDown(APT_HSS_Obj.APT_HSS.name_dropdown);
		addDropdownValues_commonMethod("Name", APT_HSS_Obj.APT_HSS.name_dropdown, existingdevicename);
		// click(APT_HSS_Obj.APT_HSS.name_dropdown,"Name drop down");
		// webDriver.findElement(By.xpath("(//div[label[text()='Name']]//div[contains(text(),'"+existingdevicename+"'+)])")).click();

		scrollDown(APT_HSS_Obj.APT_HSS.okbutton);
		click(APT_HSS_Obj.APT_HSS.okbutton, "OK");
		waitToPageLoad();
		NewTab.verifysuccessmessage("Device successfully created.");
		scrollDown(APT_HSS_Obj.APT_HSS.Backbutton);
		click(APT_HSS_Obj.APT_HSS.Backbutton, "Back");
		waitToPageLoad();
	}

	public void selectInterfacelinkforDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingDeviceName");

		Reporter.log("check 'Select Interface' link");
		waitForAjax();
		scrollDown(APT_HSS_Obj.APT_HSS.existingdevicegrid1);
		waitForAjax();
		if (isVisible(APT_HSS_Obj.APT_HSS.existingdevicegrid1)) {
			waitForAjax();
			webDriver.findElement(By.xpath(
					"//tr//*[contains(.,'" + name + "')]//following-sibling::span//a[contains(.,'Select Interfaces')]"))
					.click();
			waitForAjax();
		} else {
			Reporter.log("No Device added in grid");
		}
	}

}
