package testHarness.aptFunctions;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.Lanlink_NewTab_Obj;

public class Lanlink_NewTab extends SeleniumUtils {
	public void searchOrderORservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String existingService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");

		mouseMoveOn(Lanlink_NewTab_Obj.Lanlink_NewTab.ManageCustomerServiceLink);
		waitForAjax();

		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchorderORservicelink, "SearchorderORservice");

		sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.searchService_serviceTextField, existingService, "Service");
		scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.searchButton);

		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchButton, "Search");
		waitToPageLoad();

		// String selectTheSearchedService =
		// getTextFrom(Lanlink_NewTab_Obj.Lanlink_NewTab.selectService).replace("value",
		// existingService);
		// click(selectTheSearchedService);
		click(Lanlink_NewTab_Obj.Lanlink_NewTab.selectService, "Select service checkbox");

		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchSerice_ActionDropdown, "Action");
		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchService_viewLink, "View");
		waitForAjax();
		waitToPageLoad();
	}

	public void editService(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

		String customerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "customerName");
		String serviceIdentification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"serviceName");
		String singleEndPointCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edtSingleendPointCPE");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "edit_Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "edit_PhoneContact");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "edit_Remark");
		String performancereporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_performancereporting");
		String proactiveMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_ProactiveMonitoring");
		String deliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_deliveryChannel");
		String managementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_managmentOrder");
		String notificationmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_notficationManagementTeam");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNtopology");
		String intermediateTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_intermediateTechnology");
		String circuitReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_circuitReference");

		String editService = "Edit Service";

		String expectedPageTitle = "Edit Order / Service";
		scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.srvicePanel);
		waitForAjax();

		// click on Action dropdown
		click(Lanlink_NewTab_Obj.Lanlink_NewTab.ActionDropdown_InViewPage, "Action");
		waitForAjax();

		// New tab Implementation for create Customer Page
		openLinkInNewTab(Lanlink_NewTab_Obj.Lanlink_NewTab.editLink_InViewPage, "Edit"); // opens
																							// link
																							// in
																							// new
																							// tab
		Switchtotab();

		waitToPageLoad();
		scrollIntoTop();

		String actualPageTitle = webDriver.getTitle();

		if (expectedPageTitle.equals(actualPageTitle)) {

			Reporter.log("Page Title is displaying as " + actualPageTitle + " as expected");

			String el1 = (Lanlink_NewTab_Obj.Lanlink_NewTab.breadcrumb1).replace("value", "Home");
			verifyExists(el1, "Home");

			String el2 = (Lanlink_NewTab_Obj.Lanlink_NewTab.breadcrumb1).replace("value", customerName);
			verifyExists(el2, "Customer Name");

			String el3 = (Lanlink_NewTab_Obj.Lanlink_NewTab.breadcrumb1).replace("value", serviceIdentification);
			verifyExists(el3, "Service Name");

			// String el4 =
			// getTextFrom(Lanlink_NewTab_Obj.Lanlink_NewTab.breadCrumbForcurrentPage").replace("value",
			// "Edit Service"));
			// verifyExists(el4, "Edit Service");

			(Lanlink_NewTab_Obj.Lanlink_NewTab.editServicePage_pnaleHeader).equals(editService);

			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.emailField, "Email");
			sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.emailField, email, "Email");
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.phoneField, "Phone Contat");
			sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.phoneField, phone, "Phone Contat");
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.remarkField, "Remark");
			sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.remarkField, remark, "Remark");
			scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.deliveryChannelField);
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.deliveryChannelField, "Delivery channel field");
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.deliveryChannelField, "Delivery channel");
			addDropdownValues_commonMethod("Delivery Channel", Lanlink_NewTab_Obj.Lanlink_NewTab.deliveryChannelField,
					deliveryChannel);
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.managementOrder, "management Order");
			addDropdownValues_commonMethod("Management Order", Lanlink_NewTab_Obj.Lanlink_NewTab.managementOrder,
					managementOrder);
			
			scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.proactiveMonitoring);
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.proactiveMonitoring, "Proactive Monitoring");
			click(Lanlink_NewTab_Obj.Lanlink_NewTab.proactiveMonitoring, "Proactive Monitoring");
			
			if (proactiveMonitoring.equalsIgnoreCase("Yes")) {
				if(isVisible(Lanlink_NewTab_Obj.Lanlink_NewTab.notificationManagement)){
				scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.notificationManagement);
				verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.notificationManagement, "Notification Management");
				addDropdownValues_commonMethod("Notification Management Team",
						Lanlink_NewTab_Obj.Lanlink_NewTab.notificationManagement, notificationmanagement);
				}
				}
			scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.performancereporting);
			verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.performancereporting, "Performance Reporting");
			click(Lanlink_NewTab_Obj.Lanlink_NewTab.performancereporting, "Performance Reporting");

			// VPN topology
			boolean vpnDisplayedValues = isElementPresent(Lanlink_NewTab_Obj.Lanlink_NewTab.VPNTopology1 + vpnTopology
					+ Lanlink_NewTab_Obj.Lanlink_NewTab.VPNTopology2);
			if (vpnDisplayedValues) {
				Reporter.log(vpnTopology + " is displaying under 'VPN Topology' as expected");

				if (vpnTopology.equals("Point-to-Point")) {

					// Intermediate Technologies
					scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.intermediateTechnology);
					verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.intermediateTechnology, "Intermediate Technologies");
					sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.intermediateTechnology, intermediateTechnology,
							"Intermediate Technologies");

					// Circuit reference
					verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.circuitReferenceTextField, "Circuit Reference");
					sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.circuitReferenceTextField, circuitReference,
							"Circuit Reference");

				}

				else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

					// Circuit Reference
					verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.circuitReferenceTextField, "Circuit Reference");
					sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.circuitReferenceTextField, circuitReference,
							"Circuit Reference");

				}
				verifyExists(Lanlink_NewTab_Obj.Lanlink_NewTab.okButton,"Ok Button");
				scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.okButton);
				click(Lanlink_NewTab_Obj.Lanlink_NewTab.okButton, "OK");

				waitToPageLoad();
				verifysuccessmessage("Service successfully updated");

				CloseProposalwindow();

			} else {

				Reporter.log("Page Title mismatches. It is displaying as: " + actualPageTitle + " ."
						+ "The Expected title is: " + expectedPageTitle);
			}
		}
	}

	public void verifysuccessmessage(String expected) throws InterruptedException {

		waitToPageLoad();

		scrollIntoTop();
		waitForAjax();
		try {

			boolean successMsg = isElementPresent(Lanlink_NewTab_Obj.Lanlink_NewTab.serivceAlert);
			if (successMsg) {
				String alrtmsg = getTextFrom(Lanlink_NewTab_Obj.Lanlink_NewTab.AlertForServiceCreationSuccessMessage);
				if (expected.contains(alrtmsg)) {
					Reporter.log("Message is verified. It is displaying as: " + alrtmsg);
				} else if (expected.equals(alrtmsg)) {
					Reporter.log("Message is verified. It is displaying as: " + alrtmsg);
				} else {
					Reporter.log("Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg);
				}
			} else {
				Reporter.log(" Success Message is not displaying");
			}
			Thread.sleep(2000);

		} catch (Exception e) {
			Reporter.log("failure in fetching success message");
			Reporter.log(expected + " message is not getting dislpayed");
			waitForAjax();
		}

	}

}
