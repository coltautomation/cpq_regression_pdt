package pageObjects.aptObjects;

public class Lanlink_OLO_Obj {
	public static class Lanlink_OLO {
		public static final String obutton_spanTag1 = "@xpath=//span[contains(text(),'OK')]";

		public static final String okbutton1 = "@xpath=//span[contains(text(),'OK')]";

		public static final String ManageCustomerServiceLink1 = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";

		public static final String managecoltnetworklink = "@xpath=(//div[@class='ant-menu-submenu-title'])[4]";

		public static final String createOrder_NextButton = "@xpath=//span[text()='Next']";

		public static final String createOrderService_warningMessage = "@xpath=(//span[text()='Order/Contract Number(Parent SID) is required'])[1]";

		public static final String serviceType_warningMessage = "@xpath=//span[text()='Service type is required']";

		public static final String CreateOrderServiceLink1 = "@xpath=//li[text()='Create Order/Service']";

		public static final String entercustomernamefield1 = "@xpath=//input[@id='customerName']";

		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";

		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";

		public static final String selectorderswitch = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";

		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";

		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";

		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";

		public static final String newordertextfield = "@xpath=//input[@id='orderName']";

		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";

		public static final String OrderContractNumber_Select1 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";

		// create order/service page

		public static final String createOrderORService = "@xpath=//div[text()='Create Order / Service']";

		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";

		public static final String choosecustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";

		public static final String nextbutton1 = "@xpath=//span[contains(text(),'Next')]";

		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";

		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";

		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";

		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";

		public static final String country1 = "@xpath=//div[div[label[text()='Country']]]/div[2]";

		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";

		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";

		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";

		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";

		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";

		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";

		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";

		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";

		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";

		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String nametextfield = "@xpath=//input[@id='name']";

		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";

		public static final String country2 = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String ocntextfield = "@xpath=//input[@id='ocn']";

		public static final String referencetextfield = "@xpath=//input[@id='reference']";

		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";

		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";

		public static final String emailtextfield = "@xpath=//input[@id='email']";

		public static final String phonetextfield = "@xpath=//input[@id='phone']";

		public static final String faxtextfield = "@xpath=//input[@id='fax']";

		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";

		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";

		public static final String okbutton2 = "@xpath=//span[contains(text(),'OK')]";

		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String clearbutton = "@xpath=//span[text()='Reset']";

		public static final String resetButton = "@xpath=//span[text()='Reset']";

		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";

		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// Verify warning messages

		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";

		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";

		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";

		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";

		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";

		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";

		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";

		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";

		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";

		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";

		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";

		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String orderpanelheader = "@xpath=(//div[contains(text(),'Order')])[1]";

		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";

		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";

		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";

		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";

		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";

		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";

		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";

		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String successmsg = "@xpath=//div[@role='alert']//span";

		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";

		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";

		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";

		public static final String EmailColumn = "@xpath=//div[@col-id='email']";

		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";

		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";

		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";

		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// New User creation in view service page

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";

		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";

		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";

		// Warning messages

		public static final String warningmsg_username = "@xpath=//label[text()='User Name']/parent::div//div";

		public static final String warningmsg_firstname = "@xpath=//label[text()='First Name']/parent::div//div";

		public static final String warningmsg_surname = "@xpath=//label[text()='Surname']/parent::div//div";

		public static final String warningmsg_postaladdress = "@xpath=//label[text()='Postal Address']/parent::div/following-sibling::div";

		public static final String warningmsg_useremail = "@xpath=//label[text()='Email']/parent::div//div";

		public static final String warningmsg_userphone = "@xpath=//label[text()='Phone']/parent::div//div";

		public static final String warningmsg_userpassword = "@xpath=//label[text()='Password']/parent::div//div";

		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";

		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";

		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";

		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";

		public static final String Email1 = "@xpath=(//div[@class='position-relative form-group'])[5]//input[@id='email']";

		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";

		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";

		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";

		public static final String OkButton = "@xpath=//button[@type='submit']//span[text()='Ok']";

		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String delete_alertpopup1 = "@xpath=//div[@class='modal-content']";

		public static final String deletebutton1 = "@xpath=//button[text()=' Delete']";

		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String userspanel_header = "@xpath=//div[text()='Users']";

		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";

		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";

		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";

		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";

		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";

		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";

		public static final String OK_button = "@xpath=//button[@type='submit']//span[text()='OK']";

		public static final String userdelete = "@xpath=//button[text()='Delete']";

		public static final String IPGuardianAccountGroup = "@xpath=//input[@id='ipGuardianAccountGrp']";

		public static final String IPGuardianAccountGroup_Text = "@xpath=//label[contains(text(),'IPGuardian Account Group')]";

		public static final String IPGuardianAccountGroup_viewpage = "@xpath=//div[div[label//b[contains(text(),'IPGuardian Account Group')]]]/div[2]";

		public static final String ColtOnlineUser = "@xpath=//input[@id='onlineUser']";

		public static final String ColtOnlineUser_Text = "@xpath=//label[contains(text(),'Colt Online User')]";

		public static final String coltonlineuser_viewpage = "@xpath=//div[div[label//b[contains(text(),'Colt Online User')]]]/div[2]";

		public static final String Password_Text = "@xpath=//label[contains(text(),'Password')]";

		public static final String Password_Textfield = "@xpath=//input[@id='password']";

		public static final String GeneratePasswordLink = "@xpath=//span[text()='Generate Password']";

		public static final String GeneratePasswordLink_Text = "@xpath=//span[@class='badge-success badge badge-secondary']";

		public static final String roleDropdown_available = "@xpath=//select[@id='availableRoles']//option";

		public static final String roleDropdown_addButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";

		public static final String roleDropdown_removeButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='value']";

		public static final String roleDropdown_selectedValues = "@xpath=//select[@id='selectedRoles']//option";

		public static final String hideRouterToolIPv4_Huawei_available = "@xpath=//select[@id='availableIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4__Huawei_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";

		public static final String hideRouterToolIPv4_Huawei_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";

		public static final String hideRouterToolIpv4_Huawei_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4_Cisco_Available = "@xpath=//select[@id='availableIPV4CommandCisco']//option";

		public static final String hideRouterToolIPv4_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String hideRouterToolIPv4_Cisco_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";

		public static final String hideRouterToolIpv4_Cisco_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandCisco']//option";

		public static final String HideService_Available = "@xpath=//select[@id='availableHideService']//option";

		public static final String HideService_addButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";

		public static final String HideService_removeButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";

		public static final String HideServicesDropdown_selectedValues = "@xpath=//select[@id='selectedHideService']//option";

		public static final String HideSiteOrder_Available = "@xpath=//select[@id='availableHideSiteOrder']//option";

		public static final String hideSiteOrder_addbutton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";

		public static final String hideSiteOrder_removeButton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";

		public static final String HideSiteOrderDropdown_selectedValues = "@xpath=//select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath=//select[@id='availableIPV6Command']//option";

		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";

		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";

		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String selectValueUnderUserPanel_ViewSericePage = "@xpath=//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String changeorder_cancelbutton = "@xpath=//button[span[text()='Cancel']]";

		public static final String changeOrder_orderDropdown = "@xpath=//div[label[text()='Order/Contract Number (Parent SID)']]//input";

		public static final String fetchorderNumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//following-sibling::div";

		public static final String checkExistingUserUnderUserPanel = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String userPanelFilterButton = "@xpath=(//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String userNamefilterTextField = "@xpath=//input[@id='filterText']";
		public static final String selectuncheckedCheckbox_UserPanel = "@xpath=(//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";

		// CreateOrderService

		public static final String obutton_spanTag2 = "@xpath=//span[contains(text(),'OK')]";

		public static final String okbutton3 = "@xpath=//span[contains(text(),'OK')]";

		public static final String ManageCustomerServiceLink = "@xpath=//span/b[contains(text(),'MANAGE CUSTOMER')]";
		public static final String nextbutton2 = "@xpath=//span[contains(text(),'Next')]";
		public static final String chooseAcustomerWarningMessage = "@xpath=//div[text()='Choose a customer']";

		public static final String order_contractnumberErrmsg1 = "@xpath=(//span[text()='Order/Contract Number(Parent SID) is required'])[2]";
		public static final String servicetypeerrmsg1 = "@xpath=//span[contains(text(),'Service type is required')]";
		public static final String interfaceSpeedErrmsg1 = "@xpath=//span[contains(text(),'Interface Speed is required')]";
		public static final String servicesubtypeErrMsg1 = "@xpath=//span[contains(text(),'Sub service type is required')]";

		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String chooseCustomer_build3 = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30']";
		public static final String entercustomernamefield2 = "@xpath=//input[@id='customerName']";

		public static final String chooseCustomerdropdown1 = "@xpath=//div[label[text()='Choose a customer']]//input";
		public static final String ordererrormsg = "@xpath=(//span[text()='Order/Contract number is required'])[1]";
		public static final String Servicetypeerrmsg = "@xpath=//span[contains(text(),'Service type is required')]";

		public static final String CreateOrderService_Text = "@xpath=//li[text()='Create Order Service']";
		public static final String ChooseCustomer_Select = "@xpath=(//input[@placeholder='Select...'])[2]";
		public static final String Next_Button1 = "@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";
		public static final String OrderContractNumber_Select = "@xpath=(//input[@placeholder='Select...'])[2]";
		public static final String ordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']]//input)[1]";
		public static final String ordercontractNumberdropdown_secondTime = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']]//input)[1]";

		public static final String OrderContractNumber_Select_build3_1 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String ServiceType_Select1_build3_2 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[2]";
		public static final String servicetypedropdowntoclick = "@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String orderdropdown = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']])[1]//input";

		public static final String OrderContractNumbertoggleButton1 = "@xpath=(//div[div[label[text()='Select Order']]]//div[@class='react-switch-handle'])[1]";
		public static final String OrderName1 = "@xpath=//input[@id='orderName']";
		public static final String createButton = "@xpath=//button[@class='btn btnSquar btnWitdh undefined btn-secondary   ']";
		public static final String CreateOrderSuccessMessage = "@xpath=//span[contains(text(),'Order created successfully')]";

		public static final String InterfaceSpeed1 = "@xpath=//div[label[contains(text(),'Interface Speed')]]//input";
		public static final String Interfacespeedropdownvalue1 = "@xpath=//div[@role='list']//span[@role='option']";
		public static final String ServiceSubtype1 = "@xpath=//div[label[contains(text(),'Service Subtype:')]]//input";
		public static final String intefacespeederrormsg = "@xpath=//span[contains(text(),'Interface Speed is required')]";
		public static final String servicesubtypeerrmsg = "@xpath=//span[contains(text(),'Sub service type is required')]";

		public static final String AvailableCircuits1 = "@xpath=//div[label[contains(text(),'Available Circuits')]]//input";

		public static final String modularmspcheckbox1 = "@xpath=//div[@class='form-group']//div[@class='form-group']//input[@id='modularMSP']";
		public static final String AutocreateServicecheckbox1 = "@xpath=//input[@id='autoCreateService']";

		public static final String A_Endtechnology1 = "@xpath=//div[label[contains(text(),'A-End Technology')]]//input";
		public static final String B_Endtechnology1 = "@xpath=//div[label[contains(text(),'B-End Technology')]]//input";

		public static final String ServiceType_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'][1])[2]";
		public static final String listOfservicetypes = "@xpath=//div/span[@role='option']";

		// serviceSelected

		public static final String obutton_spanTag3 = "@xpath=//span[contains(text(),'OK')]";
		public static final String okbutton4 = "@xpath=//span[contains(text(),'OK')]";
		public static final String ServiceIdentification1 = "@xpath=//input[@id='serviceIdentification']";
		public static final String ServiceType1 = "@xpath=//div[@class='customLabelValue form-label']";
		public static final String Email2 = "@xpath=//input[@id='email']";
		public static final String PhoneContact1 = "@xpath=//input[@id='phoneContact']";
		public static final String Remark1 = "@xpath=//textarea[@name='remark']";
		public static final String Performancereporting1 = "@xpath=//input[@id='proactiveMonitoring']";
		public static final String proActivenotification_TransmissionLink1 = "@xpath=//input[@id='proactiveNotification']";
		public static final String NotificationManagementTeam_TransmissionLink1 = "@xpath=//select[@id='notificationManagementTeam']";
		public static final String Next_Button2 = "@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";

		public static final String AddMASdevice = "@xpath=//a[contains(text(),'Add MAS Switch')]";
		public static final String AddPEDevice = "@xpath=//a[contains(text(),'Add PE Device')]";
		public static final String SelectIMSlocation_MAS = "@xpath=//select[@id='imsPOP']";
		public static final String selectdevice_ToggleButton1 = "@xpath=//div[@class='div-margin row']//div[@class='react-switch-bg']";
		public static final String Name1 = "@xpath=//input[@id='name']";
		public static final String VenderModel1 = "@xpath=//select[@id='vender']";
		public static final String managementaddress1 = "@xpath=//input[@id='address']";
		public static final String Snmpro1 = "@xpath=//input[@id='snmpro']";
		public static final String country3 = "@xpath=//select[@id='country']";
		public static final String city1 = "@xpath=//select[@id='city']";
		public static final String site1 = "@xpath=//select[@id='site']";
		public static final String premise1 = "@xpath=//select[@id='premise']";

		// VOIP Access
		public static final String serviceType_VOIPaccess = "@xpath=//div[contains(text(),'VOIP Access')]";
		public static final String ResellerCode = "@xpath=//input[@id='resellerCode']";
		public static final String managementOptions_Package = "@xpath=//select[@id='package']";
		public static final String managementOptions_managesservice = "@xpath=//div[label[contains(text(),'Managed Service')]]//input";
		public static final String managementOptions_SyslogEventView = "@xpath=//div[label[contains(text(),'Syslog Event View')]]//input";
		public static final String managementOptions_serviceStatusView = "@xpath=//div[label[contains(text(),'Service Status View')]]//input";
		public static final String managementOptions_RouterConfigView = "@xpath=//div[label[contains(text(),'Router Configuration View')]]//input";
		public static final String managementOptions_PerformnceReport = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String managementOptions_DialsUserAdmin = "@xpath=//div[label[contains(text(),'Dial User Administration')]]//input";
		public static final String managementOptions_ProActiveNotify = "@xpath=//div[label[contains(text(),'Dial User Administration')]]//input";
		public static final String managementOptions_NotifyManageTeam = "@xpath=//select[@id='deliveryChannel']";

		// NGIN
		public static final String managementOptions_CustomAdmin = "@xpath=//input[@id='customerAdministration']";
		public static final String managementOptions_SANadmin = "@xpath=//input[@id='sanAdministration']";
		public static final String managementOptions_ResellerAdmin = "@xpath=//input[@id='resellerAdministration']";

		// MDF/MVF/DI
		public static final String STRM_IP_tag = "@xpath=//input[@id='strmIpTag']";

		// LANLINK

		public static final String ClassNameForDropdowns = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String selectValueUnderTechnologyDropdown = "@xpath=//div[text()='value']";

		public static final String obutton_spanTag = "@xpath=//span[contains(text(),'OK')]";

		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String OrderContractNumber_Select_build3 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String ServiceType_Select1_build3 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[2]";
		public static final String OrderContractNumbertoggleButton = "@xpath=(//div[div[label[text()='Select Order']]]//div[@class='react-switch-handle'])[1]";
		public static final String OrderName = "@xpath=//input[@id='orderName']";

		public static final String ServiceSubtype = "@xpath=(//div[label[contains(text(),'Service Subtype')]]//input)[1]";
		public static final String InterfaceSpeed = "@xpath=//div[label[contains(text(),'Interface Speed')]]//input";
		public static final String Interfacespeedropdownvalue = "@xpath=//div[@role='list']//span[@role='option']";
		public static final String AvailableCircuits = "@xpath=//div[label[contains(text(),'Available Circuits')]]//input";
		public static final String Next = "@xpath=(//span[contains(text(),'Next')])[1]";
		public static final String A_Endtechnology = "@xpath=//div[label[contains(text(),'A-End Technology')]]//input";
		public static final String A_EndTechnology_xbuton = "@xpath=//div[label[text()='A-End Technology']]//div[text()='�']";
		public static final String B_Endtechnology = "@xpath=//div[label[contains(text(),'B-End Technology')]]//input";
		public static final String B_EndTechcnology_xbutton = "@xpath=//div[label[text()='B-End Technology']]//div[text()='�']";
		public static final String Autocreatebutton = "@xpath=//span[contains(text(),'AutoCreate Service')]";
		public static final String interfacespeedvaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[3]";
		public static final String interfacespeedlabel = "@xpath=//label[contains(text(),'Interface Speed')]";
		public static final String servicesubtypevaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[2]";
		public static final String servicetypevaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[1]";
		public static final String modularmspcheckbox = "@xpath=//div[@class='form-group']//div[@class='form-group']//input[@id='modularMSP']";
		public static final String AutocreateServicecheckbox = "@xpath=//input[@id='autoCreateService']";
		public static final String notificationmanagement = "@xpath=//div[label[contains(text(),'Notification Management Team')]]//input";
		public static final String E_VPNtechnologyDropdown = "@xpath=//div[label[text()='E-VPN Technology']]//input";
		public static final String E_VPNtechnologyDropdown_xButton = "@xpath=//div[label[text()='E-VPN Technology']]//div[text()='�']";
		public static final String ENNI_checkbox = "@xpath=//div[label[contains(text(),'ENNI')]]//input";

		public static final String configurtoinptionpanel_webelementToScroll = "@xpath=//div[contains(text(),'Configuration Options')]";

		public static final String ServiceIdentification = "@xpath=//input[@id='serviceIdentification']";
		public static final String ServiceType = "@xpath=//div[contains(text(),'LANLink')]";
		public static final String circuitType_MSPselected_Default = "@xpath=//div[div[div[label[text()='Circuit Type']]]]/following-sibling::div//span[text()='Default']";
		public static final String EndpointCPE = "@xpath=//div[label[contains(text(),'Single Endpoint CPE')]]//input";
		public static final String Email = "@xpath=//div[label[contains(text(),'Email')]]//input";
		public static final String PhoneContact = "@xpath=//input[@id='phoneContact']";
		public static final String Remark = "@xpath=//textarea[@name='remark']";
		public static final String Performancereporting = "@xpath=//input[@id='proactiveMonitoring']";
		public static final String proActivenotification_TransmissionLink = "@xpath=//input[@id='proactiveNotification']";
		public static final String NotificationManagementTeam_TransmissionLink = "@xpath=//select[@id='notificationManagementTeam']";
		public static final String Next_Button = "@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";
		public static final String performancemonitoring = "@xpath=//div[label[contains(text(),'Performance Monitoring')]]//input";
		public static final String performanceReportingcheckbox = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String perCoSperformncereport = "@xpath=//div[label[contains(text(),'Per CoS Performance Reporting')]]//input";
		public static final String HCoSperformanceReport = "@xpath=//div[label[text()='HCoS Performance Reporting (Modular MSP)']]//input";
		public static final String ActelisBasedcheckbox = "@xpath=//input[@id='actelisBased']";
		public static final String standardCIRtextfield = "@xpath=//input[@id='standardCir']";
		public static final String standardEIRtextfield = "@xpath=//input[@id='standardEir']";
		public static final String premiumCIRtextfield = "@xpath=//input[@id='premiumCir']";
		public static final String premiumEIRtextfield = "@xpath=//input[@id='premiumEir']";
		public static final String proactiveMonitoring = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//input";
		public static final String DeliveryChannel = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";
		public static final String managementOrder = "@xpath=//div[label[contains(text(),'Management Order')]]//input";
		public static final String VPNtopology = "@xpath=//div[label[contains(text(),'VPN Topology')]]//input";
		public static final String VPNtopology_xbutton = "@xpath=//div[label[text()='VPN Topology']]//div[text()='�']";
		public static final String IntermediateTechnology = "@xpath=//div[label[contains(text(),'Intermediate Technologies')]]//input";
		public static final String circuitReference = "@xpath=//div[label[contains(text(),'Circuit Reference')]]//input";
		public static final String Aggregate_10Gig_1Gig_Traffic = "@xpath=//div[label[contains(text(),'Aggregate 10Gig-1Gig Traffic')]]//input";
		public static final String DeliverychannelForselectTag = "@xpath=//select[@id='deliveryChannel']";
		public static final String VpnTopologyForSelctTag = "@xpath=//select[@id='vpnTechnology']";
		public static final String ENNIcheckbox = "@xpath=//input[@id='enni']";
		public static final String xButton = "@xpath=//span[contains(text(),'�')]";
		public static final String deliverychannel_withclasskey = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";
		public static final String cosCheckbox_createService = "@xpath=//input[@id='cos']";
		public static final String multiport_checkbox = "@xpath=//input[@id='multiport']";

		public static final String cicuitreferenceLabel_serviceCreation = "@xpath=//label[contains(text(),'Circuit Reference')]";
		public static final String circuitTyeplabel_servicecreation = "@xpath=//label[contains(text(),'Circuit Type')]";
		public static final String intermediatetechLabelname = "@xpath=//label[contains(text(),'Intermediate Technology')]";

		public static final String vpntopoloty_xbutton = "@xpath=(//div[@class='react-dropdown-select-clear css-w49n55-ClearComponent e11qlq5e0'])[3]";
		public static final String notigymanagement_xbutton = "@xpath=//div[label[contains(text(),'Notification Management Team')]]//div[text()='�']";
		public static final String deliverychannel_xbutton = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//div[text()='�']";
		public static final String managementOrder_xButton = "@xpath=//div[label[contains(text(),'Management Order')]]//div[text()='�']";

		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";

		public static final String managementConnectForselectTag = "@xpath=//select[@id='managementConnection']";
		public static final String listofcircuittypes = "@xpath=//div[@class='div-border div-margin container']//div[@class='row'][4]//input";
		public static final String cancelButton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String Editinterface_popupTitlename = "@xpath=//div[@class='modal-title h4']";
		public static final String EditInterfacepopup_xbutton = "@xpath=//span[text()='�']";

		public static final String AlertForServiceCreationSuccessMessage = "@xpath=//div[@role='alert']/span";
		public static final String serivceAlert = "@xpath=//div[@role='alert']";

		public static final String Actiondropdown_siteorder = "@xpath=//div[div[text()='Site Orders']]//button[text()='Action']";
		public static final String ActiondropdownforshowInterfaceunderEquipment = "@xpath=(//button[@id='dropdown-basic-button'])[2]";

		public static final String Viewcreatedservice_serviceIdentification = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_Servicetype = "@xpath=//div[div[label[contains(text(),'Service Type')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_servicesubtype = "@xpath=//div[div[label[contains(text(),'Service Subtype')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_interfacespeed = "@xpath=//div[div[label[contains(text(),'Interface Speed')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_SinplepointCPE = "@xpath=//div[div[label[contains(text(),'Single Endpoint CPE')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_email = "@xpath=//div[div[label[contains(text(),'Email')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_phone = "@xpath=//div[div[label[contains(text(),'Phone Contact')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_msp = "@xpath=//div[div[label[contains(text(),'Modular MSP')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_remark = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_performReport = "@xpath=//div[div[label[contains(text(),'Performance Reporting')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_proactivemonitor = "@xpath=//div[div[label[contains(text(),'Proactive Monitoring')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_deliverychanel = "@xpath=//div[div[label[contains(text(),'Delivery Channel')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_vpnTopology = "@xpath=//div[div[label[contains(text(),'VPN Topology')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_circuittype = "@xpath=//div[div[label[contains(text(),'Circuit Type')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_circuitreference = "@xpath=//div[div[label[contains(text(),'Circuit Reference')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_notificationManagement = "@xpath=//div[div[label[contains(text(),'Notification Management Team')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_managementorder = "@xpath=//div[div[label[contains(text(),'Management Order')]]]//div[@class='customLabelValue form-label']";
		public static final String viewcreatedservice_ManagementConnection = "@xpath=//div[div[label[contains(text(),'Management Connection')]]]//div[@class='customLabelValue form-label']";
		public static final String viewcreatedservice_ENNI = "@xpath=//div[div[label[contains(text(),'ENNI')]]]//div[@class='customLabelValue form-label']";

		// Add site order Error warning messages
		public static final String Addsiteorder_countryerrmsg = "@xpath=//div[text()='Device Country']";
		public static final String Addsiteorder_devciexngCityErrmsg = "@xpath=//div[text()='Device Xng City']";
		public static final String Addsiteorder_xngcitynameerrmsg = "@xpath=//div[text()='Device Xng City Name']";
		public static final String Addsiteorder_xngcityCodeerrmsg = "@xpath=//div[text()='Device Xng City Code']";
		public static final String Addsiteorder_csrnameErrmsg = "@xpath=//div[text()='Physical Site: CSR Name']";
		public static final String Addsitieorder_technologyErrmsg = "@xpath=//div[text()='Technology']";
		public static final String Addsiteorder_physicalsiteErrmsg = "@xpath=//div[text()='Physical Site']";
		public static final String Addsiteorder_sitOrderNumberErrmsg = "@xpath=//div[text()='Site Order Number(Siebel Service ID)']";
		public static final String Addsiteorder_circuitReferenceErrmsg = "@xpath=//div[text()='Circuit Reference']";
		public static final String Addsiteorder_IVReferenceErrmsg = "@xpath=//div[text()='IV Reference']";

		// Add Site Order fields
		public static final String Addsiteorderlink = "@xpath=//a[contains(text(),'Add Site Order')]";
		public static final String EditsideOrderlink = "@xpath=//a[contains(text(),'Edit')]";
		public static final String deletesiteorderlink = "@xpath=//a[contains(text(),'Delete')]";
		public static final String viewsiteorderlink = "@xpath=//a[@class='dropdown-item'][contains(text(),'View')]";
		public static final String Addsiteorder_Country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String Addsiteorder_City = "@xpath=//div[label[contains(text(),'Device Xng City')]]//input";
		public static final String Addsiteorder_Citytogglebutton = "@xpath=(//div[@class='react-switch']/div)[2]";
		public static final String Addsiteorder_disabledCitytogglebutton = "@xpath=(//div[@class='react-switch']/div)[3]";
		public static final String Addsiteorder_Sitetogglebutton = "@xpath=(//div[@class='react-switch']/div)[6]";
		public static final String AddsiteOrdr_disabledSitetogglebutton = "@xpath=(//div[div[label[text()='Select Site']]]//div[@class='react-switch-handle'])[1]";
		public static final String Addsiteorder_sitedropdown = "@xpath=//div[label[contains(text(),'Physical Site')]]//input";
		public static final String Addsiteorder_CSRname = "@xpath=//input[@id='csrName']";
		public static final String Addsiteorder_performancereporting = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String Addsiteorder_performancereporting_xbutton = "@xpath=//div[label[text()='Performance Reporting']]//div[text()='�']";
		public static final String Addsiteorder_performancereportingdefaultvalue = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//span[text()='Follow Service']";
		public static final String Addsiteorder_proactivemonitoring = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//input";
		public static final String Addsitorder_proactivemonitoring_xbutton = "@xpath=//div[label[text()='Proactive Monitoring']]//div[text()='�']";
		public static final String Addsiteorder_proactivemonitoringdefaultvalue = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//span[text()='Follow Service']";
		public static final String Addsiteorder_smartmonitoring = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";
		public static final String Addsiteorder_smartmonitoring_xbutton = "@xpath=//div[label[text()='Smarts Monitoring']]//div[text()='�']";
		public static final String Addsiteorder_smartmonitoringdefaultvalue = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//span[text()='Follow Service']";
		public static final String Addsiteorder_Technology = "@xpath=//div[label[contains(text(),'Technology')]]//input";
		public static final String Addsiteorder_sitealias = "@xpath=//div[label[text()='Site Alias']]//input";
		public static final String Addsiteorder_Vlanid = "@xpath=//div[label[text()='VLAN Id']]//input";
		public static final String Addsiteorder_DCAenabledsitecheckbox = "@xpath=//input[@id='dcaEnabledSite']";
		public static final String Addsiteorder_next = "@xpath=//span[contains(text(),'Next')]";
		public static final String Addsiteorder_cancel = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String Addsiteorder_cloudserviceProvider = "@xpath=//div[label[contains(text(),'Cloud Service Provider')]]//input";
		public static final String Addsiteorder_remark = "@xpath=//div[label[text()='Remark']]//textarea";
		public static final String Addsiteorder_selectcitytoggle = "@xpath=(//div[div[label[text()='Select City']]]//div[@class='react-switch-bg'])[1]";
		public static final String Addsiteorder_xngcityname = "@xpath=//input[@id='cityName']";
		public static final String Addsiteorder_XNGcitycode = "@xpath=//input[@id='cityCode']";
		public static final String Addsiteorder_IVreferencedropdown = "@xpath=//div[label[contains(text(),'IV Reference')]]//input";
		public static final String Addsiteorder_circuitReferenceField = "@xpath=//div[label[contains(text(),'Circuit Reference')]]//input";
		public static final String Addsiteorder_AccessCircuitID = "@xpath=//div[label[text()='Access Circuit ID']]//input";
		public static final String Addsiteorder_AccessCircuitIDwarningMessage = "@xpath=//div[text()='Access Circuit ID']";
		public static final String Addsiteorder_offnetCheckbox = "@xpath=//div[label[contains(text(),'Offnet')]]//input";
		public static final String Addsiteorder_spokeId = "@xpath=//div[div[label[contains(text(),'Spoke Id')]]]//div[text()='0']";
		public static final String Addsiteorder_spokeIdField = "@xpath=//label[contains(text(),'Spoke Id')]";
		public static final String Addsiteorder_GCRoloTypeDropdown = "@xpath=//div[label[contains(text(),'GCR OLO Type')]]//input";
		public static final String Addsiteorder_GCRoloTypeDropdown_xbutton = "@xpath=//div[label[text()='GCR OLO Type']]//div[text()='�']";
		public static final String Addsiteorder_VLAN = "@xpath=//div[label[text()='VLAN']]//input";
		public static final String Addsiteorder_VLANEtherTypeDropdown = "@xpath=//div[label[text()='VLAN Ether Type']]//input";
		public static final String Addsiteorder_VLAnEtheryTypeDropdown_xbutton = "@xpath=//div[label[text()='VLAN Ether Type']]//div[text()='�']";
		public static final String Addsiteorder_PrimaryVLAN = "@xpath=//div[label[text()='Primary VLAN']]//input";
		public static final String Addsiteorder_primaryVLANEtherTypeDropdown = "@xpath=//div[label[contains(text(),'Primary VLAN Ether Type')]]//input";
		public static final String Addsiteorder_primaryVLANEtherTypeDropdown_xbutton = "@xpath=//div[label[text()='Primary VLAN Ether Type']]//div[text()='�']";
		public static final String Addsiteorder_EPNoffnetCheckbox1 = "@xpath=//div[label[contains(text(),'EPN Offnet')]]//input";
		public static final String Addsiteorder_siteordernumbertextfield = "@xpath=//div[label[contains(text(),'Site Order Number(Siebel Service ID)')]]//input";
		public static final String Addsiteorder_EPNoffnetCheckbox = "@xpath=//div[label[contains(text(),'EPN Offnet')]]//input";
		public static final String Addsiteorder_EPNEOSDHCheckbox = "@xpath=//div[label[contains(text(),'EPN EOSDH')]]//input";
		public static final String Addsiteorder_IVreference_Primary = "@xpath=//div[contains(text(),'Primary')]";
		public static final String Addsiteorder_IVreference_Access = "@xpath=//div[contains(text(),'Access')]";
		public static final String AddSiteOrder_sitePreferenceType_Dropodwn = "@xpath=//label[text()='Site Preference Type']/parent::div//input";

		public static final String Addsiteorder_Devicenamefield = "@xpath=//div[label[text()='Device Name']]//input";
		public static final String Addsiteorder_nonterminationpoint = "@xpath=//div[label[text()='Non Termination Point']]//input";
		public static final String Addsiteorder_protected = "@xpath=//div[label[text()='Protected']]//input";
		public static final String Addsiteorder_mappingModedropdown = "@xpath=//div[label[text()='Mapping Mode']]//input";
		public static final String Addsiteorder_mappingModedropdown_xbutton = "@xpath=//div[label[text()='Mapping Mode']]//div[text()='�']";
		public static final String portname_textField = "@xpath=//div[label[text()='Port Name']]//input";
		public static final String vlanid_textfield = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String Backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String Editsiteorder_country = "@xpath=(//div[@class='customLabelValue form-label'])[1]";
		public static final String Editsiteorder_city = "@xpath=(//div[@class='customLabelValue form-label'])[2]";
		public static final String Editsiteorder_CSRname = "@xpath=(//div[@class='customLabelValue form-label'])[3]";
		public static final String Editsiteorder_performreport = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String Editsiteorder_proactivemonitor = "@xpath=//div[label[contains(text(),'Procative Monitoring')]]//input";
		public static final String Editsiteorder_smartmonitor = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";
		public static final String Editsiteorder_technology = "@xpath=(//div[@class='customLabelValue form-label'])[4]";
		public static final String Editsiteorder_sitealias = "@xpath=//div[label[contains(text(),'Site Alias')]]//input";
		public static final String Editsiteorder_VLANid = "@xpath=//div[label[contains(text(),'VLAN Id')]]//input";
		public static final String Editsiteorder_remark = "@xpath=//textarea[@placeholder='Enter Remarks']";
		public static final String Editsiteorder_DCAenabledsite = "@xpath=//div[label[contains(text(),'DCA Enabled Site')]]//input";
		public static final String Editsiteorder_cloudserviceprovider = "@xpath=//div[label[contains(text(),'Cloud Service Provider')]]//input";

		public static final String verifysiteOrder_Devicecountry = "@xpath=//div[div[label[contains(text(),'Device Country')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_DeviceCity = "@xpath=//div[div[label[contains(text(),'Device Xng City')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_CSRname = "@xpath=//div[div[label[contains(text(),'CSR Name')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_performReport = "@xpath=//div[div[label[contains(text(),'Performance Reporting')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_proactivemonitor = "@xpath=//div[div[label[contains(text(),'Procative Monitoring')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_smartmonitor = "@xpath=//div[div[label[contains(text(),'Smarts Monitoring')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_Technology = "@xpath=//div[div[label[contains(text(),'Technology')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_DCAenabledsite = "@xpath=//div[div[label[contains(text(),'DCA Enabled Site')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_VLAnid = "@xpath=//div[div[label[contains(text(),'VLAN Id')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_sitealias = "@xpath=//div[div[label[contains(text(),'Site Alias')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_remark = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteordertablevalue_totalpages = "@xpath=";
		public static final String verifysiteordertablevalue_currentpge = "@xpath=";
		public static final String verifysiteordertablevalue_nextpage = "@xpath=";
		public static final String verifysiteordertablevalue_previouspage = "@xpath=";

		public static final String Deletesiteorderrbutton = "@xpath=//button[@class='btn btn-danger']";

		public static final String totalpage_intermeiateshowinterfacelink = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//span[@ref='lbTotal']";
		public static final String currentpage_intermeduateshowinterfacelink = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//span[@ref='lbCurrent']";
		public static final String TotalPagesforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbTotal']";
		public static final String Currentpageforsiteorderfunc = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbCurrent']";
		public static final String pagenavigationfornextpage_underviewserviceforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//button[text()='Next']";
		public static final String Equipmentconfig_Currentpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String Equipmentconfig_Totalpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String Equipmentconfig_nextpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//button[text()='Next']";

		public static final String Equipment_configurationPanelName = "@xpath=//div[text()='Equipment']";
		public static final String IntermediateEquipment_configurationPanelName = "@xpath=//div[text()='Intermediate Equipment']";

		public static final String intermediateEquip_Totalpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//span[@ref='lbTotal']";
		public static final String intermediateequip_currentpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//span[@ref='lbCurrent']";
		public static final String intermediateequip_nextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//button[@ref='btNext']";

		public static final String Pagenavigationfornextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[2]//button[text()='Next']";

		public static final String pagenavigateforshowinterface = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//button[text()='Next']";
		public static final String pagenavigateforIntermediate = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//button[text()='Next']";

		// Add CPE device for Provider Equipment Error Message
		public static final String AddCPEdevice_nameerrmsg = "@xpath=//span[contains(text(),' should be replaced by corresponding value on Device Name')]";
		public static final String AddCPEdevice_snmproerrmsg = "@xpath=//div[text()='Snmpro']";
		public static final String AddCPEdevice_managementAddresserrmsg = "@xpath=//div[text()='Management Address']";
		public static final String AddCPEdevice_powerAlarmerrmsg = "@xpath=//div[text()='Power Alarm']";
		public static final String AddCPEdevice_mediaselectionerrmsg = "@xpath=//div[text()='Media Selection']";
		public static final String AddCPEdevice_macAdressErrmsg = "@xpath=//div[text()='MAC Address']";
		public static final String AddCPEdevice_vendorErrmsg = "@xpath=//div[text()='Vendor/Model']";
		public static final String AddCPEdevice_serialNumberErrmsg = "@xpath=//div[text()='Serial Number']";
		public static final String AddCPEdevice_countryErrmsg = "@xpath=//div[text()='Country']";
		public static final String AddCPEdevice_hexaSerialNumnerErrmsg = "@xpath=//div[text()='Hexa Serial Number']";

		// Add CPE device fields for Provider Eqeuipment
		public static final String CPEdevice_adddevicelink = "@xpath=//div[div[div[text()='Equipment']]]//a[text()='Add Device']";
		public static final String CPEdevice_showinterfaceslink = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Equipment']]]//a[text()='Show Interfaces']";
		public static final String CPEdevice_hideinterfaceslinkforequipment = "@xpath=//div[div[@class='div-margin row']//div[text()='Equipment']]//a[text()='Hide Interfaces']";
		public static final String AddCPEdevice_Name = "@xpath=//div[label[contains(text(),'Name')]]//input";
		public static final String AddCPEdevice_selectdevicetogglebutton = "@xpath=(//div[@class='react-switch'])[1]";
		public static final String AddCPEdevice_vender = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String AddCPEdevice_vlanId = "@xpath=//div[label[text()='VLAN Id']]//input";
		public static final String AddCPEdevice_snmpro = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";
		public static final String AddCPEdevice_manageaddress = "@xpath=//input[@id='managementAddress']";
		public static final String AddCPEdevice_manageaddressdropdown = "@xpath=//div[label[contains(text(),'Management Address')]]//input";
		public static final String AddCPEdevice_getSubnetbutton = "@xpath=//span[text()='Get Subnets']";
		public static final String AddCPEdevice_selectSubnettogglebutton = "@xpath=//div[div[label[contains(text(),'Select SubNet')]]]//div[@class='react-switch-bg']";
		public static final String AddCPEdevice_mepid = "@xpath=//div[label[contains(text(),'MEP Id')]]//input";
		public static final String AddCPEdevice_poweralarm = "@xpath=//div[label[text()='Power Alarm']]//input";
		public static final String AddCPEdevice_mediaselection = "@xpath=//div[label[text()='Media Selection']]//input";
		public static final String AddCPEdevice_macaddress = "@xpath=//div[label[text()='MAC Address']]//input";
		public static final String AddCPEdevice_serialnumber = "@xpath=//div[label[text()='Serial Number']]//input";
		public static final String AddCPEdevice_hexaserialnumber = "@xpath=//input[@id='hexaSerialNumber']";
		public static final String AddCPEdevice_linklostforowarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//input[@type='checkbox']";
		public static final String AddCPEdevice_OKbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String AddCPEdevice_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String CPEdevice_snmpro_autoPopulatedValue = "@xpath=//input[@value='JdhquA5']";

		public static final String viewCPEdevicelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='View'])[3]";
		public static final String viewCPEdevice_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_mepid = "@xpath=//div[div[label[contains(text(),'MEP Id')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_poweralarm = "@xpath=//div[div[label[contains(text(),'Power Alarm')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_mediaselection = "@xpath=//div[div[label[contains(text(),'Media Selection')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_linklostforwarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_macaddress = "@xpath=//div[div[label[contains(text(),'MAC Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_serialnumber = "@xpath=//div[div[label[text()='Serial Number']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_hexaserialnumber = "@xpath=//div[div[label[contains(text(),'Hexa Serial Number')]]]//div[@class='customLabelValue form-label']";
		public static final String viewPCEdevice_Actiondropdown = "@xpath=(//div[div[text()='View CPE Device']]//button[text()='Action'])";

		public static final String EditCPEdevicelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Edit'])[3]";
		public static final String EditCPEdevicelinkunderviewpage = "@xpath=//a[text()='Edit']";
		public static final String EditCPEdevlielink_underEquipment = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Edit'])[2]";
		public static final String EditCPEdevice_vendoModel_xbutton = "@xpath=//div[label[text()='Vendor/Model']]//div[text()='�']";
		public static final String EditCPEdevice_powerAlarm_xbutton = "@xpath=//div[label[text()='Power Alarm']]//div[text()='�']";
		public static final String EditCPEdevice_mediaselection_xbutton = "@xpath=//div[label[text()='Media Selection']]//div[text()='�']";

		public static final String DeletfromserviceforcpeDevice_Equipment = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Delete from Service'])[3]";
		public static final String deleteMessage_equipment = "@xpath=//div[@class='modal-body']";
		public static final String deletebutton_deletefromsrviceforequipment = "@xpath=//button[@class='btn btn-danger']";
		public static final String xbuttonfordeletefromservicepopup_Equipment = "@xpath=//div[@class='modal-header']//span[contains(text(),'�')]";
		public static final String deleteDevice_successmEssage = "@xpath=//span[text()='Site device deleted successfully']";

		public static final String technologyPopup = "@xpath=//div[@class='modal-title h4']";
		public static final String tchnologyPopup_dropdownValues = "@xpath=//div[label[@class='form-label labelStyle']]//span";
		public static final String technologypopup_dropdown = "@xpath=//div[label[@class='form-label labelStyle']]//input";
		public static final String AddCPEforIntermediateEquip_adddevicelink = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Add Device']";
		public static final String AddCPEforIntermediateEquip_Name = "@xpath=//input[@id='deviceName']";
		public static final String AddCPEforIntermediateEquip_selectdevicetogglebutton = "@xpath=(//div[@class='react-switch'])[1]";
		public static final String AddCPEforIntermediateEquip_vender = "@xpath=//div[label[text()='Vender/Model']]//input";
		public static final String AddCPEforIntermediateEquip_snmpro = "@xpath=//input[@id='snmpro']";
		public static final String AddCPEforIntermediateEquip_manageaddress = "@xpath=//input[@id='managementAddress']";
		public static final String AddCPEforIntermediateEquip_selectSubnettogglebutton = "@xpath=(//div[@class='react-switch'])[2]";
		public static final String AddCPEforIntermediateEquip_mepid = "@xpath=//input[@id='mePId']";
		public static final String AddCPEforIntermediateEquip_poweralarm = "@xpath=//div[label[text()='Power Alarm']]//input";
		public static final String AddCPEforIntermediateEquip_mediaselection = "@xpath=//div[label[text()='Media Selection']]//input";
		public static final String AddCPEforIntermediateEquip_macaddress = "@xpath=//input[@id='macAddress']";
		public static final String AddCPEforIntermediateEquip_serialnumber = "@xpath=//input[@id='serialNumber']";
		public static final String AddCPEforIntermediateEquip_hexaserialnumber = "@xpath=//input[@id='hexaSerialNumber']";
		public static final String AddCPEforIntermediateEquip_linklostforowarding = "@xpath=//input[@id='linkLostForwarding']";
		public static final String AddCPEforIntermediateEquip_country = "@xpath=//select[@id='country']";
		public static final String AddCPEforIntermediateEquip_countrydiv = "@xpath=//div[label[text()='Country']]//input";
		public static final String countrylabelname_IntEquipment = "@xpath=//label[text()='Country']";
		public static final String AddCPEforIntermediateEquip__city = "@xpath=//select[@id='city']";
		public static final String AddCPEforIntermediateEquip__citydiv = "@xpath=//div[label[text()='City']]//input";
		public static final String AddCPEforIntermediateEquip_citytogglebutton = "@xpath=(//div[@class='react-switch'])[3]";
		public static final String AddCPEforIntermediateEquip_disabledCityToggleButton = "@xpath=(//div[@class='react-switch-bg'])[4]";
		public static final String AddCPEforIntermediateEquip_site = "@xpath=//select[@id='site']";
		public static final String AddCPEforIntermediateEquip_sitediv = "@xpath=//div[label[text()='Site']]//input";
		public static final String AddCPEforIntermediateEquip_sitetogglebutton = "@xpath=(//div[@class='react-switch'])[5]";
		public static final String AddCPEforIntermediateEquip_premise = "@xpath=//select[@id='premise']";
		public static final String AddCPEforIntermediateEquip_premisediv = "@xpath=//div[label[text()='Premise']]//input";
		public static final String AddCPEforIntermediateEquip_premisetogglebutton = "@xpath=(//div[@class='react-switch'])[7]";
		public static final String AddCPEforIntermediateEquip_citynamefield = "@xpath=//input[@id='cityName']";
		public static final String AddCPEforIntermediateEquip_citycodefield = "@xpath=//input[@id='cityCode']";
		public static final String AddCPEforIntermediateEquip_sitenamefield = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String AddCPEforIntermediateEquip_sitecodefield = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String AddCPEforIntermediateEquip_premisenamefield = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";
		public static final String AddCPEforIntermediateEquip_premisecodefield = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";

		public static final String AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";
		public static final String AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";

		public static final String AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";
		public static final String AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";

		public static final String deletefromservicelink_IntermediateEquipment = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='Delete from Service']";
		public static final String viewCPEforIntermediateEquiplink = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='View']";
		public static final String viewCPEforIntermediateEquip_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_mepid = "@xpath=//div[div[label[contains(text(),'MEP Id')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_poweralarm = "@xpath=//div[div[label[contains(text(),'Power Alarm')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_mediaselection = "@xpath=//div[div[label[contains(text(),'Media Selection')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_linklostforwarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_macaddress = "@xpath=//div[div[label[contains(text(),'MAC Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_serialnumber = "@xpath=//div[div[label[text()='Serial Number']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_hexaserialnumber = "@xpath=//div[div[label[contains(text(),'Hexa Serial Number')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_country = "@xpath=//div[div[label[text()='Country']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_city = "@xpath=//div[div[label[text()='City']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_site = "@xpath=//div[div[label[text()='Site']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_premise = "@xpath=//div[div[label[text()='Premise']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_Actiondropdown = "@xpath=(//div[@class='heading-green row'][1]//button[@id='dropdown-basic-button'])[1]";
		public static final String EditCPEdeviceforIntermediateEquipmentlinkunderviewpage = "@xpath=//a[text()='Edit']";

		public static final String EditCPEdeviceforIntermediateEquipmentlink = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='Edit']";
		public static final String showInterface_ForIntermediateEquipment = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Show Interfaces']";
		public static final String HideInterface_ForIntermediateEquipment = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Hide Interfaces']";

		// Fetch Dveice Interface
		public static final String fetchDeviceinterfacelink_viewDevicePage = "@xpath=//a[text()='Fetch Device Interfaces']";
		public static final String succesMessageForfetchDeviceInterfcae = "@xpath=//div[contains(text(),'Fetch Interfaces started successfully. Please chec')]";
		public static final String clickherelink_fetchDeviceinterfcae = "@xpath=//a[@class='customLink heading-green-1']";

		public static final String ManageNetworkHeaderName = "@xpath=//div[div[contains(text(),'Manage Network')]]";

		// xpath for table

		public static final String configure_totalPage = "@xpath=//span[@ref='lbTotal']";
		public static final String configure_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String Actelisconfig_addDSLAM = "@xpath=//div[div[div[text()='Actelis Configuration']]]//a[text()='Add DSLAM and HSL']";
		public static final String DSLM_Device_Select = "@xpath=//div[div[div[label[@class='form-label']]]]//input";
		public static final String List_HSL_Link = "@xpath=//span[contains(text(),'List HSL')]";
		public static final String ActelisConfigurationPanel = "@xpath=//div[div[contains(text(),'Actelis Configuration')]]";

		public static final String ActiondropdownforEditLink_IntermediateEquipmentforcpedeviec = "@xpath=(//div[div[text()='View CPE Device']]//button[text()='Action'])[2]";

		public static final String AddPElink_LANlinkoutband = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Add Device']";
		public static final String ProviderEquipment_showInterfacelink = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Show Interfaces']";

		public static final String SelectIMSlocation = "@xpath=(//select[@id='imsPOP'])[1]";
		public static final String selectdevice_ToggleButton = "@xpath=(//div[@class='div-margin row']//div[@class='react-switch-bg'])[1]";
		public static final String Name = "@xpath=//input[@id='name']";
		public static final String VenderModel = "@xpath=//select[@id='vender']";
		public static final String managementaddress = "@xpath=//input[@id='address']";
		public static final String Snmpro = "@xpath=//input[@id='snmpro']";
		public static final String country = "@xpath=//select[@id='country']";
		public static final String city = "@xpath=//select[@id='city']";
		public static final String site = "@xpath=//select[@id='site']";
		public static final String premise = "@xpath=//select[@id='premise']";

		public static final String IntermediateEquipment_OKbuttonforpopup = "@xpath=//button[@class='btn btn-secondary']";

		public static final String verifyPEname_lanlinkoutband = "@xpath=(//div[div[label[text()='Name']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEvendor_lanlinkoutband = "@xpath=(//div[div[label[text()='Vendor/Model']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEaddress_lanlinkoutband = "@xpath=(//div[div[label[text()='Management Address']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEsnmpro_lanlinkoutband = "@xpath=(//div[div[label[text()='Snmpro']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEcountry_lanlinkoutband = "@xpath=(//div[div[label[text()='Country']]])[1]//div[@class='customLabelValue form-label']";
		public static final String verifyPECity_lanlinkoutband = "@xpath=(//div[div[label[text()='City']]])[1]//div[@class='customLabelValue form-label']";
		public static final String verifyPESite_lanlinkoutband = "@xpath=(//div[div[label[text()='Site']]])[1]//div[@class='customLabelValue form-label']";
		public static final String verifyPEPremise_lanlinkoutband = "@xpath=(//div[div[label[text()='Premise']]])[1]//div[@class='customLabelValue form-label']";

		public static final String verifyIMSlocation = "@xpath=";
		public static final String Providerequipment_viewlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='View'])[3]";
		public static final String Providerequipment_Editlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Edit'])[2]";
		public static final String Providerequipment_selectinterface = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Select Interfaces'])[2]";
		public static final String Providerequipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Configure'])[2]";
		public static final String Providerequipment_deletefromservice = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Delete from Service'])[3]";
		public static final String Providerequipment_ShowInterface = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Show Interfaces']";
		public static final String providerEquipment_actiondropdownfromviewpage = "@xpath=(//button[text()='Action'])[2]";
		public static final String providerEquipment_Editlinkunderviewdevicepage = "@xpath=//a[text()='Edit']";
		public static final String providerEquipment_showinterfaceTatoalpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//span[@ref='lbTotal']";
		public static final String providerEquipment_showinterfaceCurrentpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//span[@ref='lbCurrent']";
		public static final String providerEquipment_showinterfaceNextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//button[@ref='btNext']";

		public static final String ProviderEquipment_Generateconfigforstaticlink = "@xpath=//a[text()='Generate Configuration for Static Routes']";
		public static final String providerEquipment_saveconfigurationlink = "@xpath=//a[text()='Save Configuration']";
		public static final String providerEquipment_ExecuteconfigurationOndevicelink = "@xpath=//a[text()='Execute Configuration on Device']";

		public static final String providerEquipment_currentpageInterfaceToselect = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbCurrent'])[2]";
		public static final String providerEquipment_InterfaceToselectTotalpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbTotal'])[2]";
		public static final String providerEquipment_InterfaceToselectActiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String providerEquipment_InterfaceToselectAddbutton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";
		public static final String providerEquipment_InterfaceToselectnextpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//button[@ref='btNext'])[2]";

		public static final String providerEquipment_currentpageInterfaceInselect = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbCurrent'])[1]";
		public static final String providerEquipment_InterfaceInselectTotalpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbTotal'])[1]";
		public static final String providerEquipment_InterfaceInselectActiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String providerEquipment_InterfaceInselectRemovebutton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String providerEquipment_InterfaceInselectnextpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//button[@ref='btNext'])[1]";

		public static final String customerpremiseEquipment_viewlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='View'])[1]";
		public static final String customerpremiseequipment_Editlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Edit'])[1]";
		public static final String cusomerpremiseequipment_SelectInertafeceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Select Interfaces'])[1]";
		public static final String customerpremiseequipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Configure'])[1]";
		public static final String customerpremiseEquipment_FetchdeviceInterfacelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";
		public static final String customerpremiseEquipment_PPPconfigurationlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";
		public static final String customerpremiseEquipment_deletefromservceilink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";
		public static final String customerpremiseEquipment_Adddevicelink = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";
		public static final String customerpremiseEQuipment_showinetrfacelink = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Show Interfaces']";
		public static final String customerpremiseequipment_showinterfaceTotalpage = "@xpath=";
		public static final String customerpremiseequipment_showinterfaceCurrentpage = "@xpath=";
		public static final String customerpremiseequipment_showinterfaceActiondropdown = "@xpath=";
		public static final String customerpremiseequipment_showinterfaceEditlink = "@xpath=";

		public static final String AddCPElink_LANlinkoutband = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";

		public static final String Actionbutton_PE2CPE = "@xpath=(//div[@class='heading-green div-margin row']//button[text()='Action'])[1]";
		public static final String Addnewlink_Pe2CPE = "@xpath=//a[text()='Add New Link']";

		public static final String AddnewlinkPe2CPE_LinkorCircuitId = "@xpath=//input[@id='linkCircuitId']";
		public static final String AddnewlinkPe2CPE_SourceDevice = "@xpath=//select[@id='fromDeviceName']";
		public static final String AddnewlinkPe2CPE_SourceInterface = "@xpath=//select[@id='fromInterfaceName']";
		public static final String AddnewlinkPe2CPE_TargetDevice = "@xpath=//select[@id='toDeviceName']";
		public static final String AddnewlinkPe2CPE_TargetInterface = "@xpath=//select[@id='toInterfaceName']";
		public static final String AddnewlinkPe2CPE_Nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String Headername_PE2CPElink = "@xpath=//div[@class='modal-title h4']";

		public static final String Pe2CPElinktable_totalpage = "@xpath=//div[@class='ag-div-margin row']//span[@ref='lbTotal']";
		public static final String Pe2CPElinktable_currentpage = "@xpath=//div[@class='ag-div-margin row']//span[@ref='lbCurrent']";
		public static final String Pe2CPElinktable_nextpage = "@xpath=//div[@class='ag-div-margin row']//button[@ref='btNext']";

		// Copy from here
		public static final String intermediateTechErrorMessage = "@xpath=//button[@id='id1']//*[contains(@class,'svg-inline--fa fa-info-circle fa-w-16')]";
		public static final String IntermediaTechnologypopup = "@xpath=//div[@class='popover-body']";
		public static final String circuitreferenceIalert = "@xpath=//button[@id='id2']";
		public static final String circuitreferencealertmessage = "@xpath=//div[@class='modal-body']";
		public static final String circuitreferencealerterrmsg = "@xpath=//div[@class='popover-body']";
		public static final String managementconnection = "@xpath=//div[label[contains(text(),'Management Connection')]]//input";

		public static final String Equipment_selectInterfaceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Select Interfaces'])[2]";
		public static final String InterfaceToselect = "@xpath=(//div[@class='row'])[7]";
		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";

		public static final String InterfaceInslect = "@xpath=(//div[@class='row'])[6]";
		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String findlistofInterfacesNames = "@xpath=//div[@class='ag-cell ag-cell-not-inline-editing ag-cell-with-height ag-cell-no-focus ag-cell-value'][1]";

		public static final String IntermediateEquipment_selectInterfaceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Intermediate Equipment']]//span[text()='Select Interfaces'])[2]";

		public static final String configure_alertPopup = "@xpath=//div[text()='Alert']";
		public static final String configure_alertMessage = "@xpath=//div[@class='modal-body']";

		public static final String IntermediateEquipment_Configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Intermediate Equipment']]//span[text()='Configure'])[2]";
		public static final String Equipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Configure'])[2]";
		public static final String Equipment_configureActiondropdown = "@xpath=//div[div[text()='Interfaces']]//button[text()='Action']";
		public static final String Equipment_configureEditlink = "@xpath=//a[text()='Edit']";
		public static final String Equipment_showintefaceedit = "@xpath=(//a[text()='Edit'])[1]";
		public static final String Equipment_configureEditInterfaceTitle = "@xpath=//div[@class='modal-title h4']";
		public static final String Equipment_configureXNGCircuitID = "@xpath=(//div[label[text()='XNG Circuit Id']])[1]//input";
		public static final String selectCircuit_togglebutton = "@xpath=//div[@class='react-switch-bg']";
		public static final String Equipment_configureBearerType = "@xpath=//div[label[text()='Bearer Type']]//input";
		public static final String Equipment_configureBearerSpeed = "@xpath=//div[label[text()='Bearer Speed']]//input";
		public static final String Equipment_configureTotalcircuitbandwidth = "@xpath=//div[label[text()='Total Circuit Bandwidth']]//input";
		public static final String Equipment_configureVLANid = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String Equipment_configureVlanType = "@xpath=//div[label[text()='VLAN Type']]//input";

		public static final String Equipment_showInterfaceActiondropdown = "@xpath=(//button[text()='Action'])[2]";
		public static final String Equipment_showInterfaceTotalPages = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[3]//span[@ref='lbTotal']";
		public static final String Currentpageforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbCurrent']";

		public static final String IntermediateEquipment_Actiondropdown = "@xpath=(//button[text()='Action'])[4]";
		public static final String IntermediateEquipment_Editlink = "@xpath=(//a[text()='Edit'])[3]";

		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";
		public static final String Editservice_actiondropdownalternate = "@xpath=(//button[@id='dropdown-basic-button'])[4]";

		public static final String Editservice_Editlink = "@xpath=//div[div[text()='Service']]//a[contains(text(),'Edit')]";

		public static final String Editservice_Deletelink = "@xpath=//a[text()='Delete']";

		public static final String Editservice_sysnchronizelink = "@xpath=//a[text()='Synchronize']";

		public static final String Editservice_infovistareport = "@xpath=//a[text()='Show New Infovista Report']";

		public static final String Editservice_managelink = "@xpath=//a[text()='Manage']";

		public static final String Editservice_managesubnets = "@xpath=//a[text()='Manage Subnets']";

		public static final String Editservice_AMNvalidator = "@xpath=//a[text()='AMN Validator']";

		public static final String Editservice_Dumplink = "@xpath=//a[text()='Dump']";

		public static final String viewServicepage_Servicepanel = "@xpath=//div[contains(text(),'Service')]";
		public static final String viewServicepage_ManagementOptionsPanel = "@xpath=//div[contains(text(),'Management Options')]";
		public static final String viewServicepage_ConfigurationoptionsPanel = "@xpath=//div[contains(text(),'Configuration Options')]";
		public static final String viewServicepage_SiteORderPanel = "@xpath=//div[contains(text(),'Site Orders')]";
		public static final String viewServicepage_OrderPanel = "@xpath=//div[text()='Order']";

		// Outband Management

		public static final String Addsiteorder_siteordernumberfield = "@xpath=//div[label[contains(text(),'Site Order Number')]]//input";
		public static final String Addsiteorder_primarysiteorder = "@xpath=//div[label[contains(text(),'Primary Site Order')]]//input";
		public static final String Addsiteorder_perCoSperformncereport = "@xpath=//div[label[contains(text(),'Per CoS Performance Reporting')]]//input";
		public static final String AddSiteorder_IVreferencedropdown = "@xpath=//div[label[contains(text(),'IV Reference')]]//input";
		public static final String Addsiteorder_RouterconfigviewIpv4 = "@xpath=//div[label[contains(text(),'Router Configuration View IPv4')]]//input";
		public static final String Addsiteorder_wholesaleProvider = "@xpath=//div[label[contains(text(),'Wholesale Provider')]]//input";
		public static final String Addsiteorder_managedsite = "@xpath=//div[label[contains(text(),'Managed Site')]]//input";
		public static final String Addsiteorder_priority = "@xpath=//div[label[contains(text(),'Priority')]]//input";
		public static final String Addsiteorder_actelisbasedcheckbox = "@xpath=//div[label[contains(text(),'Actelis Based')]]//input";
		public static final String Addsiteorder_voipcheckbox = "@xpath=//div[label[contains(text(),'VoIP')]]//input";

		public static final String verifysiteOrder_perCoSperformReporting = "@xpath=//div[div[label[contains(text(),'Per CoS Performance Reporting')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_primarysiteorder = "@xpath=//div[div[label[contains(text(),'Primary Site Order')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_IVreference = "@xpath=//div[div[label[contains(text(),'IV Reference')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_priority = "@xpath=//div[div[label[contains(text(),'Priority')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_managedSite = "@xpath=//div[div[label[contains(text(),'Managed Site')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_RouterConfigviewIPv4 = "@xpath=//div[div[label[contains(text(),'Router Configuration View IPv4')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_actelisBased = "@xpath=//div[div[label[contains(text(),'Actelis Based')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_wholesaleProvider = "@xpath=//div[div[label[contains(text(),'Wholesale Provider')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_VoIP = "@xpath=//div[div[label[contains(text(),'VoIP')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_providerEquipment_addDevice = "@xpath=//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a[text()='Add Device']";
		public static final String outband_providerEquipment_addDevice_chooseAdevicedrodown = "@xpath=//div[label[@class='form-label labelStyle']]//input";
		public static final String outband_providerEquipment_addDevice_venderModel = "@xpath=//div[label[contains(text(),'Vendor/Model')]]//input";
		public static final String outband_providerEquipment_addDevice_managenmetaddress = "@xpath=//div[label[contains(text(),'Management Address')]]//input";
		public static final String outband_providerEquipment_addDevice_snmpro = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";
		public static final String outband_providerEquipment_addDevice_country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String outband_providerEquipment_addDevice_city = "@xpath=//div[label[contains(text(),'City')]]//input";
		public static final String outband_providerEquipment_addDevice_site = "@xpath=//div[label[contains(text(),'Site')]]//input";
		public static final String outband_providerEquipment_addDevice_premise = "@xpath=//div[label[contains(text(),'Premise')]]//input";

		public static final String outband_customerpremiseEquipment_addDevicelink = "@xpath=//div[div[div[contains(text(),'Customer Premise Equipment (CPE)')]]]//a[text()='Add Device']";
		public static final String outband_customerpremiseEquipment_addDevice_routerIderrmsg = "@xpath=//div[text()='Router Id']";
		public static final String outband_customerpremiseEquipment_addDevice_nameErrmsg = "@xpath=//div[text()='Name']";
		public static final String outband_customerpremiseEquipment_addDevice_vendorErrmsg = "@xpath=//div[text()='Vendor/Model']";
		public static final String outband_customerpremiseEquipment_addDevice_managementAddressErrmsg = "@xpath=//div[text()='Management Address']";
		public static final String outband_customerpremiseEquipment_addDevice_snmproErrmsg = "@xpath=//div[text()='Snmpro']";
		public static final String outband_customerpremiseEquipment_addDevice_routerIdField = "@xpath=//div[label[contains(text(),'Router Id')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_deviceNameField = "@xpath=//div[label[contains(text(),'Device Name')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_vendorModelField = "@xpath=//div[label[contains(text(),'Vendor/Model')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_manageAddresField = "@xpath=//div[label[contains(text(),'Management Address')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmproField = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmprwField = "@xpath=//div[label[contains(text(),'Snmprw')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3ContextnameField = "@xpath=//div[label[contains(text(),'Snmp V3 Context Name')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3ContextEngineIdField = "@xpath=//div[label[contains(text(),'Snmp V3 Context Engine ID')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3SecurityUsernameField = "@xpath=//div[label[contains(text(),'Snmp V3 Security Username')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3authprotoDropdown = "@xpath=//div[label[contains(text(),'Snmp V3 Auth Proto')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3authPasswordField = "@xpath=//div[label[contains(text(),'Snmp V3 Auth Password')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_premisedropdown = "@xpath=//div[label[text()='Premise']]//input";
		public static final String outband_customerpremiseEquipment_addDevice_selectpremiseToggleButton = "@xpath=(//div[div[label[text()='Select Premise']]]//div[@class='react-switch-handle'])[1]";
		public static final String outband_customerpremiseEquipment_addDevice_premisenameField = "@xpath=//div[label[contains(text(),'Premise Name')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_premiseCodeField = "@xpath=//div[label[contains(text(),'Premise Code')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_networkDropdown = "@xpath=";

		public static final String outband_customerpremiseEquipment_viewDevice_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_country = "@xpath=//div[div[label[contains(text(),'Country')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_city = "@xpath=//div[div[label[contains(text(),'City')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_site = "@xpath=//div[div[label[contains(text(),'Site')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_Premise = "@xpath=//div[div[label[contains(text(),'Premise')]]]//div[@class='customLabelValue form-label']";

		// For Interface speed 10GigE
		public static final String circuit_10GigeActiondropdown = "@xpath=//div[@class='dropdwon-actionBtn-right show dropdown']//button[@id='dropdown-basic-button']";
		public static final String Circuit_10GigEaddOverturelink = "@xpath=//a[contains(text(),'Add Overture')]";
		public static final String Circuit_10GigEconfigurelink = "@xpath=//a[contains(text(),'Configure')]";
		public static final String Circuit_10GigEPAMtest = "@xpath=//a[contains(text(),'PAM Test')]";
		public static final String Circuit_10GigEdeletelink = "@xpath=//a[contains(text(),'Delete')]";

		public static final String Overture_servicenameErrmsg = "@xpath=//div[text()='Service Name']";
		public static final String Overture_ServiceNameField = "@xpath=//label[text()='Service Name']";
		public static final String Overture_searchButton = "@xpath=//span[contains(text(),'Search')]";

		// Router Tool panel

		public static final String outband_customerpremiseEquipment_viewDevicePage_RouterToolPanel_commandIPv4 = "@xpath=//div[label[text()='Command IPvp4']]//input";
		public static final String outband_customerpremiseEquipment_viewDevicePage_RouterToolPanel_execute = "@xpath=//a[contains(text(),'Execute')]";

		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String order_contractnumberErrmsg = "@xpath=(//span[text()='Order/Contract number is required'])[1]";
		public static final String servicetypeerrmsg = "@xpath=//span[contains(text(),'Service type is required')]";

		public static final String interfaceSpeedErrmsg = "@xpath=//span[contains(text(),'Interface Speed is required')]";
		public static final String servicesubtypeErrMsg = "@xpath=//span[contains(text(),'Sub service type is required')]";

		public static final String serviceIdentificationerrmsg = "@xpath=//div[text()='Service Identification']";
		public static final String circuitreferenceErrmsg = "@xpath=//div[text()='Circuit Reference']";

		// 10May
		public static final String EquipementConfigurationPanel = "@xpath=//div[div[contains(text(),'Equipment Configuration')]]";
		public static final String equipConfig_addCPEdevice = "@xpath=//div[div[contains(text(),'Equipment Configuration')]]/following-sibling::div//a";
		public static final String addActelisCPEpage_headerName = "@xpath=//div[div[p[text()='Add Actelis CPE']]]";

		public static final String nameField_addCPE_Actelis = "@xpath=//div[label[text()='Name']]//input";
		public static final String vendorField_addCPE_Actelis = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String RouterIdField_addCPE_Actelis = "@xpath=//div[label[text()='Router Id']]//input";
		public static final String managementAddressField_addCPE_Actelis = "@xpath=//div[label[text()='Management Address']]//input";
		public static final String MEPidField_addCPE_Actelis = "@xpath=//div[label[text()='MEP Id']]//input";
		public static final String ETHportField_addCPE_Actelis = "@xpath=//div[label[text()='ETH Port']]//input";

		public static final String devicenameFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Device Name']";
		public static final String RouterIDFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Router Id']";
		public static final String manageAddressFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Management Address']";

		public static final String fetchInterface_AddDSLAMdevice = "@xpath=//span[contains(text(),'Fetched interfaces successfully')]";

		public static final String InterfaceToSelect_actelis_totalpage = "@xpath=//span[@ref='lbTotal']";
		public static final String InterfaceToSelect_actelis_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String alertForSynchronize = "@xpath=//div[@role='alert']";
		public static final String alertMSG_synchronize = "@xpath=//div[@role='alert']/span";

		public static final String showInterface_ActelisCnfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//a[text()='Show Interfaces']";
		public static final String AcionButton_ActelisConfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//button[text()='Action']";
		public static final String removeButton_ActelisConfiguration = "@xpath=//a[text()='Remove']";
		public static final String popupMessage_forRemove_ActelisConfiguration = "@xpath=//div[@class='modal-body']";
		public static final String deleteInterface_successMessage_ActelisCOnfiguration = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";
		public static final String successMessage_ActelisConfiguration_removeInterface = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		// 9 june
		public static final String warningmEssage1_devicename = "@xpath=(//span/li)[1]";
		public static final String warningmEssage2_devicename = "@xpath=(//span/li)[2]";
		public static final String warningmEssage3_devicename = "@xpath=(//span/li)[3]";

		public static final String changeButton_managementAddress = "@xpath=//span[contains(text(),'Change')]";

		public static final String manageSubnetPage_header = "@xpath=(//div[div[text()='Manage Subnets']])[1]";
		public static final String manageSubnet_errMsg = "@xpath=//div[@class='msg-error']/span";

		public static final String dumpPage_header = "@xpath=//div[@class='modal-header']";
		public static final String dumpheaderName = "@xpath=//div[@class='modal-header']/div";
		public static final String dumpMessage_body = "@xpath=//div[@class='modal-body']/div";
		public static final String dump_xButton = "@xpath=//span[contains(text(),'�')]";
		public static final String dump_container = "@xpath=(//div[div[@class='container']]/div)[2]";

		// Edit device
		public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String citydropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//input";
		public static final String sitedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//input";
		public static final String premisedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//input";
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";
		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		// view page
		public static final String standardHeaderName_viewPage = "@xpath=(//tr)[2]/th/label";
		public static final String standardCIRvalue_viewPage = "@xpath=(//label[text()='Standard']/parent::th/following-sibling::td/div)[1]";
		public static final String standardEIRvalue_viewPage = "@xpath=(//label[text()='Standard']/parent::th/following-sibling::td/div)[2]";

		public static final String PremisumHeaderName_viewPage = "@xpath=(//tr)[3]/th/label";
		public static final String premiumCIRvalue_viewPage = "@xpath=(//label[text()='Premium']/parent::th/following-sibling::td/div)[1]";
		public static final String premiumEIRvalue_viewPage = "@xpath=(//label[text()='Premium']/parent::th/following-sibling::td/div)[2]";

		public static final String labelName_searialnumber = "@xpath=//label[text()='Serial Number']";
		public static final String labelname_managementAddresss = "@xpath=//label[text()='Management Address']";

		// Router Tool
		public static final String commandIPV4_dropdown = "@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String executebutton_Ipv4 = "@xpath=(//span[text()='Execute'])[1]";
		public static final String result_textArea = "@xpath=//div[label[text()='Result']]//textarea";

		// Fetch device Interface
		public static final String ClickhereLink_fetchInterface = "@xpath=//a[text()='here']";
		public static final String successMessage_Alert = "@xpath=//div[@role='alert']//span";
		public static final String succesMsg_alertDisplay = "@xpath=//div[@role='alert']";

		public static final String CPEdevice_hideInterfaeLink_eqiupment = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Equipment']]]//a[text()='Hide Interfaces']";
		public static final String CPEdevoce_hideInterfacelink_intEquip = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Intermediate Equipment']]]//a[text()='Hide Interfaces']";

		// @class='fade alert-style alert alert-danger show'
		// breadcrump common code
		public static final String breadcrump = "@xpath=//a[text()='value']";

		public static final String fetchVendorModelvalue = "@xpath=//div[div[label[text()='Vendor/Model']]]//div[@class='customLabelValue form-label']";
		public static final String fetchDeviceValue = "@xpath=//div[div[label[text()='Name']]]//div[@class='customLabelValue form-label']";
		public static final String fetchmanagementAddressvalue = "@xpath=//div[div[label[text()='Management Address']]]//div[@class='customLabelValue form-label']";
		public static final String fetchCountryValue = "@xpath=//div[div[label[text()='Country']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevicepage_devices = "@xpath=//div[div[text()='View CPE Device']] ";

		public static final String devicePanelHeaders_InViewSiteOrderPage = "@xpath=//div[div[text()='value']]";
		public static final String devicePanelHeaders_InViewSiteOrderPage1 = "@xpath=//div[div[text()='Equipment']]";

		// PAM Test
		public static final String PAMtest_Link = "@xpath=//a[contains(text(),'PAM Test')]";
		public static final String PAMtest_popupPage = "@xpath=//div[div[div[text()='PAM Test']]]";
		public static final String PAMTest_TypeFieldValue = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Type']]//following-sibling::div";
		public static final String PAMTest_ServiceValue = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Service']]//following-sibling::div";
		public static final String PAMTest_ToolResponse = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Tool Response']]//following-sibling::div";

		// Manage Service
		public static final String viewServicePage_fetchServiceId = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//following-sibling::div[@class='customLabelValue form-label']";
		public static final String viewServicePage_orderNumber = "@xpath=//div[div[label[contains(text(),'Order/Contract Number (Parent SID)')]]]//following-sibling::div[@class='customLabelValue form-label']";
		public static final String ManageServicesPage = "@xpath=//div[div[contains(text(),'Manage Services')]]";
		public static final String status_columnNames = "@xpath=//div[div[text()='Status']]//following-sibling::div//label";
		public static final String statusPanel_serviceId = "@xpath=((//div[div[label[text()='Service']]])[1]//following-sibling::div//a)[1]";
		public static final String statusPanel_serviceType = "@xpath=((//div[div[label[text()='Service Type']]])[1]//following-sibling::div//div[@class='col-12 col-sm-12 col-md-2'])[2]";
		public static final String statusPanel_statusColumn = "@xpath=((//div[div[label[text()='Service Type']]])[1]//following-sibling::div//div[@class='col-12 col-sm-12 col-md-2'])[4]";
		public static final String statusPanel_detailColumn = "@xpath=((//div[div[label[text()='Service Type']]])[1]//following-sibling::div//div[@class='col-12 col-sm-12 col-md-2'])[3]";
		public static final String statusPanel_lastModificationColumn = "@xpath=((//div[div[text()='Status']]//following-sibling::div)[3]/div)[5]";

		// AMN Validator
		public static final String AMNvalidator_panelHeader = "@xpath=//div[div[text()='AMN Validator Results']]";
		public static final String AMNvalidator_status_columnHeader = "@xpath=//div[div[text()='Site Orders']]//following-sibling::div//span[@role='columnheader']";
		public static final String AMNvalidator_device_columnHeader = "@xpath=//div[div[text()='Devices For Service']]//following-sibling::div//span[@role='columnheader']";
		public static final String AMNvalidator_statusPanel_siteOrderColumnValue = "@xpath=//div[@col-id='groupName'][@role='gridcell']";
		public static final String AMNvalidator_statusPanel_CSRsiteNameColumnValue = "@xpath=//div[@col-id='siteName'][@role='gridcell']";
		public static final String AMNvalidator_statusPanel_deviceXngCityName = "@xpath=//div[@col-id='cityName'][@role='gridcell']";
		public static final String AMNvalidator_statusPanel_countryName = "@xpath=//div[@col-id='countryName'][@role='gridcell']";
		public static final String AMNvalidator_devicePanel_deviceName = "@xpath=//div[div[text()='Devices For Service']]//following-sibling::div//div[@class='ag-react-container']/a";
		public static final String physicalSiteCSRName_viewSiteOrder = "@xpath=//div[label[text()='Physical Site: CSR Name']]//following-sibling::div[@class='customLabelValue form-label']";
		public static final String deviceCityName_viewSiteOrder = "@xpath=//div[label[text()='Device Xng City']]//following-sibling::div[@class='customLabelValue form-label']";
		public static final String countryName_viewSiteOrder = "@xpath=//div[label[text()='Device Country']]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String siteOrder_ActionButton = "@xpath=//div[div[text()='Site Order']]//button[text()='Action']";
		public static final String AMNvalidatorlink = "@xpath=//a[text()='AMN Validator']";

		public static final String fetchDeviceInterace_SuccessMessage = "@xpath=(//div[div[@class='alert alert-success alert-Style col-12 col-sm-12 col-md-12']])[1]";
		public static final String fetchdeviceInterface_successtextMessage = "@xpath=(//div[@class='alert alert-success alert-Style col-12 col-sm-12 col-md-12'])[1]";

		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[@class='btn btn-danger']";
		public static final String deleteMessage = "@xpath=//div[@class='modal-body']//div[@class='col-12 col-sm-12 col-md-12']";
		public static final String deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";

		public static final String deleteLink_common = "@xpath=//a[text()='Delete']";

		public static final String existingDevice_SelectDeviceToggleButton = "@xpath=//div[div[label[text()='Select Device']]]//div[@class='react-switch']";
		public static final String chooseAdeviceDropdown = "@xpath=//div[label[text()='Choose a Device']]//input";

		public static final String remark_viewPage = "@xpath=//label[text()='Remark']/parent::div//div";

		// select tag Dropdown
		public static final String countryDropdown_selectTag = "@xpath=//label[text()='Country']/parent::div//select";
		public static final String cityDropdown_selectTag = "@xpath=//label[text()='City']/parent::div//select";
		public static final String siteDropdown_selectTag = "@xpath=//label[text()='Site']/parent::div//select";
		public static final String premiseDropdown_selectTag = "@xpath=//label[text()='Premise']/parent::div//select";

		public static final String actelis_EquipConfig_viewLink1 = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='View']";
		public static final String actelis_EquipConfig_deleteLink1 = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='Delete']";

		public static final String addDSLAMandHSL_xButton1 = "@xpath=//div[div[label[text()='DSLAM Device:']]]//following-sibling::div//div[text()='�']";
		public static final String selectDSLAMdeviceValue1 = "@xpath=//div[text()='value']";

		// Circuit Creation
		public static final String addOvertureLink1 = "@xpath=(//span[text()='Add Overture'])[1]";
		public static final String addCircuit_AccedianLink1 = "@xpath=//span[text()='Add Accedian-1G']";
		public static final String addCircuit_AtricaLink1 = "@xpath=//span[contains(text(),'Add Circuit')]";

		public static final String addOverture_OKbutton1 = "@xpath=//span[contains(text(),'OK')]";
		public static final String addOverture_overturePanel1 = "@xpath=//div[contains(text(),'Select Overture Circuit')]";
		public static final String addCircuit_Accedian1Gpage_panel1 = "@xpath=//div[contains(text(),'Select Accedian-1G Circuit')]";
		public static final String addCircuit_Atricapage_Panel1 = "@xpath=//div[text()='Select Atrica Circuit']";

		public static final String addOverture_serviceNameTextField1 = "@xpath=//label[text()='Service Name']/parent::div//input";
		public static final String addOverture_searchButton1 = "@xpath=//span[text()='Search']";
		public static final String selectValueUnderAddOverturePage1 = "@xpath=(//div[text()='value'])[1]";
		public static final String addOverture_cancelButton1 = "@xpath=//span[text()='Cancel']";

		public static final String addOverture_interfaceInServicePanel1 = "@xpath=//div[label[text()='Interfaces In Service']]";
		public static final String interfaceFilterButton1 = "@xpath=//div[span[text()='Interface']]/preceding-sibling::span";
		public static final String interfacePage_filterText1 = "@xpath=//input[@id='filterText']";
		public static final String interfaceInService_selectValueUnderTable1 = "@xpath=(//div[text()='value']/parent::div//input)[1]";
		public static final String interfaceinService_selectEdgePointforInterface1 = "@xpath=(//div[text()='value']/parent::div//input)[2]";

		public static final String PAMtest_selectinterface1 = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//div[text()='value']";
		public static final String PAMtest_actionDropdown1 = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//button[text()='Action']";
		public static final String PAMTest_siteValue1 = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Site']]//following-sibling::div";
		public static final String PAMTest_deleteButton1 = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//span[text()='Delete']";
		public static final String deleteCircuit_deleteButton1 = "@xpath=//button[@class='btn btn-danger']";
		public static final String fetchProActiveMonitoringValue = "@xpath=//div[label[text()='Proactive Monitoring']]//following-sibling::div";
		public static final String addSiteOrderpanelHeader = "@xpath=//p[text()='Add Site Order']";
		public static final String configure_alertPopup_xbutton1 = "@xpath=//span[text()='�']";

		public static final String allocateSubnetButton = "@xpath=//span[contains(text(),'Allocate Subnet')]";
		public static final String EIPallocation_xButton = "@xpath=//span[contains(text(),'�')]";
		public static final String addMultilink_dslDownstreamSpeed = "@xpath=//div[label[text()='DSL Downstream Speed (PCR)']]//input";
		public static final String addmultilink_dslUpstreamSpeed_out = "@xpath=//div[label[text()='DSL Upstream Speed (SCR)']]//input";

		public static final String fetchInterfaceAddressRangeDropdown_addInterfaceIPv6 = "@xpath=(//div[label[text()='Interface Address Range']]//span)[2]";
		public static final String FetchInterfaceDropdown_addInterface = "@xpath=(//div[label[text()='Address']])//input";
		public static final String interfaceAddressRange_textField = "@xpath=(//div[label[text()='Interface Address Range']]//input)[1]";
		public static final String interfaceAddressRangeIPv6_textField = "@xpath=(//div[label[text()='Interface Address Range']]//input)[2]";

		public static final String newInterfaceAddressRange_textField = "@xpath=//div[label[text()='Interface Address Range']]//input";

		public static final String rightArrowButton_interfaceAddressRange = "@xpath=//span[text()=' >>']";

		public static final String addLoopback_PE_addInterface_out = "@xpath=//div[text()='Interface Address']";
		public static final String addLoopback_addInterfcae_textField_out = "@xpath=//div[label[text()='Interface Address']]//input";

		// warning Messages
		public static final String InterfaceName_warningmEssage_out = "@xpath=//div[text()='Interface']";
		public static final String addInterace_vpi_warningMessage_out = "@xpath=//div[text()='VPI']";
		public static final String addInterface_vci_warningMessage = "@xpath=//div[text()='VCI']";
		public static final String addInterface_PE_slot_warningMessage = "@xpath=//div[text()='Slot']";
		public static final String addInterface_PE_port_warningmEssage = "@xpath=//div[text()='Port']";
		public static final String addMultilink_PE_encapsulation_warningMessage = "@xpath=//div[text()='Encapsulation']";
		public static final String downloadDSrdropdown_warningMessage = "@xpath=//div[text()='DSL Downstream Speed (PCR)']";

		public static final String addCPEdevice_Outband_out = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";

		public static final String CEdevice_AddrouterId_out = "@xpath=//div[label[text()='Router Id']]//input";
		public static final String CPEdveice_vendorModel_out = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String CPEdevice_managementAddress_out = "@xpath=//div[label[text()='Management Address']]//input";
		public static final String CPEdevice_snmpro_out = "@xpath=//div[label[text()='Snmpro']]//input";
		public static final String CPEdevice_snmprw_out = "@xpath=//div[label[text()='Snmprw']]//input";
		public static final String CPEdevice_snmpV3contextname = "@xpath=//div[label[text()='Snmp V3 Context Name']]//input";
		public static final String CPEdevice_SnmpV3SecurityUsername = "@xpath=//div[label[text()='Snmp V3 Security Username']]//input";
		public static final String CPEdevice_SNMpv3AuthPassword = "@xpath=//div[label[text()='Snmp V3 Auth Password']]//input";
		public static final String CPEdevice_SNMpv3ContextEngineID = "@xpath=//div[label[text()='Snmp V3 Context Engine ID']]//input";
		public static final String CPEdevice_SNMPv3AuthProto_out = "@xpath=//div[label[text()='Snmp V3 Auth Proto']]//input";
		public static final String CPEdevice_premiseDropdown_out = "@xpath=//div[label[text()='Premise']]//input";

		public static final String CPEdevice_addCPE_selectpremise_out = "@xpath=(//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch'])[1]";
		public static final String xngCityName_out = "@xpath=//div[label[text()='Device Xng City']]/parent::div//following-sibling::div[@class='customLabelValue form-label']";
		public static final String CPE_addInterface_OKbutton_out = "@xpath=//span[text()='OK']";
		public static final String CPE_network_addInterface_out = "@xpath=//div[label[text()='Network']]//input";
		public static final String CPE_IV64bitCounter_out = "@xpath=//div[label[text()='IV 64 Bit Counter']]//input";
		public static final String CPE_addInterface_ethernet_out = "@xpath=//div[label[text()='Ethernet']]//input";
		public static final String CPE_addInterface_speed_out = "@xpath=//div[label[text()='Speed']]//input";
		public static final String CPE_addInterface_duplex_out = "@xpath=//div[label[text()='Duplex']]//input";
		public static final String CPE_addInterface_clockSource_out = "@xpath=//div[label[text()='Clock Source']]//input";

		public static final String addInterfacePanel_out = "@xpath=//div/p[text()='Add Interface']/parent::div";
		public static final String multilinkBearerPanel_addMultilink = "@xpath=//div[div[text()='Multilinked Bearers']]";

		public static final String fetchDevicename_out = "@xpath=//div[label[p[text()='Name']]]/parent::div//following-sibling::div/div";
		public static final String clickOnEditLink_CPEdevice_out = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//div[div[div[contains(@title,'value')]]]//span[text()='Edit']";
		public static final String clickOnViewLink_CPEdevice_out = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//div[div[div[contains(@title,'value')]]]//span[text()='View']";

		public static final String clickOnPPPconfigurationLink_CPedveice = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//div[div[div[contains(@title,'value')]]]//span[text()='PPP Configuration']";
		public static final String clickOnDEleteFromServiceLink_CPEdevice = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//div[div[div[contains(@title,'value')]]]//span[text()='Delete from Service']";
		public static final String clickOnFetchDeviceInterface_CPEdevice = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//div[div[div[contains(@title,'value')]]]//span[text()='Fetch device interfaces']";

		public static final String pppConfigurationPanelheader = "@xpath=//div[text()='PPP Configuration']/parent::div";

		public static final String pppConfiguration_CPEdevice_clickHereLink = "@xpath=//a[text()='Click here to add']";

		public static final String addPPPconfigurationPanel_CPedevice_out = "@xpath=//div/p[text()='Add PPP Configuration']/parent::div";
		public static final String addPPPconfiguration_deviceName = "@xpath=//div[label[text()='Device']]/parent::div//following-sibling::div[@class='customLabelValue']";
		public static final String addPPPconfiguration_generatePassowrd = "@xpath=//span[text()='Generate Password']";
		public static final String addPPPconfiguration_passwordField = "@xpath=//div[label[text()='PPP Password']]//input";
		public static final String addPPPcofig_famedWANipAddress = "@xpath=//div[label[text()='Framed/WAN/IP Address']]//input";
		public static final String addPPPconfig_framedProtocol = "@xpath=//div[label[text()='Framed Protocol:']]/parent::div//following-sibling::label";
		public static final String addPPPconfig_framedRouter0 = "@xpath=//div[label[contains(text(),'Framed Route0')]]//input";
		public static final String addPPPconfig_framedRouter1 = "@xpath=//div[label[contains(text(),'Framed Route1')]]//input";
		public static final String addPPPconfig_framedroute2 = "@xpath=//div[label[contains(text(),'Framed Route2')]]//input";
		public static final String addPPPconfig_framedRoute3 = "@xpath=//div[label[contains(text(),'Framed Route3')]]//input";
		public static final String addPPconfig_framedRoute4 = "@xpath=//div[label[contains(text(),'Framed Route4')]]//input";
		public static final String addPPPconfig_framedRoute5 = "@xpath=//div[label[contains(text(),'Framed Route5')]]//input";
		public static final String addPPPconfig_framedRoute6 = "@xpath=//div[label[contains(text(),'Framed Route6')]]//input";
		public static final String addPPPconfig_framedRoute7 = "@xpath=//div[label[contains(text(),'Framed Route7')]]//input";
		public static final String addPPPconfig_uniVirtualRouterName = "@xpath=//div[label[contains(text(),'Uni Virtual Route Name')]]//input";
		public static final String addPPPconfig_uniLocalLoopbackInterface = "@xpath=//div[label[contains(text(),'Uni Local Loopback Interface')]]//input";

		public static final String addPPPconfig_viewPage_actionDrpodown = "@xpath=//button[text()='Action']";
		public static final String pppConfigurationViewPage_editLink = "@xpath=//a[text()='Edit']";
		public static final String pppConfigurationViewPage_deleteLink = "@xpath=//a[text()='Delete']";

		public static final String actelis_EquipConfig_viewLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='View']";
		public static final String actelis_EquipConfig_deleteLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='Delete']";

		public static final String addDSLAMandHSL_xButton = "@xpath=//div[div[label[text()='DSLAM Device:']]]//following-sibling::div//div[text()='�']";
		public static final String selectDSLAMdeviceValue = "@xpath=//div[text()='value']";

		// Supraja added below
		public static final String remark_viewsiteorderpage = "@xpath=//label[contains(text(),'Remark')]/following-sibling::div";
		public static final String actellisconfig_header = "@xpath=//div[text()='Actelis Configuration']";
		public static final String providerEquipment_deviceName = "@xpath=//div[label[p[text()='Name']]]/following-sibling::div";
		public static final String pedevice_viewpage = "@xpath=//div[label[p[text()='value']]]/following-sibling::div";
		public static final String multilink_interfacefield_out = "@xpath=//div[label[text()='Interface mlppp']]//input";
		public static final String addLoopback_PE_InterfaceAddress_Warningmsg_out = "@xpath=//div[label[text()='Interface']]/div";
		public static final String addLoopback_InterfaceAddress_textField_out = "@xpath=//div[label[text()='Interface Address']]//input";
		public static final String EIPallocation_addInterface = "@xpath=//span[text()='EIP Allocation']";

		public static final String addIPVPNsiteOrder_cityDropdown = "@xpath=//select[@id='cityId']";
		public static final String addIPVPNsiteOrder_countryDropdown = "@xpath=//select[@id='countryId']";
		public static final String addIPVPNsiteOrer_siteDropdown = "@xpath=//select[@id='siteType']";
		public static final String PEdevice_interfaceHeaderName = "@xpath=//p[text()='Add Interface']";
		public static final String fetchCPEdeviceName_out = "@xpath=//div[label[text()='Name']]/parent::div//following-sibling::div/div";
		public static final String viewCPEdevice_siteLabelName1 = "@xpath=//label[text()='Site']";
		public static final String viewPEdevice_siteLabel = "@xpath=";

		public static final String CPEdevice_addMultilinkPanelHeader = "@xpath=//p[text()='Add Multilink']/parent::div";
		public static final String CPEdevice_addMultilink_InterfaceTextfield = "@xpath=//div[label[text()='Interface Multilink']]//input";
		public static final String CPEdevice_addMultilink_addressTextField = "@xpath=//label[text()='Address']/parent::div//input";
		public static final String pppConfiguration_viewPage_DeleteButton = "@xpath=//span[text()='Delete']";

		public static final String viewCPEdevice_siteLabelName = "@xpath=//label[text()='Site']";

		// Add Route panel

		public static final String Routes_Header = "@xpath=//div[text()='Routes']";

		public static final String Routes_Actionbutton = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']";

		public static final String Routes_AddLink = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Add Route']";

		public static final String Routes_EditLink = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Edit']";

		public static final String Routes_DeleteLink = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Delete']";

		public static final String Routes_DeleteButton = "@xpath=//button//span[text()='Delete']";

		public static final String Routes_AddRoutes_Header = "@xpath=//div[text()='Add Route']";

		public static final String Routes_EditRoutes_Header = "@xpath=//div[text()='Edit Route']";

		public static final String Routes_EIPAllocationLink = "@xpath=//span[text()='EIP Allocation']";

		public static final String Routes_EIPSubnetAllocationforService_Header = "@xpath=//div[contains(text(),'EIP Subnet Allocation for Service ')]";

		public static final String Routes_EIPAllocation_SubnetTypeValue = "@xpath=//div[contains(text(),'CUST')]";

		public static final String Routes_EIPAllocation_CityWarningMessage = "@xpath=//div[text()='City']";

		public static final String Routes_EIPAllocation_SubnetSizeWarningMessage = "@xpath=//div[text()='Sub Net Size']";

		public static final String Routes_EIPAllocation_AvilablePoolWarningMessage = "@xpath=//div[text()='Available Pools']";

		public static final String CPE_Routes_EIPAllocation_SubnetTypeValue = "@xpath=//div[contains(text(),'CUST')]";

		public static final String Routes_EIPAllocation_CityDropdown = "@xpath=//div[label[text()='City']]/parent::div//input";

		public static final String Routes_EIPAllocation_SubnetSizeDropdown = "@xpath=//div[label[text()='Sub Net Size']]/parent::div//input";

		public static final String Routes_EIPAllocation_AvailablePoolsMessage = "@xpath=//div[div[label[text()='Available Pools']]]/span";

		public static final String Routes_EIPAllocation_CloseButton = "@xpath=//button/span[text()='�']";

		public static final String EditRoutes_EIPAllocation_CloseButton = "@xpath=(//button/span[text()='�'])[2]";

		public static final String Routes_Gatewaytextfield = "@xpath=//input[@id='gateway']";

		public static final String Routes_NetworkAddresstextfield = "@xpath=//input[@id='destination']";

		public static final String Routes_NetworkMAStextfield = "@xpath=//input[@id='networkMask']";

		public static final String Routes_Metricstextfield = "@xpath=//input[@id='metrics']";

		public static final String Routes_OKButton = "@xpath=//span[text()='OK']";

		public static final String Routes_Gatway_WarningMessage = "@xpath=//span[@class='error' and text()='Gateway']";

		public static final String Routes_NetworkAddress_WarningMessage = "@xpath=//span[@class='error' and text()='Network Address']";

		public static final String Routes_NetworkMASK_WarningMessage = "@xpath=//span[@class='error' and text()='Network Mask']";

		public static final String Routes_AllocateSubnetButon = "@xpath=//span[text()='Allocate Subnet']";

		public static final String AddRouteSuccessMessage = "@xpath=//span[text()='Static Route successfully created.']";

		public static final String UpdateRouteSuccessMessage = "@xpath=//span[contains(text(),'Static Route successfully updated.')]";

		public static final String DeleteRouteSuccessMessage = "@xpath=//span[contains(text(),'Static Route successfully deleted.')]";

		public static final String Routes_SelectRouteCheckbox = "@xpath=(//span[@class='ag-selection-checkbox']//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";

		// Customer Readonly SNMP Panel

		public static final String CustomerReadonlySNMP_panelHeader = "@xpath=//div[contains(text(),'Customer Readonly SNMP')]";

		public static final String SNMP_Actionbutton = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']";

		public static final String SNMP_AddLink = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Add Customer Readonly SNMP']";

		public static final String SNMP_EditLink = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Edit']";

		public static final String SNMP_DeleteLink = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Delete']";

		public static final String SNMP_DeleteButton = "@xpath=//button//span[text()='Delete']";

		public static final String AddSNMP_Header = "@xpath=//div[contains(text(),'Add Customer Readonly SNMP')]";

		public static final String EditSNMP_Header = "@xpath=//div[contains(text(),'Edit Customer Readonly SNMP')]";

		public static final String SNMP_CustomerIPAddress_Textfield = "@xpath=//label[text()='Customer Ip Address']/parent::div//input";

		public static final String SNMP_CustomerCommunityString_Textfield = "@xpath=//label[text()='Customer Community String']/parent::div//input";

		public static final String SNMP_CustomerIPAddress_WarningMessage = "@xpath=//div[text()='Customer Ip Address']";

		public static final String SNMP_CustomerCommunityString_WarningMessage = "@xpath=//div[text()='Customer Community String']";

		public static final String SNMP_OKButton = "@xpath=//span[text()='OK']";

		public static final String AddSNMPSuccessMessage = "@xpath=//span[contains(text(),'Customer Readonly SNMP successfully created.')]";

		public static final String UpdateSNMPSuccessMessage = "@xpath=//span[contains(text(),'Customer Readonly SNMP successfully updated.')]";

		public static final String DeleteSNMPSuccessMessage = "@xpath=//span[contains(text(),'Customer Readonly SNMP successfully deleted.')]";

		public static final String SNMP_SelectRouteCheckbox = "@xpath=(//*[@id='root']/div/div/div[2]/div/div[1]/div[1]/div[7]/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[3]/div[2]/div/div/div[1]/div[1]/span/span[1]/span[2])[1]";

		public static final String customerReadonlySelectValueUnderPanel = "@xpath=//div[text()='Customer Readonly SNMP']/parent::div//following-sibling::div//div[text()='value']";

		// Extra Subnets Panels

		public static final String ExtraSubnets_PanelHeader = "@xpath=//div[contains(text(),'Extra Subnets')]";

		public static final String ExtraSubnets_Action = "@xpath=//div[contains(text(),'Extra Subnet')]/parent::div//button";

		public static final String AllocateExtraSubnets_Link = "@xpath=//a[contains(text(),'Allocate Extra Subnet')]";

		public static final String AddExtraSubnets_Header = "@xpath=(//div[contains(text(),'Extra Subnet')]/parent::div//div[text()='Extra Subnets'])[2]";

		public static final String ExtraSubnets_SubnetTypeValue = "@xpath=//div[contains(text(),'CUST')]";

		public static final String ExtraSubnets_CityDropdown = "@xpath=//div[label[text()='City']]/parent::div//input";

		public static final String ExtraSubnets_SubnetSizeDropdown = "@xpath=//div[label[text()='Subnet Size']]/parent::div//input";

		public static final String ExtraSubnets_ValueSelection = "@xpath=(//div[@class='sc-htpNat AUGYd']/div)[1]";

		public static final String ExtraSubnets_AvailablePoolsMessage = "@xpath=//div[div[label[text()='Available Blocks']]]/div[@class='customLabelValue form-label']";

		public static final String ExtraSubnets_CloseButton = "@xpath=//button/span[text()='�']";

		public static final String ExtraSubnets_City_WarningMessage = "@xpath=//div[text()='City']";

		public static final String ExtraSubnets_SubnetSize_WarningMessage = "@xpath=//div[text()='Subnet Size']";

		public static final String ExtraSubnets_EIPAllocation_AvilablePoolWarningMessage = "@xpath=//span[text()='Available Blocks is required']";

		public static final String ExtraSubnets_AllocateSubnetButon = "@xpath=//span[text()='Allocate Subnet']";

		// NAT Configuration Panel

		public static final String NATConfiguration_panelHeader = "@xpath=//div[contains(text(),'NAT Configuration')]";

		public static final String NAT_Actionbutton = "@xpath=//div[div[text()='NAT Configuration']]/parent::div//button[text()='Action']";

		public static final String NAT_EditLink = "@xpath=//div[div[text()='NAT Configuration']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Edit']";

		public static final String EditNAT_Header = "@xpath=//div[contains(text(),'NAT Configuration') and @class='modal-title h4']";

		public static final String StaticNAT_Checkbox = "@xpath=//input[@id='staticNatFlag']";

		public static final String DynamicNAT_Checkbox = "@xpath=//input[@id='dynamicNatFlag']";

		public static final String NAT_OKButton = "@xpath=//span[contains(text(),'OK')]";

		public static final String UpdateNATSuccessMessage = "@xpath=//span[contains(text(),'Nat Configuration successfully updated.')]";

		// Static NAT Mapping Panel

		public static final String StaticNATMapping_panelHeader = "@xpath=//div[contains(text(),'Static NAT Mapping')]";

		public static final String StaticNAT_Actionbutton = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']";

		public static final String StaticNAT_AddLink = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Add Static NAT Mapping']";

		public static final String StaticNAT_Editink = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";

		public static final String StaticNAT_DeleteLink = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete']";

		public static final String AddStaticNAT_Header = "@xpath=//div[contains(text(),'Add Static NAT Mapping')]";

		public static final String EditStaticNAT_Header = "@xpath=//div[contains(text(),'Edit Static NAT Mapping')]";

		public static final String StaticNAT_Protocol_Dropdown = "@xpath=//div[label[text()='Protocol']]//input";

		public static final String StaticNAT_LocalPort_Textfield = "@xpath=//label[text()='Local Port']/parent::div//input";

		public static final String StaticNAT_GlobalPort_Textfield = "@xpath=//label[text()='Global Port']/parent::div//input";

		public static final String StaticNAT_LocalIP_Textfield = "@xpath=//label[text()='Local IP']/parent::div//input";

		public static final String StaticNAT_GlobalIP_Textfield = "@xpath=//label[text()='Global IP']/parent::div//input";

		public static final String StaticNAT_LocalPort_WarningMessage = "@xpath=//div[contains(text(),'Local Port')]";

		public static final String StaticNAT_GlobalPort_WarningMessage = "@xpath=//div[contains(text(),'Global Port')]";

		public static final String StaticNAT_LocalIP_WarningMessage = "@xpath=//div[contains(text(),'Local IP')]";

		public static final String StaticNAT_GlobalIP_WarningMessage = "@xpath=//div[contains(text(),'Global IP')]";

		public static final String AddStaticNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully inserted.')]";

		public static final String UpdateStaticNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully updated.')]";

		public static final String DeleteStaticNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully deleted.')]";

		public static final String SelectStaticNATCheckbox = "@xpath=(//body/div[@id='root']/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/span[1]/span[1]/span[2])[1]";

		public static final String OKButton_common1 = "@xpath=//span[text()='OK']";

		public static final String editStatic_selectValueUnderPanel = "@xpath=(//div[text()='Static NAT Mapping']/parent::div//following-sibling::div//div[text()='value'])[1]";

		// Dynamic NAT Mapping Panel

		public static final String DynamicNATMapping_panelHeader = "@xpath=//div[contains(text(),'Dynamic NAT Mapping')]";

		public static final String DynamicNAT_Actionbutton = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']";

		public static final String DynamicNAT_AddLink = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Add']";

		public static final String DynamicNAT_Editink = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";

		public static final String DynamicNAT_DeleteLink = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete']";

		public static final String AddDynamicNAT_Header = "@xpath=//div[contains(text(),'Add Dynamic NAT Mapping')]";

		public static final String EditDynamicNAT_Header = "@xpath=//div[contains(text(),'Edit Dynamic NAT Mapping')]";

		public static final String AddDynamicNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully inserted.')]";

		public static final String UpdateDynamicNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully updated.')]";

		public static final String DeleteDynamicNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully deleted.')]";

		public static final String SelectDynamicNATCheckbox = "@xpath=(//body/div[@id='root']/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/span[1]/span[1]/span[2])[1]";

		// DHCP Servers on CPE Panel

		public static final String DHCP_panelHeader = "@xpath=//div[contains(text(),'DHCP Servers on CPE')]";

		public static final String DHCP_Actionbutton = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']";

		public static final String DHCP_AddLink = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Add DHCP Servers on CPE']";

		public static final String DHCP_Editink = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";

		public static final String DHCP_DeleteLink = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete']";

		public static final String AddDHCP_Header = "@xpath=//div[contains(text(),'Add DHCP Servers on CPE')]";

		public static final String EditDHCP_Header = "@xpath=//div[contains(text(),'Edit DHCP Servers on CPE')]";

		public static final String DHCP_CustomerLANSubnet_Textfield = "@xpath=//div[label[text()='Customer LAN Subnet']]//input";

		public static final String DHCP_SubnetMask_Textfield = "@xpath=//div[label[text()='Subnet Mask']]//input";

		public static final String DHCP_PrimaryDNSServer_Textfield = "@xpath=//div[label[text()='Primary DNS Server']]//input";

		public static final String DHCP_SecondaryDNSServer_Textfield = "@xpath=//div[label[text()='Secondary DNS Server']]//input";

		public static final String DHCP_CustomerLANSubnet_WarningMessage = "@xpath=//div[text()='Customer LAN Subnet']";

		public static final String DHCP_SubnetMask_WarningMessage = "@xpath=//div[text()='Subnet Mask']";

		public static final String DHCP_PrimaryDNSServer_WarningMessage = "@xpath=//div[text()='Primary DNS Server']";

		public static final String AddDHCPSuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully created.')]";

		public static final String UpdateDHCPSuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully updated.')]";

		public static final String DeleteDHCPSuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully deleted.')]";

		public static final String SelectDHCPCheckbox = "@xpath=(//div[13]//div[2]//div[1]//div[1]//div[1]//div[2]//div[1]//div[1]//div[1]//div[3]//div[2]//div[1]//div[1]//div[1]//div[1]//span[1]//span[1]//span[2])[1]";

		public static final String selectValueUnderDHCPpanel = "@xpath=//div[text()='DHCP Servers on CPE']/parent::div//following-sibling::div//div[text()='value']";

		// DHCP IPV6 Servers on CPE Panel

		public static final String DHCPIPV6_panelHeader = "@xpath=//div[contains(text(),'DHCP IPV6 Servers on CPE')]";

		public static final String DHCPIPV6_Actionbutton = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']";

		public static final String DHCPIPV6_AddLink = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Add']";

		public static final String DHCPIPV6_Editink = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";

		public static final String DHCPIPV6_DeleteLink = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete ']";

		public static final String AddDHCPIPV6_Header = "@xpath=//div[contains(text(),'Add IPV6 DHCP Server on CPE')]";

		public static final String EditDHCPIPV6_Header = "@xpath=//div[contains(text(),'Edit IPV6 DHCP Server on CPE')]";

		public static final String DHCPIPV6_CustomerLANSubnet_Textfield = "@xpath=//input[@id='subnet']";

		public static final String DHCPIPV6_SubnetMask_Textfield = "@xpath=//input[@id='subnetMask']";

		public static final String DHCPIPV6_PrimaryDNSServer_Textfield = "@xpath=//input[@id='nameserver1']";

		public static final String DHCPIPV6_SecondaryDNSServer_Textfield = "@xpath=//input[@id='nameserver2']";

		public static final String DHCPIPV6_CustomerLANSubnet_WarningMessage = "@xpath=//div[text()='Customer LAN Subnet']";

		public static final String DHCPIPV6_SubnetMask_WarningMessage = "@xpath=//div[text()='Subnet Mask']";

		public static final String DHCPIPV6_PrimaryDNSServer_WarningMessage = "@xpath=//div[text()='Primary DNS Server']";

		public static final String AddDHCPIPV6SuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully created.')]";

		public static final String UpdateDHCPIPV6SuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully updated.')]";

		public static final String DeleteDHCPIPV6SuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully deleted.')]";

		public static final String SelectDHCPIPV6Checkbox = "@xpath=(//div[13]//div[2]//div[1]//div[1]//div[1]//div[2]//div[1]//div[1]//div[1]//div[3]//div[2]//div[1]//div[1]//div[1]//div[1]//span[1]//span[1]//span[2])[1]";

		public static final String interfaces_header = "@xpath=//div[text()='Interfaces']";

		public static final String configurationPanelHeader = "@xpath=//div[text()='Configuration']";
		public static final String generateConfigurationDropdown = "@xpath=//div[label[text()='Generate Configuration for']]//input";
		public static final String generateButton = "@xpath=//span[text()='Generate']";
		public static final String generateConfiguration_generatedValue = "@xpath=//textarea[@name='configurationDetails']";

		// PE t CPE link
		public static final String peToCpeLink_ActionDropdown = "@xpath=//div[text()='PE to CPE Links']/parent::div//button[text()='Action']";
		public static final String peToCpeLink_addNewLink = "@xpath=//a[text()='Add New Link']";
		public static final String addNewLink_PanelHeader = "@xpath=//div[text()='Add New Link']/parent::div";
		public static final String addNewLink_nextButton = "@xpath=//div[text()='Add New Link']/parent::div//following-sibling::div//span[text()='Next']";

		public static final String addNewLink_linkOrCircuitID_warningMessage = "@xpath=//div[text()='Link/Circuit Id']";
		public static final String addNewLink_PEsourceDevice_warningMEsage = "@xpath=//div[text()='PE Source Device']";
		public static final String addNewLink_CPEtargetDevice_warningMessage = "@xpath=//div[text()='CPE Target Device']";
		public static final String addNewLink_PEsourceInterface_warningMEsage = "@xpath=//div[text()='PE Source Interface']";
		public static final String addNewLink_CPEtargetInterface_warningMessage = "@xpath=//div[text()='CPE Target Interface']";

		public static final String addNewlink_linkOrcicuittextField = "@xpath=//label[text()='Link/Circuit Id']/parent::div//input";
		public static final String addNewlink_PEsourceDeviceDropdown = "@xpath=//label[text()='PE Source Device']/parent::div//input";
		public static final String addNewLink_PEsourceInterfaceDropdown = "@xpath=//label[text()='PE Source Interface']/parent::div//input";
		public static final String addNewlink_CPEtargetInterface = "@xpath=//label[text()='CPE Target Interface']/parent::div//input";
		public static final String addNewlink_CPEtargetDevice = "@xpath=//label[text()='CPE Target Device']/parent::div//input";

		// Circuit Creation
		public static final String addOvertureLink = "@xpath=(//span[text()='Add Overture'])[1]";
		public static final String addCircuit_AccedianLink = "@xpath=//span[text()='Add Accedian-1G']";
		public static final String addCircuit_AtricaLink = "@xpath=//span[contains(text(),'Add Circuit')]";

		public static final String addOverture_OKbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String addOverture_overturePanel = "@xpath=//body//p[1]";
		public static final String addCircuit_Accedian1Gpage_panel = "@xpath=//p[contains(text(),'Select Accedian-1G Circuit')]";
		public static final String addCircuit_Atricapage_Panel = "@xpath=//p[text()='Select Atrica Circuit']";

		public static final String addOverture_serviceNameTextField = "@xpath=//label[text()='Service Name']/parent::div//input";
		public static final String addOverture_searchButton = "@xpath=//span[text()='Search']";
		public static final String selectValueUnderAddOverturePage = "@xpath=(//div[text()='value'])[1]";
		public static final String addOverture_cancelButton = "@xpath=//span[text()='Cancel']";

		public static final String addOverture_interfaceInServicePanel = "@xpath=//div[label[text()='Interfaces In Service']]";
		public static final String interfaceFilterButton = "@xpath=//div[span[text()='Interface']]/preceding-sibling::span";
		public static final String interfacePage_filterText = "@xpath=//input[@id='filterText']";
		public static final String interfaceInService_selectValueUnderTable = "@xpath=(//div[text()='value']/parent::div//input)[1]";
		public static final String interfaceinService_selectEdgePointforInterface = "@xpath=(//div[text()='value']/parent::div//input)[2]";

		public static final String PAMtest_selectinterface = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//div[text()='value']";
		public static final String PAMtest_actionDropdown = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//button[text()='Action']";
		public static final String PAMTest_siteValue = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Site']]//following-sibling::div";
		public static final String PAMTest_deleteButton = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//span[text()='Delete']";
		public static final String deleteCircuit_deleteButton = "@xpath=//button[@class='btn btn-danger']";
		public static final String configure_alertPopup_xbutton = "@xpath=//span[text()='�']";

		public static final String peTOcpeLink_xbutton = "@xpath=//div[text()='Add New Link']/parent::div//span[text()='�']";

		public static final String devicename1 = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'";
		public static final String devicename2 = "')]]]//span[text()='Delete from Service']";

		public static final String deviceview1 = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'";
		public static final String deviceview2 = "')]]]//span[text()='View']";

		public static final String deviceInterface1 = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'";
		public static final String deviceInterface2 = "')]]]//span[text()='Select Interfaces']";

		public static final String CPEdevice_showinterfaceslink_intEquip = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Intermediate Equipment']]]//a[text()='Show Interfaces']";
		public static final String Equipmentshowintefaceedit = "@xpath=(//a[text()='Edit'])[1]";

		public static final String siteordernumber_p2p_mspSelected1 = "@xpath=//div[div[text()='Site Orders']]//following-sibling::div//div[text()='";
		public static final String siteordernumber_p2p_mspSelected2 = "']";
		public static final String siteOrderNumber_10G_p2p1 = "@xpath=//div[div[text()='Site Orders']]//following-sibling::div//div[text()='";
		public static final String siteOrderNumber_10G_p2p2 = "']";
		public static final String siteOrdernumber_P2P1 = "@xpath=//div[div[text()='Site Orders']]//following-sibling::div//div[text()='";
		public static final String siteOrdernumber_P2P2 = "']";
		public static final String siteordernumber1 = "@xpath=//div[div[text()='Site Orders']]//following-sibling::div//div[text()='";
		public static final String siteordernumber2 = "']";

		public static final String eleteDeviceService1 = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'";
		public static final String eleteDeviceService2 = "')]]]//span[text()='Delete from Service']";
		public static final String listOfTechnology = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String technologyToBeSelected1 = "@xpath=//div[@class='modal-body']//div[contains(text(),'";
		public static final String technologyToBeSelected2 = "')]";

		public static final String interfacedetails1 = "@xpath=//div[div[text()='";
		public static final String interfacedetails2 = "']]/div";

		public static final String selectInterface1 = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'";
		public static final String selectInterface2 = "')]]]//span[text()='Select Interfaces']";

		public static final String deviceView1 = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'";
		public static final String deviceView2 = "')]]]//span[text()='View']";

		public static final String viewPage1 = "@xpath=//div[div[label[contains(text(),'";
		public static final String viewPage2 = "')]]]/div[2]";

		public static final String deleteInterfaceX = "@xpath=//button[@class='btn btn-danger']";
		public static final String deleteInterafce1 = "@xpath=//div[text()='";
		public static final String deleteInterafce2 = "']";

		public static final String editInterafce1 = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'";
		public static final String editInterafce2 = "')]]]//span[text()='Configure']";

		public static final String testlist = "@xpath=//tbody/tr";
		public static final String testlist1 = "@xpath=(//tbody/tr[";
		public static final String testlist2 = "]/td)[1]";
		public static final String testlist3 = "]/td)[2]/div";

		public static final String showinterfacedevice1 = "//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'";

		public static final String showinterfacedevice2 = "')]]]";
		public static final String name = "@xpath=//label[text()='Name']";

		public static final String interfacenumber1 = "@xpath=//div[contains(text(),'";
		public static final String interfacenumber2 = "')]";

		public static final String nextBtn = "@xpath=//button[text()='Next']";
		public static final String next = "@xpath=//span[text()='Next']";

		public static final String noneditablefield1 = "@xpath=//div[div[label[text()='";
		public static final String noneditablefield2 = "']]]//div[text()='";
		public static final String noneditablefield3 = "']";

		public static final String listofinterfacespeed = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofServicesubtypes = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String results1 = "@xpath=//div[@role='row']//div[text()='";
		public static final String results2 = "']";

		public static final String listofA_endTechnologies = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofB_endTechnologies = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofvender = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofTechnology = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String technologyOverture = "@xpath=//div[@class='modal-body']//div[contains(text(),'Overture')]";

		public static final String vender1 = "@xpath=//div[label[text()='Vendor/Model']]//div[text()='";
		public static final String vender2 = "']";

		/////////////////////// sprint 3/////////////////////////

		public static final String deliveryChannel1 = "@xpath=//div[contains(text(),'";
		public static final String deliveryChannel2 = "')]";

		public static final String ManagementOrder1 = "@xpath=//div[contains(text(),'";
		public static final String ManagementOrder2 = "')][1]";

		public static final String vpnTopology1 = "@xpath=//div[text()='";
		public static final String vpnTopology2 = "']";

		public static final String CircuitType1 = "@xpath=//div[span[contains(text(),'";
		public static final String CircuitType2 = "')]]//input";

		public static final String Technology1 = "@xpath=//div[text()='";
		public static final String Technology2 = "']";

		public static final String listofcountry = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String IVReference1 = "@xpath=//div[text()='";
		public static final String IVReference2 = "']";

		public static final String IVReference = "@xpath=//div[div[label[contains(text(),'IV Reference')]]]/div[2]";

		public static final String technology1 = "@xpath=//div[contains(text(),'";
		public static final String technology2 = "')]";

		public static final String tech = "@xpath=//div[div[label[contains(text(),'Technology')]]]/div[2]";
		public static final String listofsmartmonitoring = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofproactivemonitoring = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String listofperformancereporting = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String siteOrderNumber = "@xpath=//div[div[label[contains(text(),'Site Order Number (Siebel Service ID)')]]]/div[2]";

		public static final String siteOrderNumber1 = "@xpath=//div[text()='";
		public static final String siteOrderNumber2 = "']";

		public static final String notificationManagement1 = "@xpath=//div[contains(text(),'";
		public static final String notificationManagement2 = "')]";

		public static final String device1 = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'";
		public static final String device2 = "')]]]//span[text()='View']";

		public static final String interfacenumber3 = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='";
		public static final String interfacenumber4 = "']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacenumber5 = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='";
		public static final String interfacenumber6 = "']]//input";
		public static final String interfacenumber7 = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='";
		public static final String interfacenumber8 = "']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacenumber9 = "@xpath=(//div[@class='row'])[8]//div[div[contains(text(),'";
		public static final String interfacenumber10 = "')]]//input";
		public static final String labelname1 = "@xpath=//div[div[label[contains(text(),'";
		public static final String labelname2 = "')]]]/div[2]";

		public static final String label1 = "@xpath=(//div[div[label[contains(text(),'";
		public static final String label2 = "')]]]/div[2])[2]";
		public static final String fieldValidation1 = "@xpath=//div[contains(text(),'";
		public static final String fieldValidation2 = "')]";

		public static final String listofnotificationmanagement = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofdeliverychannel = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofmanagementOrder = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofvpntopology = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofcircuittypes1 = "@xpath=//div[@class='div-border div-margin container']//div[@class='row'][3]//span";
		public static final String defaultTOpologValues = "@xpath=//span[contains(text(),'Point-to-Point')]";

		public static final String listofcloudservices = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String CircuitType_1 = "@xpath=//div[span[contains(text(),'";
		public static final String CircuitType_2 = "')]]//input";
		public static final String vpnTopology = "@xpath=//div[div[label[contains(text(),'VPN Topology')]]]//div[@class='customLabelValue form-label']";

		public static final String TextKeyword1 = "(//div[div[div[contains(@title,'";
		public static final String TextKeyword2 = "')]]]/following-sibling::div)[1]//span[@ref='lbTotal']";
		public static final String CurrentPage1 = "(//div[div[div[contains(@title,'";
		public static final String CurrentPage2 = "')]]]/following-sibling::div)[1]//span[@ref='lbCurrent']";
		public static final String deviceinterface1 = "(//div[div[div[contains(@title,'";
		public static final String deviceinterface2 = "')]]]/following-sibling::div)[1]//div[text()='";
		public static final String deviceinterface3 = "']";
		public static final String next1 = "(//div[div[div[contains(@title,'";
		public static final String next2 = "')]]]/following-sibling::div)[1]//button[text()='Next']";
		public static final String interfacename3 = "(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//div[text()='";
		public static final String interfacename4 = "']";
		public static final String action1 = "(//div[div[div[contains(@title,'";
		public static final String action2 = "')]]]/following-sibling::div)[1]//button[text()='Action']";

		public static final String order_contractnumber_warngmsg = "@xpath=//label[contains(text(),'Order/Contract Number(Parent SID)')]/following-sibling::span[3]";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";
		public static final String performReport1 = "";
		public static final String performReport2 = "";
		public static final String ProactiveMonitor = "@xpath=//div[label[text()='Proactive Monitoring']]//input";
		public static final String ProactiveMonitor2 = "";
		public static final String smartmonitor = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";
		public static final String smartmonitor2 = "";
		public static final String selectCityToggleButton = "";
		public static final String selectPremiseToggleButton = "";
		public static final String selectSiteToggleButton = "";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button ";
		public static final String servicesubtypeddwn = "@xpath=//div[label[text()='Service Subtype:']]//input";
		public static final String deliverychannel_withclasskey1 = "@xpath=//div[label[text()='Delivery Channel']]//input";
		public static final String performancereport = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service'] ";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input ";
		public static final String searchbutton = "@xpath=//span[text()='Search'] ";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')] ";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button ";
		public static final String view1 = "@xpath=//div[@class='dropdown-menu show']//a[text()='View'] ";
		public static final String deleteButton = "@xpath=//button[@type='button' and contains(text(),'Delete')]";

	}

}
