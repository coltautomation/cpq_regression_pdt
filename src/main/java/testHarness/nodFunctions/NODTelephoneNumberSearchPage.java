package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODTelephoneNumberSearchPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;

	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void SearchOrderByTelPhNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String TelephoneNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number");

		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODMyOrdersObj.MyOrder.QuickLinkDrpDwnArw,"Quick Link Drop down");
		Reusable.waitForAjax();

		click(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoOpt,"My Telephone Number Option");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoPage,15);	

		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.NumberOpt,"Number Option");
		Reusable.waitForAjax();

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, TelephoneNumber, "Telephone Number");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Range Start Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.RangeStartColumn,"Range Start Column");

		String RangeStart = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.RangeStartVal,"Range Start");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telephoneNumberStart'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range Start Is: "+RangeStart);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range Start is not Present");	
		}

		//Range End Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.RangeEndColumn,"Range End Column");

		String RangeEnd = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.RangeEndVal,"Range End");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telephoneNumberEnd'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range End Is: "+RangeEnd);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range End is not Present");	
		}

		//Date Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.DateColumn,"Date Column");

		String Date = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.DateVal,"Date");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='date'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Date Is: "+Date);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Date is not Present");	
		}

		//Order ID Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+TransID);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Id Is not Present");	
		}

		//User Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.UserColumn,"User Column");

		String User = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.UserVal,"User");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='userName'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "User Is: "+User);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "User Is not Present");	
		}

	}

	public void SearchOrderByOrderId(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_ID");

		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODMyOrdersObj.MyOrder.QuickLinkDrpDwnArw,"Quick Link Drop down");
		Reusable.waitForAjax();

		click(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoOpt,"My Telephone Number Option");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoPage,15);	

		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.OrderIDOpt,"Order ID Option");
		Reusable.waitForAjax();

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, OrderID, "Order ID");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Range Start Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.RangeStartColumn,"Range Start Column");

		String RangeStart = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.RangeStartVal,"Range Start");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telephoneNumberStart'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range Start Is: "+RangeStart);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range Start is not Present");	
		}

		//Range End Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.RangeEndColumn,"Range End Column");

		String RangeEnd = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.RangeEndVal,"Range End");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telephoneNumberEnd'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range End Is: "+RangeEnd);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range End is not Present");	
		}

		//Date Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.DateColumn,"Date Column");

		String Date = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.DateVal,"Date");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='date'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Date Is: "+Date);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Date is not Present");	
		}

		//Order ID Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+TransID);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Id Is not Present");	
		}

		//User Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.UserColumn,"User Column");

		String User = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.UserVal,"User");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='userName'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "User Is: "+User);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "User Is not Present");	
		}

	}

	public void ExportToExcel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.DownlaodExelIcon,"Downlaod To Excel");
		click(NODTelephoneNumberSearchObj.MyNumber.DownlaodExelIcon,"Downlaod To Excel");
		Reusable.waitForAjax();

		isFileDownloaded_DDI("E:\\Kiran_Workspace\\NOD", "Order-ANH-GB-Thu May 06 2021 13_32_41 GMT+0100 (British Summer Time)");

	}


	public void OrderIdHyperLink_MyNumberPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		
		SearchOrderByOrderId(testDataFile, sheetName, scriptNo, dataSetNo);

		//Order ID Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderIDColumn,"Order ID Column");
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"Order ID ");

		//String TransID = getTextFrom(NODMyOrdersObj.MyOrder.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id is Present");
			click(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"Order ID");

			//Order Details section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderDetailsSection,"Order Details Section");
			click(NODTelephoneNumberSearchObj.MyNumber.OrderDetailsSection,"Order Details Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//h5[contains(text(),'Order Details')]"))) 
			{
				//verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsPage,"Order Details Section");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
			}

			//Number Details section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.NumberDetailsection,"Number Details Section");
			click(NODTelephoneNumberSearchObj.MyNumber.NumberDetailsection,"Number Details Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//h4[contains(text(),'Telephone Numbers')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number Details page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Details page is not Accessable");	
			}

			//History Conversation Section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.HistoryConversationSection,"History Conversation Section");
			click(NODTelephoneNumberSearchObj.MyNumber.HistoryConversationSection,"History Conversation Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//div[@id='NewPortIncollapseStepper']"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "History Conversation Page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "History Conversation Page is not Accessable");	
			}

			//Number History Section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.NumberHistorysSection,"Number History Section");
			click(NODTelephoneNumberSearchObj.MyNumber.NumberHistorysSection,"Number History Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//h6[contains(text(),'Search for a number')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number History Page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Number History Page is not Accessable");	
			}		

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Is not Present");	
		}

	}
























	//Reuseable fun
	public void NavigationtoHomePage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String ServiceProfile = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Profile");

		javaScriptclick(NODMyOrdersObj.MyOrder.ServiceProfiledrpdwn,"Select your country and profile");
		sendKeys(NODMyOrdersObj.MyOrder.ServiceProfiledrpdwn, ServiceProfile, "Select your country and profile");
		javaScriptclick(NODMyOrdersObj.MyOrder.ProfileOption,"Service Profile Option");
		//Reusable.waitForAjax();

		javaScriptclick(NODMyOrdersObj.MyOrder.StartButton,"Let's get started button");
		waitForElementToBeVisible(NODMyOrdersObj.MyOrder.HomeMenu,15);

	}

	public boolean isFileDownloaded_DDI(String downloadPath, String fileName) {
		File dir = new File(downloadPath);
		File[] dirContents = dir.listFiles();

		for (int i = 0; i < dirContents.length; i++) {
			if (dirContents[i].getName().contains(fileName)) {
				// File has been found, it can now be deleted:
				//dirContents[i].delete();

				String downloadedFileName=dirContents[i].getName();
				System.out.println("Downloaded file name is displaying as: "+ downloadedFileName);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Downloaded file name is displaying as: "+ dirContents[i]);

				return true;
			}
		}
		return false;
	}

	//Atul

	public void NavigateonMyTelephoneNumbersPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODMyOrdersObj.MyOrder.QuickLinkDrpDwnArw,"Quick Link Drop down");
		Reusable.waitForAjax();
		click(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoOpt,"My Telephone Number Option");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoPage,15);	

	}

	public void AppliedFilterOnMyTelephoneNumPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String NumberStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Status");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
		verifyExists(NODMyOrdersObj.FilterOrder.filterOption,"filter Option");
		click(NODMyOrdersObj.FilterOrder.filterOption,"Click filter Option");
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.NumberStatus,15);	
		Reusable.Select(NODTelephoneNumberSearchObj.MyNumber.NumberStatus,NumberStatus);
		click(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);	
	}

	

	public void NavigateonTXPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]")))
        {                              
			click(NODTelephoneNumberSearchObj.MyNumber.OrderID,"Order ID");
			waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.OrderDetailsSection,15);		
			Reusable.waitForAjax();
        }
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
	}



	public void SearchOrderByNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String TelephoneNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number");

		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.NumberOpt,"Number Option");
		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, TelephoneNumber, "Number");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);	
	}


	public void SearchOrderByID(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_ID");

		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.NumberOpt,"Number Option");
		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, OrderID, "Number");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);	
	}

	

}



