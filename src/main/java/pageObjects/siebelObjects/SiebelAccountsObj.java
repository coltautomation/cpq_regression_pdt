package pageObjects.siebelObjects;

import java.io.File;
import java.io.IOException;

import baseClasses.DataMiner;
import baseClasses.ReadExcelFile;
public class SiebelAccountsObj {
	
	public static class Accounts
	 { 
		public static final String accountsTab="@xpath=//a[text()='Accounts']";
		public static final String OCNTxb="@xpath=//input[@aria-label='Account Number(OCN)']"; // Account Number field
		public static final String GoBtn="@xpath=//span[text()='Go']";
		public static final String InstalledAssets_NewBtn="@xpath=//button[@title='Installed Assets:New']";
	 }
	
	
	
	
}
