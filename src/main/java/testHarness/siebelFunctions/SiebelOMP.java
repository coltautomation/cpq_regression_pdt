package testHarness.siebelFunctions;

import java.io.IOException;
import java.util.Set;

import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelOMPObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import org.openqa.selenium.By;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import junit.framework.Assert;

public class SiebelOMP extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void OMPLogin(String username, String password) throws InterruptedException, AWTException, IOException 
	{
		webDriver.get(Configuration.OMP_URL);
		waitForElementToAppear(SiebelOMPObj.omp.ompusername, 5);
		verifyExists(SiebelOMPObj.omp.ompusername,"User ID textbox");
		sendKeys(SiebelOMPObj.omp.ompusername, username,"User ID textbox field");
	
		verifyExists(SiebelOMPObj.omp.omppassword,"Password textbox");
		sendKeys(SiebelOMPObj.omp.omppassword, password,"Password textbox field");
		verifyExists(SiebelOMPObj.omp.omplogin,"Login button");
		click(SiebelOMPObj.omp.omplogin,"Login button");
		Reusable.waitForSiebelLoader();
		if(isElementPresent(By.xpath("//button[text()='Send anyway']")))
		{
		verifyExists(SiebelOMPObj.omp.SendButton,"Verify Send Anyway button");
		click(SiebelOMPObj.omp.SendButton,"Click on Send anyway button");
		}
		Reusable.waitForSiebelLoader();
		waitForAjax();
		//waitForElementToAppear(SiebelOMPObj.omp.InputOrderNumber, 15);
		//verifyExists(SiebelOMPObj.omp.InputOrderNumber,"Verify Correct user has been logged in");
	}


	public void verficationOfProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception {
		
		String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Subtype");
		String OCNnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Account_Number");
		String BCNnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BCN_Number");
		
		
	//	Switchtotab();
		
	//	verifyExists(SiebelOMPObj.omp.CloseVideoPopup,"Search service text");
	//	javaScriptclick1(SiebelOMPObj.omp.CloseVideoPopup,"Search Service button");
		
		waitForElementToAppear(SiebelOMPObj.omp.InputOrderNumber, 30);
		verifyExists(SiebelOMPObj.omp.InputOrderNumber,"Order Number textbox");
		sendKeys(SiebelOMPObj.omp.InputOrderNumber, OrderNumber,"Order Number textbox field");
		
		verifyExists(SiebelOMPObj.omp.SearchServiceButton,"Search service text");
		click(SiebelOMPObj.omp.SearchServiceButton,"Search Service button");
		Reusable.waitForSiebelLoader();
	
		waitForAjax();
		verifyExists(SiebelOMPObj.omp.ClickOrderReferance.replace("Value", OrderNumber),"click order reference");
		click(SiebelOMPObj.omp.ClickOrderReferance.replace("Value", OrderNumber),"click order reference");
		waitForAjax();
		waitForAjax();
		Switchtotab();
						
		if(isElementPresent(By.xpath("//div[@class='modal-header']//button[@class='close marginB15']")))
		{
			verifyExists(SiebelOMPObj.omp.CloseVideoPopup,"Search service text");
			javaScriptclick1(SiebelOMPObj.omp.CloseVideoPopup,"Search Service button");
			
		}
		
		waitForAjax();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelOMPObj.omp.ExpandAllButton,"Expand All button");
		click(SiebelOMPObj.omp.ExpandAllButton,"Expand All button");
		
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelOMPObj.omp.OrderNumberBold, 20);
		verifyExists(SiebelOMPObj.omp.OrderNumberBold,"Service Order Reference Number");
		String applicationOrderNumber = getTextFrom(SiebelOMPObj.omp.OrderNumberBold);
		Assert.assertEquals(OrderNumber, applicationOrderNumber);
		
		waitForAjax();
		
			
		waitForElementToAppear(SiebelOMPObj.omp.ProductName, 10);
		verifyExists(SiebelOMPObj.omp.ProductName,"Product Name");
		scrollDown(SiebelOMPObj.omp.ProductName);
		String applicationProductName = getTextFrom(SiebelOMPObj.omp.ProductName);
		
		
		if (!ProductName.equalsIgnoreCase("IP VPN Service")) {
		//	String parts = applicationProductName.replaceAll("\\s", "");
			//Assert.assertEquals(parts, ProductName);
			if(applicationProductName.equals(ProductName)) 
			{
				Report.LogInfo("verify","<i></b> Product name are same", "PASS");
			}
			else
			{
				Report.LogInfo("verify","<i></b> Product name is not same", "Fail");
			}
			
			
		} else {
			
			//String parts = applicationProductName.replaceAll("\\s", "");
		//	Assert.assertEquals(parts, ProductName);
			if(applicationProductName.equals(ProductName)) 
			{
				Report.LogInfo("verify","<i></b> Product name are same", "PASS");
			}
			else
			{
				Report.LogInfo("verify","<i></b> Product name is not same", "Fail");
			}
		
		}
		
		waitForAjax();
		
		waitForElementToAppear(SiebelOMPObj.omp.OrderStatus, 10);
		verifyExists(SiebelOMPObj.omp.OrderStatus,"Service Order Reference Number");
		scrollDown(SiebelOMPObj.omp.OrderStatus);
		String applicationOrderstatus = getTextFrom(SiebelOMPObj.omp.OrderStatus);
		//Assert.assertEquals("Completed", applicationOrderstatus);
		if(applicationOrderstatus.equals("Validation and Creation") || applicationOrderstatus.equals("Completed")) 
		{
			Report.LogInfo("verify","<i></b> order status completed", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> order status is not completed", "Fail");
		}
		
		waitForAjax();
		waitForElementToAppear(SiebelOMPObj.omp.OrderType, 10);
		verifyExists(SiebelOMPObj.omp.OrderType,"Service Order Reference Number");
		String applicationOrdertype = getTextFrom(SiebelOMPObj.omp.OrderType);
	//	Assert.assertEquals(OrderType, applicationOrdertype);
		if(OrderType.equals(applicationOrdertype)) 
		{
			Report.LogInfo("verify","<i></b> order type are same", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> order type is not same", "Fail");
		}
		
		
		
		waitForAjax();
		waitForElementToAppear(SiebelOMPObj.omp.OCNNumber, 10);
		verifyExists(SiebelOMPObj.omp.OCNNumber,"Service Order Reference Number");
		scrollDown(SiebelOMPObj.omp.OCNNumber);
		String applicationOCNnumber = getTextFrom(SiebelOMPObj.omp.OCNNumber);
		Assert.assertEquals(OCNnumber, applicationOCNnumber);
		
		if(OCNnumber.equals(applicationOCNnumber)) 
		{
			Report.LogInfo("verify","<i></b> Account Number are same", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> Account Number is not same", "Fail");
		}
		if ((!ProductName.equalsIgnoreCase("COLT IP VPN Wholesale"))&&(!ProductName.equalsIgnoreCase("COLT Ethernet Access Metro"))&&(!ProductName.equalsIgnoreCase("COLT IP Domain")))
		{	
			waitForElementToAppear(SiebelOMPObj.omp.BCNNumber, 10);
			verifyExists(SiebelOMPObj.omp.BCNNumber,"BCN Number");
			scrollDown(SiebelOMPObj.omp.BCNNumber);
			String applicationBCNnumber = getTextFrom(SiebelOMPObj.omp.BCNNumber);
			Assert.assertEquals(BCNnumber, applicationBCNnumber);
			
			if(BCNnumber.equals(applicationBCNnumber)) 
			{
				Report.LogInfo("verify","<i></b> BCN Number are same", "PASS");
			}
			else
			{
				Report.LogInfo("verify","<i></b> BCN Number is not same", "Fail");
			}
		}
		
		
		
		webDriver.close();
		
	}
	
	public void Switchtotab() throws Exception
	{
		String parentWinHandle = webDriver.getWindowHandle();
		Set<String> totalopenwindow = webDriver.getWindowHandles();
		for (String handle : totalopenwindow)
		{
			if (!handle.equals(parentWinHandle))
			{
				webDriver.switchTo().window(handle);
				Thread.sleep(4000);

			}
		}
		// driver.close();
		// driver.switchTo().window(parentWinHandle);
	}

}
