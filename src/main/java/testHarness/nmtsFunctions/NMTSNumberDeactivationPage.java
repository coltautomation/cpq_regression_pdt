package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberReservationObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import pageObjects.nmtsObjects.NMTSNumberDeactivationObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSNumberDeactivationPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumberRange = new NMTSAddNumberPage();
	NMTSNumberReservationPage NumberRes = new NMTSNumberReservationPage();
	String NumberDeactivationSuccessmsg;
	
	
	public void NumberDeactivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String MadeByName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String ColtandhostedNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt/Hosted_Number");
		String Availability = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
		String ActivatedNumber_OR_NMTSID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActivatedNumber/NMTS_ID");

	//	String FilterText = NumberRes.getvalue("NMTS_ID");
		
		NumberRes.UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMenu");
		waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.CeaseButton,15);
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.CeaseButton,"Cease Button");
		javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.CeaseButton,"Cease Button");
		Reusable.switchtodefault();
		waitForAjax();
		Reusable.switchtoframe("fmMain");
		//waitForAjax();
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.madeByNMTS,"made By NMTS");
		//waitForAjax();
		Reusable.Select(NMTSNumberDeactivationObj.NumberDeactivation.madeByNMTS,MadeByName);
		
		if (Country.equals("FR") || Country.equals("IT")) 
		{
			verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.Coltandhostednumber,"Colt and hosted number");
			Reusable.Select(NMTSNumberDeactivationObj.NumberDeactivation.Coltandhostednumber,ColtandhostedNumber);
		}
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.DeactivationAvail,"Availability");
		Reusable.Select(NMTSNumberDeactivationObj.NumberDeactivation.DeactivationAvail,Availability);
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.FilterBy,"Filter By");
		Reusable.Select(NMTSNumberDeactivationObj.NumberDeactivation.FilterBy,FilterBy);
		waitForAjax();
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.matching,"Matching With FilterBy");
		Reusable.Select(NMTSNumberDeactivationObj.NumberDeactivation.matching,MatchingWithFilterBy);
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.FilterText,"Filter Text");
		//sendKeys(NMTSNumberDeactivationObj.NumberDeactivation.FilterText, FilterText, "Filter Text");
		sendKeys(NMTSNumberDeactivationObj.NumberDeactivation.FilterText,ActivatedNumber_OR_NMTSID, "Filter Text");
		waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.SubmitDeactivate,15);
		verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.SubmitDeactivate,"Submit Search");
		javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.SubmitDeactivate,"Submit Search");
		waitForAjax();
		if(isElementPresent(By.xpath("//table[@id='ucxOrderHistory_grdView']//tr/td[contains(text(),'No Record Found.')]"))){
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No record found");
		} else {
			
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"OrderId");
			javaScriptDoubleclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"Order ID");
			waitForAjax();
			waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.checkbox,15);
			verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.checkbox,"checkbox");
			javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.checkbox,"checkbox");
			waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.SelectedAllLink,15);
			verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.SelectedAllLink,"Selected All Link");
			javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.SelectedAllLink,"Selected All Link");
			waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.AllocationCheckboxagain,15);
			verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.AllocationCheckboxagain,"Selected All Link");
			javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.AllocationCheckboxagain,"Selected All Link");
			waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.CancelQuarntined,15);
			verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.CancelQuarntined,"Cancel Quarntined Button");
			javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.CancelQuarntined,"Cancel Quarntined Button");
			Reusable.switchtoframe("popupFrame");
			waitForElementToBeVisible(NMTSNumberDeactivationObj.NumberDeactivation.yespopup,15);
			verifyExists(NMTSNumberDeactivationObj.NumberDeactivation.yespopup,"yes popup");
			javaScriptclick(NMTSNumberDeactivationObj.NumberDeactivation.yespopup,"yes popup");
			waitForAjax();
			waitForAjax();
			Reusable.switchtodefault();
			Reusable.switchtoframe("fmMain");
			waitForAjax();
			NumberDeactivationSuccessmsg = getTextFrom(NMTSNumberDeactivationObj.NumberDeactivation.Deactivationsuccessmsg,"Deactivated message");
			
			if(NumberDeactivationSuccessmsg.equals("Ceased"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number Deactivated successfully");	
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Not Deactivated successfully");	
				System.out.println("Number Not Deactivated successfuly");
			}
			
		
	}
	}	
}	
