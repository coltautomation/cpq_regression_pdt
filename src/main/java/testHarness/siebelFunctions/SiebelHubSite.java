package testHarness.siebelFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelHubSite extends SeleniumUtils {
	
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	SiebelMode SiebelMod = new SiebelMode();

	public void hubSiteCustomize(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{

		String ResilienceOptionValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		String ServiceBandwidthValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String OSSFlagValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
		String Aend_Street_NameValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Street_Name");
		String Aend_CountryValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
		String Aend_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");
		String Aend_Postal_CodeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
		String Aend_PremisesValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Premises");
		String Service_Party_NameValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");
		String Site_LastNameValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_LastName");
		String Access_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String Access_TechnologyValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Technology");
		String Building_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Type");
		String Customer_Site_Pop_StatusValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Site_Pop_Status");
		String Cabinet_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_Type");
		String Presentation_InterfaceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
		String Connector_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String Fibre_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String Install_TimeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
		
	
		Reusable.waitForSiebelLoader();
	   // Reusable.ConnectorTypeDropdownSelection("Connector Type","RJ-45");
	    Reusable.AEndDropdownSelection("Connector Type","LC");

		waitForElementToAppear(SiebelHubSiteObj.Customize.serviceBandwidthDropdownAccess, 10);
		click(SiebelHubSiteObj.Customize.serviceBandwidthDropdownAccess,"Service Bandwidth drop down");
		verifyExists(SiebelHubSiteObj.Customize.serviceBandwidthSelectAccess.replace("value",ServiceBandwidthValue),"Service Bandwidth Value");
		click(SiebelHubSiteObj.Customize.serviceBandwidthSelectAccess.replace("value",ServiceBandwidthValue),"Service Bandwidth Value");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Resilience Option"),"Resilience Option");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Resilience Option"),"Resilience Option");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", ResilienceOptionValue),"Resilience Option value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", ResilienceOptionValue),"Resilience Option value");
		Reusable.waitForSiebelLoader();
		/*
		//waitForElementToAppear(SiebelHubSiteObj.Customize.serviceBandwidthDropdownAccess, 10);
		//click(SiebelHubSiteObj.Customize.serviceBandwidthDropdownAccess,"Service Bandwidth drop down");
		//verifyExists(SiebelHubSiteObj.Customize.serviceBandwidthSelectAccess.replace("Value", ServiceBandwidthValue),"Service Bandwidth Value");
		//click(SiebelHubSiteObj.Customize.serviceBandwidthSelectAccess.replace("Value", ServiceBandwidthValue),"Service Bandwidth Value");
		*/
		Reusable.waitForSiebelSpinnerToDisappear();
//		Reusable.ClickHereSave();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Hard Modify Flag"),"Hard Modify Flag drop down");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Hard Modify Flag"),"Hard Modify Flag drop down");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", "N"),"Hard Modify Flag value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", "N"),"Hard Modify Flag value");
		verifyExists(SiebelHubSiteObj.Customize.textInput.replace("Value", "OSS Platform Flag"),"OSS Platform Flag");
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "OSS Platform Flag"), OSSFlagValue, "OSS Flag Value");
		///
		
		waitForElementToAppear(SiebelHubSiteObj.Customize.clickheretoSaveAccess, 10);
		click(SiebelHubSiteObj.Customize.clickheretoSaveAccess," Click Here to save Access link");
		Reusable.waitForSiebelLoader();
		/*
		verifyExists(SiebelHubSiteObj.Customize.SearchAddressSiteA,"Search Address site A");
		javaScriptclick(SiebelHubSiteObj.Customize.SearchAddressSiteA);
		verifyExists(SiebelHubSiteObj.Customize.StreetNamerfs,"Street Name Value");
		sendKeys(SiebelHubSiteObj.Customize.StreetNamerfs, Aend_Street_NameValue, "Street Name Value");
		verifyExists(SiebelHubSiteObj.Customize.Country,"County");
		click(SiebelHubSiteObj.Customize.Country,"County");
		verifyExists(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Aend_CountryValue),"Country Value");
		click(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Aend_CountryValue),"Country Value");

	
		verifyExists(SiebelHubSiteObj.Customize.City,"City");
		sendKeys(SiebelHubSiteObj.Customize.City, Aend_City, "City");
		
		verifyExists(SiebelHubSiteObj.Customize.PostalCode,"Postal Code");
		sendKeys(SiebelHubSiteObj.Customize.PostalCode, Aend_Postal_CodeValue, "Postal Code");
		
		verifyExists(SiebelHubSiteObj.Customize.Premises,"Premises");
		sendKeys(SiebelHubSiteObj.Customize.Premises, Aend_PremisesValue, "Premises");
		
		verifyExists(SiebelHubSiteObj.Customize.Search,"Search");
		click(SiebelHubSiteObj.Customize.Search,"Search");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
	
		verifyExists(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
		click(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
		
		verifyExists(SiebelHubSiteObj.Customize.PickAddress,"Pick Address");
		click(SiebelHubSiteObj.Customize.PickAddress,"Pick Address");
		
		verifyExists(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
		click(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelHubSiteObj.Customize.PickBuilding,"Pick Building");
		click(SiebelHubSiteObj.Customize.PickBuilding,"Pick Building");
		
		verifyExists(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
		click(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelHubSiteObj.Customize.PickSite,"Pick Site");
		click(SiebelHubSiteObj.Customize.PickSite,"Pick Site");
		*/
		SiebelMod.addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForSiebelLoader();
		
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
	//////// Code for Technical Contact
		verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
		click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
		javaScriptclick(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
		click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
		click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
		Reusable.waitForSiebelLoader();
//		Save();
		Reusable.waitForSiebelSpinnerToDisappear();

		//////////////////////////
		
		verifyExists(SiebelHubSiteObj.Customize.ServicePartySearchAccess,"Service Party Search Access");

		//scrollIntoView(webDriver.findElement(By.xpath(SiebelHubSiteObj.Customize.ServicePartySearchAccess)));
		javaScriptclick(SiebelHubSiteObj.Customize.ServicePartySearchAccess,"Service Party Search Access");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelHubSiteObj.Customize.ServicePartyDropdownAccess,"Service Party Drop down Access");
		click(SiebelHubSiteObj.Customize.ServicePartyDropdownAccess,"Service Party Drop down Access");
		verifyExists(SiebelHubSiteObj.Customize.PartyNameAccess,"Party Name Access");
		click(SiebelHubSiteObj.Customize.PartyNameAccess,"Party Name Access");
		sendKeys(SiebelHubSiteObj.Customize.InputPartyNameAccess, Service_Party_NameValue, "Service Party Name Value");
		
		verifyExists(SiebelHubSiteObj.Customize.PartyNameSearchAccess,"Party Name Search Access");
		click(SiebelHubSiteObj.Customize.PartyNameSearchAccess,"Party Name Search Access");
		verifyExists(SiebelHubSiteObj.Customize.PartyNameSubmitAccess,"Party Name submit Access");
		javaScriptclick(SiebelHubSiteObj.Customize.PartyNameSubmitAccess,"Party Name submit Access");
		Reusable.waitForSiebelLoader();

		waitForElementToAppear(SiebelHubSiteObj.Customize.SiteContactSearchAccess, 10);
		javaScriptclick(SiebelHubSiteObj.Customize.SiteContactSearchAccess,"Site Contact Search Access");
		/*
		verifyExists(SiebelHubSiteObj.Customize.SiteContactDropdownAccess,"Site Contact Drop down Access");
		click(SiebelHubSiteObj.Customize.SiteContactDropdownAccess,"Site Contact Drop down Access");
		verifyExists(SiebelHubSiteObj.Customize.InputSiteNameAccess,"Site Name");
		sendKeys(SiebelHubSiteObj.Customize.InputSiteNameAccess, Site_LastNameValue, "Site Name");
		
		verifyExists(SiebelHubSiteObj.Customize.LastNameSiteSearchAccess,"Search");
		click(SiebelHubSiteObj.Customize.LastNameSiteSearchAccess,"Search");
		*/
		Reusable.waitForSiebelLoader();
	
       
		verifyExists(SiebelHubSiteObj.Customize.SelectContact,"Select Contact");
		click(SiebelHubSiteObj.Customize.SelectContact,"Select Contact");
	  
		
		verifyExists(SiebelHubSiteObj.Customize.LastNameSiteSubmitAccess,"Submit");
		click(SiebelHubSiteObj.Customize.LastNameSiteSubmitAccess,"Submit");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
		//verifyExists(SiebelHubSiteObj.Customize.IpGurdianSave,"Ip Gurdian Save");
		//click(SiebelHubSiteObj.Customize.IpGurdianSave,"Ip Gurdian Save");
		Reusable.waitForSiebelLoader();
		
//		Access Type	
			verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Access Type"),"Access Type");
			click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Access Type"),"Access Type");
			verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TypeValue),"Access Type value");
			click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TypeValue),"Access Type value");
			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
					click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
					Reusable.waitForSiebelLoader();
			}
			Reusable.waitForSiebelSpinnerToDisappear();
		
		
		//Access Technology
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Access Technology"),"Access Technology");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Access Technology"),"Access Technology");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TechnologyValue),"Access Technology Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TechnologyValue),"Access Technology Value");
		
			
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Building Type"),"Building Type");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Building Type"),"Building Type");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Building_TypeValue),"Building Type Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Building_TypeValue),"Building Type Value");
		
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Customer Site Pop Status"),"Customer Site Pop Status");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Customer Site Pop Status"),"Customer Site Pop Status");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Customer_Site_Pop_StatusValue),"Customer Site Pop Status value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Customer_Site_Pop_StatusValue),"Customer Site Pop Status value");
		
	
		clearTextBox(SiebelHubSiteObj.Customize.textInput.replace("Value", "3rd Party Connection Reference"));
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "3rd Party Connection Reference"), "As12", "3rd Party Connection Reference");
		clearTextBox(SiebelHubSiteObj.Customize.textInput.replace("Value", "BCP Reference"));
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "BCP Reference"), "As12", "BCP Reference");
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Cabinet Type"),"Cabinet Type");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Cabinet Type"),"Cabinet Type");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Cabinet_TypeValue),"Cabinet Type value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Cabinet_TypeValue),"Cabinet Type value");
		
		clearTextBox(SiebelHubSiteObj.Customize.textInput.replace("Value", "Cabinet ID"));
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Cabinet ID"), "12", "Cabinet ID");
		Reusable.SendkeaboardKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Cabinet ID"),Keys.ENTER);
		clearTextBox(SiebelHubSiteObj.Customize.textInput.replace("Value", "Shelf ID"));
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Shelf ID"), "12", "Shelf ID");
		Reusable.SendkeaboardKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Shelf ID"),Keys.ENTER);

		clearTextBox(SiebelHubSiteObj.Customize.textInput.replace("Value", "Slot ID"));
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Slot ID"), "12", "Slot ID");
		Reusable.SendkeaboardKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Slot ID"),Keys.ENTER);

		clearTextBox(SiebelHubSiteObj.Customize.textInput.replace("Value", "Physical Port ID"));
		sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Physical Port ID"), "12", "Physical Port ID");
		Reusable.SendkeaboardKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "Physical Port ID"),Keys.ENTER);
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Presentation Interface"),"Presentation Interface");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Presentation Interface"),"Presentation Interface");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Presentation_InterfaceValue),"Presentation Interface value");
		if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
			verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				Reusable.waitForSiebelLoader();
		}
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
	
		//Connector Type
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Connector Type"),"Connector Type");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Connector Type"),"Connector Type");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Connector_TypeValue),"Connector Type Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Connector_TypeValue),"Connector Type Value");
	

		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Fibre Type"),"Fibre Type");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Fibre Type"),"Fibre Type");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Fibre_TypeValue),"Fibre Type Value");
		
		
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Port Role"),"Port Role");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Port Role"),"Port Role");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value","Physical Port"),"Physical Port Value");
		
		verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Install Time"),"Install Time");
		click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Install Time"),"Install Time");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Install_TimeValue),"Install Time Value");
		Reusable.waitForSiebelLoader();		

		//Reusable.ClickHereSave();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}
	

}
