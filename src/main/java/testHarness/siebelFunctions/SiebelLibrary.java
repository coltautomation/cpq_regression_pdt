package testHarness.siebelFunctions;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.util.DocumentFormatException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelLiabrary_Obj;
import testHarness.commonFunctions.ReusableFunctions;

import testHarness.siebelFunctions.SiebelLoginPage;


public class SiebelLibrary extends SeleniumUtils{
	SiebelLoginPage Login = new SiebelLoginPage();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void ServiceTab_Bespoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{

		String ServiceOrderReference_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Order");
		String Opportunity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Opportunity#");
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Existing_Capacity_Lead_Time_Primary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Capacity_ Lead_Time_Primary");
		String Maintenance_Contact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Contact");
		String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String Maintenance_Party = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Party");
		String Maintenance_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Address");

		Reusable.waitForSiebelLoader();

		try {
			verifyExists(SiebelLiabrary_Obj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			click(SiebelLiabrary_Obj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		} catch (Exception e) {
			try {
				javaScriptclick(SiebelLiabrary_Obj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		}
		Reusable.waitForSiebelLoader();
		//		waitToPageLoad();
		verifyExists(SiebelLiabrary_Obj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		click(SiebelLiabrary_Obj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		sendKeys(SiebelLiabrary_Obj.ServiceTab.InputServiceOrder,ServiceOrderReference_Number,"Service OrderReference Number");
		verifyExists(SiebelLiabrary_Obj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
		click(SiebelLiabrary_Obj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelLiabrary_Obj.ServiceTab.ServiceOrderReferenceNo, "Verify ServiceOrderReferenceNo");
		click(SiebelLiabrary_Obj.ServiceTab.ServiceOrderReferenceNo, "Click on ServiceOrderReferenceNo");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
	}
	public void Bespoke_Journey(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		
		Reusable.waitForSiebelLoader();
		if(isVisible(SiebelLiabrary_Obj.ServiceTab.BespokeTab))
	 {	
		ScrollIntoViewByString(SiebelLiabrary_Obj.ServiceTab.BespokeTab);
		verifyExists(SiebelLiabrary_Obj.ServiceTab.BespokeTab,"Verify Bespoke Tab");
		click(SiebelLiabrary_Obj.ServiceTab.BespokeTab,"Click Bespoke Tab");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelLiabrary_Obj.ServiceTab.BespokeSummary_Popup,"Verify Bespoke Summary Popup");
		
		verifyExists(SiebelLiabrary_Obj.ServiceTab.CutomerView,"Verify Cutomer View Section");
		
		verifyExists(SiebelLiabrary_Obj.ServiceTab.ColtView,"Verify Colt View Section");
		
		verifyExists(SiebelLiabrary_Obj.ServiceTab.BespokePopupClose,"Verify Close Button");
		click(SiebelLiabrary_Obj.ServiceTab.BespokePopupClose,"Click on Close button");

		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelLiabrary_Obj.ServiceTab.BespokeReferance,"Verify Bespoke Referance");
	//	click(SiebelLiabrary_Obj.ServiceTab.BespokeReferance,"Click Bespoke Referance");	
		Reusable.waitForSiebelLoader();
	/*	
		System.setProperty("webdriver.chrome.driver","E:\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		 driver.get("https://login.microsoftonline.com/b859cf7e-ff8a-40bb-bd0f-da56e6dc0eb8/oauth2/authorize?client_id=00000003-0000-0ff1-ce00-000000000000&response_mode=form_post&protectedtoken=true&response_type=code%20id_token&resource=00000003-0000-0ff1-ce00-000000000000&scope=openid&nonce=99ED715D41A11769F5E81EEB6846B562DE6C3BE09DCC29FB-898ADA961BACB1861B27457702BDBC04A8226ADCB73A497171879F535CECF234&redirect_uri=https%3A%2F%2Fcoltinternal.sharepoint.com%2F_forms%2Fdefault.aspx&claims=%7B%22id_token%22%3A%7B%22xms_cc%22%3A%7B%22values%22%3A%5B%22CP1%22%5D%7D%7D%7D&wsucxt=1&cobrandid=11bd8083-87e0-41b5-bb78-0bc43c8a8e8a&client-request-id=a190b99f-e029-c000-1473-0e9b4ef3f82c&sso_reload=true");
		
		 driver.findElement(By.name("loginfmt")).sendKeys("Arati.Amate@colt.net");	
		 driver.findElement(By.id("idSIButton9")).submit();
	        driver.findElement(By.name("submit")).submit();		
		// driver.findElement(By.name("cusid")).sendKeys("53920");	
		 driver.findElement(By.className("lightbox-cover"));
		mouseMoveOn(SiebelLiabrary_Obj.ServiceTab.popupbox);
		verifyExists(SiebelLiabrary_Obj.ServiceTab.ColtMailIdTextbox);
		sendKeys(SiebelLiabrary_Obj.ServiceTab.ColtMailIdTextbox,"Arati.Amate@colt.net","Enter Colt Id Field");

		mainPopup();
	
//		mouseOverAction(SiebelModeObj.ServiceTab.ColtMailIdTextbox);
//		ScrollIntoViewByString(SiebelModeObj.ServiceTab.ColtMailIdTextbox);
		verifyExists(SiebelLiabrary_Obj.ServiceTab.ColtMailIdTextbox,"Verify Colt Id Field");
	//	click(SiebelModeObj.ServiceTab.ColtMailIdTextbox,"Click Colt Id Field");	
		sendKeys(SiebelLiabrary_Obj.ServiceTab.ColtMailIdTextbox,"Arati.Amate@colt.net","Enter Colt Id Field");
		
		verifyExists(SiebelLiabrary_Obj.ServiceTab.Nextbutton,"Verify Next Button");
		click(SiebelLiabrary_Obj.ServiceTab.Nextbutton,"Click on Next button");
		
		Reusable.waitForSiebelLoader();
		*/Reusable.waitForSiebelLoader();
		//Thread.sleep(30000);
		if(isVisible(SiebelLiabrary_Obj.ServiceTab.sharepointlink))
		{
			verifyExists(SiebelLiabrary_Obj.ServiceTab.sharepointlink,"Verify Share point link");
			verifyExists(SiebelLiabrary_Obj.ServiceTab.SharepointDocument,"Verify Share point link Documents");
			
		}
		
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
	}
		else
		{
			System.out.println("Bespoke Description is not displayed");
		}
	}
				

	    public String mainPopup() throws InterruptedException {									
	        	WebDriver driver=new ChromeDriver();			
	        		
	        //Launching the site.				
	            driver.get("https://login.microsoftonline.com");			
	        driver.manage().window().maximize();		
	                		
	driver.findElement(By.xpath("//input[@aria-label='Enter your email, phone, or Skype.']")).click();	
	
	driver.findElement(By.xpath("//input[@aria-label='Enter your email, phone, or Skype.']")).sendKeys("Arati.Amate@colt.net");    		
	 
	driver.findElement(By.xpath("//input[@type='submit'] ")).click();
//	driver.findElement(By.name(SiebelLiabrary_Obj.ServiceTab.Nextbutton)).click();
       
	  // Switching to Alert        
    Alert alert = driver.switchTo().alert();		
    		
    // Capturing alert message.    
    String alertMessage= driver.switchTo().alert().getText();
       driver.switchTo().alert().sendKeys("AAmate");
            
	String MainWindow=driver.getWindowHandle();		
	        		
	        // To handle all new opened window.				
	            Set<String> s1=driver.getWindowHandles();		
	        Iterator<String> i1=s1.iterator();		
	        		
	        while(i1.hasNext())			
	        {		
	            String ChildWindow=i1.next();		
	            		
	            if(MainWindow.equalsIgnoreCase(ChildWindow))			
	            {    		
	                 
	                    // Switching to Child window
	                    driver.switchTo().window(ChildWindow);	                                                                                                           
	                    driver.findElement(By.name(SiebelLiabrary_Obj.ServiceTab.ColtMailIdTextbox))
	                    .sendKeys("Arati.Amate@colt.net");                			
	                    
	                    //driver.findElement(By.name("btnLogin")).click();	
	                    driver.findElement(By.name(SiebelLiabrary_Obj.ServiceTab.Nextbutton)).click();
	                          
	                    driver.switchTo().alert().sendKeys("AAmate");
	                    
	                    
				// Closing the Child Window.
	                        driver.close();		
	            }		
	        }		
	        // Switching to Parent window i.e Main Window.
	            driver.switchTo().window(MainWindow);
				return MainWindow;				
	    }
	
	public String Siebel_Operations(String testDataFile,String tsSheetName,String scriptNo, String dataSetNo, String Service_Order) throws Exception {

		String result = null;
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Product_Name");
		String ServOrder = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Service_Order");
		
	
	//	openBrowser(g.getBrowser());		
	//	openurl(Configuration.Siebel_URL);
		Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
		
		verifyExists(SiebelLiabrary_Obj.ServiceTab.ClickLink.replace("Value", "Customer Orders"));
		click(SiebelLiabrary_Obj.ServiceTab.ClickLink.replace("Value", "Customer Orders"));
		Reusable.waitForSiebelLoader();
//		Splitting the Customer order from service order
		String[] parts = ServOrder.split("/");
		String part1 = parts[0];
		String part2 = parts[1];
		Reusable.waitForSiebelLoader();
		
		sendKeys(SiebelLiabrary_Obj.ServiceTab.CustOrder, part2);
		click(SiebelLiabrary_Obj.ServiceTab.CustOrderGo,"CustOrderGo");
		Reusable.waitForSiebelLoader();
		
		click(SiebelLiabrary_Obj.ServiceTab.ClickSeibelOrder,"Click Seibel Order");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelLiabrary_Obj.ServiceTab.NewServiceOrder,"New Service Order");
		Reusable.waitForSiebelLoader();
		//calling below method for create customer order
		createCustomerOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);
		
		//Search Service Order
		ServiceTab_Bespoke(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
		//Enter in header	
		enterMandatoryFieldsInHeader(testDataFile, tsSheetName, scriptNo, dataSetNo);
	
		//Fetch Network Reference
		Fetch_Network_Reference(testDataFile, tsSheetName, scriptNo, dataSetNo);
		
		//Middle Applet Entry
				MiddleApplet_Entry(testDataFile, tsSheetName, scriptNo, dataSetNo);
		
		//		Calling the below function to select service party and site contact entry
				if (!Product_Name.equalsIgnoreCase("EthernetSpoke")) {
				SiteOperation_AEnd(testDataFile, tsSheetName, scriptNo, dataSetNo);
				}
				if (!Product_Name.equalsIgnoreCase("EthernetHub") && !Product_Name.equalsIgnoreCase("ColtIpAccess")) {
					SiteOperation_BEnd(testDataFile, tsSheetName, scriptNo, dataSetNo);
				}
				//Calling the below method to enter and save order dates
				EnterDateInFooter(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
//				Calling the below method to enter the billing details
				EnterBillingDateInFooter(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
				return result;
	}
	
	public void SiebelLogin(String username, String password) throws InterruptedException, AWTException, IOException 
	{
		
	//	webDriver.get(Configuration.Siebel_URL);
		Reusable.waitForSiebelLoader();
		if (isElementPresent(By.xpath("//*[@id='details-button']")))
		{
		verifyExists(SiebelLiabrary_Obj.Login.Advanced,"Advanced button");
		click(SiebelLiabrary_Obj.Login.Advanced,"Advanced button");
		
		verifyExists(SiebelLiabrary_Obj.Login.Navigate,"Navigate button");
		click(SiebelLiabrary_Obj.Login.Navigate,"Navigate button");
		}
		waitForElementToAppear(SiebelLiabrary_Obj.Login.userNameTxb, 5);
		verifyExists(SiebelLiabrary_Obj.Login.userNameTxb,"User ID textbox");
		sendKeys(SiebelLiabrary_Obj.Login.userNameTxb, username,"User ID textbox field");
	
		verifyExists(SiebelLiabrary_Obj.Login.passWordTxb,"Password textbox");
		sendKeys(SiebelLiabrary_Obj.Login.passWordTxb, password,"Password textbox field");
		verifyExists(SiebelLiabrary_Obj.Login.loginBtn,"Login button");
		click(SiebelLiabrary_Obj.Login.loginBtn,"Login button");
		
		waitForAjax();
		Reusable.waitForSiebelLoader();
	//	waitForElementToAppear(SiebelLoginObj.Login.SiebelUserinfo1, 10);
	//	verifyExists(SiebelLoginObj.Login.SiebelUserinfo1,"Verify Correct user has been logged in");
		
			
	}
	public void NavigateToAccounts() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelLiabrary_Obj.Accounts.accountsTab, 10);
		verifyExists(SiebelLiabrary_Obj.Accounts.accountsTab,"Account Tab");
		click(SiebelLiabrary_Obj.Accounts.accountsTab,"Account Tab");
		waitForAjax();
		verifyExists(SiebelLiabrary_Obj.Accounts.OCNTxb,"Account Number");	
		
	}
	
	public void SearchAccount(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Account_Number");
		verifyExists(SiebelLiabrary_Obj.Accounts.OCNTxb,"Account Number");	
			sendKeys(SiebelLiabrary_Obj.Accounts.OCNTxb, OCN, "Account Number Textbox");
			verifyExists(SiebelLiabrary_Obj.Accounts.GoBtn,"Go Button");
			click(SiebelLiabrary_Obj.Accounts.GoBtn,"Go Button");
			Reusable.waitForSiebelLoader();
			ClickonElementByString("//td[text()='"+OCN+"']//following-sibling::*//a", 30);
			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelLiabrary_Obj.Accounts.InstalledAssets_NewBtn, 10);
			verifyExists(SiebelLiabrary_Obj.Accounts.InstalledAssets_NewBtn,"Installed Assets New Button");
			click(SiebelLiabrary_Obj.Accounts.InstalledAssets_NewBtn,"Installed Assets New Button");
			Reusable.waitForSiebelLoader();
		
		
	}

public void openServiceOrderNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception {
		
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelLiabrary_Obj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelLiabrary_Obj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		String seriveOrderRefNum = getTextFrom(SiebelLiabrary_Obj.serviceOrder.ServiceOrderReferenceNo);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceOrderReference_Number", seriveOrderRefNum);	
		click(SiebelLiabrary_Obj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();
	
	}
public void createCustomerOrder(String testDataFile, String tsSheetName,String scriptNo,String dataSetNo) throws Exception {
	
	String OpportunityNumber = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Opportunity#");
	String Delivery_Channel = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Delivery_Channel");
	String Ordering_Party = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Ordering_Party");
	String Ordering_Party_Address = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Ordering_Party_Address");
	String Ordering_Party_Contract = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Ordering_Party_Contract");
	String Sales_Channel = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Sales_Channel");
													
	String Contracting_City = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Contracting_City");
	
	verifyExists(SiebelLiabrary_Obj.CustomerOrderPage.DeliveryChannel,"Delivery Channel textbox");
	sendKeys(SiebelLiabrary_Obj.CustomerOrderPage.DeliveryChannel, Delivery_Channel,"Enter Delivery Channel");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelLiabrary_Obj.CustomerOrderPage.SalesChannel,"Sales Channel");
	sendKeys(SiebelLiabrary_Obj.CustomerOrderPage.SalesChannel,Sales_Channel, "Enter Sales Channel");
	
//	Selecting the Contracting City
	String Contract_ID = getAttributeFrom(SiebelLiabrary_Obj.CustomerOrderPage.contractLabelTxb, "value");
	Select_Contracting_City(Contract_ID, Contracting_City);

	
	Reusable.savePage();
	
/*	
	verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.NewServiceOrder,"New Service Order");
	click(SiebelQuoteCreationObj.CustomerOrderPage.NewServiceOrder, "Click on New Service Order");
*/	Reusable.waitForSiebelLoader();
}
public String Select_Contracting_City(String Contract_ID, String Contracting_City) throws IOException, InterruptedException {
	
	String sResult = null;
	Reusable.waitForSiebelLoader();
	
//	Entering the Credentials
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.contractLookup))
	{
		click(SiebelLiabrary_Obj.ServiceEntry.contractLookup, "Click on contractLookup");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.popUpFilterQueryLst,"New Service Order");
		click(SiebelLiabrary_Obj.ServiceEntry.popUpFilterQueryLst);
		
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.popUpFilterQuerySubLst,"New Service Order");
		click(SiebelLiabrary_Obj.ServiceEntry.popUpFilterQuerySubLst);
		
		//selectByValueDIV(SiebelQuoteCreationObj.ServiceEntry.popUpFilterQueryLst, SiebelQuoteCreationObj.ServiceEntry.popUpFilterQuerySubLst, "Contract Id");
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.popUpQueryTxb,Contract_ID);
		click(SiebelLiabrary_Obj.ServiceEntry.popUpContractQuerySearchBtn);
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.contractIDTxb,Contract_ID);
		Reusable.SendkeaboardKeys(SiebelLiabrary_Obj.ServiceEntry.contractIDTxb,Keys.ENTER);
		Reusable.waitForSiebelLoader();
		click(SiebelLiabrary_Obj.ServiceEntry.contractCityCell);
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.contractCityLst,"Verify Contract City");
		click(SiebelLiabrary_Obj.ServiceEntry.contractCityLst);
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.contractCitySubLst,"Verify Contract City from drop down");
		click(SiebelLiabrary_Obj.ServiceEntry.contractCitySubLst);
	//	selectByValueDIV(SiebelQuoteCreationObj.ServiceEntry.contractCityLst, SiebelQuoteCreationObj.ServiceEntry.contractCitySubLst,Contracting_City);
		Reusable.waitForSiebelLoader();
		click(SiebelLiabrary_Obj.ServiceEntry.popUpOKBtn);
		Reusable.waitForSiebelLoader();
	}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "contractLookup in Customer Order Page is not visible, Please Verify");
			System.out.println("contractLookup in Customer Order Page is not visible, Please Verify");
		
		}
	return sResult;
}
public void enterMandatoryFieldsInHeader(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
{
	waitToPageLoad(); 
	String OrderSubtype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Subtype");
	String Order_Processing_User = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Processing_User");


	Reusable.waitForSiebelLoader();
	if (isVisible(SiebelLiabrary_Obj.ServiceEntry.showFullInfoLnk))
	{
		scrollIntoTop();
	}
	else{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLnk in Service Order Page is not visible, Please Verify");
		System.out.println("showFullInfoLnk in Service Order Page is not visible, Please Verify");
	}
/*	Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch, 10);
	verifyExists(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch,"Order Sub Type Search");
	click(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch,"Order Sub Type Search");
	Reusable.waitForSiebelLoader();

	Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType, 10);
	verifyExists(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType,"Add Order Sub Type");
	click(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType,"Add Order Sub Type");
	Reusable.waitForSiebelLoader();
	sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputOrderSubType, OrderSubtype, "Input Order Sub Type");
	Reusable.waitForSiebelLoader();
	Robot robot = new Robot();
	// robot.keyPress(KeyEvent.VK_ENTER);
	waitForAjax();
	Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType, 10);
	verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType,"Submit Sub Order Type");
	click(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType,"Submit Sub Order Type");
	Reusable.waitForSiebelLoader();
	//	waitforPagetobeenable();
*/
//	calling the below method to enter order processing user
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.orderProcessingUserLookup,"Verify Order processing Lookup");
	click(SiebelLiabrary_Obj.ServiceEntry.orderProcessingUserLookup,"Click on Order Processing Lookup");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelLiabrary_Obj.ServiceEntry.popUpFilterQueryLst,"Verify Order processing dropdown");
	click(SiebelLiabrary_Obj.ServiceEntry.popUpFilterQueryLst,"Click on Order Processing dropdown");

	verifyExists(SiebelLiabrary_Obj.ServiceEntry.orderProcessingOption.replace("Value","Login User"));
	click(SiebelLiabrary_Obj.ServiceEntry.orderProcessingOption.replace("Value","Login User"));
	
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.popUpQueryTxb,"Verify Order processing Value");
	click(SiebelLiabrary_Obj.ServiceEntry.popUpQueryTxb,"Click on Order Processing Value");

	sendKeys(SiebelLiabrary_Obj.ServiceEntry.popUpQueryTxb,Order_Processing_User,"Order processing Value");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.popUpGOBtn,"Verify Go Button");
	click(SiebelLiabrary_Obj.ServiceEntry.popUpGOBtn,"Click on Go button");
	Reusable.waitForSiebelLoader();
	click(SiebelLiabrary_Obj.ServiceEntry.promotionCodeTxb,"Click on Promotion Code");
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	waitToPageLoad();
}
public String Fetch_Network_Reference(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	if (Product_Name.equalsIgnoreCase("EthernetHub")) {
	Reusable.waitForSiebelLoader();
	if(waitForElementToBeVisible(SiebelLiabrary_Obj.ServiceTab.NetworkRefField, 60))
	{
		String Network_Reference = getTextFrom(SiebelLiabrary_Obj.ServiceTab.NetworkRefField);
		DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Network_Reference", Network_Reference);
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "networkReferenceTxb in Service Order Page is not visible, Please Verify");
		System.out.println("networkReferenceTxb in Service Order Page is not visible, Please Verify");
	}
	Reusable.waitForSiebelLoader();
	}
return "True";
}
public String MiddleApplet_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
	
//	Initialize the Variable
	String OSS_Platform_Flag = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"OSS_Platform_Flag");
	String SubSea_Worker_Path = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"SubSea_Worker_Path");
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Router_Technology = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Router_Technology");
	String Layer3_Resilience = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Layer3_Resilience");
	String sResult;
	
	Reusable.waitForSiebelLoader();
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.ossPlatformFlag))
	{
	 //OSS Platform Flag
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.ossPlatformFlag,"Verify Input oss platform flag");
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.ossPlatformFlag,OSS_Platform_Flag);
		Reusable.SendkeaboardKeys(SiebelLiabrary_Obj.ServiceEntry.ossPlatformFlag, Keys.TAB);

		Reusable.waitForSiebelLoader();
		if(Product_Name.toString().equalsIgnoreCase("Wave"))
		{
			//Sub Sea Cable System (Worker Path)
			verifyExists(SiebelLiabrary_Obj.ServiceEntry.MiddleDropDown.replace("Value", "Sub Sea Cable System (Worker Path)"),"Sub Sea Cable System (Worker Path)");
			click(SiebelLiabrary_Obj.ServiceEntry.MiddleDropDown.replace("Value", "Sub Sea Cable System (Worker Path)"),"Sub Sea Cable System (Worker Path)");
			Reusable.waitForSiebelLoader();

			click(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", SubSea_Worker_Path),"Sub Sea Cable System (Worker Path)");

		}
		
		if(Product_Name.toString().equalsIgnoreCase("ColtIpAccess"))
		{
			verifyExists(SiebelLiabrary_Obj.ServiceEntry.RouterTypeDropdownAccess,"Verify SelectRouterTypeDropDownAccess");
			click(SiebelLiabrary_Obj.ServiceEntry.RouterTypeDropdownAccess,"Click On RouterTypeDropDownAccess");
			verifyExists(SiebelLiabrary_Obj.ServiceEntry.SelectRouterTypeDropDownAccess.replace("Value", Router_Technology),"Verify RouterTypeDropDownAccess");
			click(SiebelLiabrary_Obj.ServiceEntry.SelectRouterTypeDropDownAccess.replace("Value", Router_Technology),"Click On RouterTypeDropDownAccess");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelLiabrary_Obj.ServiceEntry.Layer3ResillanceDropdownAccess,"Verify Layer3ResillanceDropdownAccess");
			click(SiebelLiabrary_Obj.ServiceEntry.Layer3ResillanceDropdownAccess,"Click On Layer3ResillanceDropdownAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelLiabrary_Obj.ServiceEntry.Layer3ResillancePartialSelectDropdownAccess.replace("Value", Layer3_Resilience),"Verify Layer3ResillancePartialSelectDropdownAccess");
			click(SiebelLiabrary_Obj.ServiceEntry.Layer3ResillancePartialSelectDropdownAccess.replace("Value", Layer3_Resilience),"Click On Layer3ResillancePartialSelectDropdownAccess");
			Reusable.waitForSiebelLoader();
		}
		
	}
	else{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "networkReferenceTxb in Service Order Page is not visible, Please Verify");
		System.out.println("networkReferenceTxb in Service Order Page is not visible, Please Verify");
	
	}
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	ExtentTestManager.getTest().log(LogStatus.PASS, "Middle Applet details have been entered Successfully");
	System.out.println("Middle Applet details have been entered Successfully");
	
	return "True";
}
public String SiteOperation_AEnd(String file_name,String Sheet_Name,String iScript, String iSubScript) throws Exception 
{
	String result = null;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	
	
		SiteAServiceParty(file_name,Sheet_Name,iScript,iSubScript);
		PickServiceParty(file_name,Sheet_Name,iScript,iSubScript);
		SiteASiteContact(file_name,Sheet_Name,iScript,iSubScript);
		PickSiteContactParty(file_name,Sheet_Name,iScript,iSubScript);
		Reusable.alertPopUp();
		//ClickHereSave();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		if (!Product_Name.equalsIgnoreCase("Wave")) {
			SiteInfo_AEnd(file_name,Sheet_Name,iScript,iSubScript);
		}
//		Calling the below method to enter popup details in site info
		SiteDetailsEntry_AEnd(file_name,Sheet_Name,iScript,iSubScript);
	return result;
	
}
public String SiteOperation_BEnd(String file_name,String Sheet_Name,String iScript, String iSubScript) throws Exception 
{
	String result = null;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");

	SiteBServiceParty(file_name,Sheet_Name,iScript,iSubScript);
	PickServiceParty(file_name,Sheet_Name,iScript,iSubScript);
	SiteBSiteContact(file_name,Sheet_Name,iScript,iSubScript);
	PickSiteContactParty(file_name,Sheet_Name,iScript,iSubScript);
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	if (!Product_Name.equalsIgnoreCase("Wave")) {
		SiteInfo_BEnd(file_name,Sheet_Name,iScript,iSubScript);
		
	}
//	Calling the below method to enter popup details in site info
	SiteDetailsEntry_BEnd(file_name,Sheet_Name,iScript,iSubScript);

	return result;
	
}
public void SiteAServiceParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
{

	Reusable.waitForSiebelLoader();
	if(isElementPresent(By.xpath("//span[text()='Ok']")))
	{
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.AlertAccept,"Verify on Alert Accept");
		click(SiebelLiabrary_Obj.ServiceEntry.AlertAccept,"Click  on Alert Accept");
		Reusable.waitForSiebelLoader();
	}
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteADropdownClick.replace("Value","Service Party"));
	click(SiebelLiabrary_Obj.ServiceEntry.SiteADropdownClick.replace("Value","Service Party"),"Service Party");
	Reusable.waitForSiebelLoader();
}

public void PickServiceParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
{
	//		String NumberofIPv4Addresses = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_ of_IPv4_Addresses");
	String ServicePartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party");

	Reusable.waitForSiebelLoader();

	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickAccountDropdown,"Verify on Pick Account Dropdown");
	click(SiebelLiabrary_Obj.ServiceEntry.PickAccountDropdown,"Click  on Pick Account Dropdown");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.AList.replace("Value","Party Name"));
	click(SiebelLiabrary_Obj.ServiceEntry.AList.replace("Value","Party Name"));
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickAccountValue,"Verify on Pick Account Dropdown Value");
	click(SiebelLiabrary_Obj.ServiceEntry.PickAccountValue,"Click  on Pick Account Dropdown Value");
	Reusable.waitForSiebelLoader();
	sendKeys(SiebelLiabrary_Obj.ServiceEntry.PickAccountValue,ServicePartyName,"Account Dropdown Value");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickAccountGo,"Verify Go");
	click(SiebelLiabrary_Obj.ServiceEntry.PickAccountGo,"Click on Go");
	Reusable.waitForSiebelLoader();
	Reusable.waitForAjax();

	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickAccountOk,"Verify Ok");
	click(SiebelLiabrary_Obj.ServiceEntry.PickAccountOk,"Click on Ok");
	Reusable.waitForSiebelLoader();
}

public void SiteBServiceParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
{
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteBDropdownClick.replace("Value","Service Party"));
	click(SiebelLiabrary_Obj.ServiceEntry.SiteBDropdownClick.replace("Value","Service Party"));
	Reusable.waitForSiebelLoader();

}

public void SiteASiteContact(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
{
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteADropdownClick.replace("Value","Site Contact"),"Site Contact");
	click(SiebelLiabrary_Obj.ServiceEntry.SiteADropdownClick.replace("Value","Site Contact"),"Site Contact");
	Reusable.waitForSiebelLoader();
}

/*  for R4 Products */
public void SiteBSiteContact(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
{
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteBDropdownClick.replace("Value","Site Contact"));
	click(SiebelLiabrary_Obj.ServiceEntry.SiteBDropdownClick.replace("Value","Site Contact"));
	Reusable.waitForSiebelLoader();
}

public void PickSiteContactParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException 
{
	String IP_GuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
	String Site_Contact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Contact");

	Reusable.waitForSiebelLoader();
	Reusable.waitForAjax();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickAccountDropdown,"Verify on Pick Account Dropdown");
	click(SiebelLiabrary_Obj.ServiceEntry.PickAccountDropdown,"Click  on Pick Account Dropdown");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.AList.replace("Value","Last Name"));
	click(SiebelLiabrary_Obj.ServiceEntry.AList.replace("Value","Last Name"));
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickAccountValue,"Verify on Pick Account Dropdown Value");
	click(SiebelLiabrary_Obj.ServiceEntry.PickAccountValue,"Click  on Pick Account Dropdown Value");
	sendKeys(SiebelLiabrary_Obj.ServiceEntry.PickAccountValue,Site_Contact,"Account Dropdown Value");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickContactGo,"Verify Go");
	click(SiebelLiabrary_Obj.ServiceEntry.PickContactGo,"Click on Go");
	Reusable.waitForSiebelLoader();
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelLiabrary_Obj.ServiceEntry.PickContactOk,"Verify Ok");
	click(SiebelLiabrary_Obj.ServiceEntry.PickContactOk,"Click on Ok");
	Reusable.waitForSiebelLoader();


}
public String SiteInfo_AEnd(String file_name,String Sheet_Name,String iScript, String iSubScript) throws Exception 
{
	String Delivery_Country = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_Country");
	String Delivery_City = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_City");
	String result = null;
if(isVisible(SiebelLiabrary_Obj.ServiceEntry.showFullInfoAendLnk))
{
	click(SiebelLiabrary_Obj.ServiceEntry.showFullInfoAendLnk, "Click on ShowfullInfo link at A end site");
	Reusable.waitForSiebelLoader();
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.addressContactLookup))
	{
		if(!isEnable(SiebelLiabrary_Obj.ServiceEntry.deliveryCountryTxb))
	    {	
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCountryTxb,Delivery_Country);
		Reusable.SendkeaboardKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCountryTxb,Keys.ENTER);
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCityTxb,Delivery_City);
		Reusable.SendkeaboardKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCityTxb,Keys.ENTER);
		}
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.closePopUpBtn,"Verify Close Button");
		click(SiebelLiabrary_Obj.ServiceEntry.closePopUpBtn,"Click on close button");
		Reusable.waitForSiebelLoader();
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
		System.out.println("showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
		return "False";
	}
}
else {
	ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
	System.out.println("showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
	return "False";
}
Reusable.savePage();
Reusable.waitForSiebelLoader();

return result;
}
public String SiteInfo_BEnd(String file_name,String Sheet_Name,String iScript, String iSubScript) throws Exception 
{
	String Delivery_Country = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_Country");
	String Delivery_City = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_City");
	String result = null;
if(isVisible(SiebelLiabrary_Obj.ServiceEntry.showFullInfoBendLnk))
{
	click(SiebelLiabrary_Obj.ServiceEntry.showFullInfoBendLnk, "Click on ShowfullInfo link at B end site");
	Reusable.waitForSiebelLoader();
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.addressContactLookup))
	{
		if(!isEnable(SiebelLiabrary_Obj.ServiceEntry.deliveryCountryTxb))
	    {
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCountryTxb,Delivery_Country);
		Reusable.SendkeaboardKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCountryTxb,Keys.ENTER);
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCityTxb,Delivery_City);
		Reusable.SendkeaboardKeys(SiebelLiabrary_Obj.ServiceEntry.deliveryCityTxb,Keys.ENTER);
	    }
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.closePopUpBtn,"Click Service Order Go");
		click(SiebelLiabrary_Obj.ServiceEntry.closePopUpBtn,"Click Service Order Go");
		
		Reusable.waitForSiebelLoader();
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
		System.out.println("showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
		return "False";
	}
}
else {
	ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
	System.out.println("showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
	return "False";
}
Reusable.savePage();
Reusable.waitForSiebelLoader();

return result;
}
public String SiteDetailsEntry_AEnd(String file_name,String Sheet_Name,String iScript, String iSubScript) throws Exception 
{
	String result = null;
//	Initialize the Variable
	String ThirdParty_Provider = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"ThirdParty_Provider");
	String Shelf_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Shelf_ID");
	String PhysicalPort_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PhysicalPort_ID");
	String Slot_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Slot_ID");
	String VLAN_TagID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"VLAN_TagID");
	String Installaton_Time = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Installaton_Time");
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Flow_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
	String Fibre_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Fibre_Type");
	String sResult, sXpath;
	
	Reusable.waitForSiebelLoader();
	if(Flow_Type.equalsIgnoreCase("Override")&&isVisible(SiebelLiabrary_Obj.ServiceEntry.thirdPartyAccessProviderALst))
	{
		ScrollIntoViewByString(SiebelLiabrary_Obj.ServiceEntry.thirdPartyAccessProviderALst);
		click(SiebelLiabrary_Obj.ServiceEntry.thirdPartyAccessProviderALst);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", ThirdParty_Provider),"ThirdParty_Provider");
		click(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", ThirdParty_Provider),"ThirdParty_Provider");
	
	}
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.shelfIDATxb))
	{
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.shelfIDATxb, Shelf_ID);
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.slotIDATxb, Slot_ID);
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.physicalportIDATxb, PhysicalPort_ID);
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			ScrollIntoViewByString(SiebelLiabrary_Obj.ServiceEntry.fibreTypeLst);
			click(SiebelLiabrary_Obj.ServiceEntry.fibreTypeLst);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", Fibre_Type),"fibreTypeLst");
			click(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", Fibre_Type),"fibreTypeLst");
			Reusable.waitForSiebelLoader();
		}
		//Installation Time
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteADropdownClick.replace("Value","Install Time"),"SiteADropdownClick");
		click(SiebelLiabrary_Obj.ServiceEntry.SiteADropdownClick.replace("Value","Install Time"));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteABSelection.replace("Value",Installaton_Time),"SiteABSelection");
		click(SiebelLiabrary_Obj.ServiceEntry.SiteABSelection.replace("Value",Installaton_Time));
		Reusable.waitForSiebelLoader();
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "shelfIDATxb in Service Order Page is not visible, Please Verify");
		System.out.println("shelfIDATxb in Service Order Page is not visible, Please Verify");
		return "False";
	}
	return result;

}
public String SiteDetailsEntry_BEnd(String file_name,String Sheet_Name,String iScript, String iSubScript) throws Exception 
{
	String result = null;
//	Initialize the Variable
	String ThirdParty_Provider = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_ThirdParty_Provider");
	String Shelf_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_Shelf_ID");
	String PhysicalPort_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_PhysicalPort_ID");
	String Slot_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_Slot_ID");
	String VLAN_TagID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_VLAN_TagID");
	String BEnd_Installaton_Time = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_Installaton_Time");
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Flow_Type = DataMiner.fngetcolvalue(file_name,Sheet_Name, iScript, iSubScript,"Flow_Type");
	String Fibre_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"B_End_Fibre_Type");
	String sResult, sXpath;
	
	Reusable.waitForSiebelLoader();
	if(Flow_Type.equalsIgnoreCase("Override")&& isVisible(SiebelLiabrary_Obj.ServiceEntry.thirdPartyAccessProviderALst))
	{
		ScrollIntoViewByString(SiebelLiabrary_Obj.ServiceEntry.thirdPartyAccessProviderALst);
		click(SiebelLiabrary_Obj.ServiceEntry.thirdPartyAccessProviderALst);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", ThirdParty_Provider),"ThirdParty_Provider");
		click(SiebelLiabrary_Obj.ServiceEntry.MiddleLi.replace("Value", ThirdParty_Provider),"ThirdParty_Provider");
	
	}
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.shelfIDBTxb))
	{
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.shelfIDBTxb, Shelf_ID);
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.slotIDBTxb, Slot_ID);
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.physicalportIDBTxb, PhysicalPort_ID);
		if (Product_Name.equalsIgnoreCase("EthernetSpoke")) {
			sendKeys(SiebelLiabrary_Obj.ServiceEntry.vlanTagIDBTxb,VLAN_TagID);
			Reusable.waitForSiebelLoader();
		}
		//Installation Time
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteBDropdownClick.replace("Value","Install Time"),"SiteADropdownClick");
		click(SiebelLiabrary_Obj.ServiceEntry.SiteBDropdownClick.replace("Value","Install Time"));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.SiteABSelection.replace("Value",BEnd_Installaton_Time),"SiteABSelection");
		click(SiebelLiabrary_Obj.ServiceEntry.SiteABSelection.replace("Value",BEnd_Installaton_Time));
		Reusable.waitForSiebelLoader();
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "shelfIDATxb in Service Order Page is not visible, Please Verify");
		System.out.println("shelfIDATxb in Service Order Page is not visible, Please Verify");
		return "False";
	}
	
	return result;

}
public void EnterDateInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
{

	//	waitToPageLoad();
	Reusable.waitForSiebelLoader();
	alertPopUp();
	mouseMoveOn(SiebelLiabrary_Obj.EnterDateInFooter.OrderDates);

	verifyExists(SiebelLiabrary_Obj.EnterDateInFooter.OrderDates,"Order Dates Tab");
	click(SiebelLiabrary_Obj.EnterDateInFooter.OrderDates," Click on Order Dates");
	//	waitToPageLoad();
	Reusable.waitForSiebelLoader();
	//	alertPopUp();
	Reusable.savePage();
/*	verifyExists(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date Icon");
	//	sendKeysByJS(SiebelModeObj.EnterDateInFooter.CustomerRequestedDate, Reusable.CurrentDate());
	Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date");
*/
	verifyExists(SiebelLiabrary_Obj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date Icon");
	Reusable.SelectTodaysDate(SiebelLiabrary_Obj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date");

	//		verifyExists(SiebelModeObj.EnterDateInFooter.ColtActualDateIcon,"Colt Actual Date Icon");
	//		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtActualDateIcon,"Colt Actual Date");

	verifyExists(SiebelLiabrary_Obj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date Icon");
	Reusable.SelectTodaysDate(SiebelLiabrary_Obj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date");

/*	verifyExists(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day Icon");
	Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day");
*/
	Reusable.savePage();	
	Reusable.waitForSiebelLoader();
}
public void EnterBillingDateInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
{
	String ContractRenewalFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ContractRenewalFlag");
	String ContractTerm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ContractTerm");
	String Bill_Cust_Ref = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bill_Cust_Ref");
	
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.ServiceEntry.Billing,"Billing Tab");
	click(SiebelLiabrary_Obj.ServiceEntry.Billing,"Click on Billing Tab");
	Reusable.waitForSiebelLoader();

	//	ClickHereSave();
	/*		if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) {
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges,"Save Order Changes");	
		Reusable.waitForSiebelLoader();
		}
	 */	
	if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) {
		Reusable.waitForElementToAppear(SiebelLiabrary_Obj.ServiceEntry.SaveOrderChanges, 10);
		click(SiebelLiabrary_Obj.ServiceEntry.SaveOrderChanges,"Save Order Changes");	
		Reusable.waitForSiebelLoader();
	}
	if(isVisible(SiebelLiabrary_Obj.ServiceEntry.refreshBtn))
	{
		
		click(SiebelLiabrary_Obj.ServiceEntry.billCustRefOrderTxb,"Save Order Changes");	
		sendKeys(SiebelLiabrary_Obj.ServiceEntry.billCustRefOrderTxb, Bill_Cust_Ref);
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelLiabrary_Obj.ServiceEntry.BillingStartDateAccessIcon,"Billing Start Date Access Icon");
		Reusable.SelectTodaysDate(SiebelLiabrary_Obj.ServiceEntry.BillingStartDateAccessIcon,"Billing Start Date Access");

	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "refreshBtn in Billing Page is not visible, Please Verify");
		System.out.println("refreshBtn in Billing Page is not visible, Please Verify");
		
	}	
	
	Reusable.savePage();
	Reusable.waitForSiebelLoader();	
	Reusable.waitForAjax();
}
public void alertPopUp() throws InterruptedException, IOException
{
	if (isVisible(SiebelLiabrary_Obj.AlertPopUp.AlertAccept)) {
		click(SiebelLiabrary_Obj.AlertPopUp.AlertAccept,"Click on Alerts Accept");
	}
}
}
