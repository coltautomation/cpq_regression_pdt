package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSSplitFreeNumberPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumer = new NMTSAddNumberPage();
	
	String Successmsg;
	private static HashMap<String, String> Data= new HashMap<String, String>();

	
	public void SplitNumbermain(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String SpitNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_for_Split_and_Filter_By");
		String BlockSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_Size");
		String Quantity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity");
		String Blocksize10 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_size10");
		String Quantity2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity_2");
		String Blocksize100 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_size100"); 
		String Quantity3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity_3");  
		String Blocksize1000 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_Size1000"); 
		String Quantity4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity_4"); 
		
		AddNumer.UserGroupforSuperUser(testDataFile, sheetName, scriptNo, dataSetNo);
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMenu");
		
		
		
		waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitNumberButton,15);
		verifyExists(NMTSSplitNumberObj.SplitNumber.SplitNumberButton,"Split Number button");
		javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitNumberButton,"Split Number button");
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMain");
		Reusable.waitForAjax();

		verifyExists(NMTSSplitNumberObj.SplitNumber.SplitNumber,"Split Number");
		sendKeys(NMTSSplitNumberObj.SplitNumber.SplitNumber, SpitNum, "Split Number");
		waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitSearchButton,15);
		verifyExists(NMTSSplitNumberObj.SplitNumber.SplitSearchButton,"Split Search button");
		javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitSearchButton,"Split Search button");
		Reusable.waitForAjax();

		
		if(isElementPresent(By.xpath("//span[contains(text(),'No Record Found.')]")))
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Record found please enter the valid Number");	
			
		} else {
			int rangefrom = Integer.parseInt(getTextFrom(NMTSSplitNumberObj.SplitNumber.Rangefromspit,"Range from spit"));
			int rangeto = Integer.parseInt(getTextFrom(NMTSSplitNumberObj.SplitNumber.Rangetospit,"Range to spit"));
		
			
			int Actual=rangeto-rangefrom;
			int ActualRange = Actual+1;
		//	System.out.println(ActualRange);
			// calculating the data from the Excel
			int firstsize = Integer.parseInt((BlockSize.toString()));
			
			int firstquantity= 0;
			if(Quantity.toString()=="") {
				firstquantity=0;
			} else {
				 firstquantity = Integer.parseInt(Quantity.toString());
			}
			
			int firstAll=firstsize*firstquantity;
		//	System.out.println(firstAll);

			int secondsize = Integer.parseInt((Blocksize10.toString()));
			int secondquantity= 0;
			if(Quantity2.toString()=="") {
				secondquantity=0;
			} else {
				secondquantity = Integer.parseInt((Quantity2.toString()));
			}
			int secondAll=secondsize*secondquantity;
		//	System.out.println(secondAll);
			
			int thirdsize = Integer.parseInt((Blocksize100.toString()));
			int thirdquantity= 0;
			if(Quantity3.toString()=="") {
				thirdquantity=0;
			} else {
				thirdquantity = Integer.parseInt((Quantity3.toString()));
			}
			int thirdAll=thirdsize*thirdquantity;
		//	System.out.println(thirdAll);
			
			int fourthsize = Integer.parseInt((Blocksize1000.toString()));
			int fourthquantity= 0;
			if(Quantity4.toString()=="") {
				fourthquantity=0;
			} else {
				fourthquantity = Integer.parseInt((Quantity4.toString()));
			}
			int fourthAll=fourthsize*fourthquantity;
		//	System.out.println(fourthAll);
			
			if(firstAll+secondAll+thirdAll+fourthAll == ActualRange) {
				//we need to calculate the null value as well here
				if(firstquantity == 0) {
					System.out.println("Since you hav't filled any data Quantity to create for block size 1 so we are checking with another quantity");
				} else  {
					waitForAjax();
					verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
					sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)), BlockSize, "Split block Size");
					waitForAjax();
					
					click(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size quantity");
					//verifyExists(NMTSSplitNumberObj.SplitNumber.IESplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Quantity");
					sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)), Quantity, "Split block Quantity");
					// may be need to mention index number in locator of IESplitQuantitysizesplit
				    waitForAjax();
					
					waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
					verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
					javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
					
				}
				if(secondquantity == 0 ) {
					//we need to calculate the null value as well here
					System.out.println("Since you hav't filled any data Quantity to create for block size 10 so we are checking with another quantity");
				} else  {
					if(firstquantity==0 && secondquantity>0) {
						Reusable.waitForAjax();
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)), Blocksize10, "Split block Size");
						Reusable.waitForAjax();
						
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),Quantity2, "Split block Size quantity");
						Reusable.waitForAjax();
						
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),Quantity2, "Split block Size quantity");
						Reusable.waitForAjax();
						
						waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						
					}
					else if(firstquantity>0 && secondquantity>0) {
						waitForAjax();

						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(2)), Blocksize10, "Split block Size");
						waitForAjax();

						click(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),"Split block Size quantity");
					//	verifyExists(NMTSSplitNumberObj.SplitNumber.IESplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)), Quantity2, "Split block Size quantity");
						
						waitForAjax();

						waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						
					}
				}
					if(thirdquantity == 0 ) {
						//we need to calculate the null value as well here
						
					} else  {
						if(firstquantity==0 && secondquantity==0 && thirdquantity>0) {
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)), Blocksize100, "Split block Size");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),Quantity3, "Split block Size quantity");
							Reusable.waitForAjax();

							
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),Quantity3, "Split block Size quantity");
							Reusable.waitForAjax();

							waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							
							
							
					} else if((firstquantity>0 && secondquantity==0 && thirdquantity>0) || (firstquantity==0 && secondquantity>0 && thirdquantity>0)) {
						Reusable.waitForAjax();

						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(2)), Blocksize100, "Split block Size");
						Reusable.waitForAjax();

						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),Quantity3, "Split block Size quantity");
						Reusable.waitForAjax();

						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),Quantity3, "Split block Size quantity");
						
						Reusable.waitForAjax();

						waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						
						
					} else if(firstquantity>0 && secondquantity>0 && thirdquantity>0) {
					    waitForAjax();
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(3)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(3)), Blocksize100, "Split block Size");
						waitForAjax();
						//verifyExists(NMTSSplitNumberObj.SplitNumber.IESplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),"Split block Size");
						click(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),"Split block Size quantity");
					//	verifyExists(NMTSSplitNumberObj.SplitNumber.IESplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),"Split block Size");
						sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),Quantity3, "Split block Size quantity");
						waitForAjax();
						
						waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
						verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						//javaScriptclick(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						waitForAjax();
						
					}
				}
					
					
					if(fourthquantity == 0 ) {
						//we need to calculate the null value as well here
						
					} else {
						if(firstquantity==0 && secondquantity==0 && thirdquantity==0 && fourthquantity>0) {
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(1)), Blocksize1000, "Split block Size");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(1)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();

							waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							
						} else if((firstquantity==0 && secondquantity==0 && thirdquantity>0 && fourthquantity>0)|| (firstquantity==0 && secondquantity>0 && thirdquantity==0 && fourthquantity>0)||(firstquantity>0 && secondquantity==0 && thirdquantity==0 && fourthquantity>0)) {
							Reusable.waitForAjax();
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(2)), Blocksize1000, "Split block Size");
							Reusable.waitForAjax();
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(2)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();
							waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							
						} else if((firstquantity==0 && secondquantity>0 && thirdquantity>0 && fourthquantity>0)|| (firstquantity>0 && secondquantity>0 && thirdquantity==0 && fourthquantity>0)||(firstquantity>0 && secondquantity==0 && thirdquantity>0 && fourthquantity>0)) {
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(3)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(3)), Blocksize1000, "Split block Size");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(3)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();

							waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							
						} else if(firstquantity>0 && secondquantity>0 && thirdquantity>0 && fourthquantity>0) {
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(4)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitblockSizesplit.replace("indexrow",String.valueOf(4)), Blocksize1000, "Split block Size");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(4)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(4)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();

							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(4)),"Split block Size");
							sendKeys(NMTSSplitNumberObj.SplitNumber.SplitQuantitysizesplit.replace("indexrow",String.valueOf(4)),Quantity4, "Split block Size quantity");
							Reusable.waitForAjax();

							waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.SplitAddButn,15);
							verifyExists(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
							//click(NMTSSplitNumberObj.SplitNumber.SplitAddButn,"Split Add button");
						}
					}
				waitForAjax();

					waitForElementToBeVisible(NMTSSplitNumberObj.SplitNumber.splitFreenumbtn,15);
					verifyExists(NMTSSplitNumberObj.SplitNumber.splitFreenumbtn,"Save Free Number Button");
					javaScriptclick(NMTSSplitNumberObj.SplitNumber.splitFreenumbtn,"Save Free Number Button");
					
					waitForAjax();
					Successmsg = getTextFrom(NMTSSplitNumberObj.SplitNumber.successmsg,"Split Message");
					if (Successmsg.contains("Number Split Successful"))
					{
						ExtentTestManager.getTest().log(LogStatus.PASS, "Number Split Successful");	
						
					}else{
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Number not Split Successful");	
						
					}
					
					} 
				else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "it seems like you have'nt calculated the correct Block size and Quantity in your Excel sheet :( please check and correct it) ");	
				} 
			}
		}
	
	
			public void setvalue(String key, String value) {
				Data.put(key, value);
			}

			public String getvalue(String key) {
				return Data.get(key);       
			}
	}

	
	
	
	
	
	


