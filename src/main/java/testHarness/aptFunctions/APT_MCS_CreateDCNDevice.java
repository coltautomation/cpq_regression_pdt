package testHarness.aptFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;
import testHarness.commonFunctions.ReusableFunctions;

public class APT_MCS_CreateDCNDevice extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	SoftAssert sa = new SoftAssert();

	public void webelementpresencelogger(WebElement ele, String fieldname) {

		try {
			boolean flag = ele.isDisplayed();
			// Log.info("element presence state : " + flag);
			Report.LogInfo("INFO", "element presence state : " + flag, "NA");

			if (flag) {

				Report.LogInfo("INFO", "Step: expected field is displayed " + fieldname, "PASS");
			} else {

				Report.LogInfo("INFO", "Step: expected field is not displayed " + fieldname, "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();

			Report.LogInfo("INFO", "Step: expected field is not displayed " + fieldname, "FAIL");
		}

	}

	public void navigatetomanagecoltnetwork() throws InterruptedException, IOException {
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink, "Manage Colt Network Link");
		mouseMoveOn(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink);
		waitForAjax();
	}

	public void navigatetocreateaccesscoredevicepage() throws InterruptedException, IOException {
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink,
				"Create Access Core Device Link");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink,
				"Create Access Core Device Link");
		waitForAjax();
	}

	public void verifyisEmpty(String xpath, String fieldname) throws InterruptedException {

		// String ele = getwebelement(xpath).getAttribute("value");
		String ele = getAttributeFrom(xpath, "value");
		if (ele.isEmpty()) {

			Report.LogInfo("INFO", "selected field is empty " + fieldname, "PASS");

		} else {

			sa.fail();
			Report.LogInfo("INFO", "selected field is not empty " + fieldname, "FAIL");
		}

		sa.assertAll();
	}

	public void verifyDefaultSelection_connectivityprotocol_telnet() throws InterruptedException, IOException {
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
		isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
	}

	public void verifyDefaultSelection_connectivityprotocol_ssh() throws InterruptedException, IOException {
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "ssh Radio Button");
		isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "ssh Radio Button");

	}

	public void fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {

		scrollUp();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");

		waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,
				"Fetch Device Interfaces");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,
				"Fetch Device Interfaces");

		waitForAjax();

		String expectedValue = "Fetch interfaces started successfully. Please check the sync status of this device";
		boolean successMessage = false;

		successMessage = isVisible(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage);
		// successMessage=getwebelement(xml.getlocator("//locators/" +
		// application + "/successMessage_Alert")).isDisplayed();

		String actualMessage = getTextFrom(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert,"success Message_Alert");
		// String actualMessage=getwebelement(xml.getlocator("//locators/" +
		// application + "/successMessage_Alert")).getText();
		if (successMessage) {

			if (actualMessage.contains(expectedValue)) {

				// click on the 'click here' link
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,
						"Interfaces click here Link");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,
						"Interfaces click here Link");
				waitForAjax();

			} else if (actualMessage.equalsIgnoreCase(expectedValue)) {
				// click on the 'click here' link
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,
						"Interfaces click here Link");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,
						"Interfaces click here Link");
				waitForAjax();
			} else {
				// Action Button Click
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
				waitForAjax();
				// click on 'Manage' link
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,
						"manage Link_view Devicepage");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,
						"manage Link view Devicepage");

				waitForAjax();

			}
		}
	}

	public String compareText_InViewPage_ForNonEditedFields(String application, String labelname)
			throws IOException, InterruptedException {

		String emptyele = null;
		// WebElement element = null;

		try {
			waitForAjax();
			// element = getwebelement("//div[div[label[contains(text(),'"+
			// labelname + "')]]]/div[2]");
			// emptyele = element.getText().toString();
			emptyele = getTextFrom(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.compareText_InViewPage_ForNonEditedFields1
							+ labelname
							+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.compareText_InViewPage_ForNonEditedFields2,"Compare text in view page");

			Report.LogInfo("INFO", labelname + " field is not edited. It is displaying as " + emptyele, "PASS");

		} catch (Exception e) {
			e.printStackTrace();

			Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");
		}
		return emptyele;

	}

	public void routerPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String Command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv4");
		String Command_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv6");
		String vrf_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");
		String vrf_Ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv6");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown,
				"Command IPV4_dropdown");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown, "Command IPV4_dropdown");
		ClickonElementByString("//div[text()='" + Command_ipv4 + "']", 30);

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField,
				"commandIPv4 Vrf Textfield");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField, vrf_Ipv4,
				"commandIPv4 Vrf Textfield");

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,
		// "commandIPv4 Hostname Textfield");
		// sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,newPremiseCode,"commandIPv4
		// Hostname Textfield");

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4,
		// "Execute Button_Ipv4");
		// click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4,"Execute
		// Button_Ipv4");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown,
				"Command IPV6_dropdown");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown, "Command IPV6_dropdown");
		ClickonElementByString("//div[text()='" + Command_ipv6 + "']", 30);

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
				"commandIPv6 Vrf Textfield");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField, vrf_Ipv6,
				"commandIPv6 Vrf Textfield");

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,
		// "commandIPv6 Hostname Textfield");
		// sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,newPremiseCode,"commandIPv6
		// Hostname Textfield");

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6,
		// "Execute Button_Ipv6");
		// click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6,"Execute
		// Button_Ipv6");

	}

	public void executeCommandAndFetchTheValue() throws InterruptedException, IOException {

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6,
				"Execute Button_Ipv6");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6, "Execute Button_Ipv6");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
		getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4,
				"Execute Button_Ipv4");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4, "Execute Button_Ipv4");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
		getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
	}

	public void managelink() throws InterruptedException, IOException {
		scrollUp();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,
				"Manage Link View Device page");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,
				"Manage Link View Device page");

		waitForAjax();
	}

	public void verifyDeviceCreationMessage() throws InterruptedException, IOException {
		Reusable.waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg,
				"Device created successfully");

		String deviceCreation_Msg = getTextFrom(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg,"device creation success msg");
		String expected_deviceUpdation = "Device created successfully";
		if (deviceCreation_Msg.equals(expected_deviceUpdation)) {
			Report.LogInfo("INFO", expected_deviceUpdation, "PASS");
		} else {
			Report.LogInfo("INFO", "Device not updated: " + deviceCreation_Msg, "FAIL");
		}

		Reusable.WaitforCPQloader();

	}

	public void verifyDeviceUpdationSuccessMessage() throws InterruptedException, IOException {
		Reusable.waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation,
				"Device updated successfully");
		String deviceUpdation_Msg = getTextFrom(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation,"success Message_device Updation");
		String expected_deviceUpdation = "Device updated successfully";
		if (deviceUpdation_Msg.equals(expected_deviceUpdation)) {
			Report.LogInfo("INFO", expected_deviceUpdation, "PASS");
		} else {
			Report.LogInfo("INFO", "Device not updated: " + deviceUpdation_Msg, "FAIL");
		}

	}

	public void verifydeviceDelete_AccessRouter() throws IOException, InterruptedException {
		scrollUp();
		refreshPage();
		waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");

		waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete, "Delete Device");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete, "Delete Device");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.delete_alertpopup, "Delete Alert Popup");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton, "Delete Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton, "Delete Button");

		waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg,
				"Device created successfully");

		String deviceCreation_Msg = getTextFrom(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg,"delete Device ConfMsg");
		String expected_deviceUpdation = "Device successfully deleted.";
		if (deviceCreation_Msg.equals(expected_deviceUpdation)) {
			Report.LogInfo("INFO", expected_deviceUpdation, "PASS");
		} else {
			Report.LogInfo("INFO", "Device not deleted: " + deviceCreation_Msg, "FAIL");
		}

	}

	public void verifydevicecreation_AccessRouter(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		try {

			scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);

			// Verify Mandatory fields
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");

			waitForAjax();

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.WarningMesassage_site,
					"Warning Mesassage Site");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_city,
					"Warning Mesassage City");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_Country,
					"Warning Mesassage Country");

			scrollUp();

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_name,
					"Warning Mesassage Name");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_deviceType,
					"Warning Mesassage Device Type");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_vendor,
					"Warning Mesassage vendor");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_routerId,
					"Warning Mesassage Router Id");

			String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
			String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceType");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
			String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterID");
			String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IOSXR");
			String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Full_IQNET");
			String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modular_MSP");
			String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Telnet");
			String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SSH");
			String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmp3");
			String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmp2C");
			String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
			String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmprwNewValue");
			String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Snmpv3UsernameNewValue");
			String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Snmpv3AuthpasswordNewValue");
			String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Snmpv3PrivpasswordNewValue");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
			String Management_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Management_Address");
			String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
			String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"ExistingCityValue");
			String ExSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
			String ExSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"ExistingSiteValue");
			String ExPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingPremise");
			String ExPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"ExistingPremiseValue");
			String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
			String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
			String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityCode");

			String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
			String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
			String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");

			String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");
			String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"NewPremiseName");
			String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"NewPremiseCode");

			// name
			// addtextFields_commonMethod(application, "Name", "nametextfield",
			// name, xml);
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Name Text Field");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, Name, "Name Text Field");

			// devicetype
			// selectValueInsideDropdown(application, "devicetypeinput", "Device
			// Type", devicetype, xml);
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, deviceType,
					"Device Type Input");

			// vendormodel
			// addDropdownValues_commonMethod(application, "Vendor/Model",
			// "vendormodelinput", vendormodel, xml);
			scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput);
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput,
					"Vendor Model Input");
			addDropdownValues_commonMethod("Vendor/Model",
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, VendorModel);

			waitForAjax();

			// modular msp selection
			if (ModularMSP.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox,
						"Modular MSP Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox,
						"Modular MSP Checkbox");
			}
			// full iqnet selection
			if (Full_IQNET.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox,
						"Full IQNET Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox,
						"Full IQNET Checkbox");

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader,
						"Full IQNET Alert Header");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
			}

			// Connectivity Protocol
			if (telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton,
						"Telnet Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton,
						"Telnet Radio Button");
			}

			else if (telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton,
						"SSH Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			}

			// SNMP version
			if (Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,
						"snmpro Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, Snmpro);

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,
						"snmprw Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, Snmprw);

			}

			else if (Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, SnmpUser);

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,
						"snmp AuthPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, SnmpAuthPasswd);

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,
						"snmp PrivPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, SnmpPrivPasswd);

			}

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
			// click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput,"Country
			// Input");
			// click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput1
			// +country+
			// APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput2,"Country
			// Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, country,
					"Country Input");

			// if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox)==true)
			// {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,
					"Management Address Textbox");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,
					Management_Address);
			// }

			// Router id
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield,
					"Router Id Textfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, RouterId);

			// Management address
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,
					"Management Address Textbox");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,
					Management_Address);

			// select country
			scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput);
			waitForAjax();

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, country,
					"Country Input");

			if (ExCity.equalsIgnoreCase("Yes") && newCity.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,
						"City dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,
						ExCityValue, "City dropdown Input");

			} else if (ExCity.equalsIgnoreCase("No") && newCity.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,
						"City Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, newCityName);

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,
						"City Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, newCityCode);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						"Site Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						newSiteName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						"Site Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						newSiteCode);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						"Premise Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						newPremiseName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						"Premise Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						newPremiseCode);

			}
			if (ExSite.equalsIgnoreCase("Yes") && newSite.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,
						"Site Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,
						ExSiteValue, "Site dropdown Input");

			} else if (ExSite.equalsIgnoreCase("No") && newSite.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,
						"Site Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,
						newSiteName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,
						"Site Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,
						newSiteCode);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,
						"Premise Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,
						newPremiseName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,
						"Premise Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,
						newPremiseCode);
			}
			if (ExPremise.equalsIgnoreCase("Yes") && newPremise.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,
						"Premise Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,
						ExPremiseValue, "Premise Dropdown Input");

			} else if (ExPremise.equalsIgnoreCase("No") && newPremise.equalsIgnoreCase("yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch,
						"Add Premise Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
						"Premise Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
						newPremiseName, "Premise Name Inputfield");

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
						"Premise Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
						newPremiseCode, "Premise Code Inputfield");
			}
		} catch (StaleElementReferenceException e) {

			e.printStackTrace();
		}

		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		Reusable.WaitforCPQloader();
	}

	public void verifydeviceEdit_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
		// String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo,"DeviceType");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editVendorModel");
		String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editRouterID");
		String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editModularMSP");
		String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editFullIQNET");
		String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editIOSXR");
		String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editTelnet");
		String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSSH");
		String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSnmp2C");
		String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSnmp3");
		String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editSnmProNewValue");
		String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editSnmprwNewValue");
		String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editSnmpv3UsernameNewValue");
		String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editSnmpv3AuthpasswordNewValue");

		String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editSnmpv3PrivpasswordNewValue");

		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editManagementAddress");

		String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");

		String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCity");
		String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCityValue");
		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSite");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSiteValue");

		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremise");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremiseValue");
		String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewCity");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewSite");

		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewPremise");
		String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityName");
		String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityCode");

		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteName");
		String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteCode");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseName");
		String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseCode");
		scrollUp();
		waitForAjax();

		// Action Button Click
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		waitForAjax();

		// Edit Button Click
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
		// String EditButtonClick =
		// getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit);
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,
				"Edit Device Name TextField");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, editDevicename,
				"Edit Device Name TextField");

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,
		// "Device Type Input");
		// selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,deviceType,
		// "Device Type Input");

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
		// selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput,
		// editVendorModel, "Vendor Model Input");
		addDropdownValues_commonMethod("Vendor/Model",
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel);

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield,
				"Edit RouterID TextField");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, editRouterID,
				"Edit RouterID TextField");

		if (editFullIQNET.equalsIgnoreCase("Yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox,
					"Full IQNET Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			waitForAjax();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader,
					"Full IQNET Alert Header");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
		}

		if (editModularMSP.equalsIgnoreCase("Yes")) {
			waitForAjax();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox,
					"Modular MSP Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
			waitForAjax();
		}

		if (editIOSXR.equalsIgnoreCase("Yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
		}

		if (editTelnet.equalsIgnoreCase("Yes") && editSSH.equalsIgnoreCase("No")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton,
					"Telnet Radio Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
		}

		else if (editTelnet.equalsIgnoreCase("No") && editSSH.equalsIgnoreCase("Yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
		}

		if (editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, editSnmProNewValue);

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, editSnmprwNewValue);

		}

		else if (editSnmp2C.equalsIgnoreCase("No") && editSnmp3.equalsIgnoreCase("Yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,
					editSnmpv3UsernameNewValue);

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,
					"snmp AuthPasswd");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,
					editSnmpv3AuthpasswordNewValue);

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,
					"snmp PrivPasswd");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,
					editSnmpv3PrivpasswordNewValue);
			// }
			// }
		}

		if (!editCountry.equalsIgnoreCase("null")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, editCountry,
					"Edit Country Input");

			if (editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,
						"City dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,
						editExistingCityValue, "City dropdown Input");

			} else if (editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,
						"City Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, editNewCityName);

				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,
						"City Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, editNewCityCode);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						"Site Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						editNewSiteName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						"Site Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						editNewSiteCode);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						"Premise Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						editNewPremiseName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						"Premise Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						editNewPremiseCode);

			}

			if (editExistingSite.equalsIgnoreCase("Yes") && editNewSite.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,
						"Site Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,
						editExistingSiteValue, "Site dropdown Input");

			} else if (editExistingSite.equalsIgnoreCase("No") && editNewSite.equalsIgnoreCase("Yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						"Site Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						editNewSiteName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						"Site Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						editNewSiteCode);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						"Premise Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						editNewPremiseName);

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						"Premise Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						editNewPremiseCode);
			}

			if (editExistingPremise.equalsIgnoreCase("Yes") && editNewPremise.equalsIgnoreCase("No")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,
						"Premise Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,
						editExistingPremiseValue, "Premise Dropdown Input");
			} else if (editExistingPremise.equalsIgnoreCase("No") && editNewPremise.equalsIgnoreCase("yes")) {
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch,
						"Add Premise Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
						"Premise Name Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
						editNewPremiseName, "Premise Name Inputfield");

				verifyExists(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
						"Premise Code Inputfield");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
						editNewPremiseCode, "Premise Code Inputfield");
			}

		} else if (editCountry.equalsIgnoreCase("Null")) {
			Report.LogInfo("INFO", " No changes made for 'Country' dropdown", "PASS");
		}
		// Edit City
		if (editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,
					"City dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,
					editExistingCityValue, "City Dropdown Input");

		} else if (editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,
					"City Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, editNewCityName);

			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,
					"City Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, editNewCityCode);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
					"Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
					editNewSiteName);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
					"Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
					editNewSiteCode);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
					"Premise Name Inputfield");
			sendKeys(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
					editNewPremiseName);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
					"Premise Code Inputfield");
			sendKeys(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
					editNewPremiseCode);

		}
		// Edit Site
		if (editExistingSite.equalsIgnoreCase("Yes") && editNewSite.equalsIgnoreCase("No")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,
					"Site Dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,
					editExistingSiteValue, "Site dropdown Input");

		} else if (editExistingSite.equalsIgnoreCase("No") && editNewSite.equalsIgnoreCase("Yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
					"Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
					editNewSiteName);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
					"Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
					editNewSiteCode);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
					"Premise Name Inputfield");
			sendKeys(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
					editNewPremiseName);

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
					"Premise Code Inputfield");
			sendKeys(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
					editNewPremiseCode);
		}
		// Edit Premise
		if (editExistingPremise.equalsIgnoreCase("Yes") && editNewPremise.equalsIgnoreCase("No")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,
					"Premise Dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,
					editExistingPremiseValue, "Premise dropdown Input");

		} else if (editExistingPremise.equalsIgnoreCase("No") && editNewPremise.equalsIgnoreCase("yes")) {
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch,
					"Add Premise Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
					"Premise Name Inputfield");
			sendKeys(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
					editNewPremiseName, "Premise Name Inputfield");

			verifyExists(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
					"Premise Code Inputfield");
			sendKeys(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
					editNewPremiseCode, "Premise Code Inputfield");
		}

		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		waitForAjax();
		Reusable.WaitforCPQloader();

	}

	public void addDropdownValues_commonMethod(String labelname, String xpath, String expectedValueToAdd)
			throws InterruptedException {
		boolean availability = false;
		List<String> ls = new ArrayList<String>();

		try {

			// availability=getwebelementNoWait(xml.getlocator("//locators/" +
			// application + "/"+ xpath +"")).isDisplayed();
			if (isVisible(xpath)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				if (expectedValueToAdd.equalsIgnoreCase("null")) {
					Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {
					webDriver.findElement(By.xpath("//div[label[text()='" + labelname + "']]//div[text()='�']"))
							.click();
					// Clickon(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//div[text()='�']"));

					// verify list of values inside dropdown
					List<WebElement> listofvalues = webDriver
							.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

					for (WebElement valuetypes : listofvalues) {
						// Log.info("List of values : " + valuetypes.getText());
						ls.add(valuetypes.getText());
					}

					// ExtentTestManager.getTest().log(LogStatus.PASS, "list of
					// values inside "+labelname+" dropdown is: "+ls);
					// System.out.println("list of values inside "+labelname+"
					// dropdown is: "+ls);

					webDriver.findElement(By.xpath("//div[label[text()='" + labelname + "']]//input"))
							.sendKeys(expectedValueToAdd);
					// SendKeys(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//input"), expectedValueToAdd);

					webDriver.findElement(By.xpath("(//div[label[text()='" + labelname + "']]//div[contains(text(),'"
							+ expectedValueToAdd + "')])[1]")).click();
					// Clickon(getwebelementNoWait("(//div[label[text()='"+
					// labelname +"']]//div[contains(text(),'"+
					// expectedValueToAdd +"')])[1]"));
					// Thread.sleep(1000);

					/*
					 * String
					 * actualValue=getwebelementNoWait("//label[text()='"+
					 * labelname +"']/following-sibling::div//span").getText();
					 * ExtentTestManager.getTest().log(LogStatus.PASS, labelname
					 * + " dropdown value selected as: "+ actualValue );
					 * System.out.println( labelname +
					 * " dropdown value selected as: "+ actualValue);
					 */
				}
			} else {
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
				// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + "
				// is not displaying");
				// System.out.println(labelname + " is not displaying");
			}
		} catch (NoSuchElementException e) {

			// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is
			// not displaying");
			Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
			System.out.println(labelname + " is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			// ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to
			// perform selection under "+ labelname + " dropdown");
			Report.LogInfo("INFO", " NOt able to perform selection under " + labelname + " dropdown", "FAIL");
			System.out.println(" NO value selected under " + labelname + " dropdown");
		}
	}
}
