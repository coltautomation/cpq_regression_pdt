package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPAccessConfig;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_IPAccessResilientConfig;
import testHarness.aptFunctions.APT_IPAccess_VCPEConfig;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_32_APT_IPAccessConfigTest extends SeleniumUtils {
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	// APT_MCN_Create_Customer CreateCust = new APT_MCN_Create_Customer();
	// APT_MCS_CreateFirewallDevice CreateFirewall = new
	// APT_MCS_CreateFirewallDevice();
	APT_IPAccessNoCPEHelper noCPE = new APT_IPAccessNoCPEHelper();
	APT_IPTransit ipTransit = new APT_IPTransit();
	APT_IPAccessResilientConfig ResilentConfg = new APT_IPAccessResilientConfig();
	APT_IPAccessConfig IPConfige = new APT_IPAccessConfig();
	APT_IPAccess_VCPEConfig IPVCPE = new APT_IPAccess_VCPEConfig();

	APT_Login Login = new APT_Login();

	/*
	 * String dataFileName = "ModifyOrder.xlsx"; File path = new
	 * File("./src/test/resources/"+dataFileName); String testDataFile =
	 * path.toString(); String tsSheetName = "CarNor"; String scriptNo = "2";
	 * String dataSetNo = "12"; String Amount = "2";
	 */

	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test()
	public void APT_IPAccessResilientConfigTest(String scriptNo, String dataSetNo, String dataSheet,
			String ScenarioName) throws Exception {
		// String Name = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "Name");
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {
			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			// setup();
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			Report.LogInfo("INFO", "TC - 1 - =====CreateCustomer_Configue Test====", "PASS");
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
				noCPE.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
				Report.LogInfo("INFO", "=====selectExistingCustomer_Resilient====", "PASS");

				noCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			Report.LogInfo("INFO", "TC - 2 - =====verifyNewOrderCreationOrExistingOrderSelectionConfigueTest====",
					"PASS");
			ResilentConfg.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 3 - =====ServiceTypeAndNetworkConfigurationSelection_Configue Test====",
					"PASS");
			noCPE.verifyServicetypeAndNetworkConfigurationSelection(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 4 - =====verifyServiceCreation_Configue====", "PASS");
			IPConfige.verifyservicecreation(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 5 - =====verifyCustomerDetailsInformation_Configue====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 6 - =====VerifyEditOrderChangeOrderFunction_Resilient====", "PASS");
			noCPE.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 7 - =====VerifyCreatedServiceInformationInViewServicePage_Resilient====",
					"PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 9 - =====VerifyServiceActionsListFunction_ConfigueTest====", "PASS");
			IPVCPE.EditServiceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyManageSubnets();
			ResilentConfg.verifyDump();

			Report.LogInfo("INFO", "TC - 10 - =====VerifyAddDeleteExistingPEDevice_Configue====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.addExistingPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);			
			//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			//noCPE.deleteDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 11 - =====VerifyAddDeleteNewPEDevice_Configue====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.navigateToAddNewDevicepage_PE();
			noCPE.verifyadddevicefields_PE();
			noCPE.addNewPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyViewpage_Devicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyEditDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 12 - =====verifyRouterToolsServicenameTag====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// ===========================================================================
			Report.LogInfo("INFO", "TC - 13 - =====verifyInterfacePanel====", "PASS");
			//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);//need to remove
			String CPEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"CPEWANTechnology_DropdownValue");
			String Edit_CPEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_CPEWANTechnology_DropdownValue");
			String PEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"PEWANTechnology_DropdownValue");
			String Edit_PEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_PEWANTechnology_DropdownValue");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");

			if (CPEWANTechnology.equalsIgnoreCase("Ethernet") || Edit_CPEWANTechnology.equalsIgnoreCase("Ethernet")) {
				if (PEWANTechnology.equalsIgnoreCase("Ethernet-Standard")
						|| Edit_PEWANTechnology.equalsIgnoreCase("Ethernet-Standard")) {
					if (VendorModel.contains("Cisco") || VendorModel.contains("OneAccess")) {
						IPConfige.verify_Cisco_Ethernet_EthernetStandard_AddInterface(testDataFile, dataSheet, scriptNo,
								dataSetNo);
						IPConfige.verify_Cisco_Ethernet_EthernetStandard_EditInterface(testDataFile, dataSheet,
								scriptNo, dataSetNo);
					} else if (VendorModel.contains("Juniper")) {
						IPConfige.verify_Juniper_Ethernet_EthernetStandard_AddInterface(testDataFile, dataSheet,
								scriptNo, dataSetNo);
						IPConfige.verify_Juniper_Ethernet_EthernetStandard_EditInterface(testDataFile, dataSheet,
								scriptNo, dataSetNo);

					}
				} else if (PEWANTechnology.equalsIgnoreCase("ATM") || Edit_PEWANTechnology.equalsIgnoreCase("ATM")) {
					IPConfige.verify_Ethernet_ATM_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_Ethernet_ATM_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
				} else if (PEWANTechnology.equalsIgnoreCase("Ethernet-L2TP")
						|| Edit_PEWANTechnology.equalsIgnoreCase("Ethernet-L2TP")) {
					IPConfige.verify_Ethernet_EthernetL2TP_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_Ethernet_EthernetL2TP_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
			} else if (CPEWANTechnology.equalsIgnoreCase("SDH") || Edit_CPEWANTechnology.equalsIgnoreCase("SDH")) {
				if (VendorModel.contains("Juniper")) {
					IPConfige.verify_Juniper_SDH_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_Juniper_SDH_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_SDH_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);

				} else {
					IPConfige.verify_OneAccess_SDH_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_OneAccess_SDH_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_SDH_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
			} else if (CPEWANTechnology.equalsIgnoreCase("ADSL") || Edit_CPEWANTechnology.equalsIgnoreCase("ADSL")) {
				if (PEWANTechnology.equalsIgnoreCase("ATM") || Edit_PEWANTechnology.equalsIgnoreCase("ATM")) {
					IPConfige.verify_Ethernet_ATM_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_Ethernet_ATM_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_ATM_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);

				} else if (PEWANTechnology.equalsIgnoreCase("L2TP") || Edit_PEWANTechnology.equalsIgnoreCase("L2TP")) {
					IPConfige.verify_ADSL_L2TP_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_L2TP_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_L2TP_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
				} else if (PEWANTechnology.equalsIgnoreCase("Ethernet")
						|| Edit_PEWANTechnology.equalsIgnoreCase("Ethernet")) {
					IPConfige.verify_ADSL_Ethernet_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_Ethernet_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_Ethernet_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);

				}
			} else if (CPEWANTechnology.equalsIgnoreCase("SDSL") || Edit_CPEWANTechnology.equalsIgnoreCase("SDSL")) {
				if (PEWANTechnology.equalsIgnoreCase("ATM") || Edit_PEWANTechnology.equalsIgnoreCase("ATM")) {
					IPConfige.verify_Ethernet_ATM_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_Ethernet_ATM_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_ATM_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
				} else if (PEWANTechnology.equalsIgnoreCase("L2TP") || Edit_PEWANTechnology.equalsIgnoreCase("L2TP")) {
					IPConfige.verify_ADSL_L2TP_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_L2TP_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_L2TP_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
				} else if (PEWANTechnology.equalsIgnoreCase("Ethernet")
						|| Edit_PEWANTechnology.equalsIgnoreCase("Ethernet")) {
					IPConfige.verify_ADSL_Ethernet_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_Ethernet_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPConfige.verify_ADSL_Ethernet_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);

				}
			}
			
			Report.LogInfo("INFO", "TC - 14 - =====verifyInterfaceConfigueHistory====", "PASS");
			noCPE.verifyInterfaceConfigHistory(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 15 - =====verifySelectInterfaces_ColtTotal====", "PASS");
			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RemoveInterface_Selection");
			String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"AddInterface_Selection");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.selectInterfacelinkforDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (RemoveInterface_Selection.equalsIgnoreCase("yes")) {
				noCPE.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				System.out.println("interfaces are not removed");
			}
			if (AddInterface_Selection.equalsIgnoreCase("yes")) {
				noCPE.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				System.out.println("Interfaces are not added");
			}
			
			Report.LogInfo("INFO", "TC - 15 - =====verifyManageService====", "PASS");
			ResilentConfg.verifyManageService1(testDataFile, dataSheet, scriptNo, dataSetNo);
						
			//================================== Other Code =============================================
			Report.LogInfo("INFO", "TC - 17 - =====VerifyActelisConfiguration_AddDSLAMandHSL====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyAddDSLAMandHSLlink();
			ResilentConfg.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 18 - =====verifyAddCPEDeviceFunction====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			IPConfige.verifyAddCPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 19 - =====verifyAddedCPEDeviceInformation_====", "PASS");
			ResilentConfg.verifyAddedCPEDeviceInformation_View(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.testStatus_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 20 - =====verifyEditCPEDeviceFunction_====", "PASS");
			ResilentConfg.verifyEditCPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 21 - =====verifyRouterToolFunction_PE====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			IPConfige.navigateToViewCEPDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			
			Report.LogInfo("INFO", "TC - 22 - =====verifyAddEditDeleteRouteFunction_CPE_====", "PASS");
			
			noCPE.addRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.editRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 23 - =====verifyCustomerReadonlySNMPFunction_CPE====", "PASS");
			
			ResilentConfg.addCustomerReadonlySNMPFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.editCustomerReadonlySNMPFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.deleteCustomerReadonlySNMPFunction_CPE();
			
			Report.LogInfo("INFO", "TC - 24 - =====verifyExtraSubnetsFunction_CPE====", "PASS");
			ResilentConfg.addExtraSubnetFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				
			
			Report.LogInfo("INFO", "TC - 25 - =====verifyeditNATConfigurationFunction_CPE====", "PASS");
			ResilentConfg.editNATConfigurationFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			
			Report.LogInfo("INFO", "TC - 26 - =====verifyStaticNATMappingFunction_CPE====", "PASS");
			String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"StaticNATEdit");
			String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"DynamicNATEdit");

			if (StaticNATEdit.equalsIgnoreCase("yes") && DynamicNATEdit.equalsIgnoreCase("no")) {
				// New Customer Creation				
				ResilentConfg.addStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
				
				ResilentConfg.addDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (StaticNATEdit.equalsIgnoreCase("Yes") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
				
				Report.LogInfo("INFO", "TC - =====Static Mapping Function====", "PASS");
				ResilentConfg.addStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

				Report.LogInfo("INFO", "TC - =====Dynamic Mapping Function====", "PASS");

				ResilentConfg.addDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("no")) {
				Report.LogInfo("INFO", "'Static NAT & Dynamic NAT is not selected as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'Static NAT & Dynamic NAT is not selected as expected");

				Report.LogInfo("INFO", "'Static NAT Selection status is :  " + StaticNATEdit, "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO, "'Static NAT Selection status is :  " + StaticNATEdit);

				Report.LogInfo("INFO", "'Dynamic NAT Selection status is :  " + DynamicNATEdit, "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'Dynamic NAT Selection status is :  " + DynamicNATEdit);
			}
			
			Report.LogInfo("INFO", "TC - 27 - =====verifyDHCPServersonCPEFunction_CPE_====", "PASS");
			ResilentConfg.addDHCPServersonCPEFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.editDHCPServersonCPEFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.deleteDHCPServersonCPEFunction_CPE();
			
			Report.LogInfo("INFO", "TC - 28 - =====verifyDHCPIPV6ServersonCPEFunction_CPE_====", "PASS");
			ResilentConfg.addDHCPIPV6ServersonCPEFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.editDHCPIPV6ServersonCPEFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.deleteDHCPIPV6ServersonCPEFunction_CPE();
		
			Report.LogInfo("INFO", "TC - 29 - =====VerifyConfigurationPanel_CPE_====", "PASS");
			ResilentConfg.VerifyConfigurationPanel_CPE();

			Report.LogInfo("INFO", "TC - 30 - =====verifyFetchDeviceInterfaceFunction_CPE_====", "PASS");
			ResilentConfg.veriyFetchDeviceInterfacesFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 31 - =====deletePEdeviceFunction_NewlyCreatedDevice_====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.deleteDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 32 - =====deleteCPEdeviceFunction_====", "PASS");
			IPConfige.deleteCEPDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 33 - =====deleteServiceFunction_====", "PASS");
			noCPE.deleteSevice1(testDataFile, dataSheet, scriptNo, dataSetNo);
			

		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

}
