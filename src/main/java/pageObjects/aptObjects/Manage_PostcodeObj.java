package pageObjects.aptObjects;

public class Manage_PostcodeObj {
	
	public static class ManageColt{
		public static final String  AlertForServiceCreationSuccessMessage = "@xpath=//span[contains(text(),'Postcode successfully created.')]";
		public static final String  AlertUploadNt = "@xpath=//span[contains(text(),'NT Service Area successfully updated.')]";
		
		public static final String  AlertforUploadupdatefile = "@xpath=//div[@role='alert']/span";

		public static final String  alertMsg_managePOstcode = "@xpath=(//div[@role='alert'])[1]";
		public static final String  AlertForServiceCreationSuccessMessage_managePOstcode = "@xpath=(//div[@role='alert']/span)[1]";
		public static final String  successmessageForUploadUpdateFile = "@xpath=//span[contains(text(),'Emergency Number successfully updated.PSX sync started successfully.')]";
		public static final String  successMessageForEmergencyNumber = "@xpath=//span[contains(text(),'Emergency Number successfully created')]";

		public static final String  historyTableValue = "@xpath=//pre";

		public static final String  alertMsg = "@xpath=//div[@role='alert']/span";
		public static final String  fetchListofCountryNames = "@xpath=//div[@class='ag-body-viewport-wrapper ag-layout-auto-height']//div[@role='row']/div[@col-id='name']";
		public static final String  alertmessageUploadNT = "@xpath=//div[@role='alert']";
		public static final String  BelMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[1]";
		public static final String  DenMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[2]";
		public static final String  FraMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[3]";
		public static final String  GerMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[4]";
		public static final String  PorMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[5]";
		public static final String  SpaMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[6]";
		public static final String  SweMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[7]";
		public static final String  SwitMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[8]";
		public static final String  ManageColtNetworkLink = "@xpath=//span/b[contains(text(),'MANAGE COLT')]";
		public static final String  ManagePostcode = "@xpath=//li[text()='Manage Postcode for IMS platform']";
		public static final String  WildcardText = "@xpath=//div[label[text()='You can use * as wildcard']]/label";
		public static final String  Searchfield = "@xpath=(//input[@name='postcode'])";
		public static final String  searchField_IMSNT = "@xpath=//input[@id='imsNumber']";
		public static final String  Searchbtn = "@xpath=//span[contains(text(),'Search')]";
		public static final String  EmergencyAreaID = "@xpath=//span[contains(text(),'Emergency Area ID')]";
		public static final String  NTServiceArea = "@xpath=//span[contains(text(),'NT Service Area')]";
		public static final String  EmergencyAreaSubcom = "@xpath=//span[contains(text(),'Emergency Area (Subcom)')]";


		public static final String  NtServiceAreaLabel = "@xpath=//label[contains(text(),'NT Service Area')]";
		public static final String  EmergencyAreaSubcomLabel = "@xpath=//label[contains(text(),'Emergency Area (SUBCOM)')]";
		public static final String  EmergencyAreaIDSubcomLabel = "@xpath=//label[contains(text(),'Emergency Area ID(SUBCOM-ID)')]";


		public static final String  ToimplementLabel = "@xpath=//label[contains(text(),'To Implement Switch')]";
		public static final String  CityTransLabel = "@xpath=//label[contains(text(),'City Translator Number')]";
		public static final String  SubArea1ProLabel = "@xpath=//label[contains(text(),'SubArea1 (Province)')]";
		public static final String  SubArea1DLabel = "@xpath=//label[contains(text(),'SubArea1-ID (SubArea1-ID)')]";
		public static final String  SubArea2CommuLabel = "@xpath=//label[contains(text(),'SubArea2 (Community)')]";
		public static final String  SubArea2IDLabel = "@xpath=//label[contains(text(),'SubArea2-ID (SubArea2-ID)')]";
		public static final String  SubArea3BLabel = "@xpath=//label[contains(text(),'SubArea3 (b)')]";
		public static final String  SubAreaZipLabel = "@xpath=//label[contains(text(),'SubArea3-ID (ZIP code)')]";

		public static final String  EmergencyKeyLabel = "@xpath=//label[contains(text(),'Emergency Number Key')]";
		public static final String  ActualProviderLabel = "@xpath=//label[contains(text(),'Actual Provider Mapping')]";
		public static final String  DummyCodeLAbel = "@xpath=//label[contains(text(),'Dummy Code')]";


		public static final String  SubAreaProvince = "@xpath=//span[contains(text(),'SubArea1 (Province)')]";
		public static final String  SubArea1_ID = "@xpath=//span[contains(text(),'SubArea 1-ID (SubArea1-ID)')]";
		public static final String  SubArea2 = "@xpath=//span[contains(text(),'SubArea2 (Community)')]";
		public static final String  SubArea2_ID = "@xpath=//span[contains(text(),'SubArea 2-ID (SubArea2-ID)')]";
		public static final String  SubArea3 = "@xpath=//span[contains(text(),'SubArea 3 (b)')]";
		public static final String  SubArea3_ID = "@xpath=//span[contains(text(),'SubArea 3-ID (Zipcode)')]";
		public static final String  Lastupdate = "@xpath=//span[contains(text(),'Last Update ')]";
		public static final String  Addpostcode = "@xpath=//a[contains(text(),'Add Postcode')]";
		public static final String  Uploadupdatefile = "@xpath=//a[contains(text(),'Upload Update File')]";
		public static final String  ViewHistory = "@xpath=//a[contains(text(),'View History')]";
		public static final String  AddEmergencyNumber = "@xpath=//a[contains(text(),'Add Emergency Numbers')]";
		public static final String  SynchronizeAllpostcodes = "@xpath=//a[contains(text(),'Synchronize All Postcodes')]";
		public static final String  DownloadNt = "@xpath=//a[contains(text(),'Download NT Service Area')]";
		public static final String  UploadNt = "@xpath=//a[contains(text(),'Upload NT Service Area')]";
		//public static final String  PostcodeCheckbox = "@xpath=(//span[@class='ag-selection-checkbox'])";
		public static final String  PostcodeCheckbox = "@xpath=//div[text()='value']/parent::div//span//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String  PostcodeCheckbox1 = "@xpath=//div[text()='";
		public static final String  PostcodeCheckbox2 = "']/parent::div//span//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String  upload_update_choosefilebutton= "@xpath=//input[@id='file']";
		public static final String  ImsntCheckbox = "@xpath=(//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";
	//	public static final String  ImsntCheckbox = "@xpath=(//div[text()='value']/parent::div//span//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";
		public static final String  PostcodeAction = "@xpath=//button[text()='Action']";
		public static final String  PostcodeActionView = "@xpath=(//a[contains(text(),'View')])[2]";
		public static final String  PostcodeActionEdit = "@xpath=//a[contains(text(),'Edit')]";
		public static final String  PostcodeActionDelete = "@xpath=//a[contains(text(),'Delete')]";
		public static final String  ViewPostcodeEAID = "@xpath=//div[text()='11']";
		public static final String  PostcodeBackbtn = "@xpath=//button/span[text()='Back']";
		public static final String  PostcodeDeletebtn = "@xpath=//button[contains(text(),'Delete')]";
		public static final String  Chosefile = "@xpath=(//input[@id='file'])";
		public static final String  okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String  ViewHistoryPage = "@xpath=(//a[contains(text(),'value')])[1]";


		public static final String  EmergencyAreaIDSubcom_TextField = "@xpath=//input[@id='postcodeName']";

		public static final String  NTServiceAreaPostcode_Text = "@xpath=//input[@id='ntServiceArea']";

		public static final String  ToimplemetSwitch_Text = "@xpath=//input[@id='toImplementSwitch']";

		public static final String  CityTranslatorNmbr_Text = "@xpath=//input[@id='cityTranslatorNumber']";

		public static final String  SubAreaProvince_1_Text = "@xpath=//input[@id='subArea1']";

		public static final String  SubArea1D_Text = "@xpath=//input[@id='subArea1Id']";

		public static final String  SubArea2Community_Text = "@xpath=//input[@id='subArea2']";

		public static final String  SubArea2ID_Text = "@xpath=//input[@id='subArea2Id']";

		public static final String  SubArea3B_Text = "@xpath=//input[@id='subArea3']";

		public static final String  SubAreaZipcode_Text = "@xpath=//input[@id='subArea3Id']";

		public static final String  EmergencyAreaSub_Text = "@xpath=//input[@id='emergencyAreaDescription']";

		public static final String  EmergencyNmbrKey_Text = "@xpath=//input[@id='enPhoneText']";

		public static final String  ActualProvider_Text = "@xpath=//input[@id='enMappingText']";

		public static final String  DummyCode_Text = "@xpath=//input[@id='enCodeText']";
		public static final String  EmptyTextbox = "@xpath=(//select[@name='selectedNumber'])";
		public static final String  forwardarrow = "@xpath=//span[contains(text(),'>>')]";
		public static final String  BackwardArrow = "@xpath=(//button[contains(@class,'btn btnSquar btnWitdh undefined btn-secondary')])[2]";
		public static final String  OKbtn = "@xpath=//span[contains(text(),'Ok')]";
		public static final String  Cancelbtn = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String  xbutton = "@xpath=//span[text()='�']";
		public static final String  OkbtnPostcode = "@xpath=//span[contains(text(),'OK')]";
		public static final String  FileNamefield = "@xpath=(//label[contains(text(),'File Name')])";
		public static final String  Datefield = "@xpath=(//label[contains(text(),'Date')])";
		public static final String  UserField = "@xpath=(//label[contains(text(),'User')])";
		public static final String  Updatebyfield = "@xpath=(//label[contains(text(),'Updated By')])";
		//public static final String  !-- Denmark -- = "@xpath=
		public static final String  SubArea1_ID_Denmark = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String  SubArea2_ID_Denmark = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String  EmergencyArea_Kommune = "@xpath=//span[contains(text(),'Emergency Area(Kommune)')]";
		public static final String  SubArea1Den = "@xpath=//span[contains(text(),'SubArea1(SubArea1)')]";
		public static final String  SubArea2Den = "@xpath=//span[contains(text(),'SubArea2(SubArea2) ')]";
		public static final String  SubArea3Den = "@xpath=//span[contains(text(),'SubArea3(SubArea3) ')]";
		public static final String  SubArea3DenArea = "@xpath=//span[contains(text(),'SubArea3-ID(SubArea3-ID) ')]";
		public static final String  SubArea1DenLabel = "@xpath=//label[contains(text(),'SubArea1 (SubArea1)')]";
		public static final String  SubArea3Denlabel = "@xpath=//label[contains(text(),'SubArea3 (SubArea3)')]";
		public static final String  SubArea2DenLabel = "@xpath=//label[contains(text(),'SubArea2 (SubArea2)')]";
		public static final String  SubArea3IDDenLabel = "@xpath=//label[contains(text(),'SubArea3-ID (SubArea3-ID)')]";
		public static final String  EmergencyArea_Kommunelabel = "@xpath=//label[contains(text(),'Emergency Area (Kommune)')]";
		public static final String  EmergencyKommunerlabel = "@xpath=//label[contains(text(),'Emergency Area ID (Kommunenummer)')]";
		public static final String  EmergencyAreaFranceLabel = "@xpath=//label[contains(text(),'Emergency Area (City (Area))')]";
		//public static final String  !-- France -- = "@xpath=
		public static final String  SubArea2_ID_France = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String  EmergencyAreaIDFranceLabel = "@xpath=//label[contains(text(),'Emergency Area ID (INSEE Code)')]";
		public static final String  EmergencyAreaCityArea = "@xpath=//span[contains(text(),'Emergency Area(City(Area))')]";
		public static final String  SubArea1Fra = "@xpath=//span[contains(text(),'SubArea1(Department)')]";
		public static final String  Department_ID = "@xpath=//span[contains(text(),'SubArea1-ID(Department-ID)')]";
		public static final String  SubArea2Fra = "@xpath=//span[contains(text(),'SubArea2(SubArea2) ')]";
		public static final String  SubArea3FraArea = "@xpath=//span[contains(text(),'SubArea3-ID(SubArea3-ID) ')]";
		public static final String  SubAreaDepartFrance = "@xpath=//label[contains(text(),'SubArea1 (Department)')]";
		public static final String  SubArea1DFranLabel = "@xpath=//label[contains(text(),'SubArea1-ID (Department ID)')]";
		
		//public static final String  !--Germany  -- = "@xpath=
		public static final String  SubArea1_ID_Germany = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String  SubArea2_ID_Germany = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";

		public static final String  EmergencyAreaLocal = "@xpath=//span[contains(text(),'Emergency Area(Local Area Name (U_ON_NAME))')]";
		public static final String  SubArea1Ger = "@xpath=//span[contains(text(),'SubArea1(SubArea1)')]";
		public static final String  SubArea2Ger = "@xpath=//span[contains(text(),'SubArea2(SubArea2) ')]";
		public static final String  EmergencyAreaIDGermanyLabel = "@xpath=//label[contains(text(),'Emergency Area ID (Local Area ID (U_ONKZ))')]";
		public static final String  EmergencyAreaLocalGermany = "@xpath=//label[contains(text(),'Emergency Area (Local Area Name (U_ON_NAME))')]";
		
		//public static final String  !--Portugal -- = "@xpath=
		public static final String  SubArea1_ID_Portugal = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String  SubArea2_ID_Portugal = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String  EmergencyAreaProvGermanyLAbel = "@xpath=//label[contains(text(),'Emergency Area (Province)')]";
		public static final String  EmergencyAreaIDPortugalLabel = "@xpath=//label[contains(text(),'Emergency Area ID (CLISRVPF INDEX)')]";
		public static final String  EmergencyAreaProvince = "@xpath=//span[contains(text(),'Emergency Area(Province)')]";
		
		//public static final String  !-- Spain -- = "@xpath=
		public static final String  SubArea2_ID_Spain = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String  EmergencyAreaID_SpainLabel = "@xpath=//label[contains(text(),'Emergency Area ID (Municip.Code)')]";
		public static final String  SubAreaProvinciaSpainLabel = "@xpath=//label[contains(text(),'SubArea1 (Provincia)')]";
		public static final String  SubArea1DSpainLabel = "@xpath=//label[contains(text(),'SubArea1-ID (Prov.Code)')]";
		public static final String  EmergencyAReaMunc_Label = "@xpath=//label[contains(text(),'Emergency Area (Municipality)')]";
		public static final String  EmergencyAreaMunicipality = "@xpath=//span[contains(text(),'Emergency Area(Municipality)')]";
		public static final String  SubArea1Provincia = "@xpath=//span[contains(text(),'SubArea1(Provincia)')]";
		public static final String  SubArea1DSpa = "@xpath=//span[contains(text(),'SubArea1-ID(Prov.Code)')]";
		
		//public static final String  !--Sweden -- = "@xpath=
		public static final String  SubArea1_ID_Sweden = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String  EmergencyAreaIDSweden_Label = "@xpath=//label[contains(text(),'Emergency Area ID (Kommun-ID/ALARM-ID)')]";
		public static final String  SubAreaLanSweden_Label = "@xpath=//label[contains(text(),'SubArea1 (Lan)')]";
		public static final String  SubArea2DSweden_Label = "@xpath=//label[contains(text(),'SubArea2-ID (ZIP)')]";
		public static final String  EmergencyAreaKommun_Label = "@xpath=//label[contains(text(),'Emergency Area (Kommun)')]";

		public static final String  EmergencyAreaKommun = "@xpath=//span[contains(text(),'Emergency Area(Kommun)')]";
		public static final String  SubArea1Lan = "@xpath=//span[contains(text(),'SubArea1(Lan)')]";

		//public static final String  !-- Switzerland -- = "@xpath=
		public static final String  EmergencyAreaIDSwitLabel = "@xpath=//label[contains(text(),'Emergency Area ID (Emergency Area ID)')]";
		public static final String  SubArea1Caton_Label = "@xpath=//label[contains(text(),'SubArea1 (Canton)')]";
		public static final String  SubArea1DGDEKT = "@xpath=//label[contains(text(),'SubArea1-ID (GDEKT)')]";
		public static final String  SubAreaDistrictLabel = "@xpath=//label[contains(text(),'SubArea2 (District)')]";
		public static final String  SuArea2IDSwit = "@xpath=//label[contains(text(),'SubArea2-ID (GDEBZNR)')]";
		public static final String  EmergencyAreaSwit = "@xpath=//label[contains(text(),'SubArea2-ID (GDEBZNR)')]";
		public static final String  SubArea3MunLabel = "@xpath=//label[contains(text(),'SubArea3(Municipality) ')]";
		public static final String  EmergencyArea = "@xpath=//span[contains(text(),'Emergency Area(Emergency Area)')]";
		public static final String  SubAreaCanton = "@xpath=//span[contains(text(),'SubArea1(Canton)')]";
		public static final String  SuAreaGDEKT = "@xpath=//span[contains(text(),'SubArea1-ID(GDEKT)')]";
		public static final String  SubAreaDistrict = "@xpath=//span[contains(text(),'SubArea2(District) ')]";
		public static final String  SubAreaGDE = "@xpath=//span[contains(text(),'SubArea2-ID(GDEBZNR) ')]";
		public static final String  SubArea3Mun = "@xpath=//span[contains(text(),'SubArea3(Municipality) ')]";
		public static final String  SubArea3Swit = "@xpath=//span[contains(text(),'SubArea3-ID(SubArea3-ID) ')]";

		public static final String  delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String  deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";
		public static final String  deletebutton = "@xpath=//button[@class='btn btn-danger']";

		//public static final String  !-- IMSNT -- = "@xpath=
		public static final String  OperationDD = "@xpath=//span[@role='option']";
		public static final String  TranDeltemsg = "@xpath=//span[contains(text(),'NumberTranslation successfully marked for deletion.')]";
		public static final String  SyncTransMsg = "@xpath=//span[text()='Sync started successfully. Please check the sync status of number translation.']";
		public static final String  UpdateTransmsg = "@xpath=//span[text()='NumberTranslation successfully updated.Sync started successfully. Please check the sync status of number translation.']";
		public static final String  ActionDD = "@xpath=//a[@role='button']";
		public static final String  Translationmsg = "@xpath=//span[contains(text(),'NumberTranslation successfully created.Sync started successfully. Please check the sync status of number translation.')]";
		public static final String  listofaddressDropDown = "@xpath=//div/span[@role='option']";
		public static final String  ActCountrycode = "@xpath=//label[contains(text(),'Country Code')]/following-sibling::*[1]";
		public static final String  Wildcardmsg = "@xpath=//span[contains(text(),'The following errors were encountered: At least 3 digits to be entered in the search field.')]";
		public static final String  ManageIms = "@xpath=//li[text()='Manage IMS Number Translation']";
		public static final String  SEManageTran = "@xpath=(//a[text()='Manage Number Translation'])[1]";
		public static final String  UKManageTran = "@xpath=(//a[text()='Manage Number Translation'])[2]";
		public static final String  BRManageTran = "@xpath=(//a[text()='Manage Number Translation'])[3]";
		public static final String  PTManageTran = "@xpath=(//a[text()='Manage Number Translation'])[4]";
		public static final String  CHManageTran = "@xpath=(//a[text()='Manage Number Translation'])[5]";
		public static final String  IEManageTran = "@xpath=(//a[text()='Manage Number Translation'])[6]";
		public static final String  ATManageTran = "@xpath=(//a[text()='Manage Number Translation'])[7]";
		public static final String  ITManageTran = "@xpath=(//a[text()='Manage Number Translation'])[8]";

		public static final String  NumberToTranslate = "@xpath=//span[contains(text(),'Number To Translate')]";
		public static final String  NumberToTranslated = "@xpath=//span[contains(text(),'Number Translated')]";
		public static final String  Country = "@xpath=//span[contains(text(),'Country')]";
		public static final String  Carrier = "@xpath=//span[contains(text(),'Carrier')]";
		public static final String  CarrierLabel = "@xpath=//label[contains(text(),'Carrier')]";
		public static final String  NatureofAddress = "@xpath=//span[contains(text(),'Nature Of Address')]";
		public static final String  NatureofAddress_1 = "@xpath=//label[contains(text(),'Nature of Address')]";
		public static final String  PSXsyncstatus = "@xpath=//span[contains(text(),'PSX Sync Status')]";
		public static final String  AddnumberTranslation = "@xpath=//a[contains(text(),'Add Number Translation')]";
		public static final String  ViewUploadHistory = "@xpath=//a[contains(text(),'View Upload History')]";
		public static final String  ViewUIHistory = "@xpath=//a[contains(text(),'View UI History')]";
		public static final String  DownloadNumberTranslation = "@xpath=//a[contains(text(),'Download Number Translation')]";
		public static final String  SynchronizeAll = "@xpath=//a[contains(text(),'Synchronize All')]";
		public static final String  TranslationWildcard = "@xpath=//div[label[text()='You can use * as wildcard']]";
		public static final String  Numbertranslate = "@xpath=//label[contains(text(),'Number to Translate')]";
		public static final String  Countrycode = "@xpath=//label[contains(text(),'Country Code')]";
		public static final String  Prefix = "@xpath=//label[contains(text(),'Prefix')]";
		public static final String  Countrylabel = "@xpath=//label[contains(text(),'Country')]/following-sibling::*[1]";
		public static final String  NatureofAddressLabel = "@xpath=//label[contains(text(),'Nature of Address')]";
		public static final String  NumberToTranslatedLabel = "@xpath=//label[contains(text(),'Number Translated')]";
		public static final String  RangeLabel = "@xpath=//label[contains(text(),'Range')]";
		public static final String  sequenceLabelName = "@xpath=//label[contains(text(),'Sequence')]";
		public static final String  CountrycodeValue = "@xpath=//label[contains(text(),'Country Code')]/following-sibling::*[1]";
		public static final String  Natureofadddropdown = "@xpath=(//div[label[text()='Nature of Address']])[1]//input";
		public static final String  Numbertotranslate_Text = "@xpath=//input[@id='numbertotranslate']";
		public static final String  Numbertotranslate_Search = "@xpath=//input[@id='numbertotranslatesearch']";
		public static final String  Country_Text = "@xpath=//input[@id='country']";
		public static final String  Numbertranslated_Text = "@xpath=//input[@id='numbertranslated']";
		public static final String  editPage_NumberToTranslateElement = "@xpath=//div[label[text()='Number to Translate']]//div";
		public static final String  editPage_countryCodeElement = "@xpath=//div[label[text()='Country Code']]//div";
		public static final String  RangeFlag = "@xpath=//input[@id='rangeFlag']";
		public static final String  sequencetextField = "@xpath=//div[label[text()='Sequence']]//input";
		public static final String  TranslationView = "@xpath=(//a[contains(text(),'View')])[3]";
		public static final String  Closesign = "@xpath=//span[contains(text(),'�')]";
		public static final String  ddiPrefix_checkbox = "@xpath=//div[label[text()='DDI Prefix']]//input";
		public static final String  prefix_checkbox = "@xpath=//input[@id='ddiPrefixFlag']";
		public static final String  PrefixText = "@xpath=//input[@id='prefix']";
		public static final String  Carrier_Text = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30']";
		public static final String  carrierDropdown = "@xpath=//div[label[text()='Carrier']]//input";
		public static final String  viewPage_CountryField = "@xpath=(//div[label[contains(text(),'Country')]]/div[1])[2]";
		public static final String  Synchronize = "@xpath=(//a[contains(text(),'Synchronize')])[2]";
		public static final String  ImsSearcfield = "@xpath=//input[@id='imsNumber']";
		public static final String  NoAddDD = "@xpath=//div[(text()='�')]";
		public static final String  natureOfAddressDropdown = "@xpath=(//div[label[text()='Nature of Address']]//input)[1]";
		public static final String  NoAddTextbox = "@xpath=//label[contains(text(),'Nature of Address')]/following-sibling::*[2]";
		public static final String  francefileupload = "@xpath=ChosefileTrans";
		public static final String  ChosefileTrans = "@xpath=(//input[@id='file'])";
		public static final String  ChosefileTransImsnt = "@xpath=(//input[@id='fileupload'])";
		public static final String  CountryPresent = "@xpath=(//div[@class='ag-body-container ag-layout-normal']//div[@role='row'])";
		public static final String  ViewUploadHisClose = "@xpath=//span[(text()='�')]";
		public static final String  Operation = "@xpath=//label[contains(text(),'Operation')]";
		public static final String  ViewUIUser = "@xpath=//label[contains(text(),'User')]";
		public static final String  ViewUINumbertoTrans = "@xpath=//label[contains(text(),'Number to Translate')]";
		public static final String  operationDropdown_viewUIhistory = "@xpath=(//div[label[text()='Operation']]//input)[1]";
		public static final String  OperationText = "@xpath=//input[@class='react-dropdown-select-input css-1j547uo-InputComponent e11wid6y0']";
		public static final String  ViewUISearch = "@xpath=(//span[contains(text(),'Search')])[2]";
		public static final String  UserText = "@xpath=//input[@id='user']";
		public static final String  OperationText_1 = "@xpath=//label[contains(text(),'Operation')]/following-sibling::*";
		//public static final String  !-- public static final String  OperationText_1 = "@xpath=//div[@class='react-dropdown-select sc-bdVaJa ibTDPh css-11x0fbf-ReactDropdownSelect e1gzf2xs0']";
		public static final String  OperationText_2 = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30']";

		public static final String  OperationTextClick = "@xpath=//div[contains(text(),'�')]";
		public static final String  numberToTranslateColumn_filter = "@xpath=(//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String  numberToTranslate_textField = "@xpath=//input[@id='filterText']";

		
	}
	
	public static class HSS{
		public static final String  ServiceMessg = "@xpath=//span[contains(text(),'Service successfully created.')]";
		public static final String  ServiceIdentification = "@xpath=//input[@id='serviceIdentification']";
		public static final String  forwardarrow = "@xpath=//span[contains(text(),'>>')]";
		public static final String  forwardarrow_1 = "@xpath=(//span[contains(text(),'>>')])[2]";
		public static final String  EmailText = "@xpath=//div[label[contains(text(),'Email')]]//input";
		public static final String  EmailLabel = "@xpath=//label[contains(text(),'Email')]";
		public static final String  PhoneContact = "@xpath=//input[@id='phoneContact']";
		public static final String  RemarkText = "@xpath=//textarea[@name='remark']";
		public static final String  RemarkLabel = "@xpath=//label[contains(text(),'Remark')]";
		public static final String  PerformancereportingLabel = "@xpath=//label[contains(text(),'Performance Reporting')]";
		public static final String  performanceReportingcheckbox = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String  ServiceIdentificationLabel = "@xpath=//label[contains(text(),'Service Identification')]";
		public static final String  ServiceTypeLabel = "@xpath=//label[contains(text(),'Service Type')]";
		public static final String  PhoneContactLabel = "@xpath=//label[contains(text(),'Phone Contact')]";
		public static final String  WaveServicelabel = "@xpath=//label[contains(text(),'Wave Service')]";
		public static final String  waveServiceCheckbox = "@xpath=//div[label[contains(text(),'Wave Service')]]//input";
		public static final String  serviceIdentificationerrmsg = "@xpath=//div[contains(text(),'Service Identification')]";
		public static final String  OKbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String  Cancelbtn = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String  OrderContractNmbr = "@xpath=(//label[contains(text(),'Order/Contract Number (Parent SID)')])[2]";
		public static final String  RFIRFQ = "@xpath=(//label[contains(text(),'RFI/RFQ/IP Voice Line Number')])[2]";
		public static final String  RFIRFQ_Text = "@xpath=//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String  OrderUpdatemsg = "@xpath=//span[text()='Order successfully updated']";
		public static final String  Selectorderlabel = "@xpath=//label[text()='Select Order']";
		public static final String  AddDeviceTogglebutton = "@xpath=(//div[div[label[text()='Add Device']]]//div[@class='react-switch-handle'])[1]";
		public static final String  OrderContractNumbertoggleButton = "@xpath=(//div[div[label[text()='Select Order']]]//div[@class='react-switch-handle'])[1]";
		public static final String  OrderName = "@xpath=//input[@id='orderName']";
		public static final String  createButton = "@xpath=//button[@class='btn btnSquar btnWitdh undefined btn-secondary']";
		public static final String  CreateOrderSuccessMessage = "@xpath=//span[contains(text(),'Order created successfully')]";
		public static final String  OrderChangemsg = "@xpath=//span[contains(text(),'Order successfully  changed.')]";
		public static final String  AddDevice = "@xpath=//a[text()='Add Device']";
		public static final String  DeviceNameLabel = "@xpath=//label[text()='Device Name']";
		public static final String  VendorLabel = "@xpath=//label[text()='Vendor/Model']";
		public static final String  ManagementAddLabel = "@xpath=//label[text()='Management Address']";
		public static final String  SnmproLabel = "@xpath=//label[text()='Snmpro']";
		public static final String  Countrylabel = "@xpath=//label[text()='Country']";
		public static final String  CityLabel = "@xpath=//label[text()='City']";
		public static final String  SiteLabel = "@xpath=//label[text()='Site']";
		public static final String  PremiseLabel = "@xpath=//label[text()='Premise']";
		public static final String  DeviceNameText = "@xpath=//input[@id='deviceName']";
		public static final String  ManagementAddText = "@xpath=//input[@id='addressSelect']";
		public static final String  Deviceemsg = "@xpath=//span[contains(text(),'Device successfully created.')]";
		public static final String  Devicedeleteemsg = "@xpath=//span[contains(text(),'Device successfully deleted')]";
		public static final String  InterfaceSpeedDropdown = "@xpath=(//div[label[text()='Interface Speed']]//input)[1]";
		public static final String  TypeofServiceDropdown = "@xpath=(//div[label[text()='Type Of Service']]//input)[1]";
		public static final String  ChangeordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number (Parent SID)']]//input)[1]";
		public static final String  AddDeviceName = "@xpath=(//div[label[text()='Name']]//input)[1]";
		public static final String  Xbutton = "@xpath=//div[text()='�']";
		public static final String  Backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String  Synchronizemsg = "@xpath=//span[contains(text(),'Sync started successfully. Please check the sync status of this service.')]";
		public static final String  Errormsg = "@xpath=//div[text()='Error']";
		
	}
	
	
    
    
}
