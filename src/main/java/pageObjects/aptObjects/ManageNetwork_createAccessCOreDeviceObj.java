package pageObjects.aptObjects;

public class ManageNetwork_createAccessCOreDeviceObj {
	
	public static class ManageNetwork{
		public static final String  managenetwork_header="@xpath=(//div[@class='heading-green-row row'])[1]//div";
		public static final String  status_header="@xpath=(//div[@class='heading-green-row row'])[2]//div";
		public static final String  synchronization_header="@xpath=(//div[@class='heading-green-row row'])[3]//div";
		public static final String  status_devicecolumn="@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String  status_statuscloumn="@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Status']";
		public static final String  status_lastmodificationcolumn="@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Last Modification']";
		public static final String  status_Action="@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String  synchronization_devicecolumn="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String  synchronization_syncstatuscolumn="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Sync Status']";
		public static final String  synchronization_smartscolumn="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Smarts']";
		public static final String  synchronization_FetchInterfacescolumn="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Fetch Interfaces']";
		public static final String  synchronization_vistamartdevice="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='VistaMart Device']";
		public static final String  synchronization_actioncolumn="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String  status_devicevalue="@xpath=//div[text()='Status']//following::label[text()='Device'][1]//following::a[1]";
		//public static final String  status_devicevalue="@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[1]";
		public static final String  status_statusvalue="@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[2]";
		public static final String  status_lastmodificationvalue="@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[3]";
		public static final String  status_statuslink="@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[2]";
		public static final String  status_viewinterfaceslink="@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[3]";
		public static final String  synchronization_devicevalue="@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div//a)[1]";
		public static final String  synchronization_syncstatusvalue="@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']/div[2]/span";
		public static final String  synchronization_smartsvalue="@xpath=(//label[text()='Smarts']/parent::div/parent::div/following-sibling::div//div)[3]/span";
		public static final String  synchronization_fetchinterfacesvalue="@xpath=(//label[text()='Fetch Interfaces']/parent::div/parent::div/following-sibling::div//div)[4]/span";
		public static final String  synchronization_vistamartValue_noManageAddress="@xpath=(//div[@class='div-margin row']//div)[9]//span";
		public static final String  synchronization_vistamartdevicevalue="@xpath=(//div[@class='div-margin row']//div)[10]//span";
		public static final String  synchronization_synchronizelink="@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div//a)[2]";
		public static final String  managenetwork_backbutton="@xpath=(//div[@class='div-margin row'])[4]//button";
		public static final String  smarts_datetimevalue="@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[4]//span/parent::div";
		public static final String  fetchinterfaces_datetime="@xpath=(//label[text()='Fetch Interfaces']/parent::div/parent::div/following-sibling::div//div)[4]";
		public static final String  vistamartdevice_datetime="@xpath=(//label[text()='VistaMart Device']/parent::div/parent::div/following-sibling::div//div)[5]";
		public static final String  vistamartDEvice_dateTime_noManageAddress="@xpath=(//label[text()='VistaMart Device']/parent::div/parent::div/following-sibling::div//div)[4]";
		public static final String  Sync_successmsg="@xpath=//div[@role='alert']//span";
		public static final String  staus_statuspopup="@xpath=//div[@class='modal-content']";
		public static final String  Statuspage_header="@xpath=//div[@class='modal-header']//div";
		public static final String  statuspage_nameheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']";
		public static final String  statuspage_vendormodelheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']";
		public static final String  statuspage_managementaddressheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']";
		public static final String  statuspage_snmproheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']";
		public static final String  statuspage_countryheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']";
		public static final String  statuspage_cityheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']";
		public static final String  statuspage_siteheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']";
		public static final String  statuspage_premiseheader="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']";
		public static final String  statuspage_namevalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']/following-sibling::div";
		public static final String  statuspage_vendormodelvalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']/following-sibling::div";
		public static final String  statuspage_managementaddressvalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']/following-sibling::div";
		public static final String  statuspage_snmprovalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']/following-sibling::div";
		public static final String  statuspage_countryvalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']/following-sibling::div";
		//public static final String  statuspage_cityvalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']/following-sibling::div";
		//public static final String  statuspage_sitevalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']/following-sibling::div";
		//public static final String  statuspage_premisevalue="@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']/following-sibling::div";
		public static final String  statuspage_cityvalue="@xpath=//label[text()='City']/following-sibling::div";
		public static final String  statuspage_sitevalue="@xpath=//label[text()='Site']/following-sibling::div";
		public static final String  statuspage_premisevalue="@xpath=//label[text()='Premise']/following-sibling::div";
		
		public static final String  Statuspage_statusheader="@xpath=//div[@class='modal-content']//div[@class='heading-green-row row']//div";
		public static final String  statuspage_currentstatusfieldheader="@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']";
		public static final String  statuspage_newstatusfieldheader="@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='New Status']";
		public static final String  statuspage_currentstatusvalue="@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']/following-sibling::div";
		public static final String  statuspage_newstatusdropdown="@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select";
		public static final String  statuspage_newstatusdropdownvalue="@xpath=((//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select//option)[1]";
		public static final String  statuspage_okbutton="@xpath=(//div[@class='modal-content']//button[@type='button']//span)[3]";
		public static final String  statuspage_statuscolumnheader="@xpath=//span[@role='columnheader'][text()='Status']";
		public static final String  statuspage_changedon_columnheader="@xpath=//span[@role='columnheader'][text()='Changed On']";
		public static final String  statuspage_changedby_columnheader="@xpath=//span[@role='columnheader'][text()='Changed By']";
		public static final String  statuspage_newstatusvalue="@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[1]";
		public static final String  statuspage_changedonvalue="@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[2]";
		public static final String  statuspage_changedbyvalue="@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[3]";
		public static final String  statuspage_closebutton="@xpath=(//div[@class='modal-content']//button[@type='button'])[1]";
		public static final String  viewinterfacepage_header="@xpath=//div[@class='modal-header']//div";
		public static final String  viewinterfacepage_interfacesubheader="@xpath=//div[@class='modal-body']//div[@class='heading-green-row row']/div";
		public static final String  viewinterface_devicenamecolumnheader="@xpath=(//div[@col-id='deviceName']//div[@ref='eLabel'])[1]/span[1]";
		public static final String  interfacename_columnheader="@xpath=(//div[@col-id='name']//div[@ref='eLabel'])[1]/span[1]";
		public static final String  interfaceaddress_columnheader="@xpath=(//div[@col-id='address']//div[@ref='eLabel'])[1]/span[1]";
		public static final String  interfacetype_columnheader="@xpath=(//div[@col-id='type.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String  interfaceaddress_rowvalue="@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='address'])[1]";
		public static final String  interfacetype_rowvalue="@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='type.desc'])[1]";
		public static final String  viewinterface_status_columnheader="@xpath=(//div[@col-id='currentStatus.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String  viewinterface_status_rowvalue="@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='currentStatus.desc'])[1]";
		public static final String  viewinterface_lastmod_columnheader="@xpath=(//div[@col-id='m_time']//div[@ref='eLabel'])[1]/span[1]";
		public static final String  viewinterface_lastmod_rowvalue="@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='m_time'])[1]";
		public static final String  viewinterface_closebutton="@xpath=(//div[@class='modal-header']//button[@type='button']/span)[1]";
		public static final String  statuspage_interfaceheader="@xpath=(//div[@class='modal-header']//div)[2]";
		public static final String  interface_statuspage_namefield="@xpath=//label[text()='Name']";
		public static final String  interface_statuspage_interfaceaddressfield="@xpath=//label[text()='Interface Address']";
		public static final String  interface_statuspage_currentstatusfield="@xpath=//label[text()='Current Status']";
		public static final String  interface_statuspage_newstatusfield="@xpath=//label[text()='New Status']";
		public static final String  interface_statuspage_namevalue="@xpath=//label[text()='Name']/following-sibling::div";
		public static final String  interface_statuspage_interfaceaddressvalue="@xpath=//label[text()='Interface Address']/following-sibling::div";
		public static final String  interface_statuspage_currentstatusvalue="@xpath=//label[text()='Current Status']/following-sibling::div";
		public static final String  interface_statuspage_newstatusdropdown="@xpath=//label[text()='New Status']/parent::div//select";
		public static final String  interface_statuspage_newstatusdropdownvalue="@xpath=//label[text()='New Status']/parent::div//select/option";
		public static final String  interface_statuspage_okbutton="@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//button[@type='button']//span";
		public static final String  interface_statuspage_statuscolumnheader="@xpath=(//div[@ref='eLabel']//span[text()='Status'])[2]";
		public static final String  interface_statuspage_changedon_columnheader="@xpath=//div[@ref='eLabel']//span[text()='Changed On']";
		public static final String  interface_statuspage_changedby_columnheader="@xpath=//div[@ref='eLabel']//span[text()='Changed By']";
		public static final String  interface_statuspage_newstatusvalue="@xpath=(//div[@role='gridcell'][@col-id='status.desc'])[1]";
		public static final String  interface_statuspage_changedonvalue="@xpath=(//div[@role='gridcell'][@col-id='changeDate'])[1]";
		public static final String  interface_statuspage_changedbyvalue="@xpath=(//div[@role='gridcell'][@col-id='user'])[1]";
		public static final String  interface_statuspage_closebutton="@xpath=(//div[@class='modal-header']//button[@type='button'])[2]";
		public static final String  interface_statuspage_statusheader="@xpath=(//div[@class='modal-body']//div[@class='heading-green-row row']//div)[2]";
		public static final String  searchdevice_header="@xpath=(//div[@class='heading-green-row row'])[1]//div";
		public static final String  viewpage_backbutton="@xpath=//span[text()='Back']";
		//New elements
		public static final String  InterfaceAddressRowValue="@xpath=//div[@role='gridcell'][@col-id='address'])[1]";
		public static final String  InterfaceAddressRowValue1="@xpath=(//div[@role='gridcell'][@col-id='address'])[1]";
		public static final String  StatusLink1 ="@xpath=//div[@role='gridcell']/parent::div[@row-id=";
		public static final String  StatusLink2 ="]//div[@col-id='Status']/div/a";
		
		public static final String nextButton = "@xpath=//div[@class='ag-div-margin row']//div//button[text()='Next']";
		
		public static final String InterfaceNameRowID1 = "@xpath=//div[@role='gridcell'][text()='";
		public static final String InterfaceNameRowID2 = "']/parent::div[@role='row']";
		
		public static final String DeviceNamevalue1 = "@xpath=//div[@role='gridcell']/parent::div[@row-id=";
		public static final String DeviceNamevalue2 = "]//div[@col-id='deviceName']";
		
		public static final String InterfaceNamevalue1 = "@xpath=//div[@role='gridcell']/parent::div[@row-id=";
		public static final String InterfaceNamevalue2 = "]//div[@col-id='name']";
		
		public static final String InterfaceAddressvalue1 = "@xpath=//div[@role='gridcell']/parent::div[@row-id=";
		public static final String InterfaceAddressvalue2 = "]//div[@col-id='address']";
	}
}
