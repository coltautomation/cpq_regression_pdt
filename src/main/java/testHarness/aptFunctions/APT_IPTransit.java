package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.APT_IPTransitObj;
//import pageObjects.aptObjects.Lanlink_OLO_Obj;

public class APT_IPTransit extends SeleniumUtils {
	// Lanlink_Outbandmanagementhelper Outband = new
	// Lanlink_Outbandmanagementhelper();
	// Lanlink_OLO OLO = new Lanlink_OLO();

	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();
	public static String InterfaceName = null;

	public APT_IPTransit() {
		super();
	}

	WebElement el;

	SoftAssert sa = new SoftAssert();
	
	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		
			//ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer Creation Functionality");

		
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink," Manage Customer Service Link");
				click(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink,"ManageCustomerServiceLink");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.createcustomerlink," create customer link");
				click(APTIPAccessNoCPE.APTNoCPE.createcustomerlink,"Create Customer Link");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.createcustomer_header,"create customer page header");
				
				//scrolltoend();
				verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton,"ok");
				click(APTIPAccessNoCPE.APTNoCPE.okbutton,"ok");
				
				//Warning msg check
				verifyExists(APTIPAccessNoCPE.APTNoCPE.customernamewarngmsg,"Legal Customer Name");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.countrywarngmsg,"Country");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ocnwarngmsg,"OCN");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.typewarngmsg,"Type");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.emailwarngmsg,"Email");
			
				//Create customer by providing all info
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.nametextfield,"name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.nametextfield,name,"name");
		
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.maindomaintextfield,"Main Domain");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.maindomaintextfield,maindomain,"Main Domain");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.country,"country");
			//	sendKeys(APTIPAccessNoCPE.APTNoCPE.country,country,"country");
				addDropdownValues_commonMethod("Country",APTIPAccessNoCPE.APTNoCPE.country,country);
			
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ocntextfield,"ocn");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.ocntextfield,ocn,"ocn");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.referencetextfield,"reference");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.referencetextfield,reference,"reference");
			
				verifyExists(APTIPAccessNoCPE.APTNoCPE.technicalcontactnametextfield,"technical contact name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.technicalcontactnametextfield,technicalcontactname,"technical contact name");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.typedropdown,"technical contact name");
				//sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown,type,"technical contact name");
				addDropdownValues_commonMethod("Type",APTIPAccessNoCPE.APTNoCPE.type,type);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield,"email");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield,email,"email");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.phonetextfield,"phone text field");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.phonetextfield,phone,"phone text field");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.faxtextfield,"fax text field");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.faxtextfield,fax,"fax text field");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton,"ok");
				click(APTIPAccessNoCPE.APTNoCPE.okbutton,"ok");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.customercreationsuccessmsg,"Customer successfully created.");
				
			}

	public void deleteService() throws InterruptedException, IOException, AWTException {
		verifyExists(APT_IPTransitObj.APT_IPTransit.orderpanelheader, "Order panel header");
		// ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.orderpanelheader);
		// Delete Service

		ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown);
		verifyExists(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown, "service creation drop down");
		click(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown,"serviceactiondropdown");

		verifyExists(APT_IPTransitObj.APT_IPTransit.delete, "delete option");
		click(APT_IPTransitObj.APT_IPTransit.delete,"Delete option");

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		
		//waitForAjax();
		// Outband.verifysuccessmessage("Service deleted successfully");

	}

	public void verifyManagementOptionspanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String ManagementOptionsHeader = "Management Options";
		String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Performance Reporting");
		String IPGuardian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IP Guardian");

		scrollUp();
		ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.managementoptions_header);

		// ((JavascriptExecutor)
		// driver).executeScript("window.scrollBy(0,-100)");

		(APT_IPTransitObj.APT_IPTransit.managementoptions_header).equals(ManagementOptionsHeader);
		(APT_IPTransitObj.APT_IPTransit.managementoptions_performancereporting).equals(PerformanceReporting);
		(APT_IPTransitObj.APT_IPTransit.managementoptions_ipguardian).equals(IPGuardian);

	}

	public void webelementpresencelogger(WebElement ele, String msg) {
		boolean flag = ele.isDisplayed();
		System.out.println("element presence state : " + flag);
		if (flag) {
			System.out.println("webElement is present " + ele.getText());
			Reporter.log(msg);
		} else {
			System.out.println("webElement is not  present" + ele.getText());
			Reporter.log(msg);
		}
	}

	/*
	 * public void verifyInterfaceConfigHistory() throws InterruptedException,
	 * IOException { String header = "Interface Configuration History"; String
	 * date = "Date"; String filename = "File Name";
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.back, "View page back button");
	 * ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.back); waitForAjax();
	 * 
	 * (APT_IPTransitObj.APT_IPTransit.interfaceconfighistory_header).
	 * equalsIgnoreCase(header);
	 * (APT_IPTransitObj.APT_IPTransit.date_column).equalsIgnoreCase(date);
	 * (APT_IPTransitObj.APT_IPTransit.filename_column).equalsIgnoreCase(
	 * filename);
	 * 
	 * waitForAjax();
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton,
	 * "View page back button");
	 * 
	 * ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton
	 * ); waitForAjax();
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton,
	 * "View page back button");
	 * click(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton,
	 * "View page back button");
	 * 
	 * }
	 */

	public void verifyInterfaceConfigHistory(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
		// String name = DataMiner.fngetcolvalue(testDataFile, dataSheet,
		// scriptNo, dataSetNo, "NewDeviceName");

		if (vendormodel.contains("Juniper")) {
			searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			navigatetoViewDevicepage();

			verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceconfighistory_header, "Interface Configuration History");
			String interfacecon = "Interface Configuration History";
			String Interfacecon = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interfaceconfighistory_header,"Interface Configuration History");
			Interfacecon.equalsIgnoreCase(interfacecon);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.date_column, "Interface Configuration History");
			String datecolumn = "Date";
			String Datecolumn = getTextFrom(APTIPAccessNoCPE.APTNoCPE.date_column,"Date Column");
			Datecolumn.equalsIgnoreCase(datecolumn);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.filename_column, "File Name column");
			String FileName = "File Name";
			String Filname = getTextFrom(APTIPAccessNoCPE.APTNoCPE.filename_column,"File Column");
			Filname.equalsIgnoreCase(FileName);
		} else {
			Reporter.log("verifyInterfaceConfigHistory Done");

		}
	}

	public void verifyselectservicetype(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException

	{
		String CreateOrderServiceHeader = "Create Order / Service";
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		// String
		// actualValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.service);

		// select service type
		ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.servicetypetextfield);

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		// click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.ServiceTypeWarningMessage,"Service
		// Type Warning Message");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicetypetextfield, "service type textfield");
		addDropdownValues_commonMethod("Service Type", APTIPAccessNoCPE.APTNoCPE.servicetypetextfield, servicetype);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		(APT_IPTransitObj.APT_IPTransit.createorderservice_header).equals(CreateOrderServiceHeader);
	}

	public void verifyManageSubnets() throws InterruptedException, IOException {

		String ManageSubnetMessage = getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_successmsg,
				"ManageSubnetMessage");
		String SpaceName = getTextFrom(APT_IPTransitObj.APT_IPTransit.spacename_column, "SpaceName");
		String BlockName = getTextFrom(APT_IPTransitObj.APT_IPTransit.blockname_column, "BlockName");
		String SubnetName = getTextFrom(APT_IPTransitObj.APT_IPTransit.subnetname_column, "SubnetName");
		String StartAddress = getTextFrom(APT_IPTransitObj.APT_IPTransit.startaddress_column, "StartAddress");
		String Size = getTextFrom(APT_IPTransitObj.APT_IPTransit.size_column, "Size");

		String ManageSubnetMessageValue = "There are no subnets to be managed for this service.";
		String SpaceNameValue = "Space Name";
		String BlockNameValue = "Block Name";
		String SubnetNameValue = "Subnet Name";
		String StartAddressValue = "Start Address";
		String SizeValue = "Size";

		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);

		click(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.managesubnets_link, "Manage Subnets");
		waitForAjax();
		waitForAjax();
		String ManageSubnetheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_header,
				"Manage Subnet header");
		ManageSubnetMessage.equals(ManageSubnetMessageValue);
		SpaceName.equals(SpaceNameValue);
		BlockName.equals(BlockNameValue);
		SubnetName.equals(SubnetNameValue);
		StartAddress.equals(StartAddressValue);
		Size.equals(SizeValue);

		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.closesymbol, "Close");
		waitForAjax();
	}

	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException

	{
		//String TerminationDate = getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_header, "Termination Date");
		//String EmailValue = getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_successmsg, "Email");
		String ServicepanelHeader = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_header,
				"Service panel Header");
		String ServiceIdentification = getTextFrom(
				APT_IPTransitObj.APT_IPTransit.servicepanel_serviceidentificationvalue, "Service Identification");
		String ServiceType = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_servicetypevalue, "Service Type");
		String PhoneContact = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_phone, "Phone Contact");
		String BillingType = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_billingtype, "Billing Type");
		String Remarks = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_remarksvalue, "Remarks");

		String ServicepanelHeaderValue = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_header,
				"Service panel Header");
		String ServiceIdentificationValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Service Identification");
		String ServiceTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service Type");
		String PhoneContactValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Phone Contact");
		String BillingTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Billing Type");
		String RemarksValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");

		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);

		ServicepanelHeader.equals(ServicepanelHeaderValue);
		ServiceIdentification.equals(ServiceIdentificationValue);
		ServiceType.equals(ServiceTypeValue);
		PhoneContact.equals(PhoneContactValue);
		BillingType.equals(BillingTypeValue);
		Remarks.equals(RemarksValue);

	}

	public void verifyUserDetailsInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String Logincolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.LoginColumn, "LoginColumn");
		String Namecolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.NameColumn, "NameColumn");
		String Emailcolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.EmailColumn, "EmailColumn");
		String Rolescolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.RolesColumn, "RolesColumn");
		String Addresscolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.AddressColumn, "AddressColumn");
		String Resourcecolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.ResourcesColumn, "ResourceColumn");

		String LogincolumnValue = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_header, "LoginColumn");
		String NamecolumnValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NameColumn");
		String EmailcolumnValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EmailColumn");
		String RolescolumnValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RolesColumn");
		String AddresscolumnValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"AddressColumn");
		String ResourcecolumnValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ResourceColumn");

		// verify table column names
		ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.customerdetailsheader);

		scrollDown(APT_IPTransitObj.APT_IPTransit.customerdetailsheader);

		Logincolumn.equals(LogincolumnValue);
		Namecolumn.equals(NamecolumnValue);
		Emailcolumn.equals(EmailcolumnValue);
		Rolescolumn.equals(RolescolumnValue);
		Addresscolumn.equals(AddresscolumnValue);
		Resourcecolumn.equals(ResourcecolumnValue);

	}

	public void verifyManageSubnetsIpv6() throws InterruptedException, IOException {
		String ManageSubnetheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_header,
				"Manage Subnet ipv6 header");
		String ManageSubnetMessage = getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_successmsg,
				"Manage subnet ipv6 message");
		String SpaceName = getTextFrom(APT_IPTransitObj.APT_IPTransit.spacename_column, "Space Name");
		String BlockName = getTextFrom(APT_IPTransitObj.APT_IPTransit.blockname_column, "Block Name");
		String SubnetName = getTextFrom(APT_IPTransitObj.APT_IPTransit.subnetname_column, "Subnet Name");
		String StartAddress = getTextFrom(APT_IPTransitObj.APT_IPTransit.startaddress_column, "Start Address");
		String Size = getTextFrom(APT_IPTransitObj.APT_IPTransit.size_column, "Size");

		String ManageSubnetMessageValue = "There are no subnets to be managed for this service.";
		String SpaceNameValue = "Space Name";
		String BlockNameValue = "Block Name";
		String SubnetNameValue = "Subnet Name";
		String StartAddressValue = "Start Address";
		String SizeValue = "Size";

		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);

		click(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.managesubnetsipv6_link, "Manage Subnets Ipv6");
		waitForAjax();
		waitForAjax();

		ManageSubnetMessage.equals(ManageSubnetMessageValue);
		SpaceName.equals(SpaceNameValue);
		BlockName.equals(BlockNameValue);
		SubnetName.equals(SubnetNameValue);
		StartAddress.equals(StartAddressValue);
		Size.equals(SizeValue);

		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.closesymbol, "Close");
		waitForAjax();
	}

	public void verifyShowNewInfovistaReport() throws Exception {

		// OLO.shownewInfovista();
		waitForAjax();
	}

	public void navigatetoViewDevicepage() throws InterruptedException, IOException {

		//String DeviceDetails = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewdevicepage_header, "View device header");
		//String DeviceDetailsValue = "Device Details";

		// click(APTIPAccessNoCPE.APTNoCPE.back, "back");
		// ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.viewservicepage_viewdevicelink1);
		scrollDown(APT_IPTransitObj.APT_IPTransit.viewservicepage_viewdevicelink1);
		// ((JavascriptExecutor)
		// webDriver).executeScript("window.scrollBy(0,-100)");
		if (isElementPresent(APT_IPTransitObj.APT_IPTransit.viewservicepage_viewdevicelink1)) {
			click(APT_IPTransitObj.APT_IPTransit.viewservicepage_viewdevicelink1, "View");
			waitForAjax();
			waitForAjax();

			// DeviceDetails.equals(DeviceDetailsValue);
		} else {
			Reporter.log("No Device added in grid");
		}

	}

	/*
	 * public void createcustomer(String testDataFile, String sheetName, String
	 * scriptNo, String dataSetNo) throws InterruptedException, IOException {
	 * String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
	 * dataSetNo, "newCustomer"); String maindomain =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "MainDomain"); String country = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "CountryToBeSelected"); String ocn =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "OCN"); String reference = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "Reference"); String technicalcontactname
	 * = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "TechnicalContactName"); String type =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "TypeToBeSelected"); String email = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "Email"); String phone =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "Phone"); String fax = DataMiner.fngetcolvalue(testDataFile, sheetName,
	 * scriptNo, dataSetNo, "Fax");
	 * 
	 * 
	 * //ExtentTestManager.getTest().log(LogStatus.INFO,
	 * "'Verifying Customer Creation Functionality");
	 * 
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ManageCustomerServiceLink," Manage Customer Service Link");
	 * mouseMoveOn(APT_IPTransitObj.APT_IPTransit.ManageCustomerServiceLink);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * createcustomerlink," create customer link");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * createcustomerlink,"Create Customer Link");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * createcustomer_header,"create customer page header");
	 * 
	 * //((JavascriptExecutor)webDriver).executeScript(
	 * "window.scrollTo(0,document.body.scrollHeight)");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.okbutton,"ok");
	 * click(APT_IPTransitObj.APT_IPTransit.okbutton,"ok");
	 * 
	 * //Warning msg check verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * customernamewarngmsg,"Legal Customer Name");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.countrywarngmsg,"Country");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.ocnwarngmsg,"OCN");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.typewarngmsg,"Type");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.emailwarngmsg,"Email");
	 * 
	 * //Create customer by providing all info
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.nametextfield,"name");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.nametextfield,name,"name");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * maindomaintextfield,"Main Domain");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.maindomaintextfield,
	 * maindomain,"Main Domain");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.country1,"country");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.country1,country,"country");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.ocntextfield,"ocn");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.ocntextfield,ocn,"ocn");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.referencetextfield,
	 * "reference");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.referencetextfield,reference,
	 * "reference");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * technicalcontactnametextfield,"technical contact name");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.technicalcontactnametextfield,
	 * technicalcontactname,"technical contact name");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * typedropdown,"technical contact name");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.typedropdown,
	 * type,"technical contact name");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.emailtextfield1,"email");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.emailtextfield1,email,"email");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * phonetextfield,"phone text field");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.phonetextfield,
	 * phone,"phone text field");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.faxtextfield,"fax text field"
	 * );
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.faxtextfield,fax,"fax text field"
	 * );
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.okbutton,"ok");
	 * click(APT_IPTransitObj.APT_IPTransit.okbutton,"ok");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * customercreationsuccessmsg,"Customer successfully created.");
	 * 
	 * }
	 * 
	 * public void selectCustomertocreateOrder(String testDataFile, String
	 * sheetName, String scriptNo, String dataSetNo) throws
	 * InterruptedException, IOException { String name =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "newCustomer");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ManageCustomerServiceLink," Manage Customer Service Link");
	 * mouseMoveOn(APT_IPTransitObj.APT_IPTransit.ManageCustomerServiceLink);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * CreateOrderServiceLink,"Create Order Service Link");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * CreateOrderServiceLink,"Create Order Service Link");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * click(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * customer_createorderpage_warngmsg,"Next button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.entercustomernamefield,"name"
	 * ); sendKeys(APT_IPTransitObj.APT_IPTransit.entercustomernamefield,name,
	 * "name");
	 * 
	 * waitForAjax();
	 * 
	 * select(APT_IPTransitObj.APT_IPTransit.chooseCustomerdropdown,name);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * click(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * 
	 * } public void createorderservice(String testDataFile, String sheetName,
	 * String scriptNo, String dataSetNo) throws InterruptedException,
	 * IOException { String neworder = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "NewOrderService"); String newordernumber
	 * = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "NewOrderNumber"); String newrfireqno =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "NewRFIREQNumber"); String existingorderservice =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ExistingOrderService"); String existingordernumber =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ExistingOrderNumber");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * click(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ordercontractnumber,"order contract number");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * servicetype_warngmsg,"service type");
	 * 
	 * if (neworder.equalsIgnoreCase("YES")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * newordertextfield,"new order textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.newordertextfield,newordernumber,
	 * "newordertextfield");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * newrfireqtextfield,"new rfireq textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.newrfireqtextfield,
	 * newrfireqno,"new rfireq textfield");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * createorderbutton,"create order button");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * createorderbutton,"create order button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * OrderCreatedSuccessMsg,"Order Created Success Msg");
	 * 
	 * } else if (existingorderservice.equalsIgnoreCase("YES")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * selectorderswitch,"select order switch");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * selectorderswitch,"select order switch");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * existingorderdropdown,"existing order drop down");
	 * select(APT_IPTransitObj.APT_IPTransit.existingorderdropdown,
	 * existingordernumber); } else { Reporter.log("Order not selected"); }
	 * 
	 * }
	 * 
	 * public void verifyServiceCreation(String testDataFile, String sheetName,
	 * String scriptNo, String dataSetNo) throws InterruptedException,
	 * IOException
	 * 
	 * { String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "ServiceIdentification"); String
	 * BillingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
	 * dataSetNo, "BillingType"); String TerminationDate =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "TerminationDate"); String EmailService =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "EmailService"); String PhoneService =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "PhoneService"); String Remarks = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "Remarks"); String ManageService =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ManageService"); String RouterConfigurationViewIPv4 =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "RouterConfigurationViewIPv4"); String RouterConfigurationViewIPv6 =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "RouterConfigurationViewIPv6"); String PerformanceReporting =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "PerformanceReporting"); String IPGuardian =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "IPGuardian"); String SNMPNotification =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "SNMPNotification"); String DeliveryChannel =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "DeliveryChannel"); String RouterBasedFirewall =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "RouterBasedFirewall"); String TrapTargetAddress =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "TrapTargetAddress"); String Qos = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "Qos"); String ExtendPErangetocustomerLAN
	 * = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ExtendPErangetocustomerLAN");
	 * 
	 * if(isClickable(APT_IPTransitObj.APT_IPTransit.createorderservice_header))
	 * { verifyExists(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * click(APT_IPTransitObj.APT_IPTransit.nextbutton,"Next button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * sidwarngmsg,"Service Identification");
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * billingtype_warngmsg,"Billing Type");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * serviceidentificationtextfield,"new order textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.serviceidentificationtextfield,
	 * ServiceIdentification,"newordertextfield");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * billingtype_dropdown,"Billing Type");
	 * select(APT_IPTransitObj.APT_IPTransit.billingtype_dropdown,BillingType);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * terminationdate_field,"new order textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.terminationdate_field,
	 * TerminationDate,"newordertextfield");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * emailtextfield_Service,"new order textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.emailtextfield_Service,
	 * EmailService,"newordertextfield");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * phonecontacttextfield,"new order textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.phonecontacttextfield,
	 * PhoneService,"newordertextfield");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * remarktextarea,"new order textfield");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.remarktextarea,Remarks,
	 * "newordertextfield");
	 * 
	 * if(ManageService.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ManageServicecheckbox,"Manage Service checkbox");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * ManageServicecheckbox,"Manage Service checkbox"); } else{
	 * Reporter.log("Not Manage Service checkbox"); }
	 * if(RouterConfigurationViewIPv4.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * RouterConfigurationViewIPv4checkbox,"Router Configuration View IPv4 checkbox"
	 * ); click(APT_IPTransitObj.APT_IPTransit.
	 * RouterConfigurationViewIPv4checkbox,"Router Configuration View IPv4"); }
	 * else{ Reporter.log("Not Router Configuration View IPv4 checkbox "); }
	 * 
	 * if(RouterConfigurationViewIPv6.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * RouterConfigurationViewIPv6checkbox,"Router Configuration View IPv6 checkbox"
	 * ); click(APT_IPTransitObj.APT_IPTransit.
	 * RouterConfigurationViewIPv6checkbox,"Router Configuration View IPv6"); }
	 * else{ Reporter.log("Not Router Configuration View IPv6 checkbox "); }
	 * if(PerformanceReporting.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * performancereportingcheckbox,"performance reporting checkbox");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * performancereportingcheckbox,"performance reporting checkbox"); } else{
	 * Reporter.log("Not performance reporting checkbox"); }
	 * 
	 * if(IPGuardian.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * IPGuardiancheckbox,"IP Guardian checkbox");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * IPGuardiancheckbox,"IP Guardian checkbox"); } else{
	 * Reporter.log("Not IP Guardian checkbox"); }
	 * if(SNMPNotification.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * SNMPNotificationcheckbox,"SNMP Notification checkbox");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * SNMPNotificationcheckbox,"SNMP Notification checkbox");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.SNMPNotificationcheckbox,
	 * TrapTargetAddress); } else{
	 * Reporter.log("Not SNMP Notification checkbox"); }
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * DeliveryChannelDropdown,"Delivery Channel");
	 * click(APT_IPTransitObj.APT_IPTransit.DeliveryChannelDropdown,
	 * DeliveryChannel);
	 * 
	 * if(RouterBasedFirewall.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * RouterBasedFirewallcheckbox,"Router Based Firewall checkbox");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * RouterBasedFirewallcheckbox,"Router Based Firewall checkbox"); } else{
	 * Reporter.log("Not Router Based Firewall checkbox"); }
	 * if(Qos.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Qoscheckbox,"Qos checkbox");
	 * click(APT_IPTransitObj.APT_IPTransit.Qoscheckbox,"Qos checkbox"); } else{
	 * Reporter.log("Not Qos checkbox"); }
	 * if(ExtendPErangetocustomerLAN.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ExtendPErangetocustomerLANcheckbox,"Extend PE rangeto customer LAN checkbox"
	 * ); click(APT_IPTransitObj.APT_IPTransit.
	 * ExtendPErangetocustomerLANcheckbox,"Extend PE rangeto customer LAN checkbox"
	 * ); } else{ Reporter.log("Not Extend PE rangeto customer LAN checkbox"); }
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * Nextbutton_ServiceCreation,"Nextbutton Service Creation");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * Nextbutton_ServiceCreation,"Nextbutton Service Creation");
	 * 
	 * String serviceCreated = "Service successfully created"; String
	 * orderService=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * servicecreationmessage); serviceCreated.equalsIgnoreCase(orderService);
	 * Reporter.log("Service successfully created");
	 * 
	 * } else { Reporter.log("Servce Creation' page not navigated");
	 * 
	 * }
	 * 
	 * public void verifyCustomerDetailsInformation(String testDataFile, String
	 * sheetName, String scriptNo, String dataSetNo) { String
	 * newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, sheetName,
	 * scriptNo, dataSetNo, "newCustomerCreation"); String
	 * existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "existingCustomerSelection"); String
	 * newCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
	 * dataSetNo, "newCustomer"); String country =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "CountryToBeSelected"); String ocn =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "OCN"); String reference = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "Reference"); String technicalcontactname
	 * = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "TechnicalContactName"); String type =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "TypeToBeSelected"); String email = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "Email"); String phone =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "Phone"); String fax = DataMiner.fngetcolvalue(testDataFile, sheetName,
	 * scriptNo, dataSetNo, "Fax"); String existingCustomer =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "existingCustomer"); String maindomain =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "MainDomain");
	 * 
	 * if(newCustomerCreation.equalsIgnoreCase("Yes") ||
	 * existingCustomerSelection.equalsIgnoreCase("No")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Name_Value,"Name Value");
	 * String nameValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.Name_Value);
	 * nameValue.equalsIgnoreCase(newCustomer);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Country_Value,"Country");
	 * String
	 * countryValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.Country_Value);
	 * countryValue.equalsIgnoreCase(country);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.OCN_Value,"OCN"); String
	 * OCN=getTextFrom(APT_IPTransitObj.APT_IPTransit.OCN_Value);
	 * OCN.equalsIgnoreCase(ocn);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Reference_Value,"Reference");
	 * String
	 * Reference=getTextFrom(APT_IPTransitObj.APT_IPTransit.Reference_Value);
	 * Reference.equalsIgnoreCase(reference);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * TechnicalContactName_Value,"Technical Contact Name"); String
	 * technicalContactName=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * TechnicalContactName_Value);
	 * technicalContactName.equalsIgnoreCase(technicalcontactname);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Type_Value,"Type"); String
	 * Type=getTextFrom(APT_IPTransitObj.APT_IPTransit.Type_Value);
	 * Type.equalsIgnoreCase(type);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Email_Value,"Email"); String
	 * Email=getTextFrom(APT_IPTransitObj.APT_IPTransit.Email_Value);
	 * Email.equalsIgnoreCase(email);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Phone_Value,"Phone"); String
	 * Phone=getTextFrom(APT_IPTransitObj.APT_IPTransit.Phone_Value);
	 * Phone.equalsIgnoreCase(phone);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Fax_Value,"Fax"); String
	 * Fax=getTextFrom(APT_IPTransitObj.APT_IPTransit.Fax_Value);
	 * Fax.equalsIgnoreCase(fax); } else
	 * if(newCustomerCreation.equalsIgnoreCase("No") ||
	 * existingCustomerSelection.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.Name_Value,"Fax"); String
	 * ExistingCustomer=getTextFrom(APT_IPTransitObj.APT_IPTransit.Name_Value);
	 * ExistingCustomer.equalsIgnoreCase(existingCustomer); } //Main Domain
	 * if(maindomain.equalsIgnoreCase("Null")) { Reporter.
	 * log("A default displays for main domain field, if no provided while creating customer"
	 * );
	 * 
	 * } else { verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * MainDomain_Value,"Main Domain"); String
	 * Maindomain=getTextFrom(APT_IPTransitObj.APT_IPTransit.MainDomain_Value);
	 * Maindomain.equalsIgnoreCase(maindomain); }
	 * Reporter.log("Customer Details panel fields Verified"); } }
	 * 
	 * public void verifyorderpanel_editorder(String testDataFile, String
	 * sheetName, String scriptNo, String dataSetNo) throws
	 * InterruptedException, IOException
	 * 
	 * { String editOrderSelection = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "editOrderSelection"); String editorderno
	 * = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "EditOrder_OrderNumber"); String editvoicelineno =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "EditOrder_VoicelineNumber");
	 * 
	 * if(editOrderSelection.equalsIgnoreCase("no")) {
	 * Reporter.log("Edit Order is not performed"); } else
	 * if(editOrderSelection.equalsIgnoreCase("Yes")) {
	 * Reporter.log("Performing Edit Order Functionality");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * orderactionbutton,"Action dropdown");
	 * click(APT_IPTransitObj.APT_IPTransit.orderactionbutton,"Action dropdown"
	 * );
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.editorderlink,"Edit Order");
	 * click(APT_IPTransitObj.APT_IPTransit.editorderlink,"Edit Order");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.editorderheader,"Edit Order"
	 * );
	 * 
	 * String
	 * EditOrderNo=getTextFrom(APT_IPTransitObj.APT_IPTransit.editorderno);
	 * click(EditOrderNo,"edit order no");
	 * clearTextBox(APT_IPTransitObj.APT_IPTransit.editorderno);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.editorderno,"Order Number");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.editorderno,
	 * editorderno,"Order Number");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * editvoicelineno,"edit voice line no");
	 * clearTextBox(APT_IPTransitObj.APT_IPTransit.editvoicelineno);
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.editvoicelineno,
	 * editvoicelineno,"Order Number");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.cancelbutton1,"cancel button"
	 * ); click(APT_IPTransitObj.APT_IPTransit.cancelbutton1,"cancel button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * orderactionbutton,"Action dropdown");
	 * click(APT_IPTransitObj.APT_IPTransit.orderactionbutton,"Action dropdown"
	 * );
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.editorderlink,"Edit Order");
	 * click(APT_IPTransitObj.APT_IPTransit.editorderlink,"Edit Order");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * editorderheader,"Edit Order Header"); String
	 * editOrderHeader=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * editorderheader); String EditOrder= "Edit Order";
	 * editOrderHeader.equalsIgnoreCase(EditOrder);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.editorderno,"edit order no");
	 * click(APT_IPTransitObj.APT_IPTransit.editorderno,"edit order no");
	 * clearTextBox(APT_IPTransitObj.APT_IPTransit.editorderno);
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.editorderno,
	 * editorderno,"Order Number");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * editvoicelineno,"edit voice line no");
	 * clearTextBox(APT_IPTransitObj.APT_IPTransit.editvoicelineno);
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.editvoicelineno,
	 * editvoicelineno,"Order Number");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * editorder_okbutton,"OK Button");
	 * click(APT_IPTransitObj.APT_IPTransit.editorder_okbutton,"OK Button");
	 * 
	 * if(editorderno.equalsIgnoreCase("Null")) {
	 * Reporter.log("Order/Contract Number (Parent SID) field is not edited"); }
	 * else { verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ordernumbervalue,"order number value"); String
	 * editOrderno=getTextFrom(APT_IPTransitObj.APT_IPTransit.ordernumbervalue);
	 * editOrderno.equalsIgnoreCase(editorderno); }
	 * 
	 * if(editvoicelineno.equalsIgnoreCase("Null")) {
	 * Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited"); } else
	 * { verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * ordervoicelinenumbervalue,"order voice line number value"); String
	 * editVoicelineno=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * ordervoicelinenumbervalue);
	 * editVoicelineno.equalsIgnoreCase(editvoicelineno); }
	 * 
	 * Reporter.log("Edit Order is successful"); } }
	 * 
	 * public void verifyorderpanel_changeorder(String testDataFile, String
	 * sheetName, String scriptNo, String dataSetNo) throws
	 * InterruptedException, IOException
	 * 
	 * {
	 * 
	 * String changeOrderSelection_newOrder =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "changeOrderSelection_newOrder"); String
	 * changeOrderSelection_existingOrder =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "changeOrderSelection_existingOrder"); String ChangeOrder_newOrderNumber
	 * = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ChangeOrder_newOrderNumber"); String changevoicelineno =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ChangeOrder_VoicelineNumber"); String RouterConfigurationViewIPv4Edit =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "RouterConfigurationViewIPv4Edit");
	 * 
	 * if((changeOrderSelection_newOrder.equalsIgnoreCase("No")) &&
	 * (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
	 * Reporter.log("Change Order is not performed");
	 * 
	 * } else if(changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * orderactionbutton,"Action dropdown");
	 * click(APT_IPTransitObj.APT_IPTransit.orderactionbutton,"Action dropdown"
	 * );
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorderlink,"change order link");
	 * click(APT_IPTransitObj.APT_IPTransit.changeorderlink,"change order link"
	 * );
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorderheader,"change order header"); String
	 * changeOrderHeader=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * changeorderheader); String changeOrder = "Change Order";
	 * changeOrderHeader.equalsIgnoreCase(changeOrder);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorder_selectorderswitch,"select order switch");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * changeorder_selectorderswitch,"select order switch");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeordernumber,"change order number");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * changeordernumber,"change order number");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.editorderno,
	 * ChangeOrder_newOrderNumber);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeordervoicelinenumber,"change order voiceline number");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * changeordervoicelinenumber,"change order voiceline number");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.changeordervoicelinenumber,
	 * changevoicelineno);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * createorder_button,"create order button");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * createorder_button,"create order button");
	 * 
	 * String orderNumberValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * ordernumbervalue);
	 * orderNumberValue.equalsIgnoreCase(ChangeOrder_newOrderNumber);
	 * 
	 * String
	 * orderVoicelineNumberValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * ordervoicelinenumbervalue);
	 * orderVoicelineNumberValue.equalsIgnoreCase(changevoicelineno);
	 * 
	 * Reporter.log("Change Order is successful"); } else
	 * if(changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
	 * Reporter.log("Performing Change Order functionality");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * orderactionbutton,"order action button");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * orderactionbutton,"order action button");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorderlink,"change order link");
	 * click(APT_IPTransitObj.APT_IPTransit.changeorderlink,"change order link"
	 * );
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorderheader,"change order");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorder_chooseorderdropdown,"Order/Contract Number (Parent SID)");
	 * select(APT_IPTransitObj.APT_IPTransit.changeorder_chooseorderdropdown,
	 * ChangeOrder_existingOrderNumber1);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changeorder_okbutton,"change order ok button");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * changeorder_okbutton,"change order ok button");
	 * 
	 * String orderNumberValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * ordernumbervalue);
	 * orderNumberValue.equalsIgnoreCase(ChangeOrder_existingOrderNumber);
	 * 
	 * String
	 * orderVoicelineNumberValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * ordervoicelinenumbervalue);
	 * orderVoicelineNumberValue.equalsIgnoreCase(changevoicelineno);
	 * 
	 * Reporter.log("Change Order is successful");
	 * 
	 * }
	 * 
	 * }
	 * 
	 * public void verifyservicepanelInformationinviewservicepage() throws
	 * InterruptedException, IOException { String service= "Service";
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * servicepanel_header,"service panel"); String
	 * servicePanelHeader=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * servicepanel_header); servicePanelHeader.equalsIgnoreCase(service);
	 * 
	 * }
	 */
	public void searchorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");

		verifyExists(APT_IPTransitObj.APT_IPTransit.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(APT_IPTransitObj.APT_IPTransit.ManageCustomerServiceLink);

		verifyExists(APT_IPTransitObj.APT_IPTransit.searchorderlink, "search order link");
		click(APT_IPTransitObj.APT_IPTransit.searchorderlink, "search order link");

		verifyExists(APT_IPTransitObj.APT_IPTransit.servicefield, "service field");
		sendKeys(APT_IPTransitObj.APT_IPTransit.servicefield, sid);

		String searchbutton = getTextFrom(APT_IPTransitObj.APT_IPTransit.searchbutton,"Search button");
		click(APT_IPTransitObj.APT_IPTransit.searchbutton, "searchbutton");

		verifyExists(APT_IPTransitObj.APT_IPTransit.serviceradiobutton, "service radio button");
		click(APT_IPTransitObj.APT_IPTransit.serviceradiobutton, "service radio button");

		verifyExists(APT_IPTransitObj.APT_IPTransit.searchorder_actiondropdown, "search order actiodropdown");
		click(APT_IPTransitObj.APT_IPTransit.searchorder_actiondropdown, "search order linksearch order actiodropdown");

		verifyExists(APT_IPTransitObj.APT_IPTransit.view, "view");
		click(APT_IPTransitObj.APT_IPTransit.view, "view");

		// verifyExists(APT_IPTransitObj.APT_IPTransit.closeAlert,"Close
		// Alert");
		// click(APT_IPTransitObj.APT_IPTransit.closeAlert,"Close Alert");

	}

	public void shownewInfovista() throws IOException {

		String orderPanel = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewServicepage_OrderPanel,"view Service page_Order Panel");
		scrollDown(orderPanel);
		waitForAjax();

		click(APT_IPTransitObj.APT_IPTransit.Editservice_actiondropdown, "Action");
		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.Editservice_infovistareport, "Show infovista link");
		waitForAjax();

		String expectedPageName = "SSO login Page";

		// Switch to new tab
		List<String> browserTabs = new ArrayList<String>(webDriver.getWindowHandles());
		webDriver.switchTo().window(browserTabs.get(1));
		waitForAjax();

		try {
			// Get Tab name
			String pageTitle = webDriver.switchTo().window(browserTabs.get(1)).getTitle();
			Reporter.log("page title displays as: " + pageTitle);

			waitForAjax();
			webDriver.switchTo().window(browserTabs.get(0));

			sa.assertEquals(pageTitle, expectedPageName,
					" on clicking 'Show Infovista link', it got naviagted to " + pageTitle);

			sa.assertAll();

			waitForAjax();

		} catch (AssertionError e) {

			e.printStackTrace();

			waitForAjax();
			webDriver.switchTo().window(browserTabs.get(0));

		}

	}
	/*
	 * public void verifyManageService(String testDataFile, String sheetName,
	 * String scriptNo, String dataSetNo) throws InterruptedException,
	 * IOException { String Sid = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "ServiceIdentification"); String
	 * serviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
	 * dataSetNo, "ServiceType"); String servicestatus =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ServiceStatus"); String servicestatuschangerequired =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ServiceStatusChangeRequired");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * serviceactiondropdown,"service action dropdown");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * serviceactiondropdown,"service action dropdown");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.manageLink,"manage");
	 * click(APT_IPTransitObj.APT_IPTransit.manageLink,"manage");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * manageservice_header,"manage service");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * status_servicename,"space name"); String
	 * sid=getTextFrom(APT_IPTransitObj.APT_IPTransit.status_servicename);
	 * sid.equalsIgnoreCase(Sid);
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * status_servicetype,"space name"); String
	 * servicetype=getTextFrom(APT_IPTransitObj.APT_IPTransit.status_servicetype
	 * ); servicetype.equalsIgnoreCase(serviceType);
	 * 
	 * String ServiceDetails_value=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * status_servicedetails);
	 * 
	 * if(isEmptyOrNull(ServiceDetails_value)) {
	 * Reporter.log("Service Details column value is empty as expected");
	 * 
	 * } else { Reporter.log("Service Details column value should be empty"); }
	 * 
	 * String serviceStatus=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * status_currentstatus); serviceStatus.equalsIgnoreCase(servicestatus);
	 * 
	 * String
	 * LastModificationTime_value=getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * status_modificationtime);
	 * 
	 * if(LastModificationTime_value.contains("BST")) {
	 * Reporter.log("Service status is displayed as");
	 * 
	 * }
	 * 
	 * else { Reporter.log("Incorrect modification time format");
	 * 
	 * } verifyExists(APT_IPTransitObj.APT_IPTransit.statuslink,"Status");
	 * click(APT_IPTransitObj.APT_IPTransit.statuslink,"Status");
	 * 
	 * if(servicestatuschangerequired=="Yes") {
	 * 
	 * if(isClickable(APT_IPTransitObj.APT_IPTransit.Servicestatus_popup)) {
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changestatus_dropdown,"change status");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * changestatus_dropdown,"change status");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.
	 * changestatus_dropdownvalue,"change status");
	 * click(APT_IPTransitObj.APT_IPTransit.
	 * changestatus_dropdownvalue,"change status");
	 * 
	 * verifyExists(APT_IPTransitObj.APT_IPTransit.okbutton,"ok");
	 * click(APT_IPTransitObj.APT_IPTransit.okbutton,"ok");
	 * 
	 * if(isClickable(APT_IPTransitObj.APT_IPTransit.servicestatushistory)) {
	 * Reporter.log("Service status change request logged"); } else {
	 * Reporter.log(" Service status change request is not logged"); } }
	 * 
	 * } }
	 */

	public void verifyDump() throws IOException {

		//String Serviceheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.service_header, "Service header");

		//String ServiceheaderValue = "Service";

		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);
		click(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.dump_link, "Dump");
		waitForAjax();
		waitToPageLoad();
		//String DumpHeader = getTextFrom(APT_IPTransitObj.APT_IPTransit.dumppage_header, "Dump header");
		//String ServiceRetrievedTime = getTextFrom(APT_IPTransitObj.APT_IPTransit.serviceretrieved_text,
				//"Service retrieved time");

		// Serviceheader.equals(ServiceheaderValue);
		getTextFrom(APT_IPTransitObj.APT_IPTransit.dumppage_text, "Dump page service details");
		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.closesymbol, "Close");
		waitForAjax();
	}

	public String DeviceName() throws IOException {

		scrollIntoTop();
		String DeviceName = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_devicename, "Name");

		return DeviceName;
	}

	public String VendorModel() throws IOException {

		scrollIntoTop();
		String VendorModel = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_vendormodel, "Vendor/Model");

		return VendorModel;
	}

	public void navigateToAddNewDevicepage() throws IOException {
		scrollDown(APT_IPTransitObj.APT_IPTransit.providerequipment_header);

		waitForAjax();

		String PE = getTextFrom(APT_IPTransitObj.APT_IPTransit.providerequipment_header, "PE header");
		String PEValue = "Provider Equipment (PE)";
		PE.equals(PEValue);
		click(APT_IPTransitObj.APT_IPTransit.addpedevice_link, "Add PE Device");
		waitForAjax();
		waitToPageLoad();
		String AddPEDevice = getTextFrom(APT_IPTransitObj.APT_IPTransit.addpedevice_header, "Add PE Device Header");
		String AddPEDeviceValue = "Add PE Device";
		AddPEDevice.equals(AddPEDeviceValue);
		click(APT_IPTransitObj.APT_IPTransit.addnewdevice_togglebutton, "Add New Device toggle");
		waitForAjax();
	}

	public void verifyadddevicefields() throws InterruptedException, IOException {

		verifyExists(APT_IPTransitObj.APT_IPTransit.nametextfield, "Name");
		verifyExists(APT_IPTransitObj.APT_IPTransit.vendormodelinput, "Vendor Model");
		verifyExists(APT_IPTransitObj.APT_IPTransit.managementaddresstextbox, "Management Address");
		verifyExists(APT_IPTransitObj.APT_IPTransit.snmpv3username, "Snmp v3 Username");
		verifyExists(APT_IPTransitObj.APT_IPTransit.snmpv3authpassword, "Snmp v3 Auth password");
		verifyExists(APT_IPTransitObj.APT_IPTransit.snmpv3privpassword, "Snmp v3 Priv password");
		verifyExists(APT_IPTransitObj.APT_IPTransit.countryinput, "Country");
		scrollDown(APT_IPTransitObj.APT_IPTransit.citydropdowninput);
		verifyExists(APT_IPTransitObj.APT_IPTransit.citydropdowninput, "City");
		verifyExists(APT_IPTransitObj.APT_IPTransit.sitedropdowninput, "Site");
		verifyExists(APT_IPTransitObj.APT_IPTransit.premisedropdowninput, "Premise");
		verifyExists(APT_IPTransitObj.APT_IPTransit.addcityswitch, "Add City");
		verifyExists(APT_IPTransitObj.APT_IPTransit.addsiteswitch, "Add Site");
		verifyExists(APT_IPTransitObj.APT_IPTransit.addpremiseswitch, "Add Premise");
		verifyExists(APT_IPTransitObj.APT_IPTransit.cancelbutton1, "Cancel");
		verifyExists(APT_IPTransitObj.APT_IPTransit.Next_Button, "Next");
		scrollIntoTop();

	}

	/*
	 * public void SelectInterfacetoremovefromservice(String testDataFile,
	 * String sheetName, String scriptNo, String dataSetNo) throws IOException,
	 * InterruptedException { String InterfaceName =
	 * getAttributeFrom(APT_IPTransitObj.APT_IPTransit.interfacename_textfield,
	 * "value");
	 * 
	 * // String InterfaceName = DataMiner.fngetcolvalue(testDataFile, //
	 * sheetName, scriptNo, dataSetNo, "Cisco_InterfaceName");
	 * 
	 * ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.selectinterfaces);
	 * click(APT_IPTransitObj.APT_IPTransit.selectinterfaces); //
	 * scrollDown(APT_IPTransitObj.APT_IPTransit.viewpage_premise); //
	 * waitForAjax();
	 * click(APT_IPTransitObj.APT_IPTransit.interfacesinservice_filter,
	 * "Interfaces in Service Filter");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.interfacesinservice_filter,
	 * InterfaceName, "Interface in Service search"); // String
	 * InterfaceName_GridSelect = //
	 * (APT_IPTransitObj.APT_IPTransit.interfaceinservice_gridselect).replace(
	 * "value", // InterfaceName); // click(InterfaceName_GridSelect,
	 * "Interface name gris select"); // waitForAjax(); //
	 * scrollDown(APT_IPTransitObj.APT_IPTransit.viewpage_premise); //
	 * waitForAjax();
	 * ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.selectinterfaces);
	 * 
	 * click(APT_IPTransitObj.APT_IPTransit.InterfaceInselect_Actiondropdown,
	 * "Action Dropdown"); Thread.sleep(1000);
	 * click(APT_IPTransitObj.APT_IPTransit.InterfaceInselect_removebuton,
	 * "Remove"); waitForAjax(); waitToPageLoad(); }
	 */

	/*
	 * public void SelectInterfacetoaddwithservcie(String testDataFile, String
	 * sheetName, String scriptNo, String dataSetNo) throws IOException,
	 * InterruptedException { String InterfaceName =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "Interface Name");
	 * 
	 * ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.selectinterfaces);
	 * click(APT_IPTransitObj.APT_IPTransit.selectinterfaces); /*
	 * ((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get())
	 * .executeScript("0,window.scrollTo(document.body.scrollHeight)");
	 * 
	 * waitForAjax();
	 * 
	 * scrollDown(APT_IPTransitObj.APT_IPTransit.
	 * InterfaceInService_panelHeader); waitForAjax();
	 * 
	 * click(APT_IPTransitObj.APT_IPTransit.InteraceColumn_Filter,
	 * "Interfaces To Select Filter");
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.interfacesinservice_filter,
	 * InterfaceName, "Interface in Service search"); String
	 * InterfaceName_GridSelect =
	 * (APT_IPTransitObj.APT_IPTransit.interfaceinservice_gridselect)
	 * .replace("value", InterfaceName); click(InterfaceName_GridSelect,
	 * "Interface name gris select"); waitForAjax();
	 * 
	 * ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.
	 * InterfaceInService_panelHeader); waitForAjax();
	 * click(APT_IPTransitObj.APT_IPTransit.InterfaceToselect_Actiondropdown,
	 * "Action Dropdown"); waitForAjax();
	 * click(APT_IPTransitObj.APT_IPTransit.InterfaceToselect_addbuton, "Add");
	 * waitForAjax(); ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.back);
	 * 
	 * click(APTIPAccessNoCPE.APTNoCPE.back); // clickOnBreadCrumb(testDataFile,
	 * sheetName, scriptNo, dataSetNo); waitForAjax(); }
	 */

	public void clickOnBreadCrumb(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");

		scrollIntoTop();
		waitForAjax();
		String breadcrumb = null;

		try {
			breadcrumb = (APT_IPTransitObj.APT_IPTransit.breadcrumb).replace("value", sid);
			if (isElementPresent(breadcrumb)) {
				click(breadcrumb,"Breadcrumb");
			}

			else {
				Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	}

	public void hostnametextField_IPV4(String ipAddress, String command_ipv4) throws IOException, InterruptedException {

		boolean IPV4availability = false;
		try {
			IPV4availability = isElementPresent(APT_IPTransitObj.APT_IPTransit.commandIPv4_hostnameTextfield);

			if (IPV4availability) {

				sendKeys(APT_IPTransitObj.APT_IPTransit.commandIPv4_hostnameTextfield, ipAddress,
						"IP Address or Hostname");

			} else {
				System.out
						.println("'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'Hostname or IpAddress' for 'Ipv4' text field is not displaying for " + command_ipv4);
		}
	}

	public void executeCommandAndFetchTheValue() throws IOException {

		click(APT_IPTransitObj.APT_IPTransit.executebutton_Ipv4, "Execute");

		boolean remarkField = false;
		try {
			remarkField = isElementPresent(APT_IPTransitObj.APT_IPTransit.result_textArea);
			if (remarkField) {
				Reporter.log("'Remark' text field is displaying");

				String remarkvalue = getTextFrom(APT_IPTransitObj.APT_IPTransit.result_textArea,"result_text Area");
				Reporter.log("value under 'Remark' field displaying as " + remarkvalue);

			} else {
				Reporter.log("'Remark' text field is not displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'Remark' text field is not displaying");
		}
	}

	public void verify_Juniper_RouterTools(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv4");
		String ipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv6");
		String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");

		scrollDown(APT_IPTransitObj.APT_IPTransit.routertools_header);
		waitForAjax();

		// Command IPV4
		addDropdownValues_commonMethod("Command IPV4", APT_IPTransitObj.APT_IPTransit.commandIPV4_dropdown,
				commandIPv4);
		// sendKeys(APT_IPTransitObj.APT_IPTransit.commandIPV4_dropdown,
		// commandIPv4, "Command IPV4");

		hostnametextField_IPV4(ipAddress, commandIPv4);

		vrfNametextField_IPV4(commandIPv4, vrfname_ipv4);

		executeCommandAndFetchTheValue();

	}

	public void vrfNametextField_IPV4(String command_ipv4, String vrfname_ipv4) {
		boolean IPV4availability = false;

		if (command_ipv4.contains("vrf")) {
			try {
				IPV4availability = isElementPresent(APT_IPTransitObj.APT_IPTransit.commandIPv4_vrfnameTextField);

				if (IPV4availability) {
					sendKeys(APT_IPTransitObj.APT_IPTransit.commandIPv4_vrfnameTextField, vrfname_ipv4,
							"Router Vrf Name");
				} else {
					System.out.println("'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4);
			}

		} else {
			System.out.println("'VRF Name IPv4' text field is not displaying for " + command_ipv4 + " command");
		}

	}

	public void hostnametextField_IPV6(String commandIPv6, String ipv6Address) {
		boolean IPV6availability = false;
		try {
			IPV6availability = isElementPresent(APT_IPTransitObj.APT_IPTransit.commandIPv6_hostnameTextfield);

			if (IPV6availability) {

				sendKeys(APT_IPTransitObj.APT_IPTransit.commandIPv6_hostnameTextfield, ipv6Address,
						"IP Address or Hostname");

			} else {
				System.out
						.println("'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6);
			}
		} catch (Exception e) {
			e.printStackTrace();

			System.out.println("'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6);
		}
	}
	/*
	 * public void verify_Cisco_RouterTools(String testDataFile, String
	 * sheetName, String scriptNo, String dataSetNo, String
	 * managementAddressEdit) throws IOException, InterruptedException { String
	 * commandIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
	 * dataSetNo, "commandIPv4"); String ipAddress =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ipAddress"); String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "vrfname_ipv4"); String
	 * executebutton_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName,
	 * scriptNo, dataSetNo, "executebutton_Ipv4"); String commandIPv6 =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "commandIPv6"); String ipv6Address =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "ipv6Address"); String vrfname_ipv6 =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "commandIPv6"); String executebutton_IPv6 =
	 * DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
	 * "executebutton_IPv6");
	 * 
	 * scrollDown(APT_IPTransitObj.APT_IPTransit.routertools_header);
	 * Thread.sleep(1000);
	 * 
	 * // Command IPV4 // addDropdownValues_commonMethod(application,
	 * "Command IPV4", // "commandIPV4_dropdown" , commandIPv4, xml); // Command
	 * IPV4 // addDropdownValues_commonMethod(application, "Command IPV4", //
	 * "commandIPV4_dropdown" , commandIPv4, xml);
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.commandIPV4_dropdown,
	 * commandIPv4, "Command IPV4");
	 * 
	 * hostnametextField_IPV4(ipAddress, commandIPv4);
	 * 
	 * //vrfNametextField_IPV4(commandIPv4, vrfname_ipv4);
	 * 
	 * executeCommandAndFetchTheValue(executebutton_Ipv4);
	 * 
	 * // Commmand IPV6
	 * sendKeys(APT_IPTransitObj.APT_IPTransit.commandIPV6_dropdown,
	 * commandIPv6, "Command IPV6");
	 * 
	 * hostnametextField_IPV6(commandIPv6, ipv6Address);
	 * 
	 * // vrfNametextField_IPV6(commandIPv6, vrfname_ipv6);
	 * executeCommandAndFetchTheValue(executebutton_IPv6);
	 * 
	 * }
	 */

	/*
	 * public void selectInterfacelinkforDevice(String devicename) throws
	 * InterruptedException, IOException {
	 * 
	 * ((JavascriptExecutor)
	 * webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)")
	 * ;
	 * 
	 * waitForAjax(); // check 'Select Interface' link
	 * 
	 * if (isElementPresent(APT_IPTransitObj.APT_IPTransit.existingdevicegrid))
	 * { List<String> addeddevicesList = new ArrayList<>(
	 * Arrays.asList(getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * addeddevices_list))); // System.out.println(addeddevicesList); int
	 * AddedDevicesCount = addeddevicesList.size(); for (int i = 0; i <
	 * AddedDevicesCount; i++) { String AddedDeviceNameText =
	 * getTextFrom(addeddevicesList.get(i)); String AddedDevice_SNo =
	 * AddedDeviceNameText.substring(0, 1); if
	 * (AddedDeviceNameText.contains(devicename)) { String
	 * AddedDevice_SelectInterfacesLink = getTextFrom(
	 * APT_IPTransitObj.APT_IPTransit.addeddevice_selectinterfaceslink).replace(
	 * "value", AddedDevice_SNo); click(AddedDevice_SelectInterfacesLink);
	 * waitForAjax(); } else { Reporter.log("Invalid device name"); } } } else {
	 * Reporter.log("No Device added in grid"); }
	 * 
	 * }
	 */

	/*
	 * public void deleteDevice(String devicename) throws InterruptedException,
	 * IOException, AWTException {
	 * 
	 * ((JavascriptExecutor)
	 * webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)")
	 * ;
	 * 
	 * waitForAjax(); // check 'Select Interface' link
	 * 
	 * if (isElementPresent(APT_IPTransitObj.APT_IPTransit.existingdevicegrid))
	 * { List<String> addeddevicesList = new ArrayList<>(
	 * Arrays.asList(getTextFrom(APT_IPTransitObj.APT_IPTransit.
	 * addeddevices_list))); // System.out.println(addeddevicesList); int
	 * AddedDevicesCount = addeddevicesList.size(); for (int i = 0; i <
	 * AddedDevicesCount; i++) { String AddedDeviceNameText =
	 * getTextFrom(addeddevicesList.get(i)); String AddedDevice_SNo =
	 * AddedDeviceNameText.substring(0, 1); if
	 * (AddedDeviceNameText.contains(devicename)) { String
	 * AddedDevice_DeletefromserviceLink = getTextFrom(
	 * APT_IPTransitObj.APT_IPTransit.addeddevice_deletefromservicelink).replace
	 * ("value",AddedDevice_SNo); click(AddedDevice_DeletefromserviceLink);
	 * waitForAjax();
	 * 
	 * Robot r = new Robot(); r.keyPress(KeyEvent. VK_ENTER);
	 * r.keyRelease(KeyEvent. VK_ENTER); /* Alert alert =
	 * webDriver.switchTo().alert();
	 * 
	 * // Capturing alert message. String alertMessage =
	 * webDriver.switchTo().alert().getText(); if (alertMessage.isEmpty()) {
	 * System.out.println("No Message displays"); } else {
	 * System.out.println("text message for alert displays as: " +
	 * alertMessage); }
	 * 
	 * try { alert.accept(); waitForAjax(); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * Outband.verifysuccessmessage("Device successfully removed from service."
	 * ); //break; } } } else { Reporter.log("No Device added in grid"); } }
	 */

	/*
	 * public void deleteDevice(String testDataFile, String dataSheet, String
	 * scriptNo, String dataSetNo) throws InterruptedException, IOException {
	 * 
	 * 
	 * if(isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) { String
	 * DeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
	 * dataSetNo, "ExistingDeviceName"); List<WebElement> webElements =
	 * findWebElements(APTIPAccessNoCPE.APTNoCPE.addeddevicesList); //String
	 * addeddevicesList=findWebElements(APTIPAccessNoCPE.APTNoCPE.
	 * addeddevicesList); int
	 * addeddevicesLists=getXPathCount(APTIPAccessNoCPE.APTNoCPE.
	 * addeddevicesList1);
	 * 
	 * for(int i=0;i<addeddevicesLists;i++) {
	 * 
	 * String AddedDeviceNameText= webElements.get(i).getText(); String
	 * AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
	 * if(AddedDeviceNameText.contains(DeviceName)) { String
	 * AddedDevice_DeletefromserviceLink=
	 * APTIPAccessNoCPE.APTNoCPE.deleteDevice1+ AddedDevice_SNo
	 * +APTIPAccessNoCPE.APTNoCPE.deleteDevice2;
	 * click(AddedDevice_DeletefromserviceLink);
	 * 
	 * Reporter.log("Device successfully removed from service."); } else {
	 * Reporter.log("Invalid device name"); } } } else {
	 * Reporter.log("No Device added in grid"); } }
	 */
	public void verifyEditservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		// Cancel edit service
		waitForAjax();
		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);
		click(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.edit, "Edit");
		waitForAjax();
		waitToPageLoad();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(APT_IPTransitObj.APT_IPTransit.cancelbutton1, "Cancel");
		waitForAjax();
		waitToPageLoad();
		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);
		if (isElementPresent(APT_IPTransitObj.APT_IPTransit.servicepanel_header)) {
			Reporter.log("Navigated to view service page");
		} else
			// Didn't navigate to view service page
			Reporter.log("Didn't navigate to view service page");

		// Edit service
		click(APT_IPTransitObj.APT_IPTransit.serviceactiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.edit, "Edit");
		waitForAjax();
		waitToPageLoad();
		scrollIntoTop();
		// sendKeys(application, "Service Identification",
		// "serviceidentificationtextfield", sid, xml);
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// sendKeys(application, "Termination Date",
		// "terminationdate_field", editterminationdate, xml);
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// selectValueInsideDropdown(application, "billingtype_dropdown",
		// "Billing Type", editbillingtypevalue, xml);
		// selectValueInsideDropdown(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// sendKeys(application, "Remarks", "remarktextarea",
		// EditRemarks, xml);
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);

		scrollDown(APT_IPTransitObj.APT_IPTransit.remarktextarea);
		// Edit email
		click(APT_IPTransitObj.APT_IPTransit.selectedemail, "Selected Email");
		click(APT_IPTransitObj.APT_IPTransit.emailremovearrow, "Email remove arrow");
		// sendKeys(application, , "emailtextfieldvalue",
		// editemail,"Email");
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);

		click(APT_IPTransitObj.APT_IPTransit.emailaddarrow, "Email adding arrow");
		// Edit phone contact
		scrollDown(APT_IPTransitObj.APT_IPTransit.remarktextarea);
		click(APT_IPTransitObj.APT_IPTransit.selectedphone, "Selected phone contact");
		click(APT_IPTransitObj.APT_IPTransit.phoneremovearrow, "Phonecontact remove arrow");
		// sendKeys(application, ,
		// "phonecontacttextfieldvalue", editphonecontact,"Phone Contact");
		// sendKeys(testDataFile, sheetName, scriptNo,
		// dataSetNo);

		click(APT_IPTransitObj.APT_IPTransit.phoneaddarrow, "phonecontact adding Arrow");

		// management options panel
		scrollDown(APT_IPTransitObj.APT_IPTransit.managementoptions_header);
		// editcheckbox_commonMethod(application,
		// edit_performancereporting_checkbox, "performancereporting_checkbox",
		// "Performance Reporting", xml);
		// editcheckbox_commonMethod(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		// editcheckbox_commonMethod(application, edit_ipguardian_checkbox,
		// "ipguardian_checkbox", "IP Guardian", xml);
		// editcheckbox_commonMethod(testDataFile, sheetName, scriptNo,
		// dataSetNo);
		waitForAjax();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(APT_IPTransitObj.APT_IPTransit.editservice_okbutton, "OK");
		waitForAjax();
		waitToPageLoad();

		if (isElementPresent(APT_IPTransitObj.APT_IPTransit.customerdetailsheader)) {
			Reporter.log("Navigated to view service page");
			// Outband.verifysuccessmessage("Service updated successfully");
		} else {
			Reporter.log("Service not updated");
		}

	}

	/*
	 * public void addExistingPEDevice(String testDataFile, String sheetName,
	 * String scriptNo, String dataSetNo) throws IOException,
	 * InterruptedException {
	 * 
	 * String existingdevicename = DataMiner.fngetcolvalue(testDataFile,
	 * sheetName, scriptNo, dataSetNo, "ExistingDeviceName");
	 * 
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.providerequipment_header,
	 * "provide requipment header"); String providerequipment =
	 * getTextFrom(APTIPAccessNoCPE.APTNoCPE.providerequipment_header); String
	 * provideRequipment = "Provider Equipment (PE)";
	 * providerequipment.equalsIgnoreCase(provideRequipment);
	 * 
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "addpe device");
	 * click(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "addpe device");
	 * 
	 * if (isClickable(APTIPAccessNoCPE.APTNoCPE.addpedevice_header)) {
	 * Reporter.log("Add PE Device' page navigated as expected");
	 * 
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,
	 * "typepe name dropdown");
	 * click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,
	 * "typepe name dropdown"); //
	 * click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,"typepename"); //
	 * waitForAjax();
	 * 
	 * // sendKeys(APTIPAccessNoCPE.APTNoCPE.typepename_dropdowninputfield,
	 * existingdevicename,"typepe // name dropdown"); //
	 * webDriver.findElement(By.xpath("(//div[label[text()='Type PE name // to
	 * //
	 * filter']]//span[contains(text(),'"+existingdevicename+"')])[1]")).click()
	 * ; addDropdownValues_commonMethod("Type PE name to filter",
	 * APTIPAccessNoCPE.APTNoCPE.typepename_dropdown, existingdevicename);
	 * //click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,
	 * "typepe name dropdown"); //
	 * webDriver.findElement(By.xpath("(//div[label[text()='Type PE name // to
	 * //
	 * filter']]//span[contains(text(),'"+existingdevicename+"')])[1]")).click()
	 * ;
	 * 
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_vendormodelvalue,
	 * "existing device vendor model value");
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.
	 * existingdevice_managementaddressvalue,
	 * "existing device management addressvalue");
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.
	 * existingdevice_connectivityprotocol,
	 * "existing device connectivi typrotocol");
	 * 
	 * String SNMPVersionValue =
	 * APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpversion;
	 * 
	 * if (SNMPVersionValue.equalsIgnoreCase("2c")) {
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpro,
	 * "existing device snmpro");
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmprw,
	 * "existing device snmprw"); }
	 * 
	 * else if (SNMPVersionValue.equalsIgnoreCase("3")) {
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3username,
	 * "existing device snmp v3 username");
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3authpassword,
	 * "existing device snmp v3 auth password");
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3privpassword,
	 * "existingdevice snmp v3 priv password");
	 * 
	 * } else {
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpversion,
	 * "existing device snmp version");
	 * 
	 * } verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_country,
	 * "Country"); verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_city,
	 * "City"); verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_site,
	 * "Site"); verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_premise,
	 * "Premise");
	 * 
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");
	 * click(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");
	 * 
	 * verifyExists(APTIPAccessNoCPE.APTNoCPE.successmsg,
	 * "Add Device success msg"); String successMsg =
	 * getTextFrom(APTIPAccessNoCPE.APTNoCPE.successmsg); String SuccessMsg =
	 * "Device is added to Service.";
	 * 
	 * successMsg.equalsIgnoreCase(SuccessMsg);
	 * 
	 * } else { Reporter.log("Add PE Device' page not navigated");
	 * 
	 * } }
	 */

	public void verifyExistingDevice_ViewDevicedetails_PE(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		Reporter.log("Verifying Existing PE Device Information");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
			Reporter.log("View PE Device' page navigated as expected");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor/Model");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectiviy Protocol");

			String SNMPVersionValue = APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion;

			if (SNMPVersionValue.equalsIgnoreCase("2c")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro, "view page snmpro");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "view page snmprw");

			}

			else if (SNMPVersionValue.equalsIgnoreCase("3")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3username, "Snmp v3 Username");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword, "Snmp v3 Auth password");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword, "Snmp v3 Priv password");

			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_city, "City");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");

			// verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,"Back");
			// click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,"Back");
		} else {
			Reporter.log("View PE Device' page not navigated");
		}
	}

	public void deleteExistingDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException, AWTException {
		//waitForAjax();
		verifyExists(APT_IPTransitObj.APT_IPTransit.deviceAction, "Delete Action Dropdown");
		click(APT_IPTransitObj.APT_IPTransit.deviceAction, "Delete Action Dropdown");
		//waitForAjax();
		verifyExists(APT_IPTransitObj.APT_IPTransit.deviceDelete, "Delete Action Dropdown");
		click(APT_IPTransitObj.APT_IPTransit.deviceDelete, "Delete Option");
		//waitForAjax();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		waitForAjax();
		verifyExists(APT_IPTransitObj.APT_IPTransit.deleteMsg, "Delete message");

	}
	
	public void verify_JuniperVendor_AddInterface(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		
		String Interfacesheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.interfaces_header, "Interfaces");
		String InterfacesheaderValue = "Interfaces";
		scrollDown(APT_IPTransitObj.APT_IPTransit.interfaces_header);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");

		Interfacesheader.equals(InterfacesheaderValue);
		click(APT_IPTransitObj.APT_IPTransit.interfacepanel_actiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.addinterface_link, "Add Interface/Link");
		waitForAjax();
		waitToPageLoad();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		//String AddInterfaceheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.addinterface_header,
				//"Add Interface header");
		scrollDown(APT_IPTransitObj.APT_IPTransit.executeandsave_button);
		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.executeandsave_button, "Execute and Save");
		waitForAjax();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		// verify warning messages in add interface page
		verifyExists(APT_IPTransitObj.APT_IPTransit.interface_warngmsg, "Interface");
		verifyExists(APT_IPTransitObj.APT_IPTransit.bearertype_warngmsg, "BearerType");
		verifyExists(APT_IPTransitObj.APT_IPTransit.encapsulation_warngmsg, "Encapsulation");
		verifyExists(APT_IPTransitObj.APT_IPTransit.slot_warngmsg, "Slot");
		verifyExists(APT_IPTransitObj.APT_IPTransit.pic_warngmsg, "Pic");
		verifyExists(APT_IPTransitObj.APT_IPTransit.port_warngmsg, "Port");
		verifyExists(APT_IPTransitObj.APT_IPTransit.stm1number_warngmsg, "STM1Number");
		// verifyExists(APT_IPTransitObj.APT_IPTransit.interface_warngmsg,
		// "Interface");

		// ((JavascriptExecutor)
		// webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		// Add Interface
		scrollDown(APT_IPTransitObj.APT_IPTransit.configureinterface_checkbox);
		// ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.configureinterface_checkbox);
		// waitForAjax();

		click(APT_IPTransitObj.APT_IPTransit.configureinterface_checkbox, "Configure Interface on Device");
		waitForAjax();

		String network = getTextFrom(APT_IPTransitObj.APT_IPTransit.network_fieldvalue, "Network");
		String networkValue = "XFER";
		network.equals(networkValue);

		scrollDown(APT_IPTransitObj.APT_IPTransit.getaddress2_button);

		String link_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LinkValue");
		sendKeys(APT_IPTransitObj.APT_IPTransit.link_textfield, link_value, "Link");

		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BearerType_Value");
		selectByVisibleText(APT_IPTransitObj.APT_IPTransit.bearertype_dropdown, bearertype_value, "Bearer type");
		waitForAjax();

		String bandwidth_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Bandwidth_Value");
		String cardtype_dropdownvalue_gigabit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CardType_Dropdown_Gigabit");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String framingtype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"FramingType_Value");
		String clocksource_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ClockSourceValue");
		String STM1Number_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"STM1NumberValue");
		String bearerNo_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BearerNumber_Value");
		String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BGP_Checkbox");

		if (bearertype_value.equalsIgnoreCase("10Gigabit Ethernet")
				|| bearertype_value.equalsIgnoreCase("Gigabit Ethernet")) {
			click(APT_IPTransitObj.APT_IPTransit.bandwidth_dropdown, bandwidth_value);
			//click(APT_IPTransitObj.APT_IPTransit.cardtype_dropdown,cardtype_dropdownvalue_gigabit,"Card Type");
			click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
		}

		else if (bearertype_value.equalsIgnoreCase("E1")) {
			click(APT_IPTransitObj.APT_IPTransit.bandwidth_dropdown, bandwidth_value);
			click(APT_IPTransitObj.APT_IPTransit.cardtype_dropdown, cardtype_dropdownvalue_gigabit);
			click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
			click(APT_IPTransitObj.APT_IPTransit.framingtype_dropdown, framingtype_value);
			click(APT_IPTransitObj.APT_IPTransit.clocksource_dropdown, clocksource_value);
			sendKeys(APT_IPTransitObj.APT_IPTransit.STM1Number_textfield, STM1Number_value, "STM1 Number");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bearerno_textfield, bearerNo_value, "Bearer No");
		} else if (bearertype_value.equalsIgnoreCase("E3") || bearertype_value.equalsIgnoreCase("T3")) {
			//selectByVisibleText(APT_IPTransitObj.APT_IPTransit.bandwidth_dropdown, bandwidth_value, "Bandwidth");
			selectByVisibleText(APT_IPTransitObj.APT_IPTransit.cardtype_dropdown,cardtype_dropdownvalue_gigabit,
					"Card type");
			selectByVisibleText(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value,
					"Encapsulation");
			sendKeys(APT_IPTransitObj.APT_IPTransit.STM1Number_textfield, STM1Number_value, "STM1 Number");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bearerno_textfield, bearerNo_value, "Bearer No");
		} else if (bearertype_value.equalsIgnoreCase("STM-1")) {
			click(APT_IPTransitObj.APT_IPTransit.bandwidth_dropdown, bandwidth_value);
			click(APT_IPTransitObj.APT_IPTransit.cardtype_dropdown, cardtype_dropdownvalue_gigabit);
			click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
			sendKeys(APT_IPTransitObj.APT_IPTransit.STM1Number_textfield, STM1Number_value, "STM1 Number");
		} else {
			click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
			sendKeys(APT_IPTransitObj.APT_IPTransit.STM1Number_textfield, STM1Number_value, "STM1 Number");
		}

		waitForAjax();

		String unitid_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UnitIDValue");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SlotValue");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PicValue");
		String port_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PortValue");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename");

		
		sendKeys(APT_IPTransitObj.APT_IPTransit.unitid_textfield, unitid_value, "UnitIDValue");
		sendKeys(APT_IPTransitObj.APT_IPTransit.slot_textfield, slot_value, "Slot");
		sendKeys(APT_IPTransitObj.APT_IPTransit.pic_textfield, pic_value, "Pic");
		sendKeys(APT_IPTransitObj.APT_IPTransit.port_textfield, port_value, "Port");

		waitForAjax();
		InterfaceName = getAttributeFrom(APT_IPTransitObj.APT_IPTransit.interfacename_textfield, "value");

		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANID_Value");

		if (encapsulation_value.equalsIgnoreCase("802.1q")) {
			sendKeys(APT_IPTransitObj.APT_IPTransit.vlanid_textfield, vlanID_value, "VLANID_Value");
		}
		scrollDown(APT_IPTransitObj.APT_IPTransit.interfacename_textfield);
		click(APT_IPTransitObj.APT_IPTransit.ivmanagement_checkbox, "IV Management");
		click(APT_IPTransitObj.APT_IPTransit.atricaconnected_checkbox, "Atrica Connected");
		click(APT_IPTransitObj.APT_IPTransit.bgp_checkbox, "BGP");

		String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGPTemplate_Value");
		String cpewan_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEWAN_Value");
		String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEWANIPv6_Value");
		String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Description_Value");
		String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"AsCustomer_Value");
		String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGPPassword_Value");

		if (bgp_checkbox.equalsIgnoreCase("yes")) {
			//selectByVisibleText(APT_IPTransitObj.APT_IPTransit.bgptemplate_dropdown,
					//bgptemplate_dropdownvalue, "BGP template");
			sendKeys(APT_IPTransitObj.APT_IPTransit.cpewan_textfield, cpewan_value, "CPE WAN");
			sendKeys(APT_IPTransitObj.APT_IPTransit.cpewanipv6_textfield, cpewanipv6_value, "CPE WAN IPv6 Address");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bgp_descriptionfield, descriptionfield_value, "Description");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bgp_ascustomerfield, ascustomerfield_value, "AS CUSTOMER");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bgppassword_field, bgppassword_value, "BGP PASSWORD");
		}
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		// configuration panel in add interface page
		Juniper_generateConfiguration();
		waitForAjax();
		waitToPageLoad();
		// Outband.verifysuccessmessage("Interface added successfully");
		waitForAjax();

		// Verify added interface
		// ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.interfaces_header);

		// ((JavascriptExecutor)
		// webDriver).executeScript("window.scrollBy(0,-100)");
		String AddedInterfaces = getTextFrom(APT_IPTransitObj.APT_IPTransit.addedinterfaces,"added interfaces");
		String addedinterfacecheck = getAttributeFrom(AddedInterfaces, "style");
	

		scrollDown(APT_IPTransitObj.APT_IPTransit.back);
		// ((JavascriptExecutor)
		// webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_IPTransitObj.APT_IPTransit.back, "manage page back button");
		// click(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton, "Back");
		waitForAjax();
		waitToPageLoad();

	}

	public void verifyViewDevicepage_Links() throws IOException, InterruptedException, AWTException {
		String Viewdeviceheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewdevicepage_header,
				"View device header");
		String ViewdeviceheaderValue = "Device Details";
		scrollIntoTop();
		click(APT_IPTransitObj.APT_IPTransit.viewdevice_Actiondropdown, "Action");
		String Edit = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewdevice_Edit, "Edit");
		String Delete = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewdevice_delete, "Delete");
		String FetchInterface = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewdevice_fetchinterfacelink,
				"Fetch Interface");

		// Edit in view device page
		click(APT_IPTransitObj.APT_IPTransit.viewdevice_Edit, "Edit");
		waitForAjax();
		waitToPageLoad();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		if (isVisible(APT_IPTransitObj.APT_IPTransit.editdeviceheader)) {
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitForAjax();
			click(APT_IPTransitObj.APT_IPTransit.cancelbutton1, "Cancel");
			waitForAjax();
			waitToPageLoad();
			ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.providerequipment_header);
			// ((JavascriptExecutor)
			// webDriver).executeScript("window.scrollBy(0,-100)");
			if (isVisible(APT_IPTransitObj.APT_IPTransit.existingdevicegrid)) {
				click(APT_IPTransitObj.APT_IPTransit.viewservicepage_viewdevicelink, "View");
				waitForAjax();
				waitToPageLoad();
				Viewdeviceheader.equals(ViewdeviceheaderValue);
			} else {
				// No Device added in grid
			}
		} else {
			// Not navigated to 'Edit PE Device' page
		}

		// verify delete device in view device page
		// click(APT_IPTransitObj.APT_IPTransit.viewdevice_Actiondropdown,
		// "Action");
		// click(APT_IPTransitObj.APT_IPTransit.viewdevice_delete1, "Delete");

		// Robot r = new Robot();
		// r.keyPress(KeyEvent. VK_ENTER);
		// r.keyRelease(KeyEvent. VK_ENTER);
		/*
		 * Alert alert = webDriver.switchTo().alert();
		 * 
		 * // Capturing alert message String alertMessage =
		 * webDriver.switchTo().alert().getText(); if (alertMessage.isEmpty()) {
		 * Reporter.log("No Message displays"); } else {
		 * Reporter.log("Text message for alert displays as: " + alertMessage);
		 * }
		 * 
		 * try { alert.dismiss(); waitForAjax(); } catch (Exception e) {
		 * e.printStackTrace(); }
		 */
		// ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton);
		// ((JavascriptExecutor)
		// webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		// verifyExists(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton,"Back");
		// click(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton,"Back");
		waitForAjax();
		waitToPageLoad();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	}

	public void generateConfiguration() throws InterruptedException, IOException {

		// perform Generate configuration
		boolean configurationpanel = false;
		configurationpanel = isElementPresent(APT_IPTransitObj.APT_IPTransit.configuration_header);
		if (configurationpanel) {
			System.out.println("'Configuration' panel is displaying");

			click(APT_IPTransitObj.APT_IPTransit.generateconfiguration_button, "Generate Configuration");
			waitToPageLoad();
			waitForAjax();
			if (isElementPresent(APT_IPTransitObj.APT_IPTransit.staus_statuspopup)) {
				click(APT_IPTransitObj.APT_IPTransit.configAlert_okbutton, "Ok");
			}
			waitToPageLoad();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			Thread.sleep(1000);

			String configurationvalues = getTextFrom(APT_IPTransitObj.APT_IPTransit.configuration_textarea,"configuration_text area");
			if (configurationvalues.isEmpty()) {
				System.out.println(
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
			} else {
				System.out.println("After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
			}

			click(APT_IPTransitObj.APT_IPTransit.saveconfiguration_button, "Save Configuration to File");
			waitForAjax();
			click(APT_IPTransitObj.APT_IPTransit.executeandsave_button, "Execute and Save");
			waitForAjax();

		} else {
			System.out.println("'Configuration' panel is not displaying");
			click(APT_IPTransitObj.APT_IPTransit.okbutton, "OK");
		}
	}

	public void verify_CiscoVendor_AddInterface(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interface name");
		String defaultCheckbValue = "no";
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Configure Interface on Device");
		String link_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "link_value");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bearer Type");
		String ciscovendor_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Bandwidth");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Encapsulation");
		String framingtype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Framing Type");
		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
		String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BGP");

		String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGP Templates Generate For");
		String cpewan_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE WAN");
		String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE WAN IPv6 Address");
		String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Description");
		String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"AS CUSTOMER");
		String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGP PASSWORD");

		String Interfacesheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.interfaces_header, "Interfaces header");
		String InterfacesheaderValue = "Interfaces";
		scrollDown(APT_IPTransitObj.APT_IPTransit.interfaces_header);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		Interfacesheader.equals(InterfacesheaderValue);
		click(APT_IPTransitObj.APT_IPTransit.interfacepanel_actiondropdown, "Action dropdown");
		click(APT_IPTransitObj.APT_IPTransit.addinterface_link, "Add Interface/Link");
		waitForAjax();
		waitToPageLoad();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		getTextFrom(APT_IPTransitObj.APT_IPTransit.addinterface_header, "Add Interface header");
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.okbutton, "OK");
		waitForAjax();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		// verify warning messages in add interface page

		verifyExists(APT_IPTransitObj.APT_IPTransit.interface_warngmsg, "Interface");
		verifyExists(APT_IPTransitObj.APT_IPTransit.bearertype_warngmsg, "Bearer Type");
		verifyExists(APT_IPTransitObj.APT_IPTransit.encapsulation_warngmsg, "Encapsulation");

		// Add Interface
		click(APT_IPTransitObj.APT_IPTransit.configureinterface_checkbox, configureinterface_checkbox);
		sendKeys(APT_IPTransitObj.APT_IPTransit.interfacename_textfield, interfacename, "Interface");
		InterfaceName = interfacename;

		String network = getTextFrom(APT_IPTransitObj.APT_IPTransit.network_fieldvalue, "Network");
		String networkValue = "XFER";
		network.equals(networkValue);

		// interfaceAddressRangeIPv4(existingAddressRangeIPv4selection,
		// newAddressRangeIpv4selection, subnetSizeValue_IPv4,
		// eipallocation_city, existingAddressIPv4DropdownValue,
		// newinterfaceAddressrange);
		// interfaceAddressRangeIPv6(existingAddressRangeIPv6selection,
		// newAddressRangeIpv6selection, subnetSizeValue_IPv6,
		// availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);

		scrollDown(APT_IPTransitObj.APT_IPTransit.link_textfield);
		sendKeys(APT_IPTransitObj.APT_IPTransit.link_textfield, link_value, "Link Value");
		click(APT_IPTransitObj.APT_IPTransit.bearertype_dropdown, bearertype_value);
		waitForAjax();
		click(APT_IPTransitObj.APT_IPTransit.bandwidth_dropdown, ciscovendor_bandwidth_value);
		if (bearertype_value.equalsIgnoreCase("E1")) {
			click(APT_IPTransitObj.APT_IPTransit.framingtype_dropdown, framingtype_value);
			click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
		} else if (bearertype_value.equalsIgnoreCase("Ethernet")) {
			sendKeys(APT_IPTransitObj.APT_IPTransit.vlanid_textfield, vlanID_value, "VLAN Id");
		} else {
			click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
		}

		Thread.sleep(1000);
		scrollDown(APT_IPTransitObj.APT_IPTransit.link_textfield);

		click(APT_IPTransitObj.APT_IPTransit.bgp_checkbox, bgp_checkbox);

		if (bgp_checkbox.equalsIgnoreCase("yes")) {
			scrollDown(APT_IPTransitObj.APT_IPTransit.bgp_checkbox);

			click(APT_IPTransitObj.APT_IPTransit.multilink_bgptemplate_dropdown, bgptemplate_dropdownvalue);
			sendKeys(APT_IPTransitObj.APT_IPTransit.cpewan_textfield, cpewan_value, "CPE WAN");
			sendKeys(APT_IPTransitObj.APT_IPTransit.cpewanipv6_textfield, cpewanipv6_value, "CPE WAN IPv6 Address");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bgp_descriptionfield, descriptionfield_value, "Description");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bgp_ascustomerfield, ascustomerfield_value, "AS CUSTOMER");
			sendKeys(APT_IPTransitObj.APT_IPTransit.bgppassword_field, bgppassword_value, "BGP PASSWORD");
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		// configuration panel in add interface page
		generateConfiguration();

		Thread.sleep(5000);
		waitToPageLoad();
		// Outband.verifysuccessmessage("Interface added successfully");
		waitForAjax();

		// Verify added interface

		scrollDown(APT_IPTransitObj.APT_IPTransit.interfaces_header);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		String AddedInterfaces = getTextFrom(APT_IPTransitObj.APT_IPTransit.addedinterfaces,"Add Interface");
		String addedinterfacecheck = getAttributeFrom(AddedInterfaces, "style");
		if (!addedinterfacecheck.contains("height: 1px")) {
			String Interfaces = getTextFrom(APT_IPTransitObj.APT_IPTransit.interfaces_header, "Interfaces");
			String InterfacesValue = "InterfacesValue";
			Interfaces.equals(InterfacesValue);
			

		} else {
			// No Interfaces added under Interfaces panel
		}
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton, "Back");
		waitForAjax();
		waitToPageLoad();
	}



	public void Juniper_generateConfiguration() throws IOException {

		// perform Generate configuration
		boolean configurationpanel = false;
		configurationpanel = isElementPresent(APT_IPTransitObj.APT_IPTransit.configuration_header);
		if (configurationpanel) {
			Reporter.log("'Configuration' panel is displaying");

			click(APT_IPTransitObj.APT_IPTransit.generateconfiguration_button, "Generate Configuration");
			waitToPageLoad();
			waitForAjax();

			if (isElementPresent(APT_IPTransitObj.APT_IPTransit.staus_statuspopup)) {
				click(APT_IPTransitObj.APT_IPTransit.configAlert_okbutton, "OK");
			}
			waitToPageLoad();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitForAjax();

			String configurationvalues = getTextFrom(APT_IPTransitObj.APT_IPTransit.configuration_textarea,"configuration_text area");
			if (configurationvalues.isEmpty()) {
				System.out.println(
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
			} else {
				System.out.println("After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
			}

			click(APT_IPTransitObj.APT_IPTransit.saveconfiguration_button, "Save Configuration to File");
			waitForAjax();
			click(APT_IPTransitObj.APT_IPTransit.executeandsave_button, "Execute and Save");
			waitForAjax();
		}

		else {
			System.out.println("'Configuration' panel is not displaying");
			click(APT_IPTransitObj.APT_IPTransit.okbutton, "OK");
		}
	}

	public void verifyExistingDevice_ViewDevicedetails() throws IOException, InterruptedException {
		Reporter.log("Verifying Existing PE Device Information");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
			Reporter.log("View PE Device' page navigated as expected");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor/Model");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectiviy Protocol");

			String SNMPVersionValue = APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion;

			if (SNMPVersionValue.equalsIgnoreCase("2c")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro, "view page snmpro");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "view page snmprw");

			}

			else if (SNMPVersionValue.equalsIgnoreCase("3")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3username, "Snmp v3 Username");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword, "Snmp v3 Auth password");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword, "Snmp v3 Priv password");

			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_city, "City");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");

			// verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,"Back");
			// click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,"Back");
		} else {
			Reporter.log("View PE Device' page not navigated");
		}
	}
	
	

	public void verifyViewpage_Devicedetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String nameValue = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_devicename, "Name");
		// Device Name
		name.equals(nameValue);

		String vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String vendormodelValue = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_vendormodel, "Vendor/Model");
		// Vendor/model
		vendormodel.equals(vendormodelValue);

		String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Ssh");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmp2c");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmp3");
		String snmpro2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmpro2cvalue");
		String snmprw2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmprw2cvalue");
		String Snmpv3Usernamevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3Usernamevalue");
		String Snmpv3Authpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3Authpasswordvalue");

		String Snmpv3Privpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3Privpasswordvalue");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
		String Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityCode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseName");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseCode");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingCityValue");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing PremiseValue");

		String telnetValue = "telnet";
		String SshValue = "ssh";

		// Connectivity protocol
		if ((telnet.equalsIgnoreCase("Yes")) && (ssh.equalsIgnoreCase("No"))) {
			telnet.equals(telnetValue);
		} else if ((telnet.equalsIgnoreCase("no")) && (ssh.equalsIgnoreCase("Yes"))) {
			ssh.equals(SshValue);
		}

		// SNMP Version
		if ((snmp2c.equalsIgnoreCase("Yes")) && (snmp3.equalsIgnoreCase("No"))) {

			String snmp2cValue = "2c";
			// snmp version
			snmp2c.equals(snmp2cValue);
			// Snmpro
			if (!snmpro2cvalue.equalsIgnoreCase("Null")) {
				(APT_IPTransitObj.APT_IPTransit.viewpage_snmpro).equals(snmpro2cvalue);
			} else {
				String snmpro2cvalue1 = "incc";
				snmpro2cvalue.equals(snmpro2cvalue1);
			}

			// Snmprw
			if (!snmprw2cvalue.equalsIgnoreCase("Null")) {
				(APT_IPTransitObj.APT_IPTransit.viewpage_snmprw).equals(snmprw2cvalue);
			} else {
				String Snmprw1 = "ip4corp3";
				snmprw2cvalue.equals(Snmprw1);
			}

		} else if ((snmp2c.equalsIgnoreCase("No")) && (snmp3.equalsIgnoreCase("Yes"))) {

			// Snmp v3 Username
			if (!Snmpv3Usernamevalue.equalsIgnoreCase("Null")) {
				(APT_IPTransitObj.APT_IPTransit.viewpage_snmpv3username).equals(Snmpv3Usernamevalue);
			} else {
				String Snmpv3Usernamevalue1 = "colt-nms";
				Snmpv3Usernamevalue.equals(Snmpv3Usernamevalue1);
			}

			// Snmp v3 Auth Password
			if (!Snmpv3Authpasswordvalue.equalsIgnoreCase("Null")) {
				(APT_IPTransitObj.APT_IPTransit.viewpage_snmpv3authpassword).equals(Snmpv3Authpasswordvalue);
			} else {
				String Snmpv3Authpasswordvalue1 = "OrHzjWmRvr4piJZb";
				Snmpv3Authpasswordvalue.equals(Snmpv3Authpasswordvalue1);
			}

			String Snmpv3Privpasswordvalue1 = "3k0hw8thNxHucQkE";
			// Snmp v3 Priv Password
			if (!Snmpv3Privpasswordvalue.equalsIgnoreCase("Null")) {
				(APT_IPTransitObj.APT_IPTransit.viewpage_snmpv3privpassword).equals(Snmpv3Privpasswordvalue);
			} else {
				Snmpv3Privpasswordvalue.equals(Snmpv3Privpasswordvalue1);
			}
		}

		// Management Address
		getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_managementaddress, "Management Address");

		// Country
		getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_country, "Country");

		// City
		if ((existingcity.equalsIgnoreCase("yes")) && (newcity.equalsIgnoreCase("NO"))) {

			// Existing City
			getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_city, "City");

		} else if ((existingcity.equalsIgnoreCase("NO")) && (newcity.equalsIgnoreCase("Yes"))) {

			// new City
			getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_city, "City");

		}

		// Site
		if ((existingsite.equalsIgnoreCase("yes")) && (newsite.equalsIgnoreCase("NO"))) {

			// Existing Site
			getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_site, "Site");
		}

		else if ((existingsite.equalsIgnoreCase("No")) && (newsite.equalsIgnoreCase("Yes"))) {

			// New Site
			getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_site, "Site");
		}

		// Premise
		if ((existingpremise.equalsIgnoreCase("yes")) && (NewPremise.equalsIgnoreCase("NO"))) {

			// Existing premise
			getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_premise, "Premise");
		}

		else if ((existingpremise.equalsIgnoreCase("No")) && (NewPremise.equalsIgnoreCase("Yes"))) {

			// new premise
			getTextFrom(APT_IPTransitObj.APT_IPTransit.viewpage_premise, "Premise");
		}
	}

	public static String Cisco_Multilink_InterfaceName;

	public void verify_CiscoVendor_AddMultilink(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo, String devicename) throws IOException, InterruptedException {
		String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "multilink_configureinterface_checkbox");

		scrollDown(APT_IPTransitObj.APT_IPTransit.providerequipment_header);
		waitForAjax();
		if (isElementPresent(APT_IPTransitObj.APT_IPTransit.existingdevicegrid)) {
			List<String> addeddevicesList = new ArrayList<>(
					Arrays.asList(getTextFrom(APT_IPTransitObj.APT_IPTransit.addeddevices_list,"added devices_list")));
			// System.out.println(addeddevicesList);
			int AddedDevicesCount = addeddevicesList.size();
			for (int i = 0; i < AddedDevicesCount; i++) {
				String AddedDeviceNameText = getTextFrom(addeddevicesList.get(i));
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);

				if (AddedDeviceNameText.contains(devicename)) {
					String AddedDevice_ViewLink = getTextFrom(APT_IPTransitObj.APT_IPTransit.addeddevice_viewlink,"added device_view link")
							.replace("value", AddedDevice_SNo);
					click(AddedDevice_ViewLink,"Add Device");
					waitForAjax();
					waitToPageLoad();
					String Viewdeviceheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.viewdevicepage_header,
							"View device header");
					String ViewdeviceheaderValue = "Device Details";
					Viewdeviceheader.equals(ViewdeviceheaderValue);

					scrollDown(APT_IPTransitObj.APT_IPTransit.interfaces_header);
					waitForAjax();
					((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
					String Interfacesheader = getTextFrom(APT_IPTransitObj.APT_IPTransit.interfaces_header,
							"Interfaces header");
					String InterfacesheaderValue = "Interfaces";
					Interfacesheader.equals(InterfacesheaderValue);
					click(APT_IPTransitObj.APT_IPTransit.interfacepanel_actiondropdown, "Action dropdown");
					click(APT_IPTransitObj.APT_IPTransit.addinterface_link, "Add Interface/Link");
					waitForAjax();
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					getTextFrom(APT_IPTransitObj.APT_IPTransit.addmultilink_header, "Add Multilink header");

					click(APT_IPTransitObj.APT_IPTransit.configureinterface_checkbox, "Configure Interface on Device");
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
					Thread.sleep(1000);
					if (multilink_configureinterface_checkbox.equalsIgnoreCase("no")) {
						click(APT_IPTransitObj.APT_IPTransit.okbutton, "OK");
						Thread.sleep(1000);
					} else {
						click(APT_IPTransitObj.APT_IPTransit.executeandsave_button, "Execute and Save");
						Thread.sleep(1000);
					}
					// scrollToTop();
					// verify warning messages in add interface page
					verifyExists(APT_IPTransitObj.APT_IPTransit.interface_warngmsg, "Interface");
					verifyExists(APT_IPTransitObj.APT_IPTransit.encapsulation_warngmsg, "Encapsulation");

					// Add Multilink
					// isDisplayed(application, "Multilink", "multilink_text");

					String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
							dataSetNo, "Interface");
					String link_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"Link Value");
					sendKeys(APT_IPTransitObj.APT_IPTransit.interfacename_textfield, multilink_interfacename,
							"Interface");

					// interfaceAddressRangeIPv4(application,
					// existingAddressRangeIPv4selection,
					// newAddressRangeIpv4selection, subnetSizeValue_IPv4,
					// eipallocation_city, existingAddressIPv4DropdownValue,
					// newinterfaceAddressrange);
					// interfaceAddressRangeIPv6(application,
					// existingAddressRangeIPv6selection,
					// newAddressRangeIpv6selection, subnetSizeValue_IPv6,
					// availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);

					scrollDown(APT_IPTransitObj.APT_IPTransit.link_textfield);
					sendKeys(APT_IPTransitObj.APT_IPTransit.link_textfield, link_value, "Link Value");
					waitForAjax();

					String multilink_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
							dataSetNo, "Bandwidth");
					String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"encapsulation_value");
					click(APT_IPTransitObj.APT_IPTransit.multilink_bandwidth_dropdown, multilink_bandwidth_value);
					click(APT_IPTransitObj.APT_IPTransit.encapsulation_dropdown, encapsulation_value);
					scrollDown(APT_IPTransitObj.APT_IPTransit.link_textfield);
					waitForAjax();

					String multilink_bgpcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"BGP");
					String cpewan_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpewan_value");
					String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpewanipv6_value");
					String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
							dataSetNo, "descriptionfield_value");
					String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"ascustomerfield_value");
					String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"bgppassword_value");
					String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
							dataSetNo, "bgptemplate_dropdownvalue");

					String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
							dataSetNo, "checktoaddinterface_checkbox");

					if (multilink_bgpcheckbox.equalsIgnoreCase("yes")) {
						click(APT_IPTransitObj.APT_IPTransit.bgp_checkbox, multilink_bgpcheckbox);

						click(APT_IPTransitObj.APT_IPTransit.multilink_bgptemplate_dropdown, bgptemplate_dropdownvalue);
						sendKeys(APT_IPTransitObj.APT_IPTransit.cpewan_textfield, cpewan_value, "CPE WAN");
						sendKeys(APT_IPTransitObj.APT_IPTransit.cpewanipv6_textfield, cpewanipv6_value,
								"CPE WAN IPv6 Address");
						sendKeys(APT_IPTransitObj.APT_IPTransit.bgp_descriptionfield, descriptionfield_value,
								"Description");
						sendKeys(APT_IPTransitObj.APT_IPTransit.bgp_ascustomerfield, ascustomerfield_value,
								"AS CUSTOMER");
						sendKeys(APT_IPTransitObj.APT_IPTransit.bgppassword_field, bgppassword_value, "BGP PASSWORD");
					}

					// Multilinked Bearers table
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					String MultilinkedBearers = getTextFrom(APT_IPTransitObj.APT_IPTransit.multilinkedbearers_header,
							"Multilinked Bearers");
					String MultilinkedBearersValue = "Multilinked Bearers";
					MultilinkedBearers.equals(MultilinkedBearersValue);
					// table columns
					String ChecktoaddInterface = getTextFrom(APT_IPTransitObj.APT_IPTransit.checktoaddinterface_column,
							"Multilinked Bearers");
					String Interface = getTextFrom(APT_IPTransitObj.APT_IPTransit.multilink_interface_column,
							"Multilinked Bearers");
					String Link_Circuit = getTextFrom(APT_IPTransitObj.APT_IPTransit.multilink_link_column,
							"Multilinked Bearers");
					String BearerType = getTextFrom(APT_IPTransitObj.APT_IPTransit.multilink_BearerType_column,
							"Multilinked Bearers");
					String VLANId = getTextFrom(APT_IPTransitObj.APT_IPTransit.multilink_vlanid_column,
							"Multilinked Bearers");

					String ChecktoaddInterfaceValue = "Check to add Interface";
					String InterfaceValue = "Interface";
					String Link_CircuitValue = "Link/Circuit";
					String BearerTypeValue = "Bearer Type";
					String VLANIdValue = "VLAN Id";

					ChecktoaddInterface.equals(ChecktoaddInterfaceValue);
					Interface.equals(InterfaceValue);
					Link_Circuit.equals(Link_CircuitValue);
					BearerType.equals(BearerTypeValue);
					VLANId.equals(VLANIdValue);

					String MultilinkBearer_ExistingInterface = getAttributeFrom(
							APT_IPTransitObj.APT_IPTransit.multilinkbearer_tabledata, "style");

					if (!MultilinkBearer_ExistingInterface.contains("height: 1px")) {
						// Existing interface details are displaying in
						// multilink bearer table
						if (checktoaddinterface_checkbox.equalsIgnoreCase("yes")) {
							String CheckToAddInterface = getTextFrom(APT_IPTransitObj.APT_IPTransit.checktoaddinterface,"check to addinterface")
									.replace("value", InterfaceName);
							click(CheckToAddInterface,"Check To Add Interface");
						}
					} else {
						// No existing interfaces to display
					}
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
					// configuration panel in add interface page
					generateConfiguration();

					waitForAjax();
					waitToPageLoad();
					// Outband.verifysuccessmessage("Multilink Interface
					// successfully created");
					scrollDown(APT_IPTransitObj.APT_IPTransit.interfaces_header);
					waitForAjax();
					((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");

					Interfacesheader.equals(InterfacesheaderValue);

					// Multilink table values under interfaces panel
					String Cisco_Multilink_Name = "Multilink" + multilink_interfacename;
					// String Cisco_Multilink_RowID=
					// getTextFrom(APT_IPTransitObj.APT_IPTransit.multilink_rowid).replace("value",
					// Cisco_Multilink_Name).getAttribute("row-id");

					/*
					 * String InterfaceName_value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * interfacename_tablevalue).replace("value",
					 * interfacename_tablevalue); String Link_value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * link_tablevalue).replace("value", link_tablevalue);
					 * String InterfaceAddressRange_value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * interfaceaddressrange_tablevalue).replace("value",
					 * interfaceaddressrange_tablevalue); String
					 * InterfaceAddress_value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * interfaceaddress_tablevalue).replace("value",
					 * interfaceaddress_tablevalue); String BearerType_value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * bearertype_tablevalue).replace("value",
					 * bearertype_tablevalue); String Bandwidth_value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * bandwidth_tablevalue).replace("value",
					 * bandwidth_tablevalue); click(Bandwidth,"Bandwidth");
					 * Bandwidth.sendKeys(Keys.TAB); String vlanIDValue=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * vlanid_tablevalue).replace("value", vlanid_tablevalue);
					 * click(vlanID,"VlanID"); vlanID.sendKeys(Keys.TAB); String
					 * IfInOctets_Value=
					 * getTextFrom(APT_IPTransitObj.APT_IPTransit.
					 * ifinoctets_tablevalue).replace("value",
					 * ifinoctets_tablevalue); waitForAjax();
					 */

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
					click(APT_IPTransitObj.APT_IPTransit.viewpage_backbutton, "View page back button");
					waitForAjax();

					Cisco_Multilink_InterfaceName = Cisco_Multilink_Name;
				} else {
					// Invalid device name
				}
			}
		} else {
			// No Device added in grid
		}

	}

	public static String Juniper_Multilink_InterfaceName;

	public void addNewPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceName");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementAddress");
		String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SSH");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmp2C");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmp3");
		String snmpro2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
		String Snmpv3Usernamevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3UsernameNewValue");
		String Snmpv3Authpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3AuthpasswordNewValue");
		String Snmpv3Privpasswordvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Snmpv3PrivpasswordNewValue");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
		String Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityCode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseName");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseCode");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingCityValue");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing PremiseValue");

		verifyExists(APT_IPTransitObj.APT_IPTransit.Next_Button, "Nex Button");
		click(APT_IPTransitObj.APT_IPTransit.Next_Button, "Next Button");

		verifyExists(APT_IPTransitObj.APT_IPTransit.warningMessage_name, "Device Name");
		verifyExists(APT_IPTransitObj.APT_IPTransit.warningMessage_vendor, "Vendor/Model");

		verifyExists(APT_IPTransitObj.APT_IPTransit.nametextfield, "Name");
		sendKeys(APT_IPTransitObj.APT_IPTransit.nametextfield, name, "Name");

		verifyExists(APT_IPTransitObj.APT_IPTransit.vendormodelinput, "Vendor/Model");
		selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.vendormodelinput, vendormodel, "Vendor/Model");

		verifyExists(APT_IPTransitObj.APT_IPTransit.managementaddresstextbox, "Management Address");
		sendKeys(APT_IPTransitObj.APT_IPTransit.managementaddresstextbox, managementaddress, "Management Address");

		if ((telnet.equalsIgnoreCase("No")) && (ssh.equalsIgnoreCase("No"))) {
			// verifyDefaultSelection_connectivityprotocol_ssh();

			// verifyDefaultSelection_connectivityprotocol_telnet();
		} else {
			// verifyDefaultSelection_connectivityprotocol_ssh();

			// verifyDefaultSelection_connectivityprotocol_telnet();

			if ((telnet.equalsIgnoreCase("Yes")) && (ssh.equalsIgnoreCase("No"))) {
				verifyExists(APT_IPTransitObj.APT_IPTransit.telnetradiobutton, "Telnet");
				click(APT_IPTransitObj.APT_IPTransit.telnetradiobutton, telnet);
			} else if ((telnet.equalsIgnoreCase("No")) && (ssh.equalsIgnoreCase("Yes"))) {
				verifyExists(APT_IPTransitObj.APT_IPTransit.sshradiobutton, "SSH");
				click(APT_IPTransitObj.APT_IPTransit.sshradiobutton, ssh);
			}
		}

		if ((snmp2c.equalsIgnoreCase("Yes")) && (snmp3.equalsIgnoreCase("NO"))) {
			verifyExists(APT_IPTransitObj.APT_IPTransit.c2cradiobutton, "SNMP Version-2c");
			click(APT_IPTransitObj.APT_IPTransit.c2cradiobutton, snmp2c);

			verifyExists(APT_IPTransitObj.APT_IPTransit.snmprotextfield, "Snmpro");
			sendKeys(APT_IPTransitObj.APT_IPTransit.snmprotextfield, snmpro2cvalue);

			verifyExists(APT_IPTransitObj.APT_IPTransit.snmprwtextfield, "Snmprw");
			sendKeys(APT_IPTransitObj.APT_IPTransit.snmprwtextfield, snmpro2cvalue);

		} else if ((snmp2c.equalsIgnoreCase("no")) && (snmp3.equalsIgnoreCase("yes"))) {
			verifyExists(APT_IPTransitObj.APT_IPTransit.c3radiobutton, "SNMP Version-3");
			click(APT_IPTransitObj.APT_IPTransit.c3radiobutton, snmp3);

			verifyExists(APT_IPTransitObj.APT_IPTransit.snmpv3username, "Snmpro");
			sendKeys(APT_IPTransitObj.APT_IPTransit.snmpv3username, Snmpv3Usernamevalue);

			verifyExists(APT_IPTransitObj.APT_IPTransit.snmpv3authpassword, "Snmp v3 Auth password");
			sendKeys(APT_IPTransitObj.APT_IPTransit.snmpv3authpassword, Snmpv3Authpasswordvalue);

			verifyExists(APT_IPTransitObj.APT_IPTransit.snmpv3privpassword, "Snmp v3 Auth password");
			sendKeys(APT_IPTransitObj.APT_IPTransit.snmpv3privpassword, Snmpv3Privpasswordvalue);
		}
		

		verifyExists(APT_IPTransitObj.APT_IPTransit.countryinput, "Country dropdown");
		selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.countryinput, Country, "Country");
		// select(APT_IPTransitObj.APT_IPTransit.countryinput,Country);

		if (existingcity.equalsIgnoreCase("no") & newcity.equalsIgnoreCase("yes")) {
			verifyExists(APT_IPTransitObj.APT_IPTransit.addcityswitch, "city switch");
			click(APT_IPTransitObj.APT_IPTransit.addcityswitch, "city switch");

			verifyExists(APT_IPTransitObj.APT_IPTransit.citynameinputfield, "City Name");
			sendKeys(APT_IPTransitObj.APT_IPTransit.citynameinputfield, cityname, "City Name");

			verifyExists(APT_IPTransitObj.APT_IPTransit.citycodeinputfield, "City Code");
			sendKeys(APT_IPTransitObj.APT_IPTransit.citycodeinputfield, Citycode, "City Code");

			verifyExists(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addCityToggleSelected, "Site Name");
			sendKeys(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addCityToggleSelected, sitename, "Site Name");

			verifyExists(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addCityToggleSelected, "Site Code");
			sendKeys(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addCityToggleSelected, sitecode, "Site Code");

			verifyExists(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addCityToggleSelected, "Premise Name");
			sendKeys(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addCityToggleSelected, premisename,
					"Premise Name");

			verifyExists(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addCityToggleSelected, "Premise Code");
			sendKeys(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addCityToggleSelected, premisecode,
					"Premise Code");
		} else if (existingcity.equalsIgnoreCase("yes") & newcity.equalsIgnoreCase("no")) {
			verifyExists(APT_IPTransitObj.APT_IPTransit.citydropdowninput, "City dropdown");
			// addDropdownValues_commonMethod("Country",APTIPAccessNoCPE.APTNoCPE.countryinput,Country);
			selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.citydropdowninput, existingcityvalue, "City");
			// select(APT_IPTransitObj.APT_IPTransit.citydropdowninput,existingcityvalue);

			if (existingsite.equalsIgnoreCase("yes") & newsite.equalsIgnoreCase("no")) {
				verifyExists(APT_IPTransitObj.APT_IPTransit.sitedropdowninput, "Site");
				selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.sitedropdowninput, existingsitevalue, "Site");

				// addDropdownValues_commonMethod("Country",APTIPAccessNoCPE.APTNoCPE.sitedropdowninput,existingsitevalue);
				// select(APT_IPTransitObj.APT_IPTransit.sitedropdowninput,existingsitevalue);

				if (existingpremise.equalsIgnoreCase("yes") & NewPremise.equalsIgnoreCase("no")) {
					verifyExists(APT_IPTransitObj.APT_IPTransit.premisedropdowninput, "Premise");
					addDropdownValues_commonMethod("Premise", APTIPAccessNoCPE.APTNoCPE.premisedropdowninput,
							existingpremisevalue);
					}

				else if (existingpremise.equalsIgnoreCase("no") & NewPremise.equalsIgnoreCase("yes")) {
					verifyExists(APT_IPTransitObj.APT_IPTransit.addpremiseswitch, "premise switch");
					click(APT_IPTransitObj.APT_IPTransit.addpremiseswitch, "premise switch");

					verifyExists(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addPremiseToggleSelected,
							"Premise name");
					sendKeys(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addPremiseToggleSelected, premisename,
							"Premise name");

					verifyExists(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addPremiseToggleSelected,
							"Premise Code");
					sendKeys(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addPremiseToggleSelected, premisecode,
							"Premise Code");
				}
			}

			else if (existingsite.equalsIgnoreCase("no") & newsite.equalsIgnoreCase("yes")) {
				verifyExists(APT_IPTransitObj.APT_IPTransit.addsiteswitch, "site switch");
				click(APT_IPTransitObj.APT_IPTransit.addsiteswitch, "site switch");

				verifyExists(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addSiteToggleSelected, "Premise name");
				sendKeys(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addSiteToggleSelected, sitename,
						"Site Name");

				verifyExists(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addSiteToggleSelected, "Site Code");
				sendKeys(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addSiteToggleSelected, sitecode,
						"Site Code");

				verifyExists(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addSiteToggleSelected,
						"Premise name");
				sendKeys(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addSiteToggleSelected, premisename,
						"Premise name");

				verifyExists(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addSiteToggleSelected,
						"Premise Code");
				sendKeys(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addSiteToggleSelected, premisecode,
						"Premise Code");
			}
		}
		verifyExists(APT_IPTransitObj.APT_IPTransit.Next_Button, "Next Button");
		click(APT_IPTransitObj.APT_IPTransit.Next_Button, "Next Button");

	}

	public void verifyEditDevice_PE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editVendorModel");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editManagementAddress");

		String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
		String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCity");
		String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewCity");
		String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityName");
		String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityCode");
		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteName");
		String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteCode");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseName");
		String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseCode");
		String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCityValue");
		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSite");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewSite");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSiteValue");
		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremise");
		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewPremise");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremiseValue");

		Reporter.log("Verifying Edit PE Device Functionality");

		if (isClickable(APT_IPTransitObj.APT_IPTransit.existingdevicegrid)) {
			verifyExists(APT_IPTransitObj.APT_IPTransit.viewservicepage_editdevicelink, "Edit");
			click(APT_IPTransitObj.APT_IPTransit.viewservicepage_editdevicelink, "Edit");

			waitForAjax();

			verifyExists(APT_IPTransitObj.APT_IPTransit.editdeviceheader, "City");
			String editPEDevice = getTextFrom(APT_IPTransitObj.APT_IPTransit.editdeviceheader, "City");
			String EditPEDevice = "Edit PE Device";
			editPEDevice.equalsIgnoreCase(EditPEDevice);

			verifyExists(APT_IPTransitObj.APT_IPTransit.nametextfield, "Name");
			sendKeys(APT_IPTransitObj.APT_IPTransit.nametextfield, editDevicename, "Name");

			verifyExists(APT_IPTransitObj.APT_IPTransit.vendormodelinput, "Vendor/Model");
			select(APT_IPTransitObj.APT_IPTransit.vendormodelinput, editVendorModel);

			verifyExists(APT_IPTransitObj.APT_IPTransit.managementaddresstextbox, "Name");
			sendKeys(APT_IPTransitObj.APT_IPTransit.managementaddresstextbox, editManagementAddress, "Name");

			verifyExists(APT_IPTransitObj.APT_IPTransit.countryinput, "Vendor/Model");
			select(APT_IPTransitObj.APT_IPTransit.countryinput, editCountry);

			if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
				verifyExists(APT_IPTransitObj.APT_IPTransit.addcityswitch, "add city switch");
				click(APT_IPTransitObj.APT_IPTransit.addcityswitch, "add city switch");

				verifyExists(APT_IPTransitObj.APT_IPTransit.citynameinputfield, "Name");
				sendKeys(APT_IPTransitObj.APT_IPTransit.citynameinputfield, editNewCityName, "Name");

				verifyExists(APT_IPTransitObj.APT_IPTransit.citycodeinputfield, "CityCode");
				sendKeys(APT_IPTransitObj.APT_IPTransit.citycodeinputfield, editNewCityCode, "CityCode");

				verifyExists(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addCityToggleSelected, "Site Name");
				sendKeys(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addCityToggleSelected, editNewSiteName,
						"Site Name");

				verifyExists(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addCityToggleSelected, "SiteCode");
				sendKeys(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addCityToggleSelected, editNewSiteCode,
						"SiteCode");

				verifyExists(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addCityToggleSelected,
						"Premise Name");
				sendKeys(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
						"Premise Name");

				verifyExists(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addCityToggleSelected,
						"Premise Name");
				sendKeys(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
						"Premise Code");
			}

			else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
				verifyExists(APT_IPTransitObj.APT_IPTransit.citydropdowninput, "City");
				select(APT_IPTransitObj.APT_IPTransit.citydropdowninput, editExistingCityValue);

				if (editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
					verifyExists(APT_IPTransitObj.APT_IPTransit.sitedropdowninput, "Site");
					select(APT_IPTransitObj.APT_IPTransit.sitedropdowninput, editExistingSiteValue);

					if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
						verifyExists(APT_IPTransitObj.APT_IPTransit.premisedropdowninput, "City");
						select(APT_IPTransitObj.APT_IPTransit.premisedropdowninput, editExistingPremiseValue);
					}

					else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {
						verifyExists(APT_IPTransitObj.APT_IPTransit.addpremiseswitch, "add premise switch");
						click(APT_IPTransitObj.APT_IPTransit.addpremiseswitch, "add premise switch");

						verifyExists(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addPremiseToggleSelected,
								"Premise Name");
						sendKeys(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addPremiseToggleSelected,
								editNewPremiseName, "Premise Name");

						verifyExists(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addPremiseToggleSelected,
								"Premise Code");
						sendKeys(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addPremiseToggleSelected,
								editNewPremiseCode, "Premise Code");
					}
				}

				else if (editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {
					verifyExists(APT_IPTransitObj.APT_IPTransit.addsiteswitch, "site switch");
					click(APT_IPTransitObj.APT_IPTransit.addsiteswitch, "site switch");

					verifyExists(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addCityToggleSelected, "Site Name");
					sendKeys(APT_IPTransitObj.APT_IPTransit.sitenameinputfield_addCityToggleSelected, editNewSiteName,
							"Site Name");

					verifyExists(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addCityToggleSelected, "SiteCode");
					sendKeys(APT_IPTransitObj.APT_IPTransit.sitecodeinputfield_addCityToggleSelected, editNewSiteCode,
							"SiteCode");

					verifyExists(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addCityToggleSelected,
							"Premise Name");
					sendKeys(APT_IPTransitObj.APT_IPTransit.premisenameinputfield_addCityToggleSelected,
							editNewPremiseName, "Premise Name");

					verifyExists(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addCityToggleSelected,
							"Premise Name");
					sendKeys(APT_IPTransitObj.APT_IPTransit.premisecodeinputfield_addCityToggleSelected,
							editNewPremiseCode, "Premise Code");
				}

			}
		}

		else if (editCountry.equalsIgnoreCase("Null")) {
			Reporter.log("No changes made for 'Country' dropdown");

			CreateFirewall.editCity(editExistingCity, editNewCity, "citydropdowninput", "selectcityswitch",
					"addcityswitch", editExistingCityValue, editNewCityName, editNewCityCode, "City");

			// Site
			CreateFirewall.editSite(editExistingSite, editNewSite, "sitedropdowninput", "selectsiteswitch",
					"addsiteswitch", editExistingSiteValue, editNewSiteName, editNewSiteCode, "Site");

			// Premise
			CreateFirewall.editPremise(editExistingPremise, editNewPremise, "premisedropdowninput",
					"selectpremiseswitch", "addpremiseswitch", editExistingPremiseValue, editNewPremiseName,
					editNewPremiseCode, "Premise");
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	

	public void verify_JuniperVendor_EditInterface(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");

		/*String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DeviceName");
		String edit_eipallocation1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"edit_eipallocation1");
		String edit_eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String edit_eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EIPAllocation_Subnetsize");
		String edit_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_GetAddress");
		String edit_interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceAddressRange_value");
		String edit_eipallocation2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EIPAllocation2");
		String edit_eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_EIPAllocation_IPv6_Subnetsize");
		String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IPv6_GetAddress");
		String edit_interfaceaddressrangeIPv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_InterfaceAddressRangeIPv6_value");
		*/String edit_linkvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_LinkValue");
		//String edit_network = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_Network");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_value");
		String edit_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Bandwidth_value");
		String edit_cardtype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_CardType_DropdownValue");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EncapsulationValue");
		String edit_framingtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_FramingTypeValue");
		String edit_clocksource_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ClockSourceValue");
		String edit_STM1Number_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_STM1NumberValue");
		String edit_bearerNo_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerNumber_Value");
		String edit_unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_UnitIDValue");
		String edit_slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_SlotValue");
		String edit_pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PicValue");
		String edit_port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_PortValue");
		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLANIDvalue");
		String edit_juniper_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_Juniper_ConfigInterface_checkbox");

		Reporter.log("Verifying Edit Interface Functionality - Juniper");

		if (isClickable(APT_IPTransitObj.APT_IPTransit.existingdevicegrid1)) {
			if (isClickable(APT_IPTransitObj.APT_IPTransit.existingdevicegrid1)) {				

				scrollDown(APT_IPTransitObj.APT_IPTransit.existingdevicegrid1);
				waitForAjax();
				webDriver.findElement(By.xpath(
						"//tr//*[contains(text(),'" + InterfaceName + "')]//following-sibling::td//a[text()='Edit']"))
						.click();
				// verifyExists(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");
				// click(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.editinterface_header)) {
					Reporter.log("Edit Interface' page navigated as expected");

					scrollDown(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
							"Configure Interface on Device");
					click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, edit_linkvalue, "Link");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, "Bearer Type");
					selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, edit_bearertype_value,
							"Bearer Type");

					if (edit_bearertype_value.equalsIgnoreCase("10Gigabit Ethernet")
							|| edit_bearertype_value.equalsIgnoreCase("Gigabit Ethernet")
							|| edit_bearertype_value.equalsIgnoreCase("Gigabit Ethernet")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								"Bandwidth");

						//verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						//selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								//"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value,
								"Framing Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, edit_clocksource_value,
								"Clock Source");

						// verifyExists(APTIPAccessNoCPE.APTNoCPE.timeslot,
						// "Timeslot");
						// sendKeys(APTIPAccessNoCPE.APTNoCPE.timeslot,
						// timeslot,
						// "Timeslot");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");

					} else if (edit_bearertype_value.equalsIgnoreCase("E1")) {
						waitForAjax();
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						//selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								//"Bandwidth");

						//verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						//selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								//"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						//selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value,
								//"Framing Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, edit_clocksource_value,
								"Clock Source");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
					} else if (edit_bearertype_value.equalsIgnoreCase("E3")
							|| edit_bearertype_value.equalsIgnoreCase("T3")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						//selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								//"Bandwidth");

						//verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						//selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								//"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
					} else if (edit_bearertype_value.equalsIgnoreCase("STM-1")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								"Bandwidth");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						select(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, edit_encapsulation_value);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, "Unit ID");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, edit_unitid_value, "Unit ID");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_textfield, "Slot");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.slot_textfield, edit_slot_value, "Slot");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_textfield, "Pic");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.pic_textfield, edit_pic_value, "Pic");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.port_textfield, "Port");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.port_textfield, edit_port_value, "Port");

					String edit_Juniper_InterfaceName = getAttributeFrom(
							APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, "value");
					if (edit_encapsulation_value.equalsIgnoreCase("802.1q")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, edit_vlanID_value, "VLAN Id");
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
					click(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
					click(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");

					// verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox,
					// "BGP");
					// click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

					// configuration panel in edit interface page
					if (edit_juniper_configureinterface_checkbox.equalsIgnoreCase("Yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_header, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_textarea, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

						Alert alert = webDriver.switchTo().alert();

						// Capturing alert message.
						String alertMessage = webDriver.switchTo().alert().getText();
						if (alertMessage.isEmpty()) {
							Reporter.log("No message displays");

						} else {
							Reporter.log("Alert message displays as: " + alertMessage);

						}
						alert.accept();
					} else {
						ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.okbutton);
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					}
					Reporter.log("Interface successfully updated.");
				} else {
					Reporter.log("Edit Interface' page not navigated");

				}
			} else {
				Reporter.log("Interface is not added");
			}
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void deleteDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException, AWTException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");
		  if(isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid))
		  {
			  webDriver.findElement(By.xpath("//tr//*[contains(.,'"+name+"')]//following-sibling::span//a[contains(.,'Delete from Service')]")).click();
					
				    Robot r = new Robot();
					r.keyPress(KeyEvent. VK_ENTER);
					r.keyRelease(KeyEvent. VK_ENTER);
				    
					verifyExists(APTIPAccessNoCPE.APTNoCPE.PEDevicedeletionsuccessmessage,"Delete success message");
				} else
			{
			  Reporter.log("No Device added in grid");
			}

		/*if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			String DeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ExistingDeviceName");
			List<WebElement> webElements = findWebElements(APTIPAccessNoCPE.APTNoCPE.addeddevicesList);
			// String
			// addeddevicesList=findWebElements(APTIPAccessNoCPE.APTNoCPE.addeddevicesList);
			int addeddevicesLists = getXPathCount(APTIPAccessNoCPE.APTNoCPE.addeddevicesList1);

			for (int i = 0; i < addeddevicesLists; i++) {

				String AddedDeviceNameText = webElements.get(i).getText();
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);
				if (AddedDeviceNameText.contains(DeviceName)) {
					String AddedDevice_DeletefromserviceLink = APTIPAccessNoCPE.APTNoCPE.deleteDevice1 + AddedDevice_SNo
							+ APTIPAccessNoCPE.APTNoCPE.deleteDevice2;
					click(AddedDevice_DeletefromserviceLink);

					Reporter.log("Device successfully removed from service.");
				} else {
					Reporter.log("Invalid device name");
				}
			}
		} else {
			Reporter.log("No Device added in grid");
		}*/
	}

	public void addExistingPEDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingDeviceName");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.providerequipment_header, "provide requipment header");
		String providerequipment = getTextFrom(APTIPAccessNoCPE.APTNoCPE.providerequipment_header, "provide requipment header");
		String provideRequipment = "Provider Equipment (PE)";
		providerequipment.equalsIgnoreCase(provideRequipment);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "addpe device");
		click(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "addpe device");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.addpedevice_header)) {
			Reporter.log("Add PE Device' page navigated as expected");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown, "typepe name dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown, "typepe name dropdown");
			
			addDropdownValues_commonMethod("Type PE name to filter", APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,
					existingdevicename);
			waitForAjax();
			click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown, "typepe name dropdown");
			webDriver.findElement(By.xpath("(//div[label[text()='Type PE name to filter']]//span[contains(text(),'"
					+ existingdevicename + "')])[1]")).click();

			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_vendormodelvalue,
					"existing device vendor model value");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_managementaddressvalue,
					"existing device management addressvalue");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_connectivityprotocol,
					"existing device connectivi typrotocol");

			String SNMPVersionValue = APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpversion;

			if (SNMPVersionValue.equalsIgnoreCase("2c")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpro, "existing device snmpro");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmprw, "existing device snmprw");
			}

			else if (SNMPVersionValue.equalsIgnoreCase("3")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3username,
						"existing device snmp v3 username");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3authpassword,
						"existing device snmp v3 auth password");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3privpassword,
						"existingdevice snmp v3 priv password");

			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpversion, "existing device snmp version");

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_country, "Country");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_city, "City");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_site, "Site");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_premise, "Premise");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");
			click(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.successmsg, "Add Device success msg");
			String successMsg = getTextFrom(APTIPAccessNoCPE.APTNoCPE.successmsg, "Add Device success msg");
			String SuccessMsg = "Device is added to Service.";

			successMsg.equalsIgnoreCase(SuccessMsg);

		} else {
			Reporter.log("Add PE Device' page not navigated");

		}

	}

	public void selectInterfacelinkforDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String editname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");
		
		scrollDown(APT_IPTransitObj.APT_IPTransit.back);
		click(APT_IPTransitObj.APT_IPTransit.back,"Bread Crumb");
		waitForAjax();
		Reporter.log("check 'Select Interface' link");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			waitForAjax();

			webDriver.findElement(By.xpath(
					"//tr//*[contains(.,'"+editname+"')]//following-sibling::span//a[contains(.,'Select Interfaces')]"))
					.click();
			
			waitForAjax();
		
		} else {
			Reporter.log("No Device added in grid");

		}

	}

	public void SelectInterfacetoremovefromservice(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Edit_InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");
		waitForAjax();
		waitForAjax();
		waitForAjax();
		waitToPageLoad();

		scrollDown(APTIPAccessNoCPE.APTNoCPE.interfacesinservice_filter);
		waitForAjax();
		mouseMoveOn(APTIPAccessNoCPE.APTNoCPE.interfacesinservice_filter);
		click(APTIPAccessNoCPE.APTNoCPE.interfacesinservice_filter, "Interfaces in Service Filter");

		if (Edit_InterfaceName.equalsIgnoreCase("null")) {
			
			webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + InterfaceName + "'])[1]"))
					.click();
			// verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_grid+InterfaceName+APTIPAccessNoCPE.APTNoCPE.interfaceinservice_grid1);
			waitForAjax();
			// click(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_grid+InterfaceName+APTIPAccessNoCPE.APTNoCPE.interfaceinservice_grid1);
		} else

		{
			verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_fitertext, "Interface in Service search");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_fitertext, Edit_InterfaceName,
					"Interface in Service search");

			String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect,"interface inservice_gridselect")
					.replace("value", Edit_InterfaceName);
			click(CheckToAddInterface,"Check To Add Interface");

		}
		waitForAjax();
		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_Actiondropdown, "Action Dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_Actiondropdown, "Action Dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_removebuton, "Remove");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_removebuton, "Remove");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.RemoveSelectInterfaceSuccessMessage, "Remove Interface Success Message");
	}

	public void SelectInterfacetoaddwithservcie(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Edit_InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.InteraceColumn_Filter);
		waitForAjax();
		mouseOverAction(APTIPAccessNoCPE.APTNoCPE.InteraceColumn_Filter);
		click(APTIPAccessNoCPE.APTNoCPE.InteraceColumn_Filter, "Interfaces To Select Filter");

		if (Edit_InterfaceName.equalsIgnoreCase("null")) {
			// verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt,"Interface
			// search");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt,InterfaceName,"Interface
			// search");
			webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + InterfaceName + "'])[1]"))
					.click();

			// verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacetoservice_grid+InterfaceName+APTIPAccessNoCPE.APTNoCPE.interfacetoinservice_grid1);
			waitForAjax();
			// click(APTIPAccessNoCPE.APTNoCPE.interfacetoservice_grid+InterfaceName+APTIPAccessNoCPE.APTNoCPE.interfacetoinservice_grid1);
			// String CheckToAddInterface=
			// getTextFrom(APTIPAccessNoCPE.APTNoCPE.interface_gridselect).replace("value",
			// InterfaceName);
			// click(CheckToAddInterface);
		} else

		{
			verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt, "Interface search");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt, Edit_InterfaceName, "Interface search");

			String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interface_gridselect,"interface_gridselect").replace("value",
					Edit_InterfaceName);
			click(CheckToAddInterface,"Check To Add Interface");

		}
		waitForAjax();
		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_Actiondropdown, "Action Dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_Actiondropdown, "Action Dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_addbuton, "Add");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_addbuton, "Add");

		scrollDown(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
		click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
	}
	
	public void createorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingOrderService");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingOrderNumber");
		
		ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.nextbutton);
		
		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		
		verifyExists(APTIPAccessNoCPE.APTNoCPE.order_contractnumber_warngmsg,"order contract number");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicetype_warngmsg,"service type");
		
		if (neworder.equalsIgnoreCase("YES"))
		{
			verifyExists(APTIPAccessNoCPE.APTNoCPE.newordertextfield,"new order textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.newordertextfield,newordernumber,"newordertextfield");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.newrfireqtextfield,"new rfireq textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.newrfireqtextfield,newrfireqno,"new rfireq textfield");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.createorderbutton,"create order button");
			click(APTIPAccessNoCPE.APTNoCPE.createorderbutton,"create order button");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.OrderCreatedSuccessMsg,"Order Created Success Msg");
			
		}
		else if (existingorderservice.equalsIgnoreCase("YES"))
		{
			verifyExists(APTIPAccessNoCPE.APTNoCPE.selectorderswitch,"select order switch");
			click(APTIPAccessNoCPE.APTNoCPE.selectorderswitch,"select order switch");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,"existing order drop down");
			select(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber);	
		}
		else
		{
			Reporter.log("Order not selected");
		}		
	
	}
	
	public void verifyServiceCreation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String BillingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BillingTypevalue");
		String TerminationDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TerminationDate");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserEmail");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PhoneContact");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
	
	
		waitForAjax();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
			click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.sidwarngmsg,"Service Identification");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_warngmsg,"Billing Type");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield,"Service Identification");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield,ServiceIdentification,"Service Identification");
			
			waitForAjax();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,"Billing Type");
			//click(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,"Billing Type");		
			
			selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, BillingType, "Billing Type");
		
			verifyExists(APTIPAccessNoCPE.APTNoCPE.terminationdate_field,"Termination Date");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.terminationdate_field,TerminationDate,"Termination Date");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service,"Email Service");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service,EmailService,"Email Service");
			click(APTIPAccessNoCPE.APTNoCPE.emailarrow,"Email arrow");			
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield,"Phone Service");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield,PhoneService,"Phone Service");
			click(APTIPAccessNoCPE.APTNoCPE.phonearrow,"Phone arrow");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.remarktextarea,"Remarks");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.remarktextarea,Remarks,"Remarks");
		
			scrollDown(APTIPAccessNoCPE.APTNoCPE.nextbutton);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Nextbutton");
			click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Nextbutton Service");
			
			String serviceCreated = "Service successfully created";
			String orderService=getTextFrom(APTIPAccessNoCPE.APTNoCPE.servicecreationmessage,"Service successfully created");
			serviceCreated.equalsIgnoreCase(orderService);
			Reporter.log("Service successfully created");
			
		
}
	
	public void verifyCustomerDetailsInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		
			String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomerCreation");
			String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingCustomerSelection");
			String newCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
			String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
			String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
			String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");
			String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
			String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
			String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
			String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingCustomer");
			String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");

			if(newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) 
			{
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Name_Value,"Name Value");
				String nameValue=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Name_Value,"Name Value");
				nameValue.equalsIgnoreCase(newCustomer);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Country_Value,"Country");
				String countryValue=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Country_Value,"Country");
				countryValue.equalsIgnoreCase(country);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.OCN_Value,"OCN");
				String OCN=getTextFrom(APTIPAccessNoCPE.APTNoCPE.OCN_Value,"OCN");
				OCN.equalsIgnoreCase(ocn);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Reference_Value,"Reference");
				String Reference=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Reference_Value,"Reference");
				Reference.equalsIgnoreCase(reference);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.TechnicalContactName_Value,"Technical Contact Name");
				String technicalContactName=getTextFrom(APTIPAccessNoCPE.APTNoCPE.TechnicalContactName_Value,"Technical Contact Name");
				technicalContactName.equalsIgnoreCase(technicalcontactname);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Type_Value,"Type");
				String Type=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Type_Value,"Type");
				Type.equalsIgnoreCase(type);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Email_Value,"Email");
				String Email=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Email_Value,"Email");
				Email.equalsIgnoreCase(email);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Phone_Value,"Phone");
				String Phone=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Phone_Value,"Phone");
				Phone.equalsIgnoreCase(phone);
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Fax_Value,"Fax");
				String Fax=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Fax_Value,"Fax");
				Fax.equalsIgnoreCase(fax);
			}
			else if(newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes"))
			{
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Name_Value,"Fax");
				String ExistingCustomer=getTextFrom(APTIPAccessNoCPE.APTNoCPE.Name_Value,"Name");
				ExistingCustomer.equalsIgnoreCase(existingCustomer);
			}
				//Main Domain
			if(maindomain.equalsIgnoreCase("Null"))
			{
				Reporter.log("A default displays for main domain field, if no provided while creating customer");
		
			}
			else
			{
				verifyExists(APTIPAccessNoCPE.APTNoCPE.MainDomain_Value,"Main Domain");
				String Maindomain=getTextFrom(APTIPAccessNoCPE.APTNoCPE.MainDomain_Value,"Main Domain");
				Maindomain.equalsIgnoreCase(maindomain);
			}
			Reporter.log("Customer Details panel fields Verified");
		}
	
	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_VoicelineNumber");

		if(editOrderSelection.equalsIgnoreCase("no"))
		{
			Reporter.log("Edit Order is not performed");
	    }
		else if(editOrderSelection.equalsIgnoreCase("Yes"))
		{
			Reporter.log("Performing Edit Order Functionality");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"Action dropdown");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderlink,"Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderlink,"Edit Order");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderheader,"Edit Order");

			String EditOrderNo=getTextFrom(APTIPAccessNoCPE.APTNoCPE.editorderno,"Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderno,"edit order no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editorderno);
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderno,"Order Number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno,editorderno,"Order Number");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editvoicelineno,"edit voice line no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editvoicelineno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editvoicelineno,editvoicelineno,"Order Number");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.cancelbutton,"cancel button");
			click(APTIPAccessNoCPE.APTNoCPE.cancelbutton,"cancel button");
		
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"Action dropdown");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderlink,"Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderlink,"Edit Order");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderheader,"Edit Order Header");
			String editOrderHeader=getTextFrom(APTIPAccessNoCPE.APTNoCPE.editorderheader,"Edit Order Header");
			String EditOrder= "Edit Order";
			editOrderHeader.equalsIgnoreCase(EditOrder);
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderno,"edit order no");
			click(APTIPAccessNoCPE.APTNoCPE.editorderno,"edit order no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editorderno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno,editorderno,"Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editvoicelineno,"edit voice line no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editvoicelineno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editvoicelineno,editvoicelineno,"Order Number");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorder_okbutton,"OK Button");
			click(APTIPAccessNoCPE.APTNoCPE.editorder_okbutton,"OK Button");
			
			if(editorderno.equalsIgnoreCase("Null"))
			{
				Reporter.log("Order/Contract Number (Parent SID) field is not edited");
			}
			else 
			{
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue,"order number value");
				String editOrderno=getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue,"order number value");
				editOrderno.equalsIgnoreCase(editorderno);
			}
			
			if(editvoicelineno.equalsIgnoreCase("Null"))
			{
				Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
			}
			else
			{
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue,"order voice line number value");
				String editVoicelineno=getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue,"order voice line number value");
				editVoicelineno.equalsIgnoreCase(editvoicelineno);
			}
			Reporter.log("Edit Order is successful");	

		}

      }
	
	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_newOrderNumber");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_existingOrderNumber");

		if((changeOrderSelection_newOrder.equalsIgnoreCase("No")) && (changeOrderSelection_existingOrder.equalsIgnoreCase("No")))
		{
			Reporter.log("Change Order is not performed");	
	
		}
		else if(changeOrderSelection_newOrder.equalsIgnoreCase("Yes"))
		{
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"Action dropdown");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderlink,"change order link");
			click(APTIPAccessNoCPE.APTNoCPE.changeorderlink,"change order link");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderheader,"change order header");
			String changeOrderHeader=getTextFrom(APTIPAccessNoCPE.APTNoCPE.changeorderheader,"change order header");
			String changeOrder = "Change Order";
			changeOrderHeader.equalsIgnoreCase(changeOrder);
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_selectorderswitch,"select order switch");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_selectorderswitch,"select order switch");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeordernumber,"change order number");
			click(APTIPAccessNoCPE.APTNoCPE.changeordernumber,"change order number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno,ChangeOrder_newOrderNumber);
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber,"change order voiceline number");
			click(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber,"change order voiceline number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber,changevoicelineno);
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.createorder_button,"create order button");
			click(APTIPAccessNoCPE.APTNoCPE.createorder_button,"create order button");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton,"change order ok button");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton,"change order ok button");
					
			
			Reporter.log("Change Order is successful");	
		}
		else if(changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) 
		{
			Reporter.log("Performing Change Order functionality");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"order action button");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton,"order action button");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderlink,"change order link");
			click(APTIPAccessNoCPE.APTNoCPE.changeorderlink,"change order link");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderheader,"change order");
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_chooseorderdropdown,"Order/Contract Number (Parent SID)");
			select(APTIPAccessNoCPE.APTNoCPE.changeorder_chooseorderdropdown,ChangeOrder_existingOrderNumber);
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton,"change order ok button");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton,"change order ok button");
			

			Reporter.log("Change Order is successful");

		}
		
	}
	
	public void verifyManageService(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String serviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String servicestatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceStatus");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceStatusChangeRequired");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown,"service action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown,"service action dropdown");
		
		verifyExists(APTIPAccessNoCPE.APTNoCPE.manageLink,"manage");
		click(APTIPAccessNoCPE.APTNoCPE.manageLink,"manage");
		
		verifyExists(APTIPAccessNoCPE.APTNoCPE.manageservice_header,"manage service");
		
		verifyExists(APTIPAccessNoCPE.APTNoCPE.status_servicename,"space name");
		String sid=getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_servicename,"space name");
		sid.equalsIgnoreCase(Sid);
		
		verifyExists(APTIPAccessNoCPE.APTNoCPE.status_servicetype,"space name");
		String servicetype=getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_servicetype,"space type");
		servicetype.equalsIgnoreCase(serviceType);

		String ServiceDetails_value=getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_servicedetails,"status_servicedetails");
		
		if(isValueEmpty(ServiceDetails_value))
		{
			Reporter.log("Service Details column value is empty as expected");

		}
		else
		{
			Reporter.log("Service Details column value should be empty");
		}
		
		String serviceStatus=getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_currentstatus,"Current status");
		serviceStatus.equalsIgnoreCase(servicestatus);

		String LastModificationTime_value=getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_modificationtime,"status_modificationtime");
		
		if(LastModificationTime_value.contains("BST"))
		{
			Reporter.log("Service status is displayed as");

		}
		
		else
		{
			Reporter.log("Incorrect modification time format");

		}
		verifyExists(APTIPAccessNoCPE.APTNoCPE.statuslink,"Status");
		click(APTIPAccessNoCPE.APTNoCPE.statuslink,"Status");
		
		if(servicestatuschangerequired=="Yes")
		{

			if(isClickable(APTIPAccessNoCPE.APTNoCPE.Servicestatus_popup))
			{
				verifyExists(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdown,"change status");
				click(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdown,"change status");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdownvalue,"change status");
				click(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdownvalue,"change status");
				
				verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton,"ok");
				click(APTIPAccessNoCPE.APTNoCPE.okbutton,"ok");
				
				if(isClickable(APTIPAccessNoCPE.APTNoCPE.servicestatushistory))
				{
					Reporter.log("Service status change request logged");
				}
				else
				{
					Reporter.log(" Service status change request is not logged");
				}	
			}
			else
			{
				Reporter.log(" Status link is not working");
	
			}
		}
		else
		{
			Reporter.log("Service status change not reqired");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.servicestatus_popupclose,"service status");
			click(APTIPAccessNoCPE.APTNoCPE.servicestatus_popupclose,"service status");
		}
		
		String syncServicename=getTextFrom(APTIPAccessNoCPE.APTNoCPE.sync_servicename,"sync_servicename");
		syncServicename.equalsIgnoreCase(sid);
		
		String syncServiceType=getTextFrom(APTIPAccessNoCPE.APTNoCPE.sync_servicetype,"sync_servicetype");
		syncServiceType.equalsIgnoreCase(serviceType);
		
		if(isEmptyOrNull(APTIPAccessNoCPE.APTNoCPE.sync_servicedetails))
		{
			Reporter.log("Service Details column value is empty as expected");	
		}
		else
		{
			Reporter.log("Service Details column value should be empty");
		}		
		verifyExists(APT_IPTransitObj.APT_IPTransit.back,"manage page back button");
		click(APT_IPTransitObj.APT_IPTransit.back,"manage page back button");
	}
}
