package pageObjects.siebelObjects;

public class SiebelLiabrary_Obj {

	public static class Login
	{ 
		//Siebel Login Page
		public static final String userNameTxb="@xpath=//input[@name='SWEUserName']"; //Username
		public static final String passWordTxb="@xpath=//input[@name='SWEPassword']"; //Password
		public static final String loginBtn="@xpath=//a[text()='Login']"; //Login Button
		public static final String accountsTab="@xpath=//a[text()='Accounts']"; //Accounts Tab
		public static final String editLayoutBtn="@xpath=//button[@data-display='Edit Layout']"; //Edit Layout Button		
		
		public static final String Settings ="@xpath=//li[@title='Settings']";
		public static final String Logout ="@xpath=//button[text()='Logout']"; //Logout button
		
		public static final String Advanced="@xpath=//*[@id='details-button']"; // added for IP2020
		public static final String Navigate="@xpath=//*[@id='proceed-link']"; // added for IP2020
	
	}
	public static class Accounts
	 { 
		public static final String accountsTab="@xpath=//a[text()='Accounts']";
		public static final String OCNTxb="@xpath=//input[@aria-label='Account Number(OCN)']"; // Account Number field
		public static final String GoBtn="@xpath=//span[text()='Go']";
		public static final String InstalledAssets_NewBtn="@xpath=//button[@title='Installed Assets:New']";
	 }
	public static class CustomerOrderPage
	{	
	
		public static final String DeliveryChannel="@xpath=//*[(@aria-labelledby='COLT_Delivery_Channel_Label')]"; //DeliveryChannel
		public static final String SalesChannel="@xpath=//*[(@aria-labelledby='COLT_Sales_Channel_Label')]"; //SalesChannel

		public static final String contractLabelTxb ="@xpath=//input[@aria-labelledby='COLT_Contract_Label']";
		
		
		
	}
	public static class serviceOrder
	 { 
		public static final String ServiceOrderReferenceNo="@xpath=//a[contains(@name,'Order Num')]";
	
		public static final String ClickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
		public static final String AlertAccept1="@xpath=//button[@id='colt-formalerts-ok-btn']";
		public static final String Spinner="@xpath=(//i[contains(@class,'spinner')])[1]";
	 }
	public static class ServiceTab
	{
		
		public static final String ServiceOrderTab="@xpath=//a[text()='Service Order']";
		public static final String InputServiceOrder="@xpath=//input[@aria-label='Starting with']";
		public static final String ServiceOrderGo="@xpath=(//button[@aria-label='Service Order List:Go'])[2]";
		public static final String ServiceOrderReferenceNo="@xpath=//a[contains(@name,'Order Num')]";
		//
		public static final String BespokeTab="@xpath=//button[@title='Bespoke Order']";
		public static final String BespokeSummary_Popup="@xpath=//div//span[text()='Bespoke Summary']";
		public static final String CutomerView="@xpath=//textarea[@aria-label='Customer View']";
		public static final String ColtView="@xpath=//textarea[@aria-label='Colt View']";
		public static final String BespokePopupClose="@xpath=//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable ui-resizable']//following-sibling::button[@title='Close']";
		public static final String BespokeReferance="@xpath=//input[@aria-labelledby='COLT_Bespoke_Ref_Label']";
		
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String CustOrder="@xpath=//input[@aria-labelledby='Order_Number_Label']";
		public static final String CustOrderGo="@xpath=//button[contains(@class,'gotoview')]";
		public static final String ClickSeibelOrder="@xpath=//a[@name='Order Number']";
		public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]";
		public static final String NetworkRefField ="@xpath=//input[@aria-labelledby='COLT_Network_Reference_Label']";
	
		public static final String ColtMailIdTextbox="@xpath=//input[@aria-label='Enter your email, phone, or Skype.']";
		public static final String Nextbutton="@xpath=//input[@type='submit']";
		public static final String otherUserlink="@xpath=//div[@id='otherTileText']";
		
		public static final String sharepointlink="@xpath=//a[@aria-label='Go to SharePoint']";
		public static final String SharepointDocument="@xpath=//div[@id='ctl00_PlaceHolderMain_ctl01_csr1_groupContent']";
		public static final String popupbox="@xpath=//div[@class='lightbox-cover']";
	}
	public static class ServiceEntry
	{
		public static final String contractLabelTxb ="@xpath=//input[@aria-labelledby='COLT_Contract_Label']";
		public static final String contractLookup="@xpath=//input[@aria-labelledby='COLT_Contract_Label']//following-sibling::span[@aria-label='Selection Field']";
		public static final String popUpFilterQueryLst="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']//following-sibling::span";
		public static final String popUpFilterQuerySubLst="@xpath=//*[contains(@style,'block')]//li[text()='Contract Id']";
		
		public static final String popUpQueryTxb="@xpath=//input[contains(@aria-labelledby,'PopupQuerySrchspec')]";
		public static final String popUpContractQuerySearchBtn="@xpath=//button[@title='Pick Contract:Query']";
		public static final String contractIDTxb="@xpath=//input[@name='Agreement_Number']";
		public static final String contractCityCell="@xpath=//td[contains(@id,'Contracting_City')]";
		public static final String contractCityLst="@xpath=//input[@name='COLT_Contracting_City']//following-sibling::span";
		public static final String contractCitySubLst="@xpath=//*[contains(@style,'block')]//li[text()='Paris']";
		public static final String popUpOKBtn="@xpath=//div[@class='siebui-popup-btm']//..//button[contains(@title,'OK')]";
		public static final String popUpGOBtn="@xpath=//td[@class='siebui-popup-filter']//child::span[@class='siebui-popup-button']//child::button[@data-display='Go']";
		public static final String pickPopUpTable="@xpath=//table[contains(@summary,'Pick ')]";
	
		//Header fields
		public static final String showFullInfoLnk="@xpath=//a[contains(text(),'Click to Show Full info')]";
		public static final String orderProcessingUserLookup="@xpath=//input[@aria-labelledby='Order_Processing_User_Label']//following-sibling::span";
		public static final String orderProcessingOption="@xpath=//*[not(contains(@style,'display'))]//li[text()='Value']";
		//public static final String popUpQueryTxb="@xpath=//input[contains(@aria-labelledby,'PopupQuerySrchspec')]";
		//public static final String popUpGOBtn="@xpath=//td[@class='siebui-popup-filter']//child::span[@class='siebui-popup-button']//child::button[@data-display='Go']";
		public static final String promotionCodeTxb="@xpath=//input[@aria-label='Promotion Code']";
		
		//Middle Applet
		public static final String ossPlatformFlag="@xpath=//span[text()='OSS Platform Flag']/..//input";
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]"; 
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li[text()='Value']";
		public static final String showFullInfoAendLnk="@xpath=//div[@id='colt-site-left']//a[text()='Show full info']";
		public static final String showFullInfoBendLnk ="@xpath=//div[@id='colt-site-right']//a[text()='Show full info']";
		public static final String addressContactLookup="@xpath=//span[text()='Addresses & Contacts']";
		public static final String deliveryCountryTxb ="@xpath=//span[text()='Delivery Team Country']//following-sibling::input";
		public static final String deliveryCityTxb ="@xpath=//span[text()='Delivery Team City']//following-sibling::input";
		public static final String closePopUpBtn ="@xpath=//div[contains(@class,'colt-fullinfo-wrapper')]//..//button[@title='Close']";
	
		//site Details At A end
		public static final String thirdPartyAccessProviderALst="@xpath=//div[@id='colt-site-left']//span[text()='Third Party Access Provider']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]";
		public static final String shelfIDATxb="@xpath=//div[@id='colt-site-left']//span[text()='Shelf ID']//following-sibling::input";
		public static final String slotIDATxb="@xpath=//div[@id='colt-site-left']//span[text()='Slot ID']//following-sibling::input";
		public static final String physicalportIDATxb="@xpath=//div[@id='colt-site-left']//span[text()='Physical Port ID']//following-sibling::input";
		public static final String fibreTypeLst="@xpath=//div[@id='colt-site-left']//span[text()='Fibre Type']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]";
		public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span"; 
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li[text()='Value']";//updated 2020
	
		public static final String PickAccountDropdown ="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
		public static final String AList="@xpath=//*[not(contains(@style,'display'))]//li[text()='Value']"; //Update IP2020
		public static final String PickAccountValue ="@xpath=//div[(@class='ui-dialog-content ui-widget-content')]//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String PickAccountGo ="@xpath=(//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Account:Go'])[2]";
		//public static final String PickAccountOk ="@xpath=//div[contains(@style,'block')]//button[@title='Pick Account:OK']";
		public static final String PickAccountOk ="@xpath=//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Account:OK']";
		public static final String PickContactGo ="@xpath=(//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Contact:Go'])[2]";
		public static final String PickContactOk ="@xpath=//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Contact:OK']";
	
	
		//site Details At B end
		public static final String shelfIDBTxb="@xpath=//div[@id='colt-site-right']//span[text()='Shelf ID']//following-sibling::input";
		public static final String slotIDBTxb="@xpath=//div[@id='colt-site-right']//span[text()='Slot ID']//following-sibling::input";
    	public static final String physicalportIDBTxb="@xpath=//div[@id='colt-site-right']//span[text()='Physical Port ID']//following-sibling::input";
    	public static final String vlanTagIDBTxb="@xpath=//div[@id='colt-site-right']//span[text()='VLAN Tag ID']//following-sibling::input";
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
		
		public static final String refreshBtn="@xpath=//button[contains(@title,'Refresh')]";
		public static final String Billing="@xpath=//a[text()='Billing']";
		public static final String billCustRefOrderTxb="@xpath=//input[contains(@aria-labelledby,'COLT_Customer_Reference')]";
	
		public static final String RouterTypeDropdownAccess="@xpath=(//span[text()='Router Type']/..//span)[3]";
		public static final String SelectRouterTypeDropDownAccess="@xpath=//li[text()='Value']";//Update IP2020
		public static final String Layer3ResillanceDropdownAccess="@xpath=(//span[text()='Layer 3 Resilience']/..//span)[3]";
		public static final String Layer3ResillancePartialSelectDropdownAccess="@xpath=//li[text()='Value']";//Update IP2020
	
		public static final String AlertAccept ="@xpath=//span[text()='Ok']";
		public static final String SaveOrderChanges="@xpath=//a[text()='Click here to save your order changes.']";
		public static final String BillingStartDateAccessIcon="@xpath=//input[@aria-label='Billing Start Date']/following-sibling::*[contains(@class,'siebui-icon-date')]";
	}
	 public static class EnterDateInFooter
		{
			public static final String AlertAccept="@xpath=//span[text()='Ok']";
		
			public static final String OrderDates="@xpath=//a[text()='Order Dates']";
			public static final String OrderSignedDate="@xpath=//input[contains(@aria-label,'Order Signed Date')]";
			public static final String CustomerRequestedDate="@xpath=//input[@aria-labelledby='Customer_Requested_Date1_Label']";
			public static final String ColtActualDate="@xpath=//input[@aria-labelledby='COLT_Actual_RFS_Date_Label']";
			public static final String OrderReceivedDate="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']";
			public static final String ColtPromissday="@xpath=//input[@aria-labelledby='Colt_Promised_Date_Label']";

			public static final String OrderSignedDateIcon="@xpath=//input[contains(@aria-label,'Order Signed Date')]/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String CustomerRequestedDateIcon="@xpath=//input[@aria-labelledby='Customer_Requested_Date1_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String ColtActualDateIcon="@xpath=//input[@aria-labelledby='COLT_Actual_RFS_Date_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String OrderReceivedDateIcon="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String ColtPromissdayIcon="@xpath=//input[@aria-labelledby='Colt_Promised_Date_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";

			public static final String TodayDate="@xpath=//*[contains(@class,'today')]";
			
			public static final String CalendarNowBtn="@xpath=//button[text()='Now']";
			
			
		}
	 public static class AlertPopUp
		{
			public static final String AlertAccept="@xpath=//span[text()='Ok']";
		
		
		
		}
}
