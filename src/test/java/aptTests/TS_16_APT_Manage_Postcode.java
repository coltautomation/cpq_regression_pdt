package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.ManagePostcode;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_16_APT_Manage_Postcode extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	APT_Login Login=new APT_Login();
	ManagePostcode MngPostcode = new ManagePostcode();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void Manage_Postcode(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)throws IOException
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
	try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
	{

        // load a properties file
        prop.load(input);               

    }
	catch (IOException ex) 
	{
        ex.printStackTrace();
    }


	String dataFileName = prop.getProperty("TestDataSheet");
	File path = new File("./TestData/"+dataFileName);
	String testDataFile = path.toString();
	
	
	try
	{
		openurl(Configuration.APT_URL);
		Login.APT_VoiceService();
		MngPostcode.selectColtnetwork();
		MngPostcode.verifyManagePostcodepage();
		MngPostcode.verifyManagePostcodeinternal(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.addPostcode(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.fillAddPostCode(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.verifyPostcodevalues(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.editPostcode(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=======createFileForUploadupdateFile", "PASS");
		MngPostcode.createFileForUploadupdateFile(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.verifyUploadupdatefile(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.deleteFile_UploadupdateFile(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=======createFileForAddEmergencyNumber", "PASS");
		MngPostcode.createFileForAddEmergencyNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.verifyAddEmergencyNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.deleteFile_AddEmergencyNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=======createFileForuploadNTServiceArea", "PASS");
		MngPostcode.createFileForuploadNTServiceArea(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.verifyUploadNtserviceArea(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.deleteFile_NTServiceArea(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=======verifyDownloadNt", "PASS");
		MngPostcode.verifyDownloadNt(testDataFile, dataSheet, scriptNo, dataSetNo);
		MngPostcode.deletePostcode(testDataFile, dataSheet, scriptNo, dataSetNo);
		
	}
	catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
	}
}