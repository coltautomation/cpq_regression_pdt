package siebelTests;

import java.io.File;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelPremiseMasterHelperObj.PremiseMasterHelper;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCEOS;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebelFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNumberManagement;

public class TS_12_Premise_Master extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	String dataFileName = "Siebel_testData.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "PremiseMaster";
//	String tsSheetName = "ModFinal";
	String scriptNo = "4";
	String dataSetNo = "5";
	String Amount = "2";
	
	
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddProduct = new SiebelCreateCustomerOrder();
	SiebelAccounts Accounts = new SiebelAccounts();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelMode siebelmod = new SiebelMode();
	SiebelNumberManagement NumMana = new SiebelNumberManagement();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();
	PremiseMasterHelper PM = new PremiseMasterHelper();
	
	@Test(priority = 1, groups={"testSiebelLogin"})
	public void testSiebelLogin()
	{
		Report.createTestCaseReportHeader();	
				
		try
		{
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
	
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelLogin "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelLogin "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Siebel Login page functionality");
		
	}	
	
	@Test(priority = 2, groups={"testNavigateToAccounts"}, dependsOnGroups = { "testSiebelLogin" })
	public void testNavigateToAccounts()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Accounts.NavigateToAccounts();

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testNavigateToAccounts "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testNavigateToAccounts "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify navigation to accounts Page");
		
	}
	
	@Test(priority = 3, groups={"testSearchAccount"}, dependsOnGroups = { "testNavigateToAccounts" })
	public void testSearchAccount()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Accounts.SearchAccount(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSearchAccount "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSearchAccount "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Search and Open existing account");
	}	
	
	@Test(priority = 4, groups={"testSiebelCreateCustomerOrder"}, dependsOnGroups = { "testSearchAccount" })
	public void testSiebelCreateCustomerOrder()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			AddProduct.createCustomerOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelCreateCustomerOrder "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelCreateCustomerOrder "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Siebel Create Customer Order");
		
	}	
	@Test(priority = 5, groups={"testproductSelection"}, dependsOnGroups = { "testSiebelCreateCustomerOrder" })
	public void testproductSelection()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.productSelection(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testproductSelection "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testproductSelection "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to add product");
	}	
	
	
	@Test(priority = 6, groups={"testopenServiceOrderNumber"}, dependsOnGroups = { "testproductSelection" } )
	public void testopenServiceOrderNumber()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			
			Product.openServiceOrderNumber(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testopenServiceOrderNumber "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testopenServiceOrderNumber "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to open serive Order ref number");
	}	
	
	
	@Test(priority = 7, groups={"testsearchAndaddSiteDetails"} 
	, dependsOnGroups = { "testopenServiceOrderNumber" })
//	, dependsOnGroups = { "testSiebelLogin" })

	public void testsearchAndaddSiteDetails()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			String username = Configuration.Premise_Username;
			String password = Configuration.Premise_Password;
	//		Thread.sleep(20000);
			siebelmod.addProductConfigurationPremiseMaster(testDataFile, tsSheetName, scriptNo, dataSetNo,username,password);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testsearchAndaddSiteDetails "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testsearchAndaddSiteDetails "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to Search the site and add the building");
	}
	
}
