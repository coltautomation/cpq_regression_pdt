package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import java.text.SimpleDateFormat;  


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSNumberReservationObj;
import pageObjects.nmtsObjects.NMTSPortOutNumberObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSNumberPortOutPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumber = new NMTSAddNumberPage();
	
	NMTSNumberReservationPage NumberReservation = new NMTSNumberReservationPage();
	String successmsg;
	
	private static HashMap<String, String> Number= new HashMap<String, String>();

	
	public void PortOutNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String ColtOrHostedNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt/HostedNo");
		String NewNumberRangeHolder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"New_Number_range_holder");
		String ContractNumber= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_number");
		String ContractSource= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_Source");
	//	String PortOutDate= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Port-out_date");
		
		String AreaName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Area_Code");
		String AreaCodeExtention = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NMTS_Area_Code_Extension");
		String RangeFrom = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStart");
		String RangeTo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeEnd");
	
		
		NumberReservation.UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
		waitForAjax(); 
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMenu");
		
		if(Country.toString().equals("CH")) 
		{
			WaitForAjax();
			ScrollIntoViewByString(NMTSPortOutNumberObj.PortOutNumber.portoutforCH);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.portoutforCH,"portout for CH");
			javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.portoutforCH,"portout for CH");	
		}else {
		waitForAjax();
		ScrollIntoViewByString(NMTSPortOutNumberObj.PortOutNumber.portoutNMTS);
		verifyExists(NMTSPortOutNumberObj.PortOutNumber.portoutNMTS,"portout NMTS");
		javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.portoutNMTS,"portout NMTS");
		}
		waitForAjax(); 
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMain");
	
		if(Country.toString().equals("IT") || Country.toString().equals("FR"))
		{
			//WaitForAjax();
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.sELECThOSTEDnO,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.sELECThOSTEDnO,"Colt/Hosted no");
			//javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.sELECThOSTEDnO,"Colt/Hosted no");
			Reusable.Select(NMTSPortOutNumberObj.PortOutNumber.sELECThOSTEDnO, ColtOrHostedNo);
		}
		if(Country.toString().equals("CH")) 
		{
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.wholesaleradiobtn,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.wholesaleradiobtn,"wholesaleradiobtn");
			javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.wholesaleradiobtn,"wholesaleradiobtn");
			WaitForAjax();
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.portoutradiobtn,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.portoutradiobtn,"wholesaleradiobtn");
			javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.portoutradiobtn,"wholesaleradiobtn");
			//WaitForAjax();
		}
		
		if(isElementPresent(By.xpath("//input[@id='ucxSearchCriteria_txtNationalCode']")))
		{
			waitForAjax();
			
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.AreaCodePrefix,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.AreaCodePrefix,"AreaCodePrefix");
			sendKeys(NMTSPortOutNumberObj.PortOutNumber.AreaCodePrefix, AreaName, "Area Name");
			
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.AreaCodeExt,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.AreaCodeExt,"AreaCodeExt");
			sendKeys(NMTSPortOutNumberObj.PortOutNumber.AreaCodeExt, AreaCodeExtention, "Area Code Extention");
			
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.RangeFrom,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.RangeFrom,"RangeFrom");
			sendKeys(NMTSPortOutNumberObj.PortOutNumber.RangeFrom, RangeFrom, "Range From");
			
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.retandtransferRangeto,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.retandtransferRangeto,"retandtransferRangeto");
			sendKeys(NMTSPortOutNumberObj.PortOutNumber.retandtransferRangeto, RangeTo, "Range to"); 
		
			waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.Submitbtn,15);
			verifyExists(NMTSPortOutNumberObj.PortOutNumber.Submitbtn,"Submitbtn");
			javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.Submitbtn,"Submitbtn");
			WaitForAjax();
			Reusable.switchtodefault();
			WaitForAjax();
			Reusable.switchtoframe("fmMain");
			//waitForAjax();
			
			if(isElementPresent(By.xpath("//table[@id='ucxRange_grdView']//tr[contains(@ondblclick,'hdnIndex')]")))
			{
				waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.checkbox3,15);
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.checkbox3,"checkbox3");
				javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.checkbox3,"checkbox3");
				
				waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.Selectmark,15);
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.Selectmark,"Selectmark");
				javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.Selectmark,"Selectmark");
				WaitForAjax();
				
				waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.AllocationCheckboxagain,15);
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.AllocationCheckboxagain,"AllocationCheckboxagain");
				javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.AllocationCheckboxagain,"AllocationCheckboxagain");
				WaitForAjax();
				if(Country.toString().equals("CH")){
					//ScrollIntoViewByString(NMTSPortOutNumberObj.PortOutNumber.RangeHolder);
					//verifyExists(NMTSPortOutNumberObj.PortOutNumber.RangeHolder,"New Number Range Holder");
					//Reusable.Select(NMTSPortOutNumberObj.PortOutNumber.RangeHolder, NewNumberRangeHolder);
					
					verifyExists(NMTSPortOutNumberObj.PortOutNumber.ContractNumber,"Contract_Number");
					sendKeys(NMTSPortOutNumberObj.PortOutNumber.ContractNumber, ContractNumber, "Contract_Number");
					
					verifyExists(NMTSPortOutNumberObj.PortOutNumber.ContractSrc,"Contract_Source");
					Reusable.Select(NMTSPortOutNumberObj.PortOutNumber.ContractSrc, ContractSource);
				}else{
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.RangeHolder,"New Number Range Holder");
				Reusable.Select(NMTSPortOutNumberObj.PortOutNumber.RangeHolder, NewNumberRangeHolder);
				
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.ContractNumber,"Contract_Number");
				sendKeys(NMTSPortOutNumberObj.PortOutNumber.ContractNumber, ContractNumber, "Contract_Number");
				
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.ContractSrc,"Contract_Source");
				Reusable.Select(NMTSPortOutNumberObj.PortOutNumber.ContractSrc, ContractSource);
				}
				Date date = new Date();  
			    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			    String strDate= formatter.format(date);  
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.portoutdateNMTS,"portout date NMTS");
				sendKeys(NMTSPortOutNumberObj.PortOutNumber.portoutdateNMTS, strDate, "portout date NMTS");
				
				waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.Portoutselectedrangebtn,15);
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.Portoutselectedrangebtn,"Portout selected range btn");
				javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.Portoutselectedrangebtn,"Portout selected range btn");
				WaitForAjax();
				
				waitForElementToBeVisible(NMTSPortOutNumberObj.PortOutNumber.YesBtn,15);
				verifyExists(NMTSPortOutNumberObj.PortOutNumber.YesBtn,"Yes Btn");
				javaScriptclick(NMTSPortOutNumberObj.PortOutNumber.YesBtn,"Yes Btn");
				WaitForAjax();		
		} 
			else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "please check the correct data in Excel sheet for - AreaCode, AreacodeExt and Rangefrom");
			     }
			  Reusable.switchtodefault();
			  WaitForAjax();
			  Reusable.switchtoframe("fmMain");
		    	  String portout = getTextFrom(NMTSPortOutNumberObj.PortOutNumber.PortOutStatus, "Port Out Status");
				
				  if(portout.contains("Numbers Ported Out"))
					{
					  ExtentTestManager.getTest().log(LogStatus.PASS, "Number Ported Out successfuly");
						
					}
					else{					
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Not Ported Out successfuly");		
					}
	
	}
}
}	
	
	

