package pageObjects.c4cObjects;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import baseClasses.DataMiner;

public class C4CAccountsObj 
{
	
	public static class Accounts
	 { 
		//Landing Page - Left Menu Elements
		public static final String customersMenu="@xpath=//span[@title='Customers' and contains(text(), 'Customers')]"; //Customers Menu
		public static final String accountsMenu="@xpath=//a[@title='Accounts' and contains(text(), 'Accounts')]"; //Accounts Menu
		public static final String homeMenu="@xpath=//a[@title='Home' and contains(text(), 'Home')]"; //Accounts Menu
		
		//Accounts List Elements
		public static final String myAccountsSection="@xpath=//div[@class='sapUiHLayoutChildWrapper']/descendant::span[contains(text(), 'My Accounts')]"; //My Accounts Section
		public static final String selectViewDn="@xpath=//span[contains(@id,'variantManagement-trigger-img')]"; //Select View Dropdown
		public static final String selectViewOptions="@xpath=//ul[contains(@id, 'variantManagement-list')]"; //Select View Options
		public static final String allAccountsFilter="@xapth=//li[contains(text(), 'All')]"; //All Accounts Filter
		public static final String searchBtn="@xpath=//div[@title='Search']"; //Search Button
		public static final String searchTxb="@xpath=//input[@type='search']"; //Search Textbox
		public static final String searchField="@xpath=//div[contains(@id, 'searchField-search')]"; //Search Field
		public static final String accountName1="@xpath=//td[@class='sapMListTblCell sapMSize-small']/descendant::a[contains(text(), '"; //Account Name
		public static final String accountName2="')]"; //Account Name
		public static final String accountHeader1="@xpath=//span[@data-sap-automation-id='objectDetail-Header-Name' and contains(text(), '"; //Account Header
		public static final String accountHeader2="')]"; //Account Header
		public static final String searchIcon="@xpath=//span[contains(@id,'searchButton-img')]"; //Search Icon
		public static final String allLst="@xpath=//li[contains(text(),'All')]"; //All Value in dropdown
		public static final String defaultCkb="@xpath=//div[@aria-label='Select all rows'][1]"; //to verify Check box is enbled
		
		public static final String ProdConfigLoadingIcn="@xpath=//div[contains(text(),'Results expected in less than')]";
	 }

}
