package testHarness.siebelFunctions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.Map.Entry;
import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GridHelper;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelModeObj;
import pageObjects.siebelObjects.SiebelNewOrderOnnetHelperObj;
import testHarness.commonFunctions.SiebelReusableFunctions;


public class Siebel_To_SAP extends SeleniumUtils{
	
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	WebDriver driver;
	static ReadExcelFile read = new ReadExcelFile();
	String dataFileName = "Siebel_testData.xls";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	Random rnd = new Random();
	public  static ThreadLocal<String> NetworkReferenceIPVPN = new ThreadLocal<>();
	public static ThreadLocal<String> CpnValue2 = new ThreadLocal<>();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelMode siebelmod = new SiebelMode();
	GridHelper Grid = new GridHelper();

	public void addProductConfigurationDetailsAndCompleteSiebel_TO_SAP (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelHubSite  Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelCEOS CEOS = new SiebelCEOS();

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetCheck = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) {
			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo); 
			siebelmod.EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			siebelmod.EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			siebelmod.EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo); 
		} else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) {
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			siebelmod.EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			siebelmod.EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			siebelmod.EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
			{		
				Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
				Reusable.waitForSiebelSpinnerToDisappear();	
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
			}
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
		} else {
		/*	siebelmod.enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			siebelmod.VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
			siebelmod.VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
			siebelmod.EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			siebelmod.EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			siebelmod.EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationB();
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationA();
			Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);*/
			EROData(testDataFile, sheetName, scriptNo, dataSetNo);
			
		
		}
	}
	@SuppressWarnings("unused")
	public void EROData(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws IOException, InterruptedException {
				
		OpenTab("ERO");
		String CityValueTxt ="@xpath="+ SiebelNewOrderOnnetHelperObj.SAP.CpnNumber + "/tbody" + "/tr[2]/td[3]";
		String CpnNumberValueTxt ="@xpath="+ SiebelNewOrderOnnetHelperObj.SAP.CpnNumber + "/tbody" + "/tr[2]/td[5]";
		String CityValue = getTextFrom(CityValueTxt);
		Report.LogInfo("Verify","<b><i>"+CityValue +"</i></b> Is City Name", "PASS");	
		ExtentTestManager.getTest().log(LogStatus.INFO, CityValue +" Is City Name");
		
		String CpnValue = null;
		long Seconds = 360;
		for( long i = 1; i <= Seconds ;i++) {
			CpnValue= getTextFrom(CpnNumberValueTxt);
			//CpnValue = "229435881";
			//CpnValue.set(getAttributeFrom(CpnNumberValueTxt,"CPN Number"));
			//CpnValue=CpnValue2.get();
			
			if(CpnValue.isEmpty()==true)
			{
				Reusable.refreshPage();
				Reusable.waitForSiebelSpinnerToDisappear();
				Report.LogInfo("Verify","<b><i>"+CpnValue +"CPN Number is null", "FAIL");	
				ExtentTestManager.getTest().log(LogStatus.INFO, CpnValue +" CPN Number is null");
				
			}
			else {
				Report.LogInfo("Verify","<b><i>"+CpnValue+ " :Is CPN Number" , "PASS");	
				ExtentTestManager.getTest().log(LogStatus.INFO, CpnValue +" Verify CPN Number");
				break;
				
			}
			
				
		}
							
	}
	
	private void OpenTab(String TabName) throws IOException, InterruptedException {
		try {
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			if(isElementPresent(By.xpath("//button[text()='Ok']"))) {
				System.out.println("");
				System.out.println("Alert Present");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert2 Accept"); // 
				}
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabs.replace("tabName", TabName),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabs.replace("tabName", TabName),"ERO Tab");
			System.out.println("Click on tab : " + TabName);
			if(isElementPresent(By.xpath("//button[text()='Ok']"))) {
				System.out.println("");
				System.out.println("Alert Present");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert2 Accept"); // 
				verifyExists(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabs.replace("tabName", TabName));
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabs.replace("tabName", TabName));
			}
			
		} catch (Exception e) {
			select(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.installtionDropdown, TabName);

		}
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open Tab :" + TabName);
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
	}
}
