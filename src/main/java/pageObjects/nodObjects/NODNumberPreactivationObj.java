package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODNumberPreactivationObj {

	
	public static class NumPreActivation
	{ 
		public static final String TableRecordLoader="@xpath=//*[@class='ph-item tblLoader']"; 
		public static final String QuickLinkDrpDwn="@xpath=//div[contains(@class,'ng-select-container')]//div[contains(text(),'Quick Links')]"; 
		public static final String QuickLinkDrpDwnArw="@xpath=//body/app-root[1]/app-my-orders[1]/div[1]/div[1]/div[1]/div[2]/app-profile[1]/div[1]/form[1]/div[1]/ng-select[1]/div[1]/span[1]";
		public static final String ReservationLink ="@xpath=//label[contains(text(),'Reservation')]";
		public static final String ReservationPage ="@xpath=//*[contains(text(),'Reservatio..')]";
		public static final String NumberTypeRadioBtn ="@xpath=//label[contains(text(),'NoType')]";
		public static final String BlockSize ="@xpath=//select[@id='blockSizeGeo']";
		public static final String QuantitySize ="@xpath=//select[@id='quantitySizeGeo']";
		public static final String ProvideNumbersBtn ="@xpath=//button[contains(text(),'Provide Numbers')]";
		public static final String LocalAreaCode ="@xpath=(//div[@class='ng-input']/input)[3]";
		public static final String LocalAreaCodeOption ="@xpath=//span[contains(text(),'option')]";
		public static final String NumberRangeChkBox ="@xpath=//label[@for='customCheckAll']";
		public static final String PreActivateBtn ="@xpath=//button[contains(text(),'Preactivate')]";
		public static final String OrderReview ="@xpath=//button[contains(text(),'Order Review')]";
		public static final String OrderConfirmationBtn ="@xpath=//button[contains(text(),'Order Confirmation')]";
		public static final String ResrvationConfirmationMsg ="@xpath=//div[@id='test-l-4']";
		public static final String SuccessMessage ="@xpath=//*[contains(text(),'Your order was raised successfully.')]";
		public static final String ReservationOrderId ="@xpath=//span[contains(text(),'Order Id : ')]/a[@class='link']";
		public static final String CheckMyOrder ="@xpath=//button[contains(text(),'Check My Order(s)')]";
		public static final String orderStatusVal="@xpath=(//*[@tabulator-field='currentStatus'])[2]";
		public static final String orderTypeVal="@xpath=(//*[@tabulator-field='transactionType'])[2]";
		public static final String SearchTxtFld="@xpath=//*[@class='input-group']//*[@placeholder='Search']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group']//button[@type='button']";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}