package testHarness.aptFunctions;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_VoiceLineObj;
import pageObjects.aptObjects.Manage_PostcodeObj;

public class APT_VoiceLine extends SeleniumUtils{

	public String primarytrunkGroupname=null;
	public String Edit_primarytrunkGroupname=null;
	public String primarytrunkGroupname_Resilient=null;
	public String ASRDevice_ManagementAddress=null;
	
	
	public void verifysuccessmessage(String expected) throws InterruptedException {
	
	waitforPagetobeenable();
	//scrollIntoTop();
	scrollUp();
	waitforPagetobeenable();
	try {	
		
		boolean successMsg=findWebElement(Manage_PostcodeObj.ManageColt.alertMsg_managePOstcode).isDisplayed();

		if(successMsg) {
			
			String alrtmsg=findWebElement(Manage_PostcodeObj.ManageColt.AlertForServiceCreationSuccessMessage_managePOstcode).getText();
			
			if(expected.contains(alrtmsg)) {
				
				ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
				Report.LogInfo("INFO","Message is verified. It is displaying as: "+alrtmsg,"PASS");
				
				//successScreenshot(application);
				
			}else if(expected.equals(alrtmsg)){
				
				ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
				Report.LogInfo("INFO","Message is verified. It is displaying as: "+alrtmsg,"PASS");
				//successScreenshot(application);
				
			}else {
				
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg +" .The Expected value is: "+ expected);
				Report.LogInfo("INFO","Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg,"FAIL");
				//failureScreenshot(application);
			}
			
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");
			Report.LogInfo("INFO"," Success Message is not displaying","FAIL");
			//failureScreenshot(application);
		}
		
	}catch(Exception e) {
		Report.LogInfo("INFO","failure in fetching success message  ","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, expected+ " Message is not displaying");
		Report.LogInfo("INFO",expected+ " message is not getting dislpayed","FAIL");
		//failureScreenshot(application);
	}
}
	
	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception {
		
		String name=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
		String maindomain=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MainDomain");
		String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CountryToBeSelected");
		String ocn=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OCN");
		String reference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Reference");
		String tcn=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TechnicalContactName");
		String type=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TypeToBeSelected");
		String email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Email");
		String phone=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone");
		String fax=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fax");

		mouseOverAction(APT_VoiceLineObj.APT_VoiceLine.ManageCustomerServiceLink);
		waitforPagetobeenable();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Mouse hovered on 'Manage Customers Service' menu item");
		Report.LogInfo("INFO","Mouse hovered on 'Manage Customers Service' menu item","PASS");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.createcustomerlink, "create customer link");
		click(APT_VoiceLineObj.APT_VoiceLine.createcustomerlink, "create customer link");
		waitforPagetobeenable();
		
		compareText( "create customer page header", APT_VoiceLineObj.APT_VoiceLine.createcustomer_header, "Create Customer");
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "Ok");
		click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "Ok");

		//Warning msg check
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.customernamewarngmsg, "Legal Customer Name");
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.countrywarngmsg, "Country");
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.ocnwarngmsg, "OCN");
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.typewarngmsg, "Type");
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.emailwarngmsg, "Email");

		//Clear customer info
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.nametextfield, "Customer Name");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.nametextfield,name, "Customer Name");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.maindomaintextfield, "Main Domain");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.maindomaintextfield,maindomain,  "Main Domain");
		
		waitforPagetobeenable();
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.resetbutton);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.resetbutton, "Reset");
		click(APT_VoiceLineObj.APT_VoiceLine.resetbutton, "Reset");
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "All text field values are cleared");
		Report.LogInfo("INFO","All text field values are cleared","PASS");

		//Create customer by providing all info
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.nametextfield, "Customer Name");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.nametextfield,name, "Customer Name");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.maindomaintextfield, "Main Domain");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.maindomaintextfield,maindomain, "Main Domain");
		
		//verifyExists(APT_VoiceLineObj.APT_VoiceLine.country, "Country");
		//sendKeys(APT_VoiceLineObj.APT_VoiceLine.country,country, "Country");
		addDropdownValues_commonMethod("Country", APT_VoiceLineObj.APT_VoiceLine.country, country);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.ocntextfield, "OCN");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.ocntextfield,ocn, "OCN");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.referencetextfield, "Reference");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.referencetextfield,reference, "Reference");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.technicalcontactnametextfield, "Technical Contact Name");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.technicalcontactnametextfield,tcn, "Technical Contact Name");
		
		//verifyExists(APT_VoiceLineObj.APT_VoiceLine.typedropdown, "Type");
		//sendKeys(APT_VoiceLineObj.APT_VoiceLine.typedropdown,type, "Type");
		
		addDropdownValues_commonMethod("Type", APT_VoiceLineObj.APT_VoiceLine.typedropdown, type);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.emailtextfield, "Email");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.emailtextfield,email, "Email");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.phonetextfield, "Phone");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.phonetextfield,phone, "Phone");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.faxtextfield, "Fax");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.faxtextfield,fax, "Fax");
		
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "Ok");
		click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "Ok");
		waitforPagetobeenable();
		verifysuccessmessage( "Customer successfully created.");
	}


public static String newordernumber, newVoiceLineNumber, SelectOrderNumber;
public void createorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException{
	
	String neworder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewOrderService");
	String neworderno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewOrderNumber");
	String newrfireqno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewRFIREQNumber");
	String existingorderservice=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingOrderService");
	String existingordernumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingOrderNumber");

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.nextbutton);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.nextbutton,"Next");
	click(APT_VoiceLineObj.APT_VoiceLine.nextbutton,"Next");
	
	waitforPagetobeenable();

	//Warning messages verify
	//warningMessage_commonMethod(application, "order_contractnumber_warngmsg", "Order/Contract Number(Parent SID)", xml);
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.servicetype_warngmsg, "Service Type");

	if (neworder.equalsIgnoreCase("YES")) {

		waitforPagetobeenable();

		verifyExists(APT_VoiceLineObj.APT_VoiceLine.newordertextfield, "Order/Contract Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.newordertextfield,neworderno, "Order/Contract Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.newrfireqtextfield, "RFI Voice line Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.newrfireqtextfield,newrfireqno, "RFI Voice line Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.createorderbutton, "create order");
		click(APT_VoiceLineObj.APT_VoiceLine.createorderbutton, "create order");
		
		
		waitforPagetobeenable();
		verifysuccessmessage( "Order created successfully");
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.CreateOrderHeader);

		newordernumber = neworderno;
		newVoiceLineNumber = newrfireqno;
	} 

	else if (existingorderservice.equalsIgnoreCase("YES")) {
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.selectorderswitch, "select order switch");
		click(APT_VoiceLineObj.APT_VoiceLine.selectorderswitch, "select order switch");
		
		addDropdownValues_commonMethod( "Order/Contract Number(Parent SID)", APT_VoiceLineObj.APT_VoiceLine.existingorderdropdown, existingordernumber);
		
		Report.LogInfo("INFO","=== Order Contract Number selected===","INFO");

		waitforPagetobeenable();

		SelectOrderNumber = existingordernumber;
	} else {

		System.out.println("Order not selected");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Step :Order not selected");
	}

	}

public void verifyselectservicetype(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException{

	String servicetype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");

	// select service type
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.nextbutton);
	//addDropdownValues_commonMethod( "Service Type", "servicetypetextfield", servicetype);
	boolean availability=false;
	try {  
		availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.servicetypetextfield).isDisplayed();
		
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Type dropdown is displaying");
			Report.LogInfo("INFO", "Service Type dropdown is displaying", "PASS");

			if(servicetype.equalsIgnoreCase("null")) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Service Type dropdown");
				Report.LogInfo("INFO", " No values selected under Service Type dropdown", "PASS");
			}else {

				webDriver.findElement(By.xpath("//div[label[text()='Service Type']]//div[text()='�']")).click();
				//Clickon(findWebElement("//div[label[text()='Service Type']]//div[text()='�']"));
				
				waitforPagetobeenable();

				//verify list of values inside dropdown
				List<WebElement> listofvalues = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));

				ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Service Type dropdown is:  ");
				Report.LogInfo("INFO", " List of values inside Service Type dropdown is:  ","PASS");

				for (WebElement valuetypes : listofvalues) {
					Report.LogInfo("INFO","service sub types : " + valuetypes.getText(),"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
					
				}
				waitforPagetobeenable();
					
				webDriver.findElement(By.xpath("//div[label[text()='Service Type']]//input")).sendKeys(servicetype);
				
				waitforPagetobeenable();

				scrollDown(APT_VoiceLineObj.APT_VoiceLine.nextbutton);
				waitforPagetobeenable();
				
				webDriver.findElement(By.xpath("(//div[text()='"+ servicetype +"'])[1]")).click();
				waitforPagetobeenable();

				String actualValue=webDriver.findElement(By.xpath("//label[text()='Service Type']/following-sibling::div//span")).getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service Type dropdown value selected as: "+ actualValue );
				Report.LogInfo("INFO","Service Type dropdown value selected as: "+ actualValue,"PASS");

			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Service Type is not displaying");
			Report.LogInfo("INFO","Service Type is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Service Type is not displaying");
		Report.LogInfo("INFO","Service Type is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Service Type dropdown");
		Report.LogInfo("INFO"," NO value selected under Service Type dropdown","FAIL");
	}
	// click on next button
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	click(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	//click_commonMethod( "Next", "nextbutton");
	waitforPagetobeenable();
	waitforPagetobeenable();
	
	compareText( "Create Order / Service Header", APT_VoiceLineObj.APT_VoiceLine.createorderservice_header, "Create Order / Service");

	}

public void selectCustomertocreateOrderNewCust(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException {

	String ChooseCustomerToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
	
	mouseMoveOn(APT_VoiceLineObj.APT_VoiceLine.ManageCustomerServiceLink);
	waitforPagetobeenable();

	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Mouse hovered on 'Manage Customers Service' menu item");
	Report.LogInfo("INFO","Mouse hovered on 'Manage Customers Service' menu item","PASS");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.CreateOrderServiceLink, "Create Order/Service");
	click(APT_VoiceLineObj.APT_VoiceLine.CreateOrderServiceLink,"Create Order/Service");
	
	//Report.LogInfo("INFO","=== Create Order/Service navigated ===","PASS");

	//click on Next button to check mandatory messages
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	click(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");

	//Customer Error message	
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.customer_createorderpage_warngmsg, "Choose a customer");

	//Entering Customer name
	//addtextFields_commonMethod("Customer Name", APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, ChooseCustomerToBeSelected);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, "Customer Name Field");
	click(APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, "Customer Name Field");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, ChooseCustomerToBeSelected,"Customer Name Field");
	
	waitforPagetobeenable();
	//addtextFields_commonMethod("Customer Name", APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, "*");

	//Select Customer from dropdown
	addDropdownValues_commonMethod("Choose a customer", APT_VoiceLineObj.APT_VoiceLine.chooseCustomerdropdown, ChooseCustomerToBeSelected);

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	click(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	waitforPagetobeenable();
}

public void selectCustomertocreateOrderExCust(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException {

	String ChooseCustomerToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomer");
	
	mouseMoveOn(APT_VoiceLineObj.APT_VoiceLine.ManageCustomerServiceLink);
	waitforPagetobeenable();
	
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Mouse hovered on 'Manage Customers Service' menu item");
	Report.LogInfo("INFO","Mouse hovered on 'Manage Customers Service' menu item","PASS");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.CreateOrderServiceLink, "Create Order/Service");
	click(APT_VoiceLineObj.APT_VoiceLine.CreateOrderServiceLink, "Create Order/Service");
	
	
	Report.LogInfo("INFO","=== Create Order/Service navigated ===","PASS");

	//click on Next button to check mandatory messages
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	click(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");

	//Customer Error message	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.customer_createorderpage_warngmsg, "Choose a customer");

	//Entering Customer name
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, "Customer Name");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, ChooseCustomerToBeSelected,"Customer Name");
    
    waitforPagetobeenable();
    verifyExists(APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, "Customer Name");
    sendKeys(APT_VoiceLineObj.APT_VoiceLine.entercustomernamefield, "*","Customer Name" );
    
    //Select Customer from dropdown
    addDropdownValues_commonMethod("Choose a customer", APT_VoiceLineObj.APT_VoiceLine.chooseCustomerdropdown, ChooseCustomerToBeSelected);

    verifyExists(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	click(APT_VoiceLineObj.APT_VoiceLine.nextbutton, "Next");
	waitforPagetobeenable();
	waitforPagetobeenable();
}

public void verifyservicecreation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String Remarks=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Remarks");
	//String orderno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewOrderNumber");
	//String rfireqno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewRFIREQNumber");
	//String servicetype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String resellercodevalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerCode_Value");
	String thirdpartyinternet_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyInternet_Checkbox");
	String email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Email");
	String phonecontact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PhoneContact");
	String performancereporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerformanceReporting_Checkbox");
	String proactivenotification_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ProactiveNotification_Checkbox");
	String notificationmanagementteam_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NotificationManagementTeam_value");

	//Create service
	compareText( "Create Order / Service Header", APT_VoiceLineObj.APT_VoiceLine.createorderservice_header, "Create Order / Service");
	//addDropdownValues_commonMethod( "Order/Contract Number(Parent SID)", "existingorderdropdown", orderno);

	// service identification
	//cleartext( "Service Identification", "serviceidentificationtextfield");
	clearTextBox(APT_VoiceLineObj.APT_VoiceLine.serviceidentificationtextfield);
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.serviceidentificationtextfield, "Service Identification");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.serviceidentificationtextfield, sid,"Service Identification");
	waitforPagetobeenable();
	
	getTextFrom(APT_VoiceLineObj.APT_VoiceLine.servicetypevalue,"service type value");
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield, "Reseller Code");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield, resellercodevalue,"Reseller Code");
	
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.servicepage_thirdpartyinternet_checkbox, "3rd Party Internet", thirdpartyinternet_checkbox);
	
	//addtextFields_commonMethod( "Remarks", "remarktextarea", Remarks);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.remarktextarea, "Remarks");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.remarktextarea, Remarks,"Remarks");
	
	//addtextFields_commonMethod( "Email", "emailtextfieldvalue", email);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.emailtextfieldvalue, "Email");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.emailtextfieldvalue, email,"Email");
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.emailaddarrow, "Email adding Arrow");
	click( APT_VoiceLineObj.APT_VoiceLine.emailaddarrow, "Email adding Arrow");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.emailtextfieldvalue);
	
	waitforPagetobeenable();
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.phonecontacttextfieldvalue, "Phone Contact");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.phonecontacttextfieldvalue, phonecontact,"Phone Contact");
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.phoneaddarrow, "phone contact adding Arrow");
	click( APT_VoiceLineObj.APT_VoiceLine.phoneaddarrow, "phone contact adding Arrow");
	
	compareText( "Management Options", APT_VoiceLineObj.APT_VoiceLine.managementoptions_header, "Management Options");
	
	String Package= findWebElement(APT_VoiceLineObj.APT_VoiceLine.packagefield).getAttribute("disabled");
	
	if(Package!=null)
	{
		Report.LogInfo("INFO","Package dropdown field is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Package dropdown field is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Package dropdown field is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Package dropdown field is not disabled");
	}
	String ManagedService_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.managedservice_checkbox).getAttribute("disabled");
	String ManagedService_Checked= findWebElement(APT_VoiceLineObj.APT_VoiceLine.managedservice_checkbox).getAttribute("checked");
	
	if(ManagedService_Disabled!=null && ManagedService_Checked!=null)
	{
		Report.LogInfo("INFO","Managed Service Checkbox is disabled & checked by default as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Managed Service Checkbox is disabled & checked by default as expected");
	}
	else
	{
		Report.LogInfo("INFO","Managed Service Checkbox is not disabled & checked by default as expected","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Managed Service Checkbox is not disabled & checked by default");
	}
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.managementoptions_header);
	//syslog event view checkbox
	String SyslogEvent_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.syslogevent_checkbox).getAttribute("disabled");
	if(SyslogEvent_Disabled!=null)
	{
		Report.LogInfo("INFO","Syslog Event View Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Syslog Event View Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Syslog Event View Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Syslog Event View Checkbox is not disabled");
	}
	//service status view checkbox
	String ServiceStatusView_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.servicestatusview_checkbox).getAttribute("disabled");
	if(ServiceStatusView_Disabled!=null)
	{
		Report.LogInfo("INFO","Service Status View Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service Status View Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Service Status View Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Service Status View Checkbox is not disabled");
	}
	//Router configuration view checkbox
	String RouterConfigurationView_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.routerconfigview_checkbox).getAttribute("disabled");
	
	if(RouterConfigurationView_Disabled!=null)
	{
		Report.LogInfo("INFO","Router Configuration View Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Router Configuration View Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Router Configuration View Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Router Configuration View Checkbox is not disabled");
	}
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.performancereporting_checkbox, "Performance Reporting", performancereporting_checkbox);
	if(proactivenotification_checkbox.equalsIgnoreCase("Yes"))
	{
		addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.proactivenotification_checkbox, "Pro-active Notification", proactivenotification_checkbox);
		
		addDropdownValues_commonMethod( "Notification Management Team", APT_VoiceLineObj.APT_VoiceLine.notificationmanagementteam_dropdown, notificationmanagementteam_value);
	}
	else
	{
		addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.proactivenotification_checkbox, "Pro-active Notification", proactivenotification_checkbox);
	}
	//Dial user administration checkbox
	String DialUserAdministration_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.dialuseradministration_checkbox).getAttribute("disabled");
	if(DialUserAdministration_Disabled!=null)
	{
		Report.LogInfo("INFO","Dial User Administration Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Dial User Administration Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Dial User Administration Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Dial User Administration Checkbox is not disabled");
	}

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");

	waitforPagetobeenable();
	waitforPagetobeenable();
	verifysuccessmessage( "Service successfully created");
	
}

public void addCheckbox_commonMethod(String xpath, String labelname, String expectedValue)
		throws InterruptedException, IOException {

	boolean availability = false;

	availability = isVisible(xpath);
	if (availability) {
		Report.LogInfo("INFO", "checkbox is displaying as expected", "PASS");
		if (!expectedValue.equalsIgnoreCase("null")) {
			if (expectedValue.equalsIgnoreCase("yes")) {
				click(xpath, labelname);
				waitforPagetobeenable();
				boolean CPEselection = isSelected(xpath, labelname);
				if (CPEselection) {
					Report.LogInfo("INFO", "checkbox is selected as expected", "PASS");
				} else {
					Report.LogInfo("INFO", "checkbox is not selected", "FAIL");
				}
			} else {
				Report.LogInfo("INFO", "checkbox is not selected as expected", "PASS");
			}
		}
	} else {
		Report.LogInfo("INFO", "checkbox is not available", "FAIL");
	}
}

public void verifyCustomerDetailsInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String newCustomerCreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomerCreation");
	String existingCustomerSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomerSelection");
	String newCustomer=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
	String existingCustomer=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomer");
	String maindomain=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MainDomain");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CountryToBeSelected");
	String ocn=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OCN");
	String reference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Reference");
	String tcn =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TechnicalContactName");
	String type=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TypeToBeSelected");
	String email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Email");
	String phone=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone");
	String fax=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fax");

	Report.LogInfo("INFO", "Verifying Customer informations", "INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Customer informations");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.customerdetailsheader);


	//Customer Name
	if(newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
		
		compareText("Customer Name", APT_VoiceLineObj.APT_VoiceLine.Name_Value, newCustomer);

		compareText( "Country", APT_VoiceLineObj.APT_VoiceLine.Country_Value, country);
		compareText( "OCN", APT_VoiceLineObj.APT_VoiceLine.OCN_Value, ocn);
		compareText( "Reference", APT_VoiceLineObj.APT_VoiceLine.Reference_Value, reference);
		compareText( "Technical Contact Name", APT_VoiceLineObj.APT_VoiceLine.TechnicalContactName_Value, tcn);
		compareText( "Type", APT_VoiceLineObj.APT_VoiceLine.Type_Value, type);
		compareText( "Email",APT_VoiceLineObj.APT_VoiceLine.Email_Value, email);
		compareText( "Phone",APT_VoiceLineObj.APT_VoiceLine.Phone_Value, phone);
		compareText( "Fax", APT_VoiceLineObj.APT_VoiceLine.Fax_Value, fax);

	}
	else if(newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {

		compareText( "Customer Name", APT_VoiceLineObj.APT_VoiceLine.Name_Value, existingCustomer);
	}

	//Main Domain
	if(maindomain.equalsIgnoreCase("Null")) {
		Report.LogInfo("INFO","A default displays for main domain field, if no provided while creating customer","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "A default displays for main domain field, if no provided while creating customer");
	}else {
		compareText( "Main Domain", APT_VoiceLineObj.APT_VoiceLine.MainDomain_Value, maindomain);
	}

	Report.LogInfo("INFO","=== Customer Details panel fields Verified ===","INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO, "=== Customer Details panel fields Verified ===");
	
}

public void verifyUserDetailsInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException {

	String Login=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LoginColumn");
	String Name=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NameColumn");
	String Email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmailColumn");
	String Roles=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RolesColumn");
	String Address=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AddressColumn");
	String Resource=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResourceColumn");
	// verify table column names
	compareText( "Login column", APT_VoiceLineObj.APT_VoiceLine.LoginColumn, Login);
	compareText( "Name column", APT_VoiceLineObj.APT_VoiceLine.NameColumn, Name);
	compareText( "Email column", APT_VoiceLineObj.APT_VoiceLine.EmailColumn, Email);
	compareText( "Roles column", APT_VoiceLineObj.APT_VoiceLine.RolesColumn, Roles);
	compareText( "Address column", APT_VoiceLineObj.APT_VoiceLine.AddressColumn, Address);
	compareText( "Resource column", APT_VoiceLineObj.APT_VoiceLine.ResourcesColumn, Resource);

}

public void verifyorderpanel_editorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException 
{

	String editorderno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditOrder_OrderNumber");
	String editvoicelineno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditOrder_VoicelineNumber");
	String editOrderSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editOrderSelection");
	
	Report.LogInfo("INFO","Verifying 'Edit Order' Functionality","INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Edit Order' Functionality");

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);

	if(editOrderSelection.equalsIgnoreCase("No")) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "Edit Order is not performed");
		Report.LogInfo("INFO","Edit Order is not performed","PASS");
	}
	else if(editOrderSelection.equalsIgnoreCase("Yes")) {
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Performing Edit Order Functionality");
		Report.LogInfo("INFO","Performing Edit Order Functionality","PASS");
		//Cancel Edit order in Order panel
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.orderactionbutton, "Action dropdown");
		click(APT_VoiceLineObj.APT_VoiceLine.orderactionbutton, "Action dropdown");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorderlink, "Edit Order");
		click(APT_VoiceLineObj.APT_VoiceLine.editorderlink, "Edit Order");
		
		compareText( "Edit Order", APT_VoiceLineObj.APT_VoiceLine.editorderheader, "Edit Order");
		waitforPagetobeenable();

		//WebElement EditOrderNo= findWebElement(APT_VoiceLineObj.APT_VoiceLine.editorderno);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorderno, "Order Number");
		click(APT_VoiceLineObj.APT_VoiceLine.editorderno, "Order Number");
		
		waitforPagetobeenable();
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.editorderno);
		waitforPagetobeenable();
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorderno, "Order Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.editorderno, editorderno,"Order Number");
		//addtextFields_commonMethod( "Order Number", "editorderno", editorderno);

		WebElement EditVoiceLineNo= findWebElement(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno, "RFI Voice Line Number");
		click(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno, "RFI Voice Line Number");
		
		waitforPagetobeenable();
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno);
		waitforPagetobeenable();
		
		//addtextFields_commonMethod( "RFI Voiceline Number", "editvoicelineno", editvoicelineno);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno,  "RFI Voiceline Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno, editvoicelineno, "RFI Voiceline Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.cancelbutton,"Cancel");
		click(APT_VoiceLineObj.APT_VoiceLine.cancelbutton, "Cancel");

		//Edit Order
		waitforPagetobeenable();
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);
		waitforPagetobeenable();
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.orderactionbutton,"Action dropdown");
		click(APT_VoiceLineObj.APT_VoiceLine.orderactionbutton, "Action dropdown");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorderlink,"Edit Order");
		click(APT_VoiceLineObj.APT_VoiceLine.editorderlink, "Edit Order");
		
		compareText( "Edit Order Header", APT_VoiceLineObj.APT_VoiceLine.editorderheader, "Edit Order");
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorderno,"Order Number");
		click(APT_VoiceLineObj.APT_VoiceLine.editorderno, "Order Number");
		
		waitforPagetobeenable();
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.editorderno);
		waitforPagetobeenable();
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorderno,  "Order Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.editorderno, editorderno, "Order Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno,"RFI Voice Line Number");
		click(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno, "RFI Voice Line Number");
		
		waitforPagetobeenable();
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno);
		waitforPagetobeenable();
		
		//addtextFields_commonMethod( "RFI Voice Line Number", "editvoicelineno", editvoicelineno);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno,  "RFI Voice Line Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.editvoicelineno, editvoicelineno,  "RFI Voice Line Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.editorder_okbutton,"OK");
		click(APT_VoiceLineObj.APT_VoiceLine.editorder_okbutton, "OK");
		
		waitforPagetobeenable();
		waitforPagetobeenable();
		verifysuccessmessage( "Order successfully updated");
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);
		waitforPagetobeenable();

		if(editorderno.equalsIgnoreCase("Null")) {

			ExtentTestManager.getTest().log(LogStatus.PASS, "'Order/Contract Number (Parent SID)' field is not edited");
			Report.LogInfo("INFO","'Order/Contract Number (Parent SID)' field is not edited","PASS");
		}else {
			compareText( "Order Number", APT_VoiceLineObj.APT_VoiceLine.ordernumbervalue, editorderno);
		}

		if(editvoicelineno.equalsIgnoreCase("Null")) {
			ExtentTestManager.getTest().log(LogStatus.PASS,"'RFI/RFQ/IP Voice Line Number' field is not edited");
			Report.LogInfo("INFO","'RFI/RFQ/IP Voice Line Number' field is not edited","PASS");
		}else {
			compareText( "RFI Voice Line Number", APT_VoiceLineObj.APT_VoiceLine.ordervoicelinenumbervalue, editvoicelineno);
		}
		ExtentTestManager.getTest().log(LogStatus.INFO,"------ Edit Order is successful ------");
		Report.LogInfo("INFO","------ Edit Order is successful ------","INFO");
	}

}

public void verifyorderpanel_changeorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String ChangeOrder_newOrderNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ChangeOrder_OrderNumber");
	String changevoicelineno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ChangeOrder_VoicelineNumber");
	String changeOrderSelection_newOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"changeOrderSelection_newOrder");
	String changeOrderSelection_existingOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"changeOrderSelection_existingOrder");
	String ChangeOrder_existingOrderNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ChangeOrder_existingOrderNumber");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Change Order' Functionality");

	if((changeOrderSelection_newOrder.equalsIgnoreCase("No")) && (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "Change Order is not performed");
		Report.LogInfo("INFO","Change Order is not performed","PASS");
	}
	else if(changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {

		//Change Order
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.orderactionbutton,"Action dropdown");
		click( APT_VoiceLineObj.APT_VoiceLine.orderactionbutton,"Action dropdown");
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeorderlink,"Change Order");
		click( APT_VoiceLineObj.APT_VoiceLine.changeorderlink,"Change Order");
	
		waitforPagetobeenable();
		
		compareText( "Change Order header",  APT_VoiceLineObj.APT_VoiceLine.changeorderheader, "Change Order");
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeorder_selectorderswitch,"Select order switch");
		click( APT_VoiceLineObj.APT_VoiceLine.changeorder_selectorderswitch,"Select order switch");
		
		waitforPagetobeenable();
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeordernumber,"Order Number");
		click( APT_VoiceLineObj.APT_VoiceLine.changeordernumber,"Order Number");
		
		waitforPagetobeenable();
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeordernumber,"Order Number");
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.changeordernumber,ChangeOrder_newOrderNumber,"Order Number");
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeordervoicelinenumber,"RFI Voice Line Number");
		click( APT_VoiceLineObj.APT_VoiceLine.changeordervoicelinenumber,"RFI Voice Line Number");
		
		waitforPagetobeenable();
		//verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeordervoicelinenumber,"RFI Voice Line Number");
		//sendKeys( APT_VoiceLineObj.APT_VoiceLine.changeordervoicelinenumber,changevoicelineno,"RFI Voice Line Number");
		addtextFields_commonMethod( "RFI Voice Line Number", APT_VoiceLineObj.APT_VoiceLine.changeordervoicelinenumber, changevoicelineno);
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.createorder_button,"Create Order");
		click( APT_VoiceLineObj.APT_VoiceLine.createorder_button,"Create Order");
		//click_commonMethod( "Create Order", "createorder_button");
		waitforPagetobeenable();
		
		verifysuccessmessage( "Order successfully changed.");
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);
		
		waitforPagetobeenable();
		
		//compareText( "Order Number", APT_VoiceLineObj.APT_VoiceLine.ordernumbervalue, ChangeOrder_newOrderNumber);
		//compareText( "RFI Voice Line Number", APT_VoiceLineObj.APT_VoiceLine.ordervoicelinenumbervalue, changevoicelineno);
		Report.LogInfo("INFO","------ Change Order is successful ------","INFO");
	}
	else if(changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) 
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Performing Change Order functionality");
		Report.LogInfo("INFO","Performing Change Order functionality","PASS");
				
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);
		waitforPagetobeenable();
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.orderactionbutton,"Action dropdown");
		click( APT_VoiceLineObj.APT_VoiceLine.orderactionbutton,"Action dropdown");
		//click_commonMethod( "Action dropdown", "orderactionbutton");
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeorderlink,"Change Order");
		click( APT_VoiceLineObj.APT_VoiceLine.changeorderlink,"Change Order");
		//click_commonMethod( "Change Order", "changeorderlink");
		waitforPagetobeenable();
		compareText( "Change Order header",APT_VoiceLineObj.APT_VoiceLine.changeorderheader, "Change Order");
		waitforPagetobeenable();

		addDropdownValues_commonMethod( "Order/Contract Number (Parent SID)", APT_VoiceLineObj.APT_VoiceLine.changeorder_chooseorderdropdown, ChangeOrder_existingOrderNumber);

		verifyExists( APT_VoiceLineObj.APT_VoiceLine.changeorder_okbutton,"OK");
		click( APT_VoiceLineObj.APT_VoiceLine.changeorder_okbutton,"OK");
		//click_commonMethod( "OK", "changeorder_okbutton");
		
		waitforPagetobeenable();
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.userspanel_header);
		waitforPagetobeenable();
		
		compareText( "Order Number", APT_VoiceLineObj.APT_VoiceLine.ordernumbervalue, ChangeOrder_existingOrderNumber);
		compareText( "RFI Voice Line Number", APT_VoiceLineObj.APT_VoiceLine.ordervoicelinenumbervalue, changevoicelineno);
		
		Report.LogInfo("INFO","------ Change Order is successful ------","INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "------ Change Order is successful ------");
		

	}

}

public void verifyservicepanelInformationinviewservicepage(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String servicetype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String Remarks=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Remarks");
	String resellercodevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerCode_Value");
	String thirdpartyinternet_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyInternet_Checkbox");
	String phonecontact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PhoneContact");
	
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	compareText( "Service panel Header", APT_VoiceLineObj.APT_VoiceLine.servicepanel_header, "Service");
	compareText( "Service Identification", APT_VoiceLineObj.APT_VoiceLine.servicepanel_serviceidentificationvalue, sid);
	compareText( "Service Type", APT_VoiceLineObj.APT_VoiceLine.servicepanel_servicetypevalue, servicetype);
	compareText( "Reseller Code", APT_VoiceLineObj.APT_VoiceLine.servicepanel_resellercode, resellercodevalue);
	//GetText( "Email", "servicepanel_email");
	compareText( "Phone Contact", APT_VoiceLineObj.APT_VoiceLine.servicepanel_phonecontact, phonecontact);
	compareText( "3rd Party Internet", APT_VoiceLineObj.APT_VoiceLine.servicepanel_thirdpartyinternet, thirdpartyinternet_checkbox);
	compareText( "Remarks", APT_VoiceLineObj.APT_VoiceLine.servicepanel_remarksvalue, Remarks);
}


public void verifyEditService(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
	String EditRemarks=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditRemarks");
	String Remarks=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Remarks");
	String changeorderno =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ChangeOrder_OrderNumber");
	String sid =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String servicetype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String servicestatus =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceStatus");
	String syncstatus =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"syncstatus");
	String servicestatuschangerequired =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceStatusChangeRequired");
	String resellercodevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerCode_Value");
	String thirdpartyinternet_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyInternet_Checkbox");
	String phonecontact =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PhoneContact");
	String edit_resellercodevalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_ResellerCode");
	String edit_thirdpartyinternet_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_ThirdPartyInternet_Checkbox");
	String edit_email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_ServiceEmail");
	String edit_phonecontact =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_PhoneContact");
	String edit_performancereporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_PerformanceReporting_Checkbox"); 
	String edit_proactivenotification_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_ProactiveNotification_Checkbox");
	String edit_notificationmanagementteam_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_NotificationManagementTeam_Drodpwon");
	
	//Cancel edit service
	waitforPagetobeenable();
	waitforPagetobeenable();
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdown");
	click( APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdown");
	//click_commonMethod( "Action dropdown", "serviceactiondropdown");
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.edit,"Edit");
	click( APT_VoiceLineObj.APT_VoiceLine.edit,"Edit");
	//click_commonMethod( "Edit", "edit");
	waitforPagetobeenable();
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.cancelbutton);
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.cancelbutton,"Cancel");
	click( APT_VoiceLineObj.APT_VoiceLine.cancelbutton,"Cancel");
	//click_commonMethod( "Cancel", "cancelbutton");
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);

	//Edit service
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdown");
	click( APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdown");
	//click_commonMethod( "Action dropdown", "serviceactiondropdown");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.edit,"Edit");
	click(APT_VoiceLineObj.APT_VoiceLine.edit,"Edit");
	
	waitforPagetobeenable();
	
	String ServiceIdentification= findWebElement(APT_VoiceLineObj.APT_VoiceLine.serviceidentificationtextfield).getAttribute("value");
	
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service Identification number is displayed as: "+ServiceIdentification);
	Report.LogInfo("INFO","Service Identification number is displayed as: "+ServiceIdentification,"PASS");
	
	//GetText( "Service Type", "servicetypevalue");
	if(!edit_resellercodevalue.equalsIgnoreCase("null"))
	{
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield); // "Reseller Code", 
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield,"Reseller Code");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield,edit_resellercodevalue,"Reseller Code");
		//edittextFields_commonMethod( "Reseller Code", "resellercode_textfield", edit_resellercodevalue);
	}
	else
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield,"Reseller Code");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.resellercode_textfield,edit_resellercodevalue,"Reseller Code");
		//edittextFields_commonMethod( "Reseller Code", "resellercode_textfield", edit_resellercodevalue);
	}
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.servicepage_thirdpartyinternet_checkbox,"3rd Party Internet");
	//sendKeys(APT_VoiceLineObj.APT_VoiceLine.servicepage_thirdpartyinternet_checkbox,edit_thirdpartyinternet_checkbox,"3rd Party Internet");
	editcheckbox_commonMethod( edit_thirdpartyinternet_checkbox, APT_VoiceLineObj.APT_VoiceLine.servicepage_thirdpartyinternet_checkbox, "3rd Party Internet");
	
	if(!EditRemarks.equalsIgnoreCase("null"))
	{
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.remarktextarea);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.remarktextarea,"Remarks");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.remarktextarea,EditRemarks,"Remarks");
		
		//edittextFields_commonMethod( "Remarks", "remarktextarea", EditRemarks);
	}
	else
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.remarktextarea,"Remarks");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.remarktextarea,EditRemarks,"Remarks");
		//edittextFields_commonMethod( "Remarks", "remarktextarea", EditRemarks);
	}
	//Edit email
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.servicetypevalue);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.selectedemail,"Selected Email");
	click(APT_VoiceLineObj.APT_VoiceLine.selectedemail,"Selected Email");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.emailremovearrow,"Email remove arrow");
	click(APT_VoiceLineObj.APT_VoiceLine.emailremovearrow,"Email remove arrow");
	//click_commonMethod( "Email remove arrow", "emailremovearrow");
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.emailtextfieldvalue,"Email");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.emailtextfieldvalue,edit_email,"Email");
	//edittextFields_commonMethod( "Email", "emailtextfieldvalue", edit_email);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.emailaddarrow,"Email adding arrow");
	click(APT_VoiceLineObj.APT_VoiceLine.emailaddarrow,"Email adding arrow");
	//click_commonMethod( "Email adding arrow", "emailaddarrow");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	//Edit phone contact
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.selectedphone,"Selected phone contact");
	click(APT_VoiceLineObj.APT_VoiceLine.selectedphone,"Selected phone contact");
	//click_commonMethod( "Selected phone contact", "selectedphone");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.phoneremovearrow,"Phonecontact remove arrow");
	click(APT_VoiceLineObj.APT_VoiceLine.phoneremovearrow,"Phonecontact remove arrow");
	//click_commonMethod( "Phonecontact remove arrow", "phoneremovearrow");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.phonecontacttextfieldvalue,"Phone Contact");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.phonecontacttextfieldvalue,edit_phonecontact,"Phone Contact");
	//edittextFields_commonMethod( "Phone Contact", "phonecontacttextfieldvalue", edit_phonecontact);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.phoneaddarrow,"phonecontact adding Arrow");
	click(APT_VoiceLineObj.APT_VoiceLine.phoneaddarrow,"phonecontact adding Arrow");
	//click_commonMethod( "phonecontact adding Arrow", "phoneaddarrow");
	waitforPagetobeenable();

	compareText( "Management Options", APT_VoiceLineObj.APT_VoiceLine.managementoptions_header, "Management Options");
	String Package= findWebElement(APT_VoiceLineObj.APT_VoiceLine.packagefield).getAttribute("disabled");
	
	if(Package!=null)
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Package dropdown field is disabled as expected");
		Report.LogInfo("INFO","Step : Package dropdown field is disabled as expected","PASS");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Package dropdown field is not disabled");
		Report.LogInfo("INFO","Step : Package dropdown field is not disabled","FAIL");
	}
	String ManagedService_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.managedservice_checkbox).getAttribute("disabled");
	String ManagedService_Checked= findWebElement(APT_VoiceLineObj.APT_VoiceLine.managedservice_checkbox).getAttribute("checked");
	
	if(ManagedService_Disabled!=null && ManagedService_Checked!=null)
	{
		Report.LogInfo("INFO","Step : Managed Service Checkbox is disabled & checked by default as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Managed Service Checkbox is disabled & checked by default as expected");
	}
	else
	{
		Report.LogInfo("INFO","Step : Managed Service Checkbox is not disabled & checked by default","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Managed Service Checkbox is not disabled & checked by default");
	}
	//syslog event view checkbox
	String SyslogEvent_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.syslogevent_checkbox).getAttribute("disabled");
	if(SyslogEvent_Disabled!=null)
	{
		Report.LogInfo("INFO","Step : Syslog Event View Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Syslog Event View Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Step : Syslog Event View Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Syslog Event View Checkbox is not disabled");
	}
	//service status view checkbox
	String ServiceStatusView_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.servicestatusview_checkbox).getAttribute("disabled");
	if(ServiceStatusView_Disabled!=null)
	{
		Report.LogInfo("INFO","Step : Service Status View Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service Status View Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Step : Service Status View Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Service Status View Checkbox is not disabled");
	}
	//Router configuration view checkbox
	String RouterConfigurationView_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.routerconfigview_checkbox).getAttribute("disabled");
	if(RouterConfigurationView_Disabled!=null)
	{
		Report.LogInfo("INFO","Step : Router Configuration View Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Router Configuration View Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Step : Router Configuration View Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Router Configuration View Checkbox is not disabled");
	}
	editcheckbox_commonMethod( edit_performancereporting_checkbox, APT_VoiceLineObj.APT_VoiceLine.performancereporting_checkbox, "Performance Reporting");
	editcheckbox_commonMethod( edit_proactivenotification_checkbox, APT_VoiceLineObj.APT_VoiceLine.proactivenotification_checkbox, "Pro-active Notification");
	if(edit_proactivenotification_checkbox.equalsIgnoreCase("Yes"))
	{
		addDropdownValues_commonMethod( "Notification Management Team", "notificationmanagementteam_dropdown", edit_notificationmanagementteam_value);
	}
	//Dial user administration checkbox
	String DialUserAdministration_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.dialuseradministration_checkbox).getAttribute("disabled");
	if(DialUserAdministration_Disabled!=null)
	{
		Report.LogInfo("INFO","Step :  Dial User Administration Checkbox is disabled as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Dial User Administration Checkbox is disabled as expected");
	}
	else
	{
		Report.LogInfo("INFO","Step :  Dial User Administration Checkbox is not disabled","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Dial User Administration Checkbox is not disabled");
	}
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	//click_commonMethod( "OK", "okbutton");
	waitforPagetobeenable();
//	waitForpageload();
	waitforPagetobeenable();
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.customerdetailsheader).isDisplayed())
	{
		Report.LogInfo("INFO","Navigated to view service page","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Navigated to view service page");
		
		//Log.info("Navigated to view service page");
		verifysuccessmessage( "Service successfully updated");
	}
	else
	{
		Report.LogInfo("INFO","Service not updated","FAIL");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Service not updated");
		//Log.info("Service not updated");
		
	}
}

public void verifyManageSubnetsIPv6() throws InterruptedException, IOException {

	//manage subnets IPv6
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.managesubnetsipv6_link, "Manage Subnets Ipv6");
	click(APT_VoiceLineObj.APT_VoiceLine.managesubnetsipv6_link, "Manage Subnets Ipv6");
	//click_commonMethod( "Manage Subnets Ipv6", "managesubnetsipv6_link");
	waitforPagetobeenable();
	waitforPagetobeenable();
	
	getTextFrom(APT_VoiceLineObj.APT_VoiceLine.managesubnet_header,"manage subnet_header");
	
	compareText( "Manage subnet message", APT_VoiceLineObj.APT_VoiceLine.managesubnet_successmsg, "There are no subnets to be managed for this service.");
	compareText( "Space Name", APT_VoiceLineObj.APT_VoiceLine.spacename_column, "Space Name");
	compareText( "Block Name", APT_VoiceLineObj.APT_VoiceLine.blockname_column, "Block Name");
	compareText( "Subnet Name", APT_VoiceLineObj.APT_VoiceLine.subnetname_column, "Subnet Name");
	compareText( "Start Address", APT_VoiceLineObj.APT_VoiceLine.startaddress_column, "Start Address");
	compareText( "Size", APT_VoiceLineObj.APT_VoiceLine.size_column, "Size");
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.closesymbol, "Close");
	click(APT_VoiceLineObj.APT_VoiceLine.closesymbol, "Close");
	//click_commonMethod( "Close", "closesymbol");
	waitforPagetobeenable();
	waitforPagetobeenable();
}

public void verifyShowNewInfovistaReport() throws Exception {
	//Show new infovista report
	shownewInfovista();
	waitforPagetobeenable();
}

public void shownewInfovista() throws Exception {

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	//click_commonMethod("Action dropdown", "serviceactiondropdown");
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.shownewinfovistareport_link, "Show New Infovista Report");
	click(APT_VoiceLineObj.APT_VoiceLine.shownewinfovistareport_link, "Show New Infovista Report");
	//click_commonMethod("Show New Infovista Report", "shownewinfovistareport_link");
	waitforPagetobeenable();

	String expectedPageName= "SSO Login Page";

	//Switch to new tab
	List<String> browserTabs = new ArrayList<String> (webDriver.getWindowHandles());
	webDriver.switchTo().window(browserTabs .get(1));
	waitforPagetobeenable();

	try { 
		// Get Tab name
		String pageTitle=webDriver.switchTo().window(browserTabs .get(1)).getTitle();
		//System.out.println("page title displays as: "+pageTitle);

		assertEquals(pageTitle, expectedPageName, " on clicking 'Show Infovista link', it got naviagted to "+pageTitle);

		waitforPagetobeenable();
		webDriver.close();
		webDriver.switchTo().window(browserTabs.get(0)); 

		Report.LogInfo("INFO","on clicking 'Show Infovista link', it got naviagted to "+ pageTitle + " as expected","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "on clicking 'Show Infovista link', it got naviagted to "+ pageTitle + " as expected");
		waitforPagetobeenable();

		Report.LogInfo("INFO","show info vista page actual title: "+pageTitle,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "show info vista page actual title: "+pageTitle );
		
		Report.LogInfo("INFO","show info vista page expected title: "+ expectedPageName,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "show info vista page expected title: "+ expectedPageName);

	}catch(Exception e) {

		e.printStackTrace();

		waitforPagetobeenable();
		webDriver.close();
		webDriver.switchTo().window(browserTabs.get(0));

		Report.LogInfo("INFO", expectedPageName + " page is not displaying","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, expectedPageName + " page is not displaying");

	}
}

public void verifyDump() throws InterruptedException, IOException {
	//dump
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	//click_commonMethod( "Action dropdown", "serviceactiondropdown");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.dump_link, "Dump");
	click(APT_VoiceLineObj.APT_VoiceLine.dump_link, "Dump");
	//click_commonMethod( "Dump", "dump_link");
	waitforPagetobeenable();
	waitforPagetobeenable();
	
	getTextFrom( APT_VoiceLineObj.APT_VoiceLine.dumppage_header,"dumppage_header");
	getTextFrom( APT_VoiceLineObj.APT_VoiceLine.serviceretrieved_text,"serviceretrieved_text");
	
	compareText( "Service header", APT_VoiceLineObj.APT_VoiceLine.service_header, "Service");
	
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.closesymbol, "Close");
	click(APT_VoiceLineObj.APT_VoiceLine.closesymbol, "Close");
	//click_commonMethod( "Close", "closesymbol");
	waitforPagetobeenable();
}

public void verifyManageService(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException {

	String changeorderno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ChangeOrder_OrderNumber");
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String servicetype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String servicestatus=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceStatus");
	String syncstatus=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"syncstatus");
	String servicestatuschangerequired=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceStatusChangeRequired");
	
	//Manage service
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown, "Action dropdown");
	//click_commonMethod( "Action dropdown", "serviceactiondropdown");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.manageLink, "Manage");
	click(APT_VoiceLineObj.APT_VoiceLine.manageLink, "Manage");
	//click_commonMethod( "Manage", "manageLink");
	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollUp();
	
	compareText( "Manage service header", APT_VoiceLineObj.APT_VoiceLine.manageservice_header, "Manage Service");
	//compareText( "Order Name", APT_VoiceLineObj.APT_VoiceLine.status_ordername, changeorderno);
	compareText( "Service Identification", APT_VoiceLineObj.APT_VoiceLine.status_servicename, sid);
	//compareText( "Service type", APT_VoiceLineObj.APT_VoiceLine.status_servicetype, servicetype);
	
	String ServiceDetails_value = findWebElement(APT_VoiceLineObj.APT_VoiceLine.status_servicedetails).getText();
	if(ServiceDetails_value.isEmpty())
	{
		Report.LogInfo("INFO", "Service Details column value is empty as expected","INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Service Details column value is empty as expected");
		
		//Log.info("Service Details column value is empty as expected");
		
	}
	else
	{
		Report.LogInfo("INFO", "Service Details column value should be empty","INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Service Details column value should be empty");
		
		//Log.info("Service Details column value should be empty");
	}
	compareText( "Service Status", APT_VoiceLineObj.APT_VoiceLine.status_currentstatus, servicestatus);

	String LastModificationTime_value = getTextFrom(APT_VoiceLineObj.APT_VoiceLine.status_modificationtime,"status_modificationtime");
	if(LastModificationTime_value.contains("BST"))
	{
		Report.LogInfo("INFO", "Service status is displayed as : " + LastModificationTime_value,"INFO");
		//Log.info("Service status is displayed as : " + LastModificationTime_value);
		ExtentTestManager.getTest().log(LogStatus.INFO, "Service status is displayed as : " + LastModificationTime_value);
	}
	else
	{
		Report.LogInfo("INFO", "Incorrect modification time format","INFO");
		//Log.info("Incorrect modification time format");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Incorrect modification time format");
	}
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.statuslink, "Status");
	click(APT_VoiceLineObj.APT_VoiceLine.statuslink, "Status");
	//click_commonMethod( "Status", "statuslink");
	waitforPagetobeenable();
	
	if(servicestatuschangerequired=="Yes")
	{
		WebElement ServiceStatusPage= findWebElement(APT_VoiceLineObj.APT_VoiceLine.Servicestatus_popup);
		if(ServiceStatusPage.isDisplayed())
		{
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.changestatus_dropdown, "Change Status");
			click(APT_VoiceLineObj.APT_VoiceLine.changestatus_dropdown, "Change Status");
			//click_commonMethod( "Change Status", "changestatus_dropdown");
			
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.changestatus_dropdownvalue, "Change Status value");
			click(APT_VoiceLineObj.APT_VoiceLine.changestatus_dropdownvalue, "Change Status value");
			//click_commonMethod( "Change Status value", "changestatus_dropdownvalue");
			
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
			click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
			//click_commonMethod( "OK", "okbutton");
			WebElement ServiceStatusHistory= findWebElement(APT_VoiceLineObj.APT_VoiceLine.servicestatushistory);
			try
			{
				if(ServiceStatusHistory.isDisplayed())
				{
					Report.LogInfo("INFO", "Service status change request logged","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change request logged");
				}
				else
					Report.LogInfo("INFO", "Service status change request is not logged","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change request is not logged");
			}
			catch(StaleElementReferenceException e)
			{
				Report.LogInfo("INFO", "No service history to display","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No service history to display");
			}
		}
		else
		{
			Report.LogInfo("INFO", "Status link is not working","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Status link is not working");
		}
	}
	else
	{
		Report.LogInfo("INFO", "Service status change not reqired","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change not reqired");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.servicestatus_popupclose, "Close");
		click(APT_VoiceLineObj.APT_VoiceLine.servicestatus_popupclose, "Close");
	}

	//synchronize panel in manage service page

	//compareText( "Order Name", APT_VoiceLineObj.APT_VoiceLine.sync_ordername, changeorderno);
	compareText( "Service Identification", APT_VoiceLineObj.APT_VoiceLine.sync_servicename, sid);
	//compareText( "Service type", APT_VoiceLineObj.APT_VoiceLine.sync_servicetype, servicetype);

	String ServiceDetails_value1 = getTextFrom(APT_VoiceLineObj.APT_VoiceLine.sync_servicedetails,"sync_servicedetails");
	if(ServiceDetails_value1.isEmpty())
	{
		Report.LogInfo("INFO", "Service Details column value is empty as expected","INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Service Details column value is empty as expected");
		//Log.info("Service Details column value is empty as expected");
		//System.out.println("Service Details column value is empty as expected");
	}
	else
	{
		Report.LogInfo("INFO", "Service Details column value should be empty","INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Service Details column value should be empty");
		
		//Log.info("Service Details column value should be empty");
		//System.out.println("Service Details column value should be empty");
	}

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.synchronizelink);
	getTextFrom( APT_VoiceLineObj.APT_VoiceLine.sync_status,"sync_status"); // "Sync Status"
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.synchronizelink, "Synchronize");
	click( APT_VoiceLineObj.APT_VoiceLine.synchronizelink, "Synchronize");
	
	waitforPagetobeenable();
	
	verifysuccessmessage( "Sync started successfully. Please check the sync status of this service.");
	
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	waitforPagetobeenable();
	
	waitforPagetobeenable();

}


public void clickOnBreadCrump(String sid) throws IOException, InterruptedException 
{

	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();	
	String breadcrumb = null;

	try {
		//breadcrumb = getTextFrom(APT_VoiceLineObj.APT_VoiceLine.breadcrumb).replace("value", sid);
				
		
		if (isElementPresent(breadcrumb))
				{
			click(APT_VoiceLineObj.APT_VoiceLine.breadcrumb1+sid+APT_VoiceLineObj.APT_VoiceLine.breadcrumb2, "Breadcrumb clicked");
		} else {
			Report.LogInfo("INFO","Breadcrumb is not displaying for the element " + breadcrumb,"INFO");
			//Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	} catch (Exception e)
	{
		e.printStackTrace();
		Report.LogInfo("INFO","Breadcrumb is not displaying for the element " + breadcrumb,"FAIL");
		//Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
	}
}

public void verifyManagementOptionspanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException {


	String performancereporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerformanceReporting_Checkbox");
	String proactivenotification_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ProactiveNotification_Checkbox");
	String edit_performancereporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_PerformanceReporting_Checkbox");
	String edit_proactivenotification_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_ProactiveNotification_Checkbox");
	
	scrollDown (APT_VoiceLineObj.APT_VoiceLine.managementoptions_header);
	waitforPagetobeenable();	
	compareText( "Management options header", APT_VoiceLineObj.APT_VoiceLine.managementoptions_header, "Management Options");
	compareText( "Managed Service", APT_VoiceLineObj.APT_VoiceLine.viewservice_managedservicevalue, "Yes");
	compareText( "Syslog Event View", APT_VoiceLineObj.APT_VoiceLine.viewservice_syslogeventview, "No");
	compareText( "Service Status View", APT_VoiceLineObj.APT_VoiceLine.viewservice_servicestatusview, "No");
	compareText( "Router Configuration View", APT_VoiceLineObj.APT_VoiceLine.viewservice_routerconfigview, "No");
	if(edit_performancereporting_checkbox.equalsIgnoreCase("null"))
	{
		compareText( "Performance Reporting", APT_VoiceLineObj.APT_VoiceLine.viewservice_performancereporting, performancereporting_checkbox);
	}
	else
	{
		compareText( "Performance Reporting", APT_VoiceLineObj.APT_VoiceLine.viewservice_performancereporting, edit_performancereporting_checkbox);
	}
	if(edit_proactivenotification_checkbox.equalsIgnoreCase("null"))
	{
		compareText( "Pro-active Notification", APT_VoiceLineObj.APT_VoiceLine.viewservice_proactivenotification, proactivenotification_checkbox);
	}
	else
	{
		compareText( "Pro-active Notification", APT_VoiceLineObj.APT_VoiceLine.viewservice_proactivenotification, edit_proactivenotification_checkbox);
	}
	compareText( "Dial User Administration", APT_VoiceLineObj.APT_VoiceLine.viewservice_dialuseradministration, "No");

}

public void verifyAddASRDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
{
	
	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	waitforPagetobeenable();
	waitforPagetobeenable();

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	waitforPagetobeenable();
	compareText( "ASR Device header", APT_VoiceLineObj.APT_VoiceLine.asrdevice_header, "ASR Device");
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.adddevice_link, "Add ASR Device link");
	click(APT_VoiceLineObj.APT_VoiceLine.adddevice_link, "Add ASR Device link");
	waitforPagetobeenable();
	waitforPagetobeenable();
	compareText( "Add ASR Device header", APT_VoiceLineObj.APT_VoiceLine.adddevice_header, "Add ASR Device");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	//click_commonMethod( "OK", "okbutton");
	waitforPagetobeenable();
	
	click(APT_VoiceLineObj.APT_VoiceLine.imspoplocation_dropdown,"imspoplocation_dropdown");
	addDropdownValues_commonMethod( "IMS POP Location", APT_VoiceLineObj.APT_VoiceLine.imspoplocation_dropdown, imspoplocation_dropdownvalue);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	waitforPagetobeenable();
	waitforPagetobeenable();
	verifysuccessmessage( "ASR Device added successfully");

	//Added device
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	waitforPagetobeenable();
	
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevicename);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();

		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			
			//String AddedDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink).replace("value", AddedDevice_SNo).getText();
			String AddedDevice_ViewLink=getTextFrom(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink2,"Add Device");
			
			//String AddedDevice_EditLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_editlink).replace("value", AddedDevice_SNo).getText();
			String AddedDevice_EditLink=getTextFrom(APT_VoiceLineObj.APT_VoiceLine.addeddevice_editlink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_editlink2,"Edit link");
			
			//String AddedDevice_DeletefromServiceLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_Deletefromservicelink).replace("value", AddedDevice_SNo).getText();
			String AddedDevice_DeletefromServiceLink=getTextFrom(APT_VoiceLineObj.APT_VoiceLine.addeddevice_Deletefromservicelink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_Deletefromservicelink2,"Edit link");
			
			//String AddedDevice_SelectInterfacesLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_SelectInterfaceslink).replace("value", AddedDevice_SNo).getText();
			String AddedDevice_SelectInterfacesLink=getTextFrom(APT_VoiceLineObj.APT_VoiceLine.addeddevice_SelectInterfaceslink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_SelectInterfaceslink2,"Select interface ");
			
			if(imspoplocation_dropdownvalue.equalsIgnoreCase("Bangalore-1"))
			{
				//verify view link
				if(AddedDevice_ViewLink.contains("View"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "View link is displaying as expected for added device");
					Report.LogInfo("INFO","View link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "View link is not displaying for added device");
					Report.LogInfo("INFO","View link is not displaying for added device","FAIL");
				}
				//verify Edit link
				if(AddedDevice_EditLink.contains("Edit"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Edit link is displaying as expected for added device");
					Report.LogInfo("INFO","Edit link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit link is not displaying for added device");
					Report.LogInfo("INFO","Edit link is not displaying for added device","FAIL");
				}
				//verify Delete from Service link
				if(AddedDevice_DeletefromServiceLink.contains("Delete from Service"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Delete from Service link is displaying as expected for added device");
					Report.LogInfo("INFO","Delete from Service link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete from Service link is not displaying for added device");
					Report.LogInfo("INFO","Delete from Service link is not displaying for added device","FAIL");
				}
				//verify Select Interfaces link
				if(AddedDevice_SelectInterfacesLink.contains("Select Interfaces"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Select Interfaces link is displaying as expected for added device");
					Report.LogInfo("INFO","Select Interfaces link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Select Interfaces link is not displaying for added device");
					Report.LogInfo("INFO", "Select Interfaces link is not displaying for added device","FAIL");
				}

			}
			else if(imspoplocation_dropdownvalue.equalsIgnoreCase("Paris-1"))
			{
				//verify view link
				if(AddedDevice_ViewLink.contains("View"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "View link is displaying as expected for added device");
					Report.LogInfo("INFO","View link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "View link is not displaying for added device");
					Report.LogInfo("INFO", "View link is not displaying for added device","FAIL");
					
				}
				//verify Edit link
				if(AddedDevice_EditLink.contains("Edit"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Edit link is displaying as expected for added device");
					Report.LogInfo("INFO","Edit link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit link is not displaying for added device");
					Report.LogInfo("INFO", "Edit link is not displaying for added device","FAIL");
				}
				//verify Delete from Service link
				if(AddedDevice_DeletefromServiceLink.contains("Delete from Service"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Delete from Service link is displaying as expected for added device");
					Report.LogInfo("INFO","Delete from Service link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete from Service link is not displaying for added device");
					Report.LogInfo("INFO", "Delete from Service link is not displaying for added device","FAIL");
				}
				//verify Select Interfaces link
				if(AddedDevice_SelectInterfacesLink.contains("Select Interfaces"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Select Interfaces link is displaying as expected for added device");
					Report.LogInfo("INFO","Select Interfaces link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Select Interfaces link is not displaying for added device");
					Report.LogInfo("INFO", "Select Interfaces link is not displaying for added device","FAIL");
					
				}
			}
			else if(imspoplocation_dropdownvalue.equalsIgnoreCase("Frankfurt-1"))
			{
				//verify view link
				if(AddedDevice_ViewLink.contains("View"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "View link is displaying as expected for added device");
					Report.LogInfo("INFO","View link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "View link is not displaying for added device");
					Report.LogInfo("INFO", "View link is not displaying for added device","FAIL");
					
				}
				//verify Edit link
				if(AddedDevice_EditLink.contains("Edit"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Edit link is displaying as expected for added device");
					Report.LogInfo("INFO","Edit link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit link is not displaying for added device");
					Report.LogInfo("INFO",  "Edit link is not displaying for added device","FAIL");
				}
				//verify Delete from Service link
				if(AddedDevice_DeletefromServiceLink.contains("Delete from Service"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Delete from Service link is displaying as expected for added device");
					Report.LogInfo("INFO","Delete from Service link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete from Service link is not displaying for added device");
					Report.LogInfo("INFO", "Delete from Service link is not displaying for added device","FAIL");
				}
				//verify Select Interfaces link
				if(AddedDevice_SelectInterfacesLink.contains("Select Interfaces"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Select Interfaces link is displaying as expected for added device");
					Report.LogInfo("INFO","Select Interfaces link is displaying as expected for added device","PASS");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Select Interfaces link is not displaying for added device");
					Report.LogInfo("INFO", "Select Interfaces link is not displaying for added device","FAIL");
				}
			}

		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		Report.LogInfo("INFO", "No Device added in grid","FAIL");
		
	}

}
public static String ASRDeviceNameValue, ASRManagementAddressValue, ASRDeviceCountryValue;

public void verifyEditASRDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	String edit_asrdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editASRDeviceName");
	String edit_asrmanagementaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editASRManagementAddress");
	String editCountry=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
	String editExistingCity =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
	String editExistingCityValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
	String editExistingSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
	String editExistingSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
	String editExistingPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
	String editExistingPremiseValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
	String editNewCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
	String editNewSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
	String editNewPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
	String editNewCityName =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
	String editNewCityCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
	String editNewSiteName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
	String editNewSiteCode =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
	String editNewPremiseName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
	String editNewPremiseCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
	
	String ASRDeviceName=null;
	String ASRManagementAddress=null;
	String ASRDevice_Country=null;

	waitforPagetobeenable();
	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	
	//System.out.println(imsPopLocationValue(imspoplocation_dropdownvalue));
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		
		List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevicename);
	
		int AddedDevicesCount= addeddevicesList.size();
		

		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
		
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			
			//String IMSPopLocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
			
			String IMSPopLocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
			if(AddedDeviceNameText.contains(IMSPopLocation_Code))
			{
				//WebElement AddedDevice_EditLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_editlink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_editlink2);
						//.replace("value", AddedDevice_SNo);
				//AddedDevice_EditLink.click();
				click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_editlink,"Clicked on edit link");
				waitforPagetobeenable();
				compareText("Edit ASR Device header", APT_VoiceLineObj.APT_VoiceLine.editasrdevice_header, "Edit ASR Device");
				if(!edit_asrdevicename.equalsIgnoreCase("null"))
				{
					edittextFields_commonMethod("Name", APT_VoiceLineObj.APT_VoiceLine.edit_asrdevicename, edit_asrdevicename);
				}
				else
				{
					ASRDeviceName= findWebElement(APT_VoiceLineObj.APT_VoiceLine.edit_asrdevicename).getAttribute("value");
					compareText_fromtextFields("Name", APT_VoiceLineObj.APT_VoiceLine.edit_asrdevicename, ASRDeviceName);
				}

				compareText_fromActualvalue( "Vendor/Model", APT_VoiceLineObj.APT_VoiceLine.edit_asrvendormodel, "Cisco 12016");

				if(!edit_asrmanagementaddress.equalsIgnoreCase("null"))
				{
					edittextFields_commonMethod( "Management Address", APT_VoiceLineObj.APT_VoiceLine.edit_managementaddressvalue, edit_asrmanagementaddress);
				}
				else
				{
					ASRManagementAddress= findWebElement(APT_VoiceLineObj.APT_VoiceLine.edit_managementaddressvalue).getAttribute("value");
					compareText_fromtextFields( "Management Address", APT_VoiceLineObj.APT_VoiceLine.edit_managementaddressvalue, ASRManagementAddress);
				}

				compareText_fromtextFields( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.snmpro_textfield, "inccc");

				//select country
				waitforPagetobeenable();
				if(!editCountry.equalsIgnoreCase("null"))
				{
					addDropdownValues_commonMethod( "Country", APT_VoiceLineObj.APT_VoiceLine.countryinput, editCountry);
				}
				else
				{
					ASRDevice_Country= findWebElement(APT_VoiceLineObj.APT_VoiceLine.countryinput).getText();
					compareText( "Country", APT_VoiceLineObj.APT_VoiceLine.countryinput, ASRDevice_Country);
				}
				scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
				//New City		
				if(editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
					
					verifyExists( APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"Add Toggle");
					click( APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"Add Toggle");
					//City name
					edittextFields_commonMethod( "City Name",  APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, editNewCityName);
					//City Code	
					edittextFields_commonMethod( "City Code",  APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, editNewCityCode);
					//Site name
					edittextFields_commonMethod( "Site Name",  APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, editNewSiteName);
					//Site Code
					edittextFields_commonMethod( "Site Code",  APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addCityToggleSelected, editNewSiteCode);
					//Premise name	
					edittextFields_commonMethod( "Premise Name",  APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, editNewPremiseName);
					//Premise Code	
					edittextFields_commonMethod( "Premise Code",  APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode);

				}	

				//Existing City	
				else if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

					addDropdownValues_commonMethod( "City",  APT_VoiceLineObj.APT_VoiceLine.citydropdowninput, editExistingCityValue);

					//Existing Site
					if(editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
						addDropdownValues_commonMethod( "Site",  APT_VoiceLineObj.APT_VoiceLine.sitedropdowninput, editExistingSiteValue);

						//Existing Premise
						if(editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
							addDropdownValues_commonMethod( "Premise",  APT_VoiceLineObj.APT_VoiceLine.premisedropdowninput, editExistingPremiseValue);
						}

						//New Premise  
						else if(editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {

							verifyExists( APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"Add premise Toggle");
							click( APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"Add premise Toggle");
							//Clickon_addToggleButton(  APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch);
							//Premise name	
							edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName);
							//Premise Code	
							edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode);
						} 
					}

					else if(editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {

						verifyExists( APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"Add site Toggle");
						click( APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"Add site Toggle");
						
						//Clickon_addToggleButton( "addsiteswitch");
						//Site name
						edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addSiteToggleSelected, editNewSiteName);
						//Site Code
						edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addSiteToggleSelected, editNewSiteCode);
						//Premise name	
						edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addSiteToggleSelected, editNewPremiseName);
						//Premise Code	
						edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addSiteToggleSelected, editNewPremiseCode);
					}
				}

				else if(editCountry.equalsIgnoreCase("Null")) {

					ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made for 'Country' dropdown");

					//City	
					editCity( editExistingCity, editNewCity, "citydropdowninput", "selectcityswitch", "addcityswitch",
							editExistingCityValue, editNewCityName, editNewCityCode, "City");


					//Site	
					editSite( editExistingSite, editNewSite, "sitedropdowninput", "selectsiteswitch",
							"addsiteswitch", editExistingSiteValue , editNewSiteName, editNewSiteCode, "Site");

					//Premise
					editPremise( editExistingPremise, editNewPremise, "premisedropdowninput", "selectpremiseswitch",
							"addpremiseswitch", editExistingPremiseValue, editNewPremiseName, editNewPremiseCode, "Premise");

				}
				scrollDown( APT_VoiceLineObj.APT_VoiceLine.okbutton);
				verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
				click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
				//click_commonMethod( "OK", "okbutton");
				waitforPagetobeenable();
				waitforPagetobeenable();
				verifysuccessmessage( "ASR Device updated successfully");

				ASRDeviceNameValue= ASRDeviceName;
				ASRManagementAddressValue= ASRManagementAddress;
				ASRDeviceCountryValue= ASRDevice_Country;
				break;
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Device not added for "+IMSPopLocation_Code+ " location");
			}

		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public void compareText_fromtextFields( String labelname, String xpath, String ExpectedText) throws InterruptedException{

	WebElement element = null;

	try {
		element= findWebElement(xpath);
		String emptyele = findWebElement(xpath).getAttribute("value");
		if(element==null)
		{
			Report.LogInfo("INFO","Step:  '"+labelname+"' not found","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step:  '"+labelname+"' not found");
		}
		else if (emptyele!=null && emptyele.isEmpty()) {
			Report.LogInfo("INFO","Step : '"+ labelname +"' value is empty","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '"+ labelname +"' value is empty");
		}else 
		{   
			if(emptyele.equals(ExpectedText)) {
				Report.LogInfo("INFO","Step: The Expected Text for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal Text '"+emptyele+"'","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,"Step: The Expected Text for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal Text '"+emptyele+"'");
			}
			else if(emptyele.contains(ExpectedText)) {
				Report.LogInfo("INFO","Step: The Expected Text for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal Text '"+emptyele+"'","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,"Step: The Expected Text for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal Text '"+emptyele+"'");
			}
			else
			{
				Report.LogInfo("INFO","Step: The ExpectedText '"+ExpectedText+"' is not same as the Acutal Text '"+emptyele+"'","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,"Step: The ExpectedText '"+ExpectedText+"' is not same as the Acutal Text '"+emptyele+"'");
			}
		}
	}catch (Exception e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
		Report.LogInfo("INFO",labelname + " field is not displaying","FAIL");
		e.printStackTrace();
	}

}


public void compareText_fromActualvalue(String labelname, String xpath, String ExpectedText) throws InterruptedException {

	String text = null;
	WebElement element = null;

	try {
		waitforPagetobeenable();
		element= findWebElement(xpath);
		String emptyele = findWebElement(xpath).getAttribute("value");
		if(element==null)
		{
			Report.LogInfo("INFO","Step:  '"+labelname+"' not found","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step:  '"+labelname+"' not found");
		}
		else if (emptyele!=null && emptyele.isEmpty()) {
			Report.LogInfo("INFO","Step : '"+ labelname +"' value is empty","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '"+ labelname +"' value is empty");
		}else 
		{

			text = element.getText();
			if((text.contains(" ")) ||  text.contains("-")) {

				String[] actualTextValue=text.split(" ");
				String[] expectedValue =ExpectedText.split(" ");

				if(expectedValue[0].equalsIgnoreCase(actualTextValue[0])) {
					ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO"," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'","PASS");
				}
				else if(expectedValue[0].contains(actualTextValue[0])) {
					ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO","The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'","PASS");

				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO","The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'","FAIL");
				}

			}else {
				if(ExpectedText.equalsIgnoreCase(text)) {
					ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO"," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'","PASS");
				}
				else if(ExpectedText.contains(text)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO","The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'","PASS");

				}
				else if(text.contains(ExpectedText)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO","The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'","PASS");

				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					Report.LogInfo("INFO","The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'","FAIL");
				}
			}
		}
	}catch (Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
		Report.LogInfo("INFO",labelname + " field is not displaying","FAIL");
	}

}
/*
public String imsPopLocationValue(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
	String imspoplocation_dropdownvalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	
	String Location_code=null;
	if(imspoplocation_dropdownvalue.equalsIgnoreCase("Bangalore-1"))
	{
		Location_code= "BLR";
	}
	else if(imspoplocation_dropdownvalue.equalsIgnoreCase("Paris-1"))
	{
		Location_code= "PAR";
	}
	else if(imspoplocation_dropdownvalue.equalsIgnoreCase("Frankfurt-1"))
	{
		Location_code= "FRA";
	}
	return Location_code;
}
*/


public String GetText(String labelname, String xpath) throws InterruptedException {

	String text = null;
	WebElement element = null;

	try {
		waitforPagetobeenable();
		element = findWebElement(xpath); 
		String ele = findWebElement(xpath).getAttribute("value");
		if(element==null)
		{
			Report.LogInfo("INFO", "Step : '"+ labelname +"' is not found", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '"+ labelname +"' is not found");
		}
		else if (ele!=null && ele.isEmpty()) {
			Report.LogInfo("INFO", "Step : '"+ labelname +"' value is empty", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '"+ labelname +"' value is empty");
		}
		else {   

			text = element.getText();
			Report.LogInfo("INFO", "Step: '"+ labelname +"' value is displayed as : '"+text+"'", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Step: '"+ labelname +"' value is displayed as : '"+text+"'");

		}
	}catch (Exception e) {
		Report.LogInfo("INFO", "Step: '"+ labelname +"' value is not displaying", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Step: '"+ labelname +"' value is not displaying");
		e.printStackTrace();
	}
	return text;

}

public void verifyViewASRDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	
	String imspoplocation_dropdownvalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	

	scrollDown( APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	if(findWebElement( APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		List<WebElement> addeddevicesList= findWebElements( APT_VoiceLineObj.APT_VoiceLine.addeddevicename);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();

		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			//System.out.println(AddedDeviceNameText);
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			//String IMSPopLocation_Code= imsPopLocationValue(testDataFile, sheetName, scriptNo, dataSetNo);
			String IMSPopLocation_Code= imsPopLocationValue( imspoplocation_dropdownvalue);
			if(AddedDeviceNameText.contains(IMSPopLocation_Code))
			{
				//WebElement AddedDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink).replace("value", AddedDevice_SNo);
				//AddedDevice_ViewLink.click();
				waitforPagetobeenable();
				//click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink2);
				click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink," Clicked on vew ASR Device Link");
				
				waitforPagetobeenable();
				//compareText( "View Device header", "viewasrdevice_header", "Device");
				GetText( "Name", APT_VoiceLineObj.APT_VoiceLine.asrdevicename_viewpage);
				GetText( "Vendor/Model", APT_VoiceLineObj.APT_VoiceLine.asrvendormodel_viewpage);
				GetText( "Management Address", APT_VoiceLineObj.APT_VoiceLine.asrmanagementaddess_viewpage);
				GetText( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.asrsnmpro_viewpage);
				GetText( "Country", APT_VoiceLineObj.APT_VoiceLine.asrcountry_viewpage);
				GetText( "City", APT_VoiceLineObj.APT_VoiceLine.asrcity_viewpage);
				GetText( "Site", APT_VoiceLineObj.APT_VoiceLine.asrsite_viewpage);
				GetText( "Premise", APT_VoiceLineObj.APT_VoiceLine.asrpremise_viewpage);

				//String AddedDevice_DeletefromServiceLink= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_Deletefromservicelink").replace("value", AddedDevice_SNo)).getText();
				break;
			}
		}
	}
	else
	{
		Report.LogInfo("INFO",  "No Device added in grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public void verifyViewDevicepage_Links(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	 
	String sid =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
		
	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	
	 ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewdevice_Edit);
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.viewdevice_Actiondropdown, "Action");
	//click( APT_VoiceLineObj.APT_VoiceLine.viewdevice_Actiondropdown, "Action");
	
	compareText( "Edit",  APT_VoiceLineObj.APT_VoiceLine.viewdevice_Edit, "Edit");
	compareText( "Delete",  APT_VoiceLineObj.APT_VoiceLine.viewdevice_delete, "Delete");
	compareText( "Fetch Device Interfaces",  APT_VoiceLineObj.APT_VoiceLine.viewdevice_fetchinterfacelink, "Fetch Device Interfaces");

	scrollIntoTop();
	//Edit in view device page
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.viewdevice_Edit, "Edit");
	click( APT_VoiceLineObj.APT_VoiceLine.viewdevice_Edit, "Edit");
	//click_commonMethod( "Edit", "viewdevice_Edit");
	waitforPagetobeenable();
	scrollUp();
	if(isVisible(APT_VoiceLineObj.APT_VoiceLine.editasrdevice_header))
	{
		Report.LogInfo("INFO", "Navigated to 'Edit ASR Device' page successfully", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Navigated to 'Edit ASR Device' page successfully");
		waitforPagetobeenable();
		scrollUp();
	
		//clickOnBreadCrump(sid);
		//clickOnBreadCrump(sid);
		webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[text()='"+sid+"']")).click();
		
		waitforPagetobeenable();
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
		if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
		{
			List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevicename);
			//System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();

			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				//System.out.println(AddedDeviceNameText);
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				String IMSPopLocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
				if(AddedDeviceNameText.contains(IMSPopLocation_Code))
				{
					//WebElement AddedDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink).replace("value", AddedDevice_SNo);
					//AddedDevice_ViewLink.click();;
					//click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink2);
					//waitforPagetobeenable();
					ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink);
					click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink,"added device_viewlink ViewDevicepage Links");
					//compareText( "View device header", "viewasrdevice_header", "Device");
					waitforPagetobeenable();
					break;
				}
			}
		}
		else
		{
			Report.LogInfo("INFO",  "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}
	}
	else
	{
		Report.LogInfo("INFO",  "Not navigated to 'Edit ASR Device' page", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Not navigated to 'Edit ASR Device' page");
	}

	//verify delete device in view device page
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.viewdevice_Actiondropdown, "Action");
	//click( APT_VoiceLineObj.APT_VoiceLine.viewdevice_Actiondropdown, "Action");
	scrollIntoTop();
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.viewdevice_delete, "Delete");
	click( APT_VoiceLineObj.APT_VoiceLine.viewdevice_delete, "Delete");
	waitforPagetobeenable();
	
	//WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup, "Delete Alert popup");
	
	if(isVisible(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Delete alert is displayed as expected");
		Report.LogInfo("INFO","Delete alert is displayed as expected","PASS");
		//findWebElement(APT_VoiceLineObj.APT_VoiceLine.deletealertclose).click();
		click(APT_VoiceLineObj.APT_VoiceLine.deletealertclose,"Delete Alert close");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert is not displayed");
	}
	scrollUp();
	//clickOnBreadCrump(sid);
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[text()='"+sid+"']")).click();
	Report.LogInfo("INFO","BreadCrub clicked","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "BreadCrub clicked");
	waitforPagetobeenable();
}

public void verify_Cisco_RouterTools(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	String commandIPv4=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv4");
	String commandIPv6=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv6");
	String vrfname_ipv4=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrf_Ipv4");
	String vrfname_ipv6=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrf_Ipv6");
	
	waitforPagetobeenable();
	
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header,"ASR Device Header");
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid);
	waitforPagetobeenable();
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevicename);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();

		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			//System.out.println(AddedDeviceNameText);
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			String IMSPopLocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
			if(AddedDeviceNameText.contains(IMSPopLocation_Code))
			{
				//WebElement AddedDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink).replace("value", AddedDevice_SNo);
				//AddedDevice_ViewLink.click();
				//click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink1+AddedDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink2);
				click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink,"Added Device View Link");
				waitforPagetobeenable();
				scrollUp();
				
				ASRDevice_ManagementAddress=findWebElement(APT_VoiceLineObj.APT_VoiceLine.asrmanagementaddess_viewpage).getText();
				ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.routertools_header);
				waitforPagetobeenable();

				//Command IPV4	
				addDropdownValues_commonMethod( "Command IPV4", APT_VoiceLineObj.APT_VoiceLine.commandIPV4_dropdown, commandIPv4);

				hostnametextField_IPV4(commandIPv4, ASRDevice_ManagementAddress);
				vrfNametextField_IPV4(commandIPv4, vrfname_ipv4);

				executeCommandAndFetchTheValue( APT_VoiceLineObj.APT_VoiceLine.executebutton_Ipv4);

				//Commmand IPV6
				addDropdownValues_commonMethod( "Command IPV6", APT_VoiceLineObj.APT_VoiceLine.commandIPV6_dropdown , commandIPv6);

				hostnametextField_IPV6(commandIPv6, ASRDevice_ManagementAddress);

				vrfNametextField_IPV6(commandIPv6, vrfname_ipv6);

				executeCommandAndFetchTheValue(APT_VoiceLineObj.APT_VoiceLine.executebutton_IPv6);
	
				
				break;
			}
		}
	}
	else
	{
		Report.LogInfo("INFO", "No Device added in grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public void hostnametextField_IPV4(String command_ipv4, String ipAddress) throws IOException, InterruptedException {
	
	//String command_ipv4=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv4");
	//String ipAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipAddresstype");
	
	boolean IPV4availability=false;
	try {  
		IPV4availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.commandIPv4_hostnameTextfield).isDisplayed();

		if(IPV4availability) {

			verifyExists(APT_VoiceLineObj.APT_VoiceLine.commandIPv4_hostnameTextfield,"IP Address or Hostname");
			sendKeys(APT_VoiceLineObj.APT_VoiceLine.commandIPv4_hostnameTextfield,ipAddress,"IP Address or Hostname");
			//addtextFields_commonMethod("IP Address or Hostname", APT_VoiceLineObj.APT_VoiceLine.commandIPv4_hostnameTextfield, ipAddress);

		}else {
			ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4);
			
			Report.LogInfo("INFO","'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4,"INFO");
		}
	}catch(Exception e) {
		e.printStackTrace();

		ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4);
		//System.out.println("'Hostname or IpAddress' for 'Ipv4' text field is not displaying for "+ command_ipv4);
		Report.LogInfo("INFO","'Hostname or IpAddress' for 'Ipv4' text field is not displaying for "+ command_ipv4,"INFO");
	}
}

public void vrfNametextField_IPV4(String command_ipv4, String vrfname_ipv4) throws IOException, InterruptedException {
	
	//String command_ipv4=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv4");
	//String vrfname_ipv4=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrfname_ipv4");
	
	boolean IPV4availability=false;

	if(command_ipv4.contains("vrf")) {
		try {
			IPV4availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.commandIPv4_vrfnameTextField).isDisplayed();

			if(IPV4availability) {
				
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.commandIPv4_vrfnameTextField, "Router Vrf Name");
				sendKeys(APT_VoiceLineObj.APT_VoiceLine.commandIPv4_vrfnameTextField, vrfname_ipv4,"Router Vrf Name");
			
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
				Report.LogInfo("INFO","'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"FAIL");
			}
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
			Report.LogInfo("INFO","'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"FAIL");
		}

	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'VRF Name IPv4' text field is not displaying for "+ command_ipv4);
		Report.LogInfo("INFO","'VRF Name IPv4' text field is not displaying for "+ command_ipv4 +" command","PASS");
	}

}

public void vrfNametextField_IPV6(String commandIPV6, String vrfname_IPV6) throws IOException, InterruptedException {
	
	//String commandIPV6=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"commandIPV6");
	//String vrfname_IPV6=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrfname_IPV6");
	
	boolean IPV6availability=false;

	if(commandIPV6.equalsIgnoreCase("vrf")) {
		try {  
			IPV6availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.commandIPv6_vrfnameTextField).isDisplayed();

			if(IPV6availability) {
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.commandIPv6_vrfnameTextField, "Router Vrf Name");
				sendKeys(APT_VoiceLineObj.APT_VoiceLine.commandIPv6_vrfnameTextField, vrfname_IPV6,"Router Vrf Name");
			
			}else {
				ExtentTestManager.getTest().log(LogStatus.INFO, "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
				Report.LogInfo("INFO","'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"FAIL");
			}
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO, "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
			Report.LogInfo("INFO","'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"FAIL");
		}
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'VRF Name IPv6' text field is not displaying for "+ commandIPV6);
		Report.LogInfo("INFO","'VRF Name IPv6' text field is not displaying for "+ commandIPV6 +" command","PASS");
	}

}	

public void executeCommandAndFetchTheValue(String executeButton) throws InterruptedException, IOException {

	verifyExists(executeButton,"Execute");
	click(executeButton,"Execute");

	boolean remarkField=false;
	try {	
		remarkField=findWebElement(APT_VoiceLineObj.APT_VoiceLine.result_textArea).isDisplayed();
		if(remarkField) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Remark' text field is displaying");
			Report.LogInfo("INFO", "'Remark' text field is displaying","PASS");

			String remarkvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.result_textArea).getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, "value under 'Remark' field displaying as "+ remarkvalue);
			Report.LogInfo("INFO","value under 'Remark' field displaying as "+ remarkvalue,"PASS");

		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Remark' text field is not displaying");
			Report.LogInfo("INFO", "'Remark' text field is not displaying","FAIL");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Remark' text field is not displaying");
		Report.LogInfo("INFO","'Remark' text field is not displaying","FAIL");
	}

}

public void hostnametextField_IPV6(String commandIPv6, String ipv6Address) throws IOException, InterruptedException {
	
	//String commandIPv6=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"commandIPV6");
	//String ipv6Address=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipv6Address");
	
	boolean IPV6availability=false;
	try {  
		IPV6availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.commandIPv6_hostnameTextfield).isDisplayed();

		if(IPV6availability) {

			verifyExists(APT_VoiceLineObj.APT_VoiceLine.commandIPv6_hostnameTextfield,"IP Address or Hostname");
			sendKeys(APT_VoiceLineObj.APT_VoiceLine.commandIPv6_hostnameTextfield, ipv6Address,"IP Address or Hostname");

		}else {
			ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
			Report.LogInfo("INFO","'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"INFO");
		}
	}catch(Exception e) {
		e.printStackTrace();

		ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
		Report.LogInfo("INFO","'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"INFO");
	}
}

public void verify_CiscoVendor_AddInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceName");
	String addInterface_Allocate=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AddInterface_Allocate");
	String configuration_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Configuration_Dropdownvalue");
	String interfaceAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceAddress");
	String virtualTemplate=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VirtualTemplate");
	String cpeAddressRange=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEAddressRange");
	String localPreShareKey=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LocalPreShareKey");
	String remotePreShareKey=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RemotePreShareKey");
	String identityEmail=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IdentityEmail");
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");

	scrollDown(APT_VoiceLineObj.APT_VoiceLine.addinterface_link);
	waitforPagetobeenable();
	//compareText( "Interfaces header", "interfaces_header", "Interfaces");
	
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.interfacepanel_actiondropdown, "Action dropdown");
	//click(APT_VoiceLineObj.APT_VoiceLine.interfacepanel_actiondropdown, "Action dropdown");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.addinterface_link, "Add Interface/Link");
	click(APT_VoiceLineObj.APT_VoiceLine.addinterface_link, "Add Interface/Link");
	
	waitforPagetobeenable();
	scrollUp();
	GetText("Add Interface header", APT_VoiceLineObj.APT_VoiceLine.addinterface_header);
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
	waitforPagetobeenable();
	scrollUp();
	//verify warning messages in add interface page
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.interface_warngmsg,"Interface");
	//warningMessage_commonMethod( "interface_warngmsg", "Interface");

	//Add Interface
	if(addInterface_Allocate.equalsIgnoreCase("Yes"))
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.allocate_button, "Allocate");
		click(APT_VoiceLineObj.APT_VoiceLine.allocate_button, "Allocate");
		//click_commonMethod( "Allocate", "allocate_button");
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, "Interface");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, interfacename, "Interface");
		
		String InterfaceNameValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield).getAttribute("value");
		compareText_fromtextFields( "Interface", APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, InterfaceNameValue);
		
		String InterfaceAddressValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield).getAttribute("value");
		compareText_fromtextFields( "Interface Address", APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield, InterfaceAddressValue);
		
		String VirtualTemplateValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield).getAttribute("value");
		compareText_fromtextFields( "Virtual Template", APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield, VirtualTemplateValue);
		
		String CPEAddressRangeValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield).getAttribute("value");
		compareText_fromtextFields( "CPE Address Range", APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield, CPEAddressRangeValue);
		
		String LocalPreshareKeyValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield).getAttribute("value");
		compareText_fromtextFields( "Local pre-share key", APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield, LocalPreshareKeyValue);
		
		String RemotePreshareKeyValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield).getAttribute("value");
		compareText_fromtextFields( "Remote pre-share key", APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield, RemotePreshareKeyValue);
		
		String IdentityEmailValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield).getAttribute("value");
		compareText_fromtextFields( "Identity Email", APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield, IdentityEmailValue);

		scrollDown(APT_VoiceLineObj.APT_VoiceLine.configuration_header);
		//configuration panel in add interface page
		compareText( "Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_header, "Configuration");
		addDropdownValues_commonMethod( "Configuration",  APT_VoiceLineObj.APT_VoiceLine.configuration_dropdown, configuration_dropdownvalue);
		//click_commonMethod( "Generate Configuration", "generateconfiguration_link");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link, "Generate Configuration");
		click(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link, "Generate Configuration");
		waitforPagetobeenable();
		GetText( "Configuration",  APT_VoiceLineObj.APT_VoiceLine.configuration_textarea);
		waitforPagetobeenable();
		//click_commonMethod( "Save Configuration", "saveconfiguration_link");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link, "Save Configuration");
		click(APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link, "Save Configuration");
		waitforPagetobeenable();
	}
	else
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, "Interface");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, interfacename,"Interface");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield, "Interface Address");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield, interfaceAddress, "Interface Address");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield,"Virtual Template");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield, virtualTemplate, "Virtual Template");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield, "CPE Address Range" );
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield, cpeAddressRange, "CPE Address Range" );
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield, "Local pre-share key");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield, localPreShareKey,"Local pre-share key");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield, "Remote pre-share key");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield, remotePreShareKey,"Remote pre-share key");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield, "Identity Email" );
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield, identityEmail, "Identity Email" );

		scrollDown(APT_VoiceLineObj.APT_VoiceLine.configuration_header);
		//configuration panel in add interface page
		compareText( "Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_header, "Configuration");
		addDropdownValues_commonMethod( "Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_dropdown, configuration_dropdownvalue);
		
		//click_commonMethod( "Generate Configuration", "generateconfiguration_link");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link, "Generate Configuration");
		click(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link, "Generate Configuration");
		waitforPagetobeenable();
		GetText( "Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_textarea);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link, "Save Configuration");
		click(APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link, "Save Configuration");
		//click_commonMethod( "Save Configuration", "saveconfiguration_link");
		waitforPagetobeenable();
	}

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");

	waitforPagetobeenable();
	compareText( "Interface Added success message", APT_VoiceLineObj.APT_VoiceLine.successmsg, "Interface added successfully");
	waitforPagetobeenable();

	waitforPagetobeenable();
	//clickOnBreadCrump(sid);
	scrollIntoTop();
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	waitforPagetobeenable();
}

public void verify_CiscoVendor_EditInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceName");
	
	String edit_interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_InterfaceName");
	
	String configuration_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Configuration_Dropdownvalue");
	
	String edit_asrdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editASRDeviceName");
	
	String editInterface_Allocate=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditInterface_Allocate");
	
	String edit_configuration_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Configuration_Dropdownvalue");
	
	String edit_interfaceAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_InterfaceAddress");
	
	String edit_virtualTemplate=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VirtualTemplate");
	
	String edit_CPEAddressRange=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_CPEAddressRange");
	
	String edit_localPreshareKey=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_LocalPreshareKey");
	
	String edit_remotePreshareKey=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_RemotePreshareKey");
	
	String edit_identityEmail=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_IdentityEmail");
	
	Report.LogInfo("INFO", "We are in Edit Interface", "FAIL");
	//edit Interface
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header,"ASRdevice Header");
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.selectinterfaces_link).isDisplayed())	
	{
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.selectinterfaces_link,"Show Interfaces");
		click( APT_VoiceLineObj.APT_VoiceLine.selectinterfaces_link,"Show Interfaces");
	}
	
	webDriver.navigate().refresh();
	
	waitforPagetobeenable();
	WebElement SelectInterface= webDriver.findElement(By.xpath("(//div[contains(text(),'"+interfacename+"')])[1]"));
	
	if(SelectInterface.isDisplayed())
	{
		SelectInterface.click();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface checkbox");
		waitforPagetobeenable();
		
			//webDriver.findElement(By.xpath("(//label[text()='Name']/following::div//button[contains(text(),'Action')])[1]")).click();
			
		webDriver.findElement(By.xpath("(//button[text()='Action'])[2]")).click();
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.edit, "Edit");
		click(APT_VoiceLineObj.APT_VoiceLine.edit, "Edit");
	//	click_commonMethod(application, "Edit", "edit", xml);
		waitforPagetobeenable();
		compareText("Edit Interface", APT_VoiceLineObj.APT_VoiceLine.editinterface_header, "Edit Interface");

		if(editInterface_Allocate.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.allocate_button,"Allocate");
			click(APT_VoiceLineObj.APT_VoiceLine.allocate_button,"Allocate");
			waitforPagetobeenable();
			if(!edit_interfacename.equalsIgnoreCase("null"))
			{
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield,"Interface");
				sendKeys(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, edit_interfacename,"Interface");
			}
			else
			{
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield,"Interface");
				sendKeys(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, interfacename,"Interface");
			
			}
			String InterfaceAddressValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield).getAttribute("value");
			compareText_fromtextFields("Interface Address", APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield, InterfaceAddressValue);
			
			String VirtualTemplateValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield).getAttribute("value");
			compareText_fromtextFields("Virtual Template", APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield, VirtualTemplateValue);
			
			String CPEAddressRangeValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield).getAttribute("value");
			compareText_fromtextFields("CPE Address Range", APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield, CPEAddressRangeValue);
			
			String LocalPreshareKeyValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield).getAttribute("value");
			compareText_fromtextFields("Local pre-share key", APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield, LocalPreshareKeyValue);
			
			String RemotePreshareKeyValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield).getAttribute("value");
			compareText_fromtextFields("Remote pre-share key", APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield, RemotePreshareKeyValue);
			
			String IdentityEmailValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield).getAttribute("value");
			compareText_fromtextFields("Identity Email", APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield, IdentityEmailValue);

			scrollDown( APT_VoiceLineObj.APT_VoiceLine.configuration_header);
			//configuration panel in add interface page
			compareText("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_header, "Configuration");
			if(!edit_configuration_dropdownvalue.equalsIgnoreCase("null"))
			{
				addDropdownValues_commonMethod("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_dropdown, edit_configuration_dropdownvalue);
			}
			else
			{
				addDropdownValues_commonMethod("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_dropdown, configuration_dropdownvalue);
			}
			//click(APT_VoiceLineObj.APT_VoiceLine.configuration_headergenerateconfiguration_link, "Generate Configuration");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link,"Generate Configuration");
			click(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link,"Generate Configuration");
			waitforPagetobeenable();
			GetText("Configuration",  APT_VoiceLineObj.APT_VoiceLine.configuration_textarea);
			waitforPagetobeenable();
			click(APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link,"Save Configuration");
			waitforPagetobeenable();
		}
		else
		{

			if(!edit_interfacename.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("Interface", APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, edit_interfacename);
			}
			else
			{
				String EditInterfaceNameValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield).getAttribute("value");
				compareText_fromtextFields("Interface", APT_VoiceLineObj.APT_VoiceLine.interfacename_textfield, EditInterfaceNameValue);
			}

			if(!edit_interfaceAddress.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("Interface Address", APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield, edit_interfaceAddress);
			}
			else
			{
				String EditInterfaceAddressValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield).getAttribute("value");
				compareText_fromtextFields("Interface Address", APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_textfield, EditInterfaceAddressValue);
			}

			if(!edit_virtualTemplate.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("Virtual Template", APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield, edit_virtualTemplate);
			}
			else
			{
				String EditVirtualTemplateValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield).getAttribute("value");
				compareText_fromtextFields("Virtual Template", APT_VoiceLineObj.APT_VoiceLine.virtualtemplate_textfield, EditVirtualTemplateValue);
			}

			if(!edit_CPEAddressRange.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("CPE Address Range", APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield, edit_CPEAddressRange);
			}
			else
			{
				String EditCPEAddressRangeValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield).getAttribute("value");
				compareText_fromtextFields("CPE Address Range", APT_VoiceLineObj.APT_VoiceLine.cpeaddressrange_textfield, EditCPEAddressRangeValue);
			}

			if(!edit_localPreshareKey.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("Local pre-share key", APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield, edit_localPreshareKey);
			}
			else
			{
				String EditLocalPreshareKeyValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield).getAttribute("value");
				compareText_fromtextFields("Local pre-share key", APT_VoiceLineObj.APT_VoiceLine.localpresharekey_textfield, EditLocalPreshareKeyValue);
			}
			if(!edit_remotePreshareKey.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("Remote pre-share key", APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield, edit_remotePreshareKey);
			}
			else
			{
				String EditRemotePreshareKeyValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield).getAttribute("value");
				compareText_fromtextFields("Remote pre-share key", APT_VoiceLineObj.APT_VoiceLine.remotepresharekey_textfield, EditRemotePreshareKeyValue);
			}

			if(!edit_identityEmail.equalsIgnoreCase("null"))
			{
				edittextFields_commonMethod("Identity Email", APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield, edit_identityEmail);
			}
			else
			{
				String EditIdentityEmailValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield).getAttribute("value");
				compareText_fromtextFields("Identity Email", APT_VoiceLineObj.APT_VoiceLine.identityemail_textfield, EditIdentityEmailValue);
			}

			scrollDown(APT_VoiceLineObj.APT_VoiceLine.configuration_header);

			//configuration panel in add interface page
			compareText("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_header, "Configuration");
			if(!edit_configuration_dropdownvalue.equalsIgnoreCase("null"))
			{
				addDropdownValues_commonMethod("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_dropdown, edit_configuration_dropdownvalue);
			}
			else
			{
				addDropdownValues_commonMethod("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_dropdown, configuration_dropdownvalue);
			}
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link,"Generate Configuration");
			click(APT_VoiceLineObj.APT_VoiceLine.generateconfiguration_link,"Generate Configuration");
			
			waitforPagetobeenable();
			GetText("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_textarea);
			waitforPagetobeenable();
			
			verifyExists( APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link,"Save Configuration");
			click( APT_VoiceLineObj.APT_VoiceLine.saveconfiguration_link,"Save Configuration");
			waitforPagetobeenable();


		}
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.okbutton);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
		click(APT_VoiceLineObj.APT_VoiceLine.okbutton, "OK");
		
		waitforPagetobeenable();
		verifysuccessmessage("Interface updated successfully");
	}
	else
	{
		Report.LogInfo("INFO","Interface is not added","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
	}

}
public void selectInterfacelinkforDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException  {

	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	waitforPagetobeenable();
	ExtentTestManager.getTest().log(LogStatus.INFO, "check 'Select Interface' link");

	String IMSPoplocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
	
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevices_list);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();
		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			if(AddedDeviceNameText.contains(IMSPoplocation_Code))
			{
				//WebElement AddedDevice_SelectInterfacesLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_SelectInterfaceslink).replace("value", AddedDevice_SNo);
				//AddedDevice_SelectInterfacesLink.click();
				//webDriver.findElement(By.xpath("//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'"+AddedDevice_SNo+"')]/parent::div/following-sibling::div//span[text()='Select Interfaces']")).click();
				webDriver.findElement(By.xpath("//div[contains(text(),'ASR Device')]//parent::div//following-sibling::table//tr//td//span//a[text()='Select Interfaces']")).click();
				waitforPagetobeenable();
				waitforPagetobeenable();
			}
			else
			{
				Report.LogInfo("INFO","Invalid device name", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
			}
		}
	}
	else
	{
		Report.LogInfo("INFO", "No Device added in grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public void SelectInterfacetoremovefromservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws IOException, InterruptedException {
	
	String interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceName");
	
	String edit_interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_InterfaceName");
	
	//String vendormodel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.viewpage_vendormodel);
	waitforPagetobeenable();
	Cisco_selectRowforInterfaceInService(interfacename, edit_interfacename);
}

public void Cisco_selectRowforInterfaceInService(String interfacename, String edit_interfacename)throws IOException, InterruptedException {

			
	webDriver.navigate().refresh();
	waitForAjax();
	waitforPagetobeenable();
				click(APT_VoiceLineObj.APT_VoiceLine.interfacesinservice_list1+interfacename+APT_VoiceLineObj.APT_VoiceLine.interfacesinservice_list2,"interfacesinservice_list2");
					//findWebElements(APT_VoiceLineObj.APT_VoiceLine.interfacesinservice_list).replace("value", interfacename);
				Report.LogInfo("INFO", interfacename + " is selected under 'Interface in Service' table", "PASS");
				
				ExtentTestManager.getTest().log(LogStatus.PASS, interfacename + " is selected under 'Interface in Service' table");
				
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.InterfaceInselect_Actiondropdown,"InterfaceInselect_Actiondropdown");
				click(APT_VoiceLineObj.APT_VoiceLine.InterfaceInselect_Actiondropdown,"InterfaceInselect_Actiondropdown");

				waitforPagetobeenable();
				
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.InterfaceInselect_removebuton,"InterfaceInselect_removebuton");
				click(APT_VoiceLineObj.APT_VoiceLine.InterfaceInselect_removebuton,"InterfaceInselect_removebuton");
				
				Report.LogInfo("INFO",interfacename + " has been selected to get removed from service","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, interfacename + " has been selected to get removed from service");
				
				
}
public void PageNavigation_NextPageForInterfaceToselect()throws InterruptedException, IOException{

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_nextpage,"Interface To select next page");
	click(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_nextpage,"Interface To select next page");
			waitforPagetobeenable();

}
public void SelectInterfacetoaddwithservcie(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException,IOException 
	{
	
	String interfacenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"InterfaceName");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitforPagetobeenable();

	Cisco_selectrowforInterfaceToselecttable(interfacenumber);
	
	
		}


public void selectrowforInterfaceToselecttable(String interfacenumber) throws IOException, InterruptedException {

	int TotalPages;
 
	String TextKeyword = getTextFrom(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_totalpage,"Interface Toselect totalpage");

	TotalPages = Integer.parseInt(TextKeyword);
	
	Report.LogInfo("INFO","Total number of pages in Interface to select table is: " + TotalPages,"INFO");
	
	if(TotalPages!=0){
		for(int i=0;i<TotalPages;i++)
			{
		List<WebElement> results = webDriver.findElements(By.xpath("//div[contains(text(),'Interfaces in Service')]/following::div[@col-id='name']"));
		
	//	int count = webDriver.findElements(By.xpath("//div[contains(text(),'Interfaces in Service')]/following::div[@col-id='name']")).size();
			
		int count = results.size();
		
		for(int j=0;j<count;j++)
		{
			String text_interface= webDriver.findElements(By.xpath("//div[contains(text(),'Interfaces in Service')]/following::div[@col-id='name']")).get(j).getText();
			
			if(text_interface.equalsIgnoreCase(interfacenumber) || text_interface.contains(interfacenumber))
			{
				webDriver.findElements(By.xpath("//div[contains(text(),'Interfaces in Service')]/following::div[@col-id='name']")).get(j).click();
				
				//click(APT_VoiceLineObj.APT_VoiceLine.SelectaddInterface1+interfacenumber.trim()+APT_VoiceLineObj.APT_VoiceLine.SelectaddInterface2);
				scrollUp();
				click(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_Actiondropdown,"Interface Toselect Actiondropdown");
				
				waitForAjax();
				
				click(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_addbuton,"Interface To select addbuton");
				waitForAjax();
				
				scrollIntoTop();
				
				verifysuccessmessage("Interface added successfully");
				
				i=TotalPages+1;
				break;
				
			}
			
		}
			
			if(i<TotalPages)
			{
				PageNavigation_NextPageForInterfaceToselect();
			}
				
		}
			
	}
	
	 else {

		Report.LogInfo("INFO","No values found inside the table","INFO");
		Report.LogInfo("INFO","No values available inside the Interfacetoselect table","INFO");
	}

}


public void Cisco_selectrowforInterfaceToselecttable(String interfacename)
		throws IOException, InterruptedException {

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.interfacesToSelect_header);
	int TotalPages;

	String TextKeyword = getTextFrom(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_totalpage,"Interface To select_totalpage");

	TotalPages = Integer.parseInt(TextKeyword);

	
	if (TotalPages != 0) {
		ab:
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_currentpage,"Interface Toselect_currentpage");
				int Current_page = Integer.parseInt(CurrentPage);

				List<WebElement> results =  webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='"+interfacename+"']/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
				//List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_Checkbox1+interfacename+APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_Checkbox1);
						//getwebelements(xml.getlocator("//locators/" + application + "/InterfaceToselect_Checkbox").replace("value", interfacename));

				int numofrows = results.size();
			//	System.out.println("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					PageNavigation_NextPageForInterfaceToselect();

				}

				else {

					for (int i = 0; i < numofrows; i++) {

						try {

							resultflag = results.get(i).isDisplayed();
							//System.out.println("status of result: " + resultflag);
							if (resultflag) {
								//System.out.println(results.get(i).getText());
								WebElement interfaceCheckbox= webDriver.findElement(By.xpath("//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[@role='gridcell'][text()='"+interfacename+"']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
								//WebElement interfaceCheckbox=findWebElement(APT_VoiceLineObj.APT_VoiceLine.Interfacetoselect1+interfacename+APT_VoiceLineObj.APT_VoiceLine.Interfacetoselect1);
								//getwebelement(xml.getlocator("//locators/" + application + "/Interfacetoselect").replace("value", interfacename));
								scrolltoview(interfaceCheckbox);
								((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");
								results.get(i).click();
								ExtentTestManager.getTest().log(LogStatus.PASS, interfacename + " is selected under 'Interface to select' table");
								waitforPagetobeenable();

								ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.InterfaceInService_panelHeader);
								waitForAjax();
								click(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_Actiondropdown,"Interface To select Actiondropdown");

								waitForAjax();
								click(APT_VoiceLineObj.APT_VoiceLine.InterfaceToselect_addbuton,"Interface To select ADD button");
								waitForAjax();
								ExtentTestManager.getTest().log(LogStatus.PASS, interfacename + " is added to service");

								break ab;

							}

						} catch (StaleElementReferenceException e) {
							
							// e.printStackTrace();
							
							Report.LogInfo("PASS"," Failure on selecting an Interface to add with service ","FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, " Failure on selecting an Interface to add with service ");
						}
					}
					break ab;
				}
			}

	} else {
		Report.LogInfo("PASS","No values available inside the Interfacetoselect table","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "No values available inside the Interfacetoselect table");
	
	}
}

public void navigateToViewDevicePage(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	
	Report.LogInfo("INFO", "Start navigateToViewDevicePage", "PASS");
	waitForAjax();
	scrollIntoTop();
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevices_list);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();
		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			String IMSPopLocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
			if(AddedDeviceNameText.contains(IMSPopLocation_Code))
			{
				//WebElement AddedDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink).replace("value", AddedDevice_SNo);
				//AddedDevice_ViewLink.click();
				click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_viewlink,"Added Added Device");
				waitforPagetobeenable();
				
				waitForAjax();
				Report.LogInfo("INFO", "End navigateToViewDevicePage", "PASS");

			}
		}
	}
	else
	{
		Report.LogInfo("INFO","No Device added in grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public boolean fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {

	Report.LogInfo("INFO", "Start fetchDeviceInterface_viewdevicepage", "PASS");
	boolean clickLink=false;
	String actualMessage=null;

	scrollUp();
	waitforPagetobeenable();

	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewdevice_Actiondropdown,"View Device Action Dropdown");
	//click(APT_VoiceLineObj.APT_VoiceLine.viewdevice_Actiondropdown,"View Device Action Dropdown");

	waitforPagetobeenable();

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewdevice_fetchinterfacelink,"View Device Fetchinterfacelink");
	click(APT_VoiceLineObj.APT_VoiceLine.viewdevice_fetchinterfacelink,"View Device Fetchinterfacelink");
	waitforPagetobeenable();

	//verify success Message
	String expectedValue="Fetch interfaces started successfully.Please check the sync status of this device here";
	boolean successMessage=false;
	try {	
		successMessage=findWebElement(APT_VoiceLineObj.APT_VoiceLine.fetchDeviceInterace_SuccessMessage).isDisplayed();
		actualMessage=findWebElement(APT_VoiceLineObj.APT_VoiceLine.fetchdeviceInterface_successtextMessage).getText();
		
		if(successMessage) {

			if(actualMessage.contains(expectedValue)) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " After clicking on 'Fetch Device Interface' link, success Message is displaying");
				Report.LogInfo("INFO"," After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected","PASS");

				ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as: "+actualMessage);
				Report.LogInfo("INFO"," Success Message displays as: "+actualMessage,"PASS");

				clickLink=true;

			}
			else if (actualMessage.equalsIgnoreCase(expectedValue)) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected");
				Report.LogInfo("INFO"," After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected","PASS");

				ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as: "+actualMessage);
				Report.LogInfo("INFO"," Success Message displays as: "+actualMessage,"PASS");

				clickLink=true;
				Report.LogInfo("INFO", "End fetchDeviceInterface_viewdevicepage", "PASS");
			}
			else {
				ExtentTestManager.getTest().log(LogStatus.INFO, "Message displays as: 'Message not found for Message ID : Can't start the fetch operation because another process is already fetching the interfaces for this device.'");
				Report.LogInfo("INFO","Message displays as: 'Message not found for Message ID : Can't start the fetch operation because another process is already fetching the interfaces for this device.'","INFO");
				Report.LogInfo("INFO", "End fetchDeviceInterface_viewdevicepage", "PASS");
			}

		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.INFO, "Message displays as: 'Message not found for Message ID : Can't start the fetch operation because another process is already fetching the interfaces for this device.'");
		Report.LogInfo("INFO","Message displays as: 'Message not found for Message ID : Can't start the fetch operation because another process is already fetching the interfaces for this device.'","INFO");

	}
	return clickLink;
}
public static String InterfaceAddress;
public void verifyFetchInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	
	String edit_asrdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editASRDeviceName");
	
	String Inservice_status=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InServiceStatus");
	
	String Inmaintenance_status=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InMaintenanceStatus");
	
	String interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceName");
	
	String edit_interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_InterfaceName");
	
	String interfacenameValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"");
	

	waitforPagetobeenable();
	compareText( "Fetch Interface success msg", APT_VoiceLineObj.APT_VoiceLine.fetchsuccessmsg, "Fetch interfaces started successfully.Please check the sync status of this device here");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.herelink_fetchinterface, "here");
	click(APT_VoiceLineObj.APT_VoiceLine.herelink_fetchinterface, "here");
	waitforPagetobeenable();
	waitforPagetobeenable();

	//Manage Network
	compareText( "Manage Network header", APT_VoiceLineObj.APT_VoiceLine.managenetwork_header, "Manage COLT's Network - Manage Network");
	compareText( "Status header", APT_VoiceLineObj.APT_VoiceLine.status_header, "Status");
	compareText( "Synchronization header", APT_VoiceLineObj.APT_VoiceLine.synchronization_header, "Synchronization");

	//verify column headers
	compareText( "Device column header",APT_VoiceLineObj.APT_VoiceLine.status_devicecolumn, "Device");
	compareText( "Status column header", APT_VoiceLineObj.APT_VoiceLine.status_statuscloumn, "Status");
	compareText( "Last Modification column header", APT_VoiceLineObj.APT_VoiceLine.status_lastmodificationcolumn, "Last Modification");
	compareText( "Action column header", APT_VoiceLineObj.APT_VoiceLine.status_Action, "Action");
	compareText( "Device column header", APT_VoiceLineObj.APT_VoiceLine.synchronization_devicecolumn, "Device");
	compareText( "Sync Status column header", APT_VoiceLineObj.APT_VoiceLine.synchronization_syncstatuscolumn, "Sync Status");
	compareText( "Smarts column header", APT_VoiceLineObj.APT_VoiceLine.synchronization_smartscolumn, "Smarts");
	compareText( "Fetch Interfaces column header", APT_VoiceLineObj.APT_VoiceLine.synchronization_FetchInterfacescolumn, "Fetch Interfaces");
	compareText( "VistaMart Device column header", APT_VoiceLineObj.APT_VoiceLine.synchronization_vistamartdevicecolumn, "VistaMart Device");
	compareText( "Action column header", APT_VoiceLineObj.APT_VoiceLine.synchronization_actioncolumn, "Action");
	waitforPagetobeenable();
	//verify Status panel column values
	if(!edit_asrdevicename.equalsIgnoreCase("null"))
	{
		compareText( "Device", APT_VoiceLineObj.APT_VoiceLine.status_devicevalue, edit_asrdevicename);
	}
	else
	{
		compareText( "Device", APT_VoiceLineObj.APT_VoiceLine.status_devicevalue, ASRDeviceNameValue);
	}

	//String ServiceStatus= GetText( "Status", "status_statusvalue");
	GetText( "Status", APT_VoiceLineObj.APT_VoiceLine.status_statusvalue);
	//verify last modification
	try {
		String GMTValue;
		String LastModificationvalue= GetText( "Last Modification", APT_VoiceLineObj.APT_VoiceLine.status_lastmodificationvalue);
		//System.out.println("Last modified date: " +LastModificationvalue);
		//SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");

		if (LastModificationvalue.length() > 3) 
		{
			GMTValue = LastModificationvalue.substring(LastModificationvalue.length() - 3);
		} 
		else
		{
			GMTValue = LastModificationvalue;
		}
		//sa.assertEquals(GMTValue, "GMT");

	}catch(Exception e)
	{
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Last Modification column value field is not displaying");
	}

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.status_statuslink,"Status");
	click(APT_VoiceLineObj.APT_VoiceLine.status_statuslink,"Status");
	
	waitforPagetobeenable();
	compareText( "Status page header", APT_VoiceLineObj.APT_VoiceLine.Statuspage_header, "Device");

	//verify field headers in status page
	compareText( "Name field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_nameheader, "Name");
	compareText( "Vendor/Model field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_vendormodelheader, "Vendor/Model");
	compareText( "Management Address field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_managementaddressheader, "Management Address");
	compareText( "Snmpro field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_snmproheader, "Snmpro");
	compareText( "Country field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_countryheader, "Country");
	compareText( "City field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_cityheader, "City");
	compareText( "Site field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_siteheader, "Site");
	compareText( "Premise field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_premiseheader, "Premise");
	waitforPagetobeenable();
	//verify field values in status page
	if(!edit_asrdevicename.equalsIgnoreCase("null"))
	{
		compareText( "Name", APT_VoiceLineObj.APT_VoiceLine.statuspage_namevalue, edit_asrdevicename);
	}
	else
	{
		compareText( "Name", APT_VoiceLineObj.APT_VoiceLine.statuspage_namevalue, ASRDeviceNameValue);
	}
	GetText( "Vendor/Model", APT_VoiceLineObj.APT_VoiceLine.statuspage_vendormodelvalue);
	GetText( "Management Address", APT_VoiceLineObj.APT_VoiceLine.statuspage_managementaddressvalue);
	GetText( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.statuspage_snmprovalue);
	GetText( "Country", APT_VoiceLineObj.APT_VoiceLine.statuspage_countryvalue);
	GetText( "City", APT_VoiceLineObj.APT_VoiceLine.statuspage_cityvalue);
	GetText( "Site", APT_VoiceLineObj.APT_VoiceLine.statuspage_sitevalue);
	GetText( "Premise", APT_VoiceLineObj.APT_VoiceLine.statuspage_premisevalue);

	compareText( "Status header", APT_VoiceLineObj.APT_VoiceLine.Statuspage_statusheader, "Status");
	compareText( "Current Status field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_currentstatusfieldheader, "Current Status");
	compareText( "New Status field header", APT_VoiceLineObj.APT_VoiceLine.statuspage_newstatusfieldheader, "New Status");
	GetText( "Status", APT_VoiceLineObj.APT_VoiceLine.status_statusvalue);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.statuspage_newstatusdropdown, "New Status Dropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.statuspage_newstatusdropdown, "New Status Dropdown");
	
	WebElement selectNewStatusvalue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.statuspage_newstatusdropdownvalue);
	selectNewStatusvalue.click();
	String NewStatusvalue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.statuspage_newstatusdropdownvalue).getText();
	
	Report.LogInfo("INFO", "New Status Value is: "+NewStatusvalue, "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "New Status Value is: "+NewStatusvalue);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.statuspage_okbutton, "OK");
	click(APT_VoiceLineObj.APT_VoiceLine.statuspage_okbutton, "OK");
	waitforPagetobeenable();
	scrollUp();
	compareText( "Device status update success message", APT_VoiceLineObj.APT_VoiceLine.Sync_successmsg, "Device Status changed successfully");
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.statuspage_statuscolumnheader);
	//verify 'new status' table column headers
	compareText( "Status column header", APT_VoiceLineObj.APT_VoiceLine.statuspage_statuscolumnheader, "Status");
	compareText( "Changed On column header", APT_VoiceLineObj.APT_VoiceLine.statuspage_changedon_columnheader, "Changed On");
	compareText( "Changed By column header", APT_VoiceLineObj.APT_VoiceLine.statuspage_changedby_columnheader, "Changed By");

	//verify 'new status' table column values
	//Device status history table
	int TotalPages;
	String TotalPagesText = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
	TotalPages = Integer.parseInt(TotalPagesText);
	//System.out.println("Total number of pages in table is: " + TotalPages);

	if (TotalPages != 0) {

		outerloop:
			for (int k = 1; k <= TotalPages; k++) {

				String AddedInterface= webDriver.findElement(By.xpath("//div[@role='grid']//div[@ref='eBodyViewport']/div")).getAttribute("style");
				if(!AddedInterface.contains("height: 1px"))
				{
					List<WebElement> results = webDriver.findElements(By.xpath("//div[@class='modal-content']//div[@class='ag-div-margin row']//div[@ref='eBodyContainer']//div[@role='row']"));
					int numofrows = results.size();
					//System.out.println("no of results: " + numofrows);

					if ((numofrows == 0)) {
						Report.LogInfo("INFO", "Device Status History is empty", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Device Status History is empty");
					}
					else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						//System.out.println("The current page is: " + Current_page);

						//sa.assertEquals(k, Current_page);

						//System.out.println("Currently we are in page number: " + Current_page);
						for (int j = 0; j < numofrows; j++) {
							try {

								String Devicehistorydata = results.get(j).getText();
								//System.out.println(Devicehistorydata);
								if (Devicehistorydata.contains(NewStatusvalue)) 
								{
									Report.LogInfo("INFO","Device status history table has data","PASS");
									ExtentTestManager.getTest().log(LogStatus.PASS, "Device status history table has data");
									waitforPagetobeenable();
									compareText( "New Status", APT_VoiceLineObj.APT_VoiceLine.statuspage_newstatusvalue, NewStatusvalue);
									try {
										String GMTValue;
										String ChangedOnvalue= GetText( "Changed On", APT_VoiceLineObj.APT_VoiceLine.statuspage_changedonvalue);
										System.out.println("Changed On value: " +ChangedOnvalue);
										//SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");

										if (ChangedOnvalue.length() > 3) 
										{
											GMTValue = ChangedOnvalue.substring(ChangedOnvalue.length() - 3);
										} 
										else
										{
											GMTValue = ChangedOnvalue;
										}
										//assertEquals(GMTValue, "GMT");

									}catch(Exception e)
									{
										e.printStackTrace();
										Report.LogInfo("INFO","Step : Changed on column value field is not displaying","FAIL");
										ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Changed on column value field is not displaying");
									}

									GetText( "Changed By", APT_VoiceLineObj.APT_VoiceLine.statuspage_changedbyvalue);
									waitforPagetobeenable();
									
									verifyExists(APT_VoiceLineObj.APT_VoiceLine.statuspage_closebutton, "Close");
									click(APT_VoiceLineObj.APT_VoiceLine.statuspage_closebutton, "Close");
									break outerloop;
								}

							} catch (StaleElementReferenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}
						}
					}
				}
				else
				{
					Report.LogInfo("INFO","No interfaces added","INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "No interfaces added");

				}
			}

	}else {

		//System.out.println("No data available in table");
		Report.LogInfo("INFO","No data available in table","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No data available in table");
	}

	//verify view interfaces page
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.status_viewinterfaceslink,"View Interfaces");
	click(APT_VoiceLineObj.APT_VoiceLine.status_viewinterfaceslink,"View Interfaces");
	
	waitforPagetobeenable();
	compareText( "Interface page header", APT_VoiceLineObj.APT_VoiceLine.viewinterfacepage_header, "Interfaces");
	compareText( "Interface page subheader", APT_VoiceLineObj.APT_VoiceLine.viewinterfacepage_interfacesubheader, "Interfaces");
	String AddedInterface= webDriver.findElement(By.xpath("//div[@role='grid']//div[@ref='eBodyViewport']/div")).getAttribute("style");
	if(!AddedInterface.contains("height: 1px"))
	{
		compareText( "Device Name column header", APT_VoiceLineObj.APT_VoiceLine.viewinterface_devicenamecolumnheader, "Device Name");
		compareText( "Interface Name column header",APT_VoiceLineObj.APT_VoiceLine.interfacename_columnheader, "Interface Name");
		compareText( "Interface Address column header",APT_VoiceLineObj.APT_VoiceLine.interfaceaddress_columnheader, "Interface Address");
		WebElement InterfaceAddressRowValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfacetype_rowvalue);
		InterfaceAddressRowValue.click();
		
		InterfaceAddressRowValue.sendKeys(Keys.TAB);
		compareText( "Interface Type column header", APT_VoiceLineObj.APT_VoiceLine.interfacetype_columnheader, "Interface Type");
		WebElement InterfaceTypeRowValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfacetype_rowvalue);
		InterfaceTypeRowValue.click();
		InterfaceTypeRowValue.sendKeys(Keys.TAB);
		compareText( "Status column header", APT_VoiceLineObj.APT_VoiceLine.viewinterface_status_columnheader, "Status");
		WebElement StatusRowValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.viewinterface_status_rowvalue);
		StatusRowValue.click();
		StatusRowValue.sendKeys(Keys.TAB);
		compareText( "Last Modification column header", APT_VoiceLineObj.APT_VoiceLine.viewinterface_lastmod_columnheader, "Last Modification");

		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewinterface_closebutton,"Close");
		click(APT_VoiceLineObj.APT_VoiceLine.viewinterface_closebutton,"Close");
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.status_viewinterfaceslink,"View Interfaces");
		click(APT_VoiceLineObj.APT_VoiceLine.status_viewinterfaceslink,"View Interfaces");
		waitforPagetobeenable();

		if(edit_interfacename.equalsIgnoreCase("null"))
		{
			interfacenameValue= interfacename;
		}
		else
		{
			interfacenameValue= edit_interfacename;
		}
		int TotalPages1;
		String TotalPagesText1 = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
		TotalPages1 = Integer.parseInt(TotalPagesText1);
		//System.out.println("Total number of pages in table is: " + TotalPages1);

		if (TotalPages1 != 0) {

			outerloop:
				for (int k = 1; k <= TotalPages1; k++) {

					List<WebElement> results = webDriver.findElements(By.xpath("//div[@role='row']//div[@role='gridcell'][@col-id='name']"));

					int numofrows = results.size();
					System.out.println("no of results: " + numofrows);

					if ((numofrows == 0)) {

						Report.LogInfo("INFO", "Interface table is empty", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Interface table is empty");
					}
					else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						//System.out.println("The current page is: " + Current_page);

						//sa.assertEquals(k, Current_page);

						//System.out.println("Currently we are in page number: " + Current_page);
						for (int p = 0; p < numofrows; p++) {
							try {

								String AddedInterfacedata = results.get(p).getText();
								//System.out.println(AddedInterfacedata);
								if (AddedInterfacedata.equalsIgnoreCase(interfacenameValue)) {

									String InterfaceNameRowID= webDriver.findElement(By.xpath("//div[@role='gridcell'][text()='"+interfacenameValue+"']/parent::div[@role='row']")).getAttribute("row-id");
									//System.out.println("Interface row id: "+InterfaceNameRowID);

									//verify interface values in table
									String DeviceNamevalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='deviceName']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Interface Device Name value is displayed as : "+DeviceNamevalue);
									Report.LogInfo("INFO","Step: Interface Device Name value is displayed as : "+DeviceNamevalue,"PASS");
									
									String InterfaceNamevalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='name']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Interface Name value is displayed as : "+InterfaceNamevalue);
									Report.LogInfo("INFO","Step: Interface Name value is displayed as : "+InterfaceNamevalue,"PASS");
									
									String InterfaceAddressvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='address']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Interface Address value is displayed as : "+InterfaceAddressvalue);
									Report.LogInfo("INFO","Step: Interface Address value is displayed as : "+InterfaceAddressvalue,"PASS");
									
									WebElement InterfaceAddressRowValue1= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfacetype_rowvalue);
									InterfaceAddressRowValue1.click();
									InterfaceAddressRowValue1.sendKeys(Keys.TAB);
									String InterfaceTypevalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='type.desc']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Interface Type value is displayed as : "+InterfaceTypevalue);
									Report.LogInfo("INFO","Step: Interface Type value is displayed as : "+InterfaceTypevalue,"PASS");
									
									WebElement InterfaceTypeRowValue1= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interfacetype_rowvalue);
									InterfaceTypeRowValue1.click();
									InterfaceTypeRowValue1.sendKeys(Keys.TAB);
									String Statusvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='currentStatus.desc']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Status value is displayed as : "+Statusvalue);
									Report.LogInfo("INFO", "Step: Status value is displayed as : "+Statusvalue,"PASS");
									
									WebElement StatusRowValue1= findWebElement(APT_VoiceLineObj.APT_VoiceLine.viewinterface_status_rowvalue);
									StatusRowValue1.click();
									StatusRowValue1.sendKeys(Keys.TAB);
									String LastModificationvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='m_time']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Last Modification value is displayed as : "+LastModificationvalue);
									Report.LogInfo("INFO","Step: Last Modification value is displayed as : "+LastModificationvalue,"PASS");
									
									WebElement LastModificationRowValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.viewinterface_lastmod_rowvalue);
									LastModificationRowValue.click();
									LastModificationRowValue.sendKeys(Keys.TAB);
									WebElement StatusLink= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='Status']/div/a"));
									StatusLink.click();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Clicked on Status link in interface table");
									Report.LogInfo("INFO","Clicked on Status link in interface table","PASS");
									InterfaceAddress= InterfaceAddressvalue;
									break outerloop;
								}

							} catch (StaleElementReferenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}
						}
						webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//button[text()='Next']")).click();
						waitforPagetobeenable();
					}
				}

		//verify status page headers & field names
		compareText( "Interface header", APT_VoiceLineObj.APT_VoiceLine.statuspage_interfaceheader, "Interface");
		compareText( "Name field header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_namefield, "Name");
		compareText( "Interface Address field header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_interfaceaddressfield, "Interface Address");
		compareText( "Status header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_statusheader, "Status");
		compareText( "Current Status field header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_currentstatusfield, "Current Status");
		compareText( "New Status field header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_newstatusfield, "New Status");

		//verify status page field values
		compareText( "Name", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_namevalue, interfacenameValue);
		compareText( "Interface Address",APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_interfaceaddressvalue, InterfaceAddress);
		GetText( "Status",APT_VoiceLineObj.APT_VoiceLine.status_statusvalue);
		
		verifyExists("New Status Dropdown", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_newstatusdropdown);
		click("New Status Dropdown", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_newstatusdropdown);
		
		WebElement selectNewStatusvalue1= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_newstatusdropdownvalue);
		selectNewStatusvalue1.click();
		String NewStatusvalue1= findWebElement(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_newstatusdropdownvalue).getText();
		Report.LogInfo("INFO","New Status Value is: "+NewStatusvalue1,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "New Status Value is: "+NewStatusvalue1);
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_okbutton,"OK");
		click(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_okbutton,"OK");
		
		waitforPagetobeenable();
		scrollUp();
		compareText( "Interface status update success message", APT_VoiceLineObj.APT_VoiceLine.Sync_successmsg, "Interface Status changed successfully");
		waitforPagetobeenable();
		scrollDown(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_statuscolumnheader);
		waitforPagetobeenable();
		//verify 'new status' table column headers
		compareText( "Status column header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_statuscolumnheader, "Status");
		compareText( "Changed On column header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_changedon_columnheader, "Changed On");
		compareText( "Changed By column header", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_changedby_columnheader, "Changed By");

		//verify 'new status' table column values
		//verify interface status history table
		int TotalPages2=0;
		String TotalPagesText2 = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbTotal'])[1]")).getText();
		TotalPages2 = Integer.parseInt(TotalPagesText2);
		//System.out.println("Total number of pages in table is: " + TotalPages2);

		if (TotalPages2 != 0) {

			outerloop:
				for (int k = 1; k <= TotalPages2; k++) {

					List<WebElement> results = webDriver.findElements(By.xpath("(//div[@ref='eBodyContainer'])[2]//div[@role='row']"));

					int numofrows = results.size();
					//System.out.println("no of results: " + numofrows);

					if ((numofrows == 0)) {
						Report.LogInfo("INFO","Interface Status History is empty","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Interface Status History is empty");
					}
					else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent'])[1]")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						//System.out.println("The current page is: " + Current_page);

						//sa.assertEquals(k, Current_page);

						//System.out.println("Currently we are in page number: " + Current_page);
						for (int q = 0; q < numofrows; q++) {
							try {
								String Interfacehistorydata = results.get(q).getText();
								//System.out.println(Interfacehistorydata);
								if (Interfacehistorydata.contains(NewStatusvalue1)) 
								{
									Report.LogInfo("INFO","Interface status history table has data","PASS");
									ExtentTestManager.getTest().log(LogStatus.PASS, "Interface status history table has data");
									waitforPagetobeenable();
									compareText( "New Status", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_newstatusvalue, NewStatusvalue1);
									try {
										String GMTValue;
										String ChangedOnvalue= GetText( "Changed On", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_changedonvalue);
										//System.out.println("Changed On value: " +ChangedOnvalue);
										//SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");

										if (ChangedOnvalue.length() > 3)
										{
											GMTValue = ChangedOnvalue.substring(ChangedOnvalue.length() - 3);
										} 
										else
										{
											GMTValue = ChangedOnvalue;
										}
										//sa.assertEquals(GMTValue, "GMT");

									}catch(Exception e)
									{
										e.printStackTrace();
										Report.LogInfo("INFO","Step : Changed On column value field is not displaying","FAIL");
										ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Changed On column value field is not displaying");
									}

									GetText( "Changed By", APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_changedbyvalue);
									waitforPagetobeenable();
									verifyExists(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_closebutton, "Close");
									click(APT_VoiceLineObj.APT_VoiceLine.interface_statuspage_closebutton, "Close");
									break outerloop;
								}

							} catch (StaleElementReferenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}
						}
					}
				}

		}else {

			//System.out.println("No data available in status history table");
			Report.LogInfo("INFO","No data available in status history table","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No data available in status history table");
		}
		}else {

			//System.out.println("No data available in Interface table");
			Report.LogInfo("INFO","No data available in Interface table","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No data available in Interface table");
		}
	}else
	{
		Report.LogInfo("INFO","No Interface added in table","INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "No Interface added in table");
	}
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewinterface_closebutton,"Close");
	click(APT_VoiceLineObj.APT_VoiceLine.viewinterface_closebutton,"Close");
	waitforPagetobeenable();

	//verify synchronization panel column values
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.synchronization_devicevalue);
	if(!edit_asrdevicename.equalsIgnoreCase("null"))
	{
		compareText( "Device", APT_VoiceLineObj.APT_VoiceLine.synchronization_devicevalue, edit_asrdevicename);
	}
	else
	{
		compareText( "Device", APT_VoiceLineObj.APT_VoiceLine.synchronization_devicevalue, ASRDeviceNameValue);
	}

	GetText( "Sync Status", APT_VoiceLineObj.APT_VoiceLine.synchronization_syncstatusvalue);

	//verify smarts value
	GetText( "Smarts", APT_VoiceLineObj.APT_VoiceLine.smartsvalue_asrdevicepanel);
	//verify smarts date time 
	try {
		String GMTValue;
		String Smartsvalue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.smarts_datetime_asrdevicepanel).getText();
		String SmartsDateTimevalue= "";
		if (Smartsvalue.length() > 20) 
		{
			SmartsDateTimevalue = Smartsvalue.substring(Smartsvalue.length() - 20);
			//System.out.println("last 20 characters:"+SmartsDateTimevalue);
		} 
		else 
		{
			SmartsDateTimevalue = Smartsvalue;
		}
		Report.LogInfo("INFO","Smarts date value is displayed as: "+SmartsDateTimevalue,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Smarts date value is displayed as: "+SmartsDateTimevalue);
		//SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
		if (SmartsDateTimevalue.length() > 3) 
		{
			GMTValue = SmartsDateTimevalue.substring(SmartsDateTimevalue.length() - 3);
		} 
		else
		{
			GMTValue = SmartsDateTimevalue;
		}
		//assertEquals(GMTValue, "GMT");

	}catch(Exception e)
	{
		e.printStackTrace();
	}

	//verify fetch interfaces value
	GetText( "Fetch Interfaces", APT_VoiceLineObj.APT_VoiceLine.fetchinterfacevalue_asrdevicepanel);
	//verify fetch interfaces date time
	try {
		String GMTValue;
		String FetchInterfacesvalue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.fetchinterface_datetime_asrdevicepanel).getText();
		String FetchInterfaces_DateTimevalue= "";
		if (FetchInterfacesvalue.length() > 20) 
		{
			FetchInterfaces_DateTimevalue = FetchInterfacesvalue.substring(FetchInterfacesvalue.length() - 20);
			//System.out.println("last 20 characters:"+FetchInterfaces_DateTimevalue);
		} 
		else 
		{
			FetchInterfaces_DateTimevalue = FetchInterfacesvalue;
		}
		Report.LogInfo("INFO", "Fetch Interfaces date value is displayed as: "+FetchInterfaces_DateTimevalue, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Fetch Interfaces date value is displayed as: "+FetchInterfaces_DateTimevalue);
		//SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
		if (FetchInterfaces_DateTimevalue.length() > 3) 
		{
			GMTValue = FetchInterfaces_DateTimevalue.substring(FetchInterfaces_DateTimevalue.length() - 3);
		} 
		else
		{
			GMTValue = FetchInterfaces_DateTimevalue;
		}
		//assertEquals(GMTValue, "GMT");

	}catch(Exception e)
	{
		e.printStackTrace();
		Report.LogInfo("INFO", "Step : Fetch Interfaces date value field is not displaying","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Fetch Interfaces date value field is not displaying");
	}
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.synchronization_synchronizelink, "Synchronize");
	click(APT_VoiceLineObj.APT_VoiceLine.synchronization_synchronizelink, "Synchronize");
	waitforPagetobeenable();
	scrollUp();
	waitforPagetobeenable();
	compareText( "Synchronize Success Msg", APT_VoiceLineObj.APT_VoiceLine.Sync_successmsg, "Sync started successfully. Please check the sync status of this device.");
	waitforPagetobeenable();
	scrollUp();
	waitforPagetobeenable();

	//verify device name link in status panel
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.status_devicevalue,"Device");
	click(APT_VoiceLineObj.APT_VoiceLine.status_devicevalue,"Device");
	waitforPagetobeenable();
	//compareText( "View Device header", "viewasrdevice_header", "Device");
	Report.LogInfo("INFO","Step: Navigated to 'View Device' page","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to 'View Device' page");
	webDriver.navigate().back();
	waitforPagetobeenable();

	//verify device name link in synchronization panel
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.synchronization_devicevalue,"Device");
	click(APT_VoiceLineObj.APT_VoiceLine.synchronization_devicevalue,"Device");
	waitforPagetobeenable();
	//compareText( "View Device header", "viewasrdevice_header", "Device");
	Report.LogInfo("INFO","Navigated to 'View Device' page","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to 'View Device' page");
	waitforPagetobeenable();
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	waitforPagetobeenable();

}

public void navigateToViewServicePage(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	
	waitforPagetobeenable();
	scrollUp();
	waitforPagetobeenable();
	//clickOnBreadCrump(sid);
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	waitforPagetobeenable();
}

public void addTrunkSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 

	String trunkGroupOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkGroupOrder");
	
	String trunkgroupOrderNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkGroupOrderNumber");
	
	boolean trunkPanel=false;
	boolean siteOrderpage=false;
	boolean trunkgrupOrderErrMsg= false;

	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	trunkPanel= findWebElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel).isDisplayed();
	if(trunkPanel) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Trunk Group/Site Orders' panel is displaying as expected in 'view Service' page");
		Report.LogInfo("INFO","'Trunk Group/Site Orders' panel is displaying as expected in 'view Service' page","PASS");


		//Perform Add Site order
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.addTrunkSiteOrderlink,"Add TrunkGroup/SiteOrder link");
		click(APT_VoiceLineObj.APT_VoiceLine.addTrunkSiteOrderlink,"Add TrunkGroup/SiteOrder link");

		siteOrderpage= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addtrunkSiteorderPage_panelheader).isDisplayed();
		if(siteOrderpage) {

			ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Trunk Group/Site Order' page is displaying as expected");
			Report.LogInfo("INFO","'Add Trunk Group/Site Order' page is displaying as expected","PASS");

			//verify mandatory Warning Message
			//click_commonMethod( "OK", "trunk_okButton");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
			click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");


			verifyExists( APT_VoiceLineObj.APT_VoiceLine.trunkGrouporder_warningmsg, "Trunk Group Order");

			verifyExists( APT_VoiceLineObj.APT_VoiceLine.trunkGroupOrderName_warningmsg, "Trunk group Order Number");

			//Add trunkgroup Order checkbox
			if(trunkGroupOrder.equalsIgnoreCase("yes")) {
				//addCheckbox_commonMethod( "trunkGroupOrder_checkbox", "Trunk Group Order", trunkGroupOrder, "No");
				addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.trunkGroupOrder_checkbox, "Trunk Group Order", trunkGroupOrder);
				
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " ' Trunk Group order' checkbox is a mandatory field. No values passed");
				Report.LogInfo("INFO"," ' Trunk Group order' checkbox is a mandatory field. No values passed","FAIL");

			}

			//Add Trunk Group order Number text field
			//addtextFields_commonMethod( "Trunk Group Order Number", "trunkGroupOrderName_textField", trunkgroupOrderNumber);
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunkGroupOrderName_textField,"Trunk Group Order Number");
			sendKeys(APT_VoiceLineObj.APT_VoiceLine.trunkGroupOrderName_textField,trunkgroupOrderNumber,"Trunk Group Order Number");

			//Click on OK button
			//click_commonMethod( "OK", "trunk_okButton");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
			click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
			waitforPagetobeenable();

			try {
				trunkgrupOrderErrMsg=findWebElement(APT_VoiceLineObj.APT_VoiceLine.alertMSg_siteorderCreation).isDisplayed();
				String actualMsg=findWebElement(APT_VoiceLineObj.APT_VoiceLine.alertMSg_siteorderCreation).getText();
				if(trunkgrupOrderErrMsg) {
					if(actualMsg.contains("1.trunkgroup number already exists")) {

						ExtentTestManager.getTest().log(LogStatus.FAIL, " Error message we are getting as: "+ actualMsg);
						Report.LogInfo("INFO"," Error message we are getting as: "+ actualMsg,"FAIL");
					}
					else if(actualMsg.equalsIgnoreCase("Trunk Group created successfully")) {

						ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as 'Trunk Group created successfully'");
						Report.LogInfo("INFO"," Success Message displays as 'Trunk Group created successfully'","PASS");
					}

				}
			}catch(Exception e) {
				e.printStackTrace();

			}

		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Trunk Group/Site Order' page is not displaying");
			Report.LogInfo("INFO","'Add Trunk Group/Site Order' page is not displaying","PASS");
		}

	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Trunk Group/Site Orders' panel is not displaying in 'view Service' page");
		Report.LogInfo("INFO","'Trunk Group/Site Orders' panel is not displaying in 'view Service' page","PASS");
	}
}

public void editSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 

	
	String siteOrderName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkGroupOrderNumber");
	
	String trunkGroupOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_TrunkGroupOrder");
	
	String trunkGrouporderNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_TrunkGroupOrderNumber");
	
	
	boolean siteOrderpage=false;
	boolean trunkgrupOrderErrMsg=false;

	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollDown(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	WebElement editlink=webDriver.findElement(By.xpath("//div[div[span[text()='"+ siteOrderName +"']]]//a[contains(text(),'Edit')]"));
	editlink.click();

	siteOrderpage= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addtrunkSiteorderPage_panelheader).isDisplayed();
	if(siteOrderpage) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Edit Trunk Group/Site Order' page is displaying as expected");
		Report.LogInfo("INFO","'Edit Trunk Group/Site Order' page is displaying as expected","PASS");

		//Trunk group Order
		if(trunkGroupOrder.equalsIgnoreCase("no")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Trunk Group Order' is a mandatory field. It cannot be unselected");
			Report.LogInfo("INFO","'Trunk Group Order' is a mandatory field. It cannot be unselected","FAIL");
		}else {
			editcheckbox_commonMethod( trunkGroupOrder, APT_VoiceLineObj.APT_VoiceLine.trunkGroupOrder_checkbox, "Trunk Group Order");
		}

		//Trunk Group Order Number
		edittextFields_commonMethod( "Trunk Group Order Number", APT_VoiceLineObj.APT_VoiceLine.trunkGroupOrderName_textField, siteOrderName);


		//Click on OK button
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
		click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
		waitforPagetobeenable();

		try {
			trunkgrupOrderErrMsg=findWebElement(APT_VoiceLineObj.APT_VoiceLine.alertMSg_siteorderCreation).isDisplayed();
			String actualMsg=findWebElement(APT_VoiceLineObj.APT_VoiceLine.alertMSg_siteorderCreation).getText();
			if(trunkgrupOrderErrMsg) {
				if(actualMsg.contains("1.trunkgroup number already exists")) {

					ExtentTestManager.getTest().log(LogStatus.FAIL, " Error message we are getting as: "+ actualMsg);
					Report.LogInfo("INFO"," Error message we are getting as: "+ actualMsg,"FAIL");
				}
				else if(actualMsg.contains("Trunk Group successfully updated")) {

					ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as 'Trunk Group successfully updated.'");
					Report.LogInfo("INFO"," Success Message displays as 'Trunk Group successfully updated.'","PASS");
				}

			}
		}catch(Exception e) {
			e.printStackTrace();

		}

	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Edit Trunk Group/Site Order' page is not displaying");
		Report.LogInfo("INFO","'Edit Trunk Group/Site Order' page is displaying","PASS");
	}
}

public void verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 

	String siteOrderName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkGroupOrderNumber");
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	scrolltoend();
	waitforPagetobeenable();

	boolean siteOrderUnderTrunkPanel=false;
	
	siteOrderUnderTrunkPanel=webDriver.findElement(By.xpath("//span[text()='"+ siteOrderName +"']")).isDisplayed();

	if(siteOrderUnderTrunkPanel) {

		ExtentTestManager.getTest().log(LogStatus.PASS, siteOrderName + " 'Site Order' is displaying under 'Trunk' panel");
		Report.LogInfo("INFO",siteOrderName + " 'Site Order' is displaying under 'Trunk' panel","PASS");

		//Click on Add trunk link	
		//String addTrunklinkXpath="//div[div[span[text()='"+ siteOrderName +"']]]/following-sibling::div//span[text()='Add Trunk']";
		//click_commonMethod_PassingWebelementDirectly( "Add Trunk", addTrunklinkXpath);
		
		WebElement addTrunklinkXpath=webDriver.findElement(By.xpath("//div[div[span[text()='"+ siteOrderName +"']]]/following-sibling::div//a[text()='Add Trunk']"));
		addTrunklinkXpath.click();
		waitforPagetobeenable();

	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, siteOrderName + " 'Site Order' is not displaying under 'Trunk' panel");
		Report.LogInfo("INFO",siteOrderName + " 'Site Order' is not displaying under 'Trunk' panel","FAIL");
	}
}

public void addTrunk(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 

	String newCustomerCreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomerCreation");
	String newCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
	String existingCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomer");
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BillingCountry");
	String CDRdelivery=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CDRdelivery");
	String gateway=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"gateway");
	String quality=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"quality");
	String SIPURI=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPURI");
	String resellerID_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerID_Value");
	String ipAddresstype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipAddresstype");
	String SIPsignallingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPsignallingPort");
	String thirdpartyinternet=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyInternet");
	String vlanTag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTag");
	String subInterfaceSlot=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"subInterfaceSlot");
	String signallngZone=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallngZone");
	String callAdmissionControl=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callAdmissionControl");
	String callrateLimit=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimitselection");
	String PSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigvalue");
	String GSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"GSXmanualConfigvalue");
	String callLimit=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callLimit");
	String limitNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"limitNumber");
	String callrateLimiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimiteValue");
	String SBCmanualconfigValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SBCmanualconfigValue");
	String cosprofile_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COSProfile_Value");
	String lanrange_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LANRange_Value");
	String numberporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberPorting_Checkbox");
	String CLIPScreeningandCLIRperCall=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CLIPScreeningandCLIRperCall_radio");
	String clirPermanent=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CLIRPermanent_radio");
	String clipNoScreening=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ClipNoScreening_radio");
	String clipMainNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ClipMainNumber_radio");
	String presentationNumbers=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PresentationNumbers_radio");
	String presentationnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PresentationNumber_Value");
	String customerdefaultnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefaultNumber_Value");
	String egressnumberformat_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EgressNumberFormat_DropdownValue");
	String incomingroutingonDDIlevel_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IncomingRoutingOnDDILevel_Checkbox");
	String PSXmanualConfigTG_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigTG_value");
	String PSXmanualConfigDDI_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigDDI_value");
	String DRusingTDM_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DRusingTDM_checkbox"); 
	String codec_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Codec_Value");
	String faxdiversionnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FaxDiversionNumber_Value");
	String partialnumberreplacement_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PartialNumberReplacement_Checkbox");
	String cpemanualconfig_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpemanualconfig_checkbox");
	String vlanTag_FRASBC_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTag_FRASBC_value");
	
	
	String gatewayCode=null;
	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";
	String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
	String addressContextDefaultValue="EXTERNAL_AC_";
	String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
	String signallingZoneDefaultValue="OUT-UNTRUSTED";

	String SubInterfaceName=null;
	String NIFgroup=null;
	String ipInterface=null;
	String addressContext=null;
	String ipInterfaceGroup=null;
	String vlanUpdatedvalue3=null;
	String vlanUpdatedvalue5=null;

	String customerName=null;

	waitforPagetobeenable();
	scrolltoend();
	waitforPagetobeenable();

	//click_commonMethod( "OK", "trunk_okButton");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");

	scrollIntoTop();
	waitforPagetobeenable();

	if(newCustomerCreation.equalsIgnoreCase("yes")){
		customerName= newCustomerName;
	}
	else
	{
		customerName= existingCustomerName;
	}

	//Trunk Group Description
	String expectedValue1=customerName+"_"+sid+"_"+"IPVL";
	waitforPagetobeenable();
	
	scrollIntoTop();
	waitforPagetobeenable();
	compareText_fromtextFields( "Trunk Group Description", APT_VoiceLineObj.APT_VoiceLine.trunkGroupDEscription_textField, expectedValue1);

	//Primary trunk group
	//WebElement PrimaryTrunkGroup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.primaryTrunkGroup_Dropdown);
	//webelementpresencelogger(PrimaryTrunkGroup, "Primary Trunk Group");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.primaryTrunkGroup_Dropdown, "Primary Trunk Group");

	//VOIP Protocol
	GetText( "VOIP Protocol", APT_VoiceLineObj.APT_VoiceLine.voipProtocol_Dropdown);
	String voip_code="S";

	//SIP URI
	//addtextFields_commonMethod( "SIP URI", "SIPURI_textField", SIPURI);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField, "Primary Trunk Group");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField,SIPURI, "Primary Trunk Group");
	//message displaying under "SIP URI" text field	
	methodToFindMessagesUnderTextField( APT_VoiceLineObj.APT_VoiceLine.SIPURI_defaultValue_textvalue, "SIP URI", "Default URI: va.sip.colt.net");

	//SIP Signalling Port
	//addtextFields_commonMethod( "SIP Signaling Port", "SIPsignallingport_textField", SIPsignallingPort);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField, "SIP Signaling Port");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField,SIPsignallingPort, "SIP Signaling Port");
	
	//message displaying under "SIP Signalling Port" text field	
	methodToFindMessagesUnderTextField(APT_VoiceLineObj.APT_VoiceLine.SIPsignallingPOrt_defaultValue_textvalue, "SIP Signalling Port", "Default Port: 5060");

	//Billing Country
	//warningMessage_commonMethod( "country_warningMessage", "Billing Country");   //validate warning message
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.country_warningMessage, "Billing Country");
	
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown, "Billing Country", country);

	String country_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown).getAttribute("value");
	//System.out.println("country dropdown value is: "+country_code);

	//CDR Delivery
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.CDRdelivery_Dropdown, "CDR Delivery", CDRdelivery);
	String CDR_code=CDRdelivery_code( CDRdelivery);

	//ResellerID
	String ResellerIDValue= resellerID( resellerID_value);

	//Customer ID
	String CustomerID= findWebElement(APT_VoiceLineObj.APT_VoiceLine.customerid).getAttribute("value");

	//Gateway
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.gateway_Dropdown, "Gateway", gateway);
	gatewayCode=gateway_code( gateway);
	waitforPagetobeenable();

	//Quality
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown, "Quality", quality);
	String quality_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown).getAttribute("value");

	//Trunk Name
	String trunkName=country_code+gatewayCode+voip_code+"L"+CustomerID+"00"+CDR_code+ResellerIDValue+quality_code+"00";

	int totalLen=trunkName.length();
	//System.out.println("Total lenth of 'Trunk Group' field is "+ totalLen);
	//System.out.println("Trunk name is "+ trunkName);
	if(totalLen==20) {
		compareText_fromtextFields( "Trunk Group Name", APT_VoiceLineObj.APT_VoiceLine.trunkGroupName_TextField, trunkName);
		primarytrunkGroupname=trunkName;
		//System.out.println("Trunk group name is: "+primarytrunkGroupname);
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Trunk Group Name' length is: "+totalLen);
		Report.LogInfo("INFO"," 'Trunk Group Name' length is: "+totalLen,"FAIL");
		compareText_fromtextFields( "Trunk Group Name", APT_VoiceLineObj.APT_VoiceLine.trunkGroupName_TextField, trunkName);
	}

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField);
	//COS Profile
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.cosprofile, "COS Profile", cosprofile_value);

	//LAN Range
	//addtextFields_commonMethod( "LAN Range", "lanrange", lanrange_value);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.lanrange, "LAN Range");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.lanrange,lanrange_value, "LAN Range");
	
	//IP Address Type
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.IPaddresstype_Dropdown, "IP Address Type", ipAddresstype);
	waitforPagetobeenable();

	//Internet Based Customer
	String InternetBasedCustomer_Checked= findWebElement(APT_VoiceLineObj.APT_VoiceLine.internetBasedCustomer_checkbox).getAttribute("checked");
	String InternetBasedCustomer_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.internetBasedCustomer_checkbox).getAttribute("disabled");
	if(InternetBasedCustomer_Checked==null && InternetBasedCustomer_Disabled==null)
	{
		Report.LogInfo("INFO","'Internet Based Customer' is not checked and disabled by default","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Internet Based Customer' is not checked and disabled by default");
	}
	else
	{
		Report.LogInfo("INFO","'Internet Based Customer' is checked and disabled by default","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Internet Based Customer' is checked and disabled by default");
	}

	//Splitting the Gateway functionality into 2
	if(!gateway.contains("SBC")) {

		if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

			addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet", thirdpartyinternet);

			String vlanDefaultvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
			if(vlanDefaultvalue.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when '3rd Party Internet' is selected");
				Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when '3rd Party Internet' is selected","FAIL");
			}else {
				Report.LogInfo("INFO","When '3rd Party Internet' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue,"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "When '3rd Party Internet' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue);
				if(vlanTag.equalsIgnoreCase("null")) {

					//Sub Interface slot
					selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

					if(subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group  
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}

				}else {

					//VLAN Tag
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag);
					String vlanTagUpdatedvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

					//Sub Interface slot
					selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

					if(subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTagUpdatedvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTagUpdatedvalue+NIGgroupdefaultValue_last;
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTagUpdatedvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTagUpdatedvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
				}
			}
		}
		//3rd party internet checkbox not selected 
		else {
			if(vlanTag.equalsIgnoreCase("null")) {
				String vlanDefaultvalue1=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
				selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

				if(subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue1+NIGgroupdefaultValue_last;
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
				else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue1+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}

			}else {

				//VLAN Tag
				clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
				edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag);
				String vlanUpdatedvalue1=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
				selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

				if(subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanUpdatedvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanUpdatedvalue1+NIGgroupdefaultValue_last;
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
				else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanUpdatedvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanUpdatedvalue1+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
			}

		}
	}

	else if(gateway.contains("SBC")) {
		if(gateway.startsWith("FRA")) {
			if(!thirdpartyinternet.equalsIgnoreCase("Yes")) {
				if(vlanTag.equalsIgnoreCase("null")) {
					//String vlanDefaultvalue2=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).getAttribute("value");

					//IP Interface
					boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
					if(ipInterface_Enabled) {
						Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Ip Interface' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					}
					compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values

					//Address Context
					boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
					if(addressContext_Enabled) {
						Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					}

					compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField, addressContextDefaultValue);  //verify default values


					//IP Interface Group
					boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
					if(ipInterfaceGroup_Enabled) {
						Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					}else {
						Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					}

					compareText_fromtextFields( "IP Interface Group",APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values

				}
				else if(!vlanTag.equalsIgnoreCase("null")) {

					//VLAN Tag
					edittextFields_commonMethod( "VLAN Tag",APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					String vlanUpdatedvalue2=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

					//IP Interface
					boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
					if(ipInterface_Enabled) {
						Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					}

					ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue2;
					compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

					//Address Context
					boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
					if(addressContext_Enabled) {
						Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					}

					addressContext=addressContextDefaultValue+vlanUpdatedvalue2;
					compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


					//IP Interface Group
					boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
					if(ipInterfaceGroup_Enabled) {
						Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					}else {
						Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					}

					ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue2;
					compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values

				}

			}
			else if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//3rd party internet
				addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet", thirdpartyinternet);

				String vlanDefaultvalue3=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue3.isEmpty()) {
					Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					vlanUpdatedvalue3=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}

				ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue3;
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				addressContext=addressContextDefaultValue+vlanUpdatedvalue3;
				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue3;
				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			}
		}
		else {
			if(!thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//VLAN tag  
				String vlanDefaultvalue4=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue4.isEmpty()) {
					Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					//String vlanUpdatedvalue4=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).getAttribute("value");

				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled","FAIL");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled","PASS");
				}
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled","FAIL");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled","PASS");
				}

				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField, addressContextDefaultValue);  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values

			}

			if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//3rd party internet
				addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet", thirdpartyinternet);

				String vlanDefaultvalue5=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue5.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					vlanUpdatedvalue5=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}

				ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue5;
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				addressContext=addressContextDefaultValue+vlanUpdatedvalue5;
				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue5;
				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			}
		}
	}

	//Signalling Zone
	if(thirdpartyinternet.equalsIgnoreCase("yes")) {

		compareText_fromtextFields( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, signallingZoneDefaultValue);

		edittextFields_commonMethod( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, signallngZone);

	}else {
		edittextFields_commonMethod( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, signallngZone);
	}

	//Number Porting checkbox
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.numberporting_checkbox, "Number Porting", numberporting_checkbox);


	//CLI Features
	cliFeatures( CLIPScreeningandCLIRperCall, clirPermanent, clipNoScreening, clipMainNumber, presentationNumbers, presentationnumber_value);

	//Customer Default Number
	//addtextFields_commonMethod( "Customer Default Number", APT_VoiceLineObj.APT_VoiceLine.customerdefaultnumber_textfield, customerdefaultnumber_value);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.customerdefaultnumber_textfield, "Customer Default Number");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.customerdefaultnumber_textfield,customerdefaultnumber_value, "Customer Default Number");
	
	//Invalid CLI Treatment
	boolean invalidCLITreatment_Allow= findWebElement(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_allow).isSelected();

	if(invalidCLITreatment_Allow)
	{
		//Multisite Default Number
		String MultisiteDefaultNumber= findWebElement(APT_VoiceLineObj.APT_VoiceLine.multisitedefaultnumber).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'MultiSite Default Number' value is displaying as "+MultisiteDefaultNumber);
		Report.LogInfo("INFO"," 'MultiSite Default Number' value is displaying as "+MultisiteDefaultNumber,"PASS");
	}
	else
	{
		//click_commonMethod( "Block", "invalidclitreatment_block");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_block, "Block");
		click(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_block, "Block");
		
	}

	//Egress Number Format
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.egressnumberformat_dropdown, "Egress Number Format", egressnumberformat_dropdownvalue);

	//Call Admission Control
	addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.callAdmissionControl_checkbox, "Call Admission Control", callAdmissionControl);

	if(callAdmissionControl.equalsIgnoreCase("yes")) {

		//Call Limit
		selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.callLimit_Dropdown, "Call Limit", callLimit);

		if(callLimit.equalsIgnoreCase("Defined")) {
			//addtextFields_commonMethod( "Limit Number", "limitNumber_textField", limitNumber);
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.limitNumber_textField, "Limit Number");
			sendKeys(APT_VoiceLineObj.APT_VoiceLine.limitNumber_textField,limitNumber, "Limit Number");
			
		}
	}

	//Call Rate Limit
	addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.callrateLimit_checkbox, "Call Rate Limit", callrateLimit);
	if(callrateLimit.equalsIgnoreCase("yes")) {

		String callratelimitactualvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.callRateLimitt_textField).getAttribute("value");
		Report.LogInfo("INFO"," 'Call rate Limit' value is displaying as "+callratelimitactualvalue,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);

		if(!callrateLimiteValue.equalsIgnoreCase("null")) {
			int i=Integer.parseInt(callrateLimiteValue);

			if(i>100) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less than 100 for all Trunks");
				Report.LogInfo("INFO","The CallRateLimit should be less than 100 for all Trunks","FAIL");
			}
			else if(i<=100){
				edittextFields_commonMethod( "Call rate Limit", APT_VoiceLineObj.APT_VoiceLine.callRateLimitt_textField, callrateLimiteValue);
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
			Report.LogInfo("INFO","'Call rate Limit' value is not edited","PASS");
		}
	}
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.egressnumberformat_dropdown);
	waitforPagetobeenable();
	//Usage of Incoming routing on DDI Level
	addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.incomingroutingonDDIlevel_checkbox, "Usage of Incoming routing on DDI Level", incomingroutingonDDIlevel_checkbox);
	waitforPagetobeenable();

	//PSX Manual Configuration-Trunk Group 
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.PSXmanualConfigTG_checkbox, "PSX Manual Configuration-Trunk Group", PSXmanualConfigTG_value);
	waitforPagetobeenable();
	//PSX Manual Configuration-DDI-Range
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.PSXmanualConfigDDIRange_checkbox, "PSX Manual Configuration-DDI-Range", PSXmanualConfigDDI_value);
	waitforPagetobeenable();

	if(gateway.contains("SBC")) {
		//Manual Configuration on SBC
		addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.SBCmanualconfig_checkbox, "Manual Configuration On SBC", SBCmanualconfigValue);
	}else {
		//Manual Configuration On GSX
		addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.GSXmanualConfig_checkbox, "Manual Configuration On GSX", GSXmanualConfigvalue);
	}

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.incomingroutingonDDIlevel_checkbox);
	waitforPagetobeenable();
	//CPE Manual Configuration
	addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.cpemanualconfig_checkbox, "CPE Manual Configuration", cpemanualconfig_checkbox);

	//DR using TDM
	addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.DRusingTDM_checkbox, "DR Using TDM", DRusingTDM_checkbox);

	//Codec
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.codec_dropdown, "Codec", codec_value);

	scrolltoend();
	//Fax Diversion Numbers
	//addtextFields_commonMethod( "Fax Diversion Numbers", "faxdiversionnumbers_textfield", faxdiversionnumber_value);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield, "Fax Diversion Numbers");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield,faxdiversionnumber_value, "Fax Diversion Numbers");
	
	
	//click_commonMethod( "Fax Diversion Numbers Add arrow", "faxdiversion_addarrow");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.faxdiversion_addarrow, "Fax Diversion Numbers Add arrow");
	click(APT_VoiceLineObj.APT_VoiceLine.faxdiversion_addarrow, "Fax Diversion Numbers Add arrow");
	//Partial Number Replacement
	addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.partialnumberreplacement_checkbox, "Partial Number Replacement", partialnumberreplacement_checkbox);

	waitforPagetobeenable();
	scrolltoend();
	//click_commonMethod( "OK", "trunk_okButton");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton, "OK");
	click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton, "OK");
	waitforPagetobeenable();
	
	verifysuccessmessage( "Trunk created successfully");

}
public void methodToFindMessagesUnderTextField(String xpath, String labelname, String expectedmsg) {

	boolean defaultPortValueUnderSIPSignalling=false;
	try {	
		defaultPortValueUnderSIPSignalling=findWebElement(xpath).isDisplayed();
		if(defaultPortValueUnderSIPSignalling) {

			WebElement defaultValue=findWebElement(xpath);
			String defaultValueText= defaultValue.getText();
			if(defaultValueText.contains(expectedmsg)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Under '"+ labelname +"' text field', text message displays as "+ defaultValueText);
				Report.LogInfo("INFO","Under '"+ labelname +"' text field', text message displays as "+ defaultValueText,"PASS");
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Under '"+ labelname +"' text field', text message displays as "+ defaultValueText);
				Report.LogInfo("INFO","Under '"+ labelname +"' text field', text message displays as "+ defaultValueText,"FAIL");
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'");
			Report.LogInfo("INFO","No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'","FAIL");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'");
		Report.LogInfo("INFO","No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'","FAIL");
	}
}

public String CDRdelivery_code(String cdrDElivery) {

	String code_cdr=null;

	if(cdrDElivery.equalsIgnoreCase("Delivery to COCOM")) {
		code_cdr="C";
	}

	else if(cdrDElivery.equalsIgnoreCase("No delivery to COCOM")) {
		code_cdr="0";
	}

	return code_cdr;
}


public String gateway_code(String gateway) {

	String code=null;

	if(gateway.equals("DEVGSX1")) {
		code="DEV";
	}

	else if(gateway.equals("DEVSBC1")) {
		code="DEA";
	}

	else if(gateway.equals("DEVSBC2")) {
		code="DEB";		
	}

	else if(gateway.equals("OPSGSX1")) {
		code="OPS";
	}

	else if(gateway.equals("MILGSX1")) {
		code="MIA";
	}

	else if(gateway.equals("ZRHNBS1")) {
		code="ZHA";
	}

	else if(gateway.equals("PARNBS1")) {
		code="PSA";
	}

	else if(gateway.equals("LONNBS1")) {
		code="LNA";
	}

	else if(gateway.equals("FRANBS1")) {
		code="FTA";
	}

	else if(gateway.equals("MADGSX1")) {
		code="MDA";
	}

	else if(gateway.equals("BHXNBS1")) {
		code="BHA";
	}

	else if(gateway.equals("PARGSX2")) {
		code="PSB";
	}

	else if(gateway.equals("FRAGSX2")) {
		code="FTB";
	}

	else if(gateway.equals("PARGSX3")) {
		code="PSC";
	}

	else if(gateway.equals("FRASBC1")) {
		code="FTC";
	}

	else if(gateway.equals("PARSBC1")) {
		code="PSD";
	}

	else if(gateway.equals("ZRHGSX2")) {
		code="ZRB";
	}

	else if(gateway.equals("FRASBC2")) {
		code="FTD";
	}

	else if(gateway.equals("PARSBC2")) {
		code="PSE";
	}

	else if(gateway.equals("FRASBC3")) {
		code="FTF";
	}

	else if(gateway.equals("PARSBC3")) {
		code="PSF";
	}

	return code;
}

public String resellerID(String resellerID_value) throws InterruptedException, IOException {

	String ResellerID=null;
	if(!resellerID_value.equalsIgnoreCase("null"))
	{
		//cleartext("Reseller ID", "resellerid");
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.resellerid);
		
		edittextFields_commonMethod("Reseller ID", APT_VoiceLineObj.APT_VoiceLine.resellerid, resellerID_value);
		ResellerID= resellerID_value;
	}
	else
	{
		ResellerID= findWebElement(APT_VoiceLineObj.APT_VoiceLine.resellerid).getAttribute("value");
	}
	return ResellerID;
}

public void cliFeatures(String CLIPScreeningandCLIRperCall, String clirPermanent, String clipNoScreening, String clipMainNumber, String presentationNumbers, String presentationnumber_value) throws InterruptedException, IOException {

	String CLIPScreeningandCLIR= findWebElement(APT_VoiceLineObj.APT_VoiceLine.clipscreeningandclir_radio).getAttribute("checked");
	if(CLIPScreeningandCLIRperCall.equalsIgnoreCase("yes")) {
		if(CLIPScreeningandCLIR==null)
		{
			//click_commonMethod(application, "CLIP Screening and CLIR per Call (DEFAULT)", "clipscreeningandclir_radio", xml);
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.clipscreeningandclir_radio, "CLIP Screening and CLIR per Call (DEFAULT)");
			click(APT_VoiceLineObj.APT_VoiceLine.clipscreeningandclir_radio, "CLIP Screening and CLIR per Call (DEFAULT)");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "CLIP Screening and CLIR per Call (DEFAULT) radio button is selected by default");
			Report.LogInfo("INFO","CLIP Screening and CLIR per Call (DEFAULT) radio button is selected by default","PASS");
		}
	}
	else if(clirPermanent.equalsIgnoreCase("yes")) {
		//click_commonMethod(application, "CLIR Permanent", "clirPermanent_radio", xml);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.clirPermanent_radio, "CLIR Permanent");
		click(APT_VoiceLineObj.APT_VoiceLine.clirPermanent_radio, "CLIR Permanent");
	}
	else if(clipNoScreening.equalsIgnoreCase("yes")) {
		//click_commonMethod(application, "CLIP NO Screening", "clipNoScreening_radio", xml);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.clipNoScreening_radio, "CLIP NO Screening");
		click(APT_VoiceLineObj.APT_VoiceLine.clipNoScreening_radio, "CLIP NO Screening");
	}
	else if(clipMainNumber.equalsIgnoreCase("yes")) {
		//click_commonMethod(application, "CLIP Main Number", "clipmainnumber_radio", xml);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.clipmainnumber_radio, "CLIP Main Number");
		click(APT_VoiceLineObj.APT_VoiceLine.clipmainnumber_radio, "CLIP Main Number");
	}
	else if(presentationNumbers.equalsIgnoreCase("yes")) {
		//click_commonMethod(application, "Presentation Numbers", "presentationnumbers_radio", xml);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.presentationnumbers_radio, "Presentation Numbers");
		click(APT_VoiceLineObj.APT_VoiceLine.presentationnumbers_radio, "Presentation Numbers");
		
		//addtextFields_commonMethod("Presentation Number", "presentationnumber_textfield", presentationnumber_value);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.presentationnumber_textfield, "Presentation Numbers");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.presentationnumber_textfield, presentationnumber_value,"Presentation Numbers");
		
		//click_commonMethod(application, "Presentation Number Add arrow", "presentationnumber_addarrow", xml);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.presentationnumber_addarrow, "Presentation Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.presentationnumber_addarrow, "Presentation Number Add arrow");
	}
}

public void viewTrunk_Primary(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 

	String newCustomerCreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomerCreation");
	
	String newCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
	String existingCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomer");
	String serviceid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BillingCountry");
	String CDRdelivery=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CDRdelivery");
	String gateway=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"gateway");
	String quality=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"quality");
	String SIPURI=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPURI");
	String resellerID_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerID_value");
	String ipAddresstype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipAddresstype");
	String SIPsignallingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPsignallingPort");
	String thirdpartyinternet=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyInternet");
	String vlanTag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTag");
	String subInterfaceSlot=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"subInterfaceSlot");
	String signallngZone =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallngZone");
	String callAdmissionControl =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callAdmissionControl");
	String callrateLimitselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimitselection");
	String PSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigvalue");
	String GSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"GSXmanualConfigvalue");
	String callLimit =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callLimit");
	String limitNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"limitNumber");
	String callrateLimiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimiteValue");
	String SBCmanualconfigValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SBCmanualconfigValue");
	String cosprofile_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COSProfile_Value");
	String lanrange_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LANRange_Value");
	String numberporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberPorting_Checkbox");
	String CLIPScreeningandCLIRperCall=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CLIPScreeningandCLIRperCall_radio");
	String clirPermanent=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CLIRPermanent_radio");
	String clipNoScreening=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ClipNoScreening_radio");
	String clipMainNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ClipMainNumber_radio");
	String presentationNumbers=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PresentationNumbers_radio");
	String presentationnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PresentationNumber_Value");
	String customerdefaultnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefaultNumber_Value");
	String egressnumberformat_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EgressNumberFormat_DropdownValue");
	String incomingroutingonDDIlevel_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IncomingRoutingOnDDILevel_Checkbox");
	String PSXmanualConfigTG_value =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigTG_value");
	String PSXmanualConfigDDI_value =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigDDI_value");
	String DRusingTDM_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DRusingTDM_checkbox");
	String codec_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Codec_Value");
	String faxdiversionnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FaxDiversionNumber_Value");
	String partialnumberreplacement_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PartialNumberReplacement_Checkbox");
	String cpemanualconfig_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpemanualconfig_checkbox");

	scrollIntoTop();
	waitforPagetobeenable();

	String AddressContext="EXTERNAL_AC_";
	String IPINTERFACEGROUP ="EXTERNAL_IPIG_";
	String IPINTERFACE=	"EXTERNAL_IPIF_";

	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";

	String customerName=null;

	if(newCustomerCreation.equalsIgnoreCase("yes")){
		customerName= newCustomerName;
	}
	else
	{
		customerName= existingCustomerName;
	}

	//Trunk Group Description
	String expectedValue1=customerName+"_"+serviceid+"_"+"IPVL";
	waitforPagetobeenable();
	compareText_InViewPage( "Trunk Group Description", expectedValue1);

	//VOIP Protocol
	compareText_InViewPage( "VOIP Protocol", "SIP");

	//SIP Signaling Port
	compareText_InViewPage( "SIP Signalling Port", SIPsignallingPort);

	//SIP URI
	compareText_InViewPage( "SIP URI", SIPURI);

	//Billing Country
	compareText_InViewPage( "Billing Country", country);

	//CDR Delivery
	compareText_InViewPage( "CDR Delivery", CDRdelivery);

	//Reseller ID
	GetText( "Reseller ID", APT_VoiceLineObj.APT_VoiceLine.viewtrunk_resellerid);

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewtrunk_resellerid);
	//Customer ID
	GetText( "Customer ID", APT_VoiceLineObj.APT_VoiceLine.viewtrunk_customerid);

	//Gateway
	compareText_InViewPage( "Gateway", gateway);

	//Quality
	compareText_InViewPage( "Quality", quality);

	//Trunk Group Name
	GetText( "Trunk Group Name", APT_VoiceLineObj.APT_VoiceLine.viewtrunk_trunkgroupname);

	//COS Profile
	GetText( "COS Profile", APT_VoiceLineObj.APT_VoiceLine.viewpage_cosprofile);

	//LAN Range
	compareText_InViewPage( "LAN Range", lanrange_value);

	//IP Address Type
	compareText_InViewPage( "IP Address Type", ipAddresstype);

	//3rd Party Internet
	GetText( "3rd Party Internet", APT_VoiceLineObj.APT_VoiceLine.viewpage_thirdpartyinternet);

	//Sub Interface Slot
	String subinterfaceSlot_viewPage= GetText( "Sub Interface Slot", APT_VoiceLineObj.APT_VoiceLine.viewpage_subinterfaceslot);

	//VLAN Tag 
	String vlan_actualValue=GetText( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.viewpage_vlantag);
	if(!gateway.contains("SBC")) {

		if(thirdpartyinternet.equalsIgnoreCase("no")) {

			//Sub Interface Name
			String SubInterfaceName=subInterfacename_starting+subinterfaceSlot_viewPage+subInterfacename_middle+vlan_actualValue;
			compareText_InViewPage( "Sub Interface Name", SubInterfaceName);

			//NIF Group
			String NIFgroup=NIFgroupDEfaultValue_starting+subinterfaceSlot_viewPage+NIFgroupDEfaultValue_middle+vlan_actualValue+NIGgroupdefaultValue_last;
			//System.out.println("NIF Group value is: "+NIFgroup);
			compareText_InViewPage( "NIF Group", NIFgroup);

			//Signalling Zone
			String SignallingZone=findWebElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_signallingzone).getText();
			compareText_InViewPage( "Signalling Zone", SignallingZone);

		}
		else if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

			//Sub Interface Name
			String SubInterfaceName=subInterfacename_starting+subinterfaceSlot_viewPage+subInterfacename_middle+vlan_actualValue;
			compareText_InViewPage( "Sub Interface Name", SubInterfaceName);

			//NIF Group
			String NIFgroup=NIFgroupDEfaultValue_starting+subinterfaceSlot_viewPage+NIFgroupDEfaultValue_middle+vlan_actualValue+NIGgroupdefaultValue_last;
			//System.out.println("NIF Group value is: "+NIFgroup);
			compareText_InViewPage( "NIF Group", NIFgroup);

			//Signalling Zone
			String SignallingZone=findWebElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_signallingzone).getText();
			compareText_InViewPage( "Signalling Zone", SignallingZone);

		}
	}
	else if(gateway.contains("SBC")) {
		if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

			//Address Content
			compareText_InViewPage( "Address Context", AddressContext+vlan_actualValue);

			//IP Interface Group
			compareText_InViewPage( "IP Interface Group", IPINTERFACEGROUP+vlan_actualValue);

			//IP Interface
			compareText_InViewPage( "IP Interface", IPINTERFACE+vlan_actualValue);

			//Signalling Zone
			//GetText( "Signalling Zone", APT_VoiceLineObj.APT_VoiceLine.viewpage_signallingzone);

		}
		else if(thirdpartyinternet.equalsIgnoreCase("No")) {

			//VLAN Tag
			compareText_InViewPage( "VLAN Tag", vlanTag);

			//Address Content
			compareText_InViewPage( "Address Context", AddressContext+vlan_actualValue);

			//IP Interface Group
			compareText_InViewPage( "IP Interface Group", IPINTERFACEGROUP+vlan_actualValue);

			//IP Interface
			compareText_InViewPage( "IP Interface", IPINTERFACE+vlan_actualValue);
		}
	}

	//Signalling Port
	GetText( "Signalling Port", APT_VoiceLineObj.APT_VoiceLine.viewpage_signallingport);

	//Number Porting
	compareText_InViewPage( "Number Porting", numberporting_checkbox);

	//CLI Features
	GetText( "CLI Features", APT_VoiceLineObj.APT_VoiceLine.viewpage_CLIfeatures);

	if(!presentationnumber_value.equalsIgnoreCase("null"))
	{
		compareText_InViewPage1( "Presentation Numbers", presentationnumber_value);
	}
	else
	{
		compareText_InViewPage1( "Presentation Numbers", "null");
	}
	//Customer Default Number
	if(!customerdefaultnumber_value.equalsIgnoreCase("null"))
	{
		compareText_InViewPage( "Customer Default Number", customerdefaultnumber_value);
	}
	else
	{
		compareText_InViewPage( "Customer Default Number", "null");
	}

	//Invalid CLI Treatment
	String InvalidCLITreatment_viewpage= GetText( "Invalid CLI Treatment", APT_VoiceLineObj.APT_VoiceLine.viewpage_CLItreatment);
	//MultiSite Default Number
	if(InvalidCLITreatment_viewpage.equalsIgnoreCase("Allow"))
	{
		compareText_InViewPage( "MultiSite Default Number", customerdefaultnumber_value);
	}

	//Egress Number Format
	GetText( "Egress Number Format", APT_VoiceLineObj.APT_VoiceLine.viewpage_egressnumberformat);
	//Call Admission Control
	compareText_InViewPage( "Call Admission Control", callAdmissionControl);
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_signallingzone);
	waitforPagetobeenable();
	if(callAdmissionControl.equalsIgnoreCase("yes")) {
		//call limit
		compareText_InViewPage( "Call Limit", callLimit );

		if(callLimit.equalsIgnoreCase("Defined")) {
			//Limit Number
			compareText_InViewPage( "Limit Number", limitNumber );
		}
	}

	//Call Rate Limit
	if(callrateLimitselection.equalsIgnoreCase("Yes")) {

		//call rate limit value
		compareText_InViewPage("Call Rate Limit", callrateLimiteValue);
	}

	//Usage of Incoming routing on DDI Level
	GetText( "Usage of Incoming routing on DDI Level", APT_VoiceLineObj.APT_VoiceLine.viewpage_usageofincomingrouting);
	//PSX Manual Configuration - Trunk Group
	compareText_InViewPage( "PSX Manual Configuration - Trunk Group", PSXmanualConfigTG_value);

	//PSX Manual Configuration - DDI Range
	compareText_InViewPage( "PSX Manual Configuration - DDI Range", PSXmanualConfigDDI_value);


	//PSX Manual Configuration
	compareText_InViewPage( "PSX Manual Configuration", PSXmanualConfigvalue);

	if(gateway.contains("SBC")) {
		//Manual Configuration on SBC
		compareText_InViewPage( "Manual Configuration On SBC", SBCmanualconfigValue);
	}else {
		//Manual Configuration On GSX
		compareText_InViewPage( "Manual Configuration on GSX", GSXmanualConfigvalue);
	}

	//CPE Manual Configuration
	compareText_InViewPage( "CPE Manual Configuration", cpemanualconfig_checkbox);

	//Codec
	GetText( "Codec", APT_VoiceLineObj.APT_VoiceLine.viewpage_codec);
	//Fax Diversion Numbers
	if(!faxdiversionnumber_value.equalsIgnoreCase("null"))
	{
		compareText_InViewPage( "Fax Diversion Numbers", faxdiversionnumber_value);
	}
	else
	{
		compareText_InViewPage( "Fax Diversion Numbers", "null");
	}

	//DR Using TDM
	compareText_InViewPage( "DR Using TDM", DRusingTDM_checkbox);

}

public void editTrunk(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 

	String newCustomerCreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomerCreation");
	
	String newCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
	
	String existingCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomer");
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	
	String edit_SIPURI=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SIPURI");
	
	String edit_SIPsignallingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SIPsignallingPort");
	
	String edit_ipAddresstype =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_ipAddresstype");
	
	String edit_BillingCountry=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_BillingCountry");
	
	String edit_CDRdelivery=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_CDRdelivery");
	
	String edit_resellerID_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_resellerID_value");
	
	String gateway=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"gateway");
	
	String edit_gateway =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_gateway");
	
	String edit_quality=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_quality");
	
	String edit_cosprofile_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_cosprofile_value");
	
	String edit_lanrange_value =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_lanrange_value");
	
	String edit_CLIPScreeningandCLIRperCall=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_CLIPScreeningandCLIRperCall");
	
	String edit_clirPermanent=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_clirPermanent");
	
	String edit_clipNoScreening=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_clipNoScreening");
	
	String edit_clipMainNumber =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_clipMainNumber");
	
	String edit_presentationNumbers=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_presentationNumbers");
	
	String edit_presentationnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_presentationnumber_value");
	
	String edit_customerdefaultnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_customerdefaultnumber_value");
	
	String edit_egressnumberformat_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_egressnumberformat_dropdownvalue");
	
	String edit_callAdmissionControl=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_callAdmissionControl");
	
	String edit_callLimit =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_callLimit");
	
	String edit_limitNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_limitNumber");
	
	String edit_callrateLimit=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_callrateLimit");
	
	String edit_callrateLimiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_callrateLimiteValue");
	
	String edit_incomingroutingonDDIlevel_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_incomingroutingonDDIlevel_checkbox");
	
	String edit_PSXmanualConfigTG_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_PSXmanualConfigTG_value");
	
	String edit_PSXmanualConfigDDI_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_PSXmanualConfigDDI_value");
	
	String edit_SBCmanualconfigValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SBCmanualconfigValue");
	
	String edit_GSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_GSXmanualConfigvalue");
	
	String edit_cpemanualconfig_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_cpemanualconfig_checkbox");
	
	String edit_DRusingTDM_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_DRusingTDM_checkbox");
	
	String edit_codec_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_codec_value");
	
	String edit_faxdiversionnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_faxdiversionnumber_value");
	
	String edit_partialnumberreplacement_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_partialnumberreplacement_checkbox");
	
	String edit_thirdpartyinternet=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_thirdpartyinternet");
	
	String edit_subInterfaceSlot=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_subInterfaceSlot");
	
	String edit_vlanTag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_vlanTag");
	
	String edit_vlanTag_FRASBC_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_vlanTag_FRASBC_value");
	
	String edit_signallingZone=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_signallingZone");
	
	String edit_numberporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_numberporting_checkbox");
	

	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";
	String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
	String addressContextDefaultValue="EXTERNAL_AC_";
	String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
//	String signallingZoneDefaultValue="OUT-UNTRUSTED";

	String gatewayCode=null;
//	String primarytrunk="0";
	String CDR_code=null;
	String SubInterfaceName=null;
	String NIFgroup=null;
	String ipInterface=null;
	String addressContext=null;
	String ipInterfaceGroup=null;
	String vlanUpdatedvalue3=null;
	String vlanUpdatedvalue5=null;

	String customerName=null;

	waitforPagetobeenable();
	scrollIntoTop();
	//Action button	
	//click_commonMethod( "Action", "viewPage_ActionButton");
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewPage_ActionButton, "Action");
	//click(APT_VoiceLineObj.APT_VoiceLine.viewPage_ActionButton, "Action");

	//click on Edit link
	//click_commonMethod( "Edit", "edit");
	
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.edit, "Edit Trunk");
	//click(APT_VoiceLineObj.APT_VoiceLine.edit, "Edit Trunk");
	
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.edit_trunk, "Edit Trunk");
	click(APT_VoiceLineObj.APT_VoiceLine.edit_trunk, "Edit Trunk");
	waitforPagetobeenable();

	if(newCustomerCreation.equalsIgnoreCase("yes")){
		customerName= newCustomerName;
	}
	else
	{
		customerName= existingCustomerName;
	}

	//Trunk Group Description
	String expectedValue1=customerName+"_"+sid+"_"+"IPVL";
	waitforPagetobeenable();
	compareText_fromtextFields( "Trunk Group Description", APT_VoiceLineObj.APT_VoiceLine.trunkGroupDEscription_textField, expectedValue1);
	
	scrollIntoTop();
	//VOIP Protocol
	GetText( "VOIP Protocol", APT_VoiceLineObj.APT_VoiceLine.voipProtocol_Dropdown);
	String voip_code="S";

	//SIP URI
	edittextFields_commonMethod( "SIP URI", APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField, edit_SIPURI);
	String SIPURIvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField).getAttribute("value");
	
	Report.LogInfo("INFO","SIP URI value is: "+SIPURIvalue,"PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "SIP URI value is: "+SIPURIvalue);

	//SIP Signalling Port
	edittextFields_commonMethod( "SIP Signaling Port", APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField, edit_SIPsignallingPort);
	String SIPsignallingport_value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField).getAttribute("value");
	Report.LogInfo("INFO","SIP Signalling Port value is: "+SIPsignallingport_value,"PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "SIP Signalling Port value is: "+SIPsignallingport_value);

	//Billing COuntry
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown, "Billing Country", edit_BillingCountry);

	String country_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown).getAttribute("value");
	Report.LogInfo("INFO","country dropdown value is: "+country_code,"PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "country dropdown value is: "+country_code);

	//CDR Delivery
	if(!edit_CDRdelivery.equalsIgnoreCase("null"))
	{
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.CDRdelivery_Dropdown, "CDR Delivery", edit_CDRdelivery);
		CDR_code=CDRdelivery_code( edit_CDRdelivery);
	}
	else
	{
		CDR_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.CDRdelivery_Dropdown).getAttribute("value");
		Report.LogInfo("INFO","CDR Delivery dropdown value is: "+CDR_code,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "CDR Delivery dropdown value is: "+CDR_code);
	}

	//ResellerID
	String ResellerIDValue= resellerID(edit_resellerID_value);

	//Customer ID
	String CustomerID= findWebElement(APT_VoiceLineObj.APT_VoiceLine.customerid).getAttribute("value");

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown);
	//Gateway
	if(!edit_gateway.equalsIgnoreCase("null"))
	{
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.gateway_Dropdown, "Gateway", edit_gateway);
		gatewayCode=gateway_code( edit_gateway);
		Thread.sleep(2000);
	}
	else
	{
		
		gatewayCode=gateway_code( gateway);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Gateway dropdown value is: "+gatewayCode);
		Report.LogInfo("INFO","Gateway dropdown value is: "+gatewayCode,"PASS");
	}

	//Quality
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown, "Quality", edit_quality);

	String quality_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown).getAttribute("value");
	
	Report.LogInfo("INFO","Quality dropdown value is: "+quality_code,"PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Quality dropdown value is: "+quality_code);

	//Trunk Name
	String Edit_TrunkName=country_code+gatewayCode+voip_code+"L"+CustomerID+"00"+CDR_code+ResellerIDValue+quality_code+"00";
	int totalLen=Edit_TrunkName.length();
	//System.out.println("Total lenth of 'Trunk Group' field is "+ totalLen);
	//System.out.println("Trunk name is "+ Edit_TrunkName);
	if(totalLen==20) {
		compareText_fromtextFields( "Trunk Group Name", APT_VoiceLineObj.APT_VoiceLine.trunkGroupName_TextField, Edit_TrunkName);
		Edit_primarytrunkGroupname=Edit_TrunkName;
		//System.out.println("Trunk group name is: "+Edit_primarytrunkGroupname);
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Trunk Group Name' length is: "+totalLen);
		Report.LogInfo("INFO"," 'Trunk Group Name' length is: "+totalLen,"FAIL");
		//compareText_fromtextFields( "Trunk Group Name",APT_VoiceLineObj.APT_VoiceLine.trunkGroupName_TextField, Edit_TrunkName);
	}

	//COS Profile
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.cosprofile, "COS Profile", edit_cosprofile_value);
	String COSProfileValue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.cosprofile).getAttribute("value");
	
	ExtentTestManager.getTest().log(LogStatus.PASS, "COS Profile dropdown value is: "+COSProfileValue);
	Report.LogInfo("INFO","COS Profile dropdown value is: "+COSProfileValue,"PASS");


	//LAN Range
	edittextFields_commonMethod( "LAN Range", APT_VoiceLineObj.APT_VoiceLine.lanrange, edit_lanrange_value);
	String LANRangevalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.lanrange).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "LAN Range value is: "+LANRangevalue);
	Report.LogInfo("INFO","LAN Range value is: "+LANRangevalue,"PASS");

	//IP Address Type
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.IPaddresstype_Dropdown, "IP Address Type", edit_ipAddresstype);
	waitforPagetobeenable();
	String IPAddressTypevalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.IPaddresstype_Dropdown).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "IP Address Type dropdown value is: "+IPAddressTypevalue);
	Report.LogInfo("INFO", "IP Address Type dropdown value is: "+IPAddressTypevalue,"PASS");
	//Internet Based Customer
	String InternetBasedCustomer_Checked= findWebElement(APT_VoiceLineObj.APT_VoiceLine.internetBasedCustomer_checkbox).getAttribute("checked");
	String InternetBasedCustomer_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.internetBasedCustomer_checkbox).getAttribute("disabled");
	if(InternetBasedCustomer_Checked==null && InternetBasedCustomer_Disabled==null)
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Internet Based Customer' is not checked and disabled by default");
		Report.LogInfo("INFO",  "'Internet Based Customer' is not checked and disabled by default","FAIL");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Internet Based Customer' is checked and disabled by default");
		Report.LogInfo("INFO", "'Internet Based Customer' is checked and disabled by default","PASS");
	}

	//Splitting the Gateway functionality into 2
	if(!edit_gateway.contains("SBC")) {

		if(edit_thirdpartyinternet.equalsIgnoreCase("Yes")) {

			editcheckbox_commonMethod( edit_thirdpartyinternet, APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet");
			String vlanDefaultvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
			if(vlanDefaultvalue.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when '3rd Party Internet' is selected");
				Report.LogInfo("INFO", "No values displaying under 'VLAN tag' field by default, when '3rd Party Internet' is selected","FAIL");
			}else {

				ExtentTestManager.getTest().log(LogStatus.PASS, "When '3rd Party Internet' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue);
				Report.LogInfo("INFO", "When '3rd Party Internet' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue,"PASS");
				if(edit_vlanTag.equalsIgnoreCase("null")) {

					//Sub Interface slot
					selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", edit_subInterfaceSlot);
					String SubInterfaceSlot_value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown).getAttribute("value");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value);
					Report.LogInfo("INFO", "Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value,"PASS");
					if(edit_subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group  
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
					else if(!edit_subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+edit_subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+edit_subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}

				}else {

					//VLAN Tag
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, edit_vlanTag);
					String vlanTagUpdatedvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

					//Sub Interface slot
					selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", edit_subInterfaceSlot);
					String SubInterfaceSlot_value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown).getAttribute("value");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value);
					Report.LogInfo("INFO","Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value,"PASS");
					if(edit_subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTagUpdatedvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTagUpdatedvalue+NIGgroupdefaultValue_last;
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
					else if(!edit_subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+edit_subInterfaceSlot+subInterfacename_middle+vlanTagUpdatedvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+edit_subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTagUpdatedvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
				}
			}
		}
		//3rd party internet checkbox not selected 
		else {
			if(edit_vlanTag.equalsIgnoreCase("null")) {
				String vlanDefaultvalue1=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", edit_subInterfaceSlot);
				String SubInterfaceSlot_value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown).getAttribute("value");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value);
				Report.LogInfo("INFO","Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value,"PASS");
				if(edit_subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue1+NIGgroupdefaultValue_last;
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
				else if(!edit_subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+edit_subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+edit_subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue1+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}

			}else {

				//VLAN Tag
				clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
				edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, edit_vlanTag);
				String vlanUpdatedvalue1=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", edit_subInterfaceSlot);
				String SubInterfaceSlot_value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown).getAttribute("value");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value);
				Report.LogInfo("INFO","Sub Interface Slot dropdown value is displaying as "+SubInterfaceSlot_value,"PASS");

				if(edit_subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanUpdatedvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanUpdatedvalue1+NIGgroupdefaultValue_last;
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
				else if(!edit_subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+edit_subInterfaceSlot+subInterfacename_middle+vlanUpdatedvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+edit_subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanUpdatedvalue1+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
			}

		}
	}

	else if(edit_gateway.contains("SBC")) {
		if(edit_gateway.startsWith("FRA")) {
			if(!edit_thirdpartyinternet.equalsIgnoreCase("Yes")) {
				if(edit_vlanTag.equalsIgnoreCase("null")) {
					//String vlanDefaultvalue2=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).getAttribute("value");

					//IP Interface
					boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
					if(ipInterface_Enabled) {
						Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Ip Interface' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					}
					compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterfaceDEfaultValue);  //verify default values

					//Address Context
					boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
					if(addressContext_Enabled) {
						Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					}

					compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField, addressContextDefaultValue);  //verify default values


					//IP Interface Group
					boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
					if(ipInterfaceGroup_Enabled) {
						Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					}else {
						Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					}

					compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values

				}
				else if(!edit_vlanTag.equalsIgnoreCase("null")) {

					//VLAN Tag
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, edit_vlanTag_FRASBC_value);
					String vlanUpdatedvalue2=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

					//IP Interface
					boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
					if(ipInterface_Enabled) {
						Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					}

					ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue2;
					compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

					//Address Context
					boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
					if(addressContext_Enabled) {
						Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					}

					addressContext=addressContextDefaultValue+vlanUpdatedvalue2;
					compareText_fromtextFields( "Address Context",APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


					//IP Interface Group
					boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
					if(ipInterfaceGroup_Enabled) {
						Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					}else {
						Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					}

					ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue2;
					compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values

				}

			}
			else if(edit_thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//3rd party internet
				addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet", edit_thirdpartyinternet);

				String vlanDefaultvalue3=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue3.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, edit_vlanTag_FRASBC_value);
					vlanUpdatedvalue3=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}

				ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue3;
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				addressContext=addressContextDefaultValue+vlanUpdatedvalue3;
				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue3;
				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			}
		}
		else {
			if(!edit_thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//VLAN tag  
				String vlanDefaultvalue4=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue4.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
					Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, edit_vlanTag_FRASBC_value);
					//String vlanUpdatedvalue4=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).getAttribute("value");

				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField, addressContextDefaultValue);  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values

			}

			if(edit_thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//3rd party internet
				addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet", edit_thirdpartyinternet);

				String vlanDefaultvalue5=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue5.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
					Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, edit_vlanTag_FRASBC_value);
					vlanUpdatedvalue5=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}

				ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue5;
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				addressContext=addressContextDefaultValue+vlanUpdatedvalue5;
				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue5;
				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			}
		}
	}

	//Signalling Zone
	edittextFields_commonMethod( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, edit_signallingZone);
	String SignallingZone_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Signalling Zone value is: "+SignallingZone_Value);
	Report.LogInfo("INFO","Signalling Zone value is: "+SignallingZone_Value,"PASS");

	//Number Porting checkbox
	editcheckbox_commonMethod( edit_numberporting_checkbox, APT_VoiceLineObj.APT_VoiceLine.numberporting_checkbox, "Number Porting");
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField);
	//CLI Features
	cliFeatures( edit_CLIPScreeningandCLIRperCall, edit_clirPermanent, edit_clipNoScreening, edit_clipMainNumber, edit_presentationNumbers, edit_presentationnumber_value);

	//Customer Default Number
	edittextFields_commonMethod( "Customer Default Number", APT_VoiceLineObj.APT_VoiceLine.customerdefaultnumber_textfield, edit_customerdefaultnumber_value);
	String CustomerDefaultNumber_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.customerdefaultnumber_textfield).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Customer Default Number value is: "+CustomerDefaultNumber_Value);
	Report.LogInfo("INFO", "Customer Default Number value is: "+CustomerDefaultNumber_Value,"PASS");

	//Invalid CLI Treatment
	boolean invalidCLITreatment_Allow= findWebElement(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_allow).isSelected();

	if(invalidCLITreatment_Allow)
	{
		//Multisite Default Number
		String MultisiteDefaultNumber= findWebElement(APT_VoiceLineObj.APT_VoiceLine.multisitedefaultnumber).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'MultiSite Default Number' value is displaying as "+MultisiteDefaultNumber);
		Report.LogInfo("INFO", " 'MultiSite Default Number' value is displaying as "+MultisiteDefaultNumber,"PASS");
	}
	else
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_block,"Block");
		click(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_block,"Block");
	}

	//Egress Number Format
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.egressnumberformat_dropdown, "Egress Number Format", edit_egressnumberformat_dropdownvalue);
	String EgressNumberFormat_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.egressnumberformat_dropdown).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Egress Number Format value is: "+EgressNumberFormat_Value);
	Report.LogInfo("INFO","Egress Number Format value is: "+EgressNumberFormat_Value,"PASS");
	//Call Admission Control
	editcheckbox_commonMethod( edit_callAdmissionControl, APT_VoiceLineObj.APT_VoiceLine.callAdmissionControl_checkbox, "Call Admission Control");

	if(edit_callAdmissionControl.equalsIgnoreCase("yes")) {

		//Call Limit
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.callLimit_Dropdown, "Call Limit", edit_callLimit);
		String CallLimit_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.callLimit_Dropdown).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Call Limit value is: "+CallLimit_Value);
		Report.LogInfo("INFO","Call Limit value is: "+CallLimit_Value,"PASS");

		if(edit_callLimit.equalsIgnoreCase("Defined")) {
			edittextFields_commonMethod( "Limit Number", APT_VoiceLineObj.APT_VoiceLine.limitNumber_textField, edit_limitNumber);
			String LimitNumber_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.limitNumber_textField).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Limit Number value is: "+LimitNumber_Value);
			Report.LogInfo("INFO","Limit Number value is: "+LimitNumber_Value,"PASS");
		}
	}

	//Call Rate Limit
	editcheckbox_commonMethod( edit_callrateLimit, APT_VoiceLineObj.APT_VoiceLine.callrateLimit_checkbox, "Call Rate Limit");
	if(edit_callrateLimit.equalsIgnoreCase("yes")) {

		String callratelimitactualvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.callRateLimitt_textField).getAttribute("value");
		Report.LogInfo("INFO"," 'Call rate Limit' value is displaying as "+callratelimitactualvalue,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);

		if(!edit_callrateLimiteValue.equalsIgnoreCase("null")) {
			int i=Integer.parseInt(edit_callrateLimiteValue);

			if(i>100) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less than 100 for all Trunks");
				Report.LogInfo("INFO","The CallRateLimit should be less than 100 for all Trunks","FAIL");
			}
			else if(i<=100){
				edittextFields_commonMethod( "Call rate Limit", APT_VoiceLineObj.APT_VoiceLine.callRateLimitt_textField, edit_callrateLimiteValue);
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
			Report.LogInfo("INFO","'Call rate Limit' value is not edited","PASS");
		}
	}

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.egressnumberformat_dropdown);
	waitforPagetobeenable();
	//Usage of Incoming routing on DDI Level
	editcheckbox_commonMethod( edit_incomingroutingonDDIlevel_checkbox, APT_VoiceLineObj.APT_VoiceLine.incomingroutingonDDIlevel_checkbox, "Usage of Incoming routing on DDI Level");

	//PSX Manual Configuration-Trunk Group 
	editcheckbox_commonMethod( edit_PSXmanualConfigTG_value, APT_VoiceLineObj.APT_VoiceLine.PSXmanualConfigTG_checkbox, "PSX Manual Configuration-Trunk Group");

	//PSX Manual Configuration-DDI-Range
	editcheckbox_commonMethod( edit_PSXmanualConfigDDI_value, APT_VoiceLineObj.APT_VoiceLine.PSXmanualConfigDDIRange_checkbox, "PSX Manual Configuration-DDI-Range");

	if(edit_gateway.contains("SBC")) {
		//Manual Configuration on SBC
		editcheckbox_commonMethod( edit_SBCmanualconfigValue, APT_VoiceLineObj.APT_VoiceLine.SBCmanualconfig_checkbox, "Manual Configuration On SBC");

	}else {
		//Manual Configuration On GSX
		editcheckbox_commonMethod( edit_GSXmanualConfigvalue, APT_VoiceLineObj.APT_VoiceLine.GSXmanualConfig_checkbox, "Manual Configuration On GSX");
	}

	//CPE Manual Configuration
	editcheckbox_commonMethod( edit_cpemanualconfig_checkbox, APT_VoiceLineObj.APT_VoiceLine.cpemanualconfig_checkbox, "CPE Manual Configuration");

	//DR using TDM
	editcheckbox_commonMethod( edit_DRusingTDM_checkbox, APT_VoiceLineObj.APT_VoiceLine.DRusingTDM_checkbox, "DR Using TDM");

	//Codec
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.codec_dropdown, "Codec", edit_codec_value);
	String Codec_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.codec_dropdown).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Codec dropdown value is: "+Codec_Value);
	Report.LogInfo("INFO", "Codec dropdown value is: "+Codec_Value,"PASS");
	scrolltoend();
	//Fax Diversion Numbers
	if(!edit_faxdiversionnumber_value.equalsIgnoreCase("null"))
	{
		clearTextBox(APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield);
		edittextFields_commonMethod( "Fax Diversion Numbers", APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield, edit_faxdiversionnumber_value);
		
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.faxdiversion_addarrow,"Fax Diversion Numbers Add arrow");
		click( APT_VoiceLineObj.APT_VoiceLine.faxdiversion_addarrow,"Fax Diversion Numbers Add arrow");
	}
	else
	{
		String FaxDiversionNumber_Value=findWebElement(APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Fax Diversion Numbers value is: "+FaxDiversionNumber_Value);
		Report.LogInfo("INFO", "Fax Diversion Numbers value is: "+FaxDiversionNumber_Value,"PASS");
	}

	//Partial Number Replacement
	editcheckbox_commonMethod( edit_partialnumberreplacement_checkbox, APT_VoiceLineObj.APT_VoiceLine.partialnumberreplacement_checkbox, "Partial Number Replacement");
	waitforPagetobeenable();
	scrolltoend();
	//click_commonMethod( "OK", "trunk_okButton");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	waitforPagetobeenable();
	verifysuccessmessage( "Trunk updated successfully");
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");

}

public void verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(String siteOrderName) throws InterruptedException, IOException {

	waitforPagetobeenable();
	waitforPagetobeenable();
	scrolltoend();
	
	boolean siteOrderUnderTrunkPanel=false;


	//siteOrderUnderTrunkPanel=getwebelement("//span[text()='"+ siteOrderName +"']").isDisplayed();
	siteOrderUnderTrunkPanel=webDriver.findElement(By.xpath("//span[text()='"+ siteOrderName +"']")).isDisplayed();

	
	if(siteOrderUnderTrunkPanel) {

		ExtentTestManager.getTest().log(LogStatus.PASS, siteOrderName + " 'Site Order' is displaying under 'Trunk' panel");
		Report.LogInfo("INFO",siteOrderName + " 'Site Order' is displaying under 'Trunk' panel","PASS");

		//Click on Add trunk link	
		//String addTrunklinkXpath="//div[div[span[text()='"+ siteOrderName +"']]]/following-sibling::div//span[text()='Add Trunk']";
		
		//click_commonMethod_PassingWebelementDirectly("Add Trunk", addTrunklinkXpath);
		webDriver.findElement(By.xpath("//div[div[span[text()='"+ siteOrderName +"']]]/following-sibling::div//span[text()='Add Trunk']")).click();
		
		waitforPagetobeenable();

	}
	else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, siteOrderName + " 'Site Order' is not displaying under 'Trunk' panel");
		Report.LogInfo("INFO",siteOrderName + " 'Site Order' is not displaying under 'Trunk' panel","FAIL");
	}
}

public void addResilienttrunk(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {   
	
	String newCustomerCreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomerCreation");
	String newCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"newCustomer");
	String existingCustomerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"existingCustomer");
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String resilient_country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilient_Country");
	String Resilient_CDRdelivery=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilient_CDRdelivery");
	String resilient_gateway=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilient_Gateway");
	String resilient_quality=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilient_Quality");
	String SIPURI=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPURI");
	String resilient_resellerID_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilient_ResellerID_value");
	String ipAddresstype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipAddresstype");
	String SIPsignallingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPsignallingPort");
	String thirdpartyinternet=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyInternet");
	String vlanTag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTag");
	String subInterfaceSlot=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"subInterfaceSlot");
	String signallngZone=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallngZone");
	String callAdmissionControl=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callAdmissionControl");
	String callrateLimit=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimitselection");
	String PSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigvalue");
	String GSXmanualConfigvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"GSXmanualConfigvalue");
	String callLimit=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callLimit");
	String limitNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"limitNumber");
	String callrateLimiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimiteValue");
	String SBCmanualconfigValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SBCmanualconfigValue");
	String cosprofile_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COSProfile_Value");
	String lanrange_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LANRange_Value");
	String numberporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberPorting_Checkbox");
	String CLIPScreeningandCLIRperCall =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CLIPScreeningandCLIRperCall");
	String clirPermanent=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CLIRPermanent_radio");
	String clipNoScreening=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ClipNoScreening_radio");
	String clipMainNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ClipMainNumber_radio");
	String presentationNumbers=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PresentationNumbers_radio");
	String presentationnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PresentationNumber_Value");
	String customerdefaultnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefaultNumber_Value");
	String egressnumberformat_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EgressNumberFormat_DropdownValue");
	String incomingroutingonDDIlevel_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IncomingRoutingOnDDILevel_Checkbox");
	String PSXmanualConfigTG_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigTG_value");
	String PSXmanualConfigDDI_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXmanualConfigDDI_value");
	String DRusingTDM_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DRusingTDM_checkbox");
	String codec_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Codec_Value");
	String faxdiversionnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FaxDiversionNumber_Value");
	String partialnumberreplacement_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PartialNumberReplacement_Checkbox");
	String cpemanualconfig_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpemanualconfig_checkbox");
	String vlanTag_FRASBC_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTag_FRASBC_value");

	String gatewayCode=null;
	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";
	String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
	String addressContextDefaultValue="EXTERNAL_AC_";
	String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
	String signallingZoneDefaultValue="OUT-UNTRUSTED";

	String SubInterfaceName=null;
	String NIFgroup=null;
	String ipInterface=null;
	String addressContext=null;
	String ipInterfaceGroup=null;
	String vlanUpdatedvalue3=null;
	String vlanUpdatedvalue5=null;
	String CDR_code=null;
	String customerName=null;

	waitforPagetobeenable();

	if(newCustomerCreation.equalsIgnoreCase("yes")){
		customerName= newCustomerName;
	}
	else
	{
		customerName= existingCustomerName;
	}

	
	String expectedValue1=customerName+"_"+sid+"_"+"IPVL";
	waitforPagetobeenable();
	compareText_fromtextFields( "Trunk Group Description", APT_VoiceLineObj.APT_VoiceLine.trunkGroupDEscription_textField, expectedValue1);

	scrollIntoTop();
	//Primary trunk group
	//WebElement PrimaryTrunkGroup= getwebelement(xml.getlocator("//locators/" + application + "/primaryTrunkGroup_Dropdown"));
	//selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.primaryTrunkGroup_Dropdown, "Primary Trunk Group", Edit_primarytrunkGroupname);
	waitforPagetobeenable();

	//VOIP Protocol
	GetText( "VOIP Protocol", APT_VoiceLineObj.APT_VoiceLine.voipProtocol_Dropdown);
	String voip_code="S";

	//SIP URI
	//addtextFields_commonMethod( "SIP URI", APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField, SIPURI);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField,"SIP URI");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.SIPURI_textField,SIPURI,"SIP URI");
	
	
	//message displaying under "SIP URI" text field	
	methodToFindMessagesUnderTextField( APT_VoiceLineObj.APT_VoiceLine.SIPURI_defaultValue_textvalue, "SIP URI", "Default URI: va.sip.colt.net");

	//SIP Signalling Port
	//addtextFields_commonMethod( "SIP Signaling Port", APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField, SIPsignallingPort);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField,"SIP Signaling Port");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.SIPsignallingport_textField,SIPsignallingPort,"SIP Signaling Port");
	//message displaying under "SIP Signalling Port" text field	
	methodToFindMessagesUnderTextField( APT_VoiceLineObj.APT_VoiceLine.SIPsignallingPOrt_defaultValue_textvalue, "SIP Signalling Port", "Default Port: 5060");

	//Billing Country
	//warningMessage_commonMethod( "country_warningMessage", "Billing Country");   //validate warning message
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown, "Billing Country", resilient_country);

	String country_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.billingCoutry_Dropdown).getAttribute("value");
	//System.out.println("country dropdown value is: "+country_code);

	//CDR Delivery

	if(!Resilient_CDRdelivery.equalsIgnoreCase("null"))
	{
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.CDRdelivery_Dropdown, "CDR Delivery", Resilient_CDRdelivery);
		CDR_code=CDRdelivery_code( Resilient_CDRdelivery);
	}
	else
	{
		CDR_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.CDRdelivery_Dropdown).getAttribute("value");
		Report.LogInfo("INFO","CDR Delivery dropdown value is: "+CDR_code,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "CDR Delivery dropdown value is: "+CDR_code);
	}

	//ResellerID
	String ResellerIDValue= resellerID( resilient_resellerID_value);

	//Customer ID
	String CustomerID= findWebElement(APT_VoiceLineObj.APT_VoiceLine.customerid).getAttribute("value");

	//Gateway
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.gateway_Dropdown, "Gateway", resilient_gateway);
	waitforPagetobeenable();
	gatewayCode=gateway_code( resilient_gateway);
	waitforPagetobeenable();

	//Quality
	//click(APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown1);
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown, "Quality", resilient_quality);
	String quality_code=findWebElement(APT_VoiceLineObj.APT_VoiceLine.quality_Dropdown).getAttribute("value");

	//Trunk Name
	String trunkName=country_code+gatewayCode+voip_code+"L"+CustomerID+"00"+CDR_code+ResellerIDValue+quality_code+"0A";

	int totalLen=trunkName.length();
	//System.out.println("Total lenth of 'Trunk Group' field is "+ totalLen);
	//System.out.println("Trunk name is "+ trunkName);
	if(totalLen==20) {
		//compareText_fromtextFields( "Trunk Group Name", APT_VoiceLineObj.APT_VoiceLine.trunkGroupName_TextField, trunkName);
		primarytrunkGroupname_Resilient=trunkName;
		//System.out.println("Trunk group name is: "+primarytrunkGroupname_Resilient);
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Trunk Group Name' length is: "+totalLen);
		Report.LogInfo("INFO"," 'Trunk Group Name' length is: "+totalLen,"FAIL");
		compareText_fromtextFields( "Trunk Group Name", APT_VoiceLineObj.APT_VoiceLine.trunkGroupName_TextField, trunkName);
	}

	//COS Profile
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.cosprofile, "COS Profile", cosprofile_value);

	//LAN Range
	addtextFields_commonMethod( "LAN Range", APT_VoiceLineObj.APT_VoiceLine.lanrange, lanrange_value);
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.lanrange,"LAN Range");
	//sendKeys(APT_VoiceLineObj.APT_VoiceLine.lanrange,lanrange_value,"LAN Range");

	//IP Address Type
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.IPaddresstype_Dropdown, "IP Address Type", ipAddresstype);
	waitforPagetobeenable();

	//Internet Based Customer
	String InternetBasedCustomer_Checked= findWebElement(APT_VoiceLineObj.APT_VoiceLine.internetBasedCustomer_checkbox).getAttribute("checked");
	String InternetBasedCustomer_Disabled= findWebElement(APT_VoiceLineObj.APT_VoiceLine.internetBasedCustomer_checkbox).getAttribute("disabled");
	if(InternetBasedCustomer_Checked==null && InternetBasedCustomer_Disabled==null)
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Internet Based Customer' is not checked and disabled by default");
		Report.LogInfo("INFO","'Internet Based Customer' is not checked and disabled by default","FAIL");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Internet Based Customer' is checked and disabled by default");
		Report.LogInfo("INFO","'Internet Based Customer' is checked and disabled by default","PASS");
	}

	//Splitting the Gateway functionality into 2
	if(!resilient_gateway.contains("SBC")) {

		if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

			addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet", thirdpartyinternet);

			String vlanDefaultvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
			if(vlanDefaultvalue.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when '3rd Party Internet' is selected");
				Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when '3rd Party Internet' is selected","FAIL");
			}else {

				ExtentTestManager.getTest().log(LogStatus.PASS, "When '3rd Party Internet' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue);
				Report.LogInfo("INFO","When '3rd Party Internet' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue,"PASS");
				if(vlanTag.equalsIgnoreCase("null")) {

					//Sub Interface slot
					selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

					if(subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group  
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}

				}else {

					//VLAN Tag
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag);
					String vlanTagUpdatedvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

					//Sub Interface slot
					selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

					if(subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTagUpdatedvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTagUpdatedvalue+NIGgroupdefaultValue_last;
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTagUpdatedvalue;
						compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTagUpdatedvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
					}
				}
			}
		}
		//3rd party internet checkbox not selected 
		else {
			if(vlanTag.equalsIgnoreCase("null")) {
				String vlanDefaultvalue1=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

				if(subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue1+NIGgroupdefaultValue_last;
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
				else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue1+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}

			}else {

				//VLAN Tag
				clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
				edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag);
				String vlanUpdatedvalue1=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);

				if(subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanUpdatedvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanUpdatedvalue1+NIGgroupdefaultValue_last;
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
				else if(!subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanUpdatedvalue1;
					compareText_fromtextFields( "Sub Interface Name", APT_VoiceLineObj.APT_VoiceLine.subInterfaceName_textField, SubInterfaceName);

					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanUpdatedvalue1+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText_fromtextFields( "NIF Group", APT_VoiceLineObj.APT_VoiceLine.NIFgrouopp_textField, NIFgroup);
				}
			}

		}
	}

	else if(resilient_gateway.contains("SBC")) {
		if(resilient_gateway.startsWith("FRA")) {
			if(!thirdpartyinternet.equalsIgnoreCase("Yes")) {
				if(vlanTag.equalsIgnoreCase("null")) {
					//String vlanDefaultvalue2=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).getAttribute("value");

					//IP Interface
					boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
					if(ipInterface_Enabled) {
						Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Ip Interface' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					}
					compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values

					//Address Context
					boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
					if(addressContext_Enabled) {
						Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					}

					compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField, addressContextDefaultValue);  //verify default values


					//IP Interface Group
					boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
					if(ipInterfaceGroup_Enabled) {
						Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					}else {
						Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					}

					compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values

				}
				else if(!vlanTag.equalsIgnoreCase("null")) {

					//VLAN Tag
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					String vlanUpdatedvalue2=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");

					//IP Interface
					boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
					if(ipInterface_Enabled) {
						Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					}

					ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue2;
					compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

					//Address Context
					boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
					if(addressContext_Enabled) {
						Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					}else {
						Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					}

					addressContext=addressContextDefaultValue+vlanUpdatedvalue2;
					compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


					//IP Interface Group
					boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
					if(ipInterfaceGroup_Enabled) {
						Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					}else {
						Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					}

					ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue2;
					compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values

				}

			}
			else if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//3rd party internet
				editcheckbox_commonMethod( thirdpartyinternet, APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet");
				String vlanDefaultvalue3=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue3.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
					Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					vlanUpdatedvalue3=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}

				ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue3;
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				addressContext=addressContextDefaultValue+vlanUpdatedvalue3;
				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue3;
				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			}
		}
		else {
			if(!thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//VLAN tag  
				String vlanDefaultvalue4=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue4.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
					Report.LogInfo("INFO", "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					
					//String vlanUpdatedvalue4=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).getAttribute("value");

				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField, addressContextDefaultValue);  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values

			}

			if(thirdpartyinternet.equalsIgnoreCase("Yes")) {

				//3rd party internet
				editcheckbox_commonMethod( thirdpartyinternet, APT_VoiceLineObj.APT_VoiceLine.thirdpartyinternet_checkbox, "3rd Party Internet");
				String vlanDefaultvalue5=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				if(vlanDefaultvalue5.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected");
					Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Thrid Party Internet' is selected","FAIL");
				}else {
					//VLAN tag  
					clearTextBox(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField);
					edittextFields_commonMethod( "VLAN Tag", APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField, vlanTag_FRASBC_value);
					vlanUpdatedvalue5=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vlanTag_textField).getAttribute("value");
				}

				//IP Interface
				boolean ipInterface_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField).isEnabled();
				if(ipInterface_Enabled) {
					Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				}

				ipInterface = ipInterfaceDEfaultValue +vlanUpdatedvalue5;
				compareText_fromtextFields( "IP Interface", APT_VoiceLineObj.APT_VoiceLine.ipInterface_textField,ipInterface );  

				//Address Context
				boolean addressContext_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField).isEnabled();
				if(addressContext_Enabled) {
					Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				}else {
					Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				}

				addressContext=addressContextDefaultValue+vlanUpdatedvalue5;
				compareText_fromtextFields( "Address Context", APT_VoiceLineObj.APT_VoiceLine.AddressContext_textField,addressContext );  //verify default values


				//IP Interface Group
				boolean ipInterfaceGroup_Enabled=findWebElement(APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField).isEnabled();
				if(ipInterfaceGroup_Enabled) {
					Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				}else {
					Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				}

				ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanUpdatedvalue5;
				compareText_fromtextFields( "IP Interface Group", APT_VoiceLineObj.APT_VoiceLine.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			}
		}
	}

	//Signalling Zone
	if(thirdpartyinternet.equalsIgnoreCase("yes")) {

		compareText_fromtextFields( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, signallingZoneDefaultValue);

		edittextFields_commonMethod( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, signallngZone);

	}else {
		edittextFields_commonMethod( "Signaling Zone", APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField, signallngZone);
	}

	//Number Porting checkbox
	editcheckbox_commonMethod( numberporting_checkbox, APT_VoiceLineObj.APT_VoiceLine.numberporting_checkbox, "Number Porting");

	//CLI Features
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.signallingZone_textField);
	waitforPagetobeenable();
	cliFeatures( CLIPScreeningandCLIRperCall, clirPermanent, clipNoScreening, clipMainNumber, presentationNumbers, presentationnumber_value);

	//Customer Default Number
	edittextFields_commonMethod( "Customer Default Number", APT_VoiceLineObj.APT_VoiceLine.customerdefaultnumber_textfield, customerdefaultnumber_value);

	//Invalid CLI Treatment
	boolean invalidCLITreatment_Allow= findWebElement(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_allow).isSelected();

	if(invalidCLITreatment_Allow)
	{
		//Multisite Default Number
		String MultisiteDefaultNumber= findWebElement(APT_VoiceLineObj.APT_VoiceLine.multisitedefaultnumber).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'MultiSite Default Number' value is displaying as "+MultisiteDefaultNumber);
		Report.LogInfo("INFO"," 'MultiSite Default Number' value is displaying as "+MultisiteDefaultNumber,"PASS");
	}
	else
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_block,"Block");
		click(APT_VoiceLineObj.APT_VoiceLine.invalidclitreatment_block,"Block");
	}

	//Egress Number Format
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.egressnumberformat_dropdown, "Egress Number Format", egressnumberformat_dropdownvalue);

	//Call Admission Control
	editcheckbox_commonMethod( callAdmissionControl, APT_VoiceLineObj.APT_VoiceLine.callAdmissionControl_checkbox, "Call Admission Control");
	if(callAdmissionControl.equalsIgnoreCase("yes")) {

		//Call Limit
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.callLimit_Dropdown, "Call Limit", callLimit);

		if(callLimit.equalsIgnoreCase("Defined")) {
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.limitNumber_textField, "Limit Number");
			sendKeys(APT_VoiceLineObj.APT_VoiceLine.limitNumber_textField, limitNumber,"Limit Number");
		}
	}

	ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.multisitedefaultnumber);
	//Call Rate Limit
	editcheckbox_commonMethod( callrateLimit, APT_VoiceLineObj.APT_VoiceLine.callrateLimit_checkbox, "Call Rate Limit");
	waitforPagetobeenable();
	if(callrateLimit.equalsIgnoreCase("yes")) {

		String callratelimitactualvalue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.callRateLimitt_textField).getAttribute("value");
		Report.LogInfo("INFO"," 'Call rate Limit' value is displaying as "+callratelimitactualvalue,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);

		if(!callrateLimiteValue.equalsIgnoreCase("null")) {
			int i=Integer.parseInt(callrateLimiteValue);

			if(i>100) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less than 100 for all Trunks");
				Report.LogInfo("INFO", "The CallRateLimit should be less than 100 for all Trunks","FAIL");
			}
			else if(i<=100){
				edittextFields_commonMethod( "Call rate Limit", APT_VoiceLineObj.APT_VoiceLine.callRateLimitt_textField, callrateLimiteValue);
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
			Report.LogInfo("INFO","'Call rate Limit' value is not edited","PASS");
		}
	}
	waitforPagetobeenable();
	//Usage of Incoming routing on DDI Level
	editcheckbox_commonMethod( incomingroutingonDDIlevel_checkbox, APT_VoiceLineObj.APT_VoiceLine.incomingroutingonDDIlevel_checkbox, "Usage of Incoming routing on DDI Level");

	waitforPagetobeenable();
	//PSX Manual Configuration-Trunk Group 
	editcheckbox_commonMethod( PSXmanualConfigTG_value, APT_VoiceLineObj.APT_VoiceLine.PSXmanualConfigTG_checkbox, "PSX Manual Configuration-Trunk Group");
	//PSX Manual Configuration-DDI-Range
	editcheckbox_commonMethod( PSXmanualConfigDDI_value, APT_VoiceLineObj.APT_VoiceLine.PSXmanualConfigDDIRange_checkbox, "PSX Manual Configuration-DDI-Range");

	if(resilient_gateway.contains("SBC")) {
		//Manual Configuration on SBC
		editcheckbox_commonMethod( SBCmanualconfigValue, APT_VoiceLineObj.APT_VoiceLine.SBCmanualconfig_checkbox, "Manual Configuration On SBC");
	}else {
		//Manual Configuration On GSX
		editcheckbox_commonMethod( GSXmanualConfigvalue, APT_VoiceLineObj.APT_VoiceLine.GSXmanualConfig_checkbox, "Manual Configuration On GSX");
	}

	//CPE Manual Configuration
	editcheckbox_commonMethod( cpemanualconfig_checkbox, APT_VoiceLineObj.APT_VoiceLine.cpemanualconfig_checkbox, "CPE Manual Configuration");
	//DR using TDM
	editcheckbox_commonMethod( DRusingTDM_checkbox, APT_VoiceLineObj.APT_VoiceLine.DRusingTDM_checkbox, "DR Using TDM");
	//Codec
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.codec_dropdown, "Codec", codec_value);

	scrolltoend();
	//Fax Diversion Numbers
	//addtextFields_commonMethod( "Fax Diversion Numbers", APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield, faxdiversionnumber_value);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield,"Fax Diversion Numbers");
	sendKeys( APT_VoiceLineObj.APT_VoiceLine.faxdiversionnumbers_textfield,faxdiversionnumber_value,"Fax Diversion Numbers");
	
	//click_commonMethod( "Fax Diversion Numbers Add arrow", "faxdiversion_addarrow");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.faxdiversion_addarrow,"Fax Diversion Numbers Add arrow");
	click( APT_VoiceLineObj.APT_VoiceLine.faxdiversion_addarrow,"Fax Diversion Numbers Add arrow");

	//Partial Number Replacement
	editcheckbox_commonMethod( partialnumberreplacement_checkbox, APT_VoiceLineObj.APT_VoiceLine.partialnumberreplacement_checkbox, "Partial Number Replacement");
	waitforPagetobeenable();
	scrolltoend();
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	waitforPagetobeenable();
	waitforPagetobeenable();
	verifysuccessmessage( "Trunk created successfully");
	scrollIntoTop();
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO", "Clicked on BreadCrump", "INFO");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrump");
	waitforPagetobeenable();
}

public void verifySynchronize() throws InterruptedException, IOException {

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	
	//click_commonMethod("Action dropdown", "serviceactiondropdown");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdow");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdow");
	
	//click_commonMethod("Synchronize", "synchronizelink_servicepanel");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.synchronizelink_servicepanel,"Synchronize");
	click( APT_VoiceLineObj.APT_VoiceLine.synchronizelink_servicepanel,"Synchronize");
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.customerdetailsheader);
	verifysuccessmessage("Sync started successfully. Please check the sync status of this service.");
	waitforPagetobeenable();
}

public void verifyAddVoiceCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {   
	
	String voiceCPE_devicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String voiceCPE_vendormodel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPE_VendorModel");
	String voiceCPE_managementaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPE_ManagementAddress");
	String voiceCPE_country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPE_Country");
	String voiceCPE_existingcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_ExistingCity");
	String voiceCPE_existingcityvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_ExistingCityValue");
	String voiceCPE_existingsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_ExistingSite");
	String voiceCPE_existingsitevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_Existing_SiteValue");
	String voiceCPE_existingpremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_ExistingPremise");
	String voiceCPE_existingpremisevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_Existing_PremiseValue");
	String voiceCPE_newcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewCity");
	String voiceCPE_cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewCityName");
	String voiceCPE_Citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewCityCode");
	String voiceCPE_sitename =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewSiteName");
	String voiceCPE_sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewSiteCode");
	String voiceCPE_premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewPremiseName");
	String voiceCPE_premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewPremiseCode");
	String voiceCPE_newsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewSite");
	String voiceCPE_NewPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voiceCPE_NewPremise");
	String cpetoprovidedialtone_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEToProvideDialTone_Checkbox");
	String cpelinepowerrequired_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPELinePowerRequired_Checkbox");
	String numberporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberPorting_Checkbox");
	String briportmapping_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPortMapping_Checkbox");
	String crcsettings_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CRCSettings_DropdownValue");
	String numberofpriports_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfPRIPorts_DropdownValue");
	String numberofbriports_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfBRIPorts_DropdownValue");
	String briport1_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort1Number_value");
	String briport2_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort2Number_value");
	String briport3_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort3Number_value");
	String briport4_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort4Number_value");
	String briport5_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort5Number_value");
	String briport6_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort6Number_value");
	String briport7_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort7Number_value");
	String briport8_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPort8Number_value");
	String numberofFXSports_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfFXSPorts_DropdownValue");
	String fxsnumber1_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FXSNumber1_value");
	String fxsnumber2_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FXSNumber2_value");

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.voiceCPEdevice_header);
	waitforPagetobeenable();
	//click_commonMethod( "Add Voice CPE Device", "addvoicecpe_link");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.addvoicecpe_link,"Add Voice CPE Device");
	click( APT_VoiceLineObj.APT_VoiceLine.addvoicecpe_link,"Add Voice CPE Device");
	waitforPagetobeenable();
	scrollIntoTop();
	compareText( "Add VOice CPE header", APT_VoiceLineObj.APT_VoiceLine.addvoicecpe_header, "Add Voice CPE");
	edittextFields_commonMethod( "Name", APT_VoiceLineObj.APT_VoiceLine.voicecpe_devicename, voiceCPE_devicename);
	//addDropdownValues_commonMethod( "Vendor/Model", "vendormodel_dropdown", voiceCPE_vendormodel);

	boolean availability=false;
	try {  
		availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vendormodel_dropdown).isDisplayed();
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown is displaying");
			Report.LogInfo("INFO","Vendor/Model dropdown is displaying","PASS");

			if(voiceCPE_vendormodel.equalsIgnoreCase("null")) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Vendor/Model dropdown");
				Report.LogInfo("INFO"," No values selected under Vendor/Model dropdown","PASS");
			}else {

				//Clickon(findWebElement("//div[label[text()='Vendor/Model']]//div[text()='�']"));
			
				webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//div[text()='�']")).click();
				
				waitforPagetobeenable();

				//verify list of values inside dropdown
				List<WebElement> listofvalues = webDriver.findElements(By.xpath("//span[contains(@class,'css-f2o6e8-ItemComponent evc32pp0')]"));

				ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Vendor/Model dropdown is:  ");
				Report.LogInfo("INFO", " List of values inside Vendor/Model dropdown is:  ","PASS");

				for (WebElement valuetypes : listofvalues) {
					Report.LogInfo("INFO","service sub types : " + valuetypes.getText(),"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
					//System.out.println(" " + valuetypes.getText());
				}

				waitforPagetobeenable();
				//SendKeys(findWebElement("//div[label[text()='Vendor/Model']]//input"), voiceCPE_vendormodel);	
				webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input")).sendKeys(voiceCPE_vendormodel);
				
				waitforPagetobeenable();

				//Clickon(findWebElement("(//span[contains(text(),'"+ voiceCPE_vendormodel +"')])[1]"));
				webDriver.findElement(By.xpath("(//span[contains(text(),'"+ voiceCPE_vendormodel +"')])[1]")).click();
				waitforPagetobeenable();

				String actualValue=webDriver.findElement(By.xpath("//label[contains(text(),'Vendor/Model')]/following::span[contains(text(),'"+voiceCPE_vendormodel+"')]")).getText();
				
				//label[text()='"+voiceCPE_vendormodel+"']/following-sibling::div//span
				
				ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown value selected as: "+ actualValue );
				Report.LogInfo("INFO", "Vendor/Model dropdown value selected as: "+ actualValue,"PASS");

			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
			Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
		Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Vendor/Model dropdown");
		Report.LogInfo("INFO"," NO value selected under Vendor/Model dropdown","FAIL");
	}

	//addtextFields_commonMethod( "Management Address/Mask [e.g. 10.0.0.0/27]", "managementaddress_voicecpe", voiceCPE_managementaddress);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.managementaddress_voicecpe,"Management Address/Mask [e.g. 10.0.0.0/27]");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.managementaddress_voicecpe,voiceCPE_managementaddress,"Management Address/Mask [e.g. 10.0.0.0/27]");
	
	
	compareText_fromtextFields( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.snmpro_textfield, "incc");

	//select country
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.voicecpe_devicename);
	waitforPagetobeenable();
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.countryinput, "Country", voiceCPE_country);

	//New City		
	if(voiceCPE_existingcity.equalsIgnoreCase("no") & voiceCPE_newcity.equalsIgnoreCase("yes")) {
		click(APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"Add City Switch");
		//City name
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, voiceCPE_cityname,"City Name");
		//City Code	
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, voiceCPE_Citycode,"City Code");
		//Site name
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, voiceCPE_sitename,"Site Name");
		//Site Code
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addCityToggleSelected, voiceCPE_sitecode,"Site Code");
		//Premise name	
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, voiceCPE_premisename,"Premise Name");
		//Premise Code	
		sendKeys( APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, voiceCPE_premisecode,"Premise Code");

	}	

	//Existing City	
	else if(voiceCPE_existingcity.equalsIgnoreCase("yes") & voiceCPE_newcity.equalsIgnoreCase("no")) {

		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.citydropdowninput, "City", voiceCPE_existingcityvalue);

		//Existing Site
		if(voiceCPE_existingsite.equalsIgnoreCase("yes") & voiceCPE_newsite.equalsIgnoreCase("no")) {
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.sitedropdowninput, "Site", voiceCPE_existingsitevalue);

			//Existing Premise
			if(voiceCPE_existingpremise.equalsIgnoreCase("yes") & voiceCPE_NewPremise.equalsIgnoreCase("no")) {
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.premisedropdowninput, "Premise", voiceCPE_existingpremisevalue);
			}

			//New Premise  
			else if(voiceCPE_existingpremise.equalsIgnoreCase("no") & voiceCPE_NewPremise.equalsIgnoreCase("yes")) {

				click(APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"Add Premise Switch");
				//Premise name	
				sendKeys(APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addPremiseToggleSelected, voiceCPE_premisename,"Premise Name");
				//Premise Code	
				sendKeys(APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addPremiseToggleSelected, voiceCPE_premisecode,"Premise Code");
			} 
		}

		else if(voiceCPE_existingsite.equalsIgnoreCase("no") & voiceCPE_newsite.equalsIgnoreCase("yes")) {

			click(APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"Add Site Switch");
			//Site name
			sendKeys( APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addSiteToggleSelected, voiceCPE_sitename,"Site Name");
			//Site Code
			sendKeys( APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addSiteToggleSelected, voiceCPE_sitecode,"Site Code");
			//Premise name	
			sendKeys( APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addSiteToggleSelected, voiceCPE_premisename,"Premise Name");
			//Premise Code	
			sendKeys( APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addSiteToggleSelected, voiceCPE_premisecode,"Premise Code");
		}
	}
	waitforPagetobeenable();
	scrolltoend();
	waitforPagetobeenable();
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.cpetoprovidedialtone_checkbox, "CPE to Provide Dial Tone", cpetoprovidedialtone_checkbox);
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.cpelinepowerrequired_checkbox, "CPE Line Power Required", cpelinepowerrequired_checkbox);
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.numberporting_checkbox, "Number Porting", numberporting_checkbox);
	addCheckbox_commonMethod( APT_VoiceLineObj.APT_VoiceLine.briportmapping_checkbox, "BRI Port Mapping", briportmapping_checkbox);
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.crcsettings_dropdown, "CRC Settings", crcsettings_dropdownvalue);
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.numberofpriports_dropdown, "Number of PRI Ports", numberofpriports_dropdownvalue);
	selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.numberofbriports_dropdown, "Number of BRI Ports", numberofbriports_dropdownvalue);
	if(briportmapping_checkbox.equalsIgnoreCase("Yes")) 
	{
		numberofBRIPorts( numberofbriports_dropdownvalue, briport1_value, briport2_value, briport3_value, briport4_value, briport5_value, briport6_value, briport7_value, briport8_value);
	}
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.numberoffxsports_dropdown, "Number of FXS Ports", numberofFXSports_dropdownvalue);
	NumberofFXSPorts( numberofFXSports_dropdownvalue, fxsnumber1_value, fxsnumber2_value);
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	waitforPagetobeenable();
	//click_commonMethod( "OK", "okbutton");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	verifysuccessmessage( "Device created successfully");
}

public void numberofBRIPorts(String numberofbriports_dropdownvalue, String briport1_value, String briport2_value, String briport3_value, String briport4_value, String briport5_value, String briport6_value, String briport7_value, String briport8_value) throws InterruptedException, IOException {
	
	
	if(numberofbriports_dropdownvalue.equalsIgnoreCase("1"))
	{
		//addtextFields_commonMethod("BRI Port 1 Number", "briport1_textfield", briport1_value);
		//click_commonMethod("BRI Port 1 Number Add arrow", "briport1_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		
		waitforPagetobeenable();
	}
	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("2"))
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
	}
	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("3"))
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 3 Number", "briport3_textfield", briport3_value);
		//click_commonMethod("BRI Port 3 Number Add arrow", "briport3_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,"BRI Port 3 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,briport3_value,"BRI Port 3 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		waitforPagetobeenable();
	}
	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("4"))
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 3 Number", "briport3_textfield", briport3_value);
		//click_commonMethod("BRI Port 3 Number Add arrow", "briport3_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,"BRI Port 3 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,briport3_value,"BRI Port 3 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 4 Number", "briport4_textfield", briport4_value);
		//click_commonMethod("BRI Port 4 Number Add arrow", "briport4_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,"BRI Port 4 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,briport4_value,"BRI Port 4 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		waitforPagetobeenable();
	}
	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("5"))
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 3 Number", "briport3_textfield", briport3_value);
		//click_commonMethod("BRI Port 3 Number Add arrow", "briport3_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,"BRI Port 3 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,briport3_value,"BRI Port 3 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 4 Number", "briport4_textfield", briport4_value);
		//click_commonMethod("BRI Port 4 Number Add arrow", "briport4_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,"BRI Port 4 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,briport4_value,"BRI Port 4 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 5 Number", "briport5_textfield", briport5_value);
		//click_commonMethod("BRI Port 5 Number Add arrow", "briport5_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,"OK");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,briport5_value,"OK");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		waitforPagetobeenable();
	}

	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("6"))
	{
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 3 Number", "briport3_textfield", briport3_value);
		//click_commonMethod("BRI Port 3 Number Add arrow", "briport3_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,"BRI Port 3 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,briport3_value,"BRI Port 3 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 4 Number", "briport4_textfield", briport4_value);
		//click_commonMethod("BRI Port 4 Number Add arrow", "briport4_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,"BRI Port 4 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,briport4_value,"BRI Port 4 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 5 Number", "briport5_textfield", briport5_value);
		//click_commonMethod("BRI Port 5 Number Add arrow", "briport5_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,"OK");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,briport5_value,"OK");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 6 Number", "briport6_textfield", briport6_value);
		//click_commonMethod("BRI Port 6 Number Add arrow", "briport6_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport6_textfield,"BRI Port 6 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport6_textfield,briport6_value,"BRI Port 6 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport6_addarrow,"BRI Port 6 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport6_addarrow,"BRI Port 6 Number Add arrow");
		waitforPagetobeenable();
	}
	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("7"))
	{
		//addtextFields_commonMethod("BRI Port 1 Number", "briport1_textfield", briport1_value);
		//click_commonMethod("BRI Port 1 Number Add arrow", "briport1_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 3 Number", "briport3_textfield", briport3_value);
		//click_commonMethod("BRI Port 3 Number Add arrow", "briport3_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,"BRI Port 3 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,briport3_value,"BRI Port 3 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 4 Number", "briport4_textfield", briport4_value);
		//click_commonMethod("BRI Port 4 Number Add arrow", "briport4_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,"BRI Port 4 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,briport4_value,"BRI Port 4 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 5 Number", "briport5_textfield", briport5_value);
		//click_commonMethod("BRI Port 5 Number Add arrow", "briport5_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,"OK");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,briport5_value,"OK");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 6 Number", "briport6_textfield", briport6_value);
		//click_commonMethod("BRI Port 6 Number Add arrow", "briport6_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport6_textfield,"BRI Port 6 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport6_textfield,briport6_value,"BRI Port 6 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport6_addarrow,"BRI Port 6 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport6_addarrow,"BRI Port 6 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 7 Number", "briport7_textfield", briport7_value);
		//click_commonMethod("BRI Port 7 Number Add arrow", "briport7_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport7_textfield,"BRI Port 7 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport7_textfield,briport7_value,"BRI Port 7 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport7_addarrow,"BRI Port 7 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport7_addarrow,"BRI Port 7 Number Add arrow");
		waitforPagetobeenable();
	}
	else if(numberofbriports_dropdownvalue.equalsIgnoreCase("8"))
	{
		//addtextFields_commonMethod("BRI Port 1 Number", "briport1_textfield", briport1_value);
		//click_commonMethod("BRI Port 1 Number Add arrow", "briport1_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,"BRI Port 1 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport1_textfield,briport1_value,"BRI Port 1 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport1_addarrow,"BRI Port 1 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 2 Number", "briport2_textfield", briport2_value);
		//click_commonMethod("BRI Port 2 Number Add arrow", "briport2_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,"BRI Port 2 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport2_textfield,briport2_value,"BRI Port 2 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport2_addarrow,"BRI Port 2 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 3 Number", "briport3_textfield", briport3_value);
		//click_commonMethod("BRI Port 3 Number Add arrow", "briport3_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,"BRI Port 3 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport3_textfield,briport3_value,"BRI Port 3 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport3_addarrow,"BRI Port 3 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 4 Number", "briport4_textfield", briport4_value);
		//click_commonMethod("BRI Port 4 Number Add arrow", "briport4_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,"OK");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport4_textfield,briport4_value,"BRI Port 4 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport4_addarrow,"BRI Port 4 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 5 Number", "briport5_textfield", briport5_value);
		//click_commonMethod("BRI Port 5 Number Add arrow", "briport5_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,"BRI Port 5 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport5_textfield,briport5_value,"BRI Port 5 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport5_addarrow,"BRI Port 5 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 6 Number", "briport6_textfield", briport6_value);
		//click_commonMethod("BRI Port 6 Number Add arrow", "briport6_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport6_textfield,"BRI Port 6 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport6_textfield,briport6_value,"BRI Port 6 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport6_addarrow,"BRI Port 6 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport6_addarrow,"BRI Port 6 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 7 Number", "briport7_textfield", briport7_value);
		//click_commonMethod("BRI Port 7 Number Add arrow", "briport7_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport7_textfield,"BRI Port 7 Number");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport7_textfield,briport7_value,"BRI Port 7 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport7_addarrow,"BRI Port 7 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport7_addarrow,"BRI Port 7 Number Add arrow");
		waitforPagetobeenable();
		//addtextFields_commonMethod("BRI Port 8 Number", "briport8_textfield", briport8_value);
		//click_commonMethod("BRI Port 8 Number Add arrow", "briport8_addarrow");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport8_textfield,"OK");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.briport8_textfield,briport8_value,"BRI Port 8 Number");
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.briport8_addarrow,"BRI Port 8 Number Add arrow");
		click(APT_VoiceLineObj.APT_VoiceLine.briport8_addarrow,"BRI Port 8 Number Add arrow");
		waitforPagetobeenable();
	}
}

public void NumberofFXSPorts(String numberofFXSports_dropdownvalue, String fxsnumber1_value, String fxsnumber2_value) throws InterruptedException, IOException {

	if(numberofFXSports_dropdownvalue.equalsIgnoreCase("1")) {

		//addtextFields_commonMethod("fxsnumber1", "fxsnumber1_textfield", fxsnumber1_value);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.fxsnumber1_textfield,"fxsnumber1");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.fxsnumber1_textfield,fxsnumber1_value,"fxsnumber1");
	}
	else if(numberofFXSports_dropdownvalue.equalsIgnoreCase("2")) {

		//addtextFields_commonMethod("fxsnumber1", "fxsnumber1_textfield", fxsnumber1_value);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.fxsnumber1_textfield,"fxsnumber1");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.fxsnumber1_textfield,fxsnumber1_value,"fxsnumber1");
		//addtextFields_commonMethod("fxsnumber2", "fxsnumber2_textfield", fxsnumber2_value);
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.fxsnumber2_textfield,"fxsnumber2");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.fxsnumber2_textfield,fxsnumber2_value,"fxsnumber2");
	}
}

public void ViewVoiceCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {   
	
	String voiceCPE_devicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String voiceCPE_vendormodel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPE_VendorModel");
	String voiceCPE_managementaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPE_ManagementAddress");
	String voiceCPE_country =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPE_Country");
	String cpetoprovidedialtone_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEToProvideDialTone_Checkbox");
	String cpelinepowerrequired_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPELinePowerRequired_Checkbox");
	String numberporting_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberPorting_Checkbox");
	String briportmapping_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BRIPortMapping_Checkbox");
	String crcsettings_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CRCSettings_DropdownValue");
	String numberofpriports_dropdownvalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfPRIPorts_DropdownValue");
	String numberofbriports_dropdownvalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfBRIPorts_DropdownValue");
	String numberofFXSports_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfFXSPorts_DropdownValue");

	
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.voiceCPEdevice_header);
	waitforPagetobeenable();

	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingvoicecpe_devicegrid).isDisplayed())
	{
		List<WebElement> AddedVoiceCPEDevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedvoiceCPEdevicename);
		//System.out.println(AddedVoiceCPEDevicesList);
		int AddedVoiceCPEDevicesCount= AddedVoiceCPEDevicesList.size();

		for(int i=0;i<AddedVoiceCPEDevicesCount;i++) {
			String AddedVoiceCPEDeviceNameText= AddedVoiceCPEDevicesList.get(i).getText();
			//System.out.println(AddedVoiceCPEDeviceNameText);
			String AddedVoiceCPEDevice_SNo= AddedVoiceCPEDeviceNameText.substring(0, 1);

	//WebElement AddedVoiceCPEDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedvoicecpedevice_viewlink).replace("value", AddedVoiceCPEDevice_SNo);
			webDriver.findElement(By.xpath("//b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//a[contains(text(),'View')]")).click();
			//Clickon(AddedVoiceCPEDevice_ViewLink);
			//click(APT_VoiceLineObj.APT_VoiceLine.addedvoicecpedevice_viewlink);
			waitforPagetobeenable();
			scrollIntoTop();
			//compareText( "Voice CPE Device", APT_VoiceLineObj.APT_VoiceLine.viewpage_voicecpedevice_header, "Voice CPE Device");
			compareText_InViewPage( "Name", voiceCPE_devicename);
			compareText_InViewPage( "Vendor/Model", voiceCPE_vendormodel);
			compareText_InViewPage( "Management Address", voiceCPE_managementaddress);
			compareText_InViewPage( "Snmpro", "incc");
			GetText( "Country", APT_VoiceLineObj.APT_VoiceLine.country_voiceCPE_viewpage);
			GetText( "City", APT_VoiceLineObj.APT_VoiceLine.city_voiceCPE_viewpage);
			GetText( "Site", APT_VoiceLineObj.APT_VoiceLine.site_voiceCPE_viewpage);
			GetText( "Premise", APT_VoiceLineObj.APT_VoiceLine.premise_voiceCPE_viewpage);

			if(cpetoprovidedialtone_checkbox.equalsIgnoreCase("Yes"))
			{
				compareText( "CPE to Provide Dial tone", APT_VoiceLineObj.APT_VoiceLine.cpetoprovide_checkboxvalue, "true");
			}
			else
			{
				compareText( "CPE to Provide Dial tone", APT_VoiceLineObj.APT_VoiceLine.cpetoprovide_checkboxvalue, "");
			}
			if(cpelinepowerrequired_checkbox.equalsIgnoreCase("Yes"))
			{
				compareText( "CPE line power required", APT_VoiceLineObj.APT_VoiceLine.cpelinepower_checkboxvalue, "true");
			}
			else
			{
				compareText( "CPE line power required", APT_VoiceLineObj.APT_VoiceLine.cpelinepower_checkboxvalue, "");
			}
			if(numberporting_checkbox.equalsIgnoreCase("Yes"))
			{
				compareText( "Number Porting", APT_VoiceLineObj.APT_VoiceLine.numberporting_checkboxvalue, "true");
			}
			else
			{
				compareText( "Number Porting", APT_VoiceLineObj.APT_VoiceLine.numberporting_checkboxvalue, "");
			}
			
			GetText( "BRI Port Mapping", APT_VoiceLineObj.APT_VoiceLine.BRIportmapping_checkboxvalue);
			//compareText_InViewPage1( "CRC Settings", crcsettings_dropdownvalue);
			compareText_InViewPage( "Number of BRI ports", numberofbriports_dropdownvalue);

			if(briportmapping_checkbox.equalsIgnoreCase("Yes")) 
			{
				if(numberofbriports_dropdownvalue.equalsIgnoreCase("1"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("2"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("3"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					GetText( "BRI Port 3 Number", APT_VoiceLineObj.APT_VoiceLine. briport3number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("4"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					GetText( "BRI Port 3 Number", APT_VoiceLineObj.APT_VoiceLine. briport3number_viewpage);
					GetText( "BRI Port 4 Number", APT_VoiceLineObj.APT_VoiceLine.briport4number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("5"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					GetText( "BRI Port 3 Number", APT_VoiceLineObj.APT_VoiceLine. briport3number_viewpage);
					GetText( "BRI Port 4 Number", APT_VoiceLineObj.APT_VoiceLine.briport4number_viewpage);
					GetText( "BRI Port 5 Number", APT_VoiceLineObj.APT_VoiceLine.briport5number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("6"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					GetText( "BRI Port 3 Number", APT_VoiceLineObj.APT_VoiceLine. briport3number_viewpage);
					GetText( "BRI Port 4 Number", APT_VoiceLineObj.APT_VoiceLine.briport4number_viewpage);
					GetText( "BRI Port 5 Number", APT_VoiceLineObj.APT_VoiceLine.briport5number_viewpage);
					GetText( "BRI Port 6 Number", APT_VoiceLineObj.APT_VoiceLine.briport6number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("7"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					GetText( "BRI Port 3 Number", APT_VoiceLineObj.APT_VoiceLine. briport3number_viewpage);
					GetText( "BRI Port 4 Number", APT_VoiceLineObj.APT_VoiceLine.briport4number_viewpage);
					GetText( "BRI Port 5 Number", APT_VoiceLineObj.APT_VoiceLine.briport5number_viewpage);
					GetText( "BRI Port 6 Number", APT_VoiceLineObj.APT_VoiceLine.briport6number_viewpage);
					GetText( "BRI Port 7 Number", APT_VoiceLineObj.APT_VoiceLine.briport7number_viewpage);
					waitforPagetobeenable();
				}
				else if(numberofbriports_dropdownvalue.equalsIgnoreCase("8"))
				{
					GetText( "BRI Port 1 Number", APT_VoiceLineObj.APT_VoiceLine.briport1number_viewpage);
					GetText( "BRI Port 2 Number", APT_VoiceLineObj.APT_VoiceLine.briport2number_viewpage);
					GetText( "BRI Port 3 Number", APT_VoiceLineObj.APT_VoiceLine. briport3number_viewpage);
					GetText( "BRI Port 4 Number", APT_VoiceLineObj.APT_VoiceLine.briport4number_viewpage);
					GetText( "BRI Port 5 Number", APT_VoiceLineObj.APT_VoiceLine.briport5number_viewpage);
					GetText( "BRI Port 6 Number", APT_VoiceLineObj.APT_VoiceLine.briport6number_viewpage);
					GetText( "BRI Port 7 Number", APT_VoiceLineObj.APT_VoiceLine.briport7number_viewpage);
					GetText( "BRI Port 8 Number", APT_VoiceLineObj.APT_VoiceLine.briport8number_viewpage);
					waitforPagetobeenable();
				}
			}
			compareText_InViewPage( "Number of PRI ports", numberofpriports_dropdownvalue);
			compareText_InViewPage( "Number of FXS ports", numberofFXSports_dropdownvalue);

			ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton);
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton,"Back");
			click(APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton,"Back");
			waitforPagetobeenable();
		}
	}

}

public void EditVoiceCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {   
	
	String edit_voiceCPE_devicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");
	String edit_voiceCPE_vendormodel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPE_VendorModel");
	String edit_voiceCPE_managementaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPE_ManagementAddress");
	String edit_voiceCPE_country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPE_Country");
	String edit_voiceCPE_existingcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_ExistingCity");
	String edit_voiceCPE_existingcityvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_ExistingCityValue");
	String edit_voiceCPE_existingsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_ExistingSite");
	String edit_voiceCPE_existingsitevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_ExistingSiteValue");
	String edit_voiceCPE_existingpremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_ExistingPremise");
	String edit_voiceCPE_existingpremisevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_ExistingPremiseValue");
	String edit_voiceCPE_newcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewCity");
	String edit_voiceCPE_cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewCityName");
	String edit_voiceCPE_Citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewCityCode");
	String edit_voiceCPE_sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewSiteName");
	String edit_voiceCPE_sitecode =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewSiteCode");
	String edit_voiceCPE_premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewPremiseName");
	String edit_voiceCPE_premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewPremiseCode"); 
	String edit_voiceCPE_newsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewSite");
	String edit_voiceCPE_NewPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_voiceCPE_NewPremise");
	String edit_cpetoprovidedialtone_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_CPEToProvideDialTone_Checkbox");
	String edit_cpelinepowerrequired_checkbox =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_CPELinePowerRequired_Checkbox");
	String edit_numberporting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_NumberPorting_Checkbox");
	String edit_briportmapping_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPortMapping_Checkbox");
	String edit_crcsettings_dropdownvalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_CRCSettings_DropdownValue");
	String edit_numberofpriports_dropdownvalue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_NumberOfPRIPorts_DropdownValue");
	String edit_numberofbriports_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_NumberOfBRIPorts_DropdownValue");
	String edit_briport1_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort1Number_value");
	String edit_briport2_value =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort2Number_value");
	String edit_briport3_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort3Number_value");
	String edit_briport4_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort4Number_value");
	String edit_briport5_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort5Number_value");
	String edit_briport6_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort6Number_value");
	String edit_briport7_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort7Number_value"); 
	String edit_briport8_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_BRIPort8Number_value");
	String edit_numberofFXSports_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_NumberOfFXSPorts_DropdownValue");
	String edit_fxsnumber1_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_FXSNumber1_value");
	String edit_fxsnumber2_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_FXSNumber2_value");

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.voiceCPEdevice_header);
	waitforPagetobeenable();

	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingvoicecpe_devicegrid).isDisplayed())
	{
		List<WebElement> AddedVoiceCPEDevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedvoiceCPEdevicename);
		//System.out.println(AddedVoiceCPEDevicesList);
		int AddedVoiceCPEDevicesCount= AddedVoiceCPEDevicesList.size();

		for(int i=0;i<AddedVoiceCPEDevicesCount;i++) {
			String AddedVoiceCPEDeviceNameText= AddedVoiceCPEDevicesList.get(i).getText();
			System.out.println(AddedVoiceCPEDeviceNameText);
			String AddedVoiceCPEDevice_SNo= AddedVoiceCPEDeviceNameText.substring(0, 1);

			//WebElement AddedVoiceCPEDevice_EditLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedvoicecpedevice_editlink).replace("value", AddedVoiceCPEDevice_SNo);
			//Clickon(AddedVoiceCPEDevice_EditLink);
			webDriver.findElement(By.xpath("//b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//a[contains(text(),'Edit')]")).click();
			
			//click(APT_VoiceLineObj.APT_VoiceLine.addedvoicecpedevice_editlink);
			waitforPagetobeenable();

			GetText( "Edit Voice CPE header", APT_VoiceLineObj.APT_VoiceLine.editvoicecpe_header);
			scrollIntoTop();
			edittextFields_commonMethod( "Name", APT_VoiceLineObj.APT_VoiceLine.voicecpe_devicename, edit_voiceCPE_devicename);

			boolean availability=false;
			try {  
				availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.vendormodel_dropdown).isDisplayed();
				if(availability) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown is displaying");
					Report.LogInfo("INFO","Vendor/Model dropdown is displaying","PASS");

					if(edit_voiceCPE_vendormodel.equalsIgnoreCase("null")) {

						ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Vendor/Model dropdown");
						Report.LogInfo("INFO"," No values selected under Vendor/Model dropdown","PASS");
					}else {

						//Clickon(findWebElement("//div[label[text()='Vendor/Model']]//div[text()='�']"));
					
						webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//div[text()='�']")).click();
						
						waitforPagetobeenable();

						//verify list of values inside dropdown
						List<WebElement> listofvalues = webDriver.findElements(By.xpath("//span[contains(@class,'css-f2o6e8-ItemComponent evc32pp0')]"));
																						

						ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Vendor/Model dropdown is:  ");
						Report.LogInfo("INFO", " List of values inside Vendor/Model dropdown is:  ","PASS");

						for (WebElement valuetypes : listofvalues) {
							Report.LogInfo("INFO","service sub types : " + valuetypes.getText(),"PASS");
							ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
							//System.out.println(" " + valuetypes.getText());
						}

						waitforPagetobeenable();
						//SendKeys(findWebElement("//div[label[text()='Vendor/Model']]//input"), edit_voiceCPE_vendormodel);	
						webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input")).sendKeys(edit_voiceCPE_vendormodel);
						
						waitforPagetobeenable();

						//Clickon(findWebElement("(//span[contains(text(),'"+ edit_voiceCPE_vendormodel +"')])[1]"));
						webDriver.findElement(By.xpath("(//span[contains(text(),'"+ edit_voiceCPE_vendormodel +"')])[1]")).click();
						waitforPagetobeenable();

						String actualValue=webDriver.findElement(By.xpath("//label[contains(text(),'Vendor/Model')]/following::span[contains(text(),'"+edit_voiceCPE_vendormodel+"')]")).getText();
						
						//label[text()='"+voiceCPE_vendormodel+"']/following-sibling::div//span
						
						ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown value selected as: "+ actualValue );
						Report.LogInfo("INFO", "Vendor/Model dropdown value selected as: "+ actualValue,"PASS");

					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
					Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
				}
			}catch(NoSuchElementException e) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
				Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
			}catch(Exception ee) {
				ee.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Vendor/Model dropdown");
				Report.LogInfo("INFO"," NO value selected under Vendor/Model dropdown","FAIL");
			}

			edittextFields_commonMethod( "Management Address/Mask [e.g. 10.0.0.0/27]", APT_VoiceLineObj.APT_VoiceLine.managementaddress_voicecpe, edit_voiceCPE_managementaddress);
			compareText_fromtextFields( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.snmpro_textfield, "incc");

			//select country
			ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.okbutton);
			waitforPagetobeenable();
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.countryinput, "Country", edit_voiceCPE_country);
			ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.managementaddress_voicecpe);
			//New City		
			if(edit_voiceCPE_existingcity.equalsIgnoreCase("no") & edit_voiceCPE_newcity.equalsIgnoreCase("yes")) {
				verifyExists( APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"add city switch");
				click( APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"add city switch");
				//City name
				edittextFields_commonMethod( "City Name", APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, edit_voiceCPE_cityname);
				//City Code	
				edittextFields_commonMethod( "City Code", APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, edit_voiceCPE_Citycode);
				//Site name
				edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, edit_voiceCPE_sitename);
				//Site Code
				edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addCityToggleSelected, edit_voiceCPE_sitecode);
				//Premise name	
				edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, edit_voiceCPE_premisename);
				//Premise Code	
				edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, edit_voiceCPE_premisecode);

			}	

			//Existing City	
			else if(edit_voiceCPE_existingcity.equalsIgnoreCase("yes") & edit_voiceCPE_newcity.equalsIgnoreCase("no")) {

				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.citydropdowninput, "City", edit_voiceCPE_existingcityvalue);

				//Existing Site
				if(edit_voiceCPE_existingsite.equalsIgnoreCase("yes") & edit_voiceCPE_newsite.equalsIgnoreCase("no")) {
					selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.sitedropdowninput, "Site", edit_voiceCPE_existingsitevalue);

					//Existing Premise
					if(edit_voiceCPE_existingpremise.equalsIgnoreCase("yes") & edit_voiceCPE_NewPremise.equalsIgnoreCase("no")) {
						selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.premisedropdowninput, "Premise", edit_voiceCPE_existingpremisevalue);
					}

					//New Premise  
					else if(edit_voiceCPE_existingpremise.equalsIgnoreCase("no") & edit_voiceCPE_NewPremise.equalsIgnoreCase("yes")) {

						//Clickon_addToggleButton( "addpremiseswitch");
						verifyExists( APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"add premise switch");
						click( APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"add premise switch");
						//Premise name	
						edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addPremiseToggleSelected, edit_voiceCPE_premisename);
						//Premise Code	
						edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addPremiseToggleSelected, edit_voiceCPE_premisecode);
					} 
				}

				else if(edit_voiceCPE_existingsite.equalsIgnoreCase("no") & edit_voiceCPE_newsite.equalsIgnoreCase("yes")) {

					//Clickon_addToggleButton( "addsiteswitch");
					verifyExists( APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"add site switch");
					click( APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"add site switch");
					//Site name
					edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addSiteToggleSelected, edit_voiceCPE_sitename);
					//Site Code
					edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addSiteToggleSelected, edit_voiceCPE_sitecode);
					//Premise name	
					edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addSiteToggleSelected, edit_voiceCPE_premisename);
					//Premise Code	
					edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addSiteToggleSelected, edit_voiceCPE_premisecode);
				}
			}

			editcheckbox_commonMethod( edit_cpetoprovidedialtone_checkbox, APT_VoiceLineObj.APT_VoiceLine.cpetoprovidedialtone_checkbox, "CPE to Provide Dial Tone");
			editcheckbox_commonMethod( edit_cpelinepowerrequired_checkbox, APT_VoiceLineObj.APT_VoiceLine.cpelinepowerrequired_checkbox, "CPE Line Power Required");
			editcheckbox_commonMethod( edit_numberporting_checkbox, APT_VoiceLineObj.APT_VoiceLine.numberporting_checkbox, "Number Porting");
			editcheckbox_commonMethod( edit_briportmapping_checkbox, APT_VoiceLineObj.APT_VoiceLine.briportmapping_checkbox, "BRI Port Mapping");
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.crcsettings_dropdown, "CRC Settings", edit_crcsettings_dropdownvalue);
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.numberofpriports_dropdown, "Number of PRI Ports", edit_numberofpriports_dropdownvalue);
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.numberofbriports_dropdown, "Number of BRI Ports", edit_numberofbriports_dropdownvalue);
			ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.briportmapping_checkbox);
			if(edit_briportmapping_checkbox.equalsIgnoreCase("Yes")) 
			{
				numberofBRIPorts( edit_numberofbriports_dropdownvalue, edit_briport1_value, edit_briport2_value, edit_briport3_value, edit_briport4_value, edit_briport5_value, edit_briport6_value, edit_briport7_value, edit_briport8_value);
			}
			waitforPagetobeenable();
			ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.okbutton);
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.numberoffxsports_dropdown, "Number of FXS Ports", edit_numberofFXSports_dropdownvalue);
			NumberofFXSPorts( edit_numberofFXSports_dropdownvalue, edit_fxsnumber1_value, edit_fxsnumber2_value);
			ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.okbutton);
			waitforPagetobeenable();
			verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
			click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
			waitforPagetobeenable();
			waitforPagetobeenable();
			verifysuccessmessage( "Device updated successfully");
		}
	}

}


public void clickOnCPEdeviceLink(String siteOrderName) throws Exception {

	
	waitforPagetobeenable();
	ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	//WebElement getTrunkRow=findWebElement(APT_VoiceLineObj.APT_VoiceLine.selectCreatedTrunk_InViewServicePage).replace("value", Edit_primarytrunkGroupname);
	//safeJavaScriptClick(getTrunkRow);
	
	//click(APT_VoiceLineObj.APT_VoiceLine.selectCreatedTrunk_InViewServicePage1+Edit_primarytrunkGroupname+APT_VoiceLineObj.APT_VoiceLine.selectCreatedTrunk_InViewServicePage2);


	//click on Action dropdown under Trunk Panel	
	//WebElement trunkPanel_actionDropdown=findWebElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown).replace("value", siteOrderName);
	//safeJavaScriptClick(trunkPanel_actionDropdown);
	//click(APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown+siteOrderName+APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown);

	//Add CPE link
	//click_commonMethod(application, "Add CPE device Link", "addCPEdeviceLink" , xml);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.addCPEdeviceLink,"Add CPE device Link");
	click(APT_VoiceLineObj.APT_VoiceLine.addCPEdeviceLink,"Add CPE device Link");

}

public void addCPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {   
	
	
	String RouterId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_routerID");
	String VendorModel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_vendorModel");
	String ManagamentAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_managementAddress");
	String Snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_Snmpro");
	String Snmprw=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_Snmprw");
	String SNMPv3ContaxtName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3Contextname");
	String SNMPv3ContextEngineId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3ContextEngineId");
	String SNMPv3securityUsername=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3securityUsername");
	String SNMPv3AutoProto =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3AutoProto");
	String SNMPv3AuthPasswrd=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3AuthPasswrd");
	String Country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
	String existingcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
	String existingcityvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
	String existingsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
	String existingsitevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_SiteValue");
	String existingpremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
	String existingpremisevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_PremiseValue");
	String newcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
	String cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
	String Citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
	String sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
	String sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
	String premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
	String premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
	String newsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
	String NewPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
	
	waitForAjax();
	waitforPagetobeenable();
	scrollIntoTop();
	//webDriver.findElement(By.xpath("(//a[text()='Add CPE Device'])[1]")).click();
	
	//addtextFields_commonMethod( "Router ID" , "routerId"  , RouterId);   //Router ID
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.routerId,"Router ID");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.routerId,RouterId,"Router ID");

	//addDropdownValues_commonMethod( "Vendor/Model" , "CPE_vendorModel" , VendorModel);   //Vendor Model

	boolean availability=false;
	try {  
		availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.CPE_vendorModel).isDisplayed();
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown is displaying");
			Report.LogInfo("INFO","Vendor/Model dropdown is displaying","PASS");

			if(VendorModel.equalsIgnoreCase("null")) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Vendor/Model dropdown");
				Report.LogInfo("INFO"," No values selected under Vendor/Model dropdown","PASS");
			}else {

				//Clickon(findWebElement("//div[label[text()='Vendor/Model']]//div[text()='�']"));
				webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//div[text()='�']")).click();
				waitforPagetobeenable();

				//verify list of values inside dropdown
				List<WebElement> listofvalues = webDriver.findElements(By.xpath("//span[contains(@class,'css-9rgonp-DropDown e1qjn9k90')]"));
					//div[@class='sc-ifAKCX oLlzc']						
				ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Vendor/Model dropdown is:  ");
				Report.LogInfo("INFO"," List of values inside Vendor/Model dropdown is:  ","PASS");

				for (WebElement valuetypes : listofvalues) {
					Report.LogInfo("INFO","service sub types : " + valuetypes.getText(),"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
					//System.out.println(" " + valuetypes.getText());
				}

				waitforPagetobeenable();
				//SendKeys(findWebElement("//div[label[text()='Vendor/Model']]//input"), VendorModel);
				webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input")).sendKeys(VendorModel);
				
				waitforPagetobeenable();

			//	Clickon(findWebElement("(//span[contains(text(),'"+ VendorModel +"')])[1]"));
				webDriver.findElement(By.xpath("(//span[contains(text(),'"+ VendorModel +"')])[1]")).click();
				waitforPagetobeenable();

				String actualValue=webDriver.findElement(By.xpath("//label[text()='Vendor/Model']/following-sibling::div//span")).getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown value selected as: "+ actualValue );
				Report.LogInfo("INFO","Vendor/Model dropdown value selected as: "+ actualValue,"PASS");

			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
			Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
		Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Vendor/Model dropdown");
		Report.LogInfo("INFO"," NO value selected under Vendor/Model dropdown","FAIL");
	}

	addtextFields_commonMethod( "Management Address",APT_VoiceLineObj.APT_VoiceLine.CPE_manageAddress, ManagamentAddress);   //Management Address

	addtextFields_commonMethod( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.CPE_snmpro , Snmpro );   //Snmpro

	addtextFields_commonMethod( "Snmprw" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmprw , Snmprw);   //Snmprw

	addtextFields_commonMethod( "Snmp V3 Context Name", APT_VoiceLineObj.APT_VoiceLine.CPE_snmpV3ContextName , SNMPv3ContaxtName);   //Snmp V3 Context Name

	addtextFields_commonMethod( "Snmp V3 Context Engine ID" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmpV3ContextEngineID, SNMPv3ContextEngineId);   //Snmp V3 Context Engine ID

	addtextFields_commonMethod( "Snmp V3 Security Username" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmpv3SecurityUserName, SNMPv3securityUsername);   //Snmp V3 Security Username

	addDropdownValues_commonMethod( "Snmp V3 Auth Proto" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmpv3AuthProto , SNMPv3AutoProto);   ////Snmp V3 Auth Proto

	addtextFields_commonMethod( "Snmp V3 Auth Password",APT_VoiceLineObj.APT_VoiceLine.CPE_snmpv3AuthPasswrd , SNMPv3AuthPasswrd);   //Snmp V3 Auth Password


	//select country
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton);
	waitforPagetobeenable();

	//addDropdownValues_commonMethod( "Country", "countrydropdown", Country);
	selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.countrydropdown, "Country", Country);

	//New City		
	if(existingcity.equalsIgnoreCase("no") & newcity.equalsIgnoreCase("yes")) {
		click(APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"addcityswitch");
		//City name
		addtextFields_commonMethod( "City Name", APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, cityname);
		//City Code	
		addtextFields_commonMethod( "City Code", APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, Citycode);
		//Site name
		addtextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, sitename);
		//Site Code
		addtextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addCityToggleSelected, sitecode);
		//Premise name	
		addtextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, premisename);
		//Premise Code	
		addtextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, premisecode);

	}	

	//Existing City	
	else if(existingcity.equalsIgnoreCase("yes") & newcity.equalsIgnoreCase("no")) {

		//addDropdownValues_commonMethod( "City", "citydropdown", existingcityvalue);
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.citydropdown, "City", existingcityvalue);

		//Existing Site
		if(existingsite.equalsIgnoreCase("yes") & newsite.equalsIgnoreCase("no")) {
			//addDropdownValues_commonMethod( "Site", "sitedropdown", existingsitevalue);
			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.sitedropdown, "Site", existingsitevalue);

			//Existing Premise
			if(existingpremise.equalsIgnoreCase("yes") & NewPremise.equalsIgnoreCase("no")) {
				//addDropdownValues_commonMethod( "Premise", "premisedropdown", existingpremisevalue);
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.premisedropdown, "Premise", existingpremisevalue);
			}

			//New Premise  
			else if(existingpremise.equalsIgnoreCase("no") & NewPremise.equalsIgnoreCase("yes")) {

				click(APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"addpremiseswitch");
				//Premise name	
				addtextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addPremiseToggleSelected, premisename);
				//Premise Code	
				addtextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addPremiseToggleSelected, premisecode);
			} 
		}

		else if(existingsite.equalsIgnoreCase("no") & newsite.equalsIgnoreCase("yes")) {

			click(APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"addsiteswitch");
			//Site name
			addtextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addSiteToggleSelected, sitename);
			//Site Code
			addtextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addSiteToggleSelected, sitecode);
			//Premise name	
			addtextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addSiteToggleSelected, premisename);
			//Premise Code	
			addtextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addSiteToggleSelected, premisecode);
		}
	}

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton);
	waitforPagetobeenable();

	clickOnBankPage();
	waitforPagetobeenable();

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton);
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK" );
	click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK" );
	waitforPagetobeenable();
	waitforPagetobeenable();
}

public void addtextFields_commonMethod(String labelname, String xpathname, String expectedValueToAdd)
		throws InterruptedException, IOException {
	boolean availability=false;
try {
	

	//availability=getwebelementNoWait(xpathname).isDisplayed();
	availability=verifyExists(xpathname,labelname);
	if(availability) {
		ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " text field is displaying");
		Report.LogInfo("INFO",labelname + " text field is displaying","PASS");
		
		if(expectedValueToAdd.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "No values added to text field "+labelname);
			Report.LogInfo("INFO","No values added to text field "+labelname,"PASS");
		}else {
			
			sendKeys(xpathname, expectedValueToAdd,labelname);
			//String actualvalue=getwebelementNoWait(xml.getlocator("//locators/" + application + "/"+ xpathname +"")).getAttribute("value");
			String actualvalue=getTextFrom(xpathname);
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " text field value added as: "+ actualvalue);
			Report.LogInfo("INFO",labelname + " text field value added as: "+ actualvalue,"PASS");
		}
		
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " text field is not displaying");
		Report.LogInfo("INFO",labelname + " text field is not displaying","FAIL");
	}
}catch(NoSuchElementException e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " text field is not displaying");
	Report.LogInfo("INFO",labelname + " text field is not displaying","FAIL");
}catch(Exception ee) {
	ee.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to add value to "+ labelname + " text field");
	Report.LogInfo("INFO"," Not able to add value to "+ labelname + " text field","FAIL");
}
}

public void CPEdevice_clickOnViewLink() throws InterruptedException, IOException {

	
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existing_CPE_devicegrid).isDisplayed())
	{
		List<WebElement> AddedCPEDevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedCPEdevicename);
		//System.out.println(AddedCPEDevicesList);
		int AddedCPEDevicesCount= AddedCPEDevicesList.size();

		for(int i=0;i<AddedCPEDevicesCount;i++) {
			String AddedCPEDeviceNameText= AddedCPEDevicesList.get(i).getText();
			//System.out.println(AddedCPEDeviceNameText);
			String AddedCPEDevice_SNo= AddedCPEDeviceNameText.substring(0, 1);

			//WebElement AddedCPEDevice_ViewLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.viewCPEdeviceLink).replace("value", AddedCPEDevice_SNo);
			//Clickon(AddedCPEDevice_ViewLink);
			//click(APT_VoiceLineObj.APT_VoiceLine.viewCPEdeviceLink1+AddedCPEDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.viewCPEdeviceLink2);
			click(APT_VoiceLineObj.APT_VoiceLine.viewCPEdeviceLink,"View CPE Device");
			
		}
	}
}

public void viewCPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {   
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String RouterId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_routerID");
	String VendorModel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_vendorModel");
	String ManagamentAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_managementAddress");
	String Snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_Snmpro");
	String Snmprw=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_Snmprw");
	String SNMPv3ContextName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3Contextname");
	String SNMPv3ContextEngineId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3ContextEngineId");
	String SNMPv3securityUsername=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3securityUsername");
	String SNMPv3AutoProto=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3AutoProto");
	String SNMPv3AuthPasswrd=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEdevice_SNMPv3AuthPasswrd");
	String Country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
	String existingcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
	String existingcityvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
	String existingsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
	String existingsitevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_SiteValue");
	String existingpremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
	String existingpremisevalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_PremiseValue");
	String newcity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
	String cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
	String Citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
	String sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
	String sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
	String premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
	String premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
	String newsite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
	String NewPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");

	waitforPagetobeenable();

	scrollIntoTop();
	compareText_InViewPage( "Router Id" , RouterId);   //Router Id

	compareText_InViewPage( "Vendor/Model" , VendorModel );   //vendor/model

	compareText_InViewPage( "Management Address", ManagamentAddress);   //Management Address

	compareText_InViewPage( "Snmpro", Snmpro);   //Snmpro

	compareText_InViewPage( "Snmprw" , Snmprw);   //Snmprw

	compareText_InViewPage( "Snmp V3 Context Name" , SNMPv3ContextName);   //SNMP V3 Context Name

	compareText_InViewPage( "Snmp V3 Context Engine ID" , SNMPv3ContextEngineId);   //Snmp V3 Context Engine ID

	compareText_InViewPage( "Snmp V3 Security Username" , SNMPv3securityUsername);   //Snmp V3 Security Username

	compareText_InViewPage1( "Snmp V3 Auth Proto" , SNMPv3AutoProto);   //SNMP v3 Auto Proto

	compareText_InViewPage1( "Snmp V3 Auth Password", SNMPv3AuthPasswrd);

	//Country
	GetText( "Country", APT_VoiceLineObj.APT_VoiceLine.viewCPE_CountryValue);


	//City
	if((existingcity.equalsIgnoreCase("yes")) && (newcity.equalsIgnoreCase("NO"))) {

		//Existing City
		GetText( "City", APT_VoiceLineObj.APT_VoiceLine.viewCPE_CityValue);

	}
	else if((existingcity.equalsIgnoreCase("NO")) && (newcity.equalsIgnoreCase("Yes"))) {

		//new City
		GetText( "City", APT_VoiceLineObj.APT_VoiceLine.viewCPE_CityValue);

	}

	//Site
	if((existingsite.equalsIgnoreCase("yes")) && (newsite.equalsIgnoreCase("NO"))) {

		//Existing Site
		GetText( "Site", APT_VoiceLineObj.APT_VoiceLine.viewCPE_SiteValue);
	}
	else if((existingsite.equalsIgnoreCase("No")) && (newsite.equalsIgnoreCase("Yes"))) {

		//New Site
		GetText( "Site",APT_VoiceLineObj.APT_VoiceLine.viewCPE_SiteValue);

	}

	//Premise
	if((existingpremise.equalsIgnoreCase("yes")) && (NewPremise.equalsIgnoreCase("NO"))) {

		//Existing premise
		compareText_InViewPage( "Premise", existingpremisevalue);

	}
	else if((existingpremise.equalsIgnoreCase("No")) && (NewPremise.equalsIgnoreCase("Yes"))) {

		//new premise
		compareText_InViewPage( "Premise", premisename);
	}

	waitforPagetobeenable();
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO", "Clicked on BreadCrump", "INFO");
}


public void CPEdevice_clickOnEditLink() throws InterruptedException, IOException{

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existing_CPE_devicegrid).isDisplayed())
	{
		List<WebElement> AddedCPEDevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedCPEdevicename);
		//System.out.println(AddedCPEDevicesList);
		int AddedCPEDevicesCount= AddedCPEDevicesList.size();

		for(int i=0;i<AddedCPEDevicesCount;i++) {
			String AddedCPEDeviceNameText= AddedCPEDevicesList.get(i).getText();
			//System.out.println(AddedCPEDeviceNameText);
			String AddedCPEDevice_SNo= AddedCPEDeviceNameText.substring(0, 1);

			//WebElement AddedCPEDevice_EditLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.editCPEdeviceLink).replace("value", AddedCPEDevice_SNo);
			//Clickon(AddedCPEDevice_EditLink);
			//click(APT_VoiceLineObj.APT_VoiceLine.editCPEdeviceLink1+AddedCPEDevice_SNo+APT_VoiceLineObj.APT_VoiceLine.editCPEdeviceLink2);
			click(APT_VoiceLineObj.APT_VoiceLine.editCPEdeviceLink,"Click Edit CPEDevice");
		}
	}
}


public void editCPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification"); 
	String editRouterId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_routerID");
	String editVendorModel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_vendorModel");
	String editManagamentAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_managementAddress");
	String editSnmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_Snmpro");
	String editSnmprw=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_Snmprw");
	String editSNMPv3ContaxtName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_SNMPv3Contextname");
	String editSNMPv3ContextEngineId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_SNMPv3ContextEngineId");
	String editSNMPv3securityUsername =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_SNMPv3securityUsername");
	String editSNMPv3AutoProto=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_SNMPv3AuthProto");
	String editSNMPv3AuthPasswrd=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCPEdevice_SNMPv3AuthPasswrd");
	String editCountry=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
	String editExistingCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
	String editExistingCityValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
	String editExistingSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
	String editExistingSiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
	String editExistingPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
	String editExistingPremiseValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
	String editNewCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
	String editNewSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
	String editNewPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
	String editNewCityName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
	String editNewCityCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
	String editNewSiteName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
	String editNewSiteCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
	String editNewPremiseName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
	String editNewPremiseCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");


	waitforPagetobeenable();
	waitForAjax();
	scrollIntoTop();
	edittextFields_commonMethod( "Router ID" , APT_VoiceLineObj.APT_VoiceLine.routerId  , editRouterId);   //Router ID

	//addDropdownValues_commonMethod( "Vendor/Model" , "CPE_vendorModel" , editVendorModel);   //Vendor Model

	boolean availability=false;
	try {  
		availability=findWebElement(APT_VoiceLineObj.APT_VoiceLine.CPE_vendorModel).isDisplayed();
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown is displaying");
			Report.LogInfo("INFO","Vendor/Model dropdown is displaying","PASS");

			if(editVendorModel.equalsIgnoreCase("null")) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Vendor/Model dropdown");
				Report.LogInfo("INFO"," No values selected under Vendor/Model dropdown","PASS");
			}else {
				
				//Clickon(findWebElement("//div[label[text()='Vendor/Model']]//div[text()='�']"));
				webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//div[text()='�']")).click();
				waitforPagetobeenable();

				//verify list of values inside dropdown
				List<WebElement> listofvalues = webDriver.findElements(By.xpath("//span[contains(@class,'css-9rgonp-DropDown e1qjn9k90')]"));
					//div[@class='sc-ifAKCX oLlzc']						
				ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Vendor/Model dropdown is:  ");
				Report.LogInfo("INFO"," List of values inside Vendor/Model dropdown is:  ","PASS");

				for (WebElement valuetypes : listofvalues) {
					Report.LogInfo("INFO","service sub types : " + valuetypes.getText(),"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
					//System.out.println(" " + valuetypes.getText());
				}

				waitforPagetobeenable();
				//SendKeys(findWebElement("//div[label[text()='Vendor/Model']]//input"), VendorModel);
				webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input")).sendKeys(editVendorModel);
				
				waitforPagetobeenable();

			//	Clickon(findWebElement("(//span[contains(text(),'"+ VendorModel +"')])[1]"));
				webDriver.findElement(By.xpath("(//span[contains(text(),'"+ editVendorModel +"')])[1]")).click();
				waitforPagetobeenable();

				String actualValue=webDriver.findElement(By.xpath("//label[text()='Vendor/Model']/following-sibling::div//span")).getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Vendor/Model dropdown value selected as: "+ actualValue );
				Report.LogInfo("INFO","Vendor/Model dropdown value selected as: "+ actualValue,"PASS");

			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
			Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Vendor/Model is not displaying");
		Report.LogInfo("INFO","Vendor/Model is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Vendor/Model dropdown");
		Report.LogInfo("INFO"," NO value selected under Vendor/Model dropdown","FAIL");
	}

	edittextFields_commonMethod( "Management Address", APT_VoiceLineObj.APT_VoiceLine.CPE_manageAddress, editManagamentAddress);   //Management Address

	edittextFields_commonMethod( "Snmpro", APT_VoiceLineObj.APT_VoiceLine.CPE_snmpro , editSnmpro );   //Snmpro

	edittextFields_commonMethod( "Snmprw" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmprw , editSnmprw);   //Snmprw

	edittextFields_commonMethod( "Snmp V3 Context Name", APT_VoiceLineObj.APT_VoiceLine.CPE_snmpV3ContextName , editSNMPv3ContaxtName);   //Snmp V3 Context Name

	edittextFields_commonMethod( "Snmp V3 Context Engine ID" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmpV3ContextEngineID, editSNMPv3ContextEngineId);   //Snmp V3 Context Engine ID

	edittextFields_commonMethod( "Snmp V3 Security Username" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmpv3SecurityUserName, editSNMPv3securityUsername);   //Snmp V3 Security Username

	addDropdownValues_commonMethod( "Snmp V3 Auth Proto" , APT_VoiceLineObj.APT_VoiceLine.CPE_snmpv3AuthProto , editSNMPv3AutoProto);   ////Snmp V3 Auth Proto

	edittextFields_commonMethod( "Snmp V3 Auth Password", APT_VoiceLineObj.APT_VoiceLine.CPE_snmpv3AuthPasswrd , editSNMPv3AuthPasswrd);   //Snmp V3 Auth Password

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton);
	waitforPagetobeenable();

	//Country
	if(!editCountry.equalsIgnoreCase("Null")) {

		//addDropdownValues_commonMethod( "Country", "countrydropdown", editCountry);
		selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.countrydropdown, "Country", editCountry);

		//New City		
		if(editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
			click(APT_VoiceLineObj.APT_VoiceLine.addcityswitch,"addcityswitch");
			//City name
			edittextFields_commonMethod( "City Name", APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, editNewCityName);
			//City Code	
			edittextFields_commonMethod( "City Code", APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, editNewCityCode);
			//Site name
			edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, editNewSiteName);
			//Site Code
			edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addCityToggleSelected, editNewSiteCode);
			//Premise name
			edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, editNewPremiseName);
			//Premise Code	
			edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode);

		}	

		//Existing City	
		else if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

			selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.citydropdown, "City", editExistingCityValue);

			//Existing Site
			if(editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
				selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.sitedropdown, "Site", editExistingSiteValue);

				//Existing Premise
				if(editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
					selectValueInsideDropdown( APT_VoiceLineObj.APT_VoiceLine.premisedropdown, "Premise", editExistingPremiseValue);
				}

				//New Premise  
				else if(editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {

					click(APT_VoiceLineObj.APT_VoiceLine.addpremiseswitch,"addpremiseswitch");
					//Premise name	
					edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName);
					//Premise Code	
					edittextFields_commonMethod( "Premise Code",APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode);
				} 
			}

			else if(editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {

				click(APT_VoiceLineObj.APT_VoiceLine.addsiteswitch,"addsiteswitch");
				//Site name
				edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addSiteToggleSelected, editNewSiteName);
				//Site Code
				edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addSiteToggleSelected, editNewSiteCode);
				//Premise name	
				edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addSiteToggleSelected, editNewPremiseName);
				//Premise Code	
				edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addSiteToggleSelected, editNewPremiseCode);
			}
		}

	}
	else if(editCountry.equalsIgnoreCase("Null")) {

		ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made for 'Country' dropdown");
		Report.LogInfo("INFO"," No changes made for 'Country' dropdown","PASS");

		//City	
		editCity(editExistingCity, editNewCity, "citydropdowninput", "selectcityswitch", "addcityswitch",
				editExistingCityValue, editNewCityName, editNewCityCode, "City");


		//Site	
		editSite(editExistingSite, editNewSite, "sitedropdowninput", "selectsiteswitch",
				"addsiteswitch", editExistingSiteValue , editNewSiteName, editNewSiteCode, "Site");

		//Premise
		editPremise( editExistingPremise, editNewPremise, "premisedropdowninput", "selectpremiseswitch",
				"addpremiseswitch", editExistingPremiseValue, editNewPremiseName, editNewPremiseCode, "Premise");

	}


	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton);
	waitforPagetobeenable();

	clickOnBankPage();

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton);
	

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.trunk_okButton,"OK");
	
	waitforPagetobeenable();
	scrollIntoTop();
	verifysuccessmessage( "CPE Device updated successfully");
	
	//clickOnBreadCrump( sid);
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
	waitforPagetobeenable();
}


public void editCity(String editExistingCity, String editNewCity, String dropdown_xpath, String selectCityToggleButton,
		String addCityToggleButton, String dropdownValue, String editNewCityName, String editNewCityCode, String labelname) throws InterruptedException,  IOException {

	if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

		existingCity( dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {

		existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {

		newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {

		newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {

		ExtentTestManager.getTest().log(LogStatus.PASS, "No chnges made under 'City' field");
		Report.LogInfo("INFO","No chnges made under 'City' field","PASS");
	}

}


public void editSite(String editExistingCity, String editNewCity, String dropdown_xpath, String selectSiteToggleButton,
		String addSitetoggleButton, String dropdownValue, String editNewSiteName, String editNewSiteCode, String labelname) throws InterruptedException, IOException {

	if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

		existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {

		existingSite( dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {

		newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {

		newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);

	}

	else if(editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {

		ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made under 'Site' field");
		Report.LogInfo("INFO","No changes made under 'Site' field","PASS");

	}

}

public void editPremise(String editExistingPremise, String editNewPremise, String dropdown_xpath, String selectPremiseToggleButton,
		String addPremisetoggleButton, String dropdownValue, String editNewPremiseName, String editNewPremiseCode, String labelname) throws InterruptedException, IOException {

	if(editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {

		existingPremise( dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);

	}

	else if(editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("null")) {

		existingPremise( dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);

	}

	else if(editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("Yes")) {

		newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode, labelname);

	}

	else if(editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("Yes")) {

		newPremise( dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode, labelname);

	}

	else if(editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("null")) {

		ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made under 'Premise' field");
		Report.LogInfo("INFO","No changes made under 'Premise' field","PASS");

	}

}

public void existingPremise(String dropdown_xpath, String dropdownValue, String selectPremiseToggleButton, String labelname) throws InterruptedException, IOException {

	boolean premiseDisplayed=false;
	try {	
		premiseDisplayed=findWebElement(dropdown_xpath).isDisplayed();

		if(premiseDisplayed) {

			addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);

		}else {

			//click_commonMethod("Select Premise toggle button", selectPremiseToggleButton);
			verifyExists(selectPremiseToggleButton,"Select Premise toggle button");
			click(selectPremiseToggleButton,"Select Premise toggle button");
			waitforPagetobeenable();

			addDropdownValues_commonMethod(labelname, dropdown_xpath, dropdownValue);

		}
	}catch(Exception e) {
		e.printStackTrace();

		//click_commonMethod("Select Premise toggle button", selectPremiseToggleButton);
		verifyExists(selectPremiseToggleButton,"Select Premise toggle button");
		click(selectPremiseToggleButton,"Select Premise toggle button");

		addDropdownValues_commonMethod(labelname, dropdown_xpath, dropdownValue);
	}

}

public void newPremise(String dropdown_xpath, String addPremisetoggleButton, String editNewPremiseName, String editNewPremiseCode, String labelname) throws InterruptedException,  IOException {

	boolean premiseDisplayed=false;
	try {	
		premiseDisplayed=findWebElement(dropdown_xpath ).isDisplayed();

		if(premiseDisplayed) {

			//click_commonMethod( "Select Premise toggle button", addPremisetoggleButton);
			verifyExists(addPremisetoggleButton,"Select Premise toggle button");
			click(addPremisetoggleButton,"Select Premise toggle button");
			waitforPagetobeenable();

			//Premise name
			edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName);

			//Premise Code	
			edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode);

		}else {

			//Premise name
			edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, editNewPremiseName);

			//Premise Code	
			edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode);

		}
	}catch(Exception e) {
		e.printStackTrace();

		//Premise name
		edittextFields_commonMethod( "Premise Name", APT_VoiceLineObj.APT_VoiceLine.premisenameinputfield_addCityToggleSelected, editNewPremiseName);

		//Premise Code	
		edittextFields_commonMethod( "Premise Code", APT_VoiceLineObj.APT_VoiceLine.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode);

	}

}


public void existingSite(String dropdown_xpath, String dropdownValue, String selectSiteToggleButton, String labelname) throws InterruptedException, IOException {

	boolean siteDisplayed=false;
	try {	
		siteDisplayed=findWebElement(dropdown_xpath).isDisplayed();

		if(siteDisplayed) {

			addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);

		}else {

			verifyExists(selectSiteToggleButton,"Select Site toggle button");
			click(selectSiteToggleButton,"Select Site toggle button");
			waitforPagetobeenable();

			addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);

		}
	}catch(Exception e) {
		e.printStackTrace();

		//click_commonMethod( "Select Site toggle button", selectSiteToggleButton);
		verifyExists(selectSiteToggleButton,"Select Site toggle button");
		click(selectSiteToggleButton,"Select Site toggle button");

		addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);
	}

}


public void newSite(String dropdown_xpath, String addSitetoggleButton, String editNewSiteName,String editNewSiteCode, String labelname) throws InterruptedException,  IOException {

	boolean cityDisplayed=false;
	try {	
		cityDisplayed=findWebElement(dropdown_xpath).isDisplayed();

		if(cityDisplayed) {

			//click_commonMethod( "Select City toggle button", addSitetoggleButton);
			verifyExists(addSitetoggleButton,"Select Site toggle button");
			click(addSitetoggleButton,"Select Site toggle button");
			waitforPagetobeenable();

			//Site name
			edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addSiteToggleSelected, editNewSiteName);

			//Site Code	
			edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addSiteToggleSelected, editNewSiteCode);

		}else {

			//Site name
			edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, editNewSiteName);

			//Site Code	
			edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, editNewSiteCode);

		}
	}catch(Exception e) {
		e.printStackTrace();

		//Site name
		edittextFields_commonMethod( "Site Name", APT_VoiceLineObj.APT_VoiceLine.sitenameinputfield_addCityToggleSelected, editNewSiteName);

		//Site Code	
		edittextFields_commonMethod( "Site Code", APT_VoiceLineObj.APT_VoiceLine.sitecodeinputfield_addCityToggleSelected, editNewSiteCode);

	}

}

public void existingCity(String dropdown_xpath, String dropdownValue, String selectCityToggleButton, String labelname) throws InterruptedException, IOException {

	boolean cityDisplayed=false;
	try {	
		cityDisplayed=findWebElement(dropdown_xpath).isDisplayed();

		if(cityDisplayed) {

			addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);

		}else {

			//click_commonMethod( "Select City toggle button", selectCityToggleButton);
			verifyExists(selectCityToggleButton,"Select City toggle button");
			click(selectCityToggleButton,"Select City toggle button");
			waitforPagetobeenable();

			addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);

		}
	}catch(Exception e) {
		e.printStackTrace();

		//click_commonMethod( "Select City toggle button", selectCityToggleButton);
		verifyExists(selectCityToggleButton,"Select City toggle button");
		click(selectCityToggleButton,"Select City toggle button");
		
		addDropdownValues_commonMethod( labelname, dropdown_xpath, dropdownValue);
	}

}


public void newCity(String dropdown_xpath, String addCitytoggleButton, String editNewCityName,
		String editNewCityCode, String labelname) throws InterruptedException,  IOException {

	boolean cityDisplayed=false;
	try {	
		cityDisplayed=findWebElement(dropdown_xpath).isDisplayed();

		if(cityDisplayed) {

			//click_commonMethod( "Select City toggle button", addCitytoggleButton);
			verifyExists(addCitytoggleButton,"Add City toggle button");
			click(addCitytoggleButton,"Add City toggle button");
			
			waitforPagetobeenable();

			//City name
			edittextFields_commonMethod( "City Name", APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, editNewCityName);

			//City Code	
			edittextFields_commonMethod( "City Code", APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, editNewCityCode);

		}else {

			//City name
			edittextFields_commonMethod( "City Name", APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, editNewCityName);

			//City Code	
			edittextFields_commonMethod( "City Code", APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, editNewCityCode);
		}
	}catch(Exception e) {
		e.printStackTrace();

		//City name
		edittextFields_commonMethod( "City Name", APT_VoiceLineObj.APT_VoiceLine.citynameinputfield, editNewCityName);

		//City Code	
		edittextFields_commonMethod( "City Code", APT_VoiceLineObj.APT_VoiceLine.citycodeinputfield, editNewCityCode);

	}

}

public void clickOnViewTrunkLink(String siteOrderName) throws Exception {

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	//WebElement getTrunkRow=findWebElement(APT_VoiceLineObj.APT_VoiceLine.selectCreatedTrunk_InViewServicePage).replace("value", Edit_primarytrunkGroupname);
	//safeJavaScriptClick(getTrunkRow);
	//click(APT_VoiceLineObj.APT_VoiceLine.selectCreatedTrunk_InViewServicePage1+Edit_primarytrunkGroupname+APT_VoiceLineObj.APT_VoiceLine.selectCreatedTrunk_InViewServicePage2);
	
	//webDriver.findElement(By.xpath("//div[div[span[b[text()='Trunks ']]]]//following-sibling::div//td[text()='ATFTASLAB90000873800']//following-sibling::td//a[text()='View']")).click();
	
	//WebElement trunkPanel_actionDropdown=findWebElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown).replace("value", siteOrderName);
	//safeJavaScriptClick(trunkPanel_actionDropdown);
	//click(APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown1+siteOrderName+APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown2);
	
	//view trunk link
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewtrunk_link_AddDDI,"View Trunk");
	click(APT_VoiceLineObj.APT_VoiceLine.viewtrunk_link_AddDDI,"View Trunk");
	waitforPagetobeenable();

}

public void verifyAddDRPlans(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException {
	
	String Add_DRplanA=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanA"); 
	String Add_DRplanB=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanB"); 
	String Add_DRplanC=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanC"); 
	String Add_DRplanD=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanD"); 
	String Add_DRplanE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanE"); 
	String rangestart_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangestart_cc"); 
	String rangestart_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangestart_lac"); 
	String rangestart_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangestart_num"); 
	String rangefinish_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangefinish_cc"); 
	String rangefinish_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangefinish_lac"); 
	String rangefinish_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangefinish_num"); 
	String destinationnumber_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"destinationnumber_cc"); 
	String destinationnumber_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"destinationnumber_lac"); 
	String destinationnumber_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"destinationnumber_num"); 
	String activate_deactivateDRplan_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"activate_deactivateDRplan_dropdownvalue"); 
	String edit_rangestart_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangestart_cc"); 
	String edit_rangestart_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangestart_lac"); 
	String edit_rangestart_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangestart_num"); 
	String edit_rangefinish_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangefinish_cc"); 
	String edit_rangefinish_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangefinish_lac"); 
	String edit_rangefinish_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangefinish_num"); 
	String edit_destinationnumber_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_destinationnumber_cc"); 
	String edit_destinationnumber_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_destinationnumber_lac");  
	String edit_destinationnumber_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_destinationnumber_num");  
	String edit_activate_deactivateDRplan_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_activate_deactivateDRplan_dropdownvalue"); 

	waitforPagetobeenable();
	waitForAjax();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_usageofincomingrouting);
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.addDRplans_link,"Add DR Plan");
	click(APT_VoiceLineObj.APT_VoiceLine.addDRplans_link,"Add DR Plan");
	waitforPagetobeenable();
	waitforPagetobeenable();
	compareText("Add DR Plans Header",APT_VoiceLineObj.APT_VoiceLine.addDRplan_header, "Disaster Recovery Plans");

	if(Add_DRplanA.equalsIgnoreCase("Yes")) {
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanA_header);
		waitforPagetobeenable();
		compareText_fromActualvalue( "DR Plan A_header", APT_VoiceLineObj.APT_VoiceLine.DRplanA_header, "DR Plan A");
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_A_addlink,"Add");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_A_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.DRplanA_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.DRplanA_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.DRplanA_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.DRplanA_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanA_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_A_editlink,"Edit");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_A_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanA_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_A_Deletelink,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_A_Deletelink,"Delete");
		
		deleteDRPlan();
		waitforPagetobeenable();
		waitforPagetobeenable();

	}
	if(Add_DRplanB.equalsIgnoreCase("Yes")) {
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanB_header);
		waitforPagetobeenable();
		compareText_fromActualvalue( "DR Plan B_header", APT_VoiceLineObj.APT_VoiceLine.DRplanB_header, "DR Plan B");
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_B_addlink,"Add");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_B_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.DRplanB_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.DRplanB_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.DRplanB_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.DRplanB_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanB_header);
		
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_B_editlink,"Edit");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_B_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanB_header);
		
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_B_Deletelink,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_B_Deletelink,"Delete");
		
		deleteDRPlan();
		waitforPagetobeenable();
		waitforPagetobeenable();
	}
	if(Add_DRplanC.equalsIgnoreCase("Yes")) {
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanC_header);
		waitforPagetobeenable();
		compareText_fromActualvalue( "DR Plan C_header", APT_VoiceLineObj.APT_VoiceLine.DRplanC_header, "DR Plan C");
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_C_addlink,"Add");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_C_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.DRplanC_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.DRplanC_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.DRplanC_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.DRplanC_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanC_header);
		waitforPagetobeenable();
		
		
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_C_editlink,"Edit");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_C_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanC_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_C_Deletelink,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_C_Deletelink,"Delete");
		
		deleteDRPlan();
		waitforPagetobeenable();
		waitforPagetobeenable();
		
	}
	if(Add_DRplanD.equalsIgnoreCase("Yes")) {
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanD_header);
		waitforPagetobeenable();
		compareText_fromActualvalue( "DR Plan D_header", APT_VoiceLineObj.APT_VoiceLine.DRplanD_header, "DR Plan D");
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_D_addlink,"Add");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_D_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.DRplanD_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.DRplanD_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.DRplanD_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.DRplanD_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanD_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_D_editlink,"Edit");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_D_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanD_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_D_Deletelink,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_D_Deletelink,"Delete");
		
		deleteDRPlan();
		waitforPagetobeenable();
		waitforPagetobeenable();
	}
	if(Add_DRplanE.equalsIgnoreCase("Yes")) {
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanE_header);
		waitforPagetobeenable();
		compareText_fromActualvalue( "DR Plan E_header", APT_VoiceLineObj.APT_VoiceLine.DRplanE_header, "DR Plan E");
		
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_E_addlink,"Add");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_E_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.DRplanE_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.DRplanE_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.DRplanE_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.DRplanE_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanE_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_E_editlink,"Edit");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_E_editlink,"Edit");
	
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.DRplanE_header);
		waitforPagetobeenable();
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplanD_E_Deletelink,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.DRplanD_E_Deletelink,"Delete");
		

		deleteDRPlan();
		waitforPagetobeenable();
		waitforPagetobeenable();
	}
	waitforPagetobeenable();
	ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton);
	verifyExists(  APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton,"Back");
	click( APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton,"Back");
	waitforPagetobeenable();
}

public void addedDRplan(String DRPlanHeader, String rangestart_cc, String rangestart_lac, String rangestart_num) throws InterruptedException {

	String RangeStartValue= rangestart_cc+"-"+rangestart_lac+"-"+rangestart_num;
	//String AddedDRplan= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_tablelist).replace("value", DRPlanHeader).getAttribute("style");
	String AddedDRplan= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_tablelist1+DRPlanHeader+APT_VoiceLineObj.APT_VoiceLine.addedDRplan_tablelist2).getAttribute("style");
	
	if(!AddedDRplan.contains("height: 1px"))
	{
		//List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_rangestart).replace("value", DRPlanHeader);
		List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_rangestart1+DRPlanHeader+APT_VoiceLineObj.APT_VoiceLine.addedDRplan_rangestart2);

		int numofrows = results.size();
		//System.out.println("no of results: " + numofrows);

		if ((numofrows == 1)) {

			ExtentTestManager.getTest().log(LogStatus.PASS, DRPlanHeader+" table is empty");
		}
		else {
			for (int i = 1; i < numofrows; i++) {
				try {

					String AddedDRplandata = results.get(i).getText();
					//System.out.println(AddedDRplandata);
					if (AddedDRplandata.equalsIgnoreCase(RangeStartValue)) {

						String DRPlanRowID= webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@col-id='rangeStart'][text()='"+RangeStartValue+"']/parent::div")).getAttribute("row-id");
						//System.out.println("DR Plan row id: "+DRPlanRowID);
						if(webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).isDisplayed())
						{
							//WebElement AddedDRPlan_Checkbox= findWebElement("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]");
							//Clickon(AddedDRPlan_Checkbox);
							webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).click();
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is checked");
							Report.LogInfo("INFO","Added DR Plan checkbox is checked","PASS");
						}
						else
						{
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is already checked");
							Report.LogInfo("INFO","Added DR Plan checkbox is already checked","PASS");
						}

					}

				} catch (StaleElementReferenceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
			waitforPagetobeenable();
		}
	}
	else
	{
		Report.LogInfo("INFO","No DR Plan added in table","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No DR Plan added in table");
	}

}

public void EditDRPlan(String edit_rangestart_cc, String edit_rangestart_lac, String edit_rangestart_num, String edit_rangefinish_cc, String edit_rangefinish_lac, String edit_rangefinish_num, String edit_destinationnumber_cc, String edit_destinationnumber_lac, String edit_destinationnumber_num, String edit_activate_deactivateDRplan_dropdownvalue) throws InterruptedException, IOException {

	compareText( "Add DR Plan Header", APT_VoiceLineObj.APT_VoiceLine.addDRplan_header1, "Disaster Recovery Plans");
	compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.rangestart_label, "Range Start");
	edittextFields_commonMethod( "Range Start CC", APT_VoiceLineObj.APT_VoiceLine.rangestart_cc, edit_rangestart_cc);
	edittextFields_commonMethod( "Range Start LAC", APT_VoiceLineObj.APT_VoiceLine.rangestart_lac, edit_rangestart_lac);
	edittextFields_commonMethod( "Range Start Num", APT_VoiceLineObj.APT_VoiceLine.rangestart_num, edit_rangestart_num);
	compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.rangefinish_label, "Range Finish");
	edittextFields_commonMethod( "Range Finish CC", APT_VoiceLineObj.APT_VoiceLine.rangefinish_cc, edit_rangefinish_cc);
	edittextFields_commonMethod( "Range Finish LAC", APT_VoiceLineObj.APT_VoiceLine.rangefinish_lac, edit_rangefinish_lac);
	edittextFields_commonMethod( "Range Finish Num", APT_VoiceLineObj.APT_VoiceLine.rangefinish_num, edit_rangefinish_num);
	compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_label, "Destination Number(DN)");
	edittextFields_commonMethod( "Destination Number CC", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_cc, edit_destinationnumber_cc);
	edittextFields_commonMethod( "Destination Number LAC", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_lac, edit_destinationnumber_lac);
	edittextFields_commonMethod( "Destination Number Num", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_num, edit_destinationnumber_num);
	compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.activate_deactivate_label, "Activate/Deactivate DR Plan");
	//activate_deactivateDRPlan_dropdown( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.activate_deactivate_dropdownvalue, edit_activate_deactivateDRplan_dropdownvalue);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	waitforPagetobeenable();
}

public void activate_deactivateDRPlan_dropdown(String labelname, String xpath, String expectedValueToAdd) {

	boolean availability=false;
	try {  
		availability=findWebElement(xpath).isDisplayed();
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
			Report.LogInfo("INFO",labelname + " dropdown is displaying","PASS");

			if(expectedValueToAdd.equalsIgnoreCase("null")) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under "+ labelname + " dropdown");
				Report.LogInfo("INFO"," No values selected under "+ labelname + " dropdown","PASS");
			}else {

				//Clickon(findWebElement("//div[label[text()='"+ labelname +"']]/parent::div/following-sibling::div//div[text()='�']"));
				webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]/parent::div/following-sibling::div//div[text()='�']")).click();
				waitforPagetobeenable();

				//verify list of values inside dropdown
				List<WebElement> listofvalues = webDriver
						.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

				ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside "+ labelname + " dropdown is:  ");
				Report.LogInfo("INFO"," List of values inside "+ labelname + "dropdown is:  ","PASS");

				for (WebElement valuetypes : listofvalues) {
					//Log.info("service sub types : " + valuetypes.getText());
					ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
					Report.LogInfo("INFO"," " + valuetypes.getText(),"PASS");
				}

				waitforPagetobeenable();
				//SendKeys(findWebElement("(//label[text()='"+ labelname +"']/parent::div/parent::div/following-sibling::div//input)[1]"), expectedValueToAdd);	
				 webDriver.findElement(By.xpath("(//label[text()='"+ labelname +"']/parent::div/parent::div/following-sibling::div//input)[1]")).sendKeys(expectedValueToAdd);
				waitforPagetobeenable();

				//Clickon(findWebElement("(//div[contains(text(),'"+ expectedValueToAdd +"')])[1]"));
				webDriver.findElement(By.xpath("(//div[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();
				waitforPagetobeenable();

				String actualValue=findWebElement("//label[text()='"+ labelname +"']/parent::div/parent::div/following-sibling::div//span").getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
				Report.LogInfo("INFO",labelname + " dropdown value selected as: "+ actualValue,"PASS");

			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
		Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
		Report.LogInfo("INFO"," NO value selected under "+ labelname + " dropdown","FAIL");
	}
}

public void AddDRPlan(String rangestart_cc, String rangestart_lac, String rangestart_num, String rangefinish_cc, String rangefinish_lac, String rangefinish_num, String destinationnumber_cc, String destinationnumber_lac, String destinationnumber_num, String activate_deactivateDRplan_dropdownvalue) throws InterruptedException, IOException {

	compareText( "Add DR Plan Header", APT_VoiceLineObj.APT_VoiceLine.addDRplan_header1, "Disaster Recovery Plans");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	
	waitforPagetobeenable();
	//verify warning messages
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.rangestart_cc_warngmsg, "Range Start CC");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.rangestart_lac_warngmsg, "Range Start LAC");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.rangestart_num_warngmsg, "Range Start Num");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.rangefinish_cc_warngmsg, "Range Finish CC");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.rangefinish_lac_warngmsg, "Range Finish LAC");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.rangefinish_num_warngmsg, "Range Finish Num");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.destinationnumber_cc_warngmsg, "Destination Number CC");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.destinationnumber_lac_warngmsg, "Destination Number LAC");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.destinationnumber_num_warngmsg, "Destination Number Num");

	//verify Add DR plan
	compareText( "Range Start", APT_VoiceLineObj.APT_VoiceLine.rangestart_label, "Range Start");
	addtextFields_commonMethod( "Range Start CC", APT_VoiceLineObj.APT_VoiceLine.rangestart_cc, rangestart_cc);
	addtextFields_commonMethod( "Range Start LAC", APT_VoiceLineObj.APT_VoiceLine.rangestart_lac, rangestart_lac);
	addtextFields_commonMethod( "Range Start Num", APT_VoiceLineObj.APT_VoiceLine.rangestart_num, rangestart_num);
	compareText( "Range Finish", APT_VoiceLineObj.APT_VoiceLine.rangefinish_label, "Range Finish");
	addtextFields_commonMethod( "Range Finish CC", APT_VoiceLineObj.APT_VoiceLine.rangefinish_cc, rangefinish_cc);
	addtextFields_commonMethod( "Range Finish LAC", APT_VoiceLineObj.APT_VoiceLine.rangefinish_lac, rangefinish_lac);
	addtextFields_commonMethod( "Range Finish Num",APT_VoiceLineObj.APT_VoiceLine.rangefinish_num, rangefinish_num);
	compareText( "Destination Number(DN)", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_label,"Destination Number(DN)");
	addtextFields_commonMethod( "Destination Number CC", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_cc, destinationnumber_cc);
	addtextFields_commonMethod( "Destination Number LAC", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_lac, destinationnumber_lac);
	addtextFields_commonMethod( "Destination Number Num", APT_VoiceLineObj.APT_VoiceLine.destinationnumber_num, destinationnumber_num);
	compareText( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.activate_deactivate_label, "Activate/Deactivate DR Plan");
	activate_deactivateDRPlan_dropdown( "Activate/Deactivate DR Plan", APT_VoiceLineObj.APT_VoiceLine.activate_deactivate_dropdownvalue, activate_deactivateDRplan_dropdownvalue);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	waitforPagetobeenable();
}

public void editedDRplan(String DRPlanHeader, String rangestart_cc, String rangestart_lac, String rangestart_num, String edit_rangestart_cc, String edit_rangestart_lac, String edit_rangestart_num) throws InterruptedException {

	String rangestart_cc_value;
	String rangestart_lac_value;
	String rangestart_num_value;

	if(edit_rangestart_cc.equalsIgnoreCase("null"))
	{
		rangestart_cc_value= rangestart_cc;
	}
	else
	{
		rangestart_cc_value= edit_rangestart_cc;
	}
	if(edit_rangestart_lac.equalsIgnoreCase("null"))
	{
		rangestart_lac_value= rangestart_lac;
	}
	else
	{
		rangestart_lac_value= edit_rangestart_lac;
	}
	if(edit_rangestart_num.equalsIgnoreCase("null"))
	{
		rangestart_num_value= rangestart_num;
	}
	else
	{
		rangestart_num_value= edit_rangestart_num;
	}
	String RangeStartValue= rangestart_cc_value+"-"+rangestart_lac_value+"-"+rangestart_num_value;
	//String AddedDRplan= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_tablelist).replace("value", DRPlanHeader).getAttribute("style");
	String AddedDRplan= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_tablelist1+DRPlanHeader+APT_VoiceLineObj.APT_VoiceLine.addedDRplan_tablelist2).getAttribute("style");
	if(!AddedDRplan.contains("height: 1px"))
	{
		//List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_rangestart).replace("value", DRPlanHeader);
		List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedDRplan_rangestart1+DRPlanHeader+APT_VoiceLineObj.APT_VoiceLine.addedDRplan_rangestart2);
		int numofrows = results.size();
		//System.out.println("no of results: " + numofrows);

		if ((numofrows == 0)) {

			ExtentTestManager.getTest().log(LogStatus.PASS, DRPlanHeader+" table is empty");
			Report.LogInfo("INFO", DRPlanHeader+" table is empty","PASS");
		}
		else {
			for (int i = 0; i < numofrows; i++) {
				try {

					String AddedDRplandata = results.get(i).getText();
					//System.out.println(AddedDRplandata);
					if (AddedDRplandata.equalsIgnoreCase(RangeStartValue)) {

						String DRPlanRowID= findWebElement("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@col-id='rangeStart'][text()='"+RangeStartValue+"']/parent::div").getAttribute("row-id");
						//System.out.println("DR Plan row id: "+DRPlanRowID);
						if(webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).isDisplayed())
						{
							//WebElement AddedDRPlan_Checkbox= findWebElement("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]");
							//Clickon(AddedDRPlan_Checkbox);
							
							webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).click();
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is checked");
							Report.LogInfo("INFO", "Added DR Plan checkbox is checked","PASS");
						}
						else
						{
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is already checked");
							Report.LogInfo("INFO","Added DR Plan checkbox is already checked","PASS");
						}

					}

				} catch (StaleElementReferenceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
			waitforPagetobeenable();
		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No DR Plan added in table");
		Report.LogInfo("INFO","No DR Plan added in table","FAIL");
	}

}

public void deleteDRPlan() throws InterruptedException, IOException, AWTException {

	
	//Alert alert = webDriver.switchTo().alert();		

	// Capturing alert message   
	//String alertMessage= webDriver.switchTo().alert().getText();
	
	
	
	//if(alertMessage.isEmpty()) {
		//ExtentTestManager.getTest().log(LogStatus.FAIL, "No message displays");
		//Report.LogInfo("INFO","No Message displays","FAIL"); 
	//}else {
		//ExtentTestManager.getTest().log(LogStatus.PASS, "Alert message displays as: "+alertMessage);
		//Report.LogInfo("INFO","Text message for alert displays as: "+alertMessage,"PASS");
	//}

	try {  
		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
	
	}catch(Exception e) {
		e.printStackTrace();
	}
}

public void verifyDRPlansBulkInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	//String bulkjob_filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BulkJob_FilePath");
	
	
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewtrunk_link_AddDDI,"View Trunk");
	//click(APT_VoiceLineObj.APT_VoiceLine.viewtrunk_link_AddDDI,"View Trunk");
	
	String path1 = new File(".").getCanonicalPath();
	String bulkjob_filepath = path1+"/TestDr3.csv";
	
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_usageofincomingrouting);
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.DRplans_bulkinterface_link,"DR Plans Bulk Interface");
	click(APT_VoiceLineObj.APT_VoiceLine.DRplans_bulkinterface_link,"DR Plans Bulk Interface");
	
	waitforPagetobeenable();
	compareText_fromActualvalue( "Bulk Interface Header", APT_VoiceLineObj.APT_VoiceLine.bulkinterfaceheader, "Bulk Interface");
	try {
		WebElement BulkJob_Choosefile_button= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkjob_choosefilebutton);
		//click(APT_VoiceLineObj.APT_VoiceLine.bulkjob_choosefilebutton);
		//javaScriptclick(APT_VoiceLineObj.APT_VoiceLine.bulkjob_choosefilebutton);
		//click(APT_VoiceLineObj.APT_VoiceLine.bulkjob_choosefilebutton);
		//verifyExists(APT_VoiceLineObj.APT_VoiceLine.bulkjob_choosefilebutton,"Bulk File path");
		sendKeys(APT_VoiceLineObj.APT_VoiceLine.bulkjob_choosefilebutton,bulkjob_filepath,"Bulk File path");
		
		//BulkJob_Choosefile_button.sendKeys(bulkjob_filepath);
		//click_commonMethod( "Submit", "bulkjobsubmit");
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.bulkjobsubmit,"Submit");
		click( APT_VoiceLineObj.APT_VoiceLine.bulkjobsubmit,"Submit");
		waitforPagetobeenable();
	}
	catch (Exception e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Upload file path '"+bulkjob_filepath+"' is not found");
		Report.LogInfo("INFO","Step : Upload file path '"+bulkjob_filepath+"' is not found","FAIL");
	}
	//Archive link in bulk interface page
	//click_commonMethod( "Action dropdown", "bulkinterface_actiondropdown");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_actiondropdown,"Action dropdown");
	//click( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_actiondropdown,"Action dropdown");
	
	//click_commonMethod( "Archive", "bulkinterface_archivelink");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_archivelink,"Archive");
	click( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_archivelink,"Archive");
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.archive_backbutton);
	//click_commonMethod( "Back", "archive_backbutton");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.archive_backbutton,"Back");
	click( APT_VoiceLineObj.APT_VoiceLine.archive_backbutton,"Back");
	waitforPagetobeenable();
	//Refresh link in bulk interface page
	scrollIntoTop();
	//click_commonMethod( "Action dropdown", "bulkinterface_actiondropdown");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_actiondropdown,"Action dropdown");
	//click( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_actiondropdown,"Action dropdown");
	
	//click_commonMethod( "Refresh", "bulkinterface_refreshlink");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_refreshlink,"Refresh");
	click( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_refreshlink,"Refresh");
	
	waitforPagetobeenable();
	compareText_fromActualvalue( "Bulk Interface Header", APT_VoiceLineObj.APT_VoiceLine.bulkinterfaceheader, "Bulk Interface");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Bulk Interface page refresh successful");
	Report.LogInfo("INFO","Bulk Interface page refresh successful","PASS");
	waitforPagetobeenable();

	addedBulkInterface();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_backbutton);
	
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_backbutton,"Back Button Bulk Interface");
	click( APT_VoiceLineObj.APT_VoiceLine.bulkinterface_backbutton,"Back Button Bulk Interface");
}

public void addedBulkInterface() throws InterruptedException {

	compareText( "TrunkGroup Name", APT_VoiceLineObj.APT_VoiceLine.trunkgroupname_columnheader, "TrunkGroup Name");
	compareText( "User Name",APT_VoiceLineObj.APT_VoiceLine.username_columnheader, "User Name");
	compareText( "Submit Time", APT_VoiceLineObj.APT_VoiceLine.submittime_columnheader, "Submit Time");
	compareText( "Start Time", APT_VoiceLineObj.APT_VoiceLine.starttime_columnheader, "Start Time");
	compareText( "End Time", APT_VoiceLineObj.APT_VoiceLine.endtime_columnheader, "End Time");
	compareText( "Status", APT_VoiceLineObj.APT_VoiceLine.status_columnheader, "Status");
	compareText( "Completion (%)", APT_VoiceLineObj.APT_VoiceLine.completion_columnheader, "Completion (%)");
	compareText( "Log", APT_VoiceLineObj.APT_VoiceLine.log_columnheader, "Log");
	compareText( "Upload File", APT_VoiceLineObj.APT_VoiceLine.uploadfile_columnheader, "Upload File");

	int TotalPages;
	String TotalPagesText = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbTotal'])[1]")).getText();
	TotalPages = Integer.parseInt(TotalPagesText);
	//System.out.println("Total number of pages in table is: " + TotalPages);

	if (TotalPages != 0) {

		outerloop:
			for (int k = 1; k <= TotalPages; k++) {
				String AddedDRplan_BulkInterface= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedbulkinterface_tablelist).getAttribute("style");
				if(!AddedDRplan_BulkInterface.contains("height: 1px"))
				{
					//List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedbulkinterface_trunkgroupname).replace("value", Edit_primarytrunkGroupname);
					List<WebElement> results = findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedbulkinterface_trunkgroupname1+Edit_primarytrunkGroupname+APT_VoiceLineObj.APT_VoiceLine.addedbulkinterface_trunkgroupname2);
					int numofrows = results.size();
					//System.out.println("no of results: " + numofrows);

					if ((numofrows == 0)) {

						ExtentTestManager.getTest().log(LogStatus.PASS, "Bulk Interface table is empty");
						Report.LogInfo("INFO","Bulk Interface table is empty","PASS");
					}
					else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent'])[1]")).getText();//updated
						int Current_page = Integer.parseInt(CurrentPage);
						//System.out.println("The current page is: " + Current_page);

						//sa.assertEquals(k, Current_page);

						//System.out.println("Currently we are in page number: " + Current_page);
						for (int i = 0; i < numofrows; i++) {
							try {

								String AddedBulkInterfacedata = results.get(i).getText();
								//System.out.println(AddedBulkInterfacedata);
								if (AddedBulkInterfacedata.equalsIgnoreCase(Edit_primarytrunkGroupname)) {

									//String BulkInterfaceRowID= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedbulkinterface_rowid).replace("value", Edit_primarytrunkGroupname).getAttribute("row-id");
									String BulkInterfaceRowID=webDriver.findElement(By.xpath("(//div[@ref='eBodyContainer'])[1]//div[text()='"+Edit_primarytrunkGroupname+"']/parent::div[@role='row']")).getAttribute("row-id");//changed
									
									//System.out.println("Bulk Interface row id: "+BulkInterfaceRowID);
									
									//String BulkInterface_Trunkgroupname= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_trunkgroupname_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_Trunkgroupname=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='tgName']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Trunk Group Name column value:"+BulkInterface_Trunkgroupname);
									Report.LogInfo("INFO", "Step : Trunk Group Name column value:"+BulkInterface_Trunkgroupname,"PASS");
									//String BulkInterface_Username= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_username_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_Username=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='user']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : User Name column value:"+BulkInterface_Username);
									Report.LogInfo("INFO", "Step : User Name column value:"+BulkInterface_Username,"PASS");
									
									//String BulkInterface_SubmitTime= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_submittime_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_SubmitTime=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='submitTime']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Submit Time column value:"+BulkInterface_SubmitTime);
									Report.LogInfo("INFO","Step : Submit Time column value:"+BulkInterface_SubmitTime,"PASS");
									
									//String BulkInterface_startTime= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_starttime_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_startTime=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='startTime']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Start Time column value:"+BulkInterface_startTime);
									Report.LogInfo("INFO","Step : Start Time column value:"+BulkInterface_startTime,"PASS");
									
									//String BulkInterface_endTime= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_endtime_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_endTime=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='endTime']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : End Time column value:"+BulkInterface_endTime);
									Report.LogInfo("INFO","Step : End Time column value:"+BulkInterface_endTime,"PASS");
									//String BulkInterface_status= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_status_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_status=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='status']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Status column value:"+BulkInterface_status);
									Report.LogInfo("INFO","Step : Status column value:"+BulkInterface_status,"PASS");
									//String BulkInterface_completionPercentage= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_completion_value).replace("value", BulkInterfaceRowID).getText();
									String BulkInterface_completionPercentage=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='completionPercentage']")).getText();
									
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Completion Percentage column value:"+BulkInterface_completionPercentage);
									Report.LogInfo("INFO","Step : Completion Percentage column value:"+BulkInterface_completionPercentage,"PASS");
									//String BulkInterface_Log= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_log).replace("value", BulkInterfaceRowID).getText();
									
									String BulkInterface_Log=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='log']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Log column value:"+BulkInterface_Log);
									Report.LogInfo("INFO","Step : Log column value:"+BulkInterface_Log,"PASS");
									///String BulkInterface_Uploadfile= findWebElement(APT_VoiceLineObj.APT_VoiceLine.bulkinterface_file).replace("value", BulkInterfaceRowID).getText();
									
									String BulkInterface_Uploadfile=webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//div[@row-id='"+BulkInterfaceRowID+"']/div[@col-id='fileName']")).getText();
									ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Upload file column value:"+BulkInterface_Uploadfile);
									Report.LogInfo("INFO","Step : Upload file column value:"+BulkInterface_Uploadfile,"PASS");
									break outerloop;
								}

							} catch (StaleElementReferenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}
						}
						waitforPagetobeenable();
					}
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No Bulk Interface added in table");
					Report.LogInfo("INFO","No Bulk Interface added in table","FAIL");
				}
			}

	}else {

		//System.out.println("No data available in table");
		Report.LogInfo("INFO","No data available in table","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No data available in table");
	}
}

public void verifydownloadDRplans(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String DRPlansfilename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DRPlans_FileName");
	String browserfiles_downloadspath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Browserfiles_Downloadspath");

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_usageofincomingrouting);
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.downloadDRplans_link,"Download DR Plans");
	click(APT_VoiceLineObj.APT_VoiceLine.downloadDRplans_link,"Download DR Plans");
	waitforPagetobeenable();
	waitforPagetobeenable();
	//isFileDownloaded(DRPlansfilename, browserfiles_downloadspath);
}


public static boolean isFileDownloaded(String fileName, String downloadspath) {
	boolean flag = false;
	//paste your directory path below
	//eg: C:\\Users\\username\\Downloads
	String dirPath = downloadspath; 
	File dir = new File(dirPath);
	File[] files = dir.listFiles();
	try {
		if (files.length == 0 || files == null) {
			Report.LogInfo("INFO","The directory is empty","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Downloads folder is empty");
			flag = false;
		} else {
			for (File listFile : files) {
				if (listFile.getName().contains(fileName)) {
					Report.LogInfo("INFO",fileName + " is present","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '"+fileName+"' excel file is downloaded successfully");
					break;
				}
				flag = true;
			}
		}
	}
	catch (Exception e) {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Step : Downloads folder path '"+dirPath+"' is not found");
		Report.LogInfo("INFO","Step : Downloads folder path '"+dirPath+"' is not found","INFO");
	}
	return flag;
}

public void VerifyDisasterRecoveryStatus() throws InterruptedException {

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.codecfield_viewpage);
	waitforPagetobeenable();
	compareText("GSX/PSX Disaster Recovery Status header", APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header, "GSX/PSX Disaster Recovery Status");
	GetText("PSX TDM DR Status", APT_VoiceLineObj.APT_VoiceLine.psx_tdm_drstatus_value);
	waitforPagetobeenable();

}

public void AddPortGroup(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String routePriority_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutePriority_DropdownValue");
	String voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String edit_voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header);
	compareText( "Port Group", APT_VoiceLineObj.APT_VoiceLine.portgroup_header, "Port Group");
	//click_commonMethod( "Action Dropdown", portgroup_Actiondropdown);
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//click( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	waitforPagetobeenable();
	//click_commonMethod( "Add Port Group", addportgroup_link);
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.addportgroup_link,"Add Port Group");
	click( APT_VoiceLineObj.APT_VoiceLine.addportgroup_link,"Add Port Group");
	
	waitforPagetobeenable();
	compareText( "Port Group", APT_VoiceLineObj.APT_VoiceLine.addportgroup_header, "Port Group");
	addDropdownValues_commonMethod( "Prefix", APT_VoiceLineObj.APT_VoiceLine.prefix_dropdown, prefix_dropdownvalue);
	addDropdownValues_commonMethod( "Route Priority", APT_VoiceLineObj.APT_VoiceLine.routepriority_dropdown, routePriority_dropdownvalue);

	//Ports available
	selectPortsAvailableFromLeftDropdown( "Ports Available", APT_VoiceLineObj.APT_VoiceLine.portsavailable_list , voiceCPEdevicename, edit_voiceCPEdevicename, APT_VoiceLineObj.APT_VoiceLine.portsavailable_addarrow);
	verifySelectedValuesInsideRightDropdown( "Ports Selected", APT_VoiceLineObj.APT_VoiceLine.portsselected_list);

	//click_commonMethod( "OK", "okbutton");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	waitforPagetobeenable();
	verifysuccessmessage( "Port Group VCPE successfully created.");
}

public void selectPortsAvailableFromLeftDropdown(String labelname, String xpath, String voiceCPEdevicename, String edit_voiceCPEdevicename, String xpathForAddButton) {

	List<String> ls = new ArrayList<String>();

	try{

		List<WebElement> elements= findWebElements(xpath);
		int element_count= elements.size();

		if(element_count>=1) {

			//Print list of values inside Dropdown 
			for(WebElement a : elements) {
				ls.add(a.getText());
			}
			Set<String> set= new HashSet<String>(ls);
			ls=new ArrayList<String>(set);
			ExtentTestManager.getTest().log(LogStatus.PASS, "list of values displaying inside "+labelname+" available dropdown is: "+ls);
			Report.LogInfo("INFO","list of values dipslaying inside "+labelname+" dropdown is: "+ls,"PASS");

			//select value inside the dropdown     
			waitforPagetobeenable();
			for(int j=0; j<ls.size() ; j++) {
				//System.out.println("ls value "+ ls.get(j));
				if((ls.get(j)).contains(voiceCPEdevicename) || (ls.get(j)).contains(edit_voiceCPEdevicename))
				{
					elements.get(j).click();
					ExtentTestManager.getTest().log(LogStatus.PASS, elements.get(j) + " got selected" );
					Report.LogInfo("INFO", elements.get(j) + " got selected" ,"PASS");
					waitforPagetobeenable();
					//click_commonMethod(application, "Add", xpathForAddButton , xml);
					verifyExists(xpathForAddButton,"Add");
					click(xpathForAddButton,"Add");
					waitforPagetobeenable();
				}

			}

		}else {
			ExtentTestManager.getTest().log(LogStatus.INFO, "No values displaying under " + labelname + " dropdown");
			Report.LogInfo("INFO","No values displaying under " + labelname + " available dropdown","INFO");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under "+labelname + " available dropdown");
		Report.LogInfo("INFO","No values displaying under "+labelname + " available dropdown","FAIL");
	}
}

public void ViewPortGroup(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String Edit_prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Prefix_DropdownValue");
	String routePriority_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutePriority_DropdownValue");
	String voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String edit_voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header);
	compareText( "Port Group", APT_VoiceLineObj.APT_VoiceLine.portgroup_header, "Port Group");
	List<WebElement> ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]"));
	webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'View')]")).click();

	//String AddedPort_RowID= viewPortGroupRow( prefix_dropdownvalue, routePriority_dropdownvalue, voiceCPEdevicename, edit_voiceCPEdevicename);
	//WebElement AddedPort_Checkbox= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox).replace("value", AddedPort_RowID);
	//Clickon(AddedPort_Checkbox);
	//click(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox);
	//webDriver.findElement(By.xpath("//div[@row-id='"+AddedPort_RowID+"']//div//span[contains(@class,'unchecked')]")).click();
	 
	
	//click_commonMethod( "Action Dropdown", "portgroup_Actiondropdown");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//click( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//waitforPagetobeenable();
	//click_commonMethod( "View", "view");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.view,"View");
	//click(APT_VoiceLineObj.APT_VoiceLine.view,"View");
	waitforPagetobeenable();
	compareText( "Port Group", APT_VoiceLineObj.APT_VoiceLine.addportgroup_header, "Port Group");
	GetText( "Prefix", APT_VoiceLineObj.APT_VoiceLine.prefix_viewportgroup);
	GetText( "Voice CPES", APT_VoiceLineObj.APT_VoiceLine.voiceCPEs_viewportgroup);
	GetText( "Route Priority", APT_VoiceLineObj.APT_VoiceLine.routepriority_viewportgroup);
	
	//click_commonMethod( "Close", "close_viewportgroup");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.close_viewportgroup,"Close");
	click( APT_VoiceLineObj.APT_VoiceLine.close_viewportgroup,"Close");

}

public String viewPortGroupRow(String prefix_dropdownvalue, String routePriority_dropdownvalue, String voiceCPEdevicename, String edit_voiceCPEdevicename) throws InterruptedException {

	String RowID=null;
	List<WebElement> ExistingPort_prefix;
	//System.out.println("Voice CPEs"+voiceCPEdevicename);
	//ExistingPort_prefix= findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_prefixvalue).replace("value", prefix_dropdownvalue);
	//ExistingPort_prefix=webDriver.findElements(By.xpath("//div[@col-id='prefix'][text()='"+prefix_dropdownvalue+"']/parent::div"));
	ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]"));
	webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'View')]")).click();

	int ExistingPort_prefixCount= ExistingPort_prefix.size();
	for(int i=0;i<ExistingPort_prefixCount;i++)
	{
		String ExistingPort_prefix_RowID= ExistingPort_prefix.get(i).getAttribute("row-id");
		//String RoutePriorityValue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_routepriorityvalue).replace("value", ExistingPort_prefix_RowID).getText();
		String RoutePriorityValue=webDriver.findElement(By.xpath("//div[@row-id='"+ExistingPort_prefix_RowID+"']//div[@col-id='routePriority']")).getText();
		//String VoiceCPEsValue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_voiceCPEsValue).replace("value", ExistingPort_prefix_RowID).getText();
		String VoiceCPEsValue=webDriver.findElement(By.xpath("//div[@row-id='"+ExistingPort_prefix_RowID+"']//div[@col-id='scl']")).getText();
		if(routePriority_dropdownvalue.contains(RoutePriorityValue))
		{
			RowID= ExistingPort_prefix_RowID;
			if(VoiceCPEsValue.contains(voiceCPEdevicename) || VoiceCPEsValue.contains(edit_voiceCPEdevicename))
			{
				RowID= ExistingPort_prefix_RowID;
				break;
			}
		}

	}
	return RowID;
}

public void EditPortGroup(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String routePriority_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutePriority_DropdownValue");
	String Edit_prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Prefix_DropdownValue");
	String Edit_routePriority_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_RoutePriority_DropdownValue");
	String voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String edit_voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header);
	compareText( "Port Group", APT_VoiceLineObj.APT_VoiceLine.portgroup_header, "Port Group");
	
	List<WebElement> ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]"));
	webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'Edit')]")).click();

	//String AddedPort_RowID= viewPortGroupRow( prefix_dropdownvalue, routePriority_dropdownvalue, voiceCPEdevicename, edit_voiceCPEdevicename);
	//WebElement AddedPort_Checkbox= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox).replace("value", AddedPort_RowID);
	//Clickon(AddedPort_Checkbox);
	//click(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox);
	//webDriver.findElement(By.xpath("//div[@row-id='"+AddedPort_RowID+"']//div//span[contains(@class,'unchecked')]")).click();
	
	
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//click(APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.edit, "Edit");
	//click(APT_VoiceLineObj.APT_VoiceLine.edit, "Edit");
	
	waitforPagetobeenable();
	compareText( "Port Group", APT_VoiceLineObj.APT_VoiceLine.addportgroup_header, "Port Group");
	waitforPagetobeenable();
	editPrefix_dropdown( "Prefix",APT_VoiceLineObj.APT_VoiceLine. prefix_dropdown, Edit_prefix_dropdownvalue);
	addDropdownValues_commonMethod( "Route Priority", APT_VoiceLineObj.APT_VoiceLine.routepriority_dropdown, Edit_routePriority_dropdownvalue);

	//Ports available
	selectAndRemovePortsFromRightDropdown( "Ports Selected", APT_VoiceLineObj.APT_VoiceLine.portsselected_list, voiceCPEdevicename, edit_voiceCPEdevicename, APT_VoiceLineObj.APT_VoiceLine.portsavailable_removearrow);
	selectPortsAvailableFromLeftDropdown( "Ports Available", APT_VoiceLineObj.APT_VoiceLine.portsavailable_list , voiceCPEdevicename, edit_voiceCPEdevicename, APT_VoiceLineObj.APT_VoiceLine.portsavailable_addarrow);
	verifySelectedValuesInsideRightDropdown( "Ports Selected",  APT_VoiceLineObj.APT_VoiceLine.portsselected_list);

	//click_commonMethod( "OK", "okbutton");
	verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	
	waitforPagetobeenable();
	verifysuccessmessage( "Port Group VCPE successfully updated.");

}

public void editPrefix_dropdown(String labelname, String xpath, String expectedValueToAdd) throws InterruptedException {

	boolean availability=false;
	try {  
		availability=findWebElement(xpath).isDisplayed();
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
			Report.LogInfo("INFO",labelname + " dropdown is displaying","PASS");

			if(expectedValueToAdd.equalsIgnoreCase("null")) {

				ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under "+ labelname + " dropdown");
				Report.LogInfo("INFO"," No values selected under "+ labelname + " dropdown","PASS");
			}else {

				//Clickon(findWebElement("//div[label[text()='"+ labelname +"']]//div[text()='�']"));
				webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
				waitforPagetobeenable();

				//verify list of values inside dropdown
				List<WebElement> listofvalues = webDriver.findElements(By.xpath("//label[text()='Prefix']/following::div[@role='list']"));
				
				//div[@class='sc-bxivhb kqVrwh']

				ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside "+ labelname + " dropdown is:  ");
				Report.LogInfo("INFO", " List of values inside "+ labelname + "dropdown is:  ","PASS");

				for (WebElement valuetypes : listofvalues) {
					//Log.info("service sub types : " + valuetypes.getText());
					ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
					Report.LogInfo("INFO"," " + valuetypes.getText(),"PASS");
				}

				waitforPagetobeenable();
				//SendKeys(findWebElement("//div[label[text()='"+ labelname +"']]//input"), expectedValueToAdd);	
				webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd);
				waitforPagetobeenable();

				//Clickon(findWebElement("(//div[label[text()='Prefix']]//div[contains(text(),'"+ expectedValueToAdd +"')])[1]"));
				webDriver.findElement(By.xpath("(//div[label[text()='Prefix']]//div[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();
				waitforPagetobeenable();

				String actualValue=webDriver.findElement(By.xpath("//label[text()='"+ labelname +"']/following-sibling::div//span")).getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
				Report.LogInfo("INFO",labelname + " dropdown value selected as: "+ actualValue,"PASS");

			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
		Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
		Report.LogInfo("INFO"," NO value selected under "+ labelname + " dropdown","FAIL");
	}
}

public void selectAndRemovePortsFromRightDropdown(String labelname, String xpath, String voiceCPEdevicename, String edit_voiceCPEdevicename, String xpathForRemoveButton) {

	List<String> ls = new ArrayList<String>();

	try{
		List<WebElement> elements= findWebElements(xpath);
		int element_count= elements.size();

		if(element_count>=1) {

			//Print list of values inside Dropdown 
			for(WebElement a : elements) {
				ls.add(a.getText());
			}
			Set<String> set= new HashSet<String>(ls);
			ls= new ArrayList<String>(set);
			ExtentTestManager.getTest().log(LogStatus.PASS, "list of values displaying inside "+labelname+" available dropdown is: "+ls);
			Report.LogInfo("INFO","list of values dipslaying inside "+labelname+" dropdown is: "+ls,"PASS");

			//select value inside the dropdown     
			waitforPagetobeenable();
			for(int j=0; j<ls.size() ; j++) {
				//System.out.println("ls value "+ ls.get(j));
				if((ls.get(j)).contains(voiceCPEdevicename) || (ls.get(j)).contains(edit_voiceCPEdevicename))
				{
					elements.get(j).click();
					ExtentTestManager.getTest().log(LogStatus.PASS, elements.get(j) + " got selected" );
					Report.LogInfo("INFO", elements.get(j) + " got selected","PASS");
					waitforPagetobeenable();
					//WebElement removeButton=findWebElement(xpathForRemoveButton).replace("value", "<<"));
					
					//click(xpathForRemoveButton);
					
					webDriver.findElement(By.xpath("//button/span[text()='"+"<<"+"']")).click();
					
					ExtentTestManager.getTest().log(LogStatus.PASS, "clicked on remove '<<' button");
					Report.LogInfo("INFO","clicked on remove '<<' button","PASS");
					waitforPagetobeenable();
				}
			}

		}else {
			ExtentTestManager.getTest().log(LogStatus.INFO, "No values displaying under " + labelname + " dropdown");

			Report.LogInfo("INFO","No values displaying under " + labelname + " available dropdown","INFO");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under "+labelname + " available dropdown");
		Report.LogInfo("INFO", "No values displaying under "+labelname + " available dropdown","FAIL");
	}
}

public void OverflowPortGroup(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String Edit_prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Prefix_DropdownValue");
	String routePriority_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutePriority_DropdownValue");
	String voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String edit_voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");

	String overflowPrefixValue= null;
	ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header);
	compareText( "Port Group",  APT_VoiceLineObj.APT_VoiceLine.portgroup_header, "Port Group");
	waitforPagetobeenable();
if(!Edit_prefix_dropdownvalue.equalsIgnoreCase("null"))
{
	List<WebElement> ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+Edit_prefix_dropdownvalue+"')]"));
	webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+Edit_prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'Add Overflow PortGroup')]")).click();
}
	else{
		List<WebElement> ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]"));
		webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'Add Overflow PortGroup')]")).click();
}
	//String AddedPort_RowID= ExistingPortGroupRow( prefix_dropdownvalue, Edit_prefix_dropdownvalue, routePriority_dropdownvalue, voiceCPEdevicename, edit_voiceCPEdevicename);
	//WebElement AddedPort_Checkbox= findWebElement( APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox).replace("value", AddedPort_RowID);
	//Clickon(AddedPort_Checkbox);
	//click(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox);
	//webDriver.findElement(By.xpath("//div[@row-id='"+AddedPort_RowID+"']//div//span[contains(@class,'unchecked')]")).click();
	
	//click_commonMethod( "Action Dropdown", "portgroup_Actiondropdown");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//click( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	
	//click_commonMethod( "Add Overflow PortGroup", "addoverflowportgroup");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.addoverflowportgroup,"Add Overflow PortGroup");
	//click( APT_VoiceLineObj.APT_VoiceLine.addoverflowportgroup,"Add Overflow PortGroup");
	waitforPagetobeenable();

	String readonly_overflowprefix= findWebElement( APT_VoiceLineObj.APT_VoiceLine.prefix_overflowport).getAttribute("readonly");
	overflowPrefixValue= findWebElement( APT_VoiceLineObj.APT_VoiceLine.prefix_overflowport).getAttribute("value");
	if(readonly_overflowprefix==null) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "Prefix value is displayed as: "+overflowPrefixValue);
		Report.LogInfo("INFO", "Prefix value is displayed as: "+overflowPrefixValue,"PASS");
	}
	if(findWebElement( APT_VoiceLineObj.APT_VoiceLine.routeprioritydisabled_overflowport).getAttribute("disabled")==null) {
		String RoutePriorityValue= findWebElement( APT_VoiceLineObj.APT_VoiceLine.routepriorityvalue_overflowport).getText();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Route Priority value is displayed as: "+RoutePriorityValue);
		Report.LogInfo("INFO", "Route Priority value is displayed as: "+RoutePriorityValue,"PASS");
	}

	//Ports available
	selectPortsAvailableFromLeftDropdown( "Ports Available", APT_VoiceLineObj.APT_VoiceLine.overflowportsavailable_list , voiceCPEdevicename, edit_voiceCPEdevicename, APT_VoiceLineObj.APT_VoiceLine.portsavailable_addarrow);
	verifySelectedValuesInsideRightDropdown( "Ports Selected", APT_VoiceLineObj.APT_VoiceLine.overflow_portsselected_list);

	verifyExists( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click( APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	waitforPagetobeenable();
	verifysuccessmessage( "Port Group OverFlow successfully created.");

	//delete overflow portgroup
	webDriver.navigate().refresh();
	
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header);
	waitforPagetobeenable();
	//WebElement overflowcheckbox= findWebElement(APT_VoiceLineObj.APT_VoiceLine.overflow_checkbox).replace("value", overflowPrefixValue);
	//Clickon(overflowcheckbox);
	//click(APT_VoiceLineObj.APT_VoiceLine.overflow_checkbox);
	//webDriver.findElement(By.xpath("//div[@role='gridcell'][text()='"+overflowPrefixValue+"']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']")).click();
	waitforPagetobeenable();
	//click_commonMethod( "Action Dropdown", "portgroup_Actiondropdown");
	//verifyExists( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//click( APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+overflowPrefixValue+"')]/following-sibling::td//a[contains(text(),'Delete')]")).click();
	//click_commonMethod( "Delete Overflow Portgroup", "delete_overflowportgroup");
	////verifyExists( APT_VoiceLineObj.APT_VoiceLine.delete_overflowportgroup,"Delete Overflow Portgroup");
	//click( APT_VoiceLineObj.APT_VoiceLine.delete_overflowportgroup,"Delete Overflow Portgroup");
	
	waitforPagetobeenable();
	WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	if(DeleteAlertPopup.isDisplayed())
	{
		//click_commonMethod( "Delete", "delete_alertDelete");
		verifyExists( APT_VoiceLineObj.APT_VoiceLine.delete_alertDelete,"Delete");
		click( APT_VoiceLineObj.APT_VoiceLine.delete_alertDelete,"Delete");
		
		waitforPagetobeenable();
		verifysuccessmessage( "Overflow PortGroup successfully deleted.");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
	}
}

public String ExistingPortGroupRow(String prefix_dropdownvalue, String Edit_prefix_dropdownvalue, String routePriority_dropdownvalue, String voiceCPEdevicename, String edit_voiceCPEdevicename) throws InterruptedException {

	String RowID=null;
	List<WebElement> ExistingPort_prefix;
	//System.out.println("Voice CPEs"+voiceCPEdevicename);
	if(Edit_prefix_dropdownvalue.equalsIgnoreCase("null"))
	{
		//ExistingPort_prefix= findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_prefixvalue).replace("value", prefix_dropdownvalue);
		ExistingPort_prefix= webDriver.findElements(By.xpath("//div[@col-id='prefix'][text()='"+prefix_dropdownvalue+"']/parent::div"));
	}
	else
	{
		//ExistingPort_prefix= findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_prefixvalue).replace("value", Edit_prefix_dropdownvalue);
		ExistingPort_prefix= webDriver.findElements(By.xpath("//div[@col-id='prefix'][text()='"+Edit_prefix_dropdownvalue+"']/parent::div"));
	}
	int ExistingPort_prefixCount= ExistingPort_prefix.size();
	for(int i=0;i<ExistingPort_prefixCount;i++)
	{
		String ExistingPort_prefix_RowID= ExistingPort_prefix.get(i).getAttribute("row-id");
		//String RoutePriorityValue=findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_routepriorityvalue).replace("value", ExistingPort_prefix_RowID).getText();
		String RoutePriorityValue=webDriver.findElement(By.xpath("//div[@row-id='"+ExistingPort_prefix_RowID+"']//div[@col-id='routePriority']")).getText();
		//String VoiceCPEsValue=findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_voiceCPEsValue).replace("value", ExistingPort_prefix_RowID).getText();
		String VoiceCPEsValue=webDriver.findElement(By.xpath("//div[@row-id='"+ExistingPort_prefix_RowID+"']//div[@col-id='scl']")).getText();
		if(routePriority_dropdownvalue.contains(RoutePriorityValue))
		{
			RowID= ExistingPort_prefix_RowID;
			if(VoiceCPEsValue.contains(voiceCPEdevicename) || VoiceCPEsValue.contains(edit_voiceCPEdevicename))
			{
				RowID= ExistingPort_prefix_RowID;
				break;
			}
		}

	}
	return RowID;
}

public void AddDDIRange(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String lac_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LACValue");
	String mainnumber_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MainNumberValue");
	String rangestart_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStartValue");
	String rangeend_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeEndValue");
	String extensiondigits_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExtensionDigitsValue");
	String incomingrouting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IncomingRouting_Checkbox");
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String edit_prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Prefix_DropdownValue");
	
	String expectedTitleName= "Add DDI Range";
	
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.portgroup_header);
	
	compareText("DDI Range Header", APT_VoiceLineObj.APT_VoiceLine.ddirange_panelheader, "DDI Range");
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.addDDIRange_link,"Add DDI Range");
	click(APT_VoiceLineObj.APT_VoiceLine.addDDIRange_link,"Add DDI Range");
	
	waitforPagetobeenable();
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
	Iterator<String> itr = allwindows.iterator();
	while(itr.hasNext())
	{
		String childWindow = itr.next();
		if(!mainWindow.equals(childWindow)){
			webDriver.switchTo().window(childWindow);

			String actualTitlename=webDriver.switchTo().window(childWindow).getTitle();
			if(expectedTitleName.equals(actualTitlename)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Page title is displaying as "+ actualTitlename +" as expected");
				Report.LogInfo("INFO","Page title is displaying as "+ actualTitlename +" as expected","PASS");

			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Page title is mismatching. Expected title is: "+expectedTitleName +" ."
						+ "But actual title displaying is: "+ actualTitlename);
				Report.LogInfo("INFO","Page title is mismatching. Expected title is: "+expectedTitleName +" ."
						+ "But actual title displaying is: "+ actualTitlename,"FAIL");
			}

			compareText("Add DDI Range header", APT_VoiceLineObj.APT_VoiceLine.addDDIRangepage_header, "DDI Range");
			ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.addDDIRangepage_header);
			//click_commonMethod("ADD", "addDDI_addbutton");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.addDDI_addbutton,"ADD");
			click(APT_VoiceLineObj.APT_VoiceLine.addDDI_addbutton,"ADD");
			waitforPagetobeenable();
			ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.addDDI_addbutton);
			waitforPagetobeenable();
			//click_commonMethod("Remove", "addDDI_removebutton");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.addDDI_removebutton,"Remove");
			click(APT_VoiceLineObj.APT_VoiceLine.addDDI_removebutton,"Remove");
			
			waitforPagetobeenable();
			String CountrycodeValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addDDI_countrycode).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Country code is displayed as: "+CountrycodeValue);
			Report.LogInfo("INFO", "Country code is displayed as: "+CountrycodeValue,"PASS");
			addtextFields_commonMethod("LAC", APT_VoiceLineObj.APT_VoiceLine.addDDI_lactextfield, lac_value);
			addtextFields_commonMethod("Main Number", APT_VoiceLineObj.APT_VoiceLine.addDDI_mainnumber_textfield, mainnumber_Value);
			addtextFields_commonMethod("Range Start", APT_VoiceLineObj.APT_VoiceLine.addDDI_rangestart, rangestart_Value);
			addtextFields_commonMethod("Range End", APT_VoiceLineObj.APT_VoiceLine.addDDI_rangeend, rangeend_Value);
			waitforPagetobeenable();
			
			//click_commonMethod("Add Arrow", "addDDI_addArrow");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.addDDI_addArrow,"Add Arrow");
			click(APT_VoiceLineObj.APT_VoiceLine.addDDI_addArrow,"Add Arrow");
			
			addtextFields_commonMethod("Extension digits", APT_VoiceLineObj.APT_VoiceLine.addDDI_extensiondigits_textfield, extensiondigits_value);
			//addCheckbox_commonMethod(APT_VoiceLineObj.APT_VoiceLine.addDDI_incomingrouting_checkbox, "Activate Incoming Routing", incomingrouting_checkbox);
			editcheckbox_commonMethod(incomingrouting_checkbox, APT_VoiceLineObj.APT_VoiceLine.addDDI_incomingrouting_checkbox, "Activate Incoming Routing");

			if(edit_prefix_dropdownvalue.equalsIgnoreCase("null")) {
				selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.addDDI_portmappingprefix_dropdown, "Port Mapping Prefix", prefix_dropdownvalue);
			}
			else
			{
				selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.addDDI_portmappingprefix_dropdown, "Port Mapping Prefix", edit_prefix_dropdownvalue);
			}		

			ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.okbutton);
			
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
			click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
			
			
			//waitforPagetobeenable();

		}
	}

	webDriver.switchTo().window(mainWindow);

	waitforPagetobeenable();
	verifysuccessmessage("DDI Range successfully created.");
	waitforPagetobeenable();
	webDriver.navigate().refresh();

	//Verify Added DDI Range
	ScrolltoElement( APT_VoiceLineObj.APT_VoiceLine.portgroup_header);
	waitforPagetobeenable();

	//verify table column headers
	compareText("Country Code", APT_VoiceLineObj.APT_VoiceLine.countrycode_columnheader, "Country Code");
	compareText("LAC", APT_VoiceLineObj.APT_VoiceLine.lac_columnheader, "LAC");
	compareText("Main Number/Range Start-End", APT_VoiceLineObj.APT_VoiceLine.mainrange_columnheader, "Main Number/Range Start-End");
	compareText("Extension digits", APT_VoiceLineObj.APT_VoiceLine.extensiondigits_columnheader, "Extension digits");
	compareText("Port Group", APT_VoiceLineObj.APT_VoiceLine.portgroup_columnheader, "Port Group");
	compareText("Emerg Area", APT_VoiceLineObj.APT_VoiceLine.emergencyarea_columnheader, "Emerg Area");
	compareText("Incoming Routing", APT_VoiceLineObj.APT_VoiceLine.incomingrouting_columnheader, "Incoming Routing");
	compareText("Action", APT_VoiceLineObj.APT_VoiceLine.action_columnheader, "Action");

	//verify table values
	GetText("Country Code", APT_VoiceLineObj.APT_VoiceLine.countrycode_value);
	compareText("LAC", APT_VoiceLineObj.APT_VoiceLine.lac_value, lac_value);
	GetText("Main Number/Range Start-End", APT_VoiceLineObj.APT_VoiceLine.mainrange_value);
	compareText("Extension digits", APT_VoiceLineObj.APT_VoiceLine.extensiondigits_value, extensiondigits_value);
	if(edit_prefix_dropdownvalue.equalsIgnoreCase("null"))
	{
		compareText("Port Group", APT_VoiceLineObj.APT_VoiceLine.portgroup_value, prefix_dropdownvalue);
	}
	else
	{
		compareText("Port Group", APT_VoiceLineObj.APT_VoiceLine.portgroup_value, edit_prefix_dropdownvalue);
	}
	GetText("Emerg Area",  APT_VoiceLineObj.APT_VoiceLine.emergencyarea_value);
	if(incomingrouting_checkbox.equalsIgnoreCase("Yes") || incomingrouting_checkbox.equalsIgnoreCase("Null"))
	{
		//String IncomingRouting= findWebElement(APT_VoiceLineObj.APT_VoiceLine.incomingrouting_checkbox).getAttribute("checked");
		String IncomingRouting= getAttributeFrom(APT_VoiceLineObj.APT_VoiceLine.incomingrouting_checkbox, "checked");
		if(!IncomingRouting.equalsIgnoreCase("Null"))
		{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Incoming routing checkbox is checked as expected");
		Report.LogInfo("INFO","Incoming routing checkbox is checked as expected","PASS");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Incoming routing checkbox is not checked");
			Report.LogInfo("INFO","Incoming routing checkbox is not checked","PASS");
		}
}

	List<WebElement> DDIRangeLinks= findWebElements(APT_VoiceLineObj.APT_VoiceLine.ddi_links);
	int DDIRangeLinksCount= DDIRangeLinks.size();
	for(int i=0;i<DDIRangeLinksCount;i++)
	{
		String Link= DDIRangeLinks.get(i).getText();
		ExtentTestManager.getTest().log(LogStatus.PASS, ""+Link+" link is displaying under reseller panel");
		Report.LogInfo("INFO","Reseller link:"+ Link + " is displaying","PASS");
		
		waitforPagetobeenable();
	}
}

public void viewDDIRange(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String lac_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LACValue");
	String mainnumber_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MainNumberValue");
	String rangestart_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStartValue");
	String rangeend_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeEndValue");
	String extensiondigits_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExtensionDigitsValue");
	String psxconfig_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ViewDDI_PSXConfig_DropdownValue");
	
	
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.portgroup_header);
	waitforPagetobeenable();
	compareText("DDI Range Header", APT_VoiceLineObj.APT_VoiceLine.ddirange_panelheader, "DDI Range");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.ddi_viewlink,"View");
	click(APT_VoiceLineObj.APT_VoiceLine.ddi_viewlink,"View");
	
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.ddirange_header);
	compareText_fromActualvalue("DDI Range Header", APT_VoiceLineObj.APT_VoiceLine.ddirange_header, "DDI Range");

	GetText("Country Code", APT_VoiceLineObj.APT_VoiceLine.viewpage_countrycode);
	compareText("LAC", APT_VoiceLineObj.APT_VoiceLine.viewpage_lac, lac_value);
	compareText("Main Number", APT_VoiceLineObj.APT_VoiceLine.viewpage_mainnumber, "Main Number");
	compareText("Range Start", APT_VoiceLineObj.APT_VoiceLine.viewpage_rangestart, "Range Start");
	compareText("Range End", APT_VoiceLineObj.APT_VoiceLine.viewpage_rangeend, "Range End");
	compareText("Main Number Value", APT_VoiceLineObj.APT_VoiceLine.viewpage_mainnumber_Value, mainnumber_Value);
	compareText("Range Start Value", APT_VoiceLineObj.APT_VoiceLine.viewpage_rangestart_value, rangestart_Value);
	compareText("Range End Value", APT_VoiceLineObj.APT_VoiceLine.viewpage_rangeend_value, rangeend_Value);
	compareText("Extension Digits", APT_VoiceLineObj.APT_VoiceLine.viewpage_extensiondigits, extensiondigits_value);
	GetText("Emerg Area", APT_VoiceLineObj.APT_VoiceLine.viewpage_emergArea);
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton);
	GetText("Incoming Routing", APT_VoiceLineObj.APT_VoiceLine.viewpage_incomingrouting);
	//GetText("In GEO", "viewpage_InGEO");
	GetText("Port Mapping Prefix", APT_VoiceLineObj.APT_VoiceLine.viewpage_portmapping);
	compareText("Configuration Header", APT_VoiceLineObj.APT_VoiceLine.viewddipage_configheader, "Configuration");
	addDropdownValues_commonMethod("PSX Configuration", APT_VoiceLineObj.APT_VoiceLine.viewddipage_psxconfig_dropdown, psxconfig_dropdownvalue);
	//click_commonMethod("Execute", APT_VoiceLineObj.APT_VoiceLine.viewddipage_executebutton);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewddipage_executebutton,"Execute");
	click(APT_VoiceLineObj.APT_VoiceLine.viewddipage_executebutton,"Execute");
	waitforPagetobeenable();
	verifysuccessmessage("PSX sync started successfully. Please check the sync status of this Trunk Group. here ");
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton);
	//click_commonMethod("Back", "viewpage_backbutton");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton,"Back");
	click(APT_VoiceLineObj.APT_VoiceLine.viewpage_backbutton,"Back");
	
	waitforPagetobeenable();
}

public void editDDIRange(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	
	String edit_lac_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_LACValue");
	String mainnumber_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MainNumberValue");
	String edit_mainnumber_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_MainNumberValue");
	String edit_rangestart_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_RangeStartValue");
	String edit_rangeend_Value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_RangeEndValue");
	String edit_extensiondigits_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_ExtensionDigitsValue");
	String edit_incomingrouting_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_IncomingRouting_Checkbox");
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String edit_prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Prefix_DropdownValue");
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.portgroup_header);
	waitforPagetobeenable();
	compareText("DDI Range Header", APT_VoiceLineObj.APT_VoiceLine.ddirange_panelheader, "DDI Range");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.ddi_editlink,"Edit");
	click(APT_VoiceLineObj.APT_VoiceLine.ddi_editlink,"Edit");
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.ddirange_header);
	compareText("DDI Range Header", APT_VoiceLineObj.APT_VoiceLine.ddirange_header, "DDI Range");
	String CountrycodeValue= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addDDI_countrycode).getAttribute("value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Country code is displayed as: "+CountrycodeValue);
	edittextFields_commonMethod("LAC", APT_VoiceLineObj.APT_VoiceLine.addDDI_lactextfield, edit_lac_value);
	selectAndRemoveDDIPhoneNumbersFromRightDropdown("Phone Numbers", mainnumber_Value , APT_VoiceLineObj.APT_VoiceLine.editDDI_selectedphonenumbers, "editDDI_phonenumber_removearrow");
	edittextFields_commonMethod("Main Number", APT_VoiceLineObj.APT_VoiceLine.addDDI_mainnumber_textfield, edit_mainnumber_Value);
	edittextFields_commonMethod("Range Start", APT_VoiceLineObj.APT_VoiceLine.addDDI_rangestart, edit_rangestart_Value);
	edittextFields_commonMethod("Range End", APT_VoiceLineObj.APT_VoiceLine.addDDI_rangeend, edit_rangeend_Value);
	waitforPagetobeenable();
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.addDDI_addArrow,"Add Arrow");
	click(APT_VoiceLineObj.APT_VoiceLine.addDDI_addArrow,"Add Arrow");
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	edittextFields_commonMethod("Extension digits", APT_VoiceLineObj.APT_VoiceLine.addDDI_extensiondigits_textfield, edit_extensiondigits_value);
	editcheckbox_commonMethod(edit_incomingrouting_checkbox, APT_VoiceLineObj.APT_VoiceLine.addDDI_incomingrouting_checkbox, "Activate Incoming Routing");

	if(edit_prefix_dropdownvalue.equalsIgnoreCase("null")) {
		selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.addDDI_portmappingprefix_dropdown, "Port Mapping Prefix", prefix_dropdownvalue);
	}
	else
	{
		selectValueInsideDropdown(APT_VoiceLineObj.APT_VoiceLine.addDDI_portmappingprefix_dropdown, "Port Mapping Prefix", edit_prefix_dropdownvalue);
	}
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.okbutton);
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	click(APT_VoiceLineObj.APT_VoiceLine.okbutton,"OK");
	waitforPagetobeenable();
	waitforPagetobeenable();
	verifysuccessmessage("DDI Range successfully updated.");
	webDriver.navigate().refresh();
}

public void selectAndRemoveDDIPhoneNumbersFromRightDropdown(String labelname, String mainNumber, String xpath, String xpathForRemoveButton) {

	List<String> ls = new ArrayList<String>();

	try{
		List<WebElement> elements= findWebElements(xpath );
		int element_count= elements.size();

		if(element_count>=1) {

			//Print list of values inside Dropdown 
			for(WebElement a : elements) {
				ls.add(a.getText());
			}

			ExtentTestManager.getTest().log(LogStatus.PASS, "list of values displaying inside "+labelname+" available dropdown is: "+ls);
			Report.LogInfo("INFO","list of values dipslaying inside "+labelname+" dropdown is: "+ls,"PASS");

			//select value inside the dropdown     
			Thread.sleep(2000);
			for(int j=0; j<ls.size() ; j++) {
				//System.out.println("ls value "+ ls.get(j));
				if((ls.get(j)).contains(mainNumber))
				{
					elements.get(j).click();
					ExtentTestManager.getTest().log(LogStatus.PASS, elements.get(j) + " got selected" );
					Report.LogInfo("INFO",elements.get(j) + " got selected","PASS");
					waitforPagetobeenable();
					//WebElement removeButton=findWebElement(xpathForRemoveButton).replace("value", "<<");
					//Clickon(removeButton);
					//click(xpathForRemoveButton);
					webDriver.findElement(By.xpath("//button/span[text()='"+"<<"+"']")).click();
					
					ExtentTestManager.getTest().log(LogStatus.PASS, "clicked on remove '<<' button");
					Report.LogInfo("INFO","clicked on remove '<<' button","PASS");
					waitforPagetobeenable();
				}
			}

		}else {
			ExtentTestManager.getTest().log(LogStatus.INFO, "No values displaying under " + labelname + " dropdown");

			Report.LogInfo("INFO","No values displaying under " + labelname + " available dropdown","INFO");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under "+labelname + " available dropdown");
		Report.LogInfo("INFO","No values displaying under "+labelname + " available dropdown","FAIL");
	}
}

public void VerifyVoiceResiliency(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String backupnumber_checkbox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BackupNumber_Checkbox");
	String Obackupnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OBackupNumber_Value");
	String billingnumber_value=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BillingNumber_Value");

	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.ddirange_panelheader);
	waitforPagetobeenable();
	compareText( "Voice Resiliency header", APT_VoiceLineObj.APT_VoiceLine.voiceresiliency_header, "Voice Resiliency");
	GetText( "Backup Number", APT_VoiceLineObj.APT_VoiceLine.backupnumber_value);
	GetText( "OBackup Number", APT_VoiceLineObj.APT_VoiceLine.obackupnumber_value);
	GetText( "Billing Number", APT_VoiceLineObj.APT_VoiceLine.billingnumber_value);
	waitforPagetobeenable();
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.voiceresiliency_actiondropdown,"Action dropdown");
	//click(APT_VoiceLineObj.APT_VoiceLine.voiceresiliency_actiondropdown,"Action dropdown");
	
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.voiceresiliency_Edit,"Edit");
	click(APT_VoiceLineObj.APT_VoiceLine.voiceresiliency_Edit,"Edit");
	
	waitforPagetobeenable();
	WebElement VoiceResiliencyPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.voiceresiliency_popup);
	if(VoiceResiliencyPopup.isDisplayed())
	{
		editcheckbox_commonMethod( backupnumber_checkbox, APT_VoiceLineObj.APT_VoiceLine.backupnumber_checkbox, "Backup Number");
		if(backupnumber_checkbox.equalsIgnoreCase("Yes"))
		{
			addtextFields_commonMethod( "OBackup Number", APT_VoiceLineObj.APT_VoiceLine.obackupnumber_textfield, Obackupnumber_value);
			addtextFields_commonMethod( "Billing Number", APT_VoiceLineObj.APT_VoiceLine.billingnumber_textield, billingnumber_value);
		}
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.savebutton_voiceresiliency,"SAVE");
		click(APT_VoiceLineObj.APT_VoiceLine.savebutton_voiceresiliency,"SAVE");
		
		waitforPagetobeenable();
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit link is not working under Voice Resiliency panel");
		Report.LogInfo("INFO","Edit link is not working under Voice Resiliency panel","FAIL");
	}

	//ScrolltoElement( "ddirange_header");
	//Thread.sleep(1000);
	GetText( "Backup Number", APT_VoiceLineObj.APT_VoiceLine.backupnumber_value);
	GetText( "OBackup Number", APT_VoiceLineObj.APT_VoiceLine.obackupnumber_value);
	GetText( "Billing Number", APT_VoiceLineObj.APT_VoiceLine.billingnumber_value);
	waitforPagetobeenable();
}

public void VerifyPSXcommandExecution(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String PSXcommand_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXcommand_DropdownValue");
	
	String expectedTitleName="Execute Command on Device";

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.commandsexecution_header);
	waitforPagetobeenable();
	compareText("Commands Execution", APT_VoiceLineObj.APT_VoiceLine.commandsexecution_header, "Commands Execution");
	compareText("PSX Command", APT_VoiceLineObj.APT_VoiceLine.PSXcommand_label, "PSX Command");
	addDropdownValues_commonMethod("PSX Command", APT_VoiceLineObj.APT_VoiceLine.PSXcommand_dropdown, PSXcommand_dropdownvalue);
	
	//click_commonMethod("Execute", "PSXcommand_executebutton");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.PSXcommand_executebutton,"Execute");
	click(APT_VoiceLineObj.APT_VoiceLine.PSXcommand_executebutton,"Execute");
	waitforPagetobeenable();
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
	Iterator<String> itr = allwindows.iterator();
	while(itr.hasNext())
	{
		String childWindow = itr.next();
		if(!mainWindow.equals(childWindow)){
			webDriver.switchTo().window(childWindow);

			String actualTitlename=webDriver.switchTo().window(childWindow).getTitle();
			if(expectedTitleName.equals(actualTitlename)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Page title is displaying as "+ actualTitlename +" as expected");
				Report.LogInfo("INFO","Page title is displaying as "+ actualTitlename +" as expected","PASS");
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Page title is mismatching. Expected title is: "+expectedTitleName +" ."
						+ "But actual title displaying is: "+ actualTitlename);
				Report.LogInfo("INFO","Page title is mismatching. Expected title is: "+expectedTitleName +" ."
						+ "But actual title displaying is: "+ actualTitlename,"FAIL");
			}

			waitforPagetobeenable();
			webDriver.close();
		}
	}

	webDriver.switchTo().window(mainWindow);
}

public void VerifyGSXcommandExecution(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String GSXcommand_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"GSXcommand_DropdownValue");

	String expectedTitleName="Execute Command on Device";
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.commandsexecution_header);
	waitforPagetobeenable();
	compareText("Commands Execution", APT_VoiceLineObj.APT_VoiceLine.commandsexecution_header, "Commands Execution");
	compareText("GSX Command", APT_VoiceLineObj.APT_VoiceLine.GSXcommand_label, "GSX Command");
	waitforPagetobeenable();
	addDropdownValues_commonMethod("GSX Command", APT_VoiceLineObj.APT_VoiceLine.GSXcommand_dropdown, GSXcommand_dropdownvalue);
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.GSXcommand_dropdown,"Execute");
	//sendKeys(APT_VoiceLineObj.APT_VoiceLine.GSXcommand_dropdown,"Execute");
	
	//click_commonMethod("Execute", "GSXcommand_executebutton");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.GSXcommand_executebutton,"Execute");
	click(APT_VoiceLineObj.APT_VoiceLine.GSXcommand_executebutton,"Execute");
	waitforPagetobeenable();
	waitForAjax();
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
	Iterator<String> itr = allwindows.iterator();
	while(itr.hasNext())
	{
		String childWindow = itr.next();
		if(!mainWindow.equals(childWindow)){
			webDriver.switchTo().window(childWindow);

			String actualTitlename=webDriver.switchTo().window(childWindow).getTitle();
			if(expectedTitleName.equals(actualTitlename)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Page title is displaying as "+ actualTitlename +" as expected");
				Report.LogInfo("INFO","Page title is displaying as "+ actualTitlename +" as expected","PASS");
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Page title is mismatching. Expected title is: "+expectedTitleName +" ."
						+ "But actual title displaying is: "+ actualTitlename);
				Report.LogInfo("INFO","Page title is mismatching. Expected title is: "+expectedTitleName +" ."
						+ "But actual title displaying is: "+ actualTitlename,"FAIL");
			}

			waitforPagetobeenable();
			webDriver.close();
		}
	}

	webDriver.switchTo().window(mainWindow);
}


public void verifyConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String edit_voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");
	String Configuration_dropdownValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Configuration_dropdownValue");
	
	
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.configurationpanel_header);
	waitforPagetobeenable();
	((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");
	compareText("Configuration", APT_VoiceLineObj.APT_VoiceLine.configurationpanel_header, "Configuration");
	waitforPagetobeenable();
	VoiceCPEDevices_configDropdown("Voice CPE Devices", APT_VoiceLineObj.APT_VoiceLine.voiceCPEDevices_dropdown, voiceCPEdevicename, edit_voiceCPEdevicename);
	addDropdownValues_commonMethod("Configuration", APT_VoiceLineObj.APT_VoiceLine.configuration_dropdownvalue, Configuration_dropdownValue);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.generateconfig_link,"Generate Configuration");
	click(APT_VoiceLineObj.APT_VoiceLine.generateconfig_link,"Generate Configuration");
	waitforPagetobeenable();
	GetText("configuration Details", APT_VoiceLineObj.APT_VoiceLine.config_textarea);
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.saveconfig_link,"Save Configuration");
	click(APT_VoiceLineObj.APT_VoiceLine.saveconfig_link,"Save Configuration");
	waitforPagetobeenable();
	
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.executeconfigondevice_link,"Execute Configuration on Device");
	click(APT_VoiceLineObj.APT_VoiceLine.executeconfigondevice_link,"Execute Configuration on Device");
	waitforPagetobeenable();

}

public void VoiceCPEDevices_configDropdown(String labelname, String xpath, String expectedValueToAdd1, String expectedValueToAdd2) throws InterruptedException {
	boolean availability=false;
	try {  
		availability=findWebElement(xpath).isDisplayed();
		if(availability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
			Report.LogInfo("INFO",labelname + " dropdown is displaying","PASS");

			//Clickon(findWebElement("//div[label[text()='"+ labelname +"']]//div[text()='�']"));
			webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
			waitforPagetobeenable();

			//verify list of values inside dropdown
			List<WebElement> listofvalues = webDriver.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));
			int listofvaluesCount= listofvalues.size();
			ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside "+ labelname + " dropdown is:  ");
			Report.LogInfo("INFO"," List of values inside "+ labelname + " dropdown is:  ","PASS");

			for (WebElement valuetypes : listofvalues) {
				//Log.info("service sub types : " + valuetypes.getText());
				ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
				Report.LogInfo("INFO"," " + valuetypes.getText(),"PASS");
			}

			for(int i=0;i<listofvaluesCount;i++) {
				String VoiceCPEdropdownList= webDriver.findElement(By.xpath("//div[@class='sc-bxivhb kqVrwh']")).getText();
				if(VoiceCPEdropdownList.equalsIgnoreCase(expectedValueToAdd1))
				{
					waitforPagetobeenable();
					//SendKeys(findWebElement("//div[label[text()='"+ labelname +"']]//input"), expectedValueToAdd1);	
					webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd1);
					waitforPagetobeenable();

					//Clickon(findWebElement("(//div[@class='sc-ifAKCX oLlzc'][contains(text(),'"+ expectedValueToAdd1 +"')])[1]"));
					webDriver.findElement(By.xpath("(//div[@class='sc-ifAKCX oLlzc'][contains(text(),'"+ expectedValueToAdd1 +"')])[1]")).click();
					waitforPagetobeenable();
					break;
				}
				else
				{
					waitforPagetobeenable();
					//SendKeys(findWebElement("//div[label[text()='"+ labelname +"']]//input"), expectedValueToAdd2);	
					webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd2);
					waitforPagetobeenable();

					//Clickon(findWebElement("(//div[@class='sc-ifAKCX oLlzc'][contains(text(),'"+ expectedValueToAdd2 +"')])[1]"));
					webDriver.findElement(By.xpath("(//div[@class='sc-ifAKCX oLlzc'][contains(text(),'"+ expectedValueToAdd2 +"')])[1]")).click();
					waitforPagetobeenable();
				}

			}
			String actualValue=findWebElement("//label[text()='"+ labelname +"']/following-sibling::div//span").getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
			Report.LogInfo("INFO", labelname + " dropdown value selected as: "+ actualValue,"PASS");

		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
		}
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
		Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		//ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
		//Report.LogInfo("INFO"," NO value selected under "+ labelname + " dropdown","FAIL");
	}

}

public void deleteDDIRange() throws InterruptedException, IOException {

	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.ddirange_panelheader);
	waitforPagetobeenable();
	((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");
	waitforPagetobeenable();
	//click_commonMethod("Delete", "ddi_deletelink");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.ddi_deletelink,"Delete");
	click(APT_VoiceLineObj.APT_VoiceLine.ddi_deletelink,"Delete");
	waitforPagetobeenable();
	WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	if(DeleteAlertPopup.isDisplayed())
	{
		waitforPagetobeenable();
		//click_commonMethod("Delete", "deletebutton");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete");
		
		verifysuccessmessage("DDI Range successfully deleted.");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
	}
}

public void deletePortGroup(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	
	String sid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prefix_DropdownValue");
	String edit_prefix_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_Prefix_DropdownValue");
	String routePriority_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutePriority_DropdownValue");
	String voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VoiceCPEDeviceName");
	String edit_voiceCPEdevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_VoiceCPEDeviceName");
	
	
	webDriver.navigate().refresh();
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.disasterrecoverystatus_header);
	waitforPagetobeenable();
	//String AddedPort_RowID= DeletePortGroupRow(prefix_dropdownvalue, edit_prefix_dropdownvalue, routePriority_dropdownvalue, voiceCPEdevicename, edit_voiceCPEdevicename);

	//WebElement AddedPort_Checkbox= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox).replace("value", AddedPort_RowID);
	//Clickon(AddedPort_Checkbox);
	//click(APT_VoiceLineObj.APT_VoiceLine.addedport_checkbox);
	//webDriver.findElement(By.xpath("//div[@row-id='"+AddedPort_RowID+"']//div//span[contains(@class,'unchecked')]")).click();
	if(!edit_prefix_dropdownvalue.equalsIgnoreCase("null"))
	{
		List<WebElement> ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+edit_prefix_dropdownvalue+"')]"));
		webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+edit_prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'Delete')]")).click();
		
	}
	else{
	List<WebElement> ExistingPort_prefix=webDriver.findElements(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]"));
	webDriver.findElement(By.xpath("(//div[text()='Port Group']//following::table)[1]//tr//td[contains(text(),'"+prefix_dropdownvalue+"')]/following-sibling::td//a[contains(text(),'Delete')]")).click();
	}
	waitforPagetobeenable();
	//click_commonMethod("Action Dropdown", "portgroup_Actiondropdown");
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	//click(APT_VoiceLineObj.APT_VoiceLine.portgroup_Actiondropdown,"Action Dropdown");
	
	//click_commonMethod("Delete Portgroup", "delete_portgroup");
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.delete_portgroup,"Delete Portgroup");
	//click(APT_VoiceLineObj.APT_VoiceLine.delete_portgroup,"Delete Portgroup");
	
	WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	if(DeleteAlertPopup.isDisplayed())
	{
		waitforPagetobeenable();
		//click_commonMethod("Delete", "delete_alertDelete");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.delete_alertDelete,"Delete");
		click(APT_VoiceLineObj.APT_VoiceLine.delete_alertDelete,"Delete");
		waitforPagetobeenable();
		verifysuccessmessage("Port Group successfully deleted.");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
	}

	scrollIntoTop();
	waitforPagetobeenable();
	webDriver.findElement(By.xpath("//ol[@class='breadcrumb']//a[contains(text(),'"+sid+"')]")).click();
	Report.LogInfo("INFO","Clicked on BreadCrum","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BreadCrum");
}

public String DeletePortGroupRow(String prefix_dropdownvalue, String edit_prefix_dropdownvalue, String routePriority_dropdownvalue, String voiceCPEdevicename, String edit_voiceCPEdevicename) throws InterruptedException {

	String RowID=null;
	List<WebElement> ExistingPort_prefix;
	if(!edit_prefix_dropdownvalue.equalsIgnoreCase("null")) {
		//ExistingPort_prefix= findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_prefixvalue).replace("value", edit_prefix_dropdownvalue);
		ExistingPort_prefix= webDriver.findElements(By.xpath("//div[@col-id='prefix'][text()='"+edit_prefix_dropdownvalue+"']/parent::div"));
		
	}
	else
	{
		//ExistingPort_prefix= findWebElements(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_prefixvalue).replace("value", prefix_dropdownvalue);
		ExistingPort_prefix=webDriver.findElements(By.xpath("//div[@col-id='prefix'][text()='"+prefix_dropdownvalue+"']/parent::div"));
		
	}
	int ExistingPort_prefixCount= ExistingPort_prefix.size();
	for(int i=0;i<ExistingPort_prefixCount;i++)
	{
		String ExistingPort_prefix_RowID= ExistingPort_prefix.get(i).getAttribute("row-id");
		//String RoutePriorityValue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_routepriorityvalue).replace("value", ExistingPort_prefix_RowID).getText();
		String RoutePriorityValue=webDriver.findElement(By.xpath("//div[@row-id='"+ExistingPort_prefix_RowID+"']//div[@col-id='routePriority']")).getText();
		
		//String VoiceCPEsValue=findWebElement(APT_VoiceLineObj.APT_VoiceLine.portgrouptable_voiceCPEsValue).replace("value", ExistingPort_prefix_RowID).getText();
		String VoiceCPEsValue=webDriver.findElement(By.xpath("//div[@row-id='"+ExistingPort_prefix_RowID+"']//div[@col-id='scl']")).getText();
		
		if(routePriority_dropdownvalue.contains(RoutePriorityValue))
		{
			RowID= ExistingPort_prefix_RowID;
			if(VoiceCPEsValue.contains(voiceCPEdevicename) || VoiceCPEsValue.contains(edit_voiceCPEdevicename))
			{
				RowID= ExistingPort_prefix_RowID;
				break;
			}
		}

	}
	return RowID;
}

public void deleteVoiceCPEDevice() throws InterruptedException, IOException {

	
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.voiceCPEdevice_header);
	waitforPagetobeenable();

	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingvoicecpe_devicegrid).isDisplayed())
	{
		List<WebElement> AddedVoiceCPEDevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedvoiceCPEdevicename);
		//System.out.println(AddedVoiceCPEDevicesList);
		int AddedVoiceCPEDevicesCount= AddedVoiceCPEDevicesList.size();

		for(int i=0;i<AddedVoiceCPEDevicesCount;i++) {
			String AddedVoiceCPEDeviceNameText= AddedVoiceCPEDevicesList.get(i).getText();
			//System.out.println(AddedVoiceCPEDeviceNameText);
			String AddedVoiceCPEDevice_SNo= AddedVoiceCPEDeviceNameText.substring(0, 1);

			//WebElement AddedVoiceCPEDevice_DeleteLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addedvoicecpedevice_deletelink).replace("value", AddedVoiceCPEDevice_SNo);
			//Clickon(AddedVoiceCPEDevice_DeleteLink);
			//click(APT_VoiceLineObj.APT_VoiceLine.addedvoicecpedevice_deletelink);
			//webDriver.findElement(By.xpath("//b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//b[contains(text(),'"+AddedVoiceCPEDevice_SNo+"')]/parent::span/following-sibling::span[@class='deviceCrudRight']//span//a[text()='Delete']")).click();
			webDriver.findElement(By.xpath("//b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//b[contains(text(),'"+AddedVoiceCPEDevice_SNo+"')]/following::span//a[text()='Delete']")).click();
			waitforPagetobeenable();
			WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
			if(DeleteAlertPopup.isDisplayed())
			{
				waitforPagetobeenable();
				//click_commonMethod("Delete Voice CPE Device", "deletebutton");
				verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Voice CPE Device");
				click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Voice CPE Device");
				verifysuccessmessage("Device deleted successfully");
			}
			else
			{
				Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
		}
	}
}

public void deleteCPEDevice() throws InterruptedException, IOException {

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	List<WebElement> AddedCPEDevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addedCPEdevicename);
	//System.out.println(AddedCPEDevicesList);
	int AddedCPEDevicesCount= AddedCPEDevicesList.size();

	for(int i=0;i<AddedCPEDevicesCount;i++) {
		String AddedCPEDeviceNameText= AddedCPEDevicesList.get(i).getText();
		//System.out.println(AddedCPEDeviceNameText);
		String AddedCPEDevice_SNo= AddedCPEDeviceNameText.substring(0, 1);

		//WebElement AddedCPEDevice_DeleteLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.deleteCPEdeviceLink).replace("value", AddedCPEDevice_SNo);
		//Clickon(AddedCPEDevice_DeleteLink);
		//click(APT_VoiceLineObj.APT_VoiceLine.deleteCPEdeviceLink);
		//b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//a/span[text()='Delete']
		//webDriver.findElement(By.xpath("//b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//span//b[contains(text(),'"+AddedCPEDevice_SNo+"')]/following::span//a[text()='Delete']")).click();
		webDriver.findElement(By.xpath("//b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//span//a[text()='Delete']")).click();
		waitforPagetobeenable();
		WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
		if(DeleteAlertPopup.isDisplayed())
		{
			waitforPagetobeenable();
			//click_commonMethod("Delete CPE Device", "deletebutton");
			verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete CPE Device");
			click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete CPE Device");
			verifysuccessmessage("Device deleted successfully");
		}
		else
		{
			Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
		}
	}
}

public void deleteResilientTrunk() throws Exception {

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	//WebElement getTrunkRow=webDriver.findElement(By.xpath("//div[div[span[b[text()='Trunks ']]]]//following-sibling::div//div[text()='"+primarytrunkGroupname_Resilient+"']"));
	//safeJavaScriptClick(getTrunkRow);
	waitForAjax();
	//webDriver.findElement(By.xpath("(//div[text()='Trunk Group/Site Orders']/following::table//tr)/following::td[contains(text(),'"+Edit_primarytrunkGroupname+"')]/following::a[contains(text(),'Delete')]")).click();
	webDriver.findElement(By.xpath("((//div[text()='Trunk Group/Site Orders']/following::table//tr)/following::a[contains(text(),'Delete')])[2]")).click();
	//click on Action dropdown under Trunk Panel	
	//WebElement trunkPanel_actionDropdown=findWebElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown).replace("value", siteOrderName);
	//safeJavaScriptClick(trunkPanel_actionDropdown);
	//click(APT_VoiceLineObj.APT_VoiceLine.trunkPanel_ActionDropdown);
	//webDriver.findElement(By.xpath("//div[div[span[text()='"+siteOrderName+"']]]//following-sibling::div//button[text()='Action']")).click();
	//delete trunk link
	//click_commonMethod("Delete Trunk", "deletetrunk_link" );
	//verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletetrunk_link,"Delete Trunk");
	//click(APT_VoiceLineObj.APT_VoiceLine.deletetrunk_link,"Delete Trunk");

	waitforPagetobeenable();
	WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	if(DeleteAlertPopup.isDisplayed())
	{
		waitforPagetobeenable();
	//	click_commonMethod("Delete Trunk", "deletebutton");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Trunk");
		click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Trunk");
		verifysuccessmessage("Trunk deleted successfully");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
	}
}

public void deleteTrunkSiteOrder(String siteOrderNumber2) throws Exception {
	
	//String siteOrderName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkGroupOrderNumber");

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.trunkPanel);
	waitforPagetobeenable();

	//WebElement deletelink=webDriver.findElement(By.xpath("//div[div[span[text()='"+ siteOrderName +"']]]//span[text()='Delete']"));
	
	//Clickon(deletelink);
	webDriver.findElement(By.xpath("//div[div[span[text()='"+siteOrderNumber2+"']]]//a[text()='Delete']")).click();

	waitforPagetobeenable();
	WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	if(DeleteAlertPopup.isDisplayed())
	{
		waitforPagetobeenable();
		//click_commonMethod("Delete Trunk Group", "deletebutton");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Trunk Group");
		click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Trunk Group");
		verifysuccessmessage("Trunk Group deleted successfully");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
	}
}


public void deleteASRDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
	
	String imspoplocation_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IMSPopLocation_DropdownValue");
	waitforPagetobeenable();
	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.asrdevice_header);
	if(findWebElement(APT_VoiceLineObj.APT_VoiceLine.existingdevicegrid).isDisplayed())
	{
		List<WebElement> addeddevicesList= findWebElements(APT_VoiceLineObj.APT_VoiceLine.addeddevicename);
		//System.out.println(addeddevicesList);
		waitforPagetobeenable();
		waitforPagetobeenable();
		int AddedDevicesCount= addeddevicesList.size();

		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			//System.out.println(AddedDeviceNameText);
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			String IMSPopLocation_Code= imsPopLocationValue(imspoplocation_dropdownvalue);
			if(AddedDeviceNameText.contains(IMSPopLocation_Code))
			{
				//WebElement AddedDevice_DeleteLink= findWebElement(APT_VoiceLineObj.APT_VoiceLine.addeddevice_Deletefromservicelink).replace("value", AddedDevice_SNo);
				//Clickon(AddedDevice_DeleteLink);
				//click(APT_VoiceLineObj.APT_VoiceLine.addeddevice_Deletefromservicelink);
				webDriver.findElement(By.xpath("(//div[text()='ASR Device']/following::tr)//b[contains(text(),'"+AddedDevice_SNo+"')]/following::a[contains(text(),'Delete from Service')]")).click();
				waitforPagetobeenable();
				WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopupASR);
				waitforPagetobeenable();
				waitforPagetobeenable();
				if(DeleteAlertPopup.isDisplayed())
				{
					waitforPagetobeenable();
					//click_commonMethod("Delete ASR Device", "deletebutton");
					verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete ASR Device");
					click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete ASR Device");
					waitforPagetobeenable();
					verifysuccessmessage("ASR Device deleted successfully");
					break;
				}
				else
				{
					Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
				}
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Device not added for "+IMSPopLocation_Code+ " location");
				Report.LogInfo("INFO","Device not added for "+IMSPopLocation_Code+ " location","FAIL");
			}

		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		Report.LogInfo("INFO","No Device added in grid","FAIL");
	}
}

public void deleteService() throws InterruptedException, IOException {

	waitforPagetobeenable();
	waitforPagetobeenable();
	ScrolltoElement(APT_VoiceLineObj.APT_VoiceLine.orderpanelheader);
	waitforPagetobeenable();
	//click_commonMethod("Action dropdown", "serviceactiondropdown");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceactiondropdown,"Action dropdown");
	
	//click_commonMethod("Delete Service", "delete");
	verifyExists(APT_VoiceLineObj.APT_VoiceLine.delete,"Delete Service");
	click(APT_VoiceLineObj.APT_VoiceLine.delete,"Delete Service");
	waitforPagetobeenable();
	waitforPagetobeenable();
	WebElement DeleteAlertPopup= findWebElement(APT_VoiceLineObj.APT_VoiceLine.delete_alertpopup);
	if(DeleteAlertPopup.isDisplayed())
	{
		waitforPagetobeenable();
		//click_commonMethod("Delete ASR Device", "deletebutton");
		verifyExists(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Service Device");
		click(APT_VoiceLineObj.APT_VoiceLine.deletebutton,"Delete Service Device");
		waitforPagetobeenable();
		verifysuccessmessage("Service successfully deleted. ");
	}
	else
	{
		Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
		
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
	}
}


public String imsPopLocationValue(String imspoplocation_dropdownvalue) {
	String Location_code=null;
	if(imspoplocation_dropdownvalue.equalsIgnoreCase("Bangalore-1"))
	{
		Location_code= "BLR";
	}
	else if(imspoplocation_dropdownvalue.equalsIgnoreCase("Paris-1"))
	{
		Location_code= "PAR";
	}
	else if(imspoplocation_dropdownvalue.equalsIgnoreCase("Frankfurt-1"))
	{
		Location_code= "FRA";
	}
	return Location_code;
}

public void searchorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException

{
	waitforPagetobeenable();
	waitforPagetobeenable();
	String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.ManageCustomerServiceLink, " Manage Customer Service Link");
	mouseMoveOn(APT_VoiceLineObj.APT_VoiceLine.ManageCustomerServiceLink);

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.searchorderlink, "search order link");
	click(APT_VoiceLineObj.APT_VoiceLine.searchorderlink, "search order link");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.servicefield, "service field");
	sendKeys(APT_VoiceLineObj.APT_VoiceLine.servicefield, sid);

	click(APT_VoiceLineObj.APT_VoiceLine.searchbutton, "searchbutton");
	// click(searchbutton,"searchbutton");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.serviceradiobutton, "service radio button");
	click(APT_VoiceLineObj.APT_VoiceLine.serviceradiobutton, "service radio button");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.searchorder_actiondropdown, "search order actiodropdown");
	click(APT_VoiceLineObj.APT_VoiceLine.searchorder_actiondropdown, "search order linksearch order actiodropdown");

	verifyExists(APT_VoiceLineObj.APT_VoiceLine.view, "view");
	click(APT_VoiceLineObj.APT_VoiceLine.view, "view");

}


}
