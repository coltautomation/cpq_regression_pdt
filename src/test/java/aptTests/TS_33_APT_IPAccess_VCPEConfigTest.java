package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPAccessConfig;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_IPAccessResilientConfig;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_IPAccess_VCPEConfig;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_33_APT_IPAccess_VCPEConfigTest extends SeleniumUtils {
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	// APT_MCN_Create_Customer CreateCust = new APT_MCN_Create_Customer();
	// APT_MCS_CreateFirewallDevice CreateFirewall = new
	// APT_MCS_CreateFirewallDevice();
	APT_IPAccessNoCPEHelper noCPE = new APT_IPAccessNoCPEHelper();
	APT_IPTransit ipTransit = new APT_IPTransit();
	APT_IPAccessResilientConfig ResilentConfg = new APT_IPAccessResilientConfig();
	APT_IPAccessConfig IPConfige = new APT_IPAccessConfig();
	APT_IPAccess_VCPEConfig IPVCPE = new APT_IPAccess_VCPEConfig();

	APT_Login Login = new APT_Login();

	public static String DeviceNameValue=null;
	public static String VendorModelValue=null;
	public static String ManagementAddress=null;

	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test()
	public void APT_IPAccessResilientConfigTest(String scriptNo, String dataSetNo, String dataSheet,
			String ScenarioName) throws Exception {
		// String Name = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "Name");
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {
			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			// setup();
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			Report.LogInfo("INFO", "TC - 1 - =====CreateCustomer_Configue Test====", "PASS");
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
				noCPE.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
				Report.LogInfo("INFO", "=====selectExistingCustomer_Resilient====", "PASS");

				noCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			Report.LogInfo("INFO", "TC - 2 - =====verifyNewOrderCreationOrExistingOrderSelectionConfigueTest====",
					"PASS");
			ResilentConfg.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 3 - =====ServiceTypeAndNetworkConfigurationSelection_Configue Test====",
					"PASS");
			noCPE.verifyServicetypeAndNetworkConfigurationSelection(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 4 - =====verifyServiceCreation_Configue====", "PASS");
			IPVCPE.verifyServiceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 5 - =====verifyCustomerDetailsInformation_Configue====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 6 - =====VerifyEditOrderChangeOrderFunction_Resilient====", "PASS");
			noCPE.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 7 - =====VerifyCreatedServiceInformationInViewServicePage_Resilient====",
					"PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 9 - =====VerifyServiceActionsListFunction_ConfigueTest====", "PASS");
			IPVCPE.EditServiceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyManageSubnets();
			ResilentConfg.verifyDump();
			
			Report.LogInfo("INFO", "TC - 10 - =====VerifySL2TechnologyValueTest====", "PASS");

			//String L2TechnologyValue=map.get("L2Technology_DropdownValue");
			String L2TechnologyValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"L2Technology_DropdownValue");
			
			if(L2TechnologyValue.equalsIgnoreCase("Atrica"))
			{
				Report.LogInfo("INFO", "=====verifyL2Technology====", "PASS");
				IPVCPE.verifyL2Technology(testDataFile, dataSheet, scriptNo, dataSetNo);
				Report.LogInfo("INFO", "=====verifyViewL2Circuit====", "PASS");
				IPVCPE.verifyViewL2Circuit(testDataFile, dataSheet, scriptNo, dataSetNo);
				Report.LogInfo("INFO", "=====verifySiteA====", "PASS");
				IPVCPE.verifySiteA(testDataFile, dataSheet, scriptNo, dataSetNo);
				Report.LogInfo("INFO", "=====verifySiteB====", "PASS");
				IPVCPE.verifySiteB(testDataFile, dataSheet, scriptNo, dataSetNo);
				Report.LogInfo("INFO", "=====verifyEditL2Circuit====", "PASS");
				IPVCPE.verifyEditL2Circuit(testDataFile, dataSheet, scriptNo, dataSetNo);	
			}
			else if(L2TechnologyValue.equalsIgnoreCase("Actelis"))
			{
				IPVCPE.verifyActelisL2Technology(testDataFile, dataSheet, scriptNo, dataSetNo);	
				IPVCPE.verifyView_ActelisL2Circuit(testDataFile, dataSheet, scriptNo, dataSetNo);		
				ResilentConfg.verifyAddDSLAMandHSLlink();
				ResilentConfg.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);
				IPVCPE.verifyEdit_ActelisL2Circuit(testDataFile, dataSheet, scriptNo, dataSetNo);							
			}
			else if(L2TechnologyValue.equalsIgnoreCase("Overture"))
			{
				IPVCPE.verifyOvertureL2Technology(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyViewOvertureL2Circuit(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA_AddEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA_ViewEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA_EditEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_AddEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_ViewEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_EditEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verify_EditAccedianL2Technology(testDataFile, dataSheet, scriptNo, dataSetNo);							

			}
			else if(L2TechnologyValue.equalsIgnoreCase("Accedian"))
			{
				IPVCPE.verifyAccedianL2Technology(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyViewAccedianL2Circuit(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA_AddEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA_ViewEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteA_EditEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_AddEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_ViewEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_EditEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_AddEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_ViewEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verifyOvertureSiteB_EditEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);							
				IPVCPE.verify_EditAccedianL2Technology(testDataFile, dataSheet, scriptNo, dataSetNo);							
			}
			
			Report.LogInfo("INFO", "TC - 11 - =====VerifyAddDeleteExistingPEDevice_Configue====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.addExistingPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyExistingDevice_ViewDevicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			IPVCPE.deleteDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 12 - =====VerifyAddDeleteNewPEDevice_Configue====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.navigateToAddNewDevicepage_PE();
			noCPE.addNewPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyViewpage_Devicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyEditDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			
			noCPE.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			DeviceNameValue= IPVCPE.DeviceName();
			VendorModelValue= IPVCPE.VendorModel();
			ManagementAddress= IPVCPE.ManagementAddress();
			
			Report.LogInfo("INFO", "TC - 13 - =====verifyRouterToolsServicenameTag====", "PASS");
				noCPE.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			

			//if(VendorModelValue.contains("Juniper"))
			if("Juniper".contains("Juniper"))
			{
				Report.LogInfo("INFO", "TC - 14 - =====verify ADD Interface ====", "PASS");
				noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.VerifyAddInterface_JuniperVendor(testDataFile, dataSheet, scriptNo, dataSetNo);
				Report.LogInfo("INFO", "TC - 14 - =====verify Edit Interface ====", "PASS");
				IPVCPE.VerifyEditInterface_JuniperVendor(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			Report.LogInfo("INFO", "TC - 15 - =====VerifyFetchDeviceInterfaceFunction_PE_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyFetchDeviceInterface_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 16 - =====verifyInterfaceConfigueHistory====", "PASS");
			noCPE.verifyInterfaceConfigHistory(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 17 - =====verifySelectInterfaces_ColtTotal====", "PASS");
			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RemoveInterface_Selection");
			String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"AddInterface_Selection");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.selectInterfacelinkforDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (RemoveInterface_Selection.equalsIgnoreCase("yes")) {
				IPVCPE.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				System.out.println("interfaces are not removed");
			}
			if (AddInterface_Selection.equalsIgnoreCase("yes")) {
				IPVCPE.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				System.out.println("Interfaces are not added");
			}
			
			Report.LogInfo("INFO", "TC - 18 - =====verifyManageService====", "PASS");
			ResilentConfg.verifyManageService1(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 19 - =====DeleteDevice====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			IPVCPE.deleteDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 20 - =====DeleteDevice====", "PASS");
			IPVCPE.deleteL2Technology();							

			Report.LogInfo("INFO", "TC - 21 - =====deleteServiceFunction_====", "PASS");
			noCPE.deleteSevice1(testDataFile, dataSheet, scriptNo, dataSetNo);

		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

}
