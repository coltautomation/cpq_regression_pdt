 	package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.exploreFunctions.ExploreLoginPage;

public class TS_05_Nearnet_Order_Completion extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	GlobalVariables g = new GlobalVariables();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testNearnetOrderCompletion(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
						
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			String Flow_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
			if(ProductName.equalsIgnoreCase("Wave") && Flow_Type.equalsIgnoreCase("ManualNearnet")) {
				
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Created");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method to add Legal and Technical Contact Info
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
				sResult = Approvals.SwitchCPQUser("SE_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method to configure the product
				sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
			}

			if (Flow_Type.equalsIgnoreCase("ManualNearnet")) {
				
						AddProduct.verifyQuoteStage("Waiting for BCP");
						CPQLogin.CPQLogout();
						
						//Login with CPQ SE user
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);		
					
				//		Calling the Below method to perform C4C Login
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin();
						
				//		calling the below method to navigate to quotes from c4c main page
						AddProduct.navigateQuotesFromHomepage();
						
				//		calling the below method to search the quote from c4c
						String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
						AddProduct.searchQuoteC4C(Quote_ID);	
							
				//		calling the below method reconfigure the product
						AddProduct.clickProductConfigurationBtn(ProductName);
						
				//		calling the below method to update and save the product in cpq
						AddProduct.updateSaveProductCPQ("Save");
						
					}
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				            
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
			AddProduct.verifyQuoteStage("Priced");
									
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");			
			
			//Revise Quote
			String Revise_Quote = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Revise_Quote");
			String BandwidthValue = DataMiner.fngetcolvalue(testDataFile, dataSheet,scriptNo, dataSetNo, "Service_Bandwidth1");		
			String Presentation_Interface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Interface1");
			String Connector_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Connector_Type1");
			if(Revise_Quote.equalsIgnoreCase("Yes"))
			{
				AddProduct.reviseQuote();	
				AddProduct.verifyQuoteStage("Priced");
				AddProduct.clickProductConfigurationBtn(ProductName);
			
			//Reconfigure the Quote with different values
			
				AddProduct.reconfigureProduct(ProductName,BandwidthValue,Presentation_Interface,Connector_Type);
				AddProduct.updateSaveProductCPQ("Save");
				
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
			}
			
			
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			
			//String CPE_Site_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Site_BCN");
			//Submit.addBillingInformation("CPE Solutions Site", CPE_Site_BCN);
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);


		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}		
}
