package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_NGIN;

public class TS_23_APT_NGINTest extends SeleniumUtils {
	
	APT_NGIN NGIN = new APT_NGIN();
	APT_Login Login = new APT_Login();

	// ReadExcelFile read = new ReadExcelFile();
	// ReusableFunctions Reusable = new ReusableFunctions();
	// String dataFileName = "APT_IPACCESS";
	// File path = new File("./src/test/resources/"+dataFileName);
	// String testDataFile = path.toString();
	// String dataSheet = "NoCPE";
	// String scriptNo = "1";
	// String dataSetNo = "1";

	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	public void NGINService(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomerSelection");
			
			
			 if(newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
	              
	     
	              NGIN.createcustomer(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
	              String CustomerName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
	              ExtentTestManager.endTest();
	              
	              NGIN.selectCustomertocreateOrder(CustomerName);
	              ExtentTestManager.endTest();
	        }
	        else if(newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
	              
	        	  String  CustomerName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"existingCustomer");
	        	  NGIN.selectCustomertocreateOrder(CustomerName);
	        	 
	              
	        }
	    		
			 NGIN.createorderservice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						
			 NGIN.verifyselectservicetype(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			 NGIN.verifyservicecreation(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
			
			 NGIN.verifyCustomerDetailsInformation(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			 NGIN.verifyUserDetailsInformation(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			 NGIN.verifyservicepanelInformationinviewservicepage(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			 NGIN.verifyManagementOptionspanel(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
		
			 NGIN.verifyEditService(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			 NGIN.verifyManageService(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			 NGIN.searchservice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			 NGIN.verifySynchronizeLink();
			 
       	  String  BulkInterface=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BulkInterface");

			if((BulkInterface).equalsIgnoreCase("Yes"))
			{
				NGIN.verifyBulkInterface(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			}
			
	      /*  String  ResellerAdministration=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"ResellerAdministration");

			if(!(ResellerAdministration).equalsIgnoreCase("No")) {
			
			NGIN.verifyResellerpanel();
			
			NGIN.AddReseller(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			NGIN.verify_ViewReseller(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			NGIN.verify_EditReseller(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			}
			
	        String  CustomerAdministration=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CustomerAdministration");

			if(!(CustomerAdministration).equalsIgnoreCase("No")) {
			NGIN.verifyCustomerpanel();
			
			NGIN.AddCustomer(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			NGIN.verify_ViewCustomer(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			NGIN.verify_EditCustomer(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			}
			/*
	        String  SANAdministration=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"SANAdministration");

			if(!(SANAdministration).equalsIgnoreCase("No")) {
			NGIN.verifySANpanel();
			
			NGIN.AddSAN(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			NGIN.verifyViewSAN(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			NGIN.verifyEditSAN(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			//APT_NGIN.get().verifyPortIn("nginservice", map.get("CustomerNameValue"), map.get("SANNumberValue"), map.get("SelectSANSearchType"), map.get("PortInNumber"), map.get("CancelPorting"));
			//APT_NGIN.get().verifyPortOut("nginservice", map.get("CustomerNameValue"), map.get("SANNumberValue"), map.get("SelectSANSearchType"), map.get("PortOutNumber"));
			NGIN.verifySANMove(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
			NGIN.verifyBulkMove(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			NGIN.verifyManageAddnlFRC(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			}
			*/
			
			NGIN.verifyAllDeleteOperations(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			ExtentTestManager.endTest();
			
			
		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

}
