package pageObjects.aptObjects;

public class searchForDeviceObj {
	public static class searchDevice {

		public static final String mcnlink = "@xpath=//b[contains(text(),'MANAGE COLT')]";
		public static final String searchForDeviceLink = "@xpath=//li[contains(text(),'Search for Device')]";
		public static final String deviceName_textField = "@xpath=//label[text()='Device Name']/parent::div//input";
		public static final String managementAddress = "@xpath=//label[text()='Management/Interface Address']/parent::div//input";
		public static final String searchCity = "@xpath=//label[text()='Search City']/parent::div//input";
		public static final String searchCO = "@xpath=//label[text()='Search CO']/parent::div//input";
		public static final String routerId_tetField = "@xpath=//label[text()='Router Id']/parent::div//input";
		public static final String vendorModelDropdown1 = "@xpath=//select[@id='availableVendorModel']//option[@label='";
		public static final String vendorModelDropdown2 = "']";
		public static final String vendorModelDropdown = "@xpath=//select[@id='availableVendorModel']//option";

		public static final String deviceTypeDropdown1 = "@xpath=//select[@id='availableDeviceTypes']//option[@label='";
		public static final String deviceTypeDropdown2 = "']";
		public static final String deviceTypeDropdown = "@xpath=//select[@id='availableDeviceTypes']//option";

		public static final String cityDropdown1 = "@xpath=//select[@id='availableCity']//option[@label='";
		public static final String cityDropdown2 = "']";
		public static final String cityDropdown = "@xpath=//select[@id='availableCity']//option";

		public static final String avaialableCOvalue1 = "@xpath=//select[@id='availableCOTypes']//option[@label='";
		public static final String avaialableCOvalue2 = "']";
		public static final String avaialableCOvalue = "@xpath=//select[@id='availableCOTypes']//option";

		public static final String addButton_vendorModel = "@xpath=(//div[div[label[text()='Vendor/Model']]]//following-sibling::div//span[text()='>>'])[1]";
		public static final String addButton_CO = "@xpath=//div[div[label[text()='CO']]]//following-sibling::div//span[text()='>>']";
		public static final String addButton_city = "@xpath=(//div[div[label[text()='City']]]//following-sibling::div//span[text()='>>'])[1]";
		public static final String addButton_deviceType = "@xpath=(//div[div[label[text()='Device Type']]]//following-sibling::div//span[text()='>>'])[1]";
		public static final String selectedVendorModel = "@xpath=//select[@id='selectedVendorModel']//option";
		public static final String selectedDeviceType = "@xpath=//select[@id='selectedDeviceTypes']//option";
		public static final String selectedCity = "@xpath=//select[@id='selectedCity']//option";
		public static final String selectedCOtypes = "@xpath=//select[@id='selectedCOTypes']//option";
		public static final String fetchValueFromViewPage = "@xpath=//div[label[text()='value']]//following-sibling::div";
		public static final String fetchValueFromViewPage_NameTab = "@xpath=//div//label[text()='Name']//following::div[1]"; // added
		public static final String fetchValueFromViewPage_DeviceType = "@xpath=//div//label[text()='Device Type']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Vendor_Model = "@xpath=//div//label[text()='Vendor/Model']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Connectivity = "@xpath=//div//label[text()='Connectivity Protocols']//following::div[1]";// added
		public static final String fetchValueFromViewPage_SNMP = "@xpath=//div//label[text()='SNMP Version']//following::div[1]";// added
		public static final String fetchValueFromViewPage_RouterId = "@xpath=//div//label[text()='Router Id']//following::div[1]";// added
		public static final String fetchValueFromViewPage_ManagementAdd = "@xpath=//div//label[text()='Management Address']//following::div[1]";// added
		public static final String fetchValueFromViewPage_FullIQNET = "@xpath=//div//label[text()='Full IQNET']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Country = "@xpath=//div//label[text()='Country']//following::div[1]";// added
		public static final String fetchValueFromViewPage_City = "@xpath=//div//label[text()='City']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Site = "@xpath=//div//label[text()='Site']//following::div[1]";// added
		public static final String searchButton = "@xpath=//span[text()='Search']";
		public static final String clickOnSearcheddevice = "@xpath=(//div[div[text()='Search Device Result']]//following-sibling::div//div[@col-id='deviceName'])[2]";
		public static final String actiondropdown = "@xpath=//div[div[text()='Search Device Result']]//following-sibling::div//button[text()='Action']";
		public static final String viewLink = "@xpath=//a[text()='View']";
	}

}
