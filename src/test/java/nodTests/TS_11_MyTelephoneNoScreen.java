package nodTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

import testHarness.commonFunctions.ReusableFunctions;
import testHarness.nodFunctions.NODLoginPage;
import testHarness.nodFunctions.NODLogoutPage;
import testHarness.nodFunctions.NODMyOrderPage;
import testHarness.nodFunctions.NODMyTelephoneNumberScreenPage;
import testHarness.nodFunctions.NODNumberDeactivation;
import testHarness.nodFunctions.NODTelephoneNumberSearchPage;


public class TS_11_MyTelephoneNoScreen extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	NODLoginPage Login = new NODLoginPage();
	GlobalVariables g = new GlobalVariables();
	NODMyOrderPage MyOrder = new NODMyOrderPage();
	NODLogoutPage Logout = new NODLogoutPage();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();
	NODMyTelephoneNumberScreenPage TelephoneScreen = new NODMyTelephoneNumberScreenPage();
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test(groups={"ModifyAddress_TransactionPage"})
	public void ModifyAddress_TransactionPage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	

		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

			// load a properties file
			prop.load(input);               

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

		try
		{
			openurl(Configuration.NOD_URL);
			Login.NODLogin();
			MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelephoneScreen.TXDetails_ModifyAddress(testDataFile, dataSheet, scriptNo, dataSetNo);
	
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
		//		Terminating the Execution once it gets ended
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}	

	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test(groups={"MyTelephoneNoScreen"})
	public void MyTelephoneNoScreen(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	

		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

			// load a properties file
			prop.load(input);               

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

		try
		{
			openurl(Configuration.NOD_URL);
			Login.NODLogin();
			MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
		//	TelPhNumberSearch.SearchOrderByID(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelephoneScreen.MyTelephoneNumberScreen(testDataFile, dataSheet, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
		//		Terminating the Execution once it gets ended
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}	

	//directory service update is successful for 'Portin Activated' numbers from 'Telephone number' screen
			@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
			@Test(groups={"MyTelephoneNoScreen_PortinActivated_DSU"})
			public void MyTelephoneNoScreen_PortinActivated_DSU(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
			{

				Report.createTestCaseReportHeader(ScenarioName);	

				try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

					// load a properties file
					prop.load(input);               

				} catch (IOException ex) {
					ex.printStackTrace();
				}

				String dataFileName = prop.getProperty("TestDataSheet");
				File path = new File("./TestData/"+dataFileName);
				String testDataFile = path.toString();

				try
				{
					openurl(Configuration.NOD_URL);
					Login.NODLogin();
					MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
					TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
					TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
					TelephoneScreen.PortinActivated_DSU(testDataFile, dataSheet, scriptNo, dataSetNo);
			
				}catch(Exception e)
				{
					Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
					ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				} 
				//		Terminating the Execution once it gets ended
				Report.createTestCaseReportFooter();
				Report.SummaryReportlog(ScenarioName);
				ExtentTestManager.endTest();
				ExtentManager.getReporter().flush();
				WEB_DRIVER_THREAD_LOCAL.get().close();

			}
		
		
		
		
		
	
	

}
