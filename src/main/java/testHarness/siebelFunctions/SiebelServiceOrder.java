package testHarness.siebelFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelServiceOrderObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelServiceOrder extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	

	
	public void NavigateToServiceOrderTab() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelServiceOrderObj.ServiceOrder.SeriviceOrderTab,"Service Order Tab");
		click(SiebelServiceOrderObj.ServiceOrder.SeriviceOrderTab,"Service Order Tab");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelServiceOrderObj.ServiceOrder.InputOrderNoTxb,"Service Order Number Textbox");
	}
	
	public void SearchServiceOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String ServiceOrderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		verifyExists(SiebelServiceOrderObj.ServiceOrder.InputOrderNoTxb,"Service Order Number Textbox");	
		sendKeys(SiebelServiceOrderObj.ServiceOrder.InputOrderNoTxb, ServiceOrderNum, "Service Order Number Textbox");
		verifyExists(SiebelServiceOrderObj.ServiceOrder.ServiceOrderGo,"Serive order Go Button");
		click(SiebelServiceOrderObj.ServiceOrder.ServiceOrderGo,"Serive order Go Button");
		Reusable.waitForSiebelLoader();

	}
	

	

}
