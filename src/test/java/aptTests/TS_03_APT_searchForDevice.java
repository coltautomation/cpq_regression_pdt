package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.searchForDevice;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_03_APT_searchForDevice extends SeleniumUtils{
	
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	searchForDevice searchDevice = new searchForDevice();
	APT_Login Login=new APT_Login();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void searchDevice(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
		{

            // load a properties file
            prop.load(input);               

        }
		catch (IOException ex) 
		{
            ex.printStackTrace();
        }
	
	
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

	try
	{
		openurl(Configuration.APT_URL);
		Login.APT_VoiceService();
		searchDevice.clickOnSearchForDevice();
		searchDevice.search_Device(testDataFile, dataSheet, scriptNo, dataSetNo);
		searchDevice.selectValueUnderRecord();
		searchDevice.fetchValueInViewDevicePage();
	}
	catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
	}
	
}
