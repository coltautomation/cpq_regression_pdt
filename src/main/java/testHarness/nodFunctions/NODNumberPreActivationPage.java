package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODNumberPreactivationObj;
//import pageObjects.nodObjects.NODNumberReservationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODNumberPreActivationPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	String OrderType;
	


	public void PreActivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
		String NumberType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Type");
		String BlockSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_Size");
		String QuantitySize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity_Size");
		String LocalAreaCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Local_AreaCode");
		
		verifyExists(NODNumberPreactivationObj.NumPreActivation.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODNumberPreactivationObj.NumPreActivation.QuickLinkDrpDwnArw,"Quick Link Drop down");
		click(NODNumberPreactivationObj.NumPreActivation.ReservationLink,"Activation link click");
		waitForAjax();
		waitForElementToBeVisible(NODNumberPreactivationObj.NumPreActivation.ReservationPage,15);
		
		verifyExists(NODNumberPreactivationObj.NumPreActivation.NumberTypeRadioBtn.replace("NoType",NumberType),"Select Number Type radio btn");
	    javaScriptclick(NODNumberPreactivationObj.NumPreActivation.NumberTypeRadioBtn.replace("NoType",NumberType),"Number Type radio btn");
	    
	    if(isElementPresent(By.xpath("//label[contains(text(),'Your Local Area Code (LAC)')]")))
	    {
	    	verifyExists(NODNumberPreactivationObj.NumPreActivation.LocalAreaCode,"LocalAreaCode");
		    javaScriptclick(NODNumberPreactivationObj.NumPreActivation.LocalAreaCode,"LocalAreaCode");
		    javaScriptclick(NODNumberPreactivationObj.NumPreActivation.LocalAreaCodeOption.replace("option",LocalAreaCode),"LocalAreaCode");
	    }
		Reusable.Select(NODNumberPreactivationObj.NumPreActivation.BlockSize,BlockSize);
		Reusable.Select(NODNumberPreactivationObj.NumPreActivation.QuantitySize,QuantitySize);
		javaScriptclick(NODNumberPreactivationObj.NumPreActivation.ProvideNumbersBtn," Provide Numbers Btn");
		
		waitForAjax();
		waitForElementToBeVisible(NODNumberPreactivationObj.NumPreActivation.NumberRangeChkBox,15);
		javaScriptclick(NODNumberPreactivationObj.NumPreActivation.NumberRangeChkBox," Number Range CheckBox");
		waitForAjax();
		if(isElementPresent(By.xpath("//button[contains(text(),'Preactivate')]"))){
		waitForElementToBeVisible(NODNumberPreactivationObj.NumPreActivation.PreActivateBtn,15);
		javaScriptclick(NODNumberPreactivationObj.NumPreActivation.PreActivateBtn,"Number Reservation Btn");
		}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Pre-Activation is not allowed for this country");
		}
		WaitForAjax();
		waitForElementToBeVisible(NODNumberPreactivationObj.NumPreActivation.OrderConfirmationBtn,15);
		javaScriptclick(NODNumberPreactivationObj.NumPreActivation.OrderConfirmationBtn,"Order Confirmation Btn");
		
		waitForElementToBeVisible(NODNumberPreactivationObj.NumPreActivation.ResrvationConfirmationMsg,15);
	    if(isElementPresent(By.xpath("//div[@id='test-l-4']")))
	    {
	    	String SuccessMessage=getTextFrom(NODNumberPreactivationObj.NumPreActivation.SuccessMessage, "SuccessMessage");
	    	//String emailNotification=getTextFrom(NODNumberPreactivationObj.NumPreActivation.EmailNotification, "SuccessMessage");
	    	ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
	    }
	    else{
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Pre-Activation is Not successful");
	    }
	    String DirectActivationOrderId=getTextFrom(NODNumberPreactivationObj.NumPreActivation.ReservationOrderId, "Order Id");
	    WaitForAjax();
	    javaScriptclick(NODNumberPreactivationObj.NumPreActivation.CheckMyOrder,"Check MyOrder Btn");
	    WaitForAjax();
	    waitForElementToDisappear(NODNumberPreactivationObj.NumPreActivation.TableRecordLoader,120);
	    click(NODNumberPreactivationObj.NumPreActivation.SearchTxtFld, "Send Order ID");
	    sendKeys(NODNumberPreactivationObj.NumPreActivation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
	    WaitForAjax();
		click(NODNumberPreactivationObj.NumPreActivation.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODNumberPreactivationObj.NumPreActivation.TableRecordLoader,120);
		waitForAjax();
		
		//String OrderStatus;
		if(isElementPresent(By.xpath("//a[@class='transaction']"))){
		 //OrderStatus = getTextFrom(NODNumberPreactivationObj.NumPreActivation.orderStatusVal,"Order Status");
		 OrderType = getTextFrom(NODNumberPreactivationObj.NumPreActivation.orderTypeVal,"Order Type");
		 
		}else
		{  
		    for(int i=0;i<5;i++)
            {          
		    	WaitForAjax();
				sleep(60000);
				refreshPage();
				waitForElementToDisappear(NODNumberPreactivationObj.NumPreActivation.TableRecordLoader,120);
			    click(NODNumberPreactivationObj.NumPreActivation.SearchTxtFld, "Send Order ID");
			    sendKeys(NODNumberPreactivationObj.NumPreActivation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
			    WaitForAjax();
				click(NODNumberPreactivationObj.NumPreActivation.SearchIcon,"Search Icon");
				//waitForAjax();
				waitForElementToDisappear(NODNumberPreactivationObj.NumPreActivation.TableRecordLoader,120);
                                                         
                 if (isElementPresent(By.xpath("//a[@class='transaction']")))
                 {
                	// OrderStatus = getTextFrom(NODNumberPreactivationObj.NumPreActivation.orderStatusVal,"Order Status"); 
                	 OrderType = getTextFrom(NODNumberPreactivationObj.NumPreActivation.orderTypeVal,"Order Type");
                     break;         	
                 }
                 
   	        }
   		   }
   		if(OrderType.equals("Number Activation")) 
   		{
   			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Type and Order Status Is: "+OrderType);
   		}else{
   			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Status is not Present");	
   		}
   		
   		}
           
   	



}



