
package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_IPTransitObj;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
//import testHarness.aptFunctions.Lanlink_Outbandmanagementhelper;
import testHarness.aptFunctions.searchForDevice;
import testHarness.aptFunctions.voipAccess_NewTab;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_24_voipAccess_NewTab_Test extends SeleniumUtils {
	public String CustomerName = null;
	public static String DeviceNameValue = null;
	public static String VendorModelValue = null;

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	searchForDevice searchDevice = new searchForDevice();
	APT_Login Login = new APT_Login();
	voipAccess_NewTab voipAccess = new voipAccess_NewTab();
	// Lanlink_Outbandmanagementhelper Outband = new
	// Lanlink_Outbandmanagementhelper();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void APT_IPTransit_01(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException {
		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			// Search Existing Service
			voipAccess.searchOrderORservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			// Add MAS switch
			voipAccess.verifyAddMASswitch(testDataFile, dataSheet, scriptNo, dataSetNo);

			// EditMASswitch and close child tabs
			voipAccess.editMASdevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			voipAccess.closeChildtab();

			// deleteMASdevice_wholesaleSIP");
			voipAccess.clickOnBreadCrump(testDataFile, dataSheet, scriptNo, dataSetNo);

			String deviceName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"MAS_deviceName");
			String deviceName2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"MAS_devicename2");

			voipAccess.MASswitch__DeleteFromServiceFunctionality(deviceName1);
			voipAccess.MASswitch__DeleteFromServiceFunctionality(deviceName2);

		}

		catch (

		Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

		}

		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}
}
