
package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_VOIPAccess;

public class TS_28_APT_VOIPAccess_Test extends SeleniumUtils {

	APT_Login Login = new APT_Login();
	APT_VOIPAccess VOIPAccess = new APT_VOIPAccess();

	Properties prop = new Properties();
	

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void VOIPAccess(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws Exception {

		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		
		
		
		try {
			
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");
			String editGateway = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, scriptNo, "editGateway");
			String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
			String DRusingTDM_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"DRusingTDM_checkbox");
			String edit_DRusingTDM_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"edit_DRusingTDM_checkbox");
			String gateway = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "gateway");

			
			 if(newCustomerName.equalsIgnoreCase("yes") &&
			  existingCustomer.equalsIgnoreCase("no")) {
			 
			 VOIPAccess.createcustomer(testDataFile, dataSheet, scriptNo,
			  dataSetNo);
			  
			  
			  VOIPAccess.selectCustomertocreateOrder(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  
			  }else if(newCustomerName.equalsIgnoreCase("no") &&
			  existingCustomer.equalsIgnoreCase("Yes")) {
			  
			  
			  VOIPAccess.selectCustomertocreateOrder(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  }
			  
			  
			  //verifyNewOrderCreationOrExistingOrderSelection_VOIP
			  VOIPAccess.createorderservice(testDataFile, dataSheet, scriptNo,
			  dataSetNo);
			  
			  
			  
			  //verifyServiceTypeSelection_VOIP
			  VOIPAccess.verifyselectservicetype(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  
			  
			  //verifyServiceCreation_VOIP
			  VOIPAccess.verifyingservicecreation(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  
			  
			  //verifyCustomerDetailsInformation_VOIP
			  VOIPAccess.searchorder(sid);
			  VOIPAccess.verifyCustomerDetailsInformation(testDataFile,
			  dataSheet, scriptNo, dataSetNo);
			  
			  
			  
			  
			  //VerifyEditOrderChangeOrderFunction_VOIP
			  VOIPAccess.verifyorderpanel_editorder(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  VOIPAccess.verifyorderpanel_changeorder(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  
			  
			  //VerifyServiceInfoEditSeviceFunction_VOIP
			  VOIPAccess.searchorder(sid);
			  VOIPAccess.verifyservicepanelInformationinviewservicepage(
			  testDataFile, dataSheet, scriptNo, dataSetNo);
			  VOIPAccess.editService(testDataFile, dataSheet, scriptNo,
			  dataSetNo);
			  
			  
			  
			  //verifyManageService_ManageSubnet_IPv6_VOIP
			  VOIPAccess.searchorder(sid);
			  VOIPAccess.manageSubnet_viewServicepage();
			  VOIPAccess.manageServiceFunctionTest(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  
			  
			  //verifyDumpFunction_VOIP VOIPAccess.searchorder(sid);
			  VOIPAccess.searchorder(sid);
			  VOIPAccess.verifyDumpFunctionInviewServicepage();
			  
			  
			  
			  //verifyManagementOptionspanel_VOIP VOIPAccess.searchorder(sid);
			  VOIPAccess.searchorder(sid);
			  VOIPAccess.verifyManagementOptionspanel(testDataFile, dataSheet,
			  scriptNo, dataSetNo);
			  
			  
			  
			 

			/*
			 * *****************************************************************
			 * **************** * MAS Switch * *
			 *********************************************************************************
			 */

			// verifyAddMASswitchFunction_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyAddMASswitch(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyAddedMASswitchInformation_VOIP
			VOIPAccess.navigatetoViewDevicepage_MAS();
			VOIPAccess.verifyAddedMASswitchInformation_View(testDataFile, dataSheet, scriptNo, dataSetNo);
			//VOIPAccess.testStatus_MAS();

			// verifyEditMasSwitchFunction_VOIP
			VOIPAccess.verifyEditMASswitchFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyFetchDeviceInterfaceFunction_MAS_VOIP
			VOIPAccess.veriyFetchDeviceInterfacesFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyRouterToolFunction_MAS_VOIP
			VOIPAccess.navigateToDevicePageFromManageColtNetwork_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.routerPanel_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			//// ADD INTERFACE for MAS Switch ///

			// verifyAddInterfaceFunction_MAS_VOIP
		

			VOIPAccess.verifyAddInterfaceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyEditInterfaceFunction_MAS_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyEditInterfaceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyConfigureInterfaceFunction_MAS_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyConfigureInterfaceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyDeleteInterfaceFunction_MAS_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyDeleteInterfaceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyDeleteDeviceFunction_MAS_VOIP
			VOIPAccess.verifyDeleteDeviceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.verifyDeleteDeviceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);

			/*
			 * *****************************************************************
			 * **************** * Provider Equipment (PE) * *
			 *********************************************************************************
			 */

			// verifyAddPEDeviceFunction_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyAddPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyAddedPEDeviceInformation_VOIP
			VOIPAccess.navigateToViewDevicePage_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.verifyAddedPEDeviceInformation_View(testDataFile, dataSheet, scriptNo, dataSetNo);
			//VOIPAccess.testStatus_PE();

			// verifyEditPEDeviceFunction_VOIP
			VOIPAccess.verifyEditPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyFetchDeviceInterfaceFunction_PE_VOIP
			VOIPAccess.verifFetchDeviceInterfacesFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyRouterToolFunction_PE_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.navigateToViewDevicePage_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			//////////////////////////// ADD INTERFACE for PE
			//////////////////////////// ///////////////////////////

			// verifyAddInterfaceFunction_PE_VOIP
		VOIPAccess.searchorder(sid);
			VOIPAccess.navigateToViewDevicePage_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.verifyAddInterfaceFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyEditInterfaceFunction_PE_VOIP
			//VOIPAccess.searchorder(sid);
			//VOIPAccess.verifyEditInterfaceFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyConfigureInterfaceFunction_PE_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyConfigureInterfaceFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyDeleteInterfaceFunction_PE_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyDeleteInterfaceFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyDeleteDeviceFunction_PE_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyDeleteDeviceFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// *********************************************************************************
			// * *
			// * Trunk Group/Site Order Function *
			// * *
			// *********************************************************************************

			// verifyTrunkGroupSiteOrderFunction_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.addTrunkGroupSiteOrderNumber(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyAddTrunkFunction_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.verifyAddTrunkFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyAddedTrunkInformationInviewTrunk_VOIP
			VOIPAccess.verifyAddedTrunkInformationInviewTrunk(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyEditTrunkFunction_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyEditTrunkFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			// viewTrunk_History_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.navigateToViewTrunkPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.trunkHistory(testDataFile, dataSheet, scriptNo, dataSetNo);

			// viewTrunk_PSXQueue_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.navigateToViewTrunkPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.trunkPSXQueue(testDataFile, dataSheet, scriptNo, dataSetNo);

			// viewTrunk_GSX_PSX_SBC_configuration_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.navigateToViewTrunkPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.viewTrunk_PSX_executeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);

		
			/*
			 * *****************************************************************
			 * **************** * Customer Premise Equipment (CPE) * *
			 ************************************************************************************
			 */

			// verifyAddCPEDeviceFunction_VOIP
			// waitForAjax();
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyAddCPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyAddedCPEDeviceInformation_VOIP
			VOIPAccess.searchorder(sid);
			VOIPAccess.verifyAddedCPEDeviceInformation_View(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyEditCPEDeviceFunction_VOIP
			VOIPAccess.verifyEditCPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyFetchDeviceInterfaceFunction_CPE_VOIP
			VOIPAccess.veriyFetchDeviceInterfacesFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyRouterToolFunction_CPE_VOIP
			VOIPAccess.verifyRouterToolFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyDeleteDeviceFunction_CPE_VOIP
			VOIPAccess.verifyDeleteDeviceFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyTrunkAndTrunkSiteOrderDeletion_VOIP
		VOIPAccess.searchorder(sid);
			VOIPAccess.navigateToViewTrunkPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.deleteTrunk(testDataFile, dataSheet, scriptNo, dataSetNo);
			VOIPAccess.searchorder(sid);
			VOIPAccess.deleteTrunkGroupSiteOrderNumber(testDataFile, dataSheet, scriptNo, dataSetNo);


		} catch (

		Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

		}

		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}
}