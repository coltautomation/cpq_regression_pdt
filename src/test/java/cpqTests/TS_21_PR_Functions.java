package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.exploreFunctions.ExploreLoginPage;

public class TS_21_PR_Functions extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
		
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testPRFunctions(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
					
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			
			Accounts.NavigateToAccounts();
			
			String PR_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PR_Type");
			String Notes_Characters = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Notes_Characters");
			
			if (PR_Type.length()>0) { 
				DataMiner.fnsetcolvalue(testDataFile,dataSheet, scriptNo, dataSetNo, "PR_Type", PR_Type); 
			}
			if(Notes_Characters.length()>0) {
				DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Notes_Characters", Notes_Characters); 
			}	
			
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			if(PR_Type.equalsIgnoreCase("WholeSaleTeams")) {
				String sResult=AddProduct.validateWholeSaleTeam(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");		
			AddProduct.AddProductToQuote(ProductName);
			
			String sResult;			
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
//				calling the below method to enter the site address
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
		//		calling the below method to configure the product
				sResult = AddProduct.productConfiguration(testDataFile,dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
			} else {
//				Calling the below method to add the product in CPQ
				sResult = AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
			String Quote_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
			String Flow_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Flow_Type");
			
			if(ProductName.equalsIgnoreCase("Wave")&&!Flow_Type.equalsIgnoreCase("onnet")) {
				
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Created");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method to add Legal and Technical Contact Info
				sResult = Approvals.SwitchCPQUser("SE_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method to configure the product
				sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
			}
			
			if (Flow_Type.equalsIgnoreCase("ManualOffnet")) {
				
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.logoutCPQ("Main");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
						if(PR_Type.equalsIgnoreCase("RejectedCost")) {
							close();
							openBrowser(g.getBrowser());		
							openurl(Configuration.EXPLORE_URL);
							sResult = ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo);
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						}else {
				//			To Process Offnet Cost in Explore
							close();
							openBrowser(g.getBrowser());		
							openurl(Configuration.EXPLORE_URL);
							sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						}
						
				//		Calling the Below method to perform C4C Login	
						Login.C4CLogin();
												
				//		calling the below method to navigate to quotes from c4c main page
						AddProduct.navigateQuotesFromHomepage();
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to search the quote from c4c
						sResult = AddProduct.searchQuoteC4C(Quote_ID);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
				//		calling the below method reconfigure the product
						sResult = AddProduct.clickProductConfigurationBtn(ProductName);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to update and save the product in cpq
						sResult = AddProduct.updateSaveProductCPQ("Save");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
					}
			
			if (Flow_Type.equalsIgnoreCase("ManualNearnet")) {
				
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.logoutCPQ("Main");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
						if(PR_Type.equalsIgnoreCase("RejectedCost")) {
							close();
							openBrowser(g.getBrowser());		
							openurl(Configuration.EXPLORE_URL);
							sResult = ExpConfig.nearnetExploreRejectOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						}else {
				//			To Process Offnet Cost in Explore
							close();
							openBrowser(g.getBrowser());		
							openurl(Configuration.EXPLORE_URL);
							sResult = ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						}
						
				//		Calling the Below method to perform C4C Login	
						Login.C4CLogin();
												
				//		calling the below method to navigate to quotes from c4c main page
						AddProduct.navigateQuotesFromHomepage();
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to search the quote from c4c
						sResult = AddProduct.searchQuoteC4C(Quote_ID);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
				//		calling the below method reconfigure the product
						sResult = AddProduct.clickProductConfigurationBtn(ProductName);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to update and save the product in cpq
						sResult = AddProduct.updateSaveProductCPQ("Save");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
					}
			
			if (Flow_Type.equalsIgnoreCase("ManualDSL")) {
				
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.logoutCPQ("Main");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
						if(PR_Type.equalsIgnoreCase("RejectedCost")) {
							close();
							openBrowser(g.getBrowser());		
							openurl(Configuration.EXPLORE_URL);
							sResult = ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo);
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						}else {
				//			To Process Offnet Cost in Explore
							close();
							openBrowser(g.getBrowser());		
							openurl(Configuration.EXPLORE_URL);
							sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						}
						
				//		Calling the Below method to perform C4C Login
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						Login.C4CLogin();
												
				//		calling the below method to navigate to quotes from c4c main page
						AddProduct.navigateQuotesFromHomepage();
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to search the quote from c4c
						sResult = AddProduct.searchQuoteC4C(Quote_ID);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
				//		calling the below method reconfigure the product
						sResult = AddProduct.clickProductConfigurationBtn(ProductName);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to update and save the product in cpq
						sResult = AddProduct.updateSaveProductCPQ("Save");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
					}
			
			if (PR_Type.equalsIgnoreCase("RejectedCost")) {
				
				//		calling the below method to verify quote stage and status
						sResult = AddProduct.verifyQuoteStage("No Bid");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }										
						
				//		calling the below method reconfigure the product
						sResult = AddProduct.clickProductConfigurationBtn(ProductName);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//	    Calling the below method for onnet Configuration
					    sResult = AddProduct.onnetConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					    if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				//	    Calling the below method for feature select
					    sResult = AddProduct.selectProductFeatures(testDataFile, dataSheet, scriptNo, dataSetNo);
					    if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }						
						
				//		calling the below method to update and save the product in cpq
						sResult = AddProduct.updateSaveProductCPQ("Save");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
						
		}
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo); 
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
			AddProduct.verifyQuoteStage("Priced");
			String Re_Explore_Option = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Re_Explore_Option");
			if(Re_Explore_Option.equalsIgnoreCase("UPDATE SELECTED REQUEST")) {
//				calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

//				calling the below method re-valdiate or re-negotiate the product
				sResult = AddProduct.reValidatereNegotiateQuote(testDataFile,dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to verify quote stage and status
				sResult = AddProduct.logoutCPQ("Main");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Priced");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			}
			
			if(PR_Type.equalsIgnoreCase("ACV")) {
				sResult = AddProduct.validateACVTCVValues(testDataFile, dataSheet, scriptNo, dataSetNo,"Priced");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
			String Discount_Percentage = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Discount_Percentage");
			String Discount_Applicable = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Discount_Applicable");
			Quote_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
			
			if(Discount_Applicable.equalsIgnoreCase("Yes")) {
				
//				Calling the below method to enter the discounting process
				sResult = AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to verify quote stage and status
				AddProduct.SubmitForApproval();
				
				if (Integer.parseInt(Discount_Percentage) > 5) {
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Pending Governance Approval");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
			if (Integer.parseInt(Discount_Percentage) > 10) {
//				calling the below method to switch to VP sales user
				sResult = Approvals.SwitchCPQUser("VPSales1_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} else {
//				calling the below method to switch to VP sales user
				sResult = Approvals.SwitchCPQUser("VPSales2_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
			
//			calling the below method to switch to VP sales user
			sResult = AddProduct.quoteDiscountGovernanceApproval("Approve");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to verify quote stage and status
			sResult = AddProduct.verifyQuoteStage("Approved");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to verify quote stage and status
			sResult = AddProduct.logoutCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
			
//			Calling the Below method to perform C4C Login
			close();
			openBrowser(g.getBrowser());
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
						
//			calling the below method to navigate to quotes from c4c main page
			AddProduct.navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to search the quote from c4c
			sResult = AddProduct.searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} 
				
//			calling the below method to verify quote stage and status
			sResult = AddProduct.verifyQuoteStage("Approved");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
			Discount_Applicable = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Discount_Applicable");
			if(!Discount_Applicable.equalsIgnoreCase("Yes")) {
//			      calling the below method to move the quote status to Commercial Approval
					AddProduct.SubmitForApproval();				

//					calling the below method to verify quote stage and status
					sResult = AddProduct.verifyQuoteStage("Approved");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
			if(PR_Type.equalsIgnoreCase("WholeSaleTeams")) {
				
				sResult=AddProduct.validateWholeSaleTeam(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				Revising Commercial
				sResult = AddProduct.reviseQuote();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Priced");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				Validate Wholessale Team after revising commercial
				sResult=AddProduct.validateWholeSaleTeam(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to move the quote status to Commercial Approval
				AddProduct.SubmitForApproval();
							
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Approved");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
		}
			
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			
			//String CPE_Site_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Site_BCN");
			//Submit.addBillingInformation("CPE Solutions Site", CPE_Site_BCN);
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.logoutCPQ("Main");
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	

}
