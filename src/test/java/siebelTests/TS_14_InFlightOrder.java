package siebelTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCEOS;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebelFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNumberManagement;

public class TS_14_InFlightOrder extends SeleniumUtils
{
	
	/*
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	String dataFileName = "Siebel_testData.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "InFlight";
	String scriptNo = "14";
	String dataSetNo = "5";
	String Amount = "2";
	*/
	
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddProduct = new SiebelCreateCustomerOrder();
	SiebelAccounts Accounts = new SiebelAccounts();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelMode siebelmod = new SiebelMode();
	SiebelNumberManagement NumMana = new SiebelNumberManagement();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();

Properties prop = new Properties();
	
	
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testInFlightOrder(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
				
		try
		{
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.createCustomerOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.productSelection(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.openServiceOrderNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			siebelmod.enterMandatoryFieldsInHeader(testDataFile, dataSheet, scriptNo, dataSetNo);
	
			siebelmod.addProductConfigurationDetailsAndCompleteInFlightOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	    }
	        Report.createTestCaseReportFooter();
	        Report.SummaryReportlog(ScenarioName);
			
	}
	
	
	
	/*
	@Test(priority = 1, groups={"testSiebelLogin"})
	public void testSiebelLogin()
	{
		Report.createTestCaseReportHeader();	
				
		try
		{
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
			//siebelmod.addProductConfigurationDetailsAndCompleteInFlightOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelLogin "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelLogin "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Siebel Login page functionality");
		
	}	
	
	@Test(priority = 2, groups={"testNavigateToAccounts"}, dependsOnGroups = { "testSiebelLogin" })
	public void testNavigateToAccounts()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Accounts.NavigateToAccounts();

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testNavigateToAccounts "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testNavigateToAccounts "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify navigation to accounts Page");
		
	}
	
	@Test(priority = 3, groups={"testSearchAccount"}, dependsOnGroups = { "testNavigateToAccounts" })
	public void testSearchAccount()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Accounts.SearchAccount(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSearchAccount "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSearchAccount "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Search and Open existing account");
	}	
	
	@Test(priority = 4, groups={"testSiebelCreateCustomerOrder"}, dependsOnGroups = { "testSearchAccount" })
	public void testSiebelCreateCustomerOrder()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			AddProduct.createCustomerOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelCreateCustomerOrder "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelCreateCustomerOrder "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Siebel Create Customer Order");
		
	}	
	@Test(priority = 5, groups={"testproductSelection"}, dependsOnGroups = { "testSiebelCreateCustomerOrder" })
	public void testproductSelection()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.productSelection(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testproductSelection "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testproductSelection "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to add product");
	}	
	
	
	@Test(priority = 6, groups={"testopenServiceOrderNumber"}, dependsOnGroups = { "testproductSelection" } )
	public void testopenServiceOrderNumber()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			
			Product.openServiceOrderNumber(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testopenServiceOrderNumber "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testopenServiceOrderNumber "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to open serive Order ref number");
	}	
	
	
	@Test(priority = 7, groups={"testenterMandatoryFieldsInHeader"}, dependsOnGroups = { "testopenServiceOrderNumber" })
	public void testenterMandatoryFieldsInHeader()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.enterMandatoryFieldsInHeader(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Mandatory Fields In Header "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in enter Mandatory Fields InHeader "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Mandatory Details In Middle Applet.");
	}	
	
	
	@Test(priority = 8, groups={"addProductConfigurationDetailsAndCompleteInFlightOrder"}, dependsOnGroups = { "testenterMandatoryFieldsInHeader" })
	public void testaddProductConfigurationDetailsAndCompleteOrderStatus()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			
			siebelmod.addProductConfigurationDetailsAndCompleteInFlightOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in addProductConfigurationDetailsAndCompleteInFlightOrder "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testaddProductConfigurationDetailsAndCompleteOrderStatus "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to add Product Configuration Details And Complete Order Status");
	}
	*/
}
