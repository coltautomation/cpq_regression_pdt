package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODMyTelephoneNumberScreenPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();


	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void TXDetails_ModifyAddress(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String CustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");

		if(isElementPresent(By.xpath("//*[@class='actionLinks ml-4 ng-star-inserted']")))
		{                              

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Action Drop down"); 
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,15);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,"Address Update Option");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,"Address Update Option");
			waitForAjax();

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");

			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			if(isElementPresent(By.xpath("//*[contains(text(),'The Address you have entered is partially complete')]")))
			{                              
				if(isElementPresent(By.xpath("(//*[@class='card rounded-0'])[1]")))
				{                              
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressSuggestion,"Suggested Address");
					javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,15);
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");

					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");

				}
				else
				{
					Report.LogInfo("Log Report","INFO","Address suggestions are not there");
				}			
			}
			else{
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");

				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");

				Report.LogInfo("Log Report","INFO","No need to select the Address from suggetion");
			}
		}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Transaction Details page is not getting open");	
		}

	}






	
	public void MyTelephoneNumberScreen(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String CustomerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Type");
        String FirstName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"First_Name");
	    String LastName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Last_Name");
		String LanValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Lan_Value");
		String SubcriberId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubcriberId");
		String DepartmentValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Department_Value");

		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]")))
        {                              
	        javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionOption,"Action Option");
	        waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,15);
	        click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,"AddressUpdate link");
	        waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
	        if(isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]")))
			{
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"Select Country");
			    javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"wholesaleradiobtn");
			    
			     if(CustomerType.toString().equals("Residential"))
			     {
			    	sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.FirstName, FirstName, "Enter First Name");
			    	sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LastName, LastName, "Enter Last Name");
			    	verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,"Language");
			    	selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue,"LanguageType");
			    	sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
			     }
			     else if(CustomerType.toString().equals("Business")){
			    	sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			    	Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue);
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
			 		sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
			     }
			}
			   else{
			WaitForAjax();
			if(isElementPresent(By.xpath("//label[contains(text(),'Service Type')]"))){
	        Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			}
	        if(isElementPresent(By.xpath("//input[@id='emerBusinessNameUpdated']"))){
	    		    sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
	    					}else{
	    						sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName, CustomerName, "Enter Customer Name");
	    					}
	        if(isElementPresent(By.xpath("//label[contains(text(),'Subscriber ID')]"))){
	        sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter House Number");
	        }
	        sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
			if (isElementPresent(By.xpath("//label[contains(text(),'Department')]"))){
		        selectByValue(NODNumberDirectActivationObj.NumDirActivation.DepartmentType,DepartmentValue,"Department Type");
		        WaitForAjax();
		        waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,30);
		        Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,CityTown);
		        waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,15);
		    	Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,PostalCode);
		    	}
			else{
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
			}
			
			   }
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]")))
	        {                              
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
	        }
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateAddressBtn,"Validate Address Button");
	        verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Updated Successfully");
        }
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not Updated Successfullys");	
    		
        }
		
	}






	public void PortinActivated_DSU(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String Customer_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String House_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"House_Number");
		String Street_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String City_Town = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City_Town");
		String Postal_Code = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Order_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Type");
		String Telephone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DSU_Telephone_Number");
		String Line_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Line_Type");
		String Tariff = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Tariff");
		String Entry_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Entry_Type");
		String Typeface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Typeface");
		String Listing_Category = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Listing_Category");
		String Listing_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Listing_Type");
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");

		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]")))
		{                              
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionOption,"Action Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,15);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,"Directory Service Update Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		

			if (Country.toString().equals("UK")) 
			{
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LineTypeDrpDwn,Line_Type,"Line Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TraffDrpDwn,Tariff,"Tariff");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.EntryTypeDrpDwn,Entry_Type,"Entry Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TypeFaceDrpDwn,Typeface,"Entry Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingCategoryDrpDwn,Listing_Category,"Listing Category");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.listingTypeDrpDwn,Listing_Type,"Listing Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PriorityDrpDwn,Priority,"Listing Type");
				waitForAjax();
				 ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");

			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "DSU is not getting updated");	

			}

		}

	}








	
	
	
	
	
	
	
	
	
	
	


}



