package testHarness.ncFunctions;

import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.DeviceCreationObj;
import testHarness.commonFunctions.ReusableFunctions;

public class DeviceCreation extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();

	public void NcCreateDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String DeviceType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Comments");
		
		if (DeviceType.toString().contains("GX"))
		{
			SearchArDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			CreateGxDevice(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if ((DeviceType.toString().contains("LTS")) || DeviceType.toString().contains("ANT"))
		{
			SearchArDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			CreateLtsAntDevice(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (DeviceType.toString().contains("L3CPE"))
		{
			SearchArDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			CreateL3cpeDevice(testDataFile, sheetName, scriptNo, dataSetNo);
		}
	}

		
	public void SearchArDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String AR_Details= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AR Details");
		
		Reusable.waitForAjax();
		verifyExists(DeviceCreationObj.DeviceCreation.Fastsearch,"Fastsearch");
		click(DeviceCreationObj.DeviceCreation.Fastsearch,"Fastsearch");
//		Reusable.waitForAjax();

		mouseMoveOn(DeviceCreationObj.DeviceCreation.ResourceInventorySearch);
		
		verifyExists(DeviceCreationObj.DeviceCreation.DeviceSearch,"DeviceSearch");
		click(DeviceCreationObj.DeviceCreation.DeviceSearch,"DeviceSearch");		
		
		sendKeys(DeviceCreationObj.DeviceCreation.DeviceNameCriteria,AR_Details,"DeviceNameCriteria");
		
		Reusable.SendkeaboardKeys(DeviceCreationObj.DeviceCreation.DeviceNameCriteria,Keys.TAB);

		verifyExists(DeviceCreationObj.DeviceCreation.SearchButton,"SearchButton");
		click(DeviceCreationObj.DeviceCreation.SearchButton,"SearchButton");
		Reusable.waitForpageloadmask();
		
		ClickonElementByString("//a/span[contains(text(),'"+AR_Details+"')]/parent::*",10);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(DeviceCreationObj.DeviceCreation.Accountbredcrumb,"Accountbredcrumb");
		click(DeviceCreationObj.DeviceCreation.Accountbredcrumb,"Accountbredcrumb");
		Reusable.waitForpageloadmask();	
	}
	
	public void CreateGxDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Device_Role= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device Role");
		String Order_Number= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
		String Site_ID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site ID");
		String Template= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Template");

		verifyExists(DeviceCreationObj.DeviceCreation.AddNewDevice,"AddNewDevice");
		click(DeviceCreationObj.DeviceCreation.AddNewDevice,"AddNewDevice");
		Reusable.waitForpageloadmask();	

		verifyExists(DeviceCreationObj.DeviceCreation.AddFromTemplate,"AddFromTemplate");
		click(DeviceCreationObj.DeviceCreation.AddFromTemplate,"AddFromTemplate");
		
		verifyExists(DeviceCreationObj.DeviceCreation.Search,"Search");
		click(DeviceCreationObj.DeviceCreation.Search,"Search");
		
		selectByVisibleText(DeviceCreationObj.DeviceCreation.DeviceRole,Device_Role,"DeviceRole");
		
		sendKeys(DeviceCreationObj.DeviceCreation.OrderNumber,Order_Number,"OrderNumber");
		
		sendKeys(DeviceCreationObj.DeviceCreation.Site,Site_ID,"Site");		
		
		Reusable.SendkeaboardKeys(DeviceCreationObj.DeviceCreation.Site,Keys.ENTER);
		
		verifyExists(DeviceCreationObj.DeviceCreation.CreateSlot,"CreateSlot");
		click(DeviceCreationObj.DeviceCreation.CreateSlot,"CreateSlot");		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String SlotName=getTextFrom(DeviceCreationObj.DeviceCreation.SlotName);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Slot Name",SlotName);
		
		verifyExists(DeviceCreationObj.DeviceCreation.Slots,"Slots");
		click(DeviceCreationObj.DeviceCreation.Slots,"Slots");		
		
		verifyExists(DeviceCreationObj.DeviceCreation.Slot1,"Slot1");
		click(DeviceCreationObj.DeviceCreation.Slot1,"Slot1");	
		
		verifyExists(DeviceCreationObj.DeviceCreation.InsertCards,"InsertCards");
		click(DeviceCreationObj.DeviceCreation.InsertCards,"InsertCards");	

		verifyExists(DeviceCreationObj.DeviceCreation.InsertCardFromTemplate,"InsertCard From Template");
		click(DeviceCreationObj.DeviceCreation.InsertCardFromTemplate,"InsertCard From Template");
		Reusable.waitForAjax();

		ClickonElementByString("//a[text()='"+Template+"']/parent::*/parent::*//input",10);
		
		sendKeys(DeviceCreationObj.DeviceCreation.OrderNumber,Order_Number,"OrderNumber");
		
		sendKeys(DeviceCreationObj.DeviceCreation.Site,Site_ID,"Site");		
		
		Reusable.SendkeaboardKeys(DeviceCreationObj.DeviceCreation.Site,Keys.ENTER);
		
		verifyExists(DeviceCreationObj.DeviceCreation.Insert,"Insert");
		click(DeviceCreationObj.DeviceCreation.Insert,"Insert");	

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String DeviceName=getTextFrom(DeviceCreationObj.DeviceCreation.GxDeviceName);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device Name",DeviceName);
		
		verifyExists(DeviceCreationObj.DeviceCreation.GxDeviceName,"GxDeviceName");
		click(DeviceCreationObj.DeviceCreation.GxDeviceName,"GxDeviceName");	
		
		String DeviceURL=getAttributeFrom(DeviceCreationObj.DeviceCreation.LinkforDevice, "href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device URL",DeviceURL);

		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: GX Device Successfully Created");
	}
	
	public void CreateLtsAntDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Template= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Template");
		String Device_Role= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device Role");
		String Order_Number= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
		String Site_ID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site ID");
		String Comments= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Comments");


		verifyExists(DeviceCreationObj.DeviceCreation.NetworkElements,"NetworkElements");
		click(DeviceCreationObj.DeviceCreation.NetworkElements,"NetworkElements");
		
		verifyExists(DeviceCreationObj.DeviceCreation.NewNetwkElement,"NewNetwkElement");
		click(DeviceCreationObj.DeviceCreation.NewNetwkElement,"NewNetwkElement");

		Reusable.waitForpageloadmask();
		
		verifyExists(DeviceCreationObj.DeviceCreation.AddFromTemplate,"AddFromTemplate");
		click(DeviceCreationObj.DeviceCreation.AddFromTemplate,"AddFromTemplate");
				
		verifyExists(DeviceCreationObj.DeviceCreation.Search,"Search");
		click(DeviceCreationObj.DeviceCreation.Search,"Search");		
		
		Reusable.waitForpageloadmask();
		
		selectByVisibleText(DeviceCreationObj.DeviceCreation.Template,Template, "Template");		
		Reusable.waitForpageloadmask();
		
		selectByVisibleText(DeviceCreationObj.DeviceCreation.DeviceRole,Device_Role, "DeviceRole");		
		
		sendKeys(DeviceCreationObj.DeviceCreation.OrderNumber, Order_Number,"OrderNumber");
		
		sendKeys(DeviceCreationObj.DeviceCreation.Site, Site_ID,"Site");
		Reusable.SendkeaboardKeys(DeviceCreationObj.DeviceCreation.Site, Keys.ENTER);
		
		verifyExists(DeviceCreationObj.DeviceCreation.CreateSlot,"CreateSlot");
		click(DeviceCreationObj.DeviceCreation.CreateSlot,"CreateSlot");		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(DeviceCreationObj.DeviceCreation.Devices,"Devices");
		click(DeviceCreationObj.DeviceCreation.Devices,"Devices");	
		
		String DeviceName=getTextFrom(DeviceCreationObj.DeviceCreation.DeviceName);
		DataMiner.fnsetcolvalue( testDataFile, sheetName, scriptNo, dataSetNo,"Device Name",DeviceName);		
		
		verifyExists(DeviceCreationObj.DeviceCreation.DeviceName,"DeviceName");
		click(DeviceCreationObj.DeviceCreation.DeviceName,"DeviceName");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Device");
		if (Comments.contains("LTS"))
		{
			String DeviceURL=getAttributeFrom(DeviceCreationObj.DeviceCreation.LinkforDevice,"href");
			DataMiner.fnsetcolvalue( testDataFile, sheetName, scriptNo, dataSetNo,"Device URL",DeviceURL);		

			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: LTS Device Successfully Created");
		}
		else
		{
			String DeviceURL=getAttributeFrom(DeviceCreationObj.DeviceCreation.LinkforAntDevice,"href");
			DataMiner.fnsetcolvalue( testDataFile, sheetName, scriptNo, dataSetNo,"Device URL",DeviceURL);		

			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: ANT Device Successfully Created");
		}
	}
	
	public void CreateL3cpeDevice(String dataFileName,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String Device_Model= DataMiner.fngetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo, "Device Model");
		String Template= DataMiner.fngetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo, "Template");
		String Device_Role= DataMiner.fngetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo, "Device Role");
		String Order_Number= DataMiner.fngetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo, "Order Number");
		String Site_ID= DataMiner.fngetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo, "Site ID");


		verifyExists(DeviceCreationObj.DeviceCreation.NetworkElements,"NetworkElements");
		click(DeviceCreationObj.DeviceCreation.NetworkElements,"NetworkElements");
		
		verifyExists(DeviceCreationObj.DeviceCreation.NewNetwkElement,"NewNetwkElement");
		click(DeviceCreationObj.DeviceCreation.NewNetwkElement,"NewNetwkElement");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		

		verifyExists(DeviceCreationObj.DeviceCreation.AddFromTemplate,"AddFromTemplate");
		click(DeviceCreationObj.DeviceCreation.AddFromTemplate,"AddFromTemplate");

		sendKeys(DeviceCreationObj.DeviceCreation.Title,Device_Model, "Title");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(DeviceCreationObj.DeviceCreation.Search,"Search");
		click(DeviceCreationObj.DeviceCreation.Search,"Search");

//		selectByVisibleText(DeviceCreationObj.DeviceCreation.Template,Template, "Template");
//		Reusable.waitForAjax();
//		Reusable.waitForpageloadmask();
		
		selectByVisibleText(DeviceCreationObj.DeviceCreation.DeviceRole,Device_Role, "DeviceRole");
		
		sendKeys(DeviceCreationObj.DeviceCreation.OrderNumber,Order_Number, "OrderNumber");
		
		sendKeys(DeviceCreationObj.DeviceCreation.Site,Site_ID, "Site");
		
		Reusable.SendkeaboardKeys(DeviceCreationObj.DeviceCreation.Site,Keys.ENTER);
		
		verifyExists(DeviceCreationObj.DeviceCreation.CreateSlot,"CreateSlot");
		click(DeviceCreationObj.DeviceCreation.CreateSlot,"CreateSlot");		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(DeviceCreationObj.DeviceCreation.Devices,"Devices");
		click(DeviceCreationObj.DeviceCreation.Devices,"Devices");
		
		String DeviceName=getTextFrom(DeviceCreationObj.DeviceCreation.L3cpeDeviceName);
		DataMiner.fnsetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo,"Device Name",DeviceName);
		
		verifyExists(DeviceCreationObj.DeviceCreation.L3cpeDeviceName,"L3cpeDeviceName");
		click(DeviceCreationObj.DeviceCreation.L3cpeDeviceName,"L3cpeDeviceName");
		
		String DeviceURL= getAttributeFrom(DeviceCreationObj.DeviceCreation.LinkforL3cpeDevice, "href");
		DataMiner.fnsetcolvalue(dataFileName, sheetName, scriptNo, dataSetNo,"Device URL",DeviceURL);
		if (Device_Model.contains("4431"))
		{
			verifyExists(DeviceCreationObj.DeviceCreation.Slots,"Slots");
			click(DeviceCreationObj.DeviceCreation.Slots,"Slots");
			
			verifyExists(DeviceCreationObj.DeviceCreation.SFPCombo0,"SFPCombo0");
			click(DeviceCreationObj.DeviceCreation.SFPCombo0,"SFPCombo0");
			
			verifyExists(DeviceCreationObj.DeviceCreation.SFPCombo1,"SFPCombo1");
			click(DeviceCreationObj.DeviceCreation.SFPCombo1,"SFPCombo1");
			
			verifyExists(DeviceCreationObj.DeviceCreation.InsertCards,"InsertCards");
			click(DeviceCreationObj.DeviceCreation.InsertCards,"InsertCards");			
			Reusable.waitForpageloadmask();
			
			verifyExists(DeviceCreationObj.DeviceCreation.InsertCardFromRepos,"InsertCardFromRepos");
			click(DeviceCreationObj.DeviceCreation.InsertCardFromRepos,"InsertCardFromRepos");	
			Reusable.waitForpageloadmask();
			
			verifyExists(DeviceCreationObj.DeviceCreation.L3cpeCard,"L3cpeCard");
			click(DeviceCreationObj.DeviceCreation.L3cpeCard,"L3cpeCard");	
			
			sendKeys(DeviceCreationObj.DeviceCreation.OrderNumber, Order_Number, "OrderNumber");
			
			verifyExists(DeviceCreationObj.DeviceCreation.Insert,"Insert");
			click(DeviceCreationObj.DeviceCreation.Insert,"Insert");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(DeviceCreationObj.DeviceCreation.Ports,"Ports");
			click(DeviceCreationObj.DeviceCreation.Ports,"Ports");
		}
		else if (Device_Model.contains("891F"))
		{
			verifyExists(DeviceCreationObj.DeviceCreation.Slots,"Slots");
			click(DeviceCreationObj.DeviceCreation.Slots,"Slots");
			
			verifyExists(DeviceCreationObj.DeviceCreation.SFPCombo8,"SFPCombo8");
			click(DeviceCreationObj.DeviceCreation.SFPCombo8,"SFPCombo8");
			
			verifyExists(DeviceCreationObj.DeviceCreation.InsertCards,"InsertCards");
			click(DeviceCreationObj.DeviceCreation.InsertCards,"InsertCards");
			Reusable.waitForpageloadmask();

			verifyExists(DeviceCreationObj.DeviceCreation.InsertCardFromRepos,"InsertCardFromRepos");
			click(DeviceCreationObj.DeviceCreation.InsertCardFromRepos,"InsertCardFromRepos");			
			Reusable.waitForpageloadmask();
			
			verifyExists(DeviceCreationObj.DeviceCreation.L3cpeCard,"L3cpeCard");
			click(DeviceCreationObj.DeviceCreation.L3cpeCard,"L3cpeCard");	
			
			sendKeys(DeviceCreationObj.DeviceCreation.OrderNumber, Order_Number, "OrderNumber");
			
			verifyExists(DeviceCreationObj.DeviceCreation.Insert,"Insert");
			click(DeviceCreationObj.DeviceCreation.Insert,"Insert");
			Reusable.waitForpageloadmask();
			
			verifyExists(DeviceCreationObj.DeviceCreation.Ports,"Ports");
			click(DeviceCreationObj.DeviceCreation.Ports,"Ports");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Filled Site");
		}
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: L3CPE Device Successfully Created");
	}
}