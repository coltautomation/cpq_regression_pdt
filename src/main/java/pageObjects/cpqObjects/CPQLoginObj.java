package pageObjects.cpqObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadingAndWritingTextFile;

public class CPQLoginObj {
	
	public static class Login
	{ 
		//Login page elements
		public static final String userNameTxb="@id=userId"; //Username
		public static final String passWordTxb="@name=Ecom_Password"; //Password
		public static final String loginBtn="@xpath=//button[@type='submit' and contains(text(), 'Login')]"; //Login Button		

		//Landing page elements
		public static final String ManImg="@id=logo-wrapper"; //Landing Page Header User Image
		
		public static final String headerOracleQuotelink ="@xpath=(//a[@class='commerce-sidebar-item'])[1]"; //landing page Header Transaction icon
		public static final String MyProfilelink = "@xpath=(//a[@class='header-item'])[4]";

		////Quote Submission to Siebel element
		public static final String GearIcon ="@xpath=//a[@class='header-item']//img[@title='Admin']"; //landing page Header Gear icon
		public static final String TransactionIcon ="@xpath=(//a[@class='header-item'])[3]"; //landing page Header Transaction icon
		//public static final String TransactionIcon ="@xpath=//a[contains(text(),'Transactions')]";
		public static final String QuoteID1 ="@xpath=//a[contains(text(),'"; //Approved Quote Id on transaction page
		public static final String QuoteID2 ="')]";
	}

}