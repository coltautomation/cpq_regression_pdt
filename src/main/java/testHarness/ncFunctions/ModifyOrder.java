package testHarness.ncFunctions;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.ModifyOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;


public class ModifyOrder extends SeleniumUtils 
{
	GlobalVariables g = new GlobalVariables();
	ReusableFunctions Reusable = new ReusableFunctions();
	public static String Features;
	public static String[] FeaturesArray;
	
	public void NcModifyOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
		String ScenarioType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String TypeofOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");		
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");		
		
		if (TypeofOrder.equalsIgnoreCase("Soft-Mod"))
		{
			if (ProductName.contains("Ethernet"))
			{
				if (ScenarioType.equalsIgnoreCase("BW Upgrade"))
				{
					SoftModCompositOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					UpdateDescription(testDataFile, sheetName, scriptNo, dataSetNo);
					EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else if (ScenarioType.equalsIgnoreCase("VLAN Tagging Mode"))
				{
					SoftModCompositOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					UpdateDescription(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					EditProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else
				{
					SoftModCompositOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					UpdateDescription(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					EditProductDeviceDetailsTagId(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
			}
			else if (ProductName.contains("IP Access"))
			{
				SoftModIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				UpdateDescription(testDataFile, sheetName, scriptNo, dataSetNo);
				if (ScenarioType.equalsIgnoreCase("BW Upgrade and Add LAN"))
				{
					EditIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					if (IpFormat.contains("IP"))
					{
						AddProviderFeature(testDataFile, sheetName, scriptNo, dataSetNo);
						ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
					}
					DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else if (ScenarioType.equalsIgnoreCase("Add LAN IP"))
				{
					if (IpFormat.contains("IP"))
					{
						IpAddressingFeatureUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
						AddProviderFeature(testDataFile, sheetName, scriptNo, dataSetNo);
						ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
					}
					DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
//					EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else if (ScenarioType.equalsIgnoreCase("Add PI and Update"))
				{
					if (IpFormat.contains("IP"))
					{
						AddProviderFeature(testDataFile, sheetName, scriptNo, dataSetNo);
						ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
					}
					DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
//					EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else if (ScenarioType.equalsIgnoreCase("Add PI and BGP update"))
				{
					if (IpFormat.contains("IP"))
					{
						AddProviderFeature(testDataFile, sheetName, scriptNo, dataSetNo);
						ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
					}
					AsNumberUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
//					EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
					DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else if (ScenarioType.equalsIgnoreCase("Modify IPv4 to IPv6"))
				{
					if (IpFormat.contains("IP"))
					{
						RemoveFeature(testDataFile, sheetName, scriptNo, dataSetNo);
						IpAddressingFeatureUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
						if (AddressingType.equalsIgnoreCase("PA"))
						{
							AddProviderFeature(testDataFile, sheetName, scriptNo, dataSetNo);
							ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
						}
						else if (AddressingType.equalsIgnoreCase("PI"))
						{
							AddProviderFeature(testDataFile, sheetName, scriptNo, dataSetNo);
							ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
						}
						DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
//						EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
						DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
						ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
						CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
					}
				}
			}
		}
	}

	public void SoftModCompositOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
		String CompOrdernumber=null;
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");

		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");		
		click(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");
		
		String[] arrOfStr = Ordernumber.split("#", 0);
		
		waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		
		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
// 		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
		String ProductInstancenumber = getTextFrom(ModifyOrderObj.ModifyOrder.ProductInstNumber);
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ProductInstNumber",arrOfStr1[1]);

		verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
		click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Accounts Page");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ProductInstTab,"Product Instance Tab");		
		click(ModifyOrderObj.ModifyOrder.ProductInstTab,"Product Instance Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,arrOfStr1[1]);

		verifyExists(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");		
		click(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ModifyCompositOrder,"Modify Composit Order");		
		click(ModifyOrderObj.ModifyOrder.ModifyCompositOrder,"Modify Composit Order");
		
		Reusable.waitForAjax();
		
		String ModifiedOrdernumber = getTextFrom(ModifyOrderObj.ModifyOrder.ModifiedNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_No",ModifiedOrdernumber);
		Report.LogInfo("Get","\""+ModifiedOrdernumber +"\" Is Created Successfully", "INFO");
		
		String ModifyOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modify_Order_URL", ModifyOrderscreenurl);
		System.out.println(ModifyOrderscreenurl);
	}


	public void SoftModIpOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
		String CompOrdernumber=null;
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");

		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");		
		click(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");
		
		String[] arrOfStr = Ordernumber.split("#", 0);
		
		waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		
		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
// 		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
		String ProductInstancenumber = getTextFrom(ModifyOrderObj.ModifyOrder.IpProductInstNumber);
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ProductInstNumber",arrOfStr1[1]);

		verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
		click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Accounts Page");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ProductInstTab,"Product Instance Tab");		
		click(ModifyOrderObj.ModifyOrder.ProductInstTab,"Product Instance Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,arrOfStr1[1]);

		verifyExists(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");		
		click(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ModifyCompositOrder,"Modify Composit Order");		
		click(ModifyOrderObj.ModifyOrder.ModifyCompositOrder,"Modify Composit Order");
		
		Reusable.waitForAjax();
		
		String ModifiedOrdernumber = getTextFrom(ModifyOrderObj.ModifyOrder.ModifiedNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_No",ModifiedOrdernumber);
		Report.LogInfo("Get","\""+ModifiedOrdernumber +"\" Is Created Successfully", "INFO");
		
		String ModifyOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modify_Order_URL", ModifyOrderscreenurl);
		System.out.println(ModifyOrderscreenurl);
	}
	

	public void UpdateDescription(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
		click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
		waitForAjax();
		
		verifyExists(ModifyOrderObj.ModifyOrder.EditDesc,"Edit Desc");		
		click(ModifyOrderObj.ModifyOrder.EditDesc,"Edit Desc");
		
		waitForAjax();
		verifyExists(ModifyOrderObj.ModifyOrder.OrderDescription,"Order Description");
		sendKeys(ModifyOrderObj.ModifyOrder.OrderDescription,"Modify Order created using CTAF Automation script");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
		click(ModifyOrderObj.ModifyOrder.Update,"Update");
		
		waitForAjax();
	}

	public void EditProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bandwidth");
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
		
		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.UndelyingModifiedOrder,"Underlying Modified Order");		
		click(ModifyOrderObj.ModifyOrder.UndelyingModifiedOrder,"Underlying Modified  Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
		click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");		
		clearTextBox(NcOrderObj.addProductDetails.ServiceBand);
//		Reusable.waitForAjax();
//		click(NcOrderObj.addProductDetails.ServiceBand,"Bandwidth");
		sendKeys(NcOrderObj.addProductDetails.ServiceBand,Bandwidth,"Service Bandwidth");
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand,Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitToPageLoad();

		sendKeys(NcOrderObj.addProductDetails.ServiceBand,Bandwidth);
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand, Keys.ENTER);

//		verifyExists(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");		
//		sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,Bandwidth,"Service Bandwidth");
//		Reusable.SendkeaboardKeys(ModifyOrderObj.ModifyOrder.ServiceBandwidth, Keys.ENTER);

		Reusable.waitForSiebelLoader();
		
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
		click(ModifyOrderObj.ModifyOrder.Update,"Update");
		Reusable.waitForAjax();
	}

	public void DecomposeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
	
		getUrl(ModifyOrderscreenurl);

		verifyExists(ModifyOrderObj.ModifyOrder.OrderTab,"Navigate to Orders Tab");		
		click(ModifyOrderObj.ModifyOrder.OrderTab,"Navigate to Orders Tab");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ModifySuborder,"Modify Sub order");		
		click(ModifyOrderObj.ModifyOrder.ModifySuborder,"Modify Sub order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Decompose,"Decompose");		
		click(ModifyOrderObj.ModifyOrder.Decompose,"Decompose");
		
		Reusable.waitForAjax();
	}

	public void EditProductDeviceDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String EndCheckExcel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"End Site Req Change");
		String AccessportRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Port Role");
		String VlanTaggingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN Tagging Mode");
		String VlanTagId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN Tag ID");
		
		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductAend,"Modify End Site Product A end");		
		click(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductAend,"Modify End Site Product A end");
		
		String EndCheck = getTextFrom(ModifyOrderObj.ModifyOrder.EndCheck);
		
		//EndCheck.set(GetText(getwebelement(xml.getlocator("//locators/EndCheck"))));
		
		if (EndCheck.equalsIgnoreCase(EndCheckExcel))
		{
			verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
			click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
			
			verifyExists(ModifyOrderObj.ModifyOrder.AccessPortLink,"AccessPortLink");		
			click(ModifyOrderObj.ModifyOrder.AccessPortLink,"AccessPortLink");
			
			verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
			click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
			
			verifyExists(ModifyOrderObj.ModifyOrder.AccessportRole,"Accessport Role");		
			selectByVisibleText(ModifyOrderObj.ModifyOrder.AccessportRole,AccessportRole,"A End Port Role");
			
			verifyExists(ModifyOrderObj.ModifyOrder.VlanTaggingMode,"VLAN Tagging Mode");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.VlanTaggingMode,VlanTaggingMode,"A End Vlan Tagging Mode");
			
			verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
			click(ModifyOrderObj.ModifyOrder.Update,"Update");

			waitForAjax();
			
			if(AccessportRole.contains("VLAN"))
			{
				verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
				click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb"); 
				
				verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
				click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
	
				verifyExists(ModifyOrderObj.ModifyOrder.AddFeaturelink,"Add Feature link");		
				click(ModifyOrderObj.ModifyOrder.AddFeaturelink,"Add Feature link");
				
			    sendKeys(ModifyOrderObj.ModifyOrder.TypeofFeature,"VLAN","Product Display");
			    Thread.sleep(30000);
			    Reusable.waitForAjax();
				click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
			    
				verifyExists(ModifyOrderObj.ModifyOrder.SelectFeature,"Feature selected");
				click(ModifyOrderObj.ModifyOrder.SelectFeature,"Feature selected");
	
//			verifyExists(ModifyOrderObj.ModifyOrder.TypeofFeature,"VLAN");		
//			sendKeys(ModifyOrderObj.ModifyOrder.TypeofFeature,TypeofFeature,"VLAN");
//			Reusable.waitForSiebelLoader();
//			Reusable.SendkeaboardKeys(ModifyOrderObj.ModifyOrder.TypeofFeature, Keys.ENTER);
//				
//			verifyExists(ModifyOrderObj.ModifyOrder.SelectFeature,"Select Feature");		
//			click(ModifyOrderObj.ModifyOrder.SelectFeature,"Select Feature");
//			
				waitForAjax();
				
				verifyExists(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");		
				click(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");
				
				verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
				click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
				
				verifyExists(ModifyOrderObj.ModifyOrder.VlanTagId,"VLAN Tag ID");
				sendKeys(ModifyOrderObj.ModifyOrder.VlanTagId,VlanTagId);
				
				verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
				click(ModifyOrderObj.ModifyOrder.Update,"Update");
				
				waitForAjax();
			}
		}
		else 
		{
			getUrl(ModifyOrderscreenurl);
		
			verifyExists(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductBend,"Modify End Site Product B end");		
			click(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductBend,"Modify End Site Product B end");
			
			verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
			click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
			waitForAjax();
			
			verifyExists(ModifyOrderObj.ModifyOrder.AccessPortLink,"Access Port Link");		
			click(ModifyOrderObj.ModifyOrder.AccessPortLink,"Access Port Link");
			
			verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
			click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
			
			verifyExists(ModifyOrderObj.ModifyOrder.AccessportRole,"Accessport Role");		
			selectByVisibleText(ModifyOrderObj.ModifyOrder.AccessportRole,AccessportRole,"B End Port Role");
			
			verifyExists(ModifyOrderObj.ModifyOrder.VlanTaggingMode,"VLAN Tagging Mode");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.VlanTaggingMode,VlanTaggingMode,"B End Vlan Tagging Mode");
			
			verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
			click(ModifyOrderObj.ModifyOrder.Update,"Update");

			Reusable.waitForAjax();
			
			if(AccessportRole.contains("VLAN"))
			{
				verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
				click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb"); 
				
				verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
				click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(ModifyOrderObj.ModifyOrder.AddFeaturelink,"Add Feature link");		
				click(ModifyOrderObj.ModifyOrder.AddFeaturelink,"Add Feature link");
				
				verifyExists(ModifyOrderObj.ModifyOrder.TypeofFeature,"VLAN");		
				sendKeys(ModifyOrderObj.ModifyOrder.TypeofFeature,"VLAN");
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(ModifyOrderObj.ModifyOrder.TypeofFeature, Keys.ENTER);
				
				verifyExists(ModifyOrderObj.ModifyOrder.SelectFeature,"Select Feature");		
				click(ModifyOrderObj.ModifyOrder.SelectFeature,"Select Feature");
				
				waitForAjax();
				
				verifyExists(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");		
				click(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");
				
				verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
				click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
				
				verifyExists(ModifyOrderObj.ModifyOrder.VlanTagId,"VLAN Tag ID");
				sendKeys(ModifyOrderObj.ModifyOrder.VlanTagId,VlanTagId);
				
	//			SendKeys(getwebelement(xml.getlocator("//locators/SiteProductDetail/VLAN/Ethertype")),"VLAN (0x8100)");
				
				verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
				click(ModifyOrderObj.ModifyOrder.Update,"Update");
				
				Reusable.waitForAjax();
			}
		}
	}

	public void EditProductDeviceDetailsTagId(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String VlanTagId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN Tag ID");
		String EndCheckExcel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"End Site Req Change");

		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductAend,"Modify End Site Product A end");		
		click(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductAend,"Modify End Site Product A end");

		String EndCheck = getTextFrom(ModifyOrderObj.ModifyOrder.EndCheck);
	
		if (EndCheck.equalsIgnoreCase(EndCheckExcel))
		{
			verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
			click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
			
			waitForAjax();
			
			verifyExists(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");		
			click(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");
			
			verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
			click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
			
			//getwebelement(xml.getlocator("//locators/VlanTagId")).clear();
			
			verifyExists(ModifyOrderObj.ModifyOrder.VlanTagId,"VLAN Tag ID");
			clearTextBox(ModifyOrderObj.ModifyOrder.VlanTagId);
			Reusable.waitForAjax();
			sendKeys(ModifyOrderObj.ModifyOrder.VlanTagId,VlanTagId);
			waitForAjax();

//			verifyExists(ModifyOrderObj.ModifyOrder.Ethertype,"Ether type");
//			String etherTypeclr= ModifyOrderObj.ModifyOrder.Ethertype;
//			clearTextBox(etherTypeclr);
//		    sendKeys(ModifyOrderObj.ModifyOrder.Ethertype,"VLAN (0x8100)","Ether Type");

//		    verifyExists(ModifyOrderObj.ModifyOrder.Ethertype,"VLAN (0x8100)");	
//			sendKeys(ModifyOrderObj.ModifyOrder.Ethertype,"VLAN (0x8100)");

		    verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
			click(ModifyOrderObj.ModifyOrder.Update,"Update");
		}
		else
		{
			getUrl(ModifyOrderscreenurl);
			
//			verifyExists(ModifyOrderObj.ModifyOrder.OrderTab,"Order Tab");		
//			click(ModifyOrderObj.ModifyOrder.OrderTab,"Order Tab");
//			
			verifyExists(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductBend,"Modify End Site Product B end");		
			click(ModifyOrderObj.ModifyOrder.ModifyEndSiteProductBend,"Modify End Site Product B end");
			
			verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
			click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
			
			verifyExists(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");		
			click(ModifyOrderObj.ModifyOrder.VLANLink,"VLANLink");
		
			verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
			click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
			
			verifyExists(ModifyOrderObj.ModifyOrder.VlanTagId,"VLAN Tag ID");
			clearTextBox(ModifyOrderObj.ModifyOrder.VlanTagId);
			Reusable.waitForSiebelLoader();
			sendKeys(ModifyOrderObj.ModifyOrder.VlanTagId,VlanTagId);
			waitForAjax();
			
//			verifyExists(ModifyOrderObj.ModifyOrder.Ethertype,"Ether type");
//			String etherTypeclr= ModifyOrderObj.ModifyOrder.Ethertype;
//			clearTextBox(etherTypeclr);
//		    sendKeys(ModifyOrderObj.ModifyOrder.Ethertype,"VLAN (0x8100)","Ether Type");

//			verifyExists(ModifyOrderObj.ModifyOrder.Ethertype,"VLAN (0x8100)");	
//			sendKeys(ModifyOrderObj.ModifyOrder.Ethertype,"VLAN (0x8100)");
			
			verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
			click(ModifyOrderObj.ModifyOrder.Update,"Update");
		}
	}
	
	public void EditIpOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bandwidth");
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");

		getUrl(ModifyOrderscreenurl);

		verifyExists(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");		
		click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
		click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
		
		verifyExists(ModifyOrderObj.ModifyOrder.IpServiceBandwidth,"Service Bandwidth");		
//		clearTextBox(ModifyOrderObj.ModifyOrder.IpServiceBandwidth);
		Reusable.waitForAjax();
//		Reusable.waitForpageloadmask();
//		click(NcOrderObj.addProductDetails.ServiceBand,"Bandwidth");
		selectByVisibleText(ModifyOrderObj.ModifyOrder.IpServiceBandwidth,Bandwidth,"Service Bandwidth");
		verifyExists(NcOrderObj.addProductDetails.ServiceBandwidth,"Service Bandwidth");
//		selectByVisibleText(NcOrderObj.addProductDetails.ServiceBandwidth,ServiceBandwidth,"Service Bandwidth");

//		Reusable.SendkeaboardKeys(ModifyOrderObj.ModifyOrder.IpServiceBandwidth, Keys.ENTER);
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
		click(ModifyOrderObj.ModifyOrder.Update,"Update");
		Reusable.waitForAjax();
	}


	public void RemoveFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String FeatureRemoval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Removal");
		
		String[] FeaturesArray = FeatureRemoval.split(",");
//		int TotalFeatures = FeaturesArray.length;

		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");		
		click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
		click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
		
		waitForAjax();
	    for(int j=0;j<FeaturesArray.length;j++)
	    {
	    	click(("@xpath=//span[contains(text(),'"+FeaturesArray[j]+"')]/parent::*/parent::*/parent::*//input"), "Selected Feature");
//	    	click(("@xpath=//span[contains(text(),'"+FeaturesArray+"')]/parent::*/parent::*/parent::*//input["+(j+1)+"]"), "Selected Feature");
	    	Reusable.waitForSiebelLoader();
			click(ModifyOrderObj.ModifyOrder.DeleteFeature,"Delete Feature button");
			clickConfirmBox("OK");
	    	Reusable.waitForAjax();
	    }
	}
	
	public void IpAddressingFeatureUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		
		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");		
		click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
		click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
		
		waitForAjax();
		
		verifyExists(ModifyOrderObj.ModifyOrder.IpAddressingLink,"IP Addressing Link");
		click(ModifyOrderObj.ModifyOrder.IpAddressingLink,"IP Addressing Link");
		Reusable.waitForAjax();
		verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
		click(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
		Reusable.waitForAjax();
		verifyExists(ModifyOrderObj.ModifyOrder.IpAddressingFormat,"IP Addressing Format");
		click(ModifyOrderObj.ModifyOrder.IpAddressingFormat,"IP Addressing Format");
		Reusable.waitForSiebelLoader();
		if (IpFormat.toString().contains("IPv4"))
		{
			selectByVisibleText(ModifyOrderObj.ModifyOrder.IpAddressingFormat,"IPv4 format only","IPv4 Format");
		}
		else if (IpFormat.toString().contains("IPv6"))
		{
			selectByVisibleText(ModifyOrderObj.ModifyOrder.IpAddressingFormat,"IPv6 format only","IPv6 Format");
		}
		else
		{
			selectByVisibleText(ModifyOrderObj.ModifyOrder.IpAddressingFormat,"IPv4 and IPv6 format","Both IP Format");
		}
			
		if (IpFormat.toString().contains("IPv4"))
		{
			verifyExists(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,"IPv4 Addressing Type");
			click(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,"IPv4 Addressing Type");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,AddressingType,"IPv4 Addressing Type");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,AddressingType,"");
		}
		else if (IpFormat.toString().contains("IPv6"))
		{
			verifyExists(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,"IPv6 Addressing Type");
//			click(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,"IPv6 Addressing Type");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,AddressingType,"IPv6 Addressing Type");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,AddressingType,"");
		}
		else
		{
			verifyExists(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,"IPv4 Addressing Type");
//			click(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,"IPv4 Addressing Type");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.Ipv4AddressingType,AddressingType,"IPv4 Addressing Type");

			Reusable.waitForAjax();
			verifyExists(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,"IPv6 Addressing Type");
//			click(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,"IPv6 Addressing Type");
			selectByVisibleText(ModifyOrderObj.ModifyOrder.Ipv6AddressingType,AddressingType,"IPv6 Addressing Type");
		}
		Reusable.waitForAjax();
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update button");
		click(ModifyOrderObj.ModifyOrder.Update,"Update button");
		Reusable.waitForAjax();
	}
		
	public void AddProviderFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		
		if (AddressingType.toString().trim().equalsIgnoreCase("PA"))
		{
			getUrl(Orderscreenurl);
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,"Provider Aggregated IPv4");
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    if (IpFormat.toString().trim().equalsIgnoreCase("IPv4"))
		    {
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
			    click(("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input"), "Provider Aggregated IPv4");
			    Reusable.waitForAjax();
			     
			    webDriver.switchTo().window(curent);
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

			    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
				click(NcOrderObj.generalInformation.SelectButton,"Select button");
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();

				// switch back to parent window
			    webDriver.switchTo().window(handleBefore);
			    Reusable.waitForAjax();
		    }
		    else if (IpFormat.toString().trim().equalsIgnoreCase("IPv6"))
		    {
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
			    click(("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input"), "Provider Aggregated IPv6");
			    Reusable.waitForAjax();
			     
			    webDriver.switchTo().window(curent);
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

			    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
				click(NcOrderObj.generalInformation.SelectButton,"Select button");
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();

				// switch back to parent window
			    webDriver.switchTo().window(handleBefore);
			    Reusable.waitForAjax();
		    }
			else
	        {
	            // Perform the actions on new window
	            webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	            verifyExists("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input","Provider Aggregated IPv4");
	            //ScrollIntoViewByString("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input");
	            click("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input", "Provider Aggregated IPv4");
	            Reusable.waitForAjax();
	            
	            verifyExists("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input","Provider Aggregated IPv6");
	            ScrollIntoViewByString("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input");
	            click("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input", "Provider Aggregated IPv6");
	            Reusable.waitForAjax();

	            webDriver.switchTo().window(curent);
	            webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	            verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
	            click(NcOrderObj.generalInformation.SelectButton,"Select button");
	            Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
	            Reusable.waitForAjax();

	            // switch back to parent window
	            //switchToDefaultFrame();
	            //verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
	            webDriver.switchTo().window(handleBefore);
	            Reusable.waitForAjax();
	        }
		}
		else if (AddressingType.toString().trim().equalsIgnoreCase("PI"))
		{
			getUrl(Orderscreenurl);
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,"Provider Independent IPv4");
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    if (IpFormat.toString().trim().equalsIgnoreCase("IPv4"))
		    {
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
			    click(("@xpath=//span[text()='Provider Independent IPv4']/parent::*/parent::*/parent::*/td//input"), "Provider Independent IPv4");
			    Reusable.waitForAjax();
			     
			    webDriver.switchTo().window(curent);
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

			    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
				click(NcOrderObj.generalInformation.SelectButton,"Select button");
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();

				// switch back to parent window
			    webDriver.switchTo().window(handleBefore);
			    Reusable.waitForAjax();
		    }
		    else if (IpFormat.toString().trim().equalsIgnoreCase("IPv6"))
		    {
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
			    click(("@xpath=//span[text()='Provider Independent IPv6']/parent::*/parent::*/parent::*/td//input"), "Provider Independent IPv6");
			    Reusable.waitForAjax();
			     
			    webDriver.switchTo().window(curent);
			    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

			    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
				click(NcOrderObj.generalInformation.SelectButton,"Select button");
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();

				// switch back to parent window
			    webDriver.switchTo().window(handleBefore);
			    Reusable.waitForAjax();
		    }
			else
	        {
	            // Perform the actions on new window
	            webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	            verifyExists("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input","Provider Independent IPv4");
	            //ScrollIntoViewByString("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input");
	            click("@xpath=//span[text()='Provider Independent IPv4']/parent::*/parent::*/parent::*/td//input", "Provider Independent IPv4");
	            Reusable.waitForAjax();
	            
	            verifyExists("@xpath=//span[text()='Provider Independent IPv6']/parent::*/parent::*/parent::*/td//input","Provider Independent IPv6");
	            ScrollIntoViewByString("@xpath=//span[text()='Provider Independent IPv6']/parent::*/parent::*/parent::*/td//input");
	            click("@xpath=//span[text()='Provider Independent IPv6']/parent::*/parent::*/parent::*/td//input", "Provider Independent IPv6");
	            Reusable.waitForAjax();

	            webDriver.switchTo().window(curent);
	            webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	            verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
	            click(NcOrderObj.generalInformation.SelectButton,"Select button");
	            Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
	            Reusable.waitForAjax();

	            // switch back to parent window
	            //switchToDefaultFrame();
	            //verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
	            webDriver.switchTo().window(handleBefore);
	            Reusable.waitForAjax();
	        }
		}
	}

	public void ProviderAggregateUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		Random rand = new Random();
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String NoIpAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv4/IPv6 Addresses");
//		String NoIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv4/IPv6 Addresses");
		String PrimaryRangeReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary Range");

		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");		
		click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
		click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
		
		waitForAjax();
	
		if (IpFormat.contains("IPv4"))
		{
		    List<WebElement> linkList = GetWebElements(ModifyOrderObj.ModifyOrder.ProviderAggIpv4Link);
		    for(int j=0;j<linkList.size();j++)
		    {
		    	javaScriptClickWE(webDriver.findElement(By.xpath("//span[contains(text(),'Provider Aggregated')]/parent::*["+(j+1)+"]")),"PA");
//				click(ModifyOrderObj.ModifyOrder.ProviderAggIpv4Link), String.valueOf(j));
				Reusable.waitForAjax();

				verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
				click(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
				Reusable.waitForAjax();
				
				if(!getAttributeFrom((ModifyOrderObj.ModifyOrder.SiebelCompId),"value").equals(""))
                {
					getUrl(ModifyOrderscreenurl);
					
					click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
					click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
					Reusable.waitForAjax();
                }
				else
				{
					verifyExists(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,"Number of IPv4 Addresses");
					selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,NoIpAddress,"NoIpv4Address");

					verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
					selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
					
					int randnumb = rand.nextInt(1000000000);
					verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
					sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
				}
		    }
		}
		else if (IpFormat.contains("IPv6"))
		{
		    List<WebElement> linkList1 = GetWebElements(ModifyOrderObj.ModifyOrder.ProviderAggIpv6Link);
		    for(int j=0;j<linkList1.size();j++)
		    {
		    	javaScriptClickWE(webDriver.findElement(By.xpath("//span[contains(text(),'Provider Aggregated')]/parent::*["+(j+1)+"]")),"PA");
//				click(findWebElement("@xpath=//span[contains(text(),'Provider Aggregated')]/parent::*["+(j+1)+"]");
//				click(ModifyOrderObj.ModifyOrder.ProviderAggIpv4Link), String.valueOf(j));
				Reusable.waitForAjax();

				verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
				click(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
				Reusable.waitForAjax();
				
				if(!getAttributeFrom((ModifyOrderObj.ModifyOrder.SiebelCompId),"value").equals(""))
                {
					getUrl(ModifyOrderscreenurl);
					
					click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
					click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
					Reusable.waitForAjax();
                }
				else
				{
					verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
					selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpAddress,"NoIpv6Address");

					verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
					selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
					
					int randnumb = rand.nextInt(1000000000);
					verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
					sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
				}
		    }
		}
//		else
//		{
//			verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
//			click(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
//			Reusable.waitForAjax();
//			
//		}
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update button");
		click(ModifyOrderObj.ModifyOrder.Update,"Update button");
		Reusable.waitForAjax();
	}
	
	public void ProviderIndependentUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
//		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String IpPrefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Prefix Inp");
		String LanIpAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN Inter IPv4/IPv6 Address");
		String NoIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv6 Addresses");
		String PrimaryRangeReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary Range");
		String TypeofPiAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PI IPv4/IPv6 Addresses");		
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		
		getUrl(ModifyOrderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (IpFormat.contains("IPv4"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			sendKeys(NcOrderObj.addProductDetails.PrefixInput,IpPrefix,"Ipv4Prefix");

			String ipAddressValue[]=LanIpAddress.split("\\.");
			for(int i=0;i<ipAddressValue.length;i++)
			{
				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,"Lan Interface IPv4 Address");
//				sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,["+(i+1)+"],ipAddressValue[i]);
				SendKeys(findWebElement("@xpath=//td/a[(text()='LAN Interface IPv4 Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),ipAddressValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else if (IpFormat.contains("IPv6"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			sendKeys(NcOrderObj.addProductDetails.PrefixInput,IpPrefix,"Ipv6Prefix");
			
			verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,"Lan Interface IPv6 Address");
			sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,LanIpAddress,"LAN Interface IPv6 Address");

//			String ipAddressValue[]=LanIpv6Address.split("\\.");
//			for(int i=0;i<ipAddressValue.length;i++)
//			{
//				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,"Lan Interface IPv6 Address");
//				SendKeys(findWebElement("@xpath=(//td/a[(text()='LAN Interface IPv6 Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),ipAddressValue[i]);
//				Reusable.waitForAjax();
//			}

			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");
			
			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv6Addresses,"Type of PI IPv6 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv6Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			selectByVisibleText(NcOrderObj.addProductDetails.PrefixInput,IpPrefix,"Ipv4Prefix");

			String ipAddressValue[]=LanIpAddress.split("\\.");
			for(int i=0;i<ipAddressValue.length;i++)
			{
				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,"Lan Interface IPv4 Address");
//				sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,["+(i+1)+"],ipAddressValue[i]);
				SendKeys(findWebElement("(//td/a[(text()='LAN Interface IPv4 Address')]/parent::*/following-sibling::*//input)["+(i+1)+"]"),ipAddressValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			selectByVisibleText(NcOrderObj.addProductDetails.PrefixInput,IpPrefix,"Ipv6Prefix");

			String ipAddressValue1[]=LanIpAddress.split("\\.");
			for(int i=0;i<ipAddressValue1.length;i++)
			{
				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,"Lan Interface IPv4 Address");
//				sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,["+(i+1)+"],ipAddressValue[i]);
				SendKeys(findWebElement("(//td/a[(text()='LAN Interface IPv6 Address')]/parent::*/following-sibling::*//input)["+(i+1)+"]"),ipAddressValue1[i]);
				Reusable.waitForAjax();
			}
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");
			
			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
//			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void AddBgpFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
//		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		
		getUrl(ModifyOrderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,"BGP4 Feed");
		Reusable.waitForAjax();

		String handleBefore = webDriver.getWindowHandle();

		verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
		click(NcOrderObj.generalInformation.Browse,"Browse button");
		Reusable.waitForAjax();

		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		
//			switchWindow(urlContent, handles);
		
	    System.out.println("Window handel"+curent);
	    webDriver.switchTo().window(curent);

	    // Perform the actions on new window
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	    click(("@xpath=//span[text()='BGP4 Feed']/parent::*/parent::*/parent::*/td//input"), "BGP4 Feed");
	    Reusable.waitForAjax();
	     
	    webDriver.switchTo().window(curent);
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
		click(NcOrderObj.generalInformation.SelectButton,"Select button");
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

		// switch back to parent window
	    webDriver.switchTo().window(handleBefore);
	    Reusable.waitForAjax();
	}

	public void BgpFeedUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String ASNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AS Number");
		String BgpFeedType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP4 Feed Type");
//		String ASType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of AS");
//		String BgpFeedReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP4 Feed Required");
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		
		getUrl(ModifyOrderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addBgpFeed.Bgp4FeedLink,"BGP4 Feed link");
		click(NcOrderObj.addBgpFeed.Bgp4FeedLink,"BGP4 Feed link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addBgpFeed.TypeofAS,"Type of AS");
		selectByVisibleText(NcOrderObj.addBgpFeed.TypeofAS,"New","ASType");
		
		verifyExists(NcOrderObj.addBgpFeed.ASNumber,"AS Number");
		sendKeys(NcOrderObj.addBgpFeed.ASNumber,ASNumber);
	
		verifyExists(NcOrderObj.addBgpFeed.BGP4FeedType,"BGP4 Feed Type");
		selectByVisibleText(NcOrderObj.addBgpFeed.BGP4FeedType,BgpFeedType,"BgpFeedType");
		
		verifyExists(NcOrderObj.addBgpFeed.BGP4FeedReq,"BGP4 Feed Required");
		selectByVisibleText(NcOrderObj.addBgpFeed.BGP4FeedReq,"Yes","BgpFeedReq");
		Reusable.waitForAjax();
		
		int randnumb = rand.nextInt(1000000000);
		verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
		sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}

	public void AsNumberUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String ASNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AS Number");
		String BgpFeedType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP4 Feed Type");

		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");		
		click(ModifyOrderObj.ModifyOrder.UnderlyingModifiedIpOrder,"Underlying Modified IP Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");		
		click(ModifyOrderObj.ModifyOrder.GeneralInformationTab,"General Information Tab");
		
		waitForAjax();
	
		verifyExists(ModifyOrderObj.ModifyOrder.Bgp4FeedLink,"BGP4 Feed Link");		
		click(ModifyOrderObj.ModifyOrder.Bgp4FeedLink,"BGP4 Feed Link");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
		click(ModifyOrderObj.ModifyOrder.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(ModifyOrderObj.ModifyOrder.ASNumber,"AS Number");
		sendKeys(NcOrderObj.addBgpFeed.ASNumber,ASNumber);
	
		verifyExists(NcOrderObj.addBgpFeed.BGP4FeedType,"BGP4 Feed Type");
		selectByVisibleText(NcOrderObj.addBgpFeed.BGP4FeedType,BgpFeedType,"BgpFeedType");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update button");
		click(ModifyOrderObj.ModifyOrder.Update,"Update button");
		Reusable.waitForAjax();
	}
	
	public void DecomposeIpOrder (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException	
	{
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		
		getUrl(ModifyOrderscreenurl);
		
        verifyExists(ModifyOrderObj.ModifyOrder.OrderTab,"Orders Tab");
        click(ModifyOrderObj.ModifyOrder.OrderTab,"Orders Tab");
       
        verifyExists(ModifyOrderObj.ModifyOrder.ModifySubIpOrder,"IP Access Product Order");
        click(ModifyOrderObj.ModifyOrder.ModifySubIpOrder,"IP Access Product Order");
       
        verifyExists(ModifyOrderObj.ModifyOrder.Decompose,"Click on Decompose button");
        click(ModifyOrderObj.ModifyOrder.Decompose,"Click on Decompose button");
        waitForAjax();
	}

	public void ProcessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String ModifiedOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_No");

		getUrl(ModifyOrderscreenurl);
        
		verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account Composite Orders tab");		
		click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account Composite Orders tab");
		
		verifyExists(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Sorting of Orders");		
		click(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Sorting of Orders");

//		String[] arrOfStr2 = ModifiedOrdernumber.split("#", 0);
//		
//		verifyExists(ModifyOrderObj.ModifyOrder.arrStr1+ arrOfStr2 +ModifyOrderObj.ModifyOrder.arrStr2);
//		click(ModifyOrderObj.ModifyOrder.arrStr1+ arrOfStr2 +ModifyOrderObj.ModifyOrder.arrStr2);
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,ModifiedOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(ModifyOrderObj.ModifyOrder.StartProccessing,"Start Proccessing");		
		click(ModifyOrderObj.ModifyOrder.StartProccessing,"Start Proccessing");
		Reusable.waitForAjax();
	}
	
	public void CompleteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
	{
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String ModifiedOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modify_Order_No");
		String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify Work Items");
		String[] arrOfStr2 = ModifiedOrdernumber.split("#", 0); 
		
//		verifyExists(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Account Name Sorting");		
//		click(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Account Name Sorting");
//		
//		//Clickon(getwebelement("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*"));
//		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr2[1] +ModifyOrderObj.ModifyOrder.Order2);
//		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr2[1] +ModifyOrderObj.ModifyOrder.Order2);
		
		getUrl(ModifyOrderscreenurl);
		
		verifyExists(ModifyOrderObj.ModifyOrder.TaskTab,"Task Tab");		
		click(ModifyOrderObj.ModifyOrder.TaskTab,"Task Tab");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ExecutionFlowlink,"Execution Flow link");		
		click(ModifyOrderObj.ModifyOrder.ExecutionFlowlink,"Execution Flow link");
		
		Reusable.waitForSiebelLoader();
		
		verifyExists(ModifyOrderObj.ModifyOrder.Workitems,"Work items");		
		click(ModifyOrderObj.ModifyOrder.Workitems,"Work items");
		
		
		waitForAjax();
		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
		{
            waitForElementToAppear(ModifyOrderObj.ModifyOrder.TaskReadytoComplete,120,10000);
            click(ModifyOrderObj.ModifyOrder.TaskReadytoComplete,"Workitem in Ready status");
            Completeworkitem(GetText(ModifyOrderObj.ModifyOrder.ModTaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);         

//	        verifyExists(ModifyOrderObj.ModifyOrder.TaskReadytoComplete,"Task Ready to Complete");
//			waitForElementToBeVisible(ModifyOrderObj.ModifyOrder.TaskReadytoComplete, 60);
//			click(ModifyOrderObj.ModifyOrder.TaskReadytoComplete,"Task Ready to Complete");
//			Completworkitem(GetText2(getwebelement(xml.getlocator("//locators/Tasks/ModTaskTitle"))),Inputdata);
		}
		
		getUrl(ModifyOrderscreenurl);
	       
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
        verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
        click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");

		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ ModifiedOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,150,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ ModifiedOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
		if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+ModifiedOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, ModifiedOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+ModifiedOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, ModifiedOrdernumber +" Execution InProgress");
		}
	}

	public void InflightSoftModCompositOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Orderscreenurl = getTextFrom(ModifyOrderObj.ModifyOrder.LinkforOrder,"href");
		
		getUrl(Orderscreenurl);
		verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
		click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");
		
		
		verifyExists(ModifyOrderObj.ModifyOrder.NameFiltering,"Name Filtering");		
		click(ModifyOrderObj.ModifyOrder.NameFiltering,"Name Filtering");
		
		verifyExists(ModifyOrderObj.ModifyOrder.FilterSelectName,"Filter Select Name");
		select(ModifyOrderObj.ModifyOrder.FilterSelectName,"Filter Select Name");
		
//		CompOrdernumber=CompositeOrderHelper.Ordernumber;
		
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
		String[] arrOfStr = Ordernumber.split("#", 0); 
		
		//SendKeys(getwebelement(xml.getlocator("//locators/FilterInputValue")),""+arrOfStr[1]+"");
		
		verifyExists(ModifyOrderObj.ModifyOrder.FilterInputValue,"Filter Input Value");	
		sendKeys(ModifyOrderObj.ModifyOrder.FilterInputValue,arrOfStr[1],"Filter Input Value");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");		
		click(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		
		//Clickon(getwebelement("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"));
		
		String EthernetProductOrdernumber = getTextFrom(ModifyOrderObj.ModifyOrder.EthernetConnProdOrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Ethernet Product Order number",EthernetProductOrdernumber);
		
		//EthernetProductOrdernumber.set(GetText(getwebelement(xml.getlocator("//locators/EthernetConnProdOrderNumber"))));
		
		String[] arrOfStr1 = EthernetProductOrdernumber.split("#", 0); 
		
		verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
		click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");
		
		
		verifyExists(ModifyOrderObj.ModifyOrder.ProductOrdrTab,"Product OrdrTab");		
		click(ModifyOrderObj.ModifyOrder.ProductOrdrTab,"Product OrdrTab");
		
		verifyExists(ModifyOrderObj.ModifyOrder.NameFiltering,"Name Filtering");		
		click(ModifyOrderObj.ModifyOrder.NameFiltering,"Name Filtering");
		
		verifyExists(ModifyOrderObj.ModifyOrder.FilterSelectName,"Filter Select Name");
		select(ModifyOrderObj.ModifyOrder.FilterSelectName,"Filter Select Name");
		
		//SendKeys(getwebelement(xml.getlocator("//locators/FilterInputValue")),""+arrOfStr[1]+"");
		
		verifyExists(ModifyOrderObj.ModifyOrder.FilterInputValue,"FilterInputValue");		
		sendKeys(ModifyOrderObj.ModifyOrder.FilterInputValue,arrOfStr1[1]);
		
		Reusable.waitForSiebelLoader();
		
		verifyExists(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");		
		click(ModifyOrderObj.ModifyOrder.ApplyButton,"Apply Button");
		
		Reusable.waitForSiebelLoader();
		
		//Clickon(getwebelement("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input"));
		verifyExists(ModifyOrderObj.ModifyOrder.arrStr1+ arrOfStr +ModifyOrderObj.ModifyOrder.arrStr2);
		click(ModifyOrderObj.ModifyOrder.arrStr1+ arrOfStr +ModifyOrderObj.ModifyOrder.arrStr2);
		
		Reusable.waitForSiebelLoader();
		
		verifyExists(ModifyOrderObj.ModifyOrder.InflightSoftProdOrder,"In flight Soft Prod Order");		
		click(ModifyOrderObj.ModifyOrder.InflightSoftProdOrder,"In flight Soft Prod Order");
		Reusable.waitForSiebelLoader();

	}
	
	public void EditInflightProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Orderscreenurl = getTextFrom(ModifyOrderObj.ModifyOrder.LinkforOrder,"href");
		//Orderscreenurl.set(getAttributeFrom(ModifyOrderObj.ModifyOrder.LinkforOrder,"href"));
		
		verifyExists(ModifyOrderObj.ModifyOrder.UndelyingInfligtOrder,"Undelying In fligt Order");		
		click(ModifyOrderObj.ModifyOrder.UndelyingInfligtOrder,"Undelying In fligt Order");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Edit,"Edit");		
		click(ModifyOrderObj.ModifyOrder.Edit,"Edit");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");		
		clearTextBox(ModifyOrderObj.ModifyOrder.ServiceBandwidth);
		
		//getwebelement(xml.getlocator("//locators/ServiceBandwidth")).clear();
		
		Reusable.waitForSiebelLoader();
		
		String Service_bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bandwidth");
		verifyExists(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");	
		sendKeys(ModifyOrderObj.ModifyOrder.ServiceBandwidth,Service_bandwidth,"Service Bandwidth");
		
		waitForAjax();
		
		verifyExists(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");
		sendKeys(ModifyOrderObj.ModifyOrder.ServiceBandwidth,Service_bandwidth,"Service Bandwidth");
		SendkeyusingAction(Keys.ENTER);
		
		verifyExists(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");	
		sendKeys(ModifyOrderObj.ModifyOrder.ServiceBandwidth,"Service Bandwidth");
		
		waitForAjax();
		
		verifyExists(ModifyOrderObj.ModifyOrder.Update,"Update");		
		click(ModifyOrderObj.ModifyOrder.Update,"Update");
		
		openurl(Orderscreenurl);
		
		Reusable.waitForSiebelLoader();
		//webDriver.get(Configuration.Orderscreen_URL);
	}
	
	public void DecomposeInflightOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Orderscreenurl = getTextFrom(ModifyOrderObj.ModifyOrder.LinkforOrder,"href");
		
		verifyExists(ModifyOrderObj.ModifyOrder.OrderTab,"Order Tab");		
		click(ModifyOrderObj.ModifyOrder.OrderTab,"Order Tab");
		
		verifyExists(ModifyOrderObj.ModifyOrder.InflightSuborder,"In flight Suborder");		
		click(ModifyOrderObj.ModifyOrder.InflightSuborder,"In flight Suborder");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Decompose,"Decompose");		
		click(ModifyOrderObj.ModifyOrder.Decompose,"Decompose");
		
		waitForAjax();
		
		getUrl(Orderscreenurl);
		//webDriver.get(Configuration.Orderscreen_URL);
		
	}
	
	
	public void ProcessInflightOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
	
		verifyExists(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");		
		click(ModifyOrderObj.ModifyOrder.Accountbredcrumb,"Account bredcrumb");
		
		
		verifyExists(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Account Name Sorting");		
		click(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Account Name Sorting");
		
		String CompOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		
		String[] arrOfStr = CompOrdernumber.split("#", 0);
		
		//String[] arrOfStr = CompOrdernumber.get().split("#", 0); 
		
		verifyExists(ModifyOrderObj.ModifyOrder.arrStr1+ arrOfStr +ModifyOrderObj.ModifyOrder.arrStr2);
		click(ModifyOrderObj.ModifyOrder.arrStr1+ arrOfStr +ModifyOrderObj.ModifyOrder.arrStr2);
			
		//Clickon(getwebelement("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input"));
		
		verifyExists(ModifyOrderObj.ModifyOrder.StartProccessing,"Start Proccessing");		
		click(ModifyOrderObj.ModifyOrder.StartProccessing,"Start Proccessing");
		
		waitForAjax();
	}

	public void CompleteInflightOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ModifyOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify_Order_URL");
		String ModifiedOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modified Order Number");
		String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modify Work Items");

		String[] arrOfStr = ModifiedOrdernumber.split("#", 0); 
		
		verifyExists(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Account Name Sorting");		
		click(ModifyOrderObj.ModifyOrder.AccountNameSorting,"Account Name Sorting");
		
		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		waitForElementToPresent(By.xpath("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Started')]"),60);
		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		
		getUrl(ModifyOrderscreenurl);
		verifyExists(ModifyOrderObj.ModifyOrder.TaskTab,"Task Tab");		
		click(ModifyOrderObj.ModifyOrder.TaskTab,"Task Tab");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ExecutionFlowlink,"Execution Flow link");		
		click(ModifyOrderObj.ModifyOrder.ExecutionFlowlink,"Execution Flow link");
	
		Reusable.waitForSiebelLoader();
		
		verifyExists(ModifyOrderObj.ModifyOrder.Workitems,"Work items");		
		click(ModifyOrderObj.ModifyOrder.Workitems,"Work items");
		
		Reusable.waitForSiebelLoader();
		
		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
		{
            waitForElementToAppear(ModifyOrderObj.ModifyOrder.TaskReadytoComplete,90,10000);
            click(ModifyOrderObj.ModifyOrder.TaskReadytoComplete,"Workitem in Ready status");
			Reusable.waitForSiebelLoader();
            Completeworkitem(GetText(ModifyOrderObj.ModifyOrder.InflightTaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);         
		}
		
		getUrl(ModifyOrderscreenurl);
	       
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
        verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
        click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");

		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ ModifiedOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,120,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ ModifiedOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
		if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+ModifiedOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, ModifiedOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+ModifiedOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, ModifiedOrdernumber +" Execution InProgress");
		}
	}	
	
	public void Completeworkitem( String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String ResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "End Resilience Option");
		String AccessNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Access nwElement");
		String AccessPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Access port");
		String CPENNiPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE NNI Port 1");
		String CPENNiPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE NNI Port 2");
		String PeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE NetworkElement");
		String PEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PEPort_1");
		String PEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PEPort_2");
		String VcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VCX Control");
		String Beacon = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Beacon");
		String EgressPolicyMap = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Egress Policy Map");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");
//		String L3cpeName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE Name");
//		String L3cpeUniPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE UNI Port");
//		String PrimarySR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR");
//		String PrimarySRGil = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR GIL");
//		String WanIpv4SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Wan IPv4 Subnet Size");
//		String DuplexBfdInterval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Duplex/Bfd interval");
//		String SpeedMultiplier = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Speed/Multiplier");
//		String PremiumCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premium CIR%");
//		String InternetCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Internet CIR%");
		
		System.out.println("In Switch case with TaskName :"+taskname);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Start completion of Task: "+taskname[0]);
		switch(taskname[0])
		{
			case "Reserve Access Resources":
			if (ResilienceOption.contains("Proctected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.workItems.AccessNetworkElement,"Access Network Element");
				clearTextBox(NcOrderObj.workItems.AccessNetworkElement);
				sendKeys(NcOrderObj.workItems.AccessNetworkElement,AccessNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AccessPort,"Access Port");
				clearTextBox(NcOrderObj.workItems.AccessPort);
				sendKeys(NcOrderObj.workItems.AccessPort,AccessPort);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort1,"CPE NNI Port1");
				clearTextBox(NcOrderObj.workItems.CPENNIPort1);
				sendKeys(NcOrderObj.workItems.CPENNIPort1,CPENNiPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort1, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort2,"CPE NNI Port2");
				clearTextBox(NcOrderObj.workItems.CPENNIPort2);
				sendKeys(NcOrderObj.workItems.CPENNIPort2,CPENNiPort2);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort2, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
				//Code for Unprotected
				verifyExists(NcOrderObj.workItems.AccessNetworkElement,"Access Network Element");
				clearTextBox(NcOrderObj.workItems.AccessNetworkElement);
				sendKeys(NcOrderObj.workItems.AccessNetworkElement,AccessNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AccessPort,"Access Port");
				clearTextBox(NcOrderObj.workItems.AccessPort);
				sendKeys(NcOrderObj.workItems.AccessPort,AccessPort);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort1,"CPE NNI Port1");
				clearTextBox(NcOrderObj.workItems.CPENNIPort1);
				sendKeys(NcOrderObj.workItems.CPENNIPort1,CPENNiPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort1, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Transport Circuit Design":
			if (ResilienceOption.contains("Proctected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
				clearTextBox(NcOrderObj.workItems.VCXController);
				sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
				clearTextBox(NcOrderObj.workItems.Beacon);
				sendKeys(NcOrderObj.workItems.Beacon,Beacon);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForSiebelLoader();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort2);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForSiebelLoader();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForSiebelLoader();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
				clearTextBox(NcOrderObj.workItems.VCXController);
				sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
				clearTextBox(NcOrderObj.workItems.Beacon);
				sendKeys(NcOrderObj.workItems.Beacon,Beacon);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Manual Design Task for MPR":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.EgressPolicyMap,"Egress Policy Map");
				clearTextBox(NcOrderObj.workItems.EgressPolicyMap);
				sendKeys(NcOrderObj.workItems.EgressPolicyMap,EgressPolicyMap);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.EgressPolicyMap, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForSiebelLoader();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForSiebelLoader();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
	        
//			case "Reserve MPR Resources" :
//			{
//				verifyExists(NcOrderObj.workItems.L3cpeName,"L3CPE Name");
//				clearTextBox(NcOrderObj.workItems.L3cpeName);
//				sendKeys(NcOrderObj.workItems.L3cpeName,L3cpeName);
//				Reusable.waitForSiebelLoader();
//				Reusable.SendkeaboardKeys(NcOrderObj.workItems.L3cpeName, Keys.ENTER);
//				Reusable.waitForSiebelSpinnerToDisappear();
//
//				verifyExists(NcOrderObj.workItems.L3cpeUNIPort,"L3CPE UNI Port");
//				clearTextBox(NcOrderObj.workItems.L3cpeUNIPort);
//				sendKeys(NcOrderObj.workItems.L3cpeUNIPort,L3cpeUniPort);
//				Reusable.waitForSiebelLoader();
//				Reusable.SendkeaboardKeys(NcOrderObj.workItems.L3cpeUNIPort, Keys.ENTER);
//				Reusable.waitForSiebelSpinnerToDisappear();
//
//				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
//				click(NcOrderObj.workItems.Complete,"Complete Workitem");
//				Reusable.waitForAjax();
//			}
//			break;
//			
//			case "Verify SR Router and GIL":
//			{
//				verifyExists(NcOrderObj.workItems.Asr,"ASR");
//				clearTextBox(NcOrderObj.workItems.Asr);
//				sendKeys(NcOrderObj.workItems.Asr,PrimarySR);
//				Reusable.waitForSiebelLoader();
//				Reusable.SendkeaboardKeys(NcOrderObj.workItems.Asr, Keys.ENTER);
//				Reusable.waitForSiebelSpinnerToDisappear();
//
//				verifyExists(NcOrderObj.workItems.AsrGil,"ASR GIL");
//				clearTextBox(NcOrderObj.workItems.AsrGil);
//				sendKeys(NcOrderObj.workItems.AsrGil,PrimarySRGil);
//				Reusable.waitForSiebelLoader();
//				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AsrGil, Keys.ENTER);
//				Reusable.waitForSiebelSpinnerToDisappear();
//
//				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
//				click(NcOrderObj.workItems.Complete,"Complete Workitem");
//				Reusable.waitForAjax();
//			}
//			break;
//
//			case "Set Bespoke Parameters":
//			{
//				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				
//				verifyExists(NcOrderObj.workItems.WanIpSubnetSize,"WAN IPv4 Subnet Size");
//				select(NcOrderObj.workItems.WanIpSubnetSize,WanIpv4SubnetSize);
//
//				verifyExists(NcOrderObj.workItems.WanIpSubnetSizeFlag,"WAN IPv4 Subnet Size Flag");
//				select(NcOrderObj.workItems.WanIpSubnetSizeFlag,"Yes");
//
//				verifyExists(NcOrderObj.workItems.OtherWanIpSubnetSize,"Other WAN IPv4 Subnet Size");
//				select(NcOrderObj.workItems.OtherWanIpSubnetSize,DuplexBfdInterval);
//				Reusable.waitForAjax();
//				
//				verifyExists(NcOrderObj.createOrder.Update,"Update button");
//				click(NcOrderObj.createOrder.Update,"Update button");
//				Reusable.waitForSiebelLoader();
//				
//				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//
//				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
//				click(NcOrderObj.addProductDetails.Edit,"Edit button");
//				Reusable.waitForSiebelLoader();
//				
//				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//
//				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
//				click(NcOrderObj.workItems.Complete,"Complete Workitem");
//				Reusable.waitForAjax();
//			}
//			break;
//			
			case "Select LAN IP Range":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.RequestNewIp,"Request New IP from EIP button");
				click(NcOrderObj.workItems.RequestNewIp,"Request New IP from EIP button");
				Reusable.waitForAjax();
			}
			break;
			
			case "Waiting for RIR Approval":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Load License to MPR":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
	        
			case "Check Order parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
	    	case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
	    	case "Activation Start Confirmation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Confirm L3 Activation Start":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "MPR Trigger Configuration Start":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Duplex Check":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "Speed Test":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "Remote Connectivity Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Customer Dynamic Routing Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
	        break;

//			case"Set Bandwidth Distribution Parameters":
//			{
//				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//
//				sendKeys(NcOrderObj.workItems.PremiumCir,PremiumCir);
//				sendKeys(NcOrderObj.workItems.InternetCir,InternetCir);
//				
//				verifyExists(NcOrderObj.createOrder.Update,"Update button");
//				click(NcOrderObj.createOrder.Update,"Update button");
//				Reusable.waitForSiebelLoader();
//				
//				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//
//				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
//				click(NcOrderObj.addProductDetails.Edit,"Edit button");
//				Reusable.waitForSiebelLoader();
//				
//				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//
//				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
//				click(NcOrderObj.workItems.Complete,"Complete Workitem");
//				Reusable.waitForAjax();
//			}
//			break;
//			
			case "Bandwidth Profile Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Check BGP":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
	        break;

			case "Service Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Legacy Activation Completed":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			default:
			// No case found that it will complete the Workitem	
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			Reusable.waitForAjax();
		}
	}
}
