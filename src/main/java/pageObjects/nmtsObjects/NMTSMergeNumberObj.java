package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSMergeNumberObj {
	
	public static class MergeNumber
	{ 
		//public static final String mergebtnCH="@xpath=(//table[@id='tblTaskMenu']//tr)[4]/td/font"; 
		public static final String mergebtnCH="@xpath=//font[contains(@onclick,'MergeNumber')]";
		public static final String MergeButtonforsuper="@xpath=//font[contains(@onclick,'MergeNumber')]"; 
		public static final String MergeRadioBtn="@xpath=//input[@id='radMergeNumerType_0']";
		public static final String poolandstatus="@xpath=//input[@id='radMergeNumerType_1']";
		public static final String Status="@xpath=//select[@id='drpNumberStatus']";
		public static final String Availability="@xpath=//select[@id='drpAvailability']";
		public static final String AreaCode="@xpath=//input[@id='txtAreaCode']";
		public static final String AreaCodeEXtension="@xpath=//input[@id='txtAreaCodeEx']";
		public static final String MergeRangeStart="@xpath=//input[@id='txtRangeFrom']";
		public static final String MergeRangeEnd="@xpath=//input[@id='txtRangeTo']";
		//public static final String IEGeoNumber="@xpath=//select[@id='drpGeoNumber']";
		public static final String GeoNumber="@id=drpGeoNumber";
		
		public static final String DropBlockSize="@xpath=//select[@id='drpBlockSize']";
		public static final String PageSize="@xpath=//select[@id='drpPageSize']";
		public static final String SubmitButton="@xpath=//input[@id='btnSubmitSearch']";
		public static final String IEnorecord="@xpath=//table[@id='grdView']//tbody/tr[2]/td[contains(text(),'No Record Found')]";
		public static final String AllCheckbox="@xpath=//input[@id='grdView__ctl1_checkMergeAll']";
		public static final String RandomCheckbox="@xpath=(//table//td[11])[index]//input";
		public static final String mergeButton="@xpath=//input[@id='btnMergeNumberRange']";
		public static final String targetStatus="@xpath=//select[@id='ddlTargetStatus']";
		public static final String poolandstatusbtn="@xpath=//input[@id='btnChangeStatusPool']";
		public static final String tgtavail="@xpath=//select[@id='ddlTargetAvail']";
		public static final String SelectColtcategory="@xpath=//select[@id='ddlColtCategory']";
		public static final String MergedMessage="@xpath=//*[@id='lblResult']";

		}

}