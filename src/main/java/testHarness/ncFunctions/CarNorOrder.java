package testHarness.ncFunctions;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.CarNorObj;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.ModifyOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class CarNorOrder extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();
	public String SpokeOrderscreenURL;
	public String CeaseOrderscreenURL;
	NcCreateOrder NcOrder = new NcCreateOrder();
	EthernetOrder Ethernet = new EthernetOrder();
	HubOrder HubAndSpoke = new HubOrder();
	IpCarNorOrder IpAccess = new IpCarNorOrder();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();

	public void NcCarnorOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String DhcpFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Feature");

		if (ProductName.toString().contains("Line"))
		{
			GetOrderDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatCarNorOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			NcOrder.AddProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			AddProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.AddFeatureDetails();
			NcOrder.DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.ProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			NcOrder.ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			EditCeaseProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			GetSpokeOrderDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatCarNorSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddSpokeProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			AddProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddSpokeFeature(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.DecomposeSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			GetHubOrderDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddHub(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.SpokeProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.ProcessSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatSpokeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			EditCeaseProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.CompleteSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("IPA"))
		{
			IpAccess.IpAccessGetOrderDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.CreateCarNorOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.AddProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.AddProductdetailsCarNor(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.IpAddressingFeatureUpdateCarNor(testDataFile, sheetName, scriptNo, dataSetNo);
			NcOrder.AddProviderAggregateFeature(testDataFile, sheetName, scriptNo, dataSetNo);
			if (AddressingType.toString().contains("Provider Aggregated"))
			{
				NcOrder.ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			else if (AddressingType.toString().contains("Provider Independent"))
			{
				NcOrder.ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			else
			{
				NcOrder.ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				NcOrder.ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if (DhcpFeature.toString().contains("Yes"))
			{
				NcOrder.AddDhcpFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				NcOrder.DhcpUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				NcOrder.IpDhcpUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			IpAccess.DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.EthernetProductDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.DecomposeEthernetOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.ProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.ProcessIpAccessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.CompleteIpAccessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.CreateIpAccessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.UpdateCeaseDescription();
			IpAccess.EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.DecomposeEthernetOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAccess.CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
	}
	
		
	public void GetOrderDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CarOrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CAR_Order_No");

		Reusable.waitForAjax();
		
		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		verifyExists(CarNorObj.carNor.FilterSelectName,"Select Name");
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input value");
		sendKeys(CarNorObj.carNor.FilterInputValue,CarOrderNo);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply button");
		click(CarNorObj.carNor.ApplyButton,"Apply button");
		
		String[] arrOfStr = CarOrderNo.split("#", 0);
		
		waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		
//		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
// 		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
//		click(CarNorObj.carNor.arrorder+ arrOfStr +CarNorObj.carNor.arrorder2);
		
		verifyExists(CarNorObj.carNor.UnderlyningOrder,"UnderLying Order");
		click(CarNorObj.carNor.UnderlyningOrder,"UnderLying Order");
		
		verifyExists(CarNorObj.carNor.NCSID,"NCSID");
		String NCServiceId = getTextFrom(CarNorObj.carNor.NCSID);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Retained NCS ID", NCServiceId);
		
		verifyExists(CarNorObj.carNor.ProductID,"ProductID");
		String ProductId = getTextFrom(CarNorObj.carNor.ProductID);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CarNorProductID", ProductId);
		System.out.println(ProductId);			
	}
	
	public void CreatCarNorOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bred crumb");
		click(CarNorObj.carNor.Accountbredcrumb,"Account bred crumb");
		
		verifyExists(CarNorObj.carNor.NewCompositeOrder,"New Composite Order");
		click(CarNorObj.carNor.NewCompositeOrder,"New Composite Order");

		verifyExists(CarNorObj.carNor.OrderNumber,"Order Number");
		String Ordernumber = getTextFrom(CarNorObj.carNor.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No", Ordernumber);
		
		//arrOfStr1 not used in same method anywhere
		String[] arrOfStr1 = Ordernumber.split("#", 0); 
		//Log.info(arrOfStr1[1]);
		
		verifyExists(CarNorObj.carNor.OrderDescription,"Order Description");
		sendKeys(CarNorObj.carNor.OrderDescription,"CarNor Order created using CTAF Automation script");
			
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		
		verifyExists(CarNorObj.carNor.LinkforOrder,"Link for Order");
		String Orderscreenurl = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
	}
	
	public void AddProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String Orderreferencenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Sys Ref ID");
		String OrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		String CarNorProductId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CarNorProductID");
		String RetainedNcsId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Retained NCS ID");
		String Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
		String SpokeBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke Bandwidth");
		String OrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		String SpokeOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
		
		if (ProductName.toString().contains("Ethernet Line"))
		{
			getUrl(OrderscreenURL);
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			getUrl(SpokeOrderscreenURL);
		}

		verifyExists(EthernetOrderObj.CompositOrders.UnderlyningOrder,"UnderlyningOrder");
		click(EthernetOrderObj.CompositOrders.UnderlyningOrder,"UnderlyningOrder");	
		
		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
		click(EthernetOrderObj.CompositOrders.Edit,"Edit button");	
		
		String Spark= "SPARK";
		verifyExists(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
		click(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.OrderSystemName,"SPARK","Order System Name");
		
		verifyExists(EthernetOrderObj.CompositOrders.Orderreferencenumber,"Orderreferencenumber");
		sendKeys(EthernetOrderObj.CompositOrders.Orderreferencenumber,Orderreferencenumber,"Orderreferencenumber");	
		
		String Topology = "Point to Point";
		verifyExists(EthernetOrderObj.CompositOrders.Topology,"Topology");
		click(EthernetOrderObj.CompositOrders.Topology,"Topology");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.Topology,Topology,"Topology");
		
		verifyExists(EthernetOrderObj.CompositOrders.Ordernumber,"Ordernumber");
		sendKeys(EthernetOrderObj.CompositOrders.Ordernumber,OrderNo,"Ordernumber");	
		
		verifyExists(EthernetOrderObj.CompositOrders.CommercialProductName,"CommercialProductName");
		sendKeys(EthernetOrderObj.CompositOrders.CommercialProductName,"ETHERNET SPOKE","CommercialProductName");	
		
		verifyExists(EthernetOrderObj.CompositOrders.CarnorProductId,"CarnorProductId");
		sendKeys(EthernetOrderObj.CompositOrders.CarnorProductId,CarNorProductId,"CarnorProductId");
		
		verifyExists(EthernetOrderObj.CompositOrders.RetainedNCSerId,"Retained NCS ID");
		sendKeys(EthernetOrderObj.CompositOrders.RetainedNCSerId,RetainedNcsId,"Retained NCS ID");
		
		String circuitCategory= "LE";
		verifyExists(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		click(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.CircuitCategory,circuitCategory,"Circuit Category");
		
		if (ProductName.toString().contains("Ethernet Line"))
		{
			verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
			sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,Bandwidth,"ServiceBandwidth");
			Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
			sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,SpokeBandwidth,"ServiceBandwidth");
			Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
		}
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.Update,"Details are Updated");
		click(EthernetOrderObj.CompositOrders.Update,"Details are Updated");	
		waitForAjax();
	}

	public void CreatCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CarOrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CAR_Order_No");

		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input Value");
		sendKeys(CarNorObj.carNor.FilterInputValue,CarOrderNo);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");

		String[] arrOfStr = CarOrderNo.split("#", 0);
		
//		ClickonElementByString(CarNorObj.carNor.arrorder+arrOfStr+CarNorObj.carNor.arrorder1,60);
//		waitForAjax();
//		click(CarNorObj.carNor.arrorder+ arrOfStr +CarNorObj.carNor.arrorder2);
		
		waitForElementToAppear(CarNorObj.carNor.arrorder+arrOfStr[1]+CarNorObj.carNor.arrorder1,60);		

		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);

		String ProductInstancenumber = getTextFrom(CarNorObj.carNor.ProductInstNumber);
		
		String[] arrOfStr2 = ProductInstancenumber.split("#", 0); 
		
		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		click(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		
		verifyExists(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		click(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		
		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		sendKeys(CarNorObj.carNor.FilterInputValue,arrOfStr2[1]);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");
		
//		click(CarNorObj.carNor.arrorder + arrOfStr2[1] + CarNorObj.carNor.arrorder3);
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		click(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		waitForAjax();
		
		String CeaseOrdernumber = getTextFrom(CarNorObj.carNor.LinkforOrder);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No", CeaseOrdernumber);

		String CeaseOrderscreenURL = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL", CeaseOrderscreenURL);

	}

	public void CreatSpokeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String SpokeOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CAR_Order_No");

		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input Value");
		sendKeys(CarNorObj.carNor.FilterInputValue,SpokeOrderNumber);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");

		String[] arrOfStr = SpokeOrderNumber.split("#", 0);
		
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
//		click(CarNorObj.carNor.arrorder+ arrOfStr +CarNorObj.carNor.arrorder2);
		
		String ProductInstancenumber = getTextFrom(CarNorObj.carNor.ProductInstNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Product_Instance_Number", ProductInstancenumber);
		
		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		click(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0);
		
		verifyExists(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		click(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		
		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		sendKeys(CarNorObj.carNor.FilterInputValue,arrOfStr1[1]);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		click(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		waitForAjax();
		
		String CeaseOrdernumber = getTextFrom(CarNorObj.carNor.LinkforOrder);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No",CeaseOrdernumber);
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		
		String CeaseOrderscreenURL = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL",CeaseOrderscreenURL);
		
		Reusable.waitForAjax();
	}
		
	public void EditCeaseProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
		verifyExists(CarNorObj.carNor.DisconnectOrder,"Disconnect Order");
		click(CarNorObj.carNor.DisconnectOrder,"Disconnect Order");
		
		verifyExists(CarNorObj.carNor.Edit,"Edit");
		click(CarNorObj.carNor.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
	}			

	public void DecomposeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		
		getUrl(CeaseOrderscreenURL);
		verifyExists(CarNorObj.carNor.OrderTab,"Order Tab");
		click(CarNorObj.carNor.OrderTab,"Order Tab");
		
		verifyExists(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		click(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		
		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
		click(CarNorObj.carNor.Decompose,"Decompose");
		
		waitForAjax();
	}
	
	public void DecomposeSpokeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
		verifyExists(CarNorObj.carNor.OrderTab,"Order Tab");
		click(CarNorObj.carNor.OrderTab,"Order Tab");
		
		verifyExists(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		click(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		
		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
		click(CarNorObj.carNor.Decompose,"Decompose");
		waitForAjax();
	}
	
	public void ProcessCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No");
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");

		getUrl(CeaseOrderscreenURL);

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
		
		getUrl(CeaseOrderscreenURL);
        
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
        
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,200,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
	    
	    if(OrderStatus.equalsIgnoreCase("Blocked by Errors"))
		{
		    if (DeviceType.contains("Stub"))
			{
		    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2);
			
				verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
				click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
				
				verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
				click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
				click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
				
				waitForAjax();
			
				if(verifyExists("@xpath=//td[text()='Error']/following-sibling::*//a[text()='Delete Service in Smarts']/parent::*/preceding-sibling::*//input"))
				{
					ErrorWorkItems.errorWorkitems("Delete Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
					click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
					Reusable.waitForAjax();
			        
					verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
					click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
					
					selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
					
					sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
					
					verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
					click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
					
					verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
					click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
					
					waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,200,10000);
					if (OrderStatus.contains("Process Completed"))
					{
						Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
						ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
					}
				}			
				else
				{
					System.out.println("Order is blocked by different errors");
				}
			}
		}    
	    else if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Execution InProgress");
		}
	}
	
	public void GetSpokeOrderDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
//		String arrOfStr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CompOrdernumber");
		String CarOrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CAR_Order_No");

		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		verifyExists(CarNorObj.carNor.FilterSelectName,"Select Name");
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input value");
		sendKeys(CarNorObj.carNor.FilterInputValue,CarOrderNo);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply button");
		click(CarNorObj.carNor.ApplyButton,"Apply button");
		
		String[] arrOfStr = CarOrderNo.split("#", 0);
	
		waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		
//		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
// 		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
//		click(CarNorObj.carNor.arrorder+ arrOfStr +CarNorObj.carNor.arrorder2);
		
		verifyExists(CarNorObj.carNor.UnderlyningOrder,"UnderLying Order");
		click(CarNorObj.carNor.UnderlyningOrder,"UnderLying Order");
		
		verifyExists(CarNorObj.carNor.NCSID,"NCSID");
		String NCServiceId = getTextFrom(CarNorObj.carNor.NCSID);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Retained NCS ID", NCServiceId);
		
		verifyExists(CarNorObj.carNor.ProductID,"ProductID");
		String ProductId = getTextFrom(CarNorObj.carNor.ProductID);
		System.out.println(ProductId);			
	}

	public void CreatCarNorSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account orders screen");
		click(CarNorObj.carNor.Accountbredcrumb,"Account orders screen");
		
		verifyExists(CarNorObj.carNor.NewCompositeOrder,"New Composite Order");
		click(CarNorObj.carNor.NewCompositeOrder,"New Composite Order");
		
		verifyExists(CarNorObj.carNor.OrderNumber,"Order Number");
		String SpokeOrdernumber = getTextFrom(CarNorObj.carNor.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke_Order_No", SpokeOrdernumber);
		String[] arrOfStr = SpokeOrdernumber.split("#", 0);
		
		verifyExists(CarNorObj.carNor.OrderDescription,"Order Description");
		sendKeys(CarNorObj.carNor.OrderDescription,"Spoke Order Created using Automation script");
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		
		String SpokeOrderscreenURL = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke_Order_URL", SpokeOrderscreenURL);
	}
		
	public void DecomposeSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String SpokeOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
		
		openurl(SpokeOrderscreenURL);
		verifyExists(CarNorObj.carNor.OrderTab,"Order Tab");
		click(CarNorObj.carNor.OrderTab,"Order Tab");
		
		verifyExists(CarNorObj.carNor.Suborder,"Suborder");
		click(CarNorObj.carNor.Suborder,"Suborder");
		
		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
		click(CarNorObj.carNor.Decompose,"Decompose");
		waitForAjax();
	}
		
	public void GetHubOrderDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String HubOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub_Order_No");
//		String CompOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"P2P/Spoke Order Number");

		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		click(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");

		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input Value");
		sendKeys(CarNorObj.carNor.FilterInputValue,HubOrder);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");
		
		String[] arrOfStr4 = HubOrder.split("#", 0);
		
		waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr4[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		
//		verifyExists(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr4[1] +ModifyOrderObj.ModifyOrder.Order2);
// 		click(ModifyOrderObj.ModifyOrder.Order1+ arrOfStr[1] +ModifyOrderObj.ModifyOrder.Order2);
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr4[1]+"')]/parent::*"),10);
		
		verifyExists(CarNorObj.carNor.UnderlyningHubOrder,"Underlyning Hub Order");
		click(CarNorObj.carNor.UnderlyningHubOrder,"Underlyning Hub Order");
		
		String HubNCServiceId = getTextFrom(CarNorObj.carNor.NCSID);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub_NCS_ID", HubNCServiceId);
	}
		
//		public void ExistingOrder() throws Exception
//		{
//			verifyExists(CarNorObj.carNor.AccountNameSorting,"Account Name Sorting");
//			click(CarNorObj.carNor.AccountNameSorting,"Account Name Sorting");
//			
//			verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
//			click(CarNorObj.carNor.NameFiltering,"Name Filtering");
//			
//			select(CarNorObj.carNor.FilterSelectName,"Name");
//			
//			sendKeys(CarNorObj.carNor.FilterInputValue,"535455");
//			
//			verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
//			click(CarNorObj.carNor.ApplyButton,"Apply Button");
//			
//			String Ordernumber = "Order #0000535455";
//			//Ordernumber.set("Order #0000535455");
//			//Log.info(Ordernumber.get());
//			String[] arrOfStr = Ordernumber.split("#", 0);
//			//Log.info(arrOfStr[1]);
//			
//			ClickonElementByString(CarNorObj.carNor.arrorder+arrOfStr[1]+CarNorObj.carNor.arrorder2,60);
//			waitForAjax();
//			OrderscreenURL = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
//			
//			verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
//			click(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
//		}
		
}
